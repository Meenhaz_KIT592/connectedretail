/**
 * PUBLIC
 * This is the model for frmShoppingList form
 */
var MVCApp = MVCApp || {};
MVCApp.ShoppingListModel = (function(){
var serviceType =   MVCApp.serviceType.Registration;
var serviceType1= MVCApp.serviceType.ShoppingList;

  
 function addToProductList(callback,requestParams,type, productID)
  {

var operationId = MVCApp.serviceType.AddToProductList;
 
       MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
         
          
           var status = results.Status ||[];
         
         if(0< status.length && status[0].errCode !== undefined &&
            status[0].errCode !== null && status[0].errCode !== "")
           {
             
               var key  = status[0].errCode ;
               MVCApp.Toolbox.common.dismissLoadingIndicator();
              alert(MVCApp.Toolbox.common.geti18nValueVA(key));
           }
         else
           {
             if(results.opstatus===0 || results.opstatus==="0" ){
               kony.print("Response is "+JSON.stringify(results)); 

                         callback(results);
                       }
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
      MVCApp.Toolbox.common.dismissLoadingIndicator();
      if(error.opstatus== 1401 || error.opstatus== "1401" ){
        gblIsPasswordExpired=true;
        MVCApp.Toolbox.common.checkIfUserIsForTouchID(requestParams.user);
        kony.store.setItem("shoppingListProdOrProjID",productID);
           if(type== MVCApp.Service.Constants.ShoppingList.pjdpType){
      			isPJDPAddProduct = true;
           }else{
          		isPJDPAddProduct=false;
        	}
      isMyList=false;
      MVCApp.getShoppingListController().navigateToLoginScreen();
         }else{ 
              alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
         }
     
    }
                   );
  }
  

  
  function removeFromProductList(requestParams)
  {
var operationId = MVCApp.serviceType.RemoveFromProductList;
 
       MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
   kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
            
         MVCApp.Toolbox.common.dismissLoadingIndicator();
          MVCApp.getShoppingListController().getProductListItems();
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
          MVCApp.Toolbox.common.dismissLoadingIndicator();
      alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
    }
                   );
  }
  function getProductListItems(callback,requestParams)
  {
var operationId = MVCApp.serviceType.GetProductListItems;
 
       MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
   kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
        if(undefined != results.result && null != results.result){
          var shopList = new MVCApp.data.ShoppingList(results.result);   
          callback(shopList);
        }    
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
      MVCApp.Toolbox.common.dismissLoadingIndicator();
         if(error.opstatus== 1401 || error.opstatus== "1401" ){
           isMyList=true;
           gblIsPasswordExpired=true;
           MVCApp.Toolbox.common.checkIfUserIsForTouchID(requestParams.user);
           MVCApp.getShoppingListController().navigateToLoginScreen();
         }else{
           alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
         }
      
    }
                   );
  }
 
  function createProductList(requestParams)
  {
var operationId = MVCApp.serviceType.CreateProductList;
 
       MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
   kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
            
        
          kony.print("success");
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
      
    }
                   );
  }
  function removeProductListLooping(requestParams)
  {
var operationId = MVCApp.serviceType.RemoveProductListLooping;
       MakeServiceCall( serviceType1,operationId,requestParams,
                    function(results)
                    {
   kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
         MVCApp.Toolbox.common.dismissLoadingIndicator();
         if(kony.application.getCurrentForm().id == "frmShoppingList"){
         	MVCApp.getShoppingListController().getProductListItems();
         }
        var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.status="success";
        MVCApp.Customer360.sendInteractionEvent("DeleteAllResult", cust360Obj);
      }   
    },function(error){
         var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.status="failure";
        MVCApp.Customer360.sendInteractionEvent("DeleteAllResult", cust360Obj);
      kony.print("Response is "+JSON.stringify(error));
          MVCApp.Toolbox.common.dismissLoadingIndicator();
      alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
    }
                   );
  }
  function addProjectListLooping(callback,requestParams,projectID)
  {
var operationId =MVCApp.serviceType.AddProductListLooping ;
 
       MakeServiceCall( serviceType1,operationId,requestParams,
                    function(results)
                    {
   kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
            
         callback(results);
      }   
    },function(error){
         kony.print("Response  Shooping list project is "+JSON.stringify(error));
         var loopset= error.LoopDataset ||"";
         if(loopset!=""){
           var errRes= loopset[0] ||"";
           var errOps= errRes.opstatus ||"";
           if(errOps== 1401 || errOps== "1401" ){
            isPJDPAddProduct=false;
            MVCApp.Toolbox.common.checkIfUserIsForTouchID(requestParams.user);
        	kony.store.setItem("shoppingListProdOrProjID",projectID);
        	isMyList=false;
           	gblIsPasswordExpired=true;
           	MVCApp.getShoppingListController().navigateToLoginScreen();
         }else{
           MVCApp.Toolbox.common.dismissLoadingIndicator();
		   alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
         }
         }
         else{
           MVCApp.Toolbox.common.dismissLoadingIndicator();
		   alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
         }
    }
                   );
  }
  return{
          addToProductList:addToProductList,          
     
        removeFromProductList:removeFromProductList,
    	getProductListItems:getProductListItems,
    	
        createProductList:createProductList,
    removeProductListLooping:removeProductListLooping,
    addProjectListLooping:addProjectListLooping
    };
});
