

/**
 * PUBLIC
 * This is the controller for the Browser form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.BrowserController = (function(){
  //Checking Code review
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
  
  var lContentID = "";
  function setContentID(pContentID){
    lContentID = pContentID;
  }
  function getContentID(){
    return lContentID;
  }
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
         
        model = new MVCApp.BrowserModel();
      
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.BrowserView();
        
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
      function load(contentId){
        MVCApp.Toolbox.common.showLoadingIndicator("");
        var requestParam = {};
        requestParam.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParam.content_id = contentId;
        setContentID(contentId);
        requestParam.locale=MVCApp.Toolbox.Service.getLocale();
      	kony.print("Browser Controller.load");
      	init();
        model.loadData(view.updateScreen,requestParam);
    }
          		
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
      init: init,
      load: load,
      getContentID:getContentID
    };
});

