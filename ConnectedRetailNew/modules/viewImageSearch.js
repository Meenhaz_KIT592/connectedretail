var MVCApp = MVCApp || {};
var imageSearchProgressAnimateDuration = 8;
var imageSearchSuccessFlag = false;
var imageSearchWaitFlag = true;
var imageSearchProgressForm = null;
// imageSearchTryAgainFlag will be only set to true in iPhone onClick of Try Again camera widget.
var imageSearchTryAgainFlag = false;
var imageSearchStoreModeRefreshFlag = false;
var imageSearchProjectBasedFlag = false;
var imageSearchPNG = null;
var imageSearchPreToPostFlag = false;


  function onBckToForegroundAtImageSeach()
  {
    if(imageSearchProgressForm)
      {
         imageSearchProgressForm.imgImageSearch.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
         imageSearchProgressForm.flexBody.setVisibility(true);
      }
  }

MVCApp.ImageSearchView = (function(){
  
  function show(){
    frmImageSearch.show();
  }
  
  function bindEvents(){
    if(imageSearchProgressForm === null){
      imageSearchProgressForm = frmImageOverlay;
    }
    if(imageSearchProgressForm.id == "frmImageSearch"){
      imageSearchProgressForm.preShow = imageSearchPreShow;
      imageSearchProgressForm.postShow = imageSearchPostShow;
      imageSearchProgressForm.flexBack.onClick = onClickImageSearchBack;
      //#ifdef iphone
      imageSearchProgressForm.cmrTryAgain.onCapture = MVCApp.getImageSearchController().didCaptureImage;
      imageSearchProgressForm.cmrTryAgain.onTouchEnd = function(){
        imageSearchTryAgainFlag = true;
        imageSearchProgressForm.flexErrorMessage.setVisibility(false);
      };
      var transform = kony.ui.makeAffineTransform();
      transform.scale(1.4, 1.4);
      imageSearchProgressForm.cmrTryAgain.previewTransform = transform;
      imageSearchProgressForm.cmrTryAgain.cameraOptions = {
      	hideControlBar: true
      };
      //#endif
    }
    imageSearchProgressForm.btnCancel.onClick = onClickImageSearchErrorCancel;
    imageSearchProgressForm.btnTryAgain.onClick = onClickImageSearchErrorTryAgain;
    imageSearchProgressForm.onDeviceBack = onClickImageSearchBack;
  }
  
  function onClickImageSearchBack(){
    imageSearchOverlayFlag = false;
    imageSearchProgressFlag = false;
    imageSearchGalleryFlag = false;
    imageSearchTryAgainFlag = false;
    if(!imageSearchStoreModeRefreshFlag && gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      imageSearchStoreModeRefreshFlag = true;
      refreshImageSearchInvokedFormWithStoreSpecificData(true);
    }else{
      var formController = MVCApp._getController(imageSearchInvokedForm.id.substring(3));
      if(formController){
        if(imageSearchInvokedForm.id == "frmHome"){
          frmHome.flxSearchOverlay.setVisibility(false);
          formController.showDefaultView();
        }else if(imageSearchInvokedForm.id == "frmProductsList"){
          frmProductsList.flxSearchOverlay.setVisibility(false);
          formController.backToScreen();
        }else if((imageSearchInvokedForm.id == "frmProductCategory")
                 || (imageSearchInvokedForm.id == "frmProjectsCategory")
                 || (imageSearchInvokedForm.id == "frmProducts")){
          imageSearchInvokedForm.flxSearchOverlay.setVisibility(false);
          formController.displayPage();
        }else if(imageSearchInvokedForm.id == "frmProjects"){
          frmProjects.flxSearchOverlay.setVisibility(false);
          formController.displayProjects();
        }else if((imageSearchInvokedForm.id == "frmProjectsList")
                 || (imageSearchInvokedForm.id == "frmPDP")){
		  imageSearchInvokedForm.flxSearchOverlay.setVisibility(false);          
          formController.showUI();
        }else if((imageSearchInvokedForm.id == "frmPJDP")){
          frmPJDP.flxSearchOverlay.setVisibility(false);
          frmPJDP.show();
        }else if(imageSearchInvokedForm.id == "frmWeeklyAdHome"){
          frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
          MVCApp.getWeeklyAdHomeController().showUI();
         }else if(imageSearchInvokedForm.id == "frmWeeklyAd"){
           frmWeeklyAd.flxVoiceSearch.setVisibility(false);
           MVCApp.getWeeklyAdController().showUI();
         }else if(imageSearchInvokedForm.id == "frmWeeklyAdDetail"){
           frmWeeklyAdDetail.flxVoiceSearch.setVisibility(false);
           MVCApp.getWeeklyAdDetailController().showUI();
        }else if(imageSearchInvokedForm.id == "frmMore"){
           frmMore.flxVoiceSearch.setVisibility(false);
           MVCApp.getMoreController().load();
         }else if(imageSearchInvokedForm.id == "frmShoppingList"){
           frmShoppingList.flxVoiceSearch.setVisibility(false);
           MVCApp.getShoppingListController().showUI();
         }else if(imageSearchInvokedForm.id == "frmProductCategoryMore"){
           MVCApp.getProductCategoryMoreController().displaySamePage();
         }
        else{
          formController.show();
        }
      }else{
        imageSearchInvokedForm.show();
        imageSearchInvokedForm.flxSearchOverlay.setVisibility(false);
      }
    }
  }
  
  function onClickImageSearchErrorCancel(){
    imageSearchOverlayFlag = false;
    imageSearchGalleryFlag = false;
    imageSearchTryAgainFlag = false;
    //#ifdef android
    if(!imageSearchStoreModeRefreshFlag && gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      imageSearchStoreModeRefreshFlag = true;
      refreshImageSearchInvokedFormWithStoreSpecificData(true);
    }else{
      onClickImageSearchBack();
    }
    //#else
      if(!imageSearchStoreModeRefreshFlag && gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
        imageSearchStoreModeRefreshFlag = true;
        refreshImageSearchInvokedFormWithStoreSpecificData(true);
      }else if(kony.application.getCurrentForm().id == "frmImageSearch"){
      	onClickImageSearchBack();
      } 
    //#endif
    imageSearchProgressForm.flexErrorMessage.setVisibility(false);
    cameraWidgetRef.closeCamera();
  }

  function onClickImageSearchErrorTryAgain(){
    imageSearchProgressForm.flexStatusBar.setVisibility(false);
    imageSearchProgressForm.lblSearchStatus.setVisibility(false);
    imageSearchProgressForm.flexErrorMessage.setVisibility(false);
    if(imageSearchProgressForm.id != "frmImageSearch"){
      imageSearchProgressForm.imgImageSearch.setVisibility(false);
      imageSearchProgressForm.flexImageSearch.setVisibility(false);
      imageSearchProgressForm.flexCrossHairs.setVisibility(true);
    }
    //#ifdef android
        cameraWidgetRef.closeCamera();
    	imageSearchStoreModeRefreshFlag = true;
        onClickImageSearchBack();
    	cameraWidgetRef.openCamera();
    //#endif
  }

  function rotateWithoutAnimation(widget, anchor, angle){
      var transform = kony.ui.makeAffineTransform();
      transform.rotate(angle);
      widget.anchorPoint = anchor;
      widget.transform = transform;
  }

  function rotationAnimation1(){
    //#ifdef iphone
      var transform = kony.ui.makeAffineTransform();
      transform.rotate(0);
      imageSearchProgressForm.flexInner1.anchorPoint = { x: 0, y: 1};
      imageSearchProgressForm.lblSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.processing") + "...";
      imageSearchProgressForm.flexInner1.animate(
        kony.ui.createAnimation({100:{opacity:1, transform: transform}}),
        {duration: imageSearchProgressAnimateDuration, delay: 0, fillMode: kony.anim.FILL_MODE_FORWARDS},
        {animationEnd: rotationAnimation2}
      );
    //#else
     	 imageSearchProgressForm.customAnimator.setVisibility(true);
        
         imageSearchProgressForm.lblAndSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.processing") + "...";
    	 imageSearchProgressForm.customAnimator.setProgress(imageSearchProgressAnimateDuration*1000, 0, 250, rotationAnimation2);
     //#endif
    
  }

  function rotationAnimation2(){
    if(imageSearchProgressFlag){
       //#ifdef iphone
      var transform = kony.ui.makeAffineTransform();
      transform.rotate(0);

      imageSearchProgressForm.flexInner2.anchorPoint = { x: 0, y: 1};
      imageSearchProgressForm.lblSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.processing") + "...";
      imageSearchProgressForm.flexInner2.animate(
        kony.ui.createAnimation({100:{opacity:1, transform: transform}}),
        {duration: imageSearchProgressAnimateDuration, delay: 0, fillMode: kony.anim.FILL_MODE_FORWARDS},
        {animationEnd: rotationAnimation3}
      );
      //#else
    	  imageSearchProgressForm.lblAndSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.processing") + "...";
    	  imageSearchProgressForm.customAnimator.setProgress(imageSearchProgressAnimateDuration*1000, 250, 500, rotationAnimation3);
       //#endif
      
    }
  }

  function rotationAnimation3(){
    if(imageSearchProgressFlag){
       //#ifdef iphone
      var transform = kony.ui.makeAffineTransform();
      transform.rotate(0);

      imageSearchProgressForm.flexInner3.anchorPoint = { x: 0, y: 1};
      imageSearchProgressForm.lblSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.searching") + "...";
      imageSearchProgressForm.flexInner3.animate(
        kony.ui.createAnimation({100:{opacity:1, transform: transform}}),
        {duration: imageSearchProgressAnimateDuration, delay: 0, fillMode: kony.anim.FILL_MODE_FORWARDS},
        {animationEnd: rotationAnimation4}
      );
        //#else
       	  imageSearchProgressForm.lblAndSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.searching") + "...";
    	  imageSearchProgressForm.customAnimator.setProgress(imageSearchProgressAnimateDuration*1000, 500, 750, rotationAnimation4);
       //#endif
    }
  }

  function rotationAnimation4(){
    if(imageSearchProgressFlag){
        //#ifdef iphone
      var transform = kony.ui.makeAffineTransform();
      transform.rotate(0);

      imageSearchProgressForm.flexInner4.anchorPoint = { x: 0, y: 1};
      imageSearchProgressForm.lblSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.searching") + "...";
      imageSearchProgressForm.flexInner4.animate(
        kony.ui.createAnimation({100:{opacity:1, transform: transform}}),
        {duration: imageSearchProgressAnimateDuration, delay: 0, fillMode: kony.anim.FILL_MODE_FORWARDS},
        {animationEnd: imageSearchProgressCheck}
      );  
       //#else
       	 imageSearchProgressForm.lblAndSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.searching") + "...";
    	  imageSearchProgressForm.customAnimator.setProgress(imageSearchProgressAnimateDuration*1000, 750, 1000, setBackgroundTemporary);
        
       //#endif
    }
  }
  
   function setBackgroundTemporary() {
		 imageSearchProgressForm.imgImageSearch.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
	     imageSearchProgressForm.flexBody.setVisibility(true);
 	     imageSearchProgressForm.customAnimator.setProgress(100, 1000, 1000, imageSearchProgressCheck);
    }

  
  function imageSearchProgressCheck(){
    
    
    
    if(imageSearchPNG && imageSearchPNG.exists()){
      imageSearchPNG.remove();
    }
    if(imageSearchWaitFlag){
      kony.print("No response yet from Slyce and hence showing error message.");
      imageSearchSuccessFlag = false;
      imageSearchWaitFlag = false;
    }
    if(imageSearchProgressFlag){
      imageSearchProgressFlag = false;
      imageSearchResultsDisplay();
    }
  }
  
  function imageSearchResultsDisplay(){
    if(imageSearchSuccessFlag){
      
      imageSearchOverlayFlag = false;
      imageSearchTryAgainFlag = false;
      if(imageSearchProjectBasedFlag){
        MVCApp.getProjectsListController().showUI();
      }else{
        MVCApp.getProductsListController().backToScreen();
      }
       cameraWidgetRef.closeCamera();
     
    }else{
      imageSearchProgressForm.btnCancel.onClick = onClickImageSearchErrorCancel;
      imageSearchProgressForm.btnTryAgain.onClick = onClickImageSearchErrorTryAgain;
      imageSearchProgressForm.onDeviceBack = onClickImageSearchBack;
      imageSearchProgressForm.lblErrorMessage.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.errorMessage");
      if(imageSearchProgressForm.id == "frmImageOverlay"){
      	imageSearchProgressForm.btnTryAgain.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.tryAgain");
      	imageSearchProgressForm.btnTryAgain.setVisibility(true);
      }else{
        //#ifdef iphone
        imageSearchProgressForm.btnTryAgain.setVisibility(false);
        imageSearchProgressForm.cmrTryAgain.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.tryAgain");
        imageSearchProgressForm.cmrTryAgain.setVisibility(true);
        //#else
        imageSearchProgressForm.btnTryAgain.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.tryAgain");
      	imageSearchProgressForm.btnTryAgain.setVisibility(true);
        //#endif
      }
      imageSearchProgressForm.btnCancel.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel");
      imageSearchProgressForm.flexErrorMessage.setVisibility(true);
      imageSearchProgressForm.flexStatusBar.setVisibility(false);
      imageSearchProgressForm.lblSearchStatus.setVisibility(false);
    }
    imageSearchGalleryFlag = false;
  }

  function imageSearchPreShow(){
    
    var angle = 90;
    var angle1 = -90;
	//#ifdef iphone
    imageSearchPreToPostFlag = true;
    enableStatusbarForIOS();
    rotateWithoutAnimation(imageSearchProgressForm.flexInner1, { x: 0, y: 1}, angle);

    rotateWithoutAnimation(imageSearchProgressForm.flex2, { x: 0, y: 1}, angle1);
    rotateWithoutAnimation(imageSearchProgressForm.flexInner2, { x: 0, y: 1}, angle);

    rotateWithoutAnimation(imageSearchProgressForm.flex3, { x: 0, y: 1}, 2 * angle1);
    rotateWithoutAnimation(imageSearchProgressForm.flexInner3, { x: 0, y: 1}, angle);

    rotateWithoutAnimation(imageSearchProgressForm.flex4, { x: 0, y: 1}, 3 * angle1);
    rotateWithoutAnimation(imageSearchProgressForm.flexInner4, { x: 0, y: 1}, angle);
    //#endif
    imageSearchProgressFlag = true;
    //#ifdef iphone
    imageSearchProgressForm.imgInnerRing.rawBytes = imageSearchProgressForm.imgImageSearch.rawBytes;
    //#else
 	 if(imageSearchGalleryFlag){
     imageSearchProgressForm.flexBody.setVisibility(true);
     imageSearchProgressForm.lblAndSearchStatus.setVisibility(true);
     imageSearchProgressForm.lblSearchStatus.setVisibility(false);
     
       
     }
    //#endif
    var screenWidth = kony.os.deviceInfo().screenWidth;
    var screenHeight = kony.os.deviceInfo().screenHeight;
    var excessWidth = 200;
    var excessHeight = 100;
    if(imageSearchGalleryFlag){
      excessWidth = 0;
      excessHeight = 0;
      imageSearchProgressForm.flexImageSearch.right = "0dp";
      imageSearchProgressForm.flexImageSearch.left = "0dp";
      imageSearchProgressForm.flexImageSearch.top = "0dp";
      imageSearchProgressForm.imgImageSearch.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
      imageSearchProgressForm.imgInnerRing.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
    }else{
      //#ifdef iphone
      imageSearchProgressForm.flexImageSearch.right = "-100dp";
      imageSearchProgressForm.flexImageSearch.left = "-100dp";
      imageSearchProgressForm.flexImageSearch.top = "-100dp";
      //#else
      excessWidth = 120;
      excessHeight = 0;
      imageSearchProgressForm.flexImageSearch.right = "-60dp";
      imageSearchProgressForm.flexImageSearch.left = "-60dp";
      imageSearchProgressForm.flexImageSearch.top = "0dp";
      //#endif
      imageSearchProgressForm.imgImageSearch.imageScaleMode = constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS;
      imageSearchProgressForm.imgInnerRing.imageScaleMode = constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS;
    }
    //#ifdef iphone
    var leftPos = ((screenWidth+excessWidth) - 155)/2;
    var topPos = ((screenHeight+excessHeight) - 155)/2;
    imageSearchProgressForm.imgInnerRing.left = -leftPos;
    imageSearchProgressForm.imgInnerRing.top = -topPos;
    imageSearchProgressForm.imgInnerRing.width = screenWidth+excessWidth;
    imageSearchProgressForm.imgInnerRing.height = screenHeight+excessHeight;
     imageSearchProgressForm.lblSearchStatus.setVisibility(true);
     //#endif
    
   
   
   
    if(imageSearchProgressForm.flexErrorMessage.isVisible){
      imageSearchProgressForm.flexErrorMessage.setVisibility(false);
    }
    if(imageSearchProgressForm.id == "frmImageOverlay"){
      imageSearchPostShow();
    }
  }
  
  function imageSearchPostShow(){
    //#ifdef iphone
    if(imageSearchProgressForm.id != "frmImageOverlay"){
      if(!imageSearchPreToPostFlag){
        imageSearchPreShow();
      }
      imageSearchPreToPostFlag = false;
    }
    //#endif
    rotationAnimation1();
  }
  
  function enableStatusbarForIOS()
  {
     imageSearchProgressForm.flexBody.setVisibility(true);
	 imageSearchProgressForm.flexStatusBar.setVisibility(true);
     imageSearchProgressForm.flexStatusBar.flex1.setVisibility(true);
     imageSearchProgressForm.flexStatusBar.flex2.setVisibility(true);
     imageSearchProgressForm.flexStatusBar.flex3.setVisibility(true);
     imageSearchProgressForm.flexStatusBar.flex4.setVisibility(true);
     imageSearchProgressForm.flexStatusBar.flexInnerRing.setVisibility(true);
    
  }
  
  return{
    	show: show,
 		bindEvents: bindEvents,
    	onClickImageSearchBack: onClickImageSearchBack,
    	imageSearchPreShow: imageSearchPreShow
    
    };
});