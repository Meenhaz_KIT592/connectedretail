/**
 * PUBLIC
 * This is the model for frmRewardsProfileStep4 form
 */
var MVCApp = MVCApp || {};
MVCApp.RewardsProfilePreferedStoreModel = (function(){
 var serviceType = "";
  var operationId = "";
  function loadDataFromSearch(requestParams,callback){
    var serviceType=MVCApp.serviceType.checkNearbyProviders;
   var operation=MVCApp.serviceEndPoints.searchByZip;
  	MVCApp.Toolbox.common.showLoadingIndicator();
   MakeServiceCall( serviceType,operation,requestParams,
                           function(results)
                           {
      kony.print("Response in search "+JSON.stringify(results));
      var storeData=new MVCApp.data.dataRewardsPreferedStore(results);
      callback(storeData);      
    },function(error){
     MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"), "");
     MVCApp.Toolbox.common.dismissLoadingIndicator();
   });
  }
 	return{
      loadDataFromSearch:loadDataFromSearch
    };
});
