/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
Childflg = false;
MVCApp.RewardsProfileChildView = (function(){
 
  /**
	 * PUBLIC
	 * Open the Events form
	 */
  
  function show(nxtFlag){           
    var data = frmRewardsProfileAddChild.segChildAgeLevel.data;         
    var childFlag = false;   
    if(data !="" && data != null && data != undefined){
      for(var i =0;i<data.length;i++){                      
        if(data[i].txtChild.text !=undefined && data[i].txtChild.text !="")      
          childFlag = true;
      }   
    }
    if(nxtFlag == true){
      frmRewardsProfileAddChild.btnReset.isVisible = false;
      frmRewardsProfileAddChild.btnNextToRewardsInterests.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update"); 
    }
    else if(childFlag ==false){
      frmRewardsProfileAddChild.btnReset.isVisible = true;
      frmRewardsProfileAddChild.btnNextToRewardsInterests.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip");
    }else{
      frmRewardsProfileAddChild.btnReset.isVisible = true;
      frmRewardsProfileAddChild.btnNextToRewardsInterests.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
    }
    frmRewardsProfileAddChild.show();
  } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   
  function bindEvents(){
    frmRewardsProfileAddChild.postShow = function(){
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Rewards Profile Creation: Step 3: Michaels Mobile App";
      lTealiumTagObj.page_name = "Rewards Profile Creation: Step 3: Michaels Mobile App";
      lTealiumTagObj.page_type = "Rewards";
      lTealiumTagObj.page_category_name = "Rewards";
      lTealiumTagObj.page_category = "Rewards";
      TealiumManager.trackView("Rewards Profile Creation Step 3 Screen", lTealiumTagObj);
    };
    MVCApp.tabbar.bindEvents(frmRewardsProfileAddChild);
	MVCApp.tabbar.bindIcons(frmRewardsProfileAddChild,"frmMore");
    frmRewardsProfileAddChild.btnNextToRewardsInterests.onClick =  function(){
      var formId = kony.application.getPreviousForm().id;
      if(formId == "frmReviewProfile"){
        var data = frmRewardsProfileAddChild.segChildAgeLevel.data;     
    	rewardsProfileData.childData = data;
    	rewardsProfileData.child = [];
        var txtflag = false; 
        rewardsProfileData.c_childrendata="";
    	for(var i =0;i<data.length;i++){
      var originalData = [];   
      if(data[i].txtChild!=undefined){
        if(data[i].txtChild.text!=undefined){
      originalData.txtChild= data[i].txtChild;      
      originalData.lblChild= data[i].lblChild;
      originalData.value= data[i].value;
      } else{
        originalData.txtChild= "";      
      originalData.lblChild= data[i].lblChild;
      originalData.value= data[i].value;
      }          
      rewardsProfileData.child.push(originalData);
      } 
          var flagFor = originalData.txtChild.text || "";
          if(flagFor!=""){
        flagFor = parseInt(flagFor);
        for(var j=1;j<=flagFor;j++){
          if(j==1 && txtflag == false){
            rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata + data[i].value;
		    txtflag = true;
          }  
		  else if(txtflag == true)
		  rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata +","+  data[i].value;
          
        }
        }
        }
        updateFlag = true;
        MVCApp.getReviewProfileController().load();
      }
      else
        rewardsProfileInterests();
    };
	frmRewardsProfileAddChild.btnHeaderLeft.onClick = showPreviousScreen;     
    frmRewardsProfileAddChild.btnReset.onClick = onClickBtnResetRewardsProfileChild;//resetFields;
    frmRewardsProfileAddChild.btnYes.onClick = navigateToMoreScreen;
    frmRewardsProfileAddChild.flxOverlay.onTouchEnd = dummyOverlay;
  }
  
  function onClickBtnResetRewardsProfileChild(){
    var data = frmRewardsProfileAddChild.segChildAgeLevel.data;     
    rewardsProfileData.childData = data;
    rewardsProfileData.child = [];
    rewardsProfileData.c_childrendata = "";
    var txtflag = false;    
    if(null !== data && undefined !== data)
      {
          for(var i = 0;i < data.length;i++){
            var originalData = [];   
            if(data[i].txtChild!=undefined){
              originalData.txtChild = data[i].txtChild;      
              originalData.lblChild = data[i].lblChild;
              originalData.value = data[i].value;
              var flagFor = originalData.txtChild.text;
              flagFor = parseInt(flagFor);
              for(var j = 1;j <= flagFor;j++){
                if(j==1 && txtflag == false){
                  rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata + data[i].value;
                  txtflag = true;
                }  
                else if(txtflag == true)
                  rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata +","+  data[i].value;
              }            
              rewardsProfileData.child.push(originalData);
            } 
          }
    }
    MVCApp.getReviewProfileController().updateRewardProfile("step3");
  }
  
  function navigateToMoreScreen(){
    frmRewardsProfileAddChild.flxOverlay.isVisible = false;
    MVCApp.getMoreController().load();
  }
  
  function dummyOverlay(){
    kony.print("LOG:dummyOverlay-viewRewardsProfileChild.js-START");
  }
 
  function updateScreen(){  
    var childDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
     childDat = childDat.getRewardsProfileChildData();
    if (childDat !=  []) {
    frmRewardsProfileAddChild.segChildAgeLevel.data = childDat;
    }
    Childflg = true;
    show(false);
  }
  
  function resetFields(){    
    var data = frmRewardsProfileAddChild.segChildAgeLevel.data;         
    rewardsProfileData.c_childrendata="";
    var txtflag = false;    
    for(var i =0;i<data.length;i++){	
     data[i].txtChild.text = "";
      frmRewardsProfileAddChild.segChildAgeLevel.setDataAt(data[i], i, 0);    	            
    }
    var formId = kony.application.getPreviousForm().id;
     if(formId == "frmReviewProfile" || formId == "frmRewardsProfileStep4" || formId =="frmMore")
        show(true);
    else
    show(false);    
    
  }
  function showPreviousScreen(){    
	var formId = kony.application.getPreviousForm().id;    
    if(formId == "frmReviewProfile")
      MVCApp.getReviewProfileController().load();
    else
    MVCApp.getRewardsProfileAdrsController().updateData();
  }
    
  function rewardsProfileInterests(){
    var data = frmRewardsProfileAddChild.segChildAgeLevel.data;     
    rewardsProfileData.childData = data;
    rewardsProfileData.child = [];
    rewardsProfileData.c_childrendata="";
    var txtflag = false;    
    if(null !== data && undefined !== data )
      {
        for(var i =0;i<data.length;i++){
          var originalData = [];   
          if(data[i].txtChild!=undefined){
          originalData.txtChild= data[i].txtChild;      
          originalData.lblChild= data[i].lblChild;
          originalData.value= data[i].value;
            var flagFor = originalData.txtChild.text;
            flagFor = parseInt(flagFor);
            for(var j=1;j<=flagFor;j++){
              if(j==1 && txtflag == false){
                rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata + data[i].value;
                txtflag = true;
              }  
              else if(txtflag == true)
              rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata +","+  data[i].value;

            }            
          rewardsProfileData.child.push(originalData);
          } 
        }       
  }
    var formId = kony.application.getPreviousForm().id;	
    if(formId == "frmReviewProfile"){
    updateFlag = true;
    MVCApp.getReviewProfileController().load();
    }
    else if(skillsflag === true)
    MVCApp.getRewardsProfileSkillsController().updateData(false);    
    else
      MVCApp.getRewardsProfileSkillsController().load();
    
  }
    
  function updateData(nxtFlag){
    if(rewardsProfileData.childData != undefined && rewardsProfileData.childData.length !==0)
    frmRewardsProfileAddChild.segChildAgeLevel.data = rewardsProfileData.childData;    
    else{
      updateScreen();
    }
    show(nxtFlag);
  }
  
  //Here we expose the public variables and functions
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	updateData:updateData
    };
  
});