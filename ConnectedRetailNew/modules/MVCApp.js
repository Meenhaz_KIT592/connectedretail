//Type your code here
var MVCApp = MVCApp || {};
MVCApp._controllers = {};

//Private helper functions, will determine if an instance exists, create one and/or return one
MVCApp._hasController = function (name) {
  if (MVCApp._controllers[name]===undefined || MVCApp._controllers[name]===null) {
    return false;	
  } else { return true;}
};
//To send metrics data
MVCApp.sendMetricReport=function(formObj,reportData){
  try{
    if(formObj){
      KNYMetricsService.sendCustomMetrics(formObj.id, reportData);
    }
  }catch(e){
    kony.print("Error at sending Custom report:"+JSON.stringify(e));
  }
};

MVCApp._setController = function (name,model) {
  MVCApp._controllers[name]=model;
  return MVCApp._controllers[name];
};

MVCApp._getController = function (name) { return MVCApp._controllers[name];};

/**
 * IMPLEMENT DESTROY AND GET FUNCTIONS FOR ALL FORMS HERE
 */

//Public functions which "destroy" instances
//We could keep the destroy functions all in the individual controllers,
//I chose to centralise them all here so as to have a global overview that
//destroy does the same thing for all forms and controllers.
MVCApp.destroyHome = function () { frmHome.destroy(); MVCApp._setController("Home",null);};

MVCApp.destroyMore = function () { frmMore.destroy(); MVCApp._setController("More",null);};
MVCApp.destroyPDP = function () { frmPDP.destroy(); MVCApp._setController("PDP",null);};
MVCApp.destroyProductCategory = function () { frmProductCategory.destroy(); MVCApp._setController("ProductCategory",null);};
MVCApp.destroyProductCategoryMore = function () { frmProductCategoryMore.destroy(); MVCApp._setController("ProductCategoryMore",null);};
MVCApp.destroyProducts = function () { frmProducts.destroy(); MVCApp._setController("Products",null);};

MVCApp.destroyProductList = function () { frmProductsList.destroy(); MVCApp._setController("ProductsList",null);};
MVCApp.destroyProjects = function () { frmProjects.destroy(); MVCApp._setController("Projects",null);};

MVCApp.destroyProjectsCategory = function () { frmProjectsCategory.destroy(); MVCApp._setController("ProjectsCategory",null);};
MVCApp.destroyProjectsList = function () { frmProjectsList.destroy(); MVCApp._setController("ProjectsList",null);};
MVCApp.destroyPJDP = function () { frmPJDP.destroy(); MVCApp._setController("PJDP",null);};

MVCApp.destroyScan = function () { frmScan.destroy(); MVCApp._setController("Scan",null);};
MVCApp.destroyImageSearch = function () { frmImageSearch.destroy(); MVCApp._setController("ImageSearch",null);};
MVCApp.destroyStoreMap = function () { frmStoreMap.destroy(); MVCApp._setController("StoreMap",null);};
MVCApp.destroyStoreLocator = function () { frmFindStoreMapView.destroy(); MVCApp._setController("MapLocator",null);};
MVCApp.destroyImageSearch = function () { frmImageOverlay.destroy();};
MVCApp.destroyVoiceSearchVideos = function () { frmVoiceSearchVideos.destroy();MVCApp._setController("VoiceSearchVideos",null);};

MVCApp.destroyReviews = function () { frmReviews.destroy(); MVCApp._setController("Reviews",null);};
MVCApp.destroyPinchandZoom = function () { frmPinchandZoom.destroy(); MVCApp._setController("pinchandzoom",null);};
MVCApp.destroyCreateAccount = function () { frmCreateAccount.destroy(); MVCApp._setController("createAccount",null);};
MVCApp.destroyCreateAccounTwo = function () { frmCreateAccountStep2.destroy(); MVCApp._setController("createAccountTwo",null);};
MVCApp.destroySignIn = function () { frmSignIn.destroy(); MVCApp._setController("signIn",null);};
MVCApp.destroyForgotPassword = function () { frmForgotPwd.destroy(); MVCApp._setController("forgotpassword",null);};
MVCApp.destroyBrowser = function () { frmRewardsProgram.destroy(); MVCApp._setController("browser",null);};

MVCApp.destroyMyAccountController = function () { frmMyAccount.destroy(); MVCApp._setController("MyAccount",null);};
MVCApp.destroyRewardsProgramInfoController = function () { frmRewardsProgramInfo.destroy(); MVCApp._setController("RewardsProgramInfo",null);};
MVCApp.destroyJoinRewardsController = function () { frmRewardsJoin.destroy(); MVCApp._setController("JoinRewards",null);}; 
MVCApp.destroySettingsController = function () { frmSettings.destroy(); MVCApp._setController("settings",null);}; 
MVCApp.destroyweeklyAdHomeController = function () { frmWeeklyAdHome.destroy(); MVCApp._setController("WeeklyAdHome",null);}; 
MVCApp.destroyweeklyAdController = function () { frmWeeklyAd.destroy(); MVCApp._setController("WeeklyAd",null);}; 
MVCApp.destroyweeklyAdDetailController = function () { frmWeeklyAdDetail.destroy(); MVCApp._setController("WeeklyAdDetail",null);}; 
MVCApp.destroySplash= function () { frmSplash.destroy(); MVCApp._setController("Splash",null);};
MVCApp.destroyShoppingList= function () { frmShoppingList.destroy(); MVCApp._setController("WishList",null);};
MVCApp.destroyHelp = function (){frmHelp.destroy(); MVCApp._setController("Help",null);};
MVCApp.destroyEvents = function (){frmEvents.destroy(); MVCApp._setController("Events",null);};
MVCApp.destroyEventDetail = function (){frmEventDetail.destroy(); MVCApp._setController("EventDetails",null);};
MVCApp.destroySortAndRefine = function (){frmSortAndRefine.destroy(); MVCApp._setController("SortProduct",null);};
MVCApp.destroyRefineProjects = function (){frmRefineProjects.destroy(); MVCApp._setController("SortProject",null);};
MVCApp.destroyGuide = function (){frmGuide.destroy(); MVCApp._setController("Guide",null);};
MVCApp.destroyRegion = function (){frmRegions.destroy(); MVCApp._setController("RegionsChange",null);};
MVCApp.destroyRewardsProfile = function () { frmRewardsProfileStep1.destroy(); MVCApp._setController("RewardsProfile",null);};
MVCApp.destroyRewardsProfileAdrs = function () { frmRewardsProfileStep2.destroy(); MVCApp._setController("RewardsProfileAdrs",null);};
MVCApp.destroyRewardsProfileChild = function () { frmRewardsProfileAddChild.destroy(); MVCApp._setController("RewardsProfileChild",null);};
MVCApp.destroyRewardsProfileInterests = function () { frmRewardsProfileInterests.destroy(); MVCApp._setController("RewardsProfileInterests",null);};
MVCApp.destroyRewardsProfileSkills = function () { frmRewardsProfileSkills.destroy(); MVCApp._setController("RewardsProfileSkills",null);};
MVCApp.destroyReviewProfile = function () { frmReviewProfile.destroy(); MVCApp._setController("ReviewProfile",null);};
MVCApp.destroyRewardsProfilePreferedStore = function () { frmRewardsProfileStep4.destroy(); MVCApp._setController("RewardsProfilePreferedStore",null);};
MVCApp.destroyChatBox = function () { frmChatBox.destroy(); MVCApp._setController("chatBox",null);};
MVCApp.destroyProductListWeb = function () { frmWebView.destroy(); MVCApp._setController("ProductListWeb",null);};
MVCApp.destroyCart = function () { frmWebView.destroy(); MVCApp._setController("Cart",null);};

//We should only get a reference to the controllers through these functions
//This will guarantee that we work against only one instance at a time

MVCApp.getHomeController = function () { var name="Home"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.HomeController())} else { return MVCApp._getController(name);}};
MVCApp.getMoreController = function () { var name="More"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.MoreController())} else { return MVCApp._getController(name);}};
MVCApp.getPDPController = function () { var name="PDP"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.PDPController())} else { return MVCApp._getController(name);}};

MVCApp.getProductCategoryController = function () { var name="ProductCategory"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProductCategoryController())} else { return MVCApp._getController(name);}};

MVCApp.getProductCategoryMoreController = function () { var name="ProductCategoryMore"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProductCategoryMoreController())} else { return MVCApp._getController(name);}};

MVCApp.getProductsController = function () { var name="Products"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProductsController())} else { return MVCApp._getController(name);}};


MVCApp.getProductsListController = function () { var name="ProductsList"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProductsListController())} else { return MVCApp._getController(name);}};
MVCApp.getProjectsController = function () { var name="Projects"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProjectsController())} else { return MVCApp._getController(name);}};
MVCApp.getProjectsCategoryController = function () { var name="ProjectsCategory"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProjectsCategoryController())} else { return MVCApp._getController(name);}};
MVCApp.getProjectsListController = function () { var name="ProjectsList"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProjectsListController())} else { return MVCApp._getController(name);}};
  MVCApp.getPJDPController = function () { var name="PJDP"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.PJDPController())} else { return MVCApp._getController(name);}};
MVCApp.getProductsListWebController = function () { var name="ProductsListWeb"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ProductsListWebController())} else { return MVCApp._getController(name);}};
 MVCApp.getCouponListController = function () { var name="CouponList"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.CouponListController())} else { return MVCApp._getController(name);}}; 

MVCApp.getImageSearchController = function () { var name="ImageSearch"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ImageSearchController())} else { return MVCApp._getController(name);}};
MVCApp.getScanController = function () { var name="Scan"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ScanController())} else { return MVCApp._getController(name);}};
MVCApp.getStoreMapController = function () { var name="StoreMap"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.StoreMapController())} else { return MVCApp._getController(name);}};
MVCApp.getLocatorMapController = function () { var name="MapLocator"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.StoreLocatorController())} else { return MVCApp._getController(name);}};
MVCApp.getSortProductController=function () { var name="SortProduct"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.SortProductController())} else { return MVCApp._getController(name);}};
MVCApp.getSortProjectController=function () { var name="SortProject"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.SortProjectController())} else { return MVCApp._getController(name);}};




MVCApp.getReviewsController = function () { var name="Reviews"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ReviewsController())} else { return MVCApp._getController(name);}};
MVCApp.getPinchandZoomController = function () { var name="pinchandzoom"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.PinchandZoomController())} else { return MVCApp._getController(name);}};


MVCApp.getCreateAccountController = function () { var name="createAccount"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.CreateAccountController())} else { return MVCApp._getController(name);}};
MVCApp.getCreateAccountTwoController = function () { var name="createAccountTwo"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.CreateAccountTwoController())} else { return MVCApp._getController(name);}};
MVCApp.getSignInController = function () { var name="signIn"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.SignInController())} else { return MVCApp._getController(name);}};
MVCApp.getForgotPasswordController = function () { var name="forgotpassword"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ForgotPasswordController())} else { return MVCApp._getController(name);}};
MVCApp.getResetPasswordController = function () { var name="resetpassword"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ResetPasswordController())} else { return MVCApp._getController(name);}};

MVCApp.getBrowserController = function () { var name="browser"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.BrowserController())} else { return MVCApp._getController(name);}};

MVCApp.getMyAccountController = function () { var name="MyAccount"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.MyAccountController())} else { return MVCApp._getController(name);}};
MVCApp.getRewardsProgramInfoController = function () { var name="RewardsProgramInfo"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProgramInfoController())} else { return MVCApp._getController(name);}};
MVCApp.getJoinRewardsController = function () { var name="JoinRewards"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.JoinRewardsController())} else { return MVCApp._getController(name);}};
MVCApp.getSettingsController = function () { var name="settings"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.SettingsController())} else { return MVCApp._getController(name);}};
MVCApp.getWeeklyAdHomeController = function () { var name="WeeklyAdHome"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.WeeklyAdHomeController())} else { return MVCApp._getController(name);}};
MVCApp.getWeeklyAdController = function () { var name="WeeklyAd"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.WeeklyAdController())} else { return MVCApp._getController(name);}};
MVCApp.getWeeklyAdDetailController = function () { var name="WeeklyAdDetail"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.WeeklyAdDetailController())} else { return MVCApp._getController(name);}};
 


MVCApp.getSplashController = function () { var name="Splash"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.SplashController())} else { return MVCApp._getController(name);}};

MVCApp.getShoppingListController = function () { var name="WishList"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ShoppingListController())} else { return MVCApp._getController(name);}};
MVCApp.getRegionChangeController = function () { var name="RegionsChange"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RegionChangeController())} else { return MVCApp._getController(name);}};


MVCApp.getEventsController = function () { var name="Events"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.EventsController())} else { return MVCApp._getController(name);}};



MVCApp.getEventDetailsController = function () { var name="EventDetails"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.EventDetailsController())} else { return MVCApp._getController(name);}};

MVCApp.getHelpDetailsController = function () { var name="Help"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.HelpController())} else { return MVCApp._getController(name);}};

MVCApp.getCustomerCareController = function () { var name="CustomerCare"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.CustomerCareController())} else { return MVCApp._getController(name);}};

MVCApp.getGuideController = function () { var name="Guide"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.GuideController())} else { return MVCApp._getController(name);}};

MVCApp.getVoiceSearchVideosDetailsController = function () { var name="VoiceSearchVideos"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.VoiceSearchVideosController())} else { return MVCApp._getController(name);}};

MVCApp.getRewardsProfileController = function () { var name="RewardsProfile"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProfileController())} else { return MVCApp._getController(name);}};
MVCApp.getRewardsProfileAdrsController = function () { var name="RewardsProfileAdrs"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProfileAdrsController())} else { return MVCApp._getController(name);}};
MVCApp.getRewardsProfileChildController = function () { var name="RewardsProfileChild"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProfileChildController())} else { return MVCApp._getController(name);}};
MVCApp.getRewardsProfileInterestsController = function () { var name="RewardsProfileInterests"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProfileInterestsController())} else { return MVCApp._getController(name);}};
MVCApp.getRewardsProfileSkillsController = function () { var name="RewardsProfileSkills"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProfileSkillsController())} else { return MVCApp._getController(name);}};
MVCApp.getReviewProfileController = function () { var name="ReviewProfile"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ReviewProfileController())} else { return MVCApp._getController(name);}};
MVCApp.getRewardsProfilePreferedStoreController = function () { var name="RewardsProfilePreferedStore"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.RewardsProfilePreferedStoreController())} else { return MVCApp._getController(name);}};
MVCApp.getChatBoxController = function () { var name="chatBox"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.ChatBoxController())} else { return MVCApp._getController(name);}};
MVCApp.getCartController = function () { var name="Cart"; if (MVCApp._hasController(name)===false) { 
  return MVCApp._setController(name,new MVCApp.CartController())} else { return MVCApp._getController(name);}};
