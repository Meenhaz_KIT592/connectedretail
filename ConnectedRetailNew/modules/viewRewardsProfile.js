/**
* PUBLIC
* This is the view for the Events form.
* All actions that impact the UI are implemented here.
* _variable or _functions indicate elements which are not publicly exposed
*/

var MVCApp = MVCApp || {};
var rewardsProfileData={};
MVCApp.RewardsProfileView = (function(){

  /**
                * PUBLIC
                * Open the Events form
                */
  
  function show(nxtFlag){
    setLocaleData();
    frmRewardsProfileStep1.txtBxHide.setFocus(true);
    frmRewardsProfileStep1.txtBxHide.setEnabled(true);
    frmRewardsProfileStep1.txtFstName.setFocus(false);
     if(nxtFlag == true){
       frmRewardsProfileStep1.btnReset.isVisible = false;
       frmRewardsProfileStep1.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
     }else{
       frmRewardsProfileStep1.btnReset.isVisible = true;
       frmRewardsProfileStep1.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
     }
    frmRewardsProfileStep1.show();     
  } 
  
  
  function setLocaleData()
	{
		frmRewardsProfileStep1.lblFormTitle.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.rewardsprofile");
        frmRewardsProfileStep1.Label0c5601159c0bb4c.text=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.step1");
        frmRewardsProfileStep1.txtFstName.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.firstname");
        frmRewardsProfileStep1.txtLstName.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.lastname");
        frmRewardsProfileStep1.txtEmlAddrss.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.emailAddress");
        frmRewardsProfileStep1.txtPhone.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.phone");
        frmRewardsProfileStep1.btnReset.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.exitAndFinishLater");

	}
    
                /**
                * PUBLIC
                * Here we define and attach all event handlers.
                */
  function onClickBtnResetRewardsProfileStep1(){
    frmRewardsProfileStep1.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.rewardsProfile.exitFinish");
    frmRewardsProfileStep1.lbl1.isVisible = false;
    frmRewardsProfileStep1.flxOverlay.isVisible = true;
  }
  
  function navigateToMoreScreen(){
    frmRewardsProfileStep1.flxOverlay.isVisible = false;
    MVCApp.getMoreController().load();
  }
  
  function dummyOverlay(){
    kony.print("LOG:dummyOverlay-viewRewardsProfile.js-START");
  }
  
  function bindEvents(){
    frmRewardsProfileStep1.btnYes.onClick = navigateToMoreScreen;
    frmRewardsProfileStep1.flxOverlay.onTouchEnd = dummyOverlay;
    frmRewardsProfileStep1.postShow = function(){
      MVCApp.Toolbox.common.setBrightness("frmRewardsProfileStep1");
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Rewards Profile Creation: Step 1: Michaels Mobile App";
      lTealiumTagObj.page_name = "Rewards Profile Creation: Step 1: Michaels Mobile App";
      lTealiumTagObj.page_type = "Rewards";
      lTealiumTagObj.page_category_name = "Rewards";
      lTealiumTagObj.page_category = "Rewards";
      TealiumManager.trackView("Rewards Profile Creation Step 1 Screen", lTealiumTagObj);
    };
    MVCApp.tabbar.bindEvents(frmRewardsProfileStep1);
    MVCApp.tabbar.bindIcons(frmRewardsProfileStep1,"frmMore");
    frmRewardsProfileStep1.btnHeaderLeft.onClick = showPreviousScreen;
    frmRewardsProfileStep1.btnReset.onClick = onClickBtnResetRewardsProfileStep1;//resetValues;
    frmRewardsProfileStep1.btnNext.onClick =navigateToProfile2;
    frmRewardsProfileStep1.preShow=onTextChange;
    frmRewardsProfileStep1.txtFstName.onTextChange =function(){
      var text = frmRewardsProfileStep1.txtFstName.text;
          
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep1.btnClearFirstName.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep1.btnClearFirstName.setVisibility(false);
        //#endif
        frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      }
      
    }; 
    frmRewardsProfileStep1.txtLstName.onTextChange = function(){
      var text = frmRewardsProfileStep1.txtLstName.text;
       
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep1.btnClearLastName.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep1.btnClearLastName.setVisibility(false);
        //#endif
        frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      }
      
    }; 
    frmRewardsProfileStep1.txtEmlAddrss.onTextChange = function(){
      var text = frmRewardsProfileStep1.txtEmlAddrss.text;
           
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep1.btnClearEmail.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep1.btnClearEmail.setVisibility(false);
        //#endif
        frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      }
           
    }; 
    frmRewardsProfileStep1.txtPhone.onTextChange = function(){  
      var text = frmRewardsProfileStep1.txtPhone.text;
           
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep1.btnClearPhone.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep1.btnClearPhone.setVisibility(false);
        //#endif
        frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      }
     
    };
    frmRewardsProfileStep1.txtDateOfBirth.onTextChange = function(){  
      var text = frmRewardsProfileStep1.txtDateOfBirth.text;
            
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep1.btnClearDateOfBirth.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep1.btnClearDateOfBirth.setVisibility(false);
        //#endif
        frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      }
      
    };
    
    //#ifdef android
    frmRewardsProfileStep1.btnClearFirstName.onClick = function(){
      frmRewardsProfileStep1.txtFstName.text ="";
       frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      frmRewardsProfileStep1.btnClearFirstName.setVisibility(false);       
    };
    frmRewardsProfileStep1.btnClearLastName.onClick = function(){
      frmRewardsProfileStep1.txtLstName.text ="";
       frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      frmRewardsProfileStep1.btnClearLastName.setVisibility(false);       
    };
    frmRewardsProfileStep1.btnClearEmail.onClick = function(){
      frmRewardsProfileStep1.txtEmlAddrss.text ="";
       frmRewardsProfileStep1.btnNext.skin="sknBtnDisable"
        frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";;
      frmRewardsProfileStep1.btnClearEmail.setVisibility(false);       
    };
    frmRewardsProfileStep1.btnClearPhone.onClick = function(){
      frmRewardsProfileStep1.txtPhone.text ="";
       frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      frmRewardsProfileStep1.btnClearPhone.setVisibility(false); 
    };  
    frmRewardsProfileStep1.btnClearDateOfBirth.onClick = function(){
      frmRewardsProfileStep1.txtDateOfBirth.text ="";
       frmRewardsProfileStep1.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
       frmRewardsProfileStep1.btnClearDateOfBirth.setVisibility(false); 
    };  
    //#endif
       
  }
  
  
  function updateScreen(results){    
      var data = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
      var datFalg = data.getData();
      if(datFalg)
        MVCApp.getReviewProfileController().sumary();
	else{
    data.getStep1Data();    
    MVCApp.Toolbox.common.dismissLoadingIndicator();
    Childflg = false;
    interestsflag = false;
    skillsflag = false;
    addressflag = false;
    show();
    }
    
  }
  
  function showPreviousScreen(){    
    var formId = kony.application.getPreviousForm().id;
    if(formId=="frmReviewProfile")
      frmReviewProfile.show();
    else if(formId=="frmCreateAccount")
      frmCreateAccount.show();
    else
        MVCApp.getMoreController().loadMore();
  }
  function onTextChange(){
    var text1 = frmRewardsProfileStep1.txtFstName.text;
    var text2 = frmRewardsProfileStep1.txtLstName.text;
    var text3 = frmRewardsProfileStep1.txtEmlAddrss.text;
    var text4 = frmRewardsProfileStep1.txtPhone.text;
    var text5 = frmRewardsProfileStep1.txtDateOfBirth.text ||"";
     if((text1.length > 0) && (text2.length > 0) && (text3.length > 0) && (text4.length > 0) && (text5.length > 0)){
           frmRewardsProfileStep1.btnNext.skin = "sknBtnRed";
        frmRewardsProfileStep1.btnNext.focusSkin="sknBtnRed";
      }else{
        frmRewardsProfileStep1.btnNext.skin = "sknBtnDisable";
         frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
      }
        
  }
  function resetValues(){
    frmRewardsProfileStep1.txtFstName.text = "";
    frmRewardsProfileStep1.txtLstName.text = "";
    frmRewardsProfileStep1.txtEmlAddrss.text = "";
    frmRewardsProfileStep1.txtPhone.text = "";
    frmRewardsProfileStep1.txtDateOfBirth.text ="";
     frmRewardsProfileStep1.btnNext.skin = "sknBtnDisable";  
     frmRewardsProfileStep1.btnNext.focusSkin="sknBtnDisable";
    
    //#ifdef android
    frmRewardsProfileStep1.btnClearFirstName.setVisibility(false);
    frmRewardsProfileStep1.btnClearLastName.setVisibility(false);
    frmRewardsProfileStep1.btnClearEmail.setVisibility(false);
    frmRewardsProfileStep1.btnClearPhone.setVisibility(false);
    frmRewardsProfileStep1.btnClearDateOfBirth.setVisibility(false);
    //#endif
  }
  function updateData(nxtFlag){
        
    if(rewardsProfileData.firstName === undefined || rewardsProfileData.firstName===""){
    var data = MVCApp.getRewardsProfileController().getRewardProfileData();
    data.getStep1Data();      
    }
    else{
    frmRewardsProfileStep1.txtFstName.text = rewardsProfileData.firstName;
    frmRewardsProfileStep1.txtLstName.text = rewardsProfileData.lastName;
    frmRewardsProfileStep1.txtEmlAddrss.text = rewardsProfileData.email;
    frmRewardsProfileStep1.txtPhone.text = rewardsProfileData.phoneNo;
    frmRewardsProfileStep1.txtDateOfBirth.text =rewardsProfileData.birthday;    
    MVCApp.Toolbox.common.dismissLoadingIndicator();
    }  
    show(nxtFlag);    
  }
  function navigateToProfile2(){
    rewardsProfileData.firstName =  frmRewardsProfileStep1.txtFstName.text;
    rewardsProfileData.lastName =  frmRewardsProfileStep1.txtLstName.text;
    rewardsProfileData.email =  frmRewardsProfileStep1.txtEmlAddrss.text;
    rewardsProfileData.phoneNo =  frmRewardsProfileStep1.txtPhone.text;
    rewardsProfileData.birthday =  frmRewardsProfileStep1.txtDateOfBirth.text;
    if(frmRewardsProfileStep1.btnNext.skin=="sknBtnRed"){
      if( validateFields()){
        var formId = kony.application.getPreviousForm().id;
        if(formId == "frmReviewProfile"){
          updateFlag = true;
          MVCApp.getReviewProfileController().load();
        }
        else if(addressflag === true)
          MVCApp.getRewardsProfileAdrsController().updateData();
        else  
          MVCApp.getRewardsProfileAdrsController().load();
      }
    }
  }
  
  

  
function validateFields(){
    var firstName = frmRewardsProfileStep1.txtFstName.text;
    var lastName = frmRewardsProfileStep1.txtLstName.text;
    var email = frmRewardsProfileStep1.txtEmlAddrss.text;
    var phone = frmRewardsProfileStep1.txtPhone.text;
    var dob = frmRewardsProfileStep1.txtDateOfBirth.text;
    var msgFlag = false;
    var errFlag = false;
    var errMsg = [];
    var numberRegEx=/^[0-9-]+$/;
    var nameRegEx = /^[a-zA-Z-_ ]+$/;
    var EmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   var dobRegEx=/^[0-9-/]+$/;
   if(firstName ===null || firstName.trim() ===""){
     
      errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstName"));
      errFlag = true;
     
      }else{
      if(!nameRegEx.test(firstName.trim())){
        
//        errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstandLastNameAlert"));
//       errFlag = true;
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstandLastNameAlert"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
         return false; 
        
      }
  }
    if(lastName === null ||lastName.trim() ===""){
    
      errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.lastName"));
      errFlag = true;
     
     
    }else{
      if(!nameRegEx.test(lastName.trim())){
//         errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstandLastNameAlert"));
//       errFlag = true;
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstandLastNameAlert"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
         return false; 
     
      }
    }
    if(email===null || email.trim() === ""){
      
     errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAddress"));
      errFlag = true;      
    }else{
      if(!EmailRegEx.test(email.trim())){
        
      errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAlert"));
      errFlag = true;
        msgFlag = true;
      }
  } 
    if(phone === null || phone === ""){
      
      errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.phoneNumberAlert"));
      errFlag = true;
      
    }else{
      if(!numberRegEx.test(phone.trim())){
        
        alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContainsNumbers"));
      	return false;
        }else{ 
          if(phone.length < 10){
          
        alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContains"));
      	return false;
  	    }
       }
    //  alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContains"));
   //   return false;
    
    }
  if(dob === null || dob === "")
    {
      errMsg.push("Birthday");
      errFlag = true;
    }
  else if(! dobRegEx.test(dob) || dob.indexOf("/") === -1)
    {
      errMsg.push("Birthday should be MM/DD format");
      errFlag = true;
    }
  else 
    {
      var dateArray=dob.split("/");
      if(dateArray.length !==2 )
        {
          errMsg.push("Birthday should be MM/DD format");
          errFlag = true;
        }
      else
        {
             var month =dateArray[0] || "";
                     var dd =dateArray[1]|| "";
          if(month !== "" && dd !== "") 
            {
                month =parseInt(dateArray[0],10);
                      dd = parseInt(dateArray[1],10);
                      if(month > 12 || month === 0)
                {
                   //errMsg.push("month should be between 1 to 12");
                  errMsg.push("Enter valid month");
                   errFlag = true;
                }
              else if(month ==2 && dd >29)
                {
                   //errMsg.push("date should be between 1 to 29");
                  errMsg.push("Enter valid date");
                   errFlag = true;
                }
               else if((month == 8 || month ==7 ||month ==1 || month ==12 ||month ==5 || month ==10 ||month ==3) && (dd >31 ||dd===0) )
                 {
                   // errMsg.push("date should be between 1 to 31");
                   errMsg.push("Enter valid date");
                   errFlag = true;
                 }
               else if((month ==4 || month ==6 ||month ==9 || month ==11) && (dd >30 ||dd===0) )
                 {
                    //errMsg.push("date should be between 1 to 30");
                    errMsg.push("Enter valid date");
                   errFlag = true;
                 }
            }
           else
             {
               errMsg.push("Birthday should be MM/DD format");
                                               errFlag = true;
             }


        }
      
    }
   
  
   if(errFlag === true){
      var result = "";
      for(var i=0;i<errMsg.length;i++){
        if(i==0){
          result = result+errMsg[i];
        }else{
          if(i>0 && (i == (errMsg.length -1))){
            result = result+" "+MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.and")+" "+errMsg[i];
          }else{
            result = result+", "+errMsg[i];
          } 
        }
      }
     if(msgFlag == false){
   MVCApp.Toolbox.common.customAlertNoTitle(result+" "+
           MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.toContinue"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
     }else{
       msgFlag = false;
       MVCApp.Toolbox.common.customAlertNoTitle(result,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
     }
      return false;
    }else{
      return true;
    }
  }
  
  //Here we expose the public variables and functions
  return{
                               show: show,
                               bindEvents: bindEvents,
        updateScreen:updateScreen,
        updateData:updateData
    };
  
});
