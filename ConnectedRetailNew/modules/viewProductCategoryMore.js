/**
 * PUBLIC
 * This is the view for the Home form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var taxanomyurl = "";
var taxanomyForm = {};
var MVCApp = MVCApp || {};

MVCApp.ProductCategoryMoreView = (function(){
var marketContentURL = ""; 
var productId;  
  
  /**
	 * PUBLIC
	 * Open the ProductCategoryMore form
	 */
  
  function show(selectedSubCategory){
  frmProductCategoryMore.segCateogories.data=[];
  frmProductCategoryMore.btnCategoryName.text = selectedSubCategory.name;
  productId =  selectedSubCategory.id; 
  kony.print("Name  "+selectedSubCategory.fullName+"  id  "+ productId);  
  //frmProductCategoryMore.imgCategoryImg.src = selectedSubCategory.headerImage;
  frmProductCategoryMore.lbltextNoResultsFound.setVisibility(false);
  frmProductCategoryMore.flxHeaderWrap.textSearch.text="";
  frmProductCategoryMore.flxSearchResultsContainer.setVisibility(false);
    //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmProductCategoryMore,2);
   frmProductCategoryMore.flxHeaderWrap.btnClearSearch.setVisibility(false);
   //#endif
 } 
  
  
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   function getSelectedCategoryLevel3(){
     var selectedItemRows=frmProductCategoryMore.segCateogories.selectedRowItems;
     if(selectedItemRows!==undefined && selectedItemRows!==null && selectedItemRows.length >0){
       var selectedItem=selectedItemRows[0];
       //kony.print(" Selected item "+ JSON.stringify(selectedItem));
       selectedItem.headerImage = frmProductCategoryMore.imgCategoryImg.src;
       gblIsFromImageSearch = false;
       imageSearchProjectBasedFlag = false;
       MVCApp.getProductsListController().setEntryType("taxonomy");
      var previousItem =  MVCApp.getProductCategoryMoreController().getSelectedSubCategory(); 
      var url = previousItem.fullName+"/"+selectedItem.fullName+"/"+ selectedItem.id;
      url = url.replace(/\s/g, "-").replace(/&/g, "and").toLowerCase();
       kony.print(" String url" +url );
       kony.store.setItem("searchedProdId","");
       fromForm = "frmProductCategoryMore";
       taxanomyForm = kony.application.getCurrentForm();
       var c_CategoryUrl = selectedItem.c_CategoryUrl || "";
       kony.print("c_CategoryUrl::"+c_CategoryUrl);
       if(c_CategoryUrl){
         pdpUrl = c_CategoryUrl;
         taxanomyurl = c_CategoryUrl;
         
       }
       else{
        pdpUrl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+selectedItem.id;
        taxanomyurl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+selectedItem.id;

       }
       if(MVCApp.Toolbox.common.getRegion()=="US"){
            //#ifdef iphone
          //frmWebView.brwrView.isVisible=false;
      //#endif

         if(c_CategoryUrl){
           MVCApp.getProductsListWebController().loadWebView(c_CategoryUrl);//Web view for US
         }else{
           MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+selectedItem.id);//Web view for US
         }
       } else {
         MVCApp.getProductsListController().load(selectedItem,false);// implies we are not enetring from Search
       }   
       
     }else{
       MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, null,"", "");
     }
   }
  
  function showPreviousScreen(){
    var storeID=MVCApp.getProductCategoryController().getStoreId();
    var lIsStoreModeAlreadyEnabled = MVCApp.getProductCategoryController().getStoreModeStatus();
    var lIsStoreModeAlreadyEnabledNew = false;
     var storeIDNEW="";
     gblProductsBackValue = true;
    
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeIDNEW = gblInStoreModeDetails.storeID;
      lIsStoreModeAlreadyEnabledNew = gblInStoreModeDetails.isInStoreModeEnabled;
    }else{
      if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
        var myLocationString=kony.store.getItem("myLocationDetails") || "";
        var locationDetails=JSON.parse(myLocationString);
        storeIDNEW=locationDetails.clientkey;
      }
    }
    var ifEntryFromCartOrWebView=MVCApp.getProductCategoryController().getFormEntryFromCartOrWebView();
    if(ifEntryFromCartOrWebView){
      if(entryBrowserFormName==="frmCartView"){
        MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
      }else if(entryBrowserFormName==="frmWebView"){
        var url= MVCApp.Toolbox.common.findWebViewUrlToGoBack();
       MVCApp.getProductsListWebController().loadWebView(url);
      }else{
        MVCApp.getHomeController().load();
      }
    }
    else{
      if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
        var backParams=MVCApp.getProductCategoryController().getSelectedSubCategory();
        MVCApp.getProductCategoryController().load(backParams);
      }else{
        if(prevFormName == "frmHome" || frombrowserdeeplink){
          MVCApp.getHomeController().load();
        }else if(prevFormName == "frmProductCategory"){
          MVCApp.getProductCategoryController().displayPage();
        }
      }
    }
    
  }
  
  function bindEvents(){
  frmProductCategoryMore.segCateogories.onRowClick = getSelectedCategoryLevel3; 
  frmProductCategoryMore.btnCategoryName.onClick = showPreviousScreen;  
  frmProductCategoryMore.lblCategoryBack.onTouchEnd = showPreviousScreen;
  MVCApp.tabbar.bindEvents(frmProductCategoryMore);
  MVCApp.tabbar.bindIcons(frmProductCategoryMore,"frmProducts");
  MVCApp.searchBar.bindEvents(frmProductCategoryMore,"frmProducts");
   
    frmProductCategoryMore.flxTransLayout.onClick = function(){
      kony.print("do nothing");
    };
    
  frmProductCategoryMore.segCateogories.onTouchStart= function(eventObj,x,y){
   		checkonTouchStart(eventObj,x,y);
 	};
	frmProductCategoryMore.segCateogories.onTouchEnd=function(eventObj,x,y){
  	 checkonTouchEnd(eventObj,x,y);
	};
	 frmProductCategoryMore.FlexContainer01bb9e029ae9a4a.onClick = function(){
       if(marketContentURL!==""){
         //#ifdef iphone
     		marketContentURL = decodeURI(marketContentURL);
     		marketContentURL = marketContentURL.replace(/%2C/g,",");
    		marketContentURL = encodeURI(marketContentURL);
    	//#endif
        MVCApp.Toolbox.common.openApplicationURL(marketContentURL); 
      }
     };
    frmProductCategoryMore.preShow=function(){
      frmProductCategoryMore.flxSearchOverlay.setVisibility(false);
      frmProductCategoryMore.flxVoiceSearch.setVisibility(false);
    }
    
    frmProductCategoryMore.postShow=function(){
          var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
        frmProductCategoryMore.flxHeaderWrap.lblItemsCount.text=cartValue;
	  MVCApp.Toolbox.common.setBrightness("frmProductCategoryMore");
      MVCApp.Toolbox.common.destroyStoreLoc();
      savePreviousScreenForm();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Category:"+frmProductCategoryMore.btnCategoryName.text;
      lTealiumTagObj.page_name = "Category:"+frmProductCategoryMore.btnCategoryName.text;
      lTealiumTagObj.page_type = "shop category";
      lTealiumTagObj.page_category_name = frmProductCategory.btnCategoryName.text+":"+frmProductCategoryMore.btnCategoryName.text;
      lTealiumTagObj.page_category = frmProductCategory.btnCategoryName.text+":"+frmProductCategoryMore.btnCategoryName.text;
      lTealiumTagObj.page_subcategory_name = frmProductCategoryMore.btnCategoryName.text;
      TealiumManager.trackView("Shop Tier 2 Category Page", lTealiumTagObj);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.categoryid=MVCApp.getProductCategoryMoreController().getCategoryIdForTracking();
      if(!formFromDeeplink)
      	MVCApp.Customer360.sendInteractionEvent("ProductsCategories3", cust360Obj);
      else
        formFromDeeplink=false;
    };
  }
  
  var prevFormName = "";
function savePreviousScreenForm(){
   var prevForm = kony.application.getPreviousForm();
	if(prevForm!=null && prevForm != undefined){
	prevForm = prevForm.id;
	}
  if(prevForm =="frmHome"){
      prevFormName = prevForm;
    }else if(prevForm =="frmProductCategory"){
      prevFormName = prevForm;
    }
}
  
  function updateScreen(results){
   var subCatList = results.getProductsListSubCategoryData();
   productCategories = results.getProductsListData();
    if(subCatList.length > 0){
      frmProductCategoryMore.segCateogories.widgetDataMap={
        lblCategoryName:"name",
        lblCategoryCount:"carrotFlag",
        lblAisle:"aisle",
        lblNumber:"number",
        btnAisle:"btnAisle"
      };
      frmProductCategoryMore.segCateogories.data = subCatList; 
    var marketingResponseData = results.getMarketingMessage();
    var imageURL = "";
    frmProductCategoryMore.FlexContainer01bb9e029ae9a4a.isVisible = true;  
    if(marketingResponseData.length > 0){
     imageURL =  marketingResponseData[0].c_contentImage || "";
     marketContentURL = marketingResponseData[0].c_contentUrl || "";
    }
    if(imageURL){
      frmProductCategoryMore.Image01c42148de6a645.src = imageURL;
    }else{
      frmProductCategoryMore.FlexContainer01bb9e029ae9a4a.isVisible = false;
    }  
   	MVCApp.Toolbox.common.dismissLoadingIndicator();
    frmProductCategoryMore.flxTransLayout.setVisibility(false);
    frmProductCategoryMore.show();       
    }else{
      frmProductCategoryMore.lbltextNoResultsFound.setVisibility(true);
      var selectedItem =  MVCApp.getProductCategoryMoreController().getSelectedSubCategory();
      var selectedCat = MVCApp.getProductCategoryController().getSelectedSubCategory();
      MVCApp.getProductsListController().setEntryType("taxonomy");

      if(!selectedCat.fullName){
        selectedCat.fullName=selectedCat.name;
      }
      var url = selectedCat.fullName+"/"+selectedItem.fullName;
      url = url.replace(/\s/g, "-").replace(/&/, "and").toLowerCase()+"/"+productId;
      kony.print(" String url" +url );
      kony.store.setItem("searchedProdId","");
     fromForm = "frmProductCategory";
     taxanomyForm = kony.application.getCurrentForm();
     var c_CategoryUrl = selectedItem.c_CategoryUrl || "";
     kony.print("c_CategoryUrl::"+c_CategoryUrl);
     if(c_CategoryUrl){
       pdpUrl = c_CategoryUrl;
       taxanomyurl = c_CategoryUrl;
     }else{
       pdpUrl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+productId; 
       taxanomyurl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+productId;

     }
     if(MVCApp.Toolbox.common.getRegion()=="US"){
       	   //#ifdef iphone
          //frmWebView.brwrView.isVisible=false;
      //#endif
            if(c_CategoryUrl){
              MVCApp.getProductsListWebController().loadWebView(c_CategoryUrl);
            }else{
              MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+productId);//Web view for US
            }
       } else {
         MVCApp.getProductsListController().load(selectedItem,false,"","",false,false,true);// implies we are not enetring from Search
       }
    }
    
  }
  
  
  //Here we expose the public variables and functions
  function displaySamePage(){
    frmProductCategoryMore.flxSearchOverlay.setVisibility(false);
    frmProductCategoryMore.flxVoiceSearch.setVisibility(false);
    frmProductCategoryMore.show();
  }
  
  //Code for Footer Animation
  var checkX;
  var checkY;
  
  function checkonTouchStart(eventObj, x, y){  
    checkX=x;
    checkY=y; 
  }
  
  function checkonTouchEnd(eventObj,x,y){  
    var tempX;
    var tempY;
    tempX=checkX;
    tempY=checkY;
    checkX=x;
    checkY=y;
     var checkScroll;
     checkScroll=tempY-checkY;
    if (checkScroll>1){
        MVCApp.Toolbox.common.hideFooter();
        frmProductCategoryMore.flexSeg.height = "72%";
      }
    else{
          MVCApp.Toolbox.common.dispFooter();
        frmProductCategoryMore.flexSeg.height = "72%";
    }
    //#ifdef android
    frmProductCategoryMore.flxMainContainer.forceLayout();
    //#endif
  }
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
        displaySamePage:displaySamePage
    };
  
});



