/**
 * PUBLIC
 * This is the view for the CreateAccount2 form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.CreateAccountTwoView = (function(){
var prevFormName="";  

  /**
	 * PUBLIC
	 * Open the More form
	 */
  
  function show(){
  clearFields();
  var screenWidth = kony.os.deviceInfo().screenWidth;
  if(screenWidth == 414 || screenWidth == 375){
     frmCreateAccountStep2.rtPrivacyPolicy.padding=[0,0.5,0,0];
   }
  frmCreateAccountStep2.show();
 } 
  
  function clearFields(){
    frmCreateAccountStep2.txtEmail.text ="";
    frmCreateAccountStep2.txtConfrimEmail.text ="";
    frmCreateAccountStep2.imgCheck.src ="checkbox_off.png";
    frmCreateAccountStep2.flxOverlay.setVisibility(false);
    //#ifdef android
   frmCreateAccountStep2.btnClearEmail.setVisibility(false);
   frmCreateAccountStep2.btnClearConfirmEmail.setVisibility(false);
  //#endif 
  }
  
  function savePreviousScreenForm(){
    var prevForm = kony.application.getPreviousForm().id;
    if(prevForm =="frmCreateAccountStep2"){
      prevFormName = prevForm;
    }else if(prevForm =="frmSignIn"){
      prevFormName = prevForm;
    }
  }
   
 function showPreviousScreen(){
   if(prevFormName == "frmCreateAccountStep2"){
     frmCreateAccountStep2.show(); 
   }else if(prevFormName == "frmSignIn"){
     frmSignIn.show(); 
     MVCApp.tabbar.bindEvents(frmSignIn);
	 MVCApp.tabbar.bindIcons(frmSignIn,"frmMore");
   }
 }
  
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function bindEvents(){
  frmCreateAccountStep2.postShow = function(){
       MVCApp.Toolbox.common.destroyStoreLoc();
	}; 
  frmCreateAccountStep2.btnHeaderLeft.onClick = function(){
    frmCreateAccount.show();
    MVCApp.tabbar.bindEvents(frmCreateAccount);
	MVCApp.tabbar.bindIcons(frmCreateAccount,"frmMore");
  };
    frmCreateAccountStep2.flxOverlay.onTouchEnd = function(){};
    frmCreateAccountStep2.onDeviceBack = function(){};
    frmCreateAccountStep2.btnPrivacyPolicy.onClick = checkPrivacyPolicy;
    frmCreateAccountStep2.btnCreateAccount.onClick  = onClickCreateAccount;
    frmCreateAccountStep2.rtPrivacyPolicy.onClick = showPrivacyPolicy;
    frmCreateAccountStep2.btnOk.onClick = function(){
      createAccountResObj = kony.store.getItem("userObj");
     MVCApp.getMyAccountController().load(createAccountResObj); 
    };
    frmCreateAccountStep2.btnYes.onClick = function(){
      createAccountResObj = kony.store.getItem("userObj");
     MVCApp.getJoinRewardsController().load(createAccountResObj); 
    };
    frmCreateAccountStep2.btnRewardProgramInfo.onClick =function(){
      MVCApp.getRewardsProgramInfoController().load(false);
    };
    frmCreateAccountStep2.btnNotRightNow.onClick = function(){
      if(createAccountObj.prevFormName == "frmGuide"){
      	getProductsCategory();
      }else{
      	MVCApp.getMoreController().load();
      }
    };
    //#ifdef android
 	frmCreateAccountStep2.txtEmail.onTextChange = function(){
    var text = frmCreateAccountStep2.txtEmail.text;
    if(text.length > 0){
      frmCreateAccountStep2.btnClearEmail.setVisibility(true);
    }else{
      frmCreateAccountStep2.btnClearEmail.setVisibility(false);
    }
    };
    frmCreateAccountStep2.txtConfrimEmail.onTextChange = function(){
  var text = frmCreateAccountStep2.txtConfrimEmail.text;
  if(text.length > 0){
    frmCreateAccountStep2.btnClearConfirmEmail.setVisibility(true);
  }else{
    frmCreateAccountStep2.btnClearConfirmEmail.setVisibility(false);
  }
  };
    frmCreateAccountStep2.btnClearConfirmEmail.onClick = function(){
      frmCreateAccountStep2.btnClearConfirmEmail.setVisibility(false);
      frmCreateAccountStep2.txtConfrimEmail.text ="";
    };
    frmCreateAccountStep2.btnClearEmail.onClick = function(){
      frmCreateAccountStep2.btnClearEmail.setVisibility(false);
      frmCreateAccountStep2.txtEmail.text="";
    };
   //#endif
  }
  
  function showPrivacyPolicy(){
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
  }
  
  function checkPrivacyPolicy(){
    if(frmCreateAccountStep2.imgCheck.src =="checkbox_off.png"){
      frmCreateAccountStep2.imgCheck.src ="checkbox_on.png";
    }else{
      frmCreateAccountStep2.imgCheck.src ="checkbox_off.png";
    }
  }
  
  function onClickCreateAccount(){
    createAccountObj.email = frmCreateAccountStep2.txtEmail.text;  
    createAccountObj.user = frmCreateAccountStep2.txtEmail.text;
    if(validateFields()){
      MVCApp.getCreateAccountTwoController().createAccount(createAccountObj);
    }     
  }
  
  function validateFields(){
    var email = frmCreateAccountStep2.txtEmail.text;
    var confirmEmail = frmCreateAccountStep2.txtConfrimEmail.text;
    var regularEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var errorFlag = false;
    var errorMsg = "";
    if(email===null || email.trim() === ""){
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.enterEmail"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
      return false;
    }else{
      if(!regularEx.test(email.trim())){
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
      return false;
      }
    }
    if(confirmEmail===null || confirmEmail.trim() === ""){
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailMismatch"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
      return false;
    }else{
      if(email.trim() != confirmEmail.trim()){
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailMismatch"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
      return false;
      }
    }
    return true;
  }
  
  function updateScreen(accountObj){
    createAccountObj = accountObj;
    show();
  }
  
  function showResponse(response){
    if(response.hasOwnProperty("c_loyaltyMemberID")){
      var isNotFromCanadaPDPOrPPJDPPage = true;
      var encryptresponse = response;
      kony.store.setItem("userObj", encryptresponse);
      
     //kony.store.setItem("userObj", response);
     var loyaltyMemberId = response.c_loyaltyMemberID || "";
      
     var customer_no = response.customer_no || "";

      if(customer_no !=""){
          kony.setUserID(customer_no);
      }
     var c_loyaltyEmail = response.c_loyaltyEmail || "";
     var c_loyaltyPhoneNo = response.c_loyaltyPhoneNo || "";
      gblPassword = createAccountObj.password;
      
      var encryptedEmail = MVCApp.Toolbox.common.MVCApp_encryptPin(encryptresponse.email);
      encryptedEmail = kony.convertToBase64(encryptedEmail);
      kony.store.setItem("username",encryptedEmail);
      
      var encryptedPass = MVCApp.Toolbox.common.MVCApp_encryptPin(createAccountObj.password);
      encryptedPass = kony.convertToBase64(encryptedPass);
      
      kony.store.setItem("userCredential",encryptedPass);
      
      
      gblDecryptedPassword = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
      gblDecryptedUser = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
      
      
      if(MVCApp.Toolbox.common.getRegion()=="US"){
        if(loyaltyMemberId ==="" && c_loyaltyEmail ==="" &&  c_loyaltyPhoneNo ===""){
      visibilityButtons();
    }else{
      disabilityButtons();
    } 
    //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.accountSetUp"));
    frmCreateAccountStep2.flxOverlay.setVisibility(true); 
      }else{
       if(isFromShoppingList){
         isFromShoppingList = false;
         if(frmObject.id === "frmPDP" || frmObject.id === "frmPJDP"){
           isNotFromCanadaPDPOrPPJDPPage = false;
           MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.accountSetUp"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"),constants.ALERT_TYPE_INFO,addProdOrProjToShoppingList,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),""); 
         }else {
           MVCApp.getMoreController().load();
           MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.accountSetUp"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"));
         }
       } 
       else{
         MVCApp.getMoreController().load();
         MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.accountSetUp"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"));
       }
       //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.accountSetUp"));
      }
      
      if(isNotFromCanadaPDPOrPPJDPPage){
        MVCApp.getShoppingListController().createProductList();  
      }
    }else{
      var status = response.Status || [];
      if(status.length>0){
        var errDesc = status[0].errDescription || "";
        //alert(errDesc);
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.signUpError"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.tryAgainTitle"));
        //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.signUpError"));
      }
      
    }
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  function addProdOrProjToShoppingList(){
    var prodOrProjID = kony.store.getItem("shoppingListProdOrProjID");
    if(prodOrProjID){
      frmObject.show();
      if(frmObject.id === "frmPDP"){
        MVCApp.getShoppingListController().addToProductList(prodOrProjID,MVCApp.Service.Constants.ShoppingList.pdpType);
      }else{
        MVCApp.getShoppingListController().addProjectListLooping(prodOrProjID);
      }
    }else{
      frmObject.show();
    }
  }
  
  function visibilityButtons(){
   frmCreateAccountStep2.btnYes.setVisibility(true);
   frmCreateAccountStep2.btnRewardProgramInfo.setVisibility(true);
   frmCreateAccountStep2.btnNotRightNow.setVisibility(true);
   frmCreateAccountStep2.flxLine.setVisibility(true);
   frmCreateAccountStep2.flxLine2.setVisibility(true);
   frmCreateAccountStep2.flxLine3.setVisibility(true);
   frmCreateAccountStep2.lbl2.setVisibility(true);
   frmCreateAccountStep2.btnOk.setVisibility(false);
  }
  
  function disabilityButtons(){
   frmCreateAccountStep2.btnYes.setVisibility(false);
   frmCreateAccountStep2.btnRewardProgramInfo.setVisibility(false);
   frmCreateAccountStep2.btnNotRightNow.setVisibility(false);
   frmCreateAccountStep2.flxLine.setVisibility(false);
   frmCreateAccountStep2.flxLine2.setVisibility(false);
   frmCreateAccountStep2.flxLine3.setVisibility(false);
   frmCreateAccountStep2.lbl2.setVisibility(false);
   frmCreateAccountStep2.btnOk.setVisibility(true);
  }
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	showResponse:showResponse
    };
  
});



