//Type your code here
var MVCApp=MVCApp ||{};
MVCApp.Service={
  getCommonInputParamaters:function(){
    var inputParams={};
    inputParams.client_id= MVCApp.Toolbox.Service.getClientId();
    inputParams.locale= MVCApp.Toolbox.Service.getLocale();
    return inputParams;
  },
  getCommonReviewsInputParamaters:function(){
    var inputRvwParams={};
    inputRvwParams.apiversion= MVCApp.Toolbox.Service.getApiversion();	
	inputRvwParams.offset= MVCApp.Toolbox.Service.getOffset();	
    return inputRvwParams;
	},
  getLanguageID:function(){
    var language=kony.store.getItem("languageSelected");
    var languageID="1";
    if(language=="fr_CA"){
      languageID="2";
    }
    return languageID;
  },
  //The Constants for all the services.
  Constants:{
    Common:{
      locale:"default",
      clientId:"0426981f-5f77-4d24-8a46-4bb3077b3030",
      country:"US",
      myLocationSet:false
    },
    Search:{
      refineString:"refine=cgid=Categories",
      count:30,
      start:0,
      expand:"images,prices,variations,availability",
      sort:"best seller",
      showCount : 10
    },
    SearchProjects:{
      refineString:"refine=cgid=Projects",
    },
    Reviews:{
      apiversion:"5.4",
      passkey:"3mha0hmqyh018uuykahljvont",
      sort:"Rating:desc",
      offset:0,
      limit:20,
	},
    ShoppingList:{
      start:0,
      count:101,
      listItemExpand:"product,images,availability",
      pdpType:"product",
      public : false,
      productAlreadyOnList:"ProductAlreadyOnList",
      wishList : "wish_list",
      pjdpType:"project"
      
    },
    Locations:{
      searchRadius:"50"
    },
    WelcomeMessage:{
      maxTime : 24 //hours
    },
    Events:{
      refineStringUS : "fdid=events&refine_1=c_location=US",
      refineStringCanada : "fdid=events&refine_1=c_location=Canada"
    }
  }
};