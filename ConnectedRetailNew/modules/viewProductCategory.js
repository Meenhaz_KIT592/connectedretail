/**
 * PUBLIC
 * This is the view for the ProductCategory form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.ProductCategoryView = (function(){
var marketContentURL = ""; 
  /**
	 * PUBLIC
	 * Open the ProductCategory form
	 */
  
  function show(selectedCategory){
    
    frmProductCategory.segCateogories.data=[];
    frmProductCategory.imgCategoryImg.src="";
    frmProductCategory.btnCategoryName.text = selectedCategory.name;
    //frmProductCategory.imgCategoryImg.src = selectedCategory.image;
    frmProductCategory.lbltextNoResultsFound.setVisibility(false);
    frmProductCategory.flxHeaderWrap.textSearch.text="";
  	frmProductCategory.flxSearchResultsContainer.setVisibility(false);
    //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmProductCategory,3);
   frmProductCategory.flxHeaderWrap.btnClearSearch.setVisibility(false);
   //#endif
    var screenWidth = kony.os.deviceInfo().screenWidth;    
    if(screenWidth == 414){      
      frmProductCategory.flexSeg.height = "78.5%";      
    }
    else if(screenWidth == 375){
      frmProductCategory.flexSeg.height = "76.5%";
    }
    else
      frmProductCategory.flexSeg.height = "75%";
 } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function bindEvents(){
  frmProductCategory.btnCategoryName.onClick = showPreviousScreen;  
  frmProductCategory.lblCategoryBack.onTouchEnd = showPreviousScreen;
  frmProductCategory.segCateogories.onRowClick = getSubCategories;
  MVCApp.tabbar.bindEvents(frmProductCategory);
  MVCApp.tabbar.bindIcons(frmProductCategory,"frmProducts");
  MVCApp.searchBar.bindEvents(frmProductCategory,"frmProducts");
  flxCategoryList.btnAisle.onClick = function(eventObj){
    onClickAisle(eventObj);
  };
    frmProductCategory.flxTransLayout.onClick = function(){
      kony.print("do nothing");
    };
     frmProductCategory.segCateogories.onTouchStart= function(eventObj,x,y){
   		checkonTouchStart(eventObj,x,y);
 	};
	frmProductCategory.segCateogories.onTouchEnd=function(eventObj,x,y){
  	 checkonTouchEnd(eventObj,x,y);
	};
    frmProductCategory.FlexContainer01bb9e029ae9a4a.onClick = function(){
      if(marketContentURL!==""){
         //#ifdef iphone
     		marketContentURL = decodeURI(marketContentURL);
     		marketContentURL = marketContentURL.replace(/%2C/g,",");
    		marketContentURL = encodeURI(marketContentURL);
    	//#endif
        MVCApp.Toolbox.common.openApplicationURL(marketContentURL); 
        if(MVCApp.Toolbox.common.determineIfExternal(marketContentURL)){
          //FacebookAnalytics.reportEvents("", "", marketContentURL, "frmProductCategory");
        }
      }
    };
    frmProductCategory.preShow=function(){
      frmProductCategory.flxSearchOverlay.setVisibility(false);
      frmProductCategory.flxVoiceSearch.setVisibility(false);
    };
    frmProductCategory.postShow=function(){
    var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
    frmProductCategory.flxHeaderWrap.lblItemsCount.text=cartValue;
      MVCApp.Toolbox.common.setBrightness("frmProductCategory");
      MVCApp.Toolbox.common.destroyStoreLoc();
      savePreviousScreenForm();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Category:"+frmProductCategory.btnCategoryName.text;
      lTealiumTagObj.page_name = "Category:"+frmProductCategory.btnCategoryName.text;
      lTealiumTagObj.page_type = "shop category";
      lTealiumTagObj.page_category_name = frmProductCategory.btnCategoryName.text;
      lTealiumTagObj.page_category = frmProductCategory.btnCategoryName.text;
      lTealiumTagObj.page_subcategory_name = frmProductCategory.btnCategoryName.text;
      TealiumManager.trackView("Main Shop Category Page", lTealiumTagObj);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.categoryid=MVCApp.getProductCategoryController().getCategoryIdForTracking();
      if(!formFromDeeplink)
        MVCApp.Customer360.sendInteractionEvent("ProductsCategories2", cust360Obj);
      else
        formFromDeeplink=false;
    };
  }
  
var prevFormName = "";
function savePreviousScreenForm(){
   var prevForm = kony.application.getPreviousForm();
	if(prevForm!=null && prevForm != undefined){
	prevForm = prevForm.id;
	}
  if(prevForm =="frmHome"){
      prevFormName = prevForm;
    }else if(prevForm =="frmProducts"){
      prevFormName = prevForm;
    }
}
  function showPreviousScreen(){
    var ifEntryFromCartOrWebView=MVCApp.getProductCategoryController().getFormEntryFromCartOrWebView();
    if(ifEntryFromCartOrWebView){
      if(entryBrowserFormName==="frmCartView"){
        MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
      }else if(entryBrowserFormName==="frmWebView"){
        var url= MVCApp.Toolbox.common.findWebViewUrlToGoBack();
       MVCApp.getProductsListWebController().loadWebView(url);
      }else{
        MVCApp.getHomeController().load();
      }
    }else if(prevFormName == "frmHome" || frombrowserdeeplink){
      MVCApp.getHomeController().load();
    }else if(prevFormName == "frmProducts"){
      MVCApp.getProductsController().displayPage();
    }
  }
  
  
  
  function onClickAisle(eventObj){
    var params = {};
    var info = eventObj.info || {};
    var aisleNo = info.aisleNum || "";
    if(aisleNo ===""){
     // getSubCategories();
    }else{
    params.aisleNo = eventObj.info.aisleNum;
    params.productName = eventObj.info.name;
    params.calloutFlag = true;
    MVCApp.sendMetricReport(frmProductCategory, [{"storemap":"click"}]);
    MVCApp.getStoreMapController().load(params);
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    var storeId= MVCApp.Toolbox.common.getStoreID();
    cust360Obj.productid=params.productName;
    cust360Obj.store_id=storeId;
    cust360Obj.entry_type="categories";
    MVCApp.Customer360.sendInteractionEvent("ProductLocationInStore", cust360Obj);
    }
   
  }
  
  function updateScreen(results){
     var subCatList = results.getProductsListCategoryData();
    if(subCatList.length > 0){
      frmProductCategory.segCateogories.widgetDataMap={
        lblCategoryName:"name",
        lblCategoryCount:"carrotFlag",
        lblAisle:"aisle",
        lblNumber:"number",
        btnAisle:"btnAisle"
      };
      frmProductCategory.segCateogories.data = subCatList;
    }else{
      frmProductCategory.lbltextNoResultsFound.setVisibility(true);
    }
    var marketingResponseData = results.getMarketingMessage();
    var imageURL = "";
    frmProductCategory.FlexContainer01bb9e029ae9a4a.isVisible = true;
    if(marketingResponseData.length >0){
     imageURL =  marketingResponseData[0].c_contentImage || "";
     marketContentURL = marketingResponseData[0].c_contentUrl || "";
    }
    if(imageURL){
      frmProductCategory.Image01c42148de6a645.src = imageURL;
    }else{
      frmProductCategory.FlexContainer01bb9e029ae9a4a.isVisible = false;
    }
   MVCApp.Toolbox.common.dismissLoadingIndicator();
   frmProductCategory.flxTransLayout.setVisibility(false);
    if(gblProductsBackValue === true){
      gblProductsBackValue = false;
      //#ifdef android
      MVCApp.Toolbox.common.setTransition(frmProductCategory,2);
      //#endif 
    }else{
      //#ifdef android
      MVCApp.Toolbox.common.setTransition(frmProductCategory,3);
      //#endif 
    }
    //#ifdef iphone
    MVCApp.Toolbox.common.setTransition(frmProductCategory,3);
    //#endif
    frmProductCategory.show();
  }
  
  //Here we expose the public variables and functions
  
  function getSubCategories(){
    var selectedIndex = frmProductCategory.segCateogories.selectedRowIndex[1];
    var segData = frmProductCategory.segCateogories.data ||[];
    if(segData!= null && segData!= undefined && segData.length>0){
      var selectedData = segData[selectedIndex] ;
      selectedData.headerImage = frmProductCategory.imgCategoryImg.src;
      gblIsFromImageSearch = false;
      imageSearchProjectBasedFlag = false;
      frombrowserdeeplink=false;
      MVCApp.getProductCategoryMoreController().setFormEntryFromCartOrWebView(false);
      MVCApp.getProductCategoryMoreController().load(selectedData);
    }else{
      MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, null,"", "");
    }
  }
  
  
  function displaySamePage(){
    //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmProductCategory,2);
    //#endif
    //#ifdef iphone
    MVCApp.Toolbox.common.setTransition(frmProductCategory,3);
    //#endif
    frmProductCategory.show();
  }
  
   //Code for Footer Animation
  var checkX;
  var checkY;
  
  function checkonTouchStart(eventObj, x, y){  
    checkX=x;
    checkY=y; 
  }
  
  function checkonTouchEnd(eventObj,x,y){  
    var tempX;
    var tempY;
    tempX=checkX;
    tempY=checkY;
    checkX=x;
    checkY=y;
     var checkScroll;
     checkScroll=tempY-checkY;
    if (checkScroll>1){
        MVCApp.Toolbox.common.hideFooter();
        frmProductCategory.flexSeg.height = "70.5%";
      }
    else{
          MVCApp.Toolbox.common.dispFooter();
          frmProductCategory.flexSeg.height = "70.5%";
    }
    //#ifdef android
  frmProductCategory.flxMainContainer.forceLayout();
    //#endif
  }
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
        displaySamePage:displaySamePage
    };
  
});



