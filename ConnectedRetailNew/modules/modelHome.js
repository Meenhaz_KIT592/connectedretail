/**
 * PUBLIC
 * This is the model for formHome form
 */
var MVCApp = MVCApp || {};
MVCApp.HomeModel = (function(){
 var serviceType = "";
  var operationId = "";
  function loadData(callback,requestParams){
    kony.print("modelHome.js");
    MakeServiceCall( MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchContent,requestParams,
                            function(results)
                            {
              if(results.opstatus===0 || results.opstatus==="0" ){
                var homeContentValue = results.hits || [];
                var showHomeContentData = [];
                for(var i=0;i<homeContentValue.length;i++){
                  var onlineFlag = homeContentValue[i].c_online || "";
                  if(onlineFlag === true || onlineFlag == "true"){
                    showHomeContentData.push(homeContentValue[i]);
                  }
                }
                kony.store.setItem("homeContent", showHomeContentData);
                callback("");
              }   
            },function(error){
              kony.print("Response is "+JSON.stringify(error));
            });
  }
  function loadSearchContentData(requestParams){
    MakeServiceCall( MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchContent,requestParams,
                            function(results)
                            {
              if(results.opstatus===0 || results.opstatus==="0" ){
                var searchContentValue = results.hits || [];
                kony.store.setItem("searchContent", searchContentValue);
              }   
            },function(error){
              kony.print("Response is "+JSON.stringify(error));
            });
  }
  
  
  function loadFeatureProject(requestParams){

    MakeServiceCall( MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchContent,requestParams,
                            function(results)
                            {
      kony.print(" feature Project results :"+JSON.stringify(results));
              if(results.opstatus===0 || results.opstatus==="0" ){
                var searchContentValue = results.hits || [];
                var validFeaturedProjectData = [];
                for(var i=0;i<searchContentValue.length;i++){
                  var onlineFlag = searchContentValue[i].c_online || "";
                  if(onlineFlag === true || onlineFlag == "true"){
                    validFeaturedProjectData.push(searchContentValue[i]);
                  }
                }
                kony.store.setItem("featureProject", validFeaturedProjectData);
              }   
            },function(error){
              kony.print("Response is "+JSON.stringify(error));
            });
}
  
 function updateMyStoreLocation(requestParams){
    MakeServiceCall( MVCApp.serviceType.KonyMFDBService,MVCApp.serviceEndPoints.getBlockedStore,requestParams,
                            function(results)
                            {
      kony.print(" store location information :"+JSON.stringify(results));
      if(results.opstatus===0 || results.opstatus==="0" ){
        var storeNo = "";
        var storeObj= "";
        var storeString=kony.store.getItem("myLocationDetails") ||"";
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeNo = gblInStoreModeDetails.storeID;
        }else{
          if(storeString!==""){
            try {
              storeObj=JSON.parse(storeString);
              storeNo =storeObj.clientkey;
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"modelHome.js on updateMyStoreLocation for storeString:" + JSON.stringify(e)}]);
              if(e){
                TealiumManager.trackEvent("POI Parse Exception in Home Model Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          }
        }
        var storeInfo  = results.MOBILE_BLACKLISTED_STORES || [];
        if(storeInfo.length > 0){
          var storeId = storeInfo[0].storeid || "";
          if(storeNo == storeId){
            storeObj.isblocked = "true";
          }else{
            storeObj.isblocked = "false";
          }
        }else{
            storeObj.isblocked = "false";
          }
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          kony.print("LOG:updateMyStoreLocation-inside if condition");//TODO
        }else{
          kony.store.setItem("myLocationDetails", JSON.stringify(storeObj));
        }
      }   
    },function(error){
     // storeObj.isblocked = "false";
      kony.print("Response is "+JSON.stringify(error));
    });
 } 
  
  
  
  /**
   * @function
   *
   */
  function getCoupons(serviceType, opertation,callback,inputParams){
    MakeServiceCall( serviceType,opertation,inputParams,
                    function(results){
      				kony.print("coupons success");
                      callback(results);
                    },   
                    function(error){
                      kony.print("error---"+JSON.stringify(error));
                      MVCApp.Toolbox.common.dismissLoadingIndicator(); 
                    }
                   );
  }
  
  function getCouponAnalytics(serviceType, opertation,callback,inputParams){
    MakeServiceCall( serviceType,opertation,inputParams,
                    function(results){
                      callback(results);
                    },   
                    function(error){
                      kony.print("error in coupons Analytics---"+JSON.stringify(error));
                      MVCApp.Toolbox.common.dismissLoadingIndicator(); 
                    }
                   );
  }
  
  function getStoreDetails(serviceType, operation,callback,requestParams){
    MakeServiceCall( serviceType, operation,requestParams,
                            function(results){
      						  kony.print("getStoreDetails----"+JSON.stringify(results));
                              callback(results);
                            } ,
                    		function(error){
                              kony.print("BEacon error Response is "+JSON.stringify(error));
      						  //createLocalnotification();
                            });
  }
  

  function androidAddCoupon(requestParams,tag){
    var serviceName="Wallet";
    var operationId="addCouponToAndroidWallet";
    if(tag && tag!=""){
      if(tag.toLowerCase()=="serialized"){
      serviceName="AddandUpdateWalletToKM";
      operationId="couponAndroidKM";
    }
    }
    MakeServiceCall(serviceName,operationId,requestParams,
                            function(results){
      						if(results.opstatus===0 || results.opstatus==="0" || results.opstatus=="1582"){
                            var androidDeeplinkURL = results.jwtToken || "";
                            if(androidDeeplinkURL !== ""){
                            kony.application.openURL(androidDeeplinkURL); 
                              var registeredUser=kony.store.getItem("userObj");
                              if(registeredUser!="" && registeredUser!=null && registeredUser!=undefined){
                                var objForAnalytics={"signedInUser":true,"offerId":requestParams.offerId,"offerType":requestParams.offerType,"platform":"android"};
                                objForAnalytics=JSON.stringify(objForAnalytics);
                              MVCApp.sendMetricReport(frmCoupons,[{"couponAddedToWallet":objForAnalytics}]);
                              //KNYMetricsService.sendEvent("Custom", "frmCoupons", "", null, {"couponAddedToWallet":objForAnalytics});
                              }else{
                                var objForAnalytics={"signedInUser":false,"offerId":requestParams.offerId,"offerType":requestParams.offerType,"platform":"android"};
                                objForAnalytics=JSON.stringify(objForAnalytics);
                              MVCApp.sendMetricReport(frmCoupons,[{"couponAddedToWallet":objForAnalytics}]);
                              }
                                
                            }
                              if(results.opstatus=="1582"){
                                kony.print("the endpoints are not working");
                              }
                              if(results.errorCode){
                                kony.print("error code is "+results.errorCode);
                              }
                              if(results.errorMessage){
                                kony.print("error Message is "+results.errorMessage);
                              }
                            }else{
                              MVCApp.Toolbox.common.dismissLoadingIndicator();
                            }
                            },
                    		function(error){
                              MVCApp.Toolbox.common.dismissLoadingIndicator();
                            });
  }
  function addCouponIphone(requestParams, callback,tag){
    var serviceName="Wallet";
    var operationId="addCouponToiPhoneWallet";
    if(tag && tag!=""){
      if(tag.toLowerCase() =="serialized"){
      serviceName="AddandUpdateWalletToKM";
      operationId="couponIosKM";
    }
    }
    MakeServiceCall(serviceName,operationId,requestParams,
                            function(results){
      						if(results.opstatus===0 || results.opstatus==="0" || results.opstatus=="1582"){
                            var base64String  = results.base64String || "";
                            if(base64String !== ""){
                           	//alert("base64String"+base64String);
                              callback(base64String);
                              if(results.opstatus=="1582"){
                                kony.print("the endpoints are not working");
                              }
                              if(results.errorCode){
                                kony.print("error code is "+results.errorCode);
                              }
                              if(results.errorMessage){
                                kony.print("error Message is "+results.errorMessage);
                              }
                            }
                            }else{
                              MVCApp.Toolbox.common.dismissLoadingIndicator();
                            }
                            },
                    		function(error){
                              MVCApp.Toolbox.common.dismissLoadingIndicator();
                            });
  }
  
  function fetchInStoreModeStoreDetails(pCallback,pRequestParams){
    var lOperationId = MVCApp.serviceType.fetchINStoreDetailsByID;
    var lServiceType = MVCApp.serviceType.LocatorService;
    MakeServiceCall(lServiceType,lOperationId,pRequestParams,
      function(pResults){
        kony.print("LOG:fetchInStoreModeStoreDetails-pResults:"+JSON.stringify(pResults)); 
        if(pResults.opstatus === 0 || pResults.opstatus === "0"){
          MVCApp.Toolbox.common.dismissLoadingIndicator();
          if(gblInStoreModeDetails && pResults){
            if(pResults.collection && pResults.collection.length > 0){
              if(pResults.collection[0] && pResults.collection[0].poi){
                gblInStoreModeDetails.POI = pResults.collection[0].poi;
              }
            }
          }
          pCallback();
        }   
      },function(pError){
        kony.print("LOG:fetchInStoreModeStoreDetails-pError:"+JSON.stringify(pError));
        MVCApp.Toolbox.common.dismissLoadingIndicator();
      	pCallback();
      }
    );
  }
  
  function fetchListOfBlockedStores(pCallback,pRequestParams){
    var lOperationId = MVCApp.serviceEndPoints.getBlockedStore;
    var lServiceType = MVCApp.serviceType.KonyMFDBService;
    MakeServiceCall(lServiceType,lOperationId,pRequestParams,
      function(pResults){
        kony.print("LOG:fetchListOfBlockedStores-pResults:"+JSON.stringify(pResults)); 
        if(pResults.opstatus === 0 || pResults.opstatus === "0"){
          MVCApp.Toolbox.common.dismissLoadingIndicator();
          if(gblInStoreModeDetails && gblInStoreModeDetails.storeID && gblInStoreModeDetails.POI && pResults){
            var lStoreObj = gblInStoreModeDetails.POI;
            var lBlockedStores = pResults.MOBILE_BLACKLISTED_STORES || [];
            if(lBlockedStores.length > 0){
              if(gblInStoreModeDetails.storeID == lBlockedStores[0].storeid){
                lStoreObj.isblocked = "true";
              }else{
                lStoreObj.isblocked = "false";
              }
            }else{
              lStoreObj.isblocked = "false";
            }
            gblInStoreModeDetails.POI = lStoreObj;
          }
          pCallback();
        }   
      },function(pError){
        kony.print("LOG:fetchListOfBlockedStores-pError:"+JSON.stringify(pError));
        MVCApp.Toolbox.common.dismissLoadingIndicator();
      	pCallback();
      }
    );
  }
  
  return{
    loadData:loadData,
    getCoupons : getCoupons,
    getCouponAnalytics:getCouponAnalytics,
    loadSearchContentData:loadSearchContentData,
    loadFeatureProject:loadFeatureProject,
    updateMyStoreLocation:updateMyStoreLocation,
    getStoreDetails : getStoreDetails,
    androidAddCoupon:androidAddCoupon,
    addCouponIphone:addCouponIphone,
    fetchInStoreModeStoreDetails: fetchInStoreModeStoreDetails,
    fetchListOfBlockedStores: fetchListOfBlockedStores
    };
});
