/**
 * PUBLIC
 * This is the view for the ProjectsCategory form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.ProjectsCategoryView = (function(){
  
  /**
	 * PUBLIC
	 * Open the ProjectsCategory form
	 */
  function showProjectsScreen(){
    frmProjectsCategory.flxHeaderWrap.textSearch.text="";
  	frmProjectsCategory.flxSearchResultsContainer.setVisibility(false);
   var previousFrmName = kony.application.getPreviousForm().id;
    var currentFrmName = kony.application.getCurrentForm().id;
   if(previousFrmName == "frmProjectsCategory"||currentFrmName == "frmProjectsList"){
     //#ifdef android
		MVCApp.Toolbox.common.setTransition(frmProjectsCategory,2);
     frmProjectsCategory.flxHeaderWrap.btnClearSearch.setVisibility(false);
   	//#endif
     //#ifdef iphone
     MVCApp.Toolbox.common.setTransition(frmProjectsCategory,2);
     //#endif
   }
   frmProjectsCategory.show();
 }
   
  function show(){
    frmProjectsCategory.flxHeaderWrap.textSearch.text="";
  	frmProjectsCategory.flxSearchResultsContainer.setVisibility(false);
  	//#ifdef android
    MVCApp.Toolbox.common.setTransition(frmProjectsCategory,2);
   frmProjectsCategory.flxHeaderWrap.btnClearSearch.setVisibility(false);
   //#endif
    var screenWidth = kony.os.deviceInfo().screenWidth;       
    if(screenWidth == 414){       
      frmProjectsCategory.flexSeg.height = "80%";      
    }
    else if(screenWidth == 375){
      frmProjectsCategory.flexSeg.height = "78%";
    }
    else
      frmProjectsCategory.flexSeg.height = "76.5%";
   // frmProjectsCategory.show();
  }
  
  function setCategoryName(data){
   frmProjectsCategory.segCateogories.data=[];
   frmProjectsCategory.imgCategoryImg.src = ""; 
   frmProjectsCategory.imgCategoryImg.src=data.image;
   frmProjectsCategory.btnCategoryName.text = data.name;
 } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   
  function bindEvents(){
    frmProjectsCategory.btnCategoryName.onClick = showPreviousScreen; 
    frmProjectsCategory.lblCategoryBack.onTouchEnd = showPreviousScreen;    
    frmProjectsCategory.segCateogories.onRowClick = getListofProjects;
    MVCApp.tabbar.bindEvents(frmProjectsCategory);
    MVCApp.tabbar.bindIcons(frmProjectsCategory,"frmProjects");
	MVCApp.searchBar.bindEvents(frmProjectsCategory,"frmProjects");
    
    frmProjectsCategory.flxTransLayout.onClick = function(){
      kony.print("do nothing");
    };
    
    frmProjectsCategory.segCateogories.onTouchStart= function(eventObj,x,y){
   		checkonTouchStart(eventObj,x,y);
 	};
	frmProjectsCategory.segCateogories.onTouchEnd=function(eventObj,x,y){
  	 checkonTouchEnd(eventObj,x,y);
	};
    frmProjectsCategory.preShow=function(){
      frmProjectsCategory.flxSearchOverlay.setVisibility(false);
      frmProjectsCategory.flxVoiceSearch.setVisibility(false);
    };
    frmProjectsCategory.postShow=function(){
          var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
    frmProjectsCategory.flxHeaderWrap.lblItemsCount.text=cartValue;
      MVCApp.Toolbox.common.setBrightness("frmProjectsCategory");
      MVCApp.Toolbox.common.destroyStoreLoc();
      savePreviousScreenForm();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Category:"+frmProjectsCategory.btnCategoryName.text;
      lTealiumTagObj.page_name = "Category:"+frmProjectsCategory.btnCategoryName.text;
      lTealiumTagObj.page_type = "project category";
      lTealiumTagObj.page_category_name = "Projects:"+frmProjectsCategory.btnCategoryName.text;
      lTealiumTagObj.page_category = "Projects:"+frmProjectsCategory.btnCategoryName.text;
      lTealiumTagObj.page_subcategory_name = frmProjectsCategory.btnCategoryName.text;
      TealiumManager.trackView("Main Project Category Page", lTealiumTagObj);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.categoryid=MVCApp.getProjectsCategoryController().getCategoryIdForTracking();
      if(!formFromDeeplink)
      	MVCApp.Customer360.sendInteractionEvent("ProjectsCategories2", cust360Obj);
      else
        formFromDeeplink=false;
    };
  }
  
  
  function showPreviousScreen(){
    if(prevFormName == "frmHome"){
  		MVCApp.getHomeController().load();
 	 }else if(prevFormName == "frmProjects"){
       MVCApp.getProjectsController().displayProjects(); 
  		}
  }
  
   var prevFormName = "";
function savePreviousScreenForm(){
   var prevForm = kony.application.getPreviousForm();
	if(prevForm!=null && prevForm != undefined){
	prevForm = prevForm.id;
	}
  if(prevForm =="frmHome"){
      prevFormName = prevForm;
    }else if(prevForm =="frmProjects"){
      prevFormName = prevForm;
    }
}
  
  function updateScreen(results){
    var subCatList = results.getSubProjectsListData();
    if(subCatList.length > 0){
        frmProjectsCategory.lbltextNoResultsFound.setVisibility(false);
      	frmProjectsCategory.segCateogories.setVisibility(true);
      	frmProjectsCategory.segCateogories.widgetDataMap={
        lblCategoryName:"name",
        lblCategoryCount:"lblCategoryCount",
        lblAisle:"lblAisle",
        lblNumber:"lblNumber",
        btnAisle:"btnAisle"  
      };
      frmProjectsCategory.segCateogories.data = subCatList;
      
    }else{
      frmProjectsCategory.segCateogories.setVisibility(false);
      frmProjectsCategory.lbltextNoResultsFound.setVisibility(true);     
    }
   MVCApp.Toolbox.common.dismissLoadingIndicator();
    frmProjectsCategory.flxTransLayout.setVisibility(false);
    //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmProjectsCategory,3);
    //#endif
    frmProjectsCategory.show();
  }
  
  function getListofProjects(){
    var selectedIndex = frmProjectsCategory.segCateogories.selectedRowIndex[1];
    var segData = frmProjectsCategory.segCateogories.data;
	var selectedData = segData[selectedIndex] ||{};
    var query = segData[selectedIndex];
    query.bgimage = frmProjectsCategory.imgCategoryImg.src;
    kony.print("selected data:"+JSON.stringify(selectedData));
	frmProjectsList.segProjectList.removeAll();
    frmProjectsList.flxRefineSearchWrap.isVisible = false;
    frmProjectsList.flxCategory.isVisible = false;
    frmProjectsList.flxHeaderWrap.textSearch.text = "";
    MVCApp.getProjectsListController().setQueryStringForSearch("");
   	frmProjectsList.flxSearchResultsContainer.isVisible = false;
    frmProjectsList.flxNoResults.isVisible = false;
    frmProjectsList.flxProdProjTabs.isVisible = false;
    gblIsFromImageSearch = false;
    imageSearchProjectBasedFlag = false;
    formFromDeeplink=true;// to denote there is one empty show- for analytics tracking
    MVCApp.getProjectsListController().setEntryType("taxonomy");
    MVCApp.getProductsListController().setEntryType("taxonomy");
    var url = frmProjectsCategory.btnCategoryName.text+"/"+selectedData.fullName+"/"+selectedData.id;
    url = url.replace(/\s/g, "-").replace(/&/g, "and").toLowerCase();
    fromForm = "frmProjectsList";
    taxanomyForm = kony.application.getCurrentForm();
     var c_CategoryUrl = selectedData.c_CategoryUrl || "";   
     kony.print("c_CategoryUrl::"+c_CategoryUrl);
    if(c_CategoryUrl){
      taxanomyurl = c_CategoryUrl;
    }else{
      taxanomyurl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+selectedData.id;

    }

    if(MVCApp.Toolbox.common.getRegion()=="US"){
        //#ifdef iphone
         //frmWebView.brwrView.isVisible=false;
      //#endif
        if(c_CategoryUrl){
	        MVCApp.getProductsListWebController().loadWebView(c_CategoryUrl);
        }else{
    	    MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"search?cgid="+selectedData.id);
        }
    } else {
      frmProjectsList.show();
      MVCApp.getProjectsListController().load(query,false,false);
    //need to add logic to navigate to search list page
    }  
    
  }
  
   //Code for Footer Animation
  var checkX;
  var checkY;
  
  function checkonTouchStart(eventObj, x, y){  
    checkX=x;
    checkY=y; 
  }
  
  function checkonTouchEnd(eventObj,x,y){  
    var tempX;
    var tempY;
    tempX=checkX;
    tempY=checkY;
    checkX=x;
    checkY=y;
     var checkScroll;
     checkScroll=tempY-checkY;
    if (checkScroll>1){
        MVCApp.Toolbox.common.hideFooter();
        frmProjectsCategory.flexSeg.height= "85%";
    }else{
        MVCApp.Toolbox.common.dispFooter();
        frmProjectsCategory.flexSeg.height = "85%";
    }
    //#ifdef android
    frmProjectsCategory.flxMainContainer.forceLayout();
    //#endif
  }
 
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	setCategoryName:setCategoryName,
    	showProjectsScreen:showProjectsScreen
    };
  
});



