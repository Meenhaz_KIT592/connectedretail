/**
 * PUBLIC
 * This is the view for the PDP form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
var dataPJDPObj={};
var togglePrimaryProducts = true;
var toggleAdditionalMaterials = true;
var toggleProjInstructions = true;
var gProductList= [];
MVCApp.PJDPView = (function(){
var selectedImageIndex = 0;
  /**
	 * PUBLIC
	 * Open the PDP form
	 */
  var lProdIdLen = 0;
  function setProductIdLength(pProdIdLen){
    lProdIdLen = pProdIdLen;
  }
  function getProductIdLength(){
    return lProdIdLen;
  }
  
  function show(){
  frmPJDP.flxHeaderWrap.textSearch.text=MVCApp.getPJDPController().getQueryString();
  frmPJDP.flxSearchResultsContainer.setVisibility(false);
  //#ifdef android
    var text=frmPJDP.flxHeaderWrap.textSearch.text ||"";
    if(text===""){
      frmPJDP.flxHeaderWrap.btnClearSearch.setVisibility(false);
    }else{
      frmPJDP.flxHeaderWrap.btnClearSearch.setVisibility(false);
    }
   MVCApp.Toolbox.common.setTransition(frmPJDP,3);
   //#endif
    try{
    frmPJDP.flxFooterWrap.isVisible = true; 
    }catch(e){
      kony.print(e);
      if(e){
        TealiumManager.trackEvent("Exception While Displaying The PJDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    } 
    frmPJDP.flexSeg.contentOffset = {
			x: 0,
			y: 0
		};
  frmPJDP.show();
 } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function shopOnline(){
       var shopOnlineSkin = frmPJDP.btnBuyOnline2.skin;
      
      if(shopOnlineSkin === "sknBtnDisable")
        {
           alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.projectInStore"));
        }
      else
        {          
          var url = MVCApp.serviceConstants.buyOnlineLink+dataPJDPObj.getProjectId();    

          var report_ProductID="NA";
          var report_Id=dataPJDPObj.getReportId();
          var inStoreModeEnabled=false;
		  var entrySearchType= MVCApp.getPJDPController().getSearchEntryType();
          entrySearchType=MVCApp.Toolbox.common.checkForWhichSearch(entrySearchType);
          var cm_mmc="MIK_ecMobileApp-_-ShopOnline"+entrySearchType+"-_-"+report_Id+"-_-"+report_ProductID;
          if(gblInStoreModeDetails){
            inStoreModeEnabled=gblInStoreModeDetails.isInStoreModeEnabled||false;
          }
          if(inStoreModeEnabled){
            cm_mmc="MIK_ecMobileApp-_-ShopInstoreMode"+entrySearchType+"-_-"+report_Id+"-_-"+report_ProductID;
          }
            kony.print("cm_mmc::"+cm_mmc);
     		//#ifdef iphone
      		 url = decodeURI(url);
   	  		 url = url.replace(/%2C/g,",");
       		 url = encodeURI(url);
    		//#endif
            if(inStoreModeEnabled){
              var storevisitValue = gblInStoreModeDetails.storeID || "";
              if(storevisitValue!=""){
              storevisitValue = MVCApp.Toolbox.common.encodeBASE64(storevisitValue);  
              }   
              MVCApp.Toolbox.common.openApplicationURL(url+"&cm_mmc="+cm_mmc + "&storevisit=" + storevisitValue);
            }else{
          	  MVCApp.Toolbox.common.openApplicationURL(url+"&cm_mmc="+cm_mmc);
            }
          MVCApp.sendMetricReport(frmPJDP, [{"shopOnline":"click"}]);
          var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
          cust360Obj.projectid=report_Id;
      	  MVCApp.Customer360.sendInteractionEvent("ProjectBuyOnline", cust360Obj);
          TealiumManager.trackEvent("Shop Online Button on Project Page", {conversion_category:"Project Page",conversion_id:"Shop Online",conversion_action:2});
          //FacebookAnalytics.reportEvents("", report_Id, url, "frmPJDP");
        }
  	} 
  
  function showPreviousScreen(){
    var indLen =  pdpIndexArray.length;
    if(indLen>1)
    {
      MVCApp.getPDPController().backHistory();
    }
    else
    {
      var tempData =    kony.store.getItem((indLen).toString());
      tempData = tempData + "";
      try {
        tempData = JSON.parse(tempData);
        var formName = tempData.formName || "";
        MVCApp.Toolbox.common.clearPdpGlobalVariables();
        if(formName == "frmShoppingList")
        {
          //#ifdef android
          MVCApp.Toolbox.common.setTransition(frmShoppingList, 2);
          //#endif
          //#ifdef iphone
          frmShoppingList.defaultAnimationEnabled = true;
          MVCApp.Toolbox.common.setTransition(frmShoppingList, 3);
          //#endif
          frmShoppingList.show();
        }
        else if(formName == "frmHome")
        {
          MVCApp.getHomeController().load();
          //frmHome.show();
        }
        else if(formName == "frmProjects"){
          //#ifdef android
          MVCApp.Toolbox.common.setTransition(frmProjects, 2);
          //#endif
          //#ifdef iphone
          frmProjects.defaultAnimationEnabled = true;
          MVCApp.Toolbox.common.setTransition(frmProjects, 3);
          //#endif
          frmProjects.show();
        }
        else{
          MVCApp.getProjectsListController().showUI();
        }
      } catch(e) {
        MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPJDP.js on showPreviousScreen for tempData:" + JSON.stringify(e)}]);
        if(e){
          TealiumManager.trackEvent("Exception While Displaying The Previous Screen Of PJDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
    }
  }
  
  function bindEvents(){
    CopyflxProductItem0bcc8e4a5dec444.lblAisleNo.onClick = function(){
      kony.print("PJDP:Aisle No Clicked");
    };
    frmPJDP.btnAddToShoppingList.onClick = addToshoppingList;
    frmPJDP.btnBuyOnline2.onClick = shopOnline;
    MVCApp.tabbar.bindEvents(frmPJDP);
  	MVCApp.tabbar.bindIcons(frmPJDP,"frmProjects");
    MVCApp.searchBar.bindEvents(frmPJDP,"frmProjects");
    frmPJDP.segPrimaryProductsList.onRowClick = goToPDP;
    frmPJDP.btnCategoryName.onClick = showPreviousScreen;
    frmPJDP.lblCategoryBack.onTouchEnd = showPreviousScreen;
    frmPJDP.btnAdditionalMaterials.onClick = function(){
      toggleAdditionalMaterials = toggleOption(frmPJDP.flxAdditionalMaterialsDesc, frmPJDP.lblExpandAdditionalMate, toggleAdditionalMaterials);
    };
    frmPJDP.btnExpColp.onClick = function(){
      togglePrimaryProducts = toggleOption(frmPJDP.flxPrimaryProducts, frmPJDP.lblPrimaryProductDropdown, togglePrimaryProducts);
    };
    frmPJDP.btnProjInstructions.onClick= function(){
      toggleProjInstructions = toggleOption(frmPJDP.flxProjInsrtuctions, frmPJDP.lblProjInstructDropdown, toggleProjInstructions);
    };
    frmPJDP.btnShare.onClick = function(){
    	var shareURL = "";
        var locale = kony.store.getItem("languageSelected");
        if(locale == "en" || locale == "en_US"){
          shareURL = MVCApp.serviceConstants.shareURL;
        }else{
          shareURL = MVCApp.serviceConstants.shareCanadaURL;
        }
    	var projectId = dataPJDPObj.getProjectId();
    	var shareTxt = shareURL+projectId;
      	var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
		cust360Obj.projectid=projectId;
      	MVCApp.Customer360.sendInteractionEvent("ShareProject", cust360Obj);
    	share(shareTxt);
    };
    CopyflxProductItem0bcc8e4a5dec444.lblAisleNo.onTouchStart = function(){};
    var locale = kony.store.getItem("languageSelected");
    if(locale === "en" || locale === "en_US"){
    	frmPJDP.btnImageZoom.onClick = loadZoomImages;
      }else{
       frmPJDP.btnImageZoom.onClick = MVCApp.WeeklyAdView().dummy;
      }
    frmPJDP.flexSeg.onScrollStart = onreachWidgetEnd;
    frmPJDP.flexSeg.onScrollEnd = onreachScrollEnd;
    frmPJDP.preShow=function(){
    	frmPJDP.flxSearchOverlay.setVisibility(false);
    }
    frmPJDP.postShow=function(){
      //#ifdef iphone
	  MVCApp.Toolbox.common.requestReview();
      //#endif
    var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
    frmPJDP.flxHeaderWrap.lblItemsCount.text=cartValue;
    
      MVCApp.Toolbox.common.setBrightness("frmPJDP");
      MVCApp.Toolbox.common.destroyStoreLoc();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Project Detail:"+frmPJDP.lblName.text;
      lTealiumTagObj.page_name = "Project Detail:"+frmPJDP.lblName.text;
      lTealiumTagObj.page_type = "projects";
      lTealiumTagObj.page_category_name = "Projects:"+frmPJDP.btnCategoryName.text;
      lTealiumTagObj.page_category = "Projects:"+frmPJDP.btnCategoryName.text;
      lTealiumTagObj.page_subcategory_name = frmPJDP.btnCategoryName.text;
      lTealiumTagObj.project_name = frmPJDP.lblName.text;
      lTealiumTagObj.project_parent_category = frmPJDP.btnCategoryName.text;
      lTealiumTagObj.product_id_length = getProductIdLength();
      TealiumManager.trackView("Project Detail Page", lTealiumTagObj);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.projectid=MVCApp.getPJDPController().getProjectIDForTracking();
      cust360Obj.entryType= MVCApp.getPJDPController().getEntryType();
      if(!formFromDeeplink){
        MVCApp.Customer360.sendInteractionEvent("PJDP", cust360Obj);
      }else{
        formFromDeeplink=false;
      }
    };
  }
  
  function share(shareTxt){
    var shareArray = new Array(frmPJDP.lblName.text+"\n"+shareTxt,frmPJDP.imgProject.src,"", frmPJDP.lblName.text);
    if(kony.os.deviceInfo().name==="android"){
	var ShareTestObject = new shareFFI.ShareTest();
       requestPermissionAtRuntime(kony.os.RESOURCE_EXTERNAL_STORAGE,function(){ShareTestObject.shareSomething(shareArray);});
    }else{
	var ShareIphoneObject = new shareFFI.ShareIphone();
	ShareIphoneObject.ShareonSocialMedia(
		/**Array*/ shareArray);
    }  
  }
    
  function loadZoomImages(){
    var imagesGroup = dataPJDPObj.getImages();
    MVCApp.getPinchandZoomController().load(imagesGroup,false,selectedImageIndex);
  }
  
  
  
  function updateScreen(results){
    selectedImageIndex =0;
    var locale=kony.store.getItem("languageSelected");
    if(locale== "fr_CA"){      
      frmPJDP.lblIconShare.padding =[35,0,0,0];
    }else{
       frmPJDP.lblIconShare.padding =[20,0,0,0];
    }
    dataPJDPObj = results;
    try{
    var craftTime=results.getCraftTime() ||"";
    var skillLevel=results.getSkillLevel() ||"";
    frmPJDP.lblName.text = results.getTitle();
    frmPJDP.lblProjDesc.text = results.getLongDescription();
    frmPJDP.imgDuration.src = MVCApp.Toolbox.common.setDuration(craftTime);
    frmPJDP.lblDuration.text = craftTime;
      if(craftTime==""){
        frmPJDP.imgDuration.src ="";
      }
    frmPJDP.imgExpertise.src = MVCApp.Toolbox.common.setSkillLevel(skillLevel);
    frmPJDP.lblExpertiseDesc.text = skillLevel;
      if(skillLevel==""){
        frmPJDP.imgExpertise.src ="";
      }
    if(craftTime=="" && skillLevel==""){
      frmPJDP.flxItemLocations.setVisibility(false);
	  frmPJDP.flxBottomSpacerDND10dp.setVisibility(true);
    }else{
      frmPJDP.flxItemLocations.setVisibility(true);
	  frmPJDP.flxBottomSpacerDND10dp.setVisibility(false);
    }
    
    var additionalMaterialsContent = results.getAdditionalMaterials();
    if(additionalMaterialsContent.length!==0){
      frmPJDP.flxAdditionalMaterials.setVisibility(true);
    }else{
      frmPJDP.flxAdditionalMaterials.setVisibility(false);
    }
    var additionalMaterialsText = "";
    //#ifdef android
  		for(var j=0;j<additionalMaterialsContent.length;j++){
          additionalMaterialsText = additionalMaterialsText +"<span>&bull;&nbsp;"+additionalMaterialsContent[j]+"</span><br><br>";
        }
  	//#endif 
    //#ifdef iphone  
    additionalMaterialsText = "<ul>";
    for(var i=0;i<additionalMaterialsContent.length;i++){
       additionalMaterialsText = additionalMaterialsText+"<li style='line-height:150%'>"+additionalMaterialsContent[i]+"</li>";
    }
    additionalMaterialsText = additionalMaterialsText +"</ul>";
    //#endif  
      
    frmPJDP.rtxtAdditionalMaterials.text = additionalMaterialsText;
    var projInstructionsText = results.getProjectInstructions();
      frmPJDP.segProjInstructions.widgetDataMap = {
        "lblSno":"lblSno",
        "rtxtProjInstructions":"rtxtProjInstructions"
      };
      frmPJDP.segProjInstructions.setData(projInstructionsText);
      if(projInstructionsText.length!==0){
        frmPJDP.flxProjInstructionshead.setVisibility(true);
      }else{
        frmPJDP.flxProjInstructionshead.setVisibility(false);
      }
    }catch(err){
      alert(err);
      if(err){
        TealiumManager.trackEvent("Updation Of PJDP Screen Exception", {exception_name:err.name,exception_reason:err.message,exception_trace:err.stack,exception_type:err});
      }
    }
    var imagesGroup = results.getImages();
    var defaultImage = imagesGroup.largeImages || [];
    if(defaultImage.length >0){
      defaultImage = defaultImage[0] || [];
      defaultImage = defaultImage[0].link || "";
    }
    frmPJDP.imgProject.src= defaultImage;
    var productData = results.getPrimaryProductsList();
    if(productData && productData.length > 0){
      setProductIdLength(productData.length);
    }
    if(locale == "en" || locale == "en_US"){
      	frmPJDP.flxPrimaryProducts.isVisible = true;
    	frmPJDP.segPrimaryProductsList.removeAll();
     	frmPJDP.segPrimaryProductsList.widgetDataMap = {
      		"imgItem":"image",
      		"lblItemName":"title",
            "lblItemPrice":"price",
            "lblItemPriceOld":"markedPriceProduct",
            "lblItemStock":"stockNumber",
            "lblItemNumberReviews":"reviewNumber",
            "lblAisleNo":"aisleNo",
            "btnStoreMap":"storeMapData",
            "flxStarRating":"flxStarRatingAlignment",
             "flxItemLocation":"flxItemLocation",
             "imgStar1":"imgStar1",
             "imgStar2":"imgStar2",
             "imgStar3":"imgStar3",
             "imgStar4":"imgStar4",
             "imgStar5":"imgStar5",
             "btnAddtoList":"btnAddtoList"
    	};
      	frmPJDP.segPrimaryProductsList.setData(productData);
      	if(productData.length!==0){
        	frmPJDP.flxPrimaryProductsHead.isVisible = true;
          	frmPJDP.btnAddToShoppingList.isVisible = true;
      	}else{
        	frmPJDP.flxPrimaryProductsHead.isVisible = false;
         	frmPJDP.btnAddToShoppingList.isVisible = false;
      	}
	}else{
      frmPJDP.flxPrimaryProductsHead.isVisible = false;
      frmPJDP.flxPrimaryProducts.isVisible = false;
      frmPJDP.btnAddToShoppingList.isVisible = false;
      frmPJDP.btnBuyOnline2.isVisible = false;
    }
    
     //show Images
    frmPJDP.flxThumbs.removeAll();
    var smallImages = imagesGroup.smallImages || [];
    if(smallImages.length > 0){
        smallImages= smallImages[0];
      }
    if(smallImages.length>6){
          frmPJDP.flxThumbs.centerX = null;
          frmPJDP.flxThumbs.left = "5dp";
        }else{
          frmPJDP.flxThumbs.centerX = "50%";
        }
     frmPJDP.flxThumbs.width = smallImages.length * 40;
    if(smallImages.length>1){
    frmPJDP.flxThumbs.width = (smallImages.length +1) * 40;
    for(var i=0; i<smallImages.length;i++){
      var defaultSelectedSkin = "sknFlexBgBorderGray";
		if (i === 0) {
		defaultSelectedSkin = "sknFlxBlackBorder2px"; //"sknFlxBlackBorder2px";
		} else {
		defaultSelectedSkin = "sknFlexBgBorderGray"; //"sknFlexBgTransBorderLtGray";
		}
       var FlexContainer067018dc9d32f45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "FlexContainer067018dc9d32f45"+i,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": defaultSelectedSkin,
        "width": "30dp",
        "onClick": onChangeProductImage,
        "zIndex": 1
    }, {}, {});
    FlexContainer067018dc9d32f45.setDefaultUnit(kony.flex.DP);
    var Image0fe184711e4e043 = new kony.ui.Image2({
        "height": "100%",
        "id": "Image0fe184711e4e043"+i,
        "imageWhenFailed": "notavailable_1012x628",
        "imageWhileDownloading": "white_1012x628",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": smallImages[i].link,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer067018dc9d32f45.add(Image0fe184711e4e043);
      
    frmPJDP.flxThumbs.add(FlexContainer067018dc9d32f45);
    } 
    } 
    try{
     var flxZoom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "edx",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "",
        "width": "30dp",
        "zIndex": 1
    },{}, {});
      flxZoom.setDefaultUnit(kony.flex.DP);
      var btnZoom = new kony.ui.Button({
         "height": "100%",
        "id": "btnZ",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon50pxRed",
        "text":"z",
        "top": "0dp",
        "width": "100%",
        "focusSkin":"sknBtnIcon50pxRed",
        "zIndex": 1,
        "onClick":loadZoomImages
      },{
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {}, {});
      if(locale === "en" || locale === "en_US"){
        flxZoom.add(btnZoom);
  		frmPJDP.flxThumbs.add(flxZoom);
      }else{
        frmPJDP.flxThumbs.isVisible = false;
      }

    }catch(err){
      alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.errZoom")+err);
      if(err){
        TealiumManager.trackEvent("Exception While Updating The PJDP Screen", {exception_name:err.name,exception_reason:err.message,exception_trace:err.stack,exception_type:err});
      }
    }
    show();
  	MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
 
  function setCategoryName(categoryData){
    frmPJDP.btnCategoryName.text = categoryData.name || "";
    frmPJDP.imgCategoryImg.src = "";
    frmPJDP.imgCategoryImg.src = categoryData.bgimage || "";
  }
  
  function goToPDP(){
    var selectedIndex = frmPJDP.segPrimaryProductsList.selectedRowIndex[1];
    var segData = frmPJDP.segPrimaryProductsList.data;
    var selectedData = segData[selectedIndex];
    var id = selectedData.id|| "";
    var master =  selectedData.c_master_id || "";
    kony.print("selected data:"+JSON.stringify(selectedData));
    var headerText =[];
    headerText.push("");
    headerText.push("");
    MVCApp.getPDPController().setFromBarcode(false);
    var queryString=MVCApp.getPJDPController().getQueryString();
    MVCApp.getPDPController().setQueryString(queryString);
    MVCApp.getPDPController().setEntryType("pjdp");
    if(master != ""){
     MVCApp.getPDPController().load(master,false,false,headerText,"frmPJDP",id); 
    }else{
     MVCApp.getPDPController().load(id,true,false,headerText,"frmPJDP"); 
    }
  }
  
  function toggleOption(flxAction,lableIcon,toggle){
    if(toggle === true){
      flxAction.setVisibility(false);
      lableIcon.text = "D";
      toggle = false;
    }else{
      flxAction.setVisibility(true);
      lableIcon.text = "U";
      toggle = true;
    }
    
    return toggle;
  }
  function addToProjectListCallBack(results) {
    var loopData = results.LoopDataset|| [];
    if(loopData.length>0){
      for(var z=0;z<loopData.length;z++){
        var  result = loopData[z].result ||"";
        result=result+"";
        if(result!==""  && result.length!==0 ) {
          try {
            result = JSON.parse(result);
            var productId = result.product_id;
            var cRelatedProjects = result.c_relatedProjects || [];
            var flash = result._flash || [];

            if (flash.length > 0 && flash[0].type == MVCApp.Service.Constants.ShoppingList.productAlreadyOnList) {
              kony.store.setItem("shoppingListProdOrProjID","");
              MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.allItemsAdded"), "");
              break;
            } else if (cRelatedProjects.length > 0) {
              if(cRelatedProjects[0].id ===dataPJDPObj.getProjectId() || cRelatedProjects[0].id  ===   kony.store.getItem("shoppingListProdOrProjID")) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.allItemsAdded"), "");
                kony.store.setItem("shoppingListProdOrProjID","");
                var entrySearchType= MVCApp.getPJDPController().getSearchEntryType();
          		entrySearchType=MVCApp.Toolbox.common.checkForWhichSearch(entrySearchType);
                var productIdTracking="";
                if(entrySearchType!=="" && gProductList && gProductList.length>0 ){
                  for(var k=0;k<gProductList.length;k++){
                    var itemData = gProductList[k];
                    productIdTracking= itemData.id;
                    TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAddToList, {conversion_category:entrySearchType+"AddToMyList",conversion_id:productIdTracking,conversion_action:2});
                  }
                }
                TealiumManager.trackEvent("Add All to My List Button on Project Page", {conversion_category:"Project Page",conversion_id:"Add all to My List",conversion_action:2});
                break;      
              }
            }
          } catch(e) {
            MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPJDP.js on addToProjectListCallBack for result:" + JSON.stringify(e)}]);
            if(e){
              TealiumManager.trackEvent("POI Parse Exception While Adding The Project To My List", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
          }
        } else {
          var status = loopData[z].Status ||[];
          kony.store.setItem("shoppingListProdOrProjID","");
          if(0< status.length && status[0].errCode !== undefined &&
             status[0].errCode !== null && status[0].errCode !== "")
          {
            var key  = status[0].errCode;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            alert(MVCApp.Toolbox.common.geti18nValueVA(key));
          }
          else if(result === "")
          {
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.notAdded"), "");
          }
          break;
        }
      }
    }
    else{
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.errProjectListAdding"), "");
    }
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  function addToProductListCallBack(results) {
      results = results.result || "";
      results=results+"";
      if(results!==""  && results.length!==0 ) {
        try {
          results = JSON.parse(results);
          var listId = results.id;
          var productId = results.product_id;
          var flash = results._flash || [];
          if (flash.length > 0 && flash[0].type == MVCApp.Service.Constants.ShoppingList.productAlreadyOnList) {
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.productAlreadyInSList"), "");
          } else {
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.itemAdded"), "");
            TealiumManager.trackEvent("Add to My List Button Project Page", {conversion_category:"Project Page",conversion_id:"Add to My List",conversion_action:2});
          }
        } catch(e) {
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPJDP.js on addToProductListCallBack for results:" + JSON.stringify(e)}]);
          MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.tryAgainLater"), "");
          if(e){
            TealiumManager.trackEvent("POI Parse Exception While Adding The Product To My List", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }else{
          MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.tryAgainLater"), "");
      }
      MVCApp.Toolbox.common.dismissLoadingIndicator();
     }

  
  function addToshoppingList(){
    currentFormForPasswordExpiryCase=frmPJDP;
    var addShopSkin = frmPJDP.btnAddToShoppingList.skin;
    if (addShopSkin === "sknBtnDisable") {
    } else {
      MVCApp.sendMetricReport(frmPJDP, [{"shoppingList":"click"}]);
      var projectId = dataPJDPObj.getProjectId();   
      MVCApp.getShoppingListController().addProjectListLooping(projectId);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.projectid=projectId;
      MVCApp.Customer360.sendInteractionEvent("AddProjectToMyList", cust360Obj);
    }
  }
  
  function onChangeProductImage() {
		if(this.skin != "sknFlxBlackBorder2px"){
		var imagesGroup = dataPJDPObj.getImages();
		var smallImages = imagesGroup.smallImages;
        if(smallImages.length>0){
          smallImages = smallImages[0];
        }
		for(var i=0;i<smallImages.length;i++){
		frmPJDP["FlexContainer067018dc9d32f45"+i].skin =  "sknFlexBgBorderGray";// "sknFlexBgTransBorderLtGray";
		}
		this.skin = "sknFlxBlackBorder2px";
		var selectedIndex = this.id.split("FlexContainer067018dc9d32f45")[1];
      	selectedImageIndex = selectedIndex;
		var defaultImage = imagesGroup.largeImages || [];
      	if(defaultImage.length > 0){
        defaultImage= defaultImage[0];
        }
      	var selImage= "";
		if (defaultImage.length > 0) {
			var selImageTemp = defaultImage[selectedIndex] || "";
			selImage = selImageTemp.link || "";
		}
		frmPJDP.imgProject.src = selImage;  
	  }
	}
      
  //Here we expose the public variables and functions
  
  
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    addToProductListCallBack:addToProductListCallBack,
    	setCategoryName:setCategoryName,
    addToProjectListCallBack:addToProjectListCallBack
        
    };
  
});



