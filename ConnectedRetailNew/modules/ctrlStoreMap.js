
/**
 * PUBLIC
 * This is the controller for the StoreMap form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.StoreMapController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.StoreMapModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.StoreMapView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  
  var isFromMyList = false;
  
  function setFromMyList(status){
    isFromMyList = status;
  }
  function getFromMyList(){
    return isFromMyList;
  }
 
  
  	var categoryData;
  	
  	function setCategoryData(data){
      categoryData = data;
    }
  	
  	function getCategoryData(){
      return categoryData;
    }
  
  
  	var fromStoreLocator=false;
  	var storeInfo;
  	function setFromStroreLocator(value){
      fromStoreLocator=value;
    }
  	function getFromStroreLocator(){
      return fromStoreLocator;
    }
  	function setStoreInfo(value){
      storeInfo=value;
    }
  	function getStoreInfo(){
      return storeInfo;
    }
  
  var currentAisle;
  
    function setCurrentAisle(aisle){
      currentAisle = aisle;
    }
    function getCurrentAisle(){
      return currentAisle;
    }
  var _indexes;
   function setFirstLastIndex(indexes){
		_indexes = indexes;
   }
  function getFirstLastIndex(){
		return _indexes;
   }
  
  function enableButtons(currentAisle){
    init();
    view.enableButtons(currentAisle);
  }
  
  	function load(calloutParams){
      	setSearchTerms("");
      	MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("StoreMap Controller.load");
      	init();
        setCurrentAisle(calloutParams.currentAisle || "");
      	setFromMyList(calloutParams.isFromMyList || false);
        var requestParams = {};      	
      	var storeNo ="";
      	var storeId="";
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeNo = gblInStoreModeDetails.storeID;
          storeId=storeNo;
        }else{
          var storeString=kony.store.getItem("myLocationDetails") ||"";
          if(storeString!==""){
            try {
              var storeObj=JSON.parse(storeString);
              storeNo =storeObj.clientkey;
              storeId=storeNo;
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlStoreMap.js on load for storeString:" + JSON.stringify(e)}]);
              storeNo ="0102";
              if(e){
                TealiumManager.trackEvent("POI Parse Exception in Store Map Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          }else{
            storeNo ="0102";
          }
        }
      	setFromStroreLocator(false);
      	if(storeId!==""){
          requestParams["storeNo"]= storeNo;
          model.loadData(view.updateScreen,requestParams,calloutParams);
        }else{
          var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"storeMapFullStore"};
          whenStoreNotSet = JSON.stringify(whenStoreNotSet);
          kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
          MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreStoreMap") ,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
        }
    }
  	function loadStoreMap(storeNo,params,storeInfo){
      	setSearchTerms("");
      	MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("StoreMap Controller.load");
      	init();
      	setCurrentAisle(params.currentAisle || "");
      	setFromMyList(params.isFromMyList || false);
        var requestParams = {};
      	setFromStroreLocator(true);
        requestParams["storeNo"]= storeNo;
        model.loadData(view.updateScreen,requestParams,params);
      	setFromStroreLocator(true);
      	setStoreInfo(storeInfo);
    }
  	var entryPoint="";
  	function getEntryPoint(){
      return entryPoint;
    }
  	function setEntryPoint(value){
      entryPoint=value;
    }
  function showStoreMapWithAisleFor(){
    var storeId="";
    var searchTerms=getSearchTerms();
    var storeString=kony.store.getItem("myLocationDetails") ||"";
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeId=gblInStoreModeDetails.storeID;
    }else if(storeString!==""){
      var storeObj=JSON.parse(storeString);
      storeId=storeObj.clientkey;
    }else{
      storeId="";
    }
    if(storeId!=""){
      var reqParams ={};
    reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
    reqParams.app_Locale = 'en';
    reqParams.store_id  = storeId;
    reqParams.q = searchTerms;
    MVCApp.Toolbox.common.showLoadingIndicatorForVoiceSearch("");
    MakeServiceCall( 'voicesearch','getAisleForSearchProduct',reqParams,
                    function(results){
      if(results.opstatus===0 || results.opstatus==="0" ){
        kony.print( JSON.stringify(results));
        var inventory = JSON.parse(results.products[0].c_store_inventory);
        var aisleNo = inventory.aisle;
        var productName = results.products[0].name;
        kony.print (" *** Product Name =  " + productName);
        kony.print (" *** aisleNo  =  " + aisleNo);
        var params = {};
        params.aisleNo = aisleNo;
        params.productName = productName;
        params.calloutFlag = true;
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        MVCApp.getStoreMapController().load(params); 
      }   
    },function(error){
      kony.print("Error :  Response is "+JSON.stringify(error));
      MVCApp.Toolbox.common.dismissLoadingIndicator();
      sorryPageFlag = true;
      currentForm.flxVoiceSearch.lblUtterenace2.text = recoText;
      currentForm.flxVoiceSearch.lblUtterence1.setVisibility(false);
      currentForm.flxVoiceSearch.lblUtterance3.setVisibility(false);
      currentForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
      currentForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.NoResultsFoundForVoiceSearch");
      currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
      currentForm.flxVoiceSearch.forceLayout();
    });
    }else{
      var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"storeMapForAisle"};
        whenStoreNotSet = JSON.stringify(whenStoreNotSet);
        kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
        //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.noStoreSet"));
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreStoreMap") ,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
    }	
  }
  var _searchTerm="";
  function setSearchTerms(text){
    _searchTerm=text;
  }
  function getSearchTerms(){
    return _searchTerm;
  }

  	
          		
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init: init,
    	load: load,
      	loadStoreMap:loadStoreMap,
      	setFromStroreLocator:setFromStroreLocator,
      	getFromStroreLocator:getFromStroreLocator,
      	getStoreInfo:getStoreInfo,
        setCategoryData:setCategoryData,
      	getCategoryData:getCategoryData,
      	setCurrentAisle:setCurrentAisle,
      	getCurrentAisle:getCurrentAisle,
      	setFromMyList:setFromMyList,
      	getFromMyList:getFromMyList,
      	setFirstLastIndex:setFirstLastIndex,
      	getFirstLastIndex:getFirstLastIndex,
      	enableButtons:enableButtons,
      	setEntryPoint:setEntryPoint,
      	getEntryPoint:getEntryPoint,
      	showStoreMapWithAisleFor:showStoreMapWithAisleFor,
      	getSearchTerms:getSearchTerms,
      	setSearchTerm:setSearchTerms
    };
});

