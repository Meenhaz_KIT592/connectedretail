
/**
 * PUBLIC
 * This is the controller for the SignIn form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
var isFromRewards=false;
MVCApp.SignInController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	var deepLinkFlag =false;
  var favButtonFromCart=false;
  var prevFormName = "";
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
         
        model = new MVCApp.SignInModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.SignInView();
        
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  
  function setDeepLinkFlag(flag)
  {
    deepLinkFlag =flag;
     prevFormName = kony.application.getCurrentForm().id;
  }
  function getDeepLinkFlag()
  {
    return deepLinkFlag;
  }
  function getDeepLinkPrevName()
  {
    return prevFormName;
  }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function load(deepLinkFlag){
      	kony.print("SignIn Controller.load");
        setDeepLinkFlag(deepLinkFlag);
      	init();
        view.updateScreen();
    }
  function validateLogin(requestParams){
   	MVCApp.Toolbox.common.showLoadingIndicator("");
    requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
    model.validateLogin(view.showTouchIDAlert,requestParams);
  }
  function invalidPassWordFromRewards(){
    isFromRewards=true;
    load(false);
  }
  function getButtonTypeForCheckOutButton(){
    return favButtonFromCart;
  }
  function setButtonTypeForCheckOutButton(flag){
    favButtonFromCart=flag;
  }
          		
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init: init,
    	load: load,
      	validateLogin:validateLogin,
      getDeepLinkFlag:getDeepLinkFlag,
      getDeepLinkPrevName:getDeepLinkPrevName,
      invalidPassWordFromRewards:invalidPassWordFromRewards,
      setButtonTypeForCheckOutButton:setButtonTypeForCheckOutButton,
      getButtonTypeForCheckOutButton:getButtonTypeForCheckOutButton
    };
});

