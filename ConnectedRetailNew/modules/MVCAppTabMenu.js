/**
 * PUBLIC
 * This is the Main App Tab Bar Menu node, it is not a class and does not need to
 * be instantiated. It is just a collection of convenience
 * functions for managing the tab bar menu
 * _variable or _functions indicate elements which are not publicly exposed
 */
gblProducts=null;
gblProjects=null;
var gblStoreKeyForProducts="";
var gblTagValues={};
var MVCApp = MVCApp || {};
MVCApp.tabbar= {
  bindEvents : function (myForm) {
    try {
      /**
       * @function
       *
       */
      
      /**
       * @function
       *
       */
      
      myForm.flxFooterWrap.btnProducts.onClick = function(){
        kony.store.setItem("searchedProdId","");
        MVCApp.getProductsListController().setFromDeepLink(false);
        gblIsPasswordExpired=false;
      	currentFormForPasswordExpiryCase=null;
        if(kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false){
          rewardsToggle();
        }
        MVCApp.Toolbox.common.dispFooter();
        
        var form = kony.application.getCurrentForm().id;
        var storeModeEnabled=false;
        try{
          /*if(form=="frmWebView"){     
          frmWebView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
          frmWebView.brwrView.evaluateJavaScript("document.open();document.close();");
        }else if(form=="frmCartView"){
          frmCartView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
          frmCartView.brwrView.evaluateJavaScript("document.open();document.close();");

        }*/
           MVCApp.Toolbox.common.destoryFrmWeb(form);
        }catch(e){
          
        }
        
      	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
              storeModeEnabled=true;
            }
        var region=MVCApp.Toolbox.common.getRegion();
    	var reportData={"storeModeEnabled":storeModeEnabled,"formName":form,"region":region};
        MVCApp.sendMetricReport(frmHome,[{"ProductsClickFromAppMenu":JSON.stringify(reportData)}]);
        if(form == "frmProducts" ||form == "frmProductCategory" || form == "frmProductCategoryMore") {
          MVCApp.getProductsController().displayScreen(); 
        }else {
          MVCApp.getProductsController().updateScreen();
        }
      };
    }
    catch (e) {
      if(e){
        TealiumManager.trackEvent("Exception On Clicking The Shop Button Present On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }

    try {
      myForm.flxFooterWrap.btnProjects.onClick = 
        function(){
        kony.store.setItem("searchedProdId","");
        gblIsPasswordExpired=false;
      	currentFormForPasswordExpiryCase=null;
        if(kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false){
          rewardsToggle();
        }
        var form = kony.application.getCurrentForm().id;
        try{
         /* if(form=="frmWebView"){     
          frmWebView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
          frmWebView.brwrView.evaluateJavaScript("document.open();document.close();");
          
        }else if(form=="frmCartView"){
          frmCartView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");

        }*/
          MVCApp.Toolbox.common.destoryFrmWeb(form);
        }catch(e){
          
        }
        
        var storeModeEnabled=false;
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeModeEnabled=true;
        }
        var region=MVCApp.Toolbox.common.getRegion();
        var reportData={"storeModeEnabled":storeModeEnabled,"formName":form,"region":region};
        MVCApp.sendMetricReport(frmHome,[{"ProjectsClickFromAppMenu":JSON.stringify(reportData)}]);
        if(form == "frmProjects" ||form == "frmProjectsCategory"){
          MVCApp.getProjectsController().displayScreen();
        }else{
          if( gblProjects!== "" && gblProjects!= null){
            MVCApp.getProjectsController().updateScreen();          
          }
          else
            MVCApp.getProjectsController().load();
        } 
      };
    } catch (e) {
      if(e){
        TealiumManager.trackEvent("Exception On Clicking The Projects Button Present On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
    
    try {
      myForm.flxFooterWrap.btnMyLists.onClick = 
        function(){
        kony.store.setItem("searchedProdId","");
        gblIsPasswordExpired=false;
      	currentFormForPasswordExpiryCase=null;
        var form=kony.application.getCurrentForm().id||"";
        try{
         /* if(form=="frmWebView"){     
          frmWebView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
        }else if(form=="frmCartView"){
          frmCartView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");

        }*/
           MVCApp.Toolbox.common.destoryFrmWeb(form);
        }catch(e){
          
        }
        
        if(form == "frmMore" && toggleRewards === false){
          rewardsToggle();
        }
        MVCApp.getShoppingListController().getProductListItems();
      };
    } catch (e) {
      if(e){
        TealiumManager.trackEvent("Exception On Clicking The My List Button Present On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
    
    try {myForm.flxFooterWrap.btnWeeklyAd.onClick= function(){
      kony.store.setItem("searchedProdId","");
      if(kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false){
        rewardsToggle();
      }
      weeklyAdEntryFromVoiceSearch=false;
      var form = kony.application.getCurrentForm().id;
      try{
       /* if(form=="frmWebView"){     
          frmWebView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
        }else if(form=="frmCartView"){
          frmCartView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");

        }*/
         MVCApp.Toolbox.common.destoryFrmWeb(form);
      }catch(e){
        
      }
      
      var storeModeEnabled=false;
      if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
        storeModeEnabled=true;
      }
      var region=MVCApp.Toolbox.common.getRegion();
      var reportData={"storeModeEnabled":storeModeEnabled,"formName":form,"region":region};
      MVCApp.sendMetricReport(frmHome,[{"WeeklyAdFromAppMenu":JSON.stringify(reportData)}]);
      MVCApp.Toolbox.common.dispFooter();
      frmWeeklyAdHome.flxFooterWrap.bottom = "0dp";
      if(MVCApp.Toolbox.Service.getMyLocationFlag()==="true" || (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled)){

        //#ifdef iphone
        frmWeeklyAdHome.defaultAnimationEnabled = false;
        //#endif
        //#ifdef android
        MVCApp.Toolbox.common.setTransition(frmWeeklyAdHome,0);
        //#endif
        loadWeeklyAds = true;
        MVCApp.getWeeklyAdHomeController().load();
      }else{  
        var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"weeklyAds"};
        whenStoreNotSet = JSON.stringify(whenStoreNotSet);
        kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd") ,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
      }

    };} catch (e) {
      if(e){
        TealiumManager.trackEvent("Exception On Clicking The Weekly Ad Button Present On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
    try {myForm.flxFooterWrap.btnMore.onClick=function(){
      kony.store.setItem("searchedProdId","");
      gblIsPasswordExpired=false;
      currentFormForPasswordExpiryCase=null;
      try{
        var form = kony.application.getCurrentForm().id;
     /* if(form=="frmWebView"){     
          frmWebView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
        }else if(form=="frmCartView"){
          frmCartView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");

        }*/
         MVCApp.Toolbox.common.destoryFrmWeb(form);
      }catch(e){
        
      }
      
      if(kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false){
        rewardsToggle();
      }
      MVCApp.getMoreController().load();
    };} catch (e) {
      if(e){
        TealiumManager.trackEvent("Exception On Clicking The More Button Present On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
         try{
    	if(myForm.flxFooterWrap.btnCoupon){
            myForm.flxFooterWrap.btnCoupon.onClick = function(){
              voiceSearchFlag = false;
              couponsEntryFromVoiceSearch=false;
            var storeModeEnabled=false;
              //#ifdef android
              try{
                if(writesettings == null || writesettings == undefined){
                   writesettings=new runtimepermissions.permissions();
                   writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow") );
                   writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
                   writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
                   writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
                   writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                }
              }catch(e){
                if(e){
                  kony.print("Exception while setting run time permissions"+e);
                }
              }
              writesettings.requestPermission("android.write_settings.permission");
              //#endif
            if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
                    storeModeEnabled=true;
                  }
            var region = MVCApp.Toolbox.common.getRegion();
          var reportData={"storeModeEnabled":storeModeEnabled,"region":region};
            MVCApp.sendMetricReport(frmHome,[{"CouponsCLick":JSON.stringify(reportData)}]);
              MVCApp.getHomeController().loadCoupons(); 
              //MVCApp.getCouponListController().load();
            frmObject=myForm;
              try{
                var form = kony.application.getCurrentForm().id;
     /* if(form=="frmWebView"){     
          frmWebView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");
        }else if(form=="frmCartView"){
          frmCartView.brwrView.evaluateJavaScript("window.stop();");
          kony.print(form+": window.stop");

        }*/
                 MVCApp.Toolbox.common.destoryFrmWeb(form);
              }catch(e){
                
              }
              };
       }
         } catch (e) {
          if(e){
            TealiumManager.trackEvent("Exception On Clicking The Coupons Icon Present On The Header", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
    
  },
  bindIcons : function (myForm,mainFormName) {
    //either pass only the current form as myForm if it is top-level
    //or pass the current form as myForm + the name of the top-level form whose
    //icon you want to make active as mainFormName in addition to myForm
    if (mainFormName===undefined || mainFormName===null || mainFormName==="") {
      mainFormName = myForm.id;
    }
    //Define the image widgets of the tab bar menu for each top-level form 
    //(if the form is linked through the bottom menu)
    var tabMenuIconMap = {
      "frmProducts" : {
        "widget" : "flxProducts",
        "skinNormal":"slFbox",
        "skinActive" : "sknFlexBgBlackOpaque10"
      },
      "frmProjects" : {
       	"widget" : "flxProjects",
        "skinNormal":"slFbox",
        "skinActive" : "sknFlexBgBlackOpaque10"
      },
      "frmWeeklyAd" : {
        "widget" : "flxWeeklyAd",
        "skinNormal":"slFbox",
        "skinActive" : "sknFlexBgBlackOpaque10"
      },
       "frmShoppingList" : {
        "widget" : "flxMyLists",
        "skinNormal":"slFbox",
        "skinActive" : "sknFlexBgBlackOpaque10"
      },
      "frmMore" : {
        "widget" : "flxMore",
        "skinNormal":"slFbox",
        "skinActive" : "sknFlexBgBlackOpaque10"
      },
      "frmHome" : {
        "widget" : "flxMore",
        "skinNormal":"slFbox",
        "skinActive" : "slFbox"
      }
    };
	
    //Go through all the image widgets and set them to their normal icon
    for (var key in tabMenuIconMap) {
      kony.print(key);
      if (tabMenuIconMap.hasOwnProperty(key)) {
        var formEntry = tabMenuIconMap[key];
        try {
          myForm["flxFooterWrap"][formEntry.widget].skin=formEntry.skinNormal;
        } catch (e) {
          if(e){
            TealiumManager.trackEvent("UI Exception Of Setting Normal Skin For a Button On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }    
      }
    }

    //Check if there is an entry for the current form
    //and if there is one, then make the current entry active
    var currentFormEntry = tabMenuIconMap[mainFormName];
    if ((currentFormEntry===undefined)===false) {
     try {myForm["flxFooterWrap"][currentFormEntry.widget].skin=currentFormEntry.skinActive;} catch (e) {
       if(e){
         TealiumManager.trackEvent("UI Exception Of Setting Active Skin For a Button On Footer", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
       }
     }     
    }
  }
};