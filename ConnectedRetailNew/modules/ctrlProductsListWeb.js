
/**
 * PUBLIC
 * This is the controller for the ProductsList form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var firstTime=false;
var MVCApp = MVCApp || {};
MVCApp.ProductsListWebController = (function(){
  	var _isInit = false;
  	 var model = null;//Backend interaction
 	 var view = null;//UI interaction
 	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */
	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.ProductsListWebModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.webView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  
  	function load(searchString){
      init();
      firstTime=true; 
      model.load(searchString,view.loadPdpView);
      _isInit=false;
    }
  
  /**
     * PUBLIC
     * Open the form.
     */
    function loadWebView(url) {
           init();
      kony.print("url is ++++++"+url);
     if( MVCApp.Toolbox.common.getCartCallback())
      	 MVCApp.Toolbox.common.setCartCallback(null);
      	if(kony.application.getCurrentForm().id !== "frmWeeklyAdDetail"){
          	var storeId= MVCApp.Toolbox.common.getStoreID();
          	var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
          	var charReplace="?";
            if(url.indexOf("?")>-1){
              charReplace="&";
            }
          	var appRequest= charReplace+"stid="+storeId+"&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
          	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
              	if(storeChangeInStoreMode){
                  	appRequest = charReplace+"stid="+favStoreId+"&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                }
                    appRequest = appRequest + "&storevisit="+storeId;
     		}
          	if(url.indexOf("?") !== -1){
              appRequest = appRequest.replace(/\?/g, "&");
            }
      		url = url+appRequest;
        }
        model.loadData(view.loadPdpView,url);
    }
  
  function getAppSrc(){
      var region= "mik_us_mobile";
      var locale= kony.store.getItem("languageSelected");
      if(locale == "en_US" || locale == "en"){
        
      }else if(locale =="en_CA"){
        region="mik_cn_en_mobile";
      }else if(locale=="fr_CA"){
        region="mik_cn_fr_mobile";
      }
      return region;
    }
    //Here we expose the public variables and functions
 	return {
      init: init,
      load: load,
      loadWebView:loadWebView
    };
});

