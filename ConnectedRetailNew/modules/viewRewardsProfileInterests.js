/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
interestsflag = false;
MVCApp.RewardsProfileInterestsView = (function(){
  var updateflag = false;
  /**
	 * PUBLIC
	 * Open the Events form
	 */
  
  function show(nextFlag){    
    frmRewardsProfileInterests.show();
    var data = frmRewardsProfileInterests.segInterests.data;
    var skillFlag = false;
    if(data !="" && data != null && data != undefined){
      for(var i=0; i< data.length; i++){ 
        if(data[i].select.isVisible === true)
          skillFlag = true;
      }
    }  
    if(nextFlag){
      frmRewardsProfileInterests.btnReset.isVisible = false;
      frmRewardsProfileInterests.btnNextToRewardsSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
    }else if(skillFlag === true){
      frmRewardsProfileInterests.btnReset.isVisible = true;
      frmRewardsProfileInterests.btnNextToRewardsSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
    }
    else{
      frmRewardsProfileInterests.btnReset.isVisible = true;
      frmRewardsProfileInterests.btnNextToRewardsSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip");
    }
  } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   
  function bindEvents(){
    MVCApp.tabbar.bindEvents(frmRewardsProfileInterests);
	MVCApp.tabbar.bindIcons(frmRewardsProfileInterests,"frmMore");
    frmRewardsProfileInterests.btnNextToRewardsSkills.onClick = rewardsProfileSkills;
	frmRewardsProfileInterests.btnHeaderLeft.onClick = showPreviousScreen;
    frmRewardsProfileInterests.segInterests.onRowClick = getSelectedItem;
    frmRewardsProfileInterests.btnReset.onClick = resetFields;
    
  }
  
  	function resetFields(){
      
      var data = frmRewardsProfileInterests.segInterests.data;    
    rewardsProfileData.interests =[];
     rewardsProfileData.c_surveyQA = [];
    for(var i=0; i< data.length; i++){ 
    var interests =[];      
      var str = "";     
      interests.value = data[i].value;
      interests.select = data[i].select;      
	  interests.select.isVisible = false;  
      rewardsProfileData.interests.push(interests);
      str = frmRewardsProfileInterests.rtxtQuestion.txt+":"+"";
      rewardsProfileData.c_surveyQA.push(str); 
	
 	}   
	
	rewardsProfileData.interestData = rewardsProfileData.interests;
	frmRewardsProfileInterests.segInterests.data = rewardsProfileData.interestData;
    var formId = kony.application.getPreviousForm().id;
    if(formId == "frmReviewProfile")
	show(true);
      else
      show(false);
      
    }
    function getSelectedItem(){    
      var selectedIndex =frmRewardsProfileInterests.segInterests.selectedRowIndex[1]; 
      var selectedItem = frmRewardsProfileInterests.segInterests.selectedRowItems[0];
      var data = frmRewardsProfileInterests.segInterests.data;
      for(var i=0; i< data.length; i++){ 
        data[i].select.isVisible = false;
      }
      frmRewardsProfileInterests.segInterests.setData(data); 

      if(updateflag){
        if(selectedItem.select.isVisible === false)
          selectedItem.select.isVisible = true;          
        else
          selectedItem.select.isVisible = false;
        frmRewardsProfileInterests.btnReset.isVisible = false;
        frmRewardsProfileInterests.btnNextToRewardsSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
      }
      else if(selectedItem.select.isVisible === false){
        selectedItem.select.isVisible = true;
        frmRewardsProfileInterests.btnReset.isVisible = true;
        frmRewardsProfileInterests.btnNextToRewardsSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
      }
      else{
        selectedItem.select.isVisible = false;
        frmRewardsProfileInterests.btnReset.isVisible = true;
        frmRewardsProfileInterests.btnNextToRewardsSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip");
      }  

      frmRewardsProfileInterests.segInterests.setDataAt (selectedItem, selectedIndex, 0);            
    }  
  
  function updateScreen(rewardsDataObj){    
    var interestsDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    interestsDat = interestsDat.getRewardsProfileInterestsData(); 
    if (interestsDat !=  []) {
    frmRewardsProfileInterests.segInterests.widgetDataMap = {
      lblInterest : "value",
      lblSelect:"select"
    };
    frmRewardsProfileInterests.segInterests.data = interestsDat;
    frmRewardsProfileInterests.rtxtQuestion.txt = interestsDat.questions;
    }
    show(false);
  }
  
  function showPreviousScreen(){    
//     frmRewardsProfileAddChild.show();
    var formId = kony.application.getPreviousForm().id;
    if(formId == "frmReviewProfile")
    MVCApp.getReviewProfileController().load();
    else
    MVCApp.getRewardsProfileChildController().updateData();
  }
    
  function rewardsProfileSkills(){    
    var qstnflag = false;
    var data = frmRewardsProfileInterests.segInterests.data;
    rewardsProfileData.interestData = data;
    rewardsProfileData.interests =[];
     rewardsProfileData.c_surveyQA = [];
    var str = "";
    for(var i=0; i< data.length; i++){ 
    var interests =[];  
      str = "";
    if(data[i].select.isVisible === true){      
     
      interests.interest = data[i].value;
      interests.select = data[i].select.isVisible;      
      rewardsProfileData.interests.push(interests);
      str = frmRewardsProfileInterests.rtxtQuestion.txt+":"+interests.interest;    
      rewardsProfileData.c_surveyQA.push(str); 
      qstnflag = true;
    }      
 	}
    if(!qstnflag){
        str = frmRewardsProfileInterests.rtxtQuestion.txt+":"+"";
        rewardsProfileData.c_surveyQA.push(str); 
      }
    var formId = kony.application.getPreviousForm().id;
    if(formId == "frmReviewProfile")
    MVCApp.getReviewProfileController().load();
    else if(skillsflag === true)
    MVCApp.getRewardsProfileSkillsController().updateData(false);    
    else
      MVCApp.getRewardsProfileSkillsController().load();    
    
  }
  function updateData(nextFlag){
    updateflag = nextFlag;
    if(rewardsProfileData.interestData != undefined){
      frmRewardsProfileInterests.segInterests.widgetDataMap = {
      lblInterest : "value",
      lblSelect:"select"
    };
      frmRewardsProfileInterests.segInterests.data = rewardsProfileData.interestData;  
       var intrst = rewardsProfileData.c_surveyQA;
      if(intrst.length!=0){
      var intrstArray=intrst[0].split(":");      
      frmRewardsProfileInterests.rtxtQuestion.txt = intrstArray[0];      
      }
      else{
        var interestsDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    	interestsDat = interestsDat.getRewardsProfileInterestsData();
        frmRewardsProfileInterests.rtxtQuestion.txt = interestsDat.questions;
      }
    }
    
    else{
    var interestsDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    interestsDat = interestsDat.getRewardsProfileInterestsData(); 
    if (interestsDat !=  []) {
    frmRewardsProfileInterests.segInterests.widgetDataMap = {
      lblInterest : "value",
      lblSelect:"select"
    };
    frmRewardsProfileInterests.segInterests.data = interestsDat;
    frmRewardsProfileInterests.rtxtQuestion.txt = interestsDat.questions[0];
    }
    }
    interestsflag = true;
    show(nextFlag);
  }  
  
  //Here we expose the public variables and functions
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	updateData:updateData
    };
  
});