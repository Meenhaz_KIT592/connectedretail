/**
 * PUBLIC
 * This is the model for formStoreMap form
 */
var MVCApp = MVCApp || {};
MVCApp.StoreMapModel = (function(){
 var serviceType = MVCApp.serviceType.KonyMFDBService;
  var operationId = MVCApp.serviceEndPoints.getStoreMapData;
  function loadData(callback,requestParams,calloutParams){ 
    kony.print("modelStoreMap.js");
  MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
          callback(results,calloutParams);
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
    }
                   );
   
  }
 	return{
      loadData:loadData
    };
});
