/**
 * PUBLIC
 * This is the model for formProjectsCategory form
 */
var MVCApp = MVCApp || {};
MVCApp.ProjectsCategoryModel = (function(){
 var serviceType = MVCApp.serviceType.DemandwareService;
  var operationId = MVCApp.serviceEndPoints.ProductCategory;
  function loadProjectCategories(callback,requestParams){
    
    kony.print("modelProjectsCategory.js");
//    MakeServiceCall( serviceType,operationId,requestParams,
//                            function(results)
//                            {
//       kony.print("Response is "+JSON.stringify(results));
//           if(results.opstatus===0 || results.opstatus==="0" ){
//             var projectsDataObj = new MVCApp.data.Projects(results);   
//             callback(projectsDataObj);
//           }

//     },function(error){
//      	 MVCApp.Toolbox.common.dismissLoadingIndicator();
//          kony.print("Response is "+JSON.stringify(error));
//     }
//                           );
    
    
   var projectList  =  kony.store.getItem("GetProjectCategories"); // this is kept for to getsubcategories 
    
    
    
    for(i=0;i<projectList.length;i++)
        {
             kony.print(projectList[i].id +" , "+ requestParams.id);
       
     		 if(projectList[i].id == requestParams.id )
              { 
              
                             
                var  projectsDataObj = new MVCApp.data.Projects(projectList[i]);
             
                  callback(projectsDataObj);
              }
       }
    
  }
 	return{
      loadProjectCategories:loadProjectCategories
    };
});
