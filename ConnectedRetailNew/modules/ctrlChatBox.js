var gblStoreForChatbot="";

/**
 * PUBLIC
 * This is the controller for the chatbox form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ChatBoxController = (function(){

  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction

  /**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
  function init(theme,locale){
    if (_isInit===true) return;
    //Enables us to use different UI interactions depending on form factor
    view = new MVCApp.ChatBoxView();
    //Bind events to UI
    view.bindEvents();

    _isInit = true;
  }
  /**
	 * PUBLIC
	 * Open the form.
	 */
  function load(){
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      gblStoreForChatbot = gblInStoreModeDetails.storeID;
      setStoreModeStatus(true);
      }else{
       if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
       var myLocationString=kony.store.getItem("myLocationDetails") || "";
       var locationDetails=JSON.parse(myLocationString);
       gblStoreForChatbot = locationDetails.clientkey;
     }else{
       gblStoreForChatbot ="";
     }  
    }
     
    MVCApp.Toolbox.common.showLoadingIndicator("");
    init();
    view.updateScreen();
  }
  var session = "";
  function setSession(value){
    session = value;
  }
  function getSession(){
    return session;
  }
  var isStoreModeAlreadyEnabled = false;
  function setStoreModeStatus(pIsStoreModeAlreadyEnabled){
      isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
  }
  function getStoreModeStatus(){
      return isStoreModeAlreadyEnabled;
  } 
  /**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

  //Here we expose the public variables and functions
  return {
    init: init,
    load: load,
    setSession:setSession,
    getSession:getSession,
    setStoreModeStatus:setStoreModeStatus,
    getStoreModeStatus:getStoreModeStatus
  };
});

