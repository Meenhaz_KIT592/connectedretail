/**
 * PUBLIC
 * This is the view for the Settins form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.SettingsView = (function(){
  
  /**
	 * PUBLIC
	 * Open the More form
	 */
  
  function show(){
  var userObject = kony.store.getItem("userObj");
  if(userObject === "" || userObject === null){
   frmSettings.flxSignOut.setVisibility(false);
  }else{
   frmSettings.flxSignOut.setVisibility(true); 
  }
  //#ifdef iphone
    if(redirectToSettings.supportFaceID()){
      frmSettings.CopylblRegionLanguageValue09a67f5778b7946.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.Settings.faceId");
    }else{
      frmSettings.CopylblRegionLanguageValue09a67f5778b7946.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.Settings.touchId");
    }
    //#endif 
  if(kony.store.getItem("touchIdSupport")){	
    frmSettings.flexTouchToggle.setVisibility(true);	
  }else{
    frmSettings.flexTouchToggle.setVisibility(false);	
  }	
    if(kony.store.getItem("storeFirstUserForTouchId") != null){
      frmSettings.switchTouch.src = "on.png";
    }else{	
      frmSettings.switchTouch.src = "off.png";
    }
    if(kony.store.getItem("switch") == null ){
      frmSettings.switchTouch.src = "on.png";
    }else if(kony.store.getItem("switch") ){
      frmSettings.switchTouch.src = "on.png";
    }else{
      frmSettings.switchTouch.src = "off.png";
    }
	// push notifications	
	if( kony.store.getItem("pushSwitch") ){	
      frmSettings.pushSwitch.src = "on.png";
	}else{
      frmSettings.pushSwitch.src = "off.png";
	}		    
	//end push
  //#ifdef android
		MVCApp.Toolbox.common.setTransition(frmSettings,3);
   //#endif 
    
    if(MVCApp.Toolbox.common.getRegion()=="US"){
      var ccpaLink = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.CCPA.link")
      frmSettings.flexCCPALink.isVisible= true;
      frmSettings.btnCCPALink.onClick =   function(){ 
         kony.application.openURL(ccpaLink); 
      }

    }else{
      frmSettings.flexCCPALink.isVisible= false;

    }
  frmSettings.show();
 } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   
  function bindEvents(){
    frmSettings.preShow = function(){
      var appVersion = "";
      if(appConfig.secureurl && appConfig.secureurl.indexOf('-') > 0){
        appVersion = appConfig.secureurl.substring(appConfig.secureurl.indexOf('-')+1, appConfig.secureurl.indexOf('.')).toUpperCase() + " - ";
      }
      appVersion += appConfig.appVersion;
      frmSettings.lblAppVersion.text = appVersion;
    };
    frmSettings.postShow = function(){
      MVCApp.Toolbox.common.destroyStoreLoc();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Michaels Mobile App: Settings";
      lTealiumTagObj.page_name = "Michaels Mobile App: Settings";
      lTealiumTagObj.page_type = "user info";
      lTealiumTagObj.page_category_name = "Settings";
      lTealiumTagObj.page_category = "Settings";
      TealiumManager.trackView("Settings Screen", lTealiumTagObj);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      MVCApp.Customer360.sendInteractionEvent("Settings", cust360Obj);
    };
  frmSettings.btnSignout.onClick = onClickSignOut;
  frmSettings.btnHeaderLeft.onClick = function(){
    if(gblCreateAccountRegioinChangeFlag){
      gblCreateAccountRegioinChangeFlag = false;
      frmCreateAccount.show();
    }else{
     //#ifdef android
      MVCApp.Toolbox.common.setTransition(frmMore,2);  	
   //#endif
   MVCApp.getMoreController().load(); 
   frmMore.show();
    
    }
  };
  frmSettings.btnTermsandConditions.onClick = function(){
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.createTermsandConditions);
  };
  frmSettings.btnPrivacyPolicy.onClick = function(){
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
  };
  frmSettings.btnRegionLanguage.onClick=MVCApp.getRegionChangeController().load;  
  MVCApp.tabbar.bindEvents(frmSettings);
  MVCApp.tabbar.bindIcons(frmSettings,"frmMore");
  frmSettings.switchTouch.onTouchEnd = slideTouch; 
  frmSettings.pushSwitch.onTouchEnd = slidePush;
  
  }
  function slideTouch(){
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    if(frmSettings.switchTouch.src == "on.png"){
      frmSettings.switchTouch.src = "off.png";
      kony.store.setItem("switch", false);
      cust360Obj.touchid="disabled";
    }else{
      frmSettings.switchTouch.src = "on.png";
      kony.store.setItem("switch", true);
      cust360Obj.touchid="enabled";
    }
    MVCApp.Customer360.sendInteractionEvent("TouchIdToggle", cust360Obj);  
  }
  function decideOnNotificationToggles(response){
   // alert("decide ONNotificationToggles:"+response);
    if(response){
      //#ifdef iphone
      redirectToSettings.appSpecificLocationSettings();
      //#else
      var KonyMain = java.import('com.konylabs.android.KonyMain');
      var activityContext = KonyMain.getActivityContext();
      var Intent = java.import('android.content.Intent');
      var Settings = java.import('android.provider.Settings');
      var notificationIntent = new Intent();
      notificationIntent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
      var Build = java.import('android.os.Build');
      if (!Build.VERSION_CODES.O || Build.VERSION.SDK_INT < Build.VERSION_CODES.O){
        notificationIntent.putExtra("app_package", activityContext.getPackageName());
        notificationIntent.putExtra("app_uid", activityContext.getPackageManager().getApplicationInfo(activityContext.getPackageName(), 0).uid);
      }else{
        notificationIntent.putExtra("android.provider.extra.APP_PACKAGE", activityContext.getPackageName());
      }
      activityContext.startActivity(notificationIntent);
      //#endif
    }
  }
  function slidePush(){
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes() ;
    if(frmSettings.pushSwitch.src == "on.png"){
	  MVCApp.PushNotification.common.unsubscribeMFMessaging();
      MVCApp.PushNotification.common.toggleSFMCPushMessaging(false);
	}else{
      //var gnFlag=kony.store.getItem("gblNotificationNotNowFlag");
      //alert("gblNotificationNotNowFlag:"+gnFlag);
      if(kony.store.getItem("gblNotificationNotNowFlag")){
        kony.store.setItem("gblNotificationNotNowFlag",false);
        //MVCApp.Toolbox.common.showLoadingIndicator("");
        MVCApp.PushNotification.common.registerPush();
      }
      //#ifdef android
      else if(dummyTokenRegistration){
        dummyTokenRegistration = false;
        gblAndroidPushSettingControlFlag = true;
        MVCApp.Toolbox.common.showLoadingIndicator("");
        MVCApp.PushNotification.common.registerPush();
      }
      //#endif
      else{ 
        var checkNotificationFlag = false;
        //#ifdef iphone
          if(redirectToSettings.checkNotificationSettingsStatus()){
            checkNotificationFlag = true;
                  
           var pushNativeId = kony.store.getItem("pushnativeid");
            if(pushNativeId === null || kony.string.len(pushNativeId)<1){
              MVCApp.PushNotification.common.registerPush();
            }
          }
        //#else
          gblAndroidPushSettingControlFlag = true;
          var KonyMain = java.import('com.konylabs.android.KonyMain');
          var activityContext = KonyMain.getActivityContext();
          var notificationManagerCompat = java.import('android.support.v4.app.NotificationManagerCompat');
          if(notificationManagerCompat && notificationManagerCompat.from(activityContext).areNotificationsEnabled()){
            checkNotificationFlag = true;
          }
        //#endif
        //alert("checkNotificationFlag:"+checkNotificationFlag);
        if(checkNotificationFlag){
          MVCApp.PushNotification.common.subscribeMFPushMessaging();
          MVCApp.PushNotification.common.toggleSFMCPushMessaging(true);
        }else{
          MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.notifications.directToSettings"),"",constants.ALERT_TYPE_CONFIRMATION,decideOnNotificationToggles,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.notifications.takeMetoNotificationSettings"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.notifications.StayHereNotifications"));
        }
        cust360Obj.appNotifications="enabled";

      }
    }
    MVCApp.Customer360.sendInteractionEvent("NotificationToggle", cust360Obj);
  } 
  
  function updateRegionLang(){
    var langReg=kony.store.getItem("languageSelected");
    if(langReg=="en"){
      frmSettings.lblRegionLanguageValue.text="United States";
    }else if(langReg=="en_CA"){
      frmSettings.lblRegionLanguageValue.text="Canada-English";
    }else if(langReg=="fr_CA"){
      frmSettings.lblRegionLanguageValue.text="Canada - français";
    }
  }
  function updateScreen(results){
    updateRegionLang();
    show();
  }
  
  function onClickSignOut(){
    
   
     MVCApp.getSettingsController().logout();
  }
  function logout(results){
   
    sfmcCustomObject.Indiv_Id = "";
   if(OneManager){
     //#ifdef iphone
     OneManager.sfmcUpdateCustomAttributes(sfmcCustomObject);
     //#else
     OneManager.sendAttributes(JSON.stringify(sfmcCustomObject));
     //#endif
   }
     kony.store.removeItem("userObj");
    kony.store.setItem("userObj", "");
    setUserDetailsForTealiumTagging(kony.store.getItem("userObj"));
    rewardsInfoFlag = false;
    fromMoreFlagRewards = false;
    isFromShoppingList = false;
    TealiumManager.trackEvent("Sign Out click From Settings Page", {conversion_category:"Settings",conversion_id:"Sign Out",conversion_action:2});    
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    MVCApp.Customer360.sendInteractionEvent("SignOut", cust360Obj);
    MVCApp.getSignInController().load();
     MVCApp.Toolbox.common.signOutUSer();
  }
  
  
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	logout:logout
    };
  
});



