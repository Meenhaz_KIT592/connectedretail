//Type your code here
function setStoreMode(){
  var region = MVCApp.Toolbox.common.getRegion();
  var reportData = {"region":region,"storeModeAccepted":true};
  var lCurrentForm = kony.application.getCurrentForm();
  stopForegroundStoreModeTimer();
  stopBackgroundStoreModeTimer();
  try{
    var inStoreModeExitThresholdSeconds = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
    kony.timer.schedule("StoreModeTimer", verifyStoreModeExtension, inStoreModeExitThresholdSeconds, false);
  }catch(e){
    kony.print("exception in invoking timer "+ JSON.stringify(e));
    if(e){
      TealiumManager.trackEvent("Timer Exception While Setting The Store Mode", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  }
  refreshPagesWithNewStoreSpecificData(true);
  MVCApp.sendMetricReport(frmHome,[{"StoreMode_Enable":JSON.stringify(reportData)}]);
  gblTealiumTagObj.instore_mode = true;
}

function verifyStoreModeExtension(){
  if(MVCApp.Toolbox.common.checkIfNetworkExists()){
    gblInStoreModeDetails.storeModeExtensionFlag = true;
    checkWiFiAndGeoFenceForStoreMode();
  }else{
    exitStoreMode();
  }
}

function exitStoreMode(){
  try{
    kony.timer.cancel("StoreModeTimer");
  }catch(e){
    if(e.errorCode == 102){
      kony.print("Timer Cancel: StoreModeTimer is already cancelled -----> " + JSON.stringify(e));
    }
    if(e){
      TealiumManager.trackEvent("Timer Exception While exiting The Store Mode", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  }
  kony.store.setItem("beacon_array", []);
  kony.store.removeItem("inStoreModeDetails");
  kony.store.removeItem("macIP");
  gblInStoreModeDetails = {isInStoreModeEnabled:false, storeModeCounter:1};
  if(appInForeground){
    startForegroundStoreModeTimer();
  }else{
    startBackgroundStoreModeTimer();
  }
  refreshPagesWithNewStoreSpecificData(false);
  gblTealiumTagObj.instore_mode = false;
}

function setStoreModeReEntry(){
  stopForegroundStoreModeTimer();
  stopBackgroundStoreModeTimer();
  var inStoreModeExitThresholdSeconds = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
  try{
    var d = new Date();
  	var currTime = Math.round(d.getTime()/1000);
    kony.timer.schedule("TempStoreModeTimer", verifyStoreModeExtension, (inStoreModeExitThresholdSeconds - (currTime - gblInStoreModeDetails.entryTimestamp)), false);
  }catch(e){
    kony.print("exception in invoking timer "+ JSON.stringify(e));
    if(e){
      TealiumManager.trackEvent("Timer Exception While Reentering The Store Mode", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  }
}

function refreshPagesWithNewStoreSpecificData(pIsInStoreModeEnabled){
  if(!imageSearchOverlayFlag && !imageSearchProgressFlag){
  var lServiceRequestObj = {};
  var lPreviousForm = "";
  if(kony.application.getPreviousForm()){
    lPreviousForm = kony.application.getPreviousForm().id;
  }
  if(kony.application.getCurrentForm().id === "frmHome"){
    initLaunchFlag = false;
    getProductsCategory();
  }else if(kony.application.getCurrentForm().id === "frmPDP"){
    lServiceRequestObj = MVCApp.getPDPController().lServiceRequestObj;
    MVCApp.getPDPController().load(lServiceRequestObj.productId,lServiceRequestObj.varientFlag,lServiceRequestObj.varientChangeFlag,lServiceRequestObj.headerTxt,lServiceRequestObj.formName,lServiceRequestObj.defaultVariantID,lServiceRequestObj.firstEnry,lServiceRequestObj.fromBack);
  }else if(kony.application.getCurrentForm().id === "frmProductsList"){
    if(gblIsFromImageSearch){
      lServiceRequestObj = MVCApp.getImageSearchController().lPLPServiceRequestObj;
      MVCApp.getImageSearchController().getPLPDataForImageSearch(lServiceRequestObj);
    }else{
      lServiceRequestObj = MVCApp.getProductsListController().lServiceRequestObj;
      MVCApp.getProductsListController().load(lServiceRequestObj.query,lServiceRequestObj.fromSearch,lServiceRequestObj.sortValue,lServiceRequestObj.refStringCgid,lServiceRequestObj.fromSort,lServiceRequestObj.fromBack,lServiceRequestObj.fromProdCat1);
    }
  }else if(kony.application.getCurrentForm().id === "frmProductCategoryMore"){
    lServiceRequestObj = MVCApp.getProductCategoryMoreController().lServiceRequestObj;
    MVCApp.getProductCategoryMoreController().load(lServiceRequestObj.selectedSubCategory);
  }else if(kony.application.getCurrentForm().id === "frmProductCategory"){
    lServiceRequestObj = MVCApp.getProductCategoryController().lServiceRequestObj;
    MVCApp.getProductCategoryController().load(lServiceRequestObj.selectedCategory);
  }else if(kony.application.getCurrentForm().id === "frmPJDP"){
    lServiceRequestObj = MVCApp.getPJDPController().lServiceRequestObj;
    MVCApp.getPJDPController().load(lServiceRequestObj.productId,lServiceRequestObj.fromPJDPFlag,lServiceRequestObj.formName);
  }else if(kony.application.getCurrentForm().id === "frmProjectsList"){
    if(gblIsFromImageSearch){
      lServiceRequestObj = MVCApp.getImageSearchController().lPJLPServiceRequestObj;
      MVCApp.getImageSearchController().getPJLPDataForImageSearch(lServiceRequestObj);
    }else{
      lServiceRequestObj = MVCApp.getProjectsListController().lServiceRequestObj;
      MVCApp.getProjectsListController().load(lServiceRequestObj.query,lServiceRequestObj.fromSearch,lServiceRequestObj.fromRefine,lServiceRequestObj.refineString);
    }
  }else if(kony.application.getCurrentForm().id === "frmShoppingList"){
    lServiceRequestObj = MVCApp.getShoppingListController().lServiceRequestObj;
    MVCApp.getShoppingListController().getProductListItems(lServiceRequestObj.fromBack);
  }else if(kony.application.getCurrentForm().id === "frmMore"){
    MVCApp.getMoreController().load();
  }else if(kony.application.getCurrentForm().id === "frmWeeklyAdHome"){
    MVCApp.getWeeklyAdHomeController().load();
  }else if(kony.application.getCurrentForm().id === "frmWeeklyAd" || kony.application.getCurrentForm().id === "frmWeeklyAdDetail"){
    alertUserAboutPageRefresh(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.entryExitMessageForWeeklyAds"));
  }else if(kony.application.getCurrentForm().id === "frmCoupons"){
    kony.print("@@@LOG:refreshPagesWithNewStoreSpecificData-frmCoupons.flxCouponDetailsPopup.isVisible:"+frmCoupons.flxCouponDetailsPopup.isVisible);
    if(frmCoupons.flxCouponDetailsPopup.isVisible){
      alertUserAboutPageRefresh(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.entryExitMessageForCoupons"));
    }else{
      gblNewBrightnessValueStoreMode = gblBrightNessValue;
      gblIsNewBrightnessValuePresent = true;
      MVCApp.getHomeController().loadCoupons();
    }
  }else if(kony.application.getCurrentForm().id === "frmFindStoreMapView"){
    if(pIsInStoreModeEnabled){
      initLaunchFlag = false;
      getProductsCategory();
    }
  }else if(kony.application.getCurrentForm().id === "frmEvents"){
    if(lPreviousForm == "frmHome"){
      MVCApp.getEventsController().load(false);
    }else{
      MVCApp.getEventsController().load(true);
    }
  }
  else if(kony.application.getCurrentForm().id === "frmEventDetail"){
    MVCApp.getEventDetailsController().loadInStoreModeEventDetails();
  } 
  }
}

function alertUserAboutPageRefresh(pMessage){
  MVCApp.Toolbox.common.customAlert(pMessage,"",constants.ALERT_TYPE_INFO,alertUserAboutPageRefreshCallback,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),"");
}

function alertUserAboutPageRefreshCallback(pResponse){
  if(pResponse){
    if(kony.application.getCurrentForm().id === "frmWeeklyAd" || kony.application.getCurrentForm().id === "frmWeeklyAdDetail"){
      MVCApp.getWeeklyAdHomeController().load();
    }else if(kony.application.getCurrentForm().id === "frmCoupons"){
      gblNewBrightnessValueStoreMode = gblBrightNessValue;
      gblIsNewBrightnessValuePresent = true;
      MVCApp.getHomeController().loadCoupons();
    }
  }
}