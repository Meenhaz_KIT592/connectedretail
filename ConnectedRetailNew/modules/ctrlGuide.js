var MVCApp = MVCApp || {};
MVCApp.GuideController = (function(){

  var _isInit = false;
  var model=null;
  var view =null;
  function init(theme,locale){
    if (_isInit===true) return;
    view = new MVCApp.GuideView(); 
    view.bindEvents();
    _isInit = true;
  }

  function load(){
    init();
    view.show();
  }
function guideFourDisplay()
  {
    view.guideFourDisplay();
  }
  function guideFormAnimation(animateObj){
    kony.print("in guide form animation  controller-------------------->")
    view.guideFormAnimation(animateObj);
  } 
  return {
    init: init,
    load: load,
    guideFourDisplay:guideFourDisplay,
    guideFormAnimation:guideFormAnimation
  };
});