
/**
 * PUBLIC
 * This is the controller for the Scan form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.SortProjectController = (function(){

  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction

  /**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
  function init(theme,locale){
    if (_isInit===true) return;

    //Enables us to use different UI interactions depending on form factor
    view = new MVCApp.RefineProjectsView();
    //}

    //Bind events to UI
    view.bindEvents();

    _isInit = true;
  }
  /**
	 * PUBLIC
	 * Open the form.
	 */
  function load(){
    kony.print("Refine Projects controller.load");
    init();
    MVCApp.getProjectsListController().setFromRefineFlag(true);
    var reqParams=MVCApp.getProjectsListController()._getRequestParams();
    setDefaultParams(reqParams);
    view.showProjSortPage(true);
    
  }
  var projListObj={};
  function setDataForProjList(value){
    projListObj=value;
  }
  function getDataForProjList(){
    return projListObj;
  }
  function loadNext(dataObj){
    kony.print("Refine Projects controller.loadNext");
    setDataForProjList(dataObj);
    view.showProjSortPage(false);
  }
  var defaultParams={};
  var skillLabel="";
  var craftLabel="";
  var skillValues=[];
  var craftValues=[];
  var prodCount=0;
  var selectedRefinements="";
  var selectedRefexcludingCgid=[];
  
  function setDefaultParams(value){
    defaultParams=value;
  }
  function getDefaultParams(){
    return defaultParams;
  }
  function goToProductList(){
    var query=getEntryQueryParam();
    var fromSearch=true;
    var refStringCgid="refine=cgid=Categories";
    var fromSort=false;
    MVCApp.getProductsListController().load(query,fromSearch,"",refStringCgid,true);
  }
  function gotToProjectsList(){
    var projData=getDataForProjList();
    var fromSearch=MVCApp.getProjectsListController()._checkIfFromSearch();
    frmProjectsList.segProjectList.removeAll();
    frmProjectsList.flxRefineSearchWrap.isVisible = false;//flxCategory
    frmProjectsList.flxCategory.isVisible = false;//flxCategory
   	frmProjectsList.flxSearchResultsContainer.setVisibility(false);
    frmProjectsList.show();
    MVCApp.getProjectsListController().loadListWOService(projData);
  }
  
  function loadDefaultProjList(){
  	var requestParams= getDefaultParams();
    var fromSearch=MVCApp.getProjectsListController()._checkIfFromSearch();
    var query=requestParams.q;
    var refineString=requestParams.refineString;
    frmProjectsList.segProjectList.removeAll();
    frmProjectsList.flxRefineSearchWrap.isVisible = false;//flxCategory
    frmProjectsList.flxCategory.isVisible = false;//flxCategory
   	frmProjectsList.flxSearchResultsContainer.setVisibility(false);
    gblIsFromImageSearch = false;
    imageSearchProjectBasedFlag = false;
    frmProjectsList.show();
    MVCApp.getProjectsListController().load(query,fromSearch,true,refineString);
  }
  
  function setSkillLabel(value){
    skillLabel=value;
  }
  function setSkillValues(values){
    skillValues=values;
  }
  function setCraftValues(values){
    craftValues=values;
  }
  function setCraftLabel(value){
    craftLabel=value;
  }
  function getSkillLabel(){
    return skillLabel;
  }
  function getSkillValues(){
    return skillValues;
  }
  function getCraftLabel(){
    return craftLabel;
  }
  function getCraftValues(){
    return craftValues;
  }
  function getProductCount(){
	return prodCount;
  }
  function setProductCount(value){
    prodCount=value;
  }
  function getSelectedRefinements(){
    return selectedRefinements;
  }
  function setSelectedRefinements(value){
    selectedRefinements=value;
    var selRef=selectedRefinements;
    if(selRef!=""){
      try {
        var selRefObj=JSON.parse(selRef);
        selectedRefexcludingCgid=[];
        for(var key in selRefObj){
          if(key !== "cgid"){
            var refValueStr=selRefObj[key];
            var refValueArr=refValueStr.split("|");
            var temp={attr:key,refValues:refValueArr};
            selectedRefexcludingCgid.push(temp);
          }else{
            setSelectedCategory(selRefObj[key]);
          }
        }
        setRefinementsExcludingCgid(selectedRefexcludingCgid);
      } catch (e) {
        MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlSortProjects.js on setSelectedRefinements for selRef:" + JSON.stringify(e)}]);
        if(e){
          TealiumManager.trackEvent("Selected Refinement Parse Exception in Sort Projects Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
    }  
  }
  
  function setRefinementsExcludingCgid(value){
    selectedRefexcludingCgid=value;
  }
  
  function getRefinementsExcludingCgid(){
    return selectedRefexcludingCgid;
  }
  var selectedCategory="";
  var entryQueryParam="";
  function getSelectedCategory(){
    return selectedCategory;
  }
  function getEntryQueryParam(){
    return entryQueryParam;
  }
  function setSelectedCategory(value){
    selectedCategory=value;
  }
  function setEntryQueryParam(value){
    entryQueryParam=value;
  }
  function prepareInputParams(){
    var query=getEntryQueryParam();
    var category=getSelectedCategory();
    var refStringCgid="";
    var allRefineComponenets=getRefinementsExcludingCgid();
    if(allRefineComponenets.length!==0){
      refStringCgid="refine_1=cgid="+category;
      for(var i=0;i<allRefineComponenets.length;i++){
        var temp=allRefineComponenets[i];
        var attribute=temp.attr||"";
        var refineSelected=temp.refValues;
        var refineStringForAttr="";
        for(var j=0;j<refineSelected.length;j++){
          if(j+1==refineSelected.length){
            refineStringForAttr+=refineSelected[j];
          }else{
            refineStringForAttr+=refineSelected[j]+"%7C";
          }
        }
          refStringCgid+="&refine_"+(i+2)+"="+attribute+"="+refineStringForAttr;
      }
    }else{
      refStringCgid="refine=cgid="+category;
    }
    return{
      query:query,
      category:category,
      refineString:refStringCgid
    };
  }
  
  function prepareForJustServiceCall(){
    var params=prepareInputParams();
    var query=params.query;
    var category=params.category;
    var refineString=params.refineString;
    refineString=refineString.replace(/ /g,"%20");
    MVCApp.getProjectsListController().loadForSortRefine(query,refineString);
  }
  
  function clearFilter(){
    var category=getSelectedCategory();
    var refineString="refine=cgid="+category;
    var query=getEntryQueryParam();
    var valueRefineMents=[];
    view.clearAllGlobals();
    MVCApp.getProjectsListController().loadForSortRefine(query,refineString);
  }


  /**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

  //Here we expose the public variables and functions
  return {
    init: init,
    load: load,
    getProductCount:getProductCount,
    setProductCount:setProductCount,
    getCraftValues:getCraftValues,
    getCraftLabel:getCraftLabel,
    getSkillValues:getSkillValues,
    getSkillLabel:getSkillLabel,
    setCraftValues:setCraftValues,
    setCraftLabel:setCraftLabel,
    setSkillValues:setSkillValues,
    setSkillLabel:setSkillLabel,
    goToProductList:goToProductList,
    getSelectedRefinements:getSelectedRefinements,
    setSelectedRefinements:setSelectedRefinements,
    getRefinementsExcludingCgid:getRefinementsExcludingCgid,
	setRefinementsExcludingCgid:setRefinementsExcludingCgid,
    gotToProjectsList:gotToProjectsList,
    clearFilter:clearFilter,
    setEntryQueryParam:setEntryQueryParam,
    prepareForJustServiceCall:prepareForJustServiceCall,
    loadNext:loadNext,
    setDataForProjList:setDataForProjList,
    loadDefaultProjList:loadDefaultProjList,
    setDefaultParams:setDefaultParams
  };
});

