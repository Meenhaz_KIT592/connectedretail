
/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.JoinRewards = (function(json,agree,email){

  var typeOf = "JoinRewards";
  var _phNo = json;
  var _agree = agree;
  var _email = email;
  
  /**
	 * PUBLIC - setter for the model
	 * Set data from the JSON retrieved by the model
	 */
   
  
  function getPhoneNo(){  
    if(_phNo!="null"&&_phNo.length==10){
      if(_agree!="null" &&_agree == "checkbox_on.png")
        return _email;
      else{
        alert("Please accept terms and conditions.");        
        return "null";}
    }      
    else{
        alert("Phone number can't be null or Invalid format.");
        return "null";}   
  }
  
  
  
  //Here we expose the public variables and functions
  return {
    typeOf: typeOf,
    getPhoneNo:getPhoneNo,
   };
});


