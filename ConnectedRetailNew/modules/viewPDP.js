/**
 * PUBLIC
 * This is the view for the PDP form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var productIDForReview = "";

var MVCApp = MVCApp || {};
MVCApp.PDPView = (function() {
  var allImagesGroup = [];
  var varientsLength;
  var dataPDPObj = {};
  var prevFormName = "";
  var productsData = [];
  var products = 0;
  var recCount = 0;
  var ProjectsData = [];
  var projects = 0;
  var projCount = 0;
  var geoFlag = false;
  var defaultColor = "";
  var defaultSize = "";
  var aisleNumber = "";
  var gblStockInfo="";
  var aisleFlag;
  var defaultProductName = "";
  var shoppingListProductID = "";
  var defaultCount= "";
  var isVariantSelected = true;
  var storeIDPast="";
  var checkStoreClicked=false;
  var selectedImageIndex = 0;
  var isBuyOnlineDisablityFlagTrue = false;
  var isMultiVariantCombinationPresent = false;
  
  var lIsTealiumTagDispatchNotDone = true;
  var lProdResultsObj = {};
  function setProductResults(pProdResultsObj){
    lProdResultsObj = pProdResultsObj;
  }
  function getProductResults(){
    return lProdResultsObj;
  }
  
  function setMultiVariantCombinationPresentFlag(pIsMultiVariantCombinationPresent){
    isMultiVariantCombinationPresent = pIsMultiVariantCombinationPresent;
  }
  function getMultiVariantCombinationPresentFlag(){
    return isMultiVariantCombinationPresent;
  }
  function setBuyOnlineDisablityFlag(pIsBuyOnlineDisablityFlagTrue){
    isBuyOnlineDisablityFlagTrue = pIsBuyOnlineDisablityFlagTrue;
  }
  function getBuyOnlineDisablityFlag(){
    return isBuyOnlineDisablityFlagTrue;
  }
	/**
	 * PUBLIC
	 * Open the PDP form
	 */
  	function defaultPDP(){
      frmPDP.flexSeg.setVisibility(false);
      frmPDP.flxHeaderWrap.textSearch.text = MVCApp.getPDPController().getQueryString();
      //frmPDP.flxHeaderWrap.textSearch.setFocus(false);
	  frmPDP.flxSearchResultsContainer.setVisibility(false);
	  //#ifdef android
      var text=frmPDP.flxHeaderWrap.textSearch.text||"";
      if(text===""){
        frmPDP.flxHeaderWrap.btnClearSearch.setVisibility(false);
      }else{
        frmPDP.flxHeaderWrap.btnClearSearch.setVisibility(false);
      }
	  //#endif 
       var formName =MVCApp.getPDPController().getFormName();
     if( formName!=="frmChatBox")
     {
        frmPDP.flxChatBoxTitleContainer.setVisibility(false);
       	frmPDP.flxCategory.setVisibility(true);
     }
      else
        {
           frmPDP.flxChatBoxTitleContainer.setVisibility(true);
           frmPDP.flxCategory.setVisibility(false);
        }
      frmPDP.show();
      //#ifdef iphone
      frmPDP.flxHeaderWrap.textSearch.setFocus(false);
      //#endif
    }
	function showBackBtn() {
    var storeID=MVCApp.getPDPController().getStoreID();
      if(storeID!=storeIDPast && checkStoreClicked===true){
        var tempData = "";
        var formName = "";
    	tempData = kony.store.getItem((1).toString());
        tempData = tempData + "";
        try {
          if(tempData){
            tempData = JSON.parse(tempData);
            formName = tempData.formName;
          }
          if(formName!=="frmChatBox"){
            frmPDP.flexSeg.top = "96dp";
            frmPDP.flxCategory.setVisibility(true);
            frmPDP.flxChatBoxTitleContainer.setVisibility(false);
          }else{
            frmPDP.flexSeg.top = "54dp";
            frmPDP.flxCategory.setVisibility(false);
            frmPDP.flxChatBoxTitleContainer.setVisibility(true);
          }
          checkStoreClicked=false;
        } catch(e) {
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPDP.js on showBackBtn for tempData:" + JSON.stringify(e)}]);
          var pdpBar = MVCApp.getPDPController().getFromBarcode();
          if (pdpBar) {
              frmPDP.flexSeg.top = "54dp";
              frmPDP.flxCategory.setVisibility(false);
          } else {
              frmPDP.flexSeg.top = "96dp";
              frmPDP.flxCategory.setVisibility(true);
          }
          if(e){
            TealiumManager.trackEvent("TempData Parse Exception In PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }else{
        var pdpBar = MVCApp.getPDPController().getFromBarcode();
		if (pdpBar) {
			frmPDP.flexSeg.top = "54dp";
			frmPDP.flxCategory.setVisibility(false);
		} else {
			frmPDP.flexSeg.top = "96dp";
			frmPDP.flxCategory.setVisibility(true);
		}
      }
		//#ifdef android
		frmPDP.flxMainContainer.forceLayout();
        //#endif
	}

	function show() {
    var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
    frmPDP.flxHeaderWrap.lblItemsCount.text=cartValue;
      var formName =MVCApp.getPDPController().getFormName();
     if( formName!=="frmChatBox")
     {
        frmPDP.flxChatBoxTitleContainer.setVisibility(false);
     }
      else
        {
           frmPDP.flxChatBoxTitleContainer.setVisibility(true);
        }
      	frmPDP.flexSeg.setVisibility(true);
		frmPDP.flxSearchResultsContainer.setVisibility(false);
		//#ifdef android
		MVCApp.Toolbox.common.setTransition(frmPDP, 3);
		//#endif    
		frmPDP.flxPinchandZoom.setVisibility(false);
		frmPDP.flexSeg.contentOffset = {
			x: 0,
			y: 0
		};
      	try{
         frmPDP.flxFooterWrap.isVisible = true; 
        }catch(e){
          kony.print(e);
          if(e){
            TealiumManager.trackEvent("UI Exception While Displaying PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
		frmPDP.show();
		showBackBtn();
		addToShoppingList();
      	//#ifdef iphone
        MVCApp.Toolbox.common.requestReview();
        //#endif
      //#ifdef android
      if(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.reviewSwitch")==="true"){
        TwoStageRate.incrementEvent();
      }
      //#endif
	}
	function showPDP(){
		frmPDP.flxSearchResultsContainer.setVisibility(false);
		//#ifdef android
		MVCApp.Toolbox.common.setTransition(frmPDP,2);
		//#endif    
		frmPDP.flxPinchandZoom.setVisibility(false);
		frmPDP.flexSeg.contentOffset = {
			x: 0,
			y: 0
		};
      	try{
         frmPDP.flxFooterWrap.isVisible = true; 
        }catch(e){
          kony.print(e);
          if(e){
            TealiumManager.trackEvent("UI Exception While Showing PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        } 
		frmPDP.show();
  
       showShoppingAt();
    }
	function isEmpty(record) {
		if (record !== undefined && record !== null) {
			return true;
		}
		return false;
	}
   function onPreShowOfForm(){
     frmPDP.flxSearchOverlay.setVisibility(false);
   }
	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
	function savePreviousScreenForm() {
      MVCApp.Toolbox.common.destroyStoreLoc();
		var prevFormObj = kony.application.getPreviousForm();
      	var prevForm = "";
      if(prevFormObj!== null && prevFormObj!==undefined){
        prevForm = prevFormObj.id;
      }
		if (prevForm == "frmProductsList") {
			prevFormName = prevForm;
		} else if (prevForm == "frmPJDP") {
			prevFormName = prevForm;
		}
    	MVCApp.Toolbox.common.setBrightness("frmPDP");
      
      var lPrevFormName = "";
      if(kony.application.getPreviousForm()){
        lPrevFormName = kony.application.getPreviousForm().id;
      }
      var lProdResults = getProductResults();
      if(typeof lProdResults.getProductId === "function" && lIsTealiumTagDispatchNotDone){
        var lPageCategoryName = "";
        if(!frmPDP.flxHeaderWrap.textSearch.text && (lPrevFormName === "frmProductCategoryMore" || lPrevFormName === "frmProductCategory" || lPrevFormName === "frmProductsList")){
          lPageCategoryName = MVCApp.getPDPController().getPageCategoryNameForPDP();
        }
        var lTealiumTagObj = gblTealiumTagObj;
        lTealiumTagObj.page_id = "Product:"+frmPDP.lblName1.text;
        lTealiumTagObj.page_name = "Product:"+frmPDP.lblName1.text;
        lTealiumTagObj.page_type = "Product";
        lTealiumTagObj.page_category_name = lPageCategoryName;
        lTealiumTagObj.page_category = lPageCategoryName;
        lTealiumTagObj.page_subcategory_name = frmPDP.btnCategoryName.text;
        lTealiumTagObj.product_id = lProdResults.getProductId();
        lTealiumTagObj.product_sku = lProdResults.getSKUId();
        lTealiumTagObj.product_category = lProdResults.getPrimaryCategoryId();
        lTealiumTagObj.product_category_ID = lProdResults.getPrimaryCategoryId();
        lTealiumTagObj.category_ID = lProdResults.getPrimaryCategoryId();
        lTealiumTagObj.product_name = frmPDP.lblName1.text;
        lTealiumTagObj.product_brand = lProdResults.getBrand();
        lTealiumTagObj.product_subcategory = frmPDP.btnCategoryName.text;
        lTealiumTagObj.product_review_number = lProdResults.getReviewsCount();
        lTealiumTagObj.product_rating = lProdResults.getRating();
        if(frmPDP.lblOnlineStock.text === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.outOfStock")){
          lTealiumTagObj.product_outofstock = true;
        }else{
          lTealiumTagObj.product_outofstock = false;
        }
        lTealiumTagObj.product_id_length = 1;
        
        TealiumManager.trackView("Product Detail Page", lTealiumTagObj);
        lIsTealiumTagDispatchNotDone = false;
        var prodId=lProdResults.getProductId();
       var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
       cust360Obj.productid=MVCApp.getPDPController().getProductIDForTracking();
       cust360Obj.entryType=MVCApp.getPDPController().getEntryType();
       if(!formFromDeeplink){
         MVCApp.Customer360.sendInteractionEvent("PDP", cust360Obj);
       }else{
         formFromDeeplink=false;
       }
      }
	}

  function showPreviousScreen() {
    var lPrevFormName = "";
    var lPrevForm = kony.application.getPreviousForm();
    if(lPrevForm){
      lPrevFormName = lPrevForm.id;
    }
    var storeID="";
    var storeIDNEW="";
    var lIsStoreModeAlreadyEnabled = false;
    var lIsStoreModeAlreadyEnabledNew = false;
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeIDNEW = gblInStoreModeDetails.storeID;
      lIsStoreModeAlreadyEnabledNew = gblInStoreModeDetails.isInStoreModeEnabled;
    }else{
      if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
        var myLocationString=kony.store.getItem("myLocationDetails") || "";
        var locationDetails=JSON.parse(myLocationString);
        storeIDNEW = locationDetails.clientkey;
      }
    }
    var indLen = pdpIndexArray.length;
    var tempData = "";
    tempData = kony.store.getItem((1).toString());
    tempData = tempData + "";
    try{
      if(tempData){
        tempData = JSON.parse(tempData);
      }
    }catch(pError){
      kony.print("@@@LOG:showPreviousScreen-pError:"+JSON.stringify(pError));
      if(pError){
        TealiumManager.trackEvent("TempData Parse Exception While Displaying The Previuos Screen Of PDP Screen", {exception_name:pError.name,exception_reason:pError.message,exception_trace:pError.stack,exception_type:pError});
      }
    }
    if(voiceSearchFlag){
      onXButtonClick(voiceSearchForm);
          if(voiceSearchForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")){
          	voiceSearchFlag = false;
            voiceSearchForm.flxVoiceSearch.setVisibility(false);
          } else {
            voiceSearchFlag = true;
            tapToCancelFlag = true;
            deviceBackFlag = true;
          }
      MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
    }
    if(null != tempData && tempData.hasOwnProperty("formName")){
      var formName = tempData.formName;
      if(formName == "frmShoppingList"){
        lIsStoreModeAlreadyEnabled = MVCApp.getShoppingListController().getStoreModeStatus();
        storeID = MVCApp.getShoppingListController().getStoreID();
      }
      if(formName == "frmProductsList"){
        lIsStoreModeAlreadyEnabled = MVCApp.getProductsListController().getStoreModeStatus();
        storeID = MVCApp.getProductsListController().getStoreId();
      }
      if(formName == "frmHome"){
        lIsStoreModeAlreadyEnabled = MVCApp.getHomeController().getStoreModeStatus();
        storeID = MVCApp.getHomeController().getStoreID();
      }
      if(formName == "frmProductCategoryMore"){
        lIsStoreModeAlreadyEnabled = MVCApp.getProductCategoryMoreController().getStoreModeStatus();
        storeID = MVCApp.getProductCategoryMoreController().getStoreID();
      }
      if(formName == "frmProjectsList"){
        lIsStoreModeAlreadyEnabled = MVCApp.getPJDPController().getStoreModeStatus();
        storeID = MVCApp.getPJDPController().getStoreID();
      }
      if(formName.indexOf("skuSearch")>-1){
        storeID=gblStoreForSkuSearch;
        }
       if( formName!=="frmChatBox")
     {
        frmPDP.flxChatBoxTitleContainer.setVisibility(false);
     }
      else
        {
         storeID=gblStoreForChatbot;
         lIsStoreModeAlreadyEnabled = MVCApp.getChatBoxController().getStoreModeStatus();
         frmPDP.flxChatBoxTitleContainer.setVisibility(true);
        }
      kony.print("@@@LOG:showPreviousScreen-storeID:"+storeID+"storeIDNEW:"+storeIDNEW+"indLen:"+indLen+"lIsStoreModeAlreadyEnabled:"+lIsStoreModeAlreadyEnabled+"lIsStoreModeAlreadyEnabledNew:"+lIsStoreModeAlreadyEnabledNew);
      if (indLen > 1) {
        if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
          backNavigationToforms(tempData,storeIDNEW);
        }else{
          backHistory();
        }
      } else {
        //this is being put in a fuction so so that the navigation to back can be called from multiple places
        backNavigationToforms(tempData,storeID);
      }
    }else {
      //#ifdef android
      if(gblIsFromImageSearch && lPrevFormName === "frmProductsList"){
        lPrevForm.show();
      }
      //#endif
    }
  }
  
  function backNavigationToforms(tempData,storeIDNEW){
    var formName = "";
    if(tempData){
      formName = tempData.formName;
    }
    var lIsStoreModeAlreadyEnabled = false;
    var lIsStoreModeAlreadyEnabledNew = false;
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      lIsStoreModeAlreadyEnabledNew = gblInStoreModeDetails.isInStoreModeEnabled;
    }
    if(formName == "frmChatBox"){
       MVCApp.getChatBoxController().load();
     }
    else if(formName == "frmShoppingList")
    {
      var storeID=MVCApp.getShoppingListController().getStoreID();
      lIsStoreModeAlreadyEnabled = MVCApp.getShoppingListController().getStoreModeStatus();
      if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
        MVCApp.getShoppingListController().getProductListItems(true);
      }else{
        //#ifdef android
        MVCApp.Toolbox.common.setTransition(frmShoppingList, 2);
        //#endif
        //#ifdef iphone
        frmShoppingList.defaultAnimationEnabled = true;
        MVCApp.Toolbox.common.setTransition(frmShoppingList, 3);
        //#endif
        MVCApp.getShoppingListController().getProductListItems(true);//frmShoppingList.show();
      }

    }
    else if(formName == "frmProductsList")
    {
      var storeID=MVCApp.getProductsListController().getStoreId();
      lIsStoreModeAlreadyEnabled = MVCApp.getProductsListController().getStoreModeStatus();
      if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
        if(gblIsFromImageSearch){
          MVCApp.getProductsListController().backToScreen();
          MVCApp.Toolbox.common.showLoadingIndicator("");
          lServiceRequestObj = MVCApp.getImageSearchController().lPLPServiceRequestObj;
          MVCApp.getImageSearchController().returnToFormFlag = true;
          imageSearchProgressFlag = true;
          MVCApp.getImageSearchController().getPLPDataForImageSearch(lServiceRequestObj);
        }else{
          var backParams=MVCApp.getProductsListController().getParamsForBack();
          MVCApp.getProductsListController().load(backParams.query,backParams.fromSearch,backParams.sortValue,backParams.refStringCgid,backParams.fromSort,true,true);
        }
      }else{
        //#ifdef android
        MVCApp.Toolbox.common.setTransition(frmProductsList, 2);
		//#endif
        //#ifdef iphone
        frmProducts.defaultAnimationEnabled = true;
        MVCApp.Toolbox.common.setTransition(frmProductsList, 3);
        //#endif
        if(!gblIsFromImageSearch){
          MVCApp.getProductsListController().resetTextAndClear();
        }
        frmProductsList.show();
      }
    }
    else if(formName == "frmHome")
    {
      var storeID=MVCApp.getHomeController().getStoreID();
      lIsStoreModeAlreadyEnabled = MVCApp.getHomeController().getStoreModeStatus();
      if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
        MVCApp.getHomeController().load();
      }else{
        //#ifdef android
        MVCApp.Toolbox.common.setTransition(frmHome, 2);
        //#endif
        //#ifdef iphone
        frmHome.defaultAnimationEnabled = true;
        MVCApp.Toolbox.common.setTransition(frmHome, 3);
        //#endif
        MVCApp.getHomeController().load();
      }

    }
    else if(formName == "frmProductCategoryMore")
    {
      var storeID=MVCApp.getProductCategoryMoreController().getStoreID(); 
      lIsStoreModeAlreadyEnabled = MVCApp.getProductCategoryMoreController().getStoreModeStatus();
      if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
        var selectedSubCategory=MVCApp.getProductCategoryMoreController().getSelectedSubCategory();
        MVCApp.getProductCategoryMoreController().load(selectedSubCategory);
      }else{
        frmProductCategoryMore.show();
      }
    }else if(formName == "frmProjectsList"){
      var storeID = MVCApp.getProjectsListController().getStoreID();
      lIsStoreModeAlreadyEnabled = MVCApp.getProjectsListController().getStoreModeStatus();
      if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
        var backParamsPJLP = MVCApp.getProjectsListController().getBackParams();
        var queryText = backParamsPJLP.query||"";
		frmProjectsList.segProjectList.removeAll();
        frmProjectsList.flxRefineSearchWrap.isVisible = false;
        frmProjectsList.flxCategory.isVisible = false;
        frmProjectsList.flxHeaderWrap.textSearch.text = queryText;
   		frmProjectsList.flxSearchResultsContainer.isVisible = false;
        //frmProjectsList.flxProducts.setVisibility(false);
        frmProjectsList.flxProdProjTabs.isVisible = false;
        gblIsFromImageSearch = false;
        imageSearchProjectBasedFlag = false;
        frmProjectsList.show();
        MVCApp.getProjectsListController().load(backParamsPJLP.query,backParamsPJLP.fromSearch,backParamsPJLP.fromRefine,backParamsPJLP.refineString);
      }else{
        var data1 = new MVCApp.data.PJDP(JSON.parse(tempData.data));
	  	MVCApp.getPJDPController().showUpdateScreen(data1);
      }
    }
    else if(formName.indexOf("skuSearch")>-1){
      var prevForm = "";
      try{
        prevForm = kony.application.getPreviousForm().id;
      }catch(e){
        if(e){
          kony.print("Exception in getting the previous form"+e);
        }
      }
      var tempArray= formName.split(" ");
      var tempFormName = "";
      if(tempArray.length==2)
        {
           tempFormName = tempArray[1];
        }
      if(prevForm=="frmPJDP"){
       var storeID=MVCApp.getPJDPController().getStoreID();
       var data2 = new MVCApp.data.PJDP(JSON.parse(tempData.data));
	   MVCApp.getPJDPController().showUpdateScreen(data2);
      }
      else
        {
          if(tempFormName!== "")
            {
              if(tempFormName == "frmProductCategoryMore"){
               frmProductCategoryMore.show();
              }
             else  if(tempFormName == "frmProductCategory"){
               frmProductCategory.show();
              }
            }
        }
    }
    MVCApp.Toolbox.common.clearPdpGlobalVariables();
  }

	function backHistory() {
      try {
        var formName = "";
        var tempData = MVCApp.Toolbox.common.indexArrayPopResults();
		tempData = tempData + "";
        if(tempData){
          tempData = JSON.parse(tempData);
          formName = tempData.formName;
        }
		if (tempData.id == "frmPDP") {
			kony.print("pdp page");
			var data = new MVCApp.data.PDP(JSON.parse(tempData.data));
			if (!tempData.varFlag) {
				var tempData2 = MVCApp.Toolbox.common.imagesPop(data.getProductId());
				//tempData2 = tempData2 + "";
				//tempData2 = JSON.parse(tempData2);
              	if(tempData2 === null || tempData2 === undefined || tempData2 === ""){
                  
                }else{
                 loadAllImages(tempData2); 
                }
				
			}
			if (tempData.change) {
				var prevData = new MVCApp.data.PDP(JSON.parse(tempData.prevData));
				dataPDPObj = prevData;
				var imagesGroup = prevData.getImages();
				setImagestoForm(imagesGroup);
				loadVarients(prevData);
				showShoppingAt();
			}
          MVCApp.getPDPController().setFromBarcode(tempData.barCodeFlag);
          MVCApp.getPDPController().setFormName(formName);
            data.isBrowserRequired=true;
			updateScreen(data, tempData.master, tempData.change);
			// recomme
			var tempData1 = MVCApp.Toolbox.common.recommendedProductPop();
			tempData1 = tempData1 + "";
			tempData1 = JSON.parse(tempData1);
          if(tempData1 !== null){
			updateRecommendedProducts(tempData1);
          }
			//end
		} else {
			var data1 = new MVCApp.data.PJDP(JSON.parse(tempData.data));
			MVCApp.getPJDPController().showUpdateScreen(data1);
		}
	  } catch(e) {
        MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPDP.js on backHistory for multiple possibilities:" + JSON.stringify(e)}]);
        if(e){
          TealiumManager.trackEvent("Exception While Tracking The Back History Of PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
	}

	function shopOnline() {
		var shopOnlineSkin = frmPDP.btnBuyOnline2.skin;
      	var searchEntryType=MVCApp.getPDPController().getSearchEntryType();
      	searchEntryType=MVCApp.Toolbox.common.checkForWhichSearch(searchEntryType);
		if (shopOnlineSkin === "sknBtnDisable") {
          if(!getBuyOnlineDisablityFlag()){
            promptUserToSelectValueFromVariantDropdown();
          }
		} else {
			var url = MVCApp.serviceConstants.buyOnlineLink + dataPDPObj.getProductId();
            var report_ProductID=dataPDPObj.getProductId();
            var report_primaryCategoryId=dataPDPObj.getPrimaryCategoryId();
          	var inStoreModeEnabled=false;
          	var prevForm=kony.application.getPreviousForm();
          	var prevFormID ="";
            try{
          		prevFormID=prevForm.id||"";
            }catch(e){
              if(e){
                kony.print("exception in getting previous form"+e);
              }
            }
          	var cm_mmc="";
          	if(gblInStoreModeDetails){
              inStoreModeEnabled=gblInStoreModeDetails.isInStoreModeEnabled||false;
            }
          	var reportData={"inStoreMode":inStoreModeEnabled,"productID":report_ProductID,"shopOnline":"click"};
          	if(prevFormID=="frmChatBox"){
              if(inStoreModeEnabled){
               cm_mmc="MIK_ecMobileApp-_-ChatBoxShopInstoreMode-_-"+report_primaryCategoryId+"-_-"+report_ProductID;
               //KNYMetricsService.sendEvent("Custom", null,"frmPDP", "btnBuyOnline2", null, {"inStoreMode":"true","productID":report_ProductID});
             }else{
               cm_mmc="MIK_ecMobileApp-_-ChatBoxShopOnline-_-"+report_primaryCategoryId+"-_-"+report_ProductID;
               //KNYMetricsService.sendEvent("Custom", null,"frmPDP", "btnBuyOnline2", null, {"inStoreMode":"false","productID":report_ProductID});
             }
            }else{
              if(inStoreModeEnabled){
               cm_mmc="MIK_ecMobileApp-_-ShopInstoreMode"+searchEntryType+"-_-"+report_primaryCategoryId+"-_-"+report_ProductID;
               //KNYMetricsService.sendEvent("Custom", null,"frmPDP", "btnBuyOnline2", null, {"inStoreMode":"true","productID":report_ProductID});
             }else{
               cm_mmc="MIK_ecMobileApp-_-ShopOnline"+searchEntryType+"-_-"+report_primaryCategoryId+"-_-"+report_ProductID;
               //KNYMetricsService.sendEvent("Custom", null,"frmPDP", "btnBuyOnline2", null, {"inStoreMode":"false","productID":report_ProductID});
             }
            }
          	 

            kony.print("cm_mmc::"+cm_mmc);
          
            //#ifdef iphone
     			url = decodeURI(url);
    		    url = url.replace(/%2C/g,",");
    		    url = encodeURI(url);
    		//#endif
          
			if(inStoreModeEnabled){
              var storevisitValue = gblInStoreModeDetails.storeID || "";
              if(storevisitValue!=""){
              storevisitValue = MVCApp.Toolbox.common.encodeBASE64(storevisitValue);  
              }              
                  if(prevFormID=="frmChatBox"){
                     var sessionID = MVCApp.getChatBoxController().getSession();
                     
                    //@TODO:: tempurl hardcoded for UAT purpose. Need to remove after UAT is complete.
                    // var tempurl = url.replace("www.michaels.com","dev10.michaels.com");
             		 
                    var tempurl = url;
                    MVCApp.Toolbox.common.openApplicationURL(tempurl+"&cm_mmc="+cm_mmc + "&storevisit=" + storevisitValue +"&sessionId="+sessionID);
                  }else{
             		 MVCApp.Toolbox.common.openApplicationURL(url+"&cm_mmc="+cm_mmc + "&storevisit=" + storevisitValue);
                  }
            }else{
                  if(prevFormID=="frmChatBox"){ 
                     var sessionID = MVCApp.getChatBoxController().getSession();
                    
                    //@TODO:: tempurl hardcoded for UAT purpose. Need to remove after UAT is complete.
                    // var tempurl = url.replace("www.michaels.com","dev10.michaels.com");
                    
                    var tempurl = url;
                    kony.print("FULL_URL:"+tempurl+"&cm_mmc="+cm_mmc +"&sessionId="+sessionID);
                     MVCApp.Toolbox.common.openApplicationURL(tempurl+"&cm_mmc="+cm_mmc +"&sessionId="+sessionID);
                  }else{
			         MVCApp.Toolbox.common.openApplicationURL(url+"&cm_mmc="+cm_mmc);                    
                  }
            }
          
            MVCApp.sendMetricReport(frmPDP, [{"shopOnline":"click"}]);
          	MVCApp.sendMetricReport(frmPDP,[{"shopOnlineDetails":JSON.stringify(reportData)}]);
          	//var metricsServiceObj = servicesObj.getMetricsService();
          	KNYMetricsService.flushEvents();
          	 var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
          	 var productIDForTracking= MVCApp.getPDPController().getProductIDForTracking();
      		 cust360Obj.productid=productIDForTracking;
      		 MVCApp.Customer360.sendInteractionEvent("ProductBuyOnline", cust360Obj);
          	 
          TealiumManager.trackEvent("Buy Online Button Click From Product Detail Page", {conversion_category:"Shop Online",conversion_id:"Product Page",conversion_action:2});
          //Facebook Analytics
          //FacebookAnalytics.reportEvents(productIDForTracking, "", url, "frmPDP");
          
		}
	}

	function addProductToShoppingList() {
      currentFormForPasswordExpiryCase=frmPDP;
      MVCApp.sendMetricReport(frmPDP, [{"shoppingList":"click"}]);
      var shopOnlineSkin = frmPDP.btnAddToShoppingList.skin;
      if (shopOnlineSkin === "sknBtnDisable") {
        promptUserToSelectValueFromVariantDropdown();
      } else {
        var productId = shoppingListProductID;
        var type =MVCApp.Service.Constants.ShoppingList.pdpType;
        MVCApp.getShoppingListController().addToProductList(productId,type);
      }
	}
	function animateShoppingListFooter(){
      frmPDP.flxFooterWrap.btnMyLists.animate(kony.ui.createAnimation({"20":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"-20dp"},
                                                                       "50":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"20dp"},
                                                                      "100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"0dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1.0},
      {"animationEnd": function(){
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.itemAdded"), "");
      } });
    }
	function addToProductListCallBack(prodResults) {
       results = {};
      if(prodResults) {
        results = prodResults.result || "";
        if(results !== ""  && results.length !== 0 && results!=null && results!="null") {
          try {
            results = results+"";
            results = JSON.parse(results);
            var listId = results.id;
            var productId = results.product_id;
            var flash = results._flash || [];
            kony.store.setItem("shoppingListProdOrProjID","");
            if (flash.length > 0 && flash[0].type == MVCApp.Service.Constants.ShoppingList.productAlreadyOnList) {
              MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.productAlreadyInSList"), "");
            } else {
              if(productId == shoppingListProductID) {
                animateShoppingListFooter();
                var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.productid=productId;
      			MVCApp.Customer360.sendInteractionEvent("AddProductToMyList", cust360Obj);
                var searchEntryType=MVCApp.getPDPController().getSearchEntryType();
      			searchEntryType=MVCApp.Toolbox.common.checkForWhichSearch(searchEntryType);
                if(searchEntryType!==""){
                  TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAddToList, {conversion_category:searchEntryType+"AddToMyList",conversion_id:productId,conversion_action:2});
                }else{
                  TealiumManager.trackEvent("Add to My List Button Product Page", {conversion_category:"Product Page",conversion_id:"Add to My List",conversion_action:2});
                }
              }
            }
          } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPDP.js on addToProductListCallBack for results:" + JSON.stringify(e)}]);
              MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.tryAgainLater"), "");
            if(e){
              TealiumManager.trackEvent("Exception While Adding A Product To My List In PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
		  }
        } else {
          MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPJDP.tryAgainLater"), "");
        }
      }
      MVCApp.Toolbox.common.dismissLoadingIndicator();
	}
  
function getLocatorMapLoad()
  {
    storeMapFromVoiceSearch=false;
     checkStoreClicked=true;
    MVCApp.getLocatorMapController().load("frmPDP");
  }
  	
  	function doNothing(){
      kony.print("LOG:doNothing-START");
    }
  	
  	function setOnClickEventForBtnMyStoreOrBtnChooseStore(){
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          doNothing();
        }else{
          getLocatorMapLoad();
        }
    }
  
    function onClickBtnAisleNum(){
      var aisleText = frmPDP.btnAisleNum.text;
      var params = {};
      params.aisleNo = MVCApp.Toolbox.common.getAisleNumber(aisleNumber);
      params.productName = frmPDP.lblName1.text;
      params.calloutFlag = true;
      MVCApp.sendMetricReport(frmPDP, [{"storemap":"click"}]);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      var storeId= MVCApp.Toolbox.common.getStoreID();
      cust360Obj.productid=MVCApp.getPDPController().getProductIDForTracking();
      cust360Obj.store_id=storeId;
      cust360Obj.entry_type="pdp";
      MVCApp.Customer360.sendInteractionEvent("ProductLocationInStore", cust360Obj);
      MVCApp.getStoreMapController().load(params);
      TealiumManager.trackEvent("pdp aisle tap", {conversion_category:"wayfinding",conversion_id:"map click pdp",conversion_action:2});
    }	

	function bindEvents() {
      	frmPDP.onHide = function(){
          lIsTealiumTagDispatchNotDone = true;
        };
      	frmPDP.onDeviceBack = showPreviousScreen;
      	frmPDP.btnHeaderLeft.onClick = showPreviousScreen;
		frmPDP.btnAddToShoppingList.onClick = addProductToShoppingList;
      	frmPDP.btnMyStore.onClick = setOnClickEventForBtnMyStoreOrBtnChooseStore;
      	frmPDP.btnChooseStore.onClick = getLocatorMapLoad;
		frmPDP.btnBuyOnline2.onClick = shopOnline;
		frmPDP.segProjectList.onRowClick = getSelectedProjectItem;
		frmPDP.segProductsList.onRowClick = getSelectedItem;
		frmPDP.ShowMoreProjects.onClick = showMoreProjects;
        frmPDP.preShow = onPreShowOfForm;
		frmPDP.postShow = savePreviousScreenForm;
      	frmPDP.btnCategoryName.onClick = showPreviousScreen;
		frmPDP.lblCategoryBack.onTouchEnd = showPreviousScreen;
		frmPDP.ShowMoreProducts.onClick = getProductsData;
		frmPDP.checkNearByStores.onClick = checkNearbyStorePDP;
		frmPDP.btnAisleNum.onClick = onClickBtnAisleNum;
		frmPDP.btnReview.onClick = function() {
			var productId = "";
            retainReviewFields = false;
			if (dataPDPObj.getMasterId() === "") {
				productId = dataPDPObj.getProductId();
			} else {
				productId = dataPDPObj.getMasterId();
			}
          kony.print("productId---"+productId);
            productIDForReview = productId;
			MVCApp.getReviewsController().load(productId, frmPDP.lblName1.text,frmPDP.btnReview.text);
		};
		frmPDP.btnImageZoom.onClick = loadZoomImages;
		frmPDP.btnZoom.onClick = loadZoomImages;
		frmPDP.btnShare.onClick = function() {
          	var shareURL = "";
          	var locale = kony.store.getItem("languageSelected");
          	if(locale == "en" || locale == "en_US"){
              shareURL = MVCApp.serviceConstants.shareURL;
            }else{
              shareURL = MVCApp.serviceConstants.shareCanadaURL;
            }
			var productId = dataPDPObj.getProductId();
			var shareTxt = shareURL + productId;
			share(shareTxt);
          	var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
          	cust360Obj.productid=MVCApp.getPDPController().getProductIDForTracking();
      		MVCApp.Customer360.sendInteractionEvent("ShareProduct", cust360Obj);
		};
		MVCApp.searchBar.bindEvents(frmPDP, "frmPDP");
        MVCApp.tabbar.bindEvents(frmPDP);
    	MVCApp.tabbar.bindIcons(frmPDP,"frmProducts");
		frmPDP.flexSeg.onScrollStart = onreachWidgetEnd;
      	frmPDP.flexSeg.onScrollEnd = onreachScrollEnd;
	}

	function share(shareTxt) {
       var shareArray = new Array(frmPDP.lblName1.text+"\n"+shareTxt,frmPDP.Image0f0a05398956349.src,"",frmPDP.lblName1.text);
		if (kony.os.deviceInfo().name === "android") {
			var ShareTestObject = new shareFFI.ShareTest();
         // ShareTestObject.shareSomething(shareArray);
          requestPermissionAtRuntime(kony.os.RESOURCE_EXTERNAL_STORAGE,function(){ShareTestObject.shareSomething(shareArray);});
		} else {
			var ShareIphoneObject = new shareFFI.ShareIphone();
			ShareIphoneObject.ShareonSocialMedia(
				/**Array*/
				shareArray);
		}
	}
  
	function showShoppingAt() {
		var storeString = kony.store.getItem("myLocationDetails") || "";
      	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          frmPDP.btnMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.YouAreShoppingAt");
          if(gblInStoreModeDetails.POI){
            if(gblInStoreModeDetails.POI.address2){
              frmPDP.lblStoreName.text = gblInStoreModeDetails.POI.address2;
            }else if(gblInStoreModeDetails.POI.city){
              frmPDP.lblStoreName.text = gblInStoreModeDetails.POI.city;  
            }else{
              frmPDP.lblStoreName.text = gblInStoreModeDetails.storeName;
            }
          }else{
            frmPDP.lblStoreName.text = gblInStoreModeDetails.storeName;
          }
          frmPDP.btnChooseStore.isVisible = false;
          frmPDP.lblChevron.isVisible = false;
          frmPDP.btnMyStore.isVisible = true;
          frmPDP.lblStoreName.isVisible = true;
        }else{
          if (storeString !== ""){
            try {
              frmPDP.btnMyStore.setVisibility(true);
              frmPDP.lblChevron.setVisibility(false);
              frmPDP.lblStoreName.setVisibility(true);
              frmPDP.btnChooseStore.setVisibility(false);
              var storeObj = JSON.parse(storeString);
              frmPDP.btnMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSortAndRefine.btnMyStore");
              if(storeObj.address2 !== "") {
                frmPDP.lblStoreName.text=storeObj.address2; 
              } else {
                frmPDP.lblStoreName.text=storeObj.city;
              }
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPDP.js on showShoppingAt for storeString:" + JSON.stringify(e)}]);
              frmPDP.btnChooseStore.setVisibility(true);
              frmPDP.lblStoreName.setVisibility(false);
              frmPDP.btnMyStore.setVisibility(false);
              frmPDP.lblChevron.setVisibility(true);
              if(e){
                TealiumManager.trackEvent("POI Parse Exception To Show Shopping At In PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          } else {
            frmPDP.btnChooseStore.setVisibility(true);
            frmPDP.lblStoreName.setVisibility(false);
            frmPDP.btnMyStore.setVisibility(false);
            frmPDP.lblChevron.setVisibility(true);
          }
        }
	}

	function getProjectData() {
		return ProjectsData;
	}

	function setProjectData(projData) {
		ProjectsData = projData;
	}

	function setPDPData(results) {
      frmPDP.btnAisleNum.text = "";
      gblWrap_Clip_ID ="";
      // aisle number code changes 
      //getCoStoreInventory
      var coStoreInventory = results.getCoStoreInventory();
      aisleFlag = false;
      var aisleDisplayName="";
      if (isEmpty(coStoreInventory) && coStoreInventory !== "" && coStoreInventory.length > 0) {
        coStoreInventory = coStoreInventory + "";
        coStoreInventory = JSON.parse(coStoreInventory);
        if(coStoreInventory.hasOwnProperty("wrap_clip_id")){
          aisleDisplayName=coStoreInventory["wrap_clip_id"] || ""; 
          gblWrap_Clip_ID = aisleDisplayName;   
        }
        for (var key in coStoreInventory) {
          var value = coStoreInventory[key] || "";
          if (key == "aisle" && value !== "") {
            aisleFlag = true;
            aisleNumber = value;
            if(aisleDisplayName==""){
              aisleDisplayName=value;
              var aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(aisleDisplayName);
              frmPDP.btnAisleNum.text = aisleInfo.name.toUpperCase() + " " + aisleInfo.number;
            }else{
              var aisleDisplayNameArr=aisleDisplayName.split("|");
              var aisleDisplayString="";
              for(var k=0;k<aisleDisplayNameArr.length;k++){
                var aisleInfoObj=MVCApp.Toolbox.common.getStoreMapElementName(aisleDisplayNameArr[k]);
                if(k==aisleDisplayNameArr.length-1){
                  aisleDisplayString=aisleDisplayString+aisleInfoObj.name.toUpperCase() + " " + aisleInfoObj.number;
                }else{
                  aisleDisplayString=aisleDisplayString+aisleInfoObj.name.toUpperCase() + " " + aisleInfoObj.number+",";
                }

              }
              aisleDisplayName=aisleDisplayString;
              frmPDP.btnAisleNum.text=aisleDisplayName;
            }                    
          }
        }
        if (aisleFlag) {
          frmPDP.lblOnlineStock.width = "60%";
          frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        } else {                
          frmPDP.lblOnlineStock.width = "100%";
          frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        }
      }
      
      //Code to display the Clearance and Promo tags when Store Mode is enforced 
      if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
        determineVisibilityOfPromoAndClearanceTags(results);
      }else{
        // clearance tag  c_itemtype	Clearance – Show Clearance badge
		var prices = results.getPrices() || [];
		frmPDP.flxClearanceText.setVisibility(false);
		if (isEmpty(prices) && prices.length > 0) {
			prices = prices + "";
			prices = JSON.parse(prices);
			for (var k in prices) {
				var value1 = prices[k] || "";
				if ((k == "michaels-usd-Clearance-price" || k == "michaels-usd-Clearance-prices") && value1 !== "") {
					frmPDP.flxClearanceText.setVisibility(true);
				}
			}
		}
		// free shipping text 
		frmPDP.lblFreeShipping.setVisibility(false);
		var productPromotions = results.getProductPromotions() || [];
		if (isEmpty(productPromotions) && productPromotions !== "" && productPromotions.length > 0) {
          var promotion = productPromotions[0];
          if (promotion.callout_msg !== "") {
            frmPDP.lblFreeShipping.text = promotion.callout_msg;
            frmPDP.lblFreeShipping.setVisibility(true);
          }
		}
      }
		
      // recommended projects
      frmPDP.segProjectList.setVisibility(false);
      frmPDP.lblRecommended.setVisibility(false);
      frmPDP.ShowMoreProjects.setVisibility(false);
      var cSets = results.getCsets() || [];
      if (isEmpty(cSets) && cSets !== "") {
        var onlineProjects = [];
        for(var i=0;i<cSets.length;i++){
          var onlinFlag = cSets[i].c_online || "";
          if(onlinFlag == "true" || onlinFlag=== true){
            onlineProjects.push(cSets[i]);
          }
        }
        setProjectData(onlineProjects);
      }
      //end
	}
  
  function determineVisibilityOfPromoAndClearanceTags(results){
    var lOnlinePromoDesc = "";
    var lStoreModePromoDesc = "";
    var prices = results.getPrices() || [];
    var productPromotions = results.getProductPromotions() || [];
    var coStoreInventory = results.getCoStoreInventory();
    
    frmPDP.flxClearanceText.isVisible = false;
    frmPDP.lblFreeShipping.isVisible = false;
    
    if(isEmpty(productPromotions) && productPromotions !== "" && productPromotions.length > 0) {
      var promotion = productPromotions[0];
      if (promotion.callout_msg !== "") {
        lOnlinePromoDesc = promotion.callout_msg;
      }
    }
    
    if(isEmpty(coStoreInventory) && coStoreInventory !== "" && coStoreInventory.length > 0) {
      coStoreInventory = coStoreInventory + "";
      coStoreInventory = JSON.parse(coStoreInventory);
      lStoreModePromoDesc = coStoreInventory.PromotionalDesc;
    }
    
    if(isEmpty(prices) && prices.length > 0) {
      prices = prices + "";
      prices = JSON.parse(prices);
      for(var k in prices) {
        var value1 = prices[k] || "";
        if((k == "michaels-usd-Clearance-price" || k == "michaels-usd-Clearance-prices") && value1 !== "") {
          frmPDP.flxClearanceText.isVisible = true;
        }
      }
    }
    
    //Scenario 1 : When clearance price is present and if it is what is being displayed on PDP page
    if(MVCApp.getPDPController().getIsClearancePricePresentFlag()){
      frmPDP.flxClearanceText.isVisible = true;
      MVCApp.getPDPController().setIsClearancePricePresentFlag(false);
    }
    //Scenario 2 : When promo price is present and if it is what is being displayed on PDP page
    else if(MVCApp.getPDPController().getIsPromoPricePresentFlag()){
      if(lStoreModePromoDesc){
        frmPDP.lblFreeShipping.text = lStoreModePromoDesc; 
      	frmPDP.lblFreeShipping.isVisible = true;
      }
      MVCApp.getPDPController().setIsPromoPricePresentFlag(false);
    }
    //Scenario 3 : When a product is a Dropship item and it is Online Only
    else if(MVCApp.getPDPController().getIsDropshipItemOnlineOnlyFlag()){
      if(lOnlinePromoDesc){
        frmPDP.lblFreeShipping.text = lOnlinePromoDesc; 
        frmPDP.lblFreeShipping.isVisible = true;
      }
      MVCApp.getPDPController().setIsDropshipItemOnlineOnlyFlag(false);
    }
    //Scenario 4 : When a product is a Dropship item and it is Sellable
    else if(MVCApp.getPDPController().getIsDropshipItemSellableFlag()){
      if(lStoreModePromoDesc){
        frmPDP.lblFreeShipping.text = lStoreModePromoDesc; 
      	frmPDP.lblFreeShipping.isVisible = true;
      }else{
        if(lOnlinePromoDesc){
          frmPDP.lblFreeShipping.text = lOnlinePromoDesc; 
          frmPDP.lblFreeShipping.isVisible = true;
        }
      }
      MVCApp.getPDPController().setIsDropshipItemSellableFlag(false);
    }
  }

	function addToShoppingList() {
      // ADD TO SHOPPING LIST 
      var isFromOnChangeVarient = false;
      var listVariant = false;
      var count = 0;
      var skin1 = "";
      var skin2 = "";
      frmPDP.flxInventoryLocation.isVisible = true;
      if (varientsLength > 0) {
        for (var v = 0; v < varientsLength; v++) {
          if (frmPDP["listboxColor" + v].isVisible) {
            var listData = frmPDP["listboxColor" + v].masterData;
            var selKey = frmPDP["listboxColor" + v].selectedKey;
            if (selKey === null || selKey == listData[0][0]) {
              count++;
              break;
            }
          }
        }
        if (count > 0) {
          skin1 = "sknBtnDisable";
          skin2 = "sknBtnDisable";
          isVariantSelected= false;
          frmPDP.checkNearByStores.isVisible = true;
          frmPDP.lblOnlineStock.text = "VARIES";
          frmPDP.lblOnlineStock.isVisible = true;
          frmPDP.lblOnlineStock.width = "100%";
          frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_CENTER;
          frmPDP.lblMapLocation.isVisible = false;
          frmPDP.btnAisleNum.isVisible = false; 
        } else {
          isVariantSelected= true;
          frmPDP.lblOnlineStock.text = "";
          frmPDP.lblOnlineStock.isVisible = false;
          skin1 = "sknBtnRed";
          skin2 = "sknBtnBlueBorder";
          frmPDP.lblOnlineStock.width = "60%";
          frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
          if(gblIsNotFromOnChangeVarient){
            frmPDP.lblMapLocation.isVisible = true;
            frmPDP.btnAisleNum.isVisible = true;
          }else{
            gblIsNotFromOnChangeVarient = true;
          }
        }
      } else {
        isVariantSelected=true;
        frmPDP.lblOnlineStock.text = "";
        frmPDP.lblOnlineStock.isVisible = false;
        skin1 = "sknBtnRed";
        skin2 = "sknBtnBlueBorder";
        frmPDP.lblOnlineStock.width = "60%";
        frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        frmPDP.lblMapLocation.isVisible = true;
        frmPDP.btnAisleNum.isVisible = true; 
      }
      frmPDP.btnAddToShoppingList.skin = skin1;
      frmPDP.btnAddToShoppingList.focusSkin = skin1;
      frmPDP.btnBuyOnline2.skin = skin1;
      frmPDP.btnBuyOnline2.focusSkin = skin1;
      frmPDP.checkNearByStores.skin = skin2;
      frmPDP.checkNearByStores.focusSkin = skin2;
      //END 
      if(getMultiVariantCombinationPresentFlag()){
        setMultiVariantCombinationPresentFlag(false);
        frmPDP.btnBuyOnline2.skin = "sknBtnDisable";
        frmPDP.btnBuyOnline2.focusSkin = "sknBtnDisable";
        frmPDP.flxInventoryLocation.isVisible = false;
      }
	}

  function updateScreen(results, varientFlag, varientChangeFlag) {
    frmPDP.flxVoiceSearch.setVisibility(false);
    if(voiceSearchFlag){
      MVCApp.Customer360.callVoiceSearchResult("success", "", "PDP");
      voiceSearchFlag = false;
    }
    setProductResults(results);
    selectedImageIndex =0;
    var locale=kony.store.getItem("languageSelected");
    if(locale== "fr_CA"){
      frmPDP.lblIconShare.padding =[35,0,0,0];
    }else{
       frmPDP.lblIconShare.padding =[20,0,0,0];
    }
    var shopOnlineURL = results.getShopOnlineURL();
    if(shopOnlineURL!==""){
       MVCApp.serviceConstants.buyOnlineLink = shopOnlineURL;
    }
    defaultColor = results.getDefaultColor();
    defaultSize = results.getDefaultSize();
     defaultCount = results.getDefaultCount();
    var cType = results.getCtype() || [];
    var inventory = results.getInventory() || [];
	
    var lPricesObj = results.getPrice();
   
     var langReg=kony.store.getItem("languageSelected");
    if(lPricesObj && lPricesObj.price){
       if(langReg==="fr_CA"){
       if(lPricesObj.price!==null && lPricesObj.price!==undefined)
		   frmPDP.lblPrice.text = Number(lPricesObj.price).toLocaleString('fr')+"$";
          //  frmPDP.lblPrice.text =  lPricesObj.price+"$";
          }else
            {
                 frmPDP.lblPrice.text="$"+lPricesObj.price;
            }
     
      frmPDP.lblPrice.isVisible = true;
    }else{
      frmPDP.lblPrice.isVisible = false;
    }
    if(lPricesObj && lPricesObj.regPrice){
       if(langReg==="fr_CA"){
          frmPDP.lblRegPrice.text = "Reg. "+Number(lPricesObj.regPrice).toLocaleString('fr') +"$";
       }else
     	 frmPDP.lblRegPrice.text = "Reg. $"+lPricesObj.regPrice;
      frmPDP.lblRegPrice.isVisible = true;
    }else{
      frmPDP.lblRegPrice.isVisible = false;
    }
    setPDPData(results);
    defaultProductName = results.getTitle();
    
    var name = results.getTitle();
    MVCApp.getLocatorMapController().setProductName(name);
    frmPDP.lblItemNumber.text = "#" + results.getProductId();
    var cAltProdutId = results.getCAltProductId();
      if(cAltProdutId!=="" && cAltProdutId !==null && cAltProdutId!="null"){
        MVCApp.getPDPController().setProductIDForCheck(results.getCAltProductId());
      }else{
        MVCApp.getPDPController().setProductIDForCheck(results.getProductId());
      } 
    //frmPDP.rtDescription.text = results.getLongDescription();
    var description = results.getLongDescription();
    description ="<div>"+description+"</div>";
    var htmlString="";
    var fontValue = "100%";
    var lineHeight = "145%";

    //alert(screenWidth);
    if(description!==""){
      //#ifdef android
      htmlString="<html><head>	<style>	body{overflow-x:hidden;overflow-y:hidden;}	</style><script> function myFunction(){ return document.body.scrollHeight;}</script><meta charset=\"UTF-8\"><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name=\"description\" content=\"Michaels Store Map\"><meta http-equiv=\"cleartype\" content=\"on\"><meta name=\"viewport\" content=\"width=100%,max-width=100%, initial-scale=1, maximum-scale=1\"></head><body id=\"demo\" onload=\"myFunction()\"max-width:100%;height=100% > <style type=\"text/css\">@font-face {font-family: 'Gotham Book';src: url(\"file:///android_asset/fonts/Gotham Book.ttf\")}	div { line-height: "+lineHeight+";color:515151;font-size:"+fontValue+";font-family:'Gotham Book'} li { line-height: "+lineHeight+";color:515151;font-size:"+fontValue+";font-family:'Gotham Book'} </style>"+description+"</body></html>";
      //#endif
      //#ifdef iphone
      htmlString="<html><head>	<style>	body{overflow-x:hidden;overflow-y:hidden;}	</style><script> function myFunction(){ return document.body.scrollHeight;}</script><meta charset=\"UTF-8\"><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name=\"description\" content=\"Michaels Store Map\"><meta http-equiv=\"cleartype\" content=\"on\"><meta name=\"viewport\" content=\"width=100%,max-width=100%, initial-scale=1, maximum-scale=1\"></head><body id=\"demo\" onload=\"myFunction()\"max-width:100%;height=100% > <style>	div { line-height: "+lineHeight+";color:515151;font-size:"+fontValue+";font-family:Gotham Book} li { line-height: "+lineHeight+";color:515151;font-size:"+fontValue+";font-family:Gotham Book} </style>"+description+"</body></html>";
      //#endif
      
    }
    kony.print("Browser string "+htmlString);
  
  // if( results.isBrowserRequired==null ||  results.isBrowserRequired==undefined){
    frmPDP.flxBrowser.remove(frmPDP.brwsrDescription);
     var brwsrDescription = new kony.ui.Browser({
        "detectTelNumber": true,
        "enableZoom": false,
            "id": "brwsrDescription",
        "isVisible": true,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
     brwsrDescription.enableJsInterface = true;
    
  // }
    frmPDP.flxBrowser.add(brwsrDescription);
    function setExternalLinks(browserWidget,params){
 	  	var URL = params.originalURL;
      	URL = decodeURI(URL);
      	//#ifdef iphone
        URL = URL.replace(/%2C/g,",");
      	URL = encodeURI(URL);
      	//#endif
      	MVCApp.Toolbox.common.openApplicationURL(URL);
      	if(URL.indexOf("demandware") !== -1)
        {
        	return true;
        }
      	return false;
    }
    function addBlankTargets(s) {
  		return (""+s).replace(/<a\s+href=/gi, '<a target="_blank" href=');
	}
    if(htmlString !== ""){
    	if(htmlString.indexOf('target="_blank"') === -1){
        	htmlString = addBlankTargets(htmlString);
      	}
     frmPDP.brwsrDescription.htmlString = htmlString;
     frmPDP.brwsrDescription.handleRequest = setExternalLinks;
      
     // frmPDP.brwsrDescription.enableJsInterface = true;
     brwsrDescription.onSuccess = function() {
      var result =  frmPDP.brwsrDescription.evaluateJavaScriptAsync ("myFunction()",function(result, error){
        
        if (!error){
            frmPDP.brwsrDescription.height = result+"dp";
          //#ifdef android 
             frmPDP.flxBrowser.forceLayout();
          //#endif

        }  
      }	);
       
      };
    }else{
    } 
    
    frmPDP.lblStockInfo.text = results.getStockLevel();
    
    loadStaticMessages(results);
    shoppingListProductID= results.getProductId();
    if (!varientChangeFlag) {
      dataPDPObj = results;
      var imagesGroup = results.getImages();
      var prodName= defaultProductName;
      var varients = results.getVariationAttributes();
      if(varients.length > 0){
      if(defaultColor !== "")
        {
          prodName =prodName+" - "+defaultColor;
        }
      if(defaultSize !== "")
        {
          prodName =prodName+" - "+defaultSize;
        }
      if(defaultCount !== "")
        {
           prodName =prodName+" - "+defaultCount;
        }  
      }
      frmPDP.lblName1.text = prodName;
      setImagestoForm(imagesGroup);
       loadVarients(results);
      showShoppingAt();
      var reviewsCount = results.getReviewsCount();
       if(reviewsCount===0 || reviewsCount =="0"){
       frmPDP.btnReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.btnWriteAReview");
       }else{
         frmPDP.btnReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviews") + "(" + results.getReviewsCount() + ")";  
       }
    var starRating = results.getRating();
    starRating = parseFloat(starRating);
    starRating = starRating.toFixed(1);
	var starFloor = Math.floor(starRating);
	var starDecimal = starRating.toString().split(".")[1];
    starDecimal = parseInt(starDecimal);      
    for (var i = 1; i <= 5; i++) {
      if (i <= starFloor) {
        frmPDP["imgStarP" + i].src = "star.png";
      } else {
        frmPDP["imgStarP" + i].src = "starempty.png";
      }
    }	
      var starFlag = starFloor+1;
      if(starDecimal>0){
        frmPDP["imgStarP" + starFlag].src = "star"+starDecimal+".png";
      }
      show();
    }
    var cType1 = results.getCtype();
    var inStoreOrOnlineStatus = "";
    var stockLevel = "";
    var ats = "";
    var stockInventory = "";
    var stockInventory1 = {};
    stockInventory = results.getInventory();
    stockInventory1 = JSON.parse(stockInventory);
    for (var key in stockInventory1) {
      var value = stockInventory1[key] || "";
      if (key == "ats" && value !== "") {
        ats = value;
      }
      if (key == "stock_level" && value !== "") {
        stockLevel = value;
      }
    }
    
    if (cType1.indexOf("Sellable") > -1) {
      inStoreOrOnlineStatus = "Sellable";
    } else if (cType1.indexOf("Online") > -1) {
      inStoreOrOnlineStatus = "Online";
    } else if (cType1.indexOf("InStore") > -1) {
      inStoreOrOnlineStatus = "InStore";
    }
    else if (cType1.indexOf("Dropship") > -1) {
      inStoreOrOnlineStatus = "Dropship";
    }
    
  	if (inStoreOrOnlineStatus !="Sellable" && (cType1.indexOf("Online") > -1 || cType1.indexOf("Dropship") > -1 )) {
      if(cAltProdutId!=="" && cAltProdutId !==null && cAltProdutId!="null" && cType1.indexOf("Dropship") > -1){
        inStoreOrOnlineStatus ="Sellable";
        MVCApp.getPDPController().setIsDropshipItemSellableFlag(true);
        frmPDP.checkNearByStores.setVisibility(true);
      }else{
        MVCApp.getPDPController().setIsDropshipItemOnlineOnlyFlag(true);
        frmPDP.checkNearByStores.setVisibility(false);
      }
      if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
        determineVisibilityOfPromoAndClearanceTags(results);
      }
    } else {
      frmPDP.checkNearByStores.setVisibility(true);
    }
    
    var stockQuantityDetail="";
    var lMyLocationFlag = false;
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      lMyLocationFlag = "true";
    }else{
      lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
    }
    if(lMyLocationFlag === "true"){
      stockQuantityDetail=results.getCoStoreInventory() || "";
      if(stockQuantityDetail!==""){
        stockQuantityDetail=JSON.parse(stockQuantityDetail);
        gblStockInfo=stockQuantityDetail.ats;
      }
    }
    
    var stockInfo = MVCApp.Toolbox.common.getStockInfo(inStoreOrOnlineStatus, stockLevel, ats,stockQuantityDetail);
    if(isVariantSelected)
      {
         frmPDP.lblOnlineStock.text = stockInfo.storeInventoryStatus;
          frmPDP.lblOnlineStock.setVisibility(stockInfo.storeInventoryFlag);
      }
   
    if (stockInfo.aisleNoStatus) {
      frmPDP.lblOnlineStock.width = "60%";
      frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    } else {
      frmPDP.lblOnlineStock.width = "100%";
      frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_CENTER;
    }
    if (!stockInfo.aisleNoStatus && !stockInfo.storeInventoryFlag && (isVariantSelected === true)) {
      frmPDP.flxInventoryLocation.setVisibility(false);
    } else {
      frmPDP.flxInventoryLocation.setVisibility(true);
    }
    
    var storeBlockFlag = "";
    var storeObj = {};
    var storeString=kony.store.getItem("myLocationDetails") ||"";
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.POI){
      storeObj = gblInStoreModeDetails.POI;
    }else{
      if(storeString!==""){
        storeObj = JSON.parse(storeString);
      }
    }
    
    if(storeObj && storeObj.isblocked){
      storeBlockFlag = storeObj.isblocked || "";
    }
       if(stockInfo.aisleNoStatus === true  && aisleFlag === true){
         kony.print("@@@LOG:updateScreen:viewPDP.js-flow1");
          if(frmPDP.btnAisleNum.text === " " || frmPDP.btnAisleNum.text === "" ){
            kony.print("@@@LOG:updateScreen:viewPDP.js-flow2");
            frmPDP.lblOnlineStock.width = "100%";
            frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_CENTER;
            frmPDP.lblMapLocation.setVisibility(false);
            frmPDP.btnAisleNum.setVisibility(false);
          }else if(lMyLocationFlag === "true" && storeBlockFlag!="true"){
            kony.print("@@@LOG:updateScreen:viewPDP.js-flow3");
            frmPDP.lblOnlineStock.width = "60%";
            frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
            frmPDP.lblMapLocation.setVisibility(true);
            frmPDP.btnAisleNum.setVisibility(true);
          }else{
            kony.print("@@@LOG:updateScreen:viewPDP.js-flow4");
            frmPDP.lblOnlineStock.width = "100%";
            frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_CENTER;
            frmPDP.lblMapLocation.setVisibility(false);
            frmPDP.btnAisleNum.setVisibility(false);
          }   
        }
        else{
          kony.print("@@@LOG:updateScreen:viewPDP.js-flow5");
          frmPDP.lblOnlineStock.width = "100%";
          frmPDP.lblOnlineStock.contentAlignment = constants.CONTENT_ALIGN_CENTER;
          frmPDP.lblMapLocation.setVisibility(false);
          frmPDP.btnAisleNum.setVisibility(false); 
        }
    setBuyOnlineDisablityFlag(stockInfo.buyOnlineDisabilityFlag);
    if(stockInfo.buyOnlineDisabilityFlag) {
      frmPDP.btnBuyOnline2.skin = "sknBtnDisable";
      frmPDP.btnBuyOnline2.focusSkin = "sknBtnDisable";
    }
    frmPDP.btnBuyOnline2.setVisibility(stockInfo.buyOnlineStatus);

      frmPDP.lblverbiage.setVisibility(true);
      frmPDP.lblverbiage.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.verbiage");
    
    MVCApp.Toolbox.common.dismissLoadingIndicator();
    updateRecommendedProjets();
  }

	function loadStaticMessages(results) {
		frmPDP.segWarnings.removeAll();
		frmPDP.segWarnings.widgetDataMap = { //warningImg:warningImg,
			"warningText": "warningText",
			"lblWarningDesc": "lblWarningDesc"
		};
		var segWarningData = [];
      	var temp = {};
		var smallMagnetsFlag = results.getSmallMagnets();
		if (smallMagnetsFlag == "true") {
			temp = {};
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = "Not intended for children.  Swallowing of magnets may cause serious injury and require immediate medical care.";
			segWarningData.push(temp);
		}
		var sharpPointFlag = results.getSharpPoint();
		if (sharpPointFlag == "true") {
          	temp = {};
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = "Keep fingers clear of the cutting area; blades are extremely sharp. Caution: Recommended for Adult use only.";
			segWarningData.push(temp);
		}
      var flammable = results.getFlammableMessage();
		if (flammable.flammableFlag && flammable.flammableMessage !== "" ) {
          	temp = {};
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = flammable.flammableMessage;
			segWarningData.push(temp);
		}
       var caPropMessage = results.getCAProp64Message();
		if (caPropMessage.caFlag && caPropMessage.caMessage !== "") {
          	temp = {};
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = caPropMessage.caMessage;
			segWarningData.push(temp);
		}
      
	  var hazardType = results.getHazardType();
      hazardType = MVCApp.Toolbox.common.isArray(hazardType) ? hazardType :[hazardType];
      for(i=0;i<hazardType.length;i++)
        {
          var temp = {};
		if (hazardType[i] == "Small Ball") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg1");
			segWarningData.push(temp);
		} else if (hazardType[i] == "Small Parts") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg2");
			segWarningData.push(temp);
		} else if (hazardType[i] == "Ballon" || hazardType[i] == "Balloon") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg3");
			segWarningData.push(temp);
		} else if (hazardType[i] == "Marble") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg4");
			segWarningData.push(temp);
		} else if (hazardType[i] == "Small Marble") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg5");
			segWarningData.push(temp);
		} else if (hazardType[i] == "Strangluation") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg6");
			segWarningData.push(temp);
		} else if (hazardType[i] == "Entanglement") {
			temp.warningText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.warning");
			temp.lblWarningDesc = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.hazardMsg7");
			segWarningData.push(temp);
		}
          
       }
      	
		if (segWarningData.length >= 0) {
			frmPDP.segWarnings.setData(segWarningData);
			frmPDP.segWarnings.setVisibility(true);
		} else {
			frmPDP.segWarnings.setVisibility(false);
		}
	}
  
  	function getSizeVariantMap(variants){
      	var variantMap = {};
      	var i = null;
        for (i = 0; variants.length > i; i += 1) {
          var varValueObj = JSON.parse(variants[i].variationValue);
          if(!variantMap[varValueObj.color]){
            variantMap[varValueObj.color] = [];
          }
          variantMap[varValueObj.color].push(varValueObj.size);
        }
		return variantMap;
    }
  
  	function getColorVariantMap(variants){
      	var variantMap = {};
      	var i = null;
        for (i = 0; variants.length > i; i += 1) {
          var varValueObj = JSON.parse(variants[i].variationValue);
          if(!variantMap[varValueObj.size]){
            variantMap[varValueObj.size] = [];
          }
          variantMap[varValueObj.size].push(varValueObj.color);
        }
		return variantMap;
    }

	function loadVarients(results) {
		frmPDP.flxVarients.removeAll();
		var varients = results.getVariationAttributes();
		varientsLength = varients.length;
        var colorVariantMap = {};
        var sizeVariantMap = {};
		var availableColorsForDefaultSize = [];
		var availableSizesForDefaultColor = [];
      	if((varients.length >= 2) && (varients[0].variationAttributesId === "color") && (varients[1].variationAttributesId === "size")){
          if(varients[0].variationAttributesId === "color"){
            colorVariantMap = getColorVariantMap(results.getVariants());
            availableColorsForDefaultSize = defaultSize ? colorVariantMap[defaultSize] : [];
          } 
          if(varients[1].variationAttributesId === "size"){
            sizeVariantMap = getSizeVariantMap(results.getVariants());
            availableSizesForDefaultColor = defaultColor ? sizeVariantMap[defaultColor] : [];
           if(typeof availableSizesForDefaultColor == "undefined"){
              for (key in sizeVariantMap) {
                if (sizeVariantMap.hasOwnProperty(key)) {
                 availableSizesForDefaultColor = sizeVariantMap[key];
                 break;
                }
              }
            }  
          }
        }
		for (var j = 0; j < varients.length; j++) {
			var masterData = [];
			masterData.push([varients[j].variationAttributesId, varients[j].variationAttributesName]);
			var varientsData = varients[j].variationAttributesValues || [];
			var dynamicColorSizeFlag = false;
			for (var k = 0; k < varientsData.length; k++) {
              if((varients.length == 2) && (varients[0].variationAttributesId === "color") && (varients[1].variationAttributesId === "size")){
                dynamicColorSizeFlag = true;
                if((availableColorsForDefaultSize && (availableColorsForDefaultSize.length === 0 || (masterData[0][0] == "color") && (availableColorsForDefaultSize.indexOf(varientsData[k].name) >= 0))) ||
                   (availableSizesForDefaultColor && (availableSizesForDefaultColor.length === 0 || (masterData[0][0] == "size") && (availableSizesForDefaultColor.indexOf(varientsData[k].name) >= 0)))){
                    var tempArray = [];
                    tempArray.push(varientsData[k].name);
                    tempArray.push(varientsData[k].value);
                    masterData.push(tempArray);
                }
              }else{
                var tempArray = [];
				tempArray.push(varientsData[k].name);
				tempArray.push(varientsData[k].value);
				masterData.push(tempArray);
              }
               // }
			}
			var dropDownFlag = true;
			var labelFlag = true;
            var labelName1 =  "";
          	var labelName2 =  "";
			if (masterData.length > 0) {
				if(masterData.length>2){
                      dropDownFlag = true;
                      labelFlag = false;
                      labelName1 = "";
                      labelName2 = "";
                }else if(masterData.length == 1) {
                      dropDownFlag = false;
                      labelFlag = true;
                      labelName1 = masterData[0][1] || "";
                      labelName2 = "";								
				}else if(masterData.length == 2) {
                      dropDownFlag = false;
                      labelFlag = true;
                      labelName1 = masterData[0][1] || "";
                      labelName2 = masterData[1][1] || "";								
				}				
			} else {
					dropDownFlag = false;
                    labelFlag = false;
                    labelName1 = "";
                    labelName2 = "";
			}
			var defaultDropDownSelection = "";
            if(masterData[0][0] === "color")
            {
			if (defaultColor === "") {
				defaultDropDownSelection = masterData[0][0];
			} else {
				if (masterData[0][0] == "color") {
					defaultDropDownSelection = defaultColor;
				} else {
					defaultDropDownSelection = masterData[0][0];
				}
			}
            }
          if(masterData[0][0] === "size")
            {
                  if (defaultSize === "") {
                    defaultDropDownSelection = masterData[0][0];
                } else {
                    if (masterData[0][0] == "size") {
                        defaultDropDownSelection = defaultSize;
                    } else {
                        defaultDropDownSelection = masterData[0][0];
                    }
                }
            }
          if(masterData[0][0] === "count")
            {
                  if (defaultCount === "") {
                    defaultDropDownSelection = masterData[0][0];
                } else {
                    if (masterData[0][0] == "count") {
                        defaultDropDownSelection = defaultCount;
                    } else {
                        defaultDropDownSelection = masterData[0][0];
                    }
                }
            }
			var infoObj = {"dynamicColorSizeFlag": dynamicColorSizeFlag};
			if(dynamicColorSizeFlag){
              if(j===0){
                infoObj.variantMap = colorVariantMap;
              }else if(j===1){
                infoObj.variantMap = sizeVariantMap;
              }
            }
			var listboxColor = new kony.ui.ListBox({
				"centerX": "50%",
				"focusSkin": "sknListboxMedium24px1pxBdr",
				"height": "30dp",
				"id": "listboxColor" + j,
				"isVisible": dropDownFlag,
				"masterData": masterData,
				"selectedKey": defaultDropDownSelection,
				"skin": "sknListboxMedium24px1pxBdr",
				//#ifdef iphone
				"onDone": onChangeVariant,
				//#else
				"onSelection": onChangeVariant,
				//#endif 
				"top": "10dp",
				"width": "100%",
				"zIndex": 1
			}, {
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
				"padding": [3, 0, 0, 0],
				"paddingInPixel": false
			}, {
				"applySkinsToPopup": true,
				"dropDownImage": "dropdown.png",
				"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
			});
          	
			var lblDesc1 = new kony.ui.Label({
				"centerX": "50%",
				"id": "lblDesc" + j,
				"isVisible": labelFlag,
				"skin": "sknLblGothamRegular24pxBlack",
				"text": labelName1 + ":" + labelName2,
				"textStyle": {
					"lineSpacing": 5,
					"letterSpacing": 0,
					"strikeThrough": false
				},
				"top": "0dp",
				"width": "94%",
              	"height":"0dp",
				"zIndex": 1
			}, {
				"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
				"padding": [0, 0, 0, 0],
				"paddingInPixel": false
			}, {
				"textCopyable": false
			});
			listboxColor.info = infoObj;
			frmPDP.flxVarients.add(listboxColor, lblDesc1);
		}
		addToShoppingList();
       if(varientsLength > 0){
        var selectedVariantForImages = [] ;
      	if(defaultColor !="" || defaultSize!="" || defaultCount !=""){
          var temp ={} ;
          if(defaultColor !="")
            {
               var colorAtt ={} ;          
              colorAtt.id ="color";
			  colorAtt.value= defaultColor;
              selectedVariantForImages.push(colorAtt); 
            }
          
           if(defaultSize !="")
            {
              var sizeAtt ={} ;          
              sizeAtt.id ="size";
			  sizeAtt.value= defaultSize;
              selectedVariantForImages.push(sizeAtt); 
            }
           if(defaultCount !="")
            {      		
              var countAtt ={} ;          
              countAtt.id ="count";
			  countAtt.value= defaultCount;
              selectedVariantForImages.push(countAtt); 
            }
          getImagesForSelectedVariant(selectedVariantForImages);
        }
      }
	}

	function setImagestoForm(imagesGroup) {
		var defaultImage = imagesGroup.largeImages || [];
		if (defaultImage.length > 0) {
			defaultImage = defaultImage[0] || [];
			defaultImage = defaultImage[0].link || "";
		}
      if(defaultImage.length === 0){
        defaultImage = "";
      }
		frmPDP.Image0f0a05398956349.src = defaultImage;
		//show Images
		frmPDP.flxThumbs.removeAll();
		var smallImagesGroup = imagesGroup.smallImages || [];
      	var smallImages =[];
      	if(smallImagesGroup.length > 0){
        smallImages= smallImagesGroup[0];
        }
      	if(smallImages.length>7){
          frmPDP.flxThumbs.centerX = null;
          frmPDP.flxThumbs.left = "5dp";
        }else{
          frmPDP.flxThumbs.centerX = "50%";
        }
      	frmPDP.flxThumbs.width = smallImages.length * 38;
      
      	if(smallImages.length>1){
          for (var i = 0; i < smallImages.length; i++) {
			var defaultSelectedSkin = "sknFlexBgBorderGray";
			if (i === 0) {
				defaultSelectedSkin = "sknFlxBlackBorder2px"; //"sknFlxBlackBorder2px";
			} else {
				defaultSelectedSkin = "sknFlexBgBorderGray"; //"sknFlexBgTransBorderLtGray";
			}
			var FlexContainer067018dc9d32f45 = new kony.ui.FlexContainer({
				"autogrowMode": kony.flex.AUTOGROW_NONE,
				"centerY": "50%",
				"clipBounds": true,
				"height": "28dp",
				"id": "FlexContainer067018dc9d32f45" + i,
				"isVisible": true,
				"layoutType": kony.flex.FREE_FORM,
				"left": "10dp",
				"skin": defaultSelectedSkin,
				"width": "28dp",
				"onClick": onChangeProductImage,
				"zIndex": 1
			}, {}, {});
			FlexContainer067018dc9d32f45.setDefaultUnit(kony.flex.DP);
			var Image0fe184711e4e043 = new kony.ui.Image2({
				"height": "100%",
				"id": "Image0fe184711e4e043" + i,
				"imageWhenFailed": "notavailable_1012x628",
				"imageWhileDownloading": "white_1012x628",
				"isVisible": true,
				"left": "0dp",
				"skin": "slImage",
				"src": smallImages[i].link,
				"top": "0dp",
				"width": "100%",
				"zIndex": 1
			}, {
				"imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
				"padding": [0, 0, 0, 0],
				"paddingInPixel": false
			}, {});
			FlexContainer067018dc9d32f45.add(Image0fe184711e4e043);
			frmPDP.flxThumbs.add(FlexContainer067018dc9d32f45);
		}
        }
	}
	
	function sortTwoDimArrayBySecondColumn(a, b) {
        if (a[1] === b[1]) {
            return 0;
        }
        else {
            return (a[1] < b[1]) ? -1 : 1;
        }
    }

	function onChangeVariant() {
        var prodName=defaultProductName;
		var selectedListboxData = this.masterData;
		var selectedColorVariant = "";
		var selectedSizeVariant = "";
		if (this.selectedKeyValue[0] != selectedListboxData[0][0]) {
          	if(this.info && this.info.dynamicColorSizeFlag){
              if(frmPDP["listboxColor0"].selectedKeyValue){
              	selectedColorVariant = frmPDP["listboxColor0"].selectedKeyValue[0];
              }
              if(frmPDP["listboxColor1"].selectedKeyValue){
              	selectedSizeVariant = frmPDP["listboxColor1"].selectedKeyValue[0]; 
              }
              if(this.id == "listboxColor0"){
                var sizeMasterData = [];
                var sizeVariantItemData = frmPDP.listboxColor1.info.variantMap[selectedColorVariant];
                for(var item = 0; item < sizeVariantItemData.length; item++){
                  var singleItem = [];
                  singleItem.push(sizeVariantItemData[item]);
                  singleItem.push(sizeVariantItemData[item]);
                  sizeMasterData.push(singleItem);
                }
				sizeMasterData.sort(sortTwoDimArrayBySecondColumn);
                sizeMasterData.unshift(["size", "Size"]);
                frmPDP.listboxColor1.masterData = sizeMasterData;
                var availableSizes = frmPDP.listboxColor1.info.variantMap[selectedColorVariant];
                if(availableSizes.indexOf(selectedSizeVariant) >= 0){
                  frmPDP.listboxColor1.selectedKey = selectedSizeVariant;
                }else{
                  if(selectedSizeVariant != "size" && availableSizes.length == 1){
                    frmPDP.listboxColor1.selectedKey = availableSizes[0];
                  }else{
                    frmPDP.listboxColor1.selectedKey = sizeMasterData[0][0];
                  }
                }
              }else{
                var colorMasterData = [];
                var colorVariantItemData = frmPDP.listboxColor0.info.variantMap[selectedSizeVariant];
                for(var item = 0; item < colorVariantItemData.length; item++){
                  var singleItem = [];
                  singleItem.push(colorVariantItemData[item]);
                  singleItem.push(colorVariantItemData[item]);
                  colorMasterData.push(singleItem);
                }
				colorMasterData.sort(sortTwoDimArrayBySecondColumn);
                colorMasterData.unshift(["color", "Color"]);
                frmPDP.listboxColor0.masterData = colorMasterData;
                var availableColors = frmPDP.listboxColor0.info.variantMap[selectedSizeVariant];
                if(availableColors.indexOf(selectedColorVariant) >= 0){
                  frmPDP.listboxColor0.selectedKey = selectedColorVariant;
                }else{
                  if(selectedColorVariant != "color" && availableColors.length == 1){
                    frmPDP.listboxColor0.selectedKey = availableColors[0];
                  }else{
                    frmPDP.listboxColor0.selectedKey = colorMasterData[0][0];
                  }
                }
              }
            }
			var selectedVariant = {};
			var selectedVariantForImages = [];
			for (var i = 0; i < varientsLength; i++) {
				var temp = {};
				if (frmPDP["lblDesc" + i].isVisible) {
					var txt = frmPDP["lblDesc" + i].text;
					var txtValue = txt.split(":");
					var tempVal = txtValue[0].charAt(0).toLowerCase() + txtValue[0].substr(1).toLowerCase();
					selectedVariant[tempVal] = txtValue[1];
					temp.id = tempVal;
					temp.value = txtValue[1];
                  prodName = prodName+" - "+ txtValue[1];
                  selectedVariantForImages.push(temp);
				} else if(this.info && this.info.dynamicColorSizeFlag){
					var listboxData = frmPDP["listboxColor" + i].masterData;
                    var selectedVariantKey = (i===0) ? selectedColorVariant : selectedSizeVariant;
                    selectedVariant[listboxData[0][0]] = selectedVariantKey;
                    temp.id = listboxData[0][0];
                    temp.value = selectedVariantKey;
                    prodName = prodName+" - "+ selectedVariantKey;
                    selectedVariantForImages.push(temp);
                }else{
					var listboxData = frmPDP["listboxColor" + i].masterData;
                    if(frmPDP["listboxColor" + i].selectedKeyValue){
                      if(listboxData[0][0] != frmPDP["listboxColor" + i].selectedKeyValue[0]){
                      selectedVariant[listboxData[0][0]] = frmPDP["listboxColor" + i].selectedKeyValue[0];
                      temp.id = listboxData[0][0];
                      temp.value = frmPDP["listboxColor" + i].selectedKeyValue[0];
                           prodName = prodName+" - "+ frmPDP["listboxColor" + i].selectedKeyValue[0];
                      selectedVariantForImages.push(temp);  
                      }
                    }
				}				
			}
			getImagesForSelectedVariant(selectedVariantForImages);
			getProductDetailsForSelectedVariant(selectedVariant);
		}
      frmPDP.lblName1.text = prodName;
      gblIsNotFromOnChangeVarient = false;
	  addToShoppingList();
	}

	function getImagesForSelectedVariant(selectedVariants) {
		var imagesArray = [];
		for (var i = 0; i < allImagesGroup.length; i++) {
			var variationAttributes = allImagesGroup[i].variation_attributes || [];
			var variantCheckFlag = false;
			if (variationAttributes.length == selectedVariants.length) {
				for (var k = 0; k < variationAttributes.length; k++) {
					if (variationAttributes[k].id == selectedVariants[k].id) {
						var values = variationAttributes[k].values || [];
						if (values[0].value == selectedVariants[k].value) {
							if (k < 1) {
								variantCheckFlag = true;
							} else if (k > 0 && variantCheckFlag) {
								variantCheckFlag = true;
							} else {
								variantCheckFlag = false;
							}
						} else {
							variantCheckFlag = false;
						}
					} else {
						variantCheckFlag = false;
					}
				}
			} else {
				variantCheckFlag = false;
			}
			if (variantCheckFlag) {
				imagesArray.push(allImagesGroup[i]);
			}
		}
      	if(imagesArray.length >0){
          loadSelectedVariantImages(imagesArray);
        }
	}

	function loadSelectedVariantImages(imagesArray) {
		dataPDPObj.setImages(imagesArray);
		setImagestoForm(dataPDPObj.getImages());
	}

	function getProductDetailsForSelectedVariant(selectedVariants) {
      var lCheckIfVariantObjsHaveSameNoOfProps = false;
      var lSelVariantLen = Object.keys(selectedVariants).length;
      var lVariantLen = 0;
		var variants = dataPDPObj.getVariants();
		var variantEqualFlag = false;
		for (var i = 0; i < variants.length; i++) {
			var varientValue = variants[i].variationValue || "";
			varientValue = varientValue + "";
          if(varientValue != "" && varientValue != null && varientValue !="null"){
            try {
              varientValue = JSON.parse(varientValue);
              lVariantLen = Object.keys(varientValue).length;
              if(lSelVariantLen === lVariantLen){
                lCheckIfVariantObjsHaveSameNoOfProps = true;
              }else{
                lCheckIfVariantObjsHaveSameNoOfProps = false;
              }
              for (var key in varientValue) {
                kony.print(key);
                if (varientValue[key] == selectedVariants[key]) {
                  variantEqualFlag = true;
                } else {
                  variantEqualFlag = false;
                  break;
                }
              }
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPDP.js on getProductDetailsForSelectedVariant for varientValue:" + JSON.stringify(e)}]);
              if(e){
                TealiumManager.trackEvent("Varient Value Parse Exception In PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          }
          if(variantEqualFlag) {
            setMultiVariantCombinationPresentFlag(false);
            var headerText = [];
            headerText[0] = frmPDP.btnCategoryName.text;
            headerText[1] = frmPDP.imgCategoryImg.src;
            MVCApp.getPDPController().setFromBarcode(false);
            MVCApp.getPDPController().load(variants[i].variantProductId, true, true, headerText,"frmPDP",false);
            return;
          }
		}
      if(!variantEqualFlag && lCheckIfVariantObjsHaveSameNoOfProps){
        setMultiVariantCombinationPresentFlag(true);
      }
	}

	function loadAllImages(results) {
		allImagesGroup = results.image_groups || [];
	}

	function loadZoomImages() {
		var imagesGroup = dataPDPObj.getImages();
		MVCApp.getPinchandZoomController().load(imagesGroup, true,selectedImageIndex);
	}

	function onChangeProductImage() {
		if(this.skin != "sknFlxBlackBorder2px"){
		var imagesGroup = dataPDPObj.getImages();
		var smallImages = imagesGroup.smallImages;
        if(smallImages.length>0){
          smallImages = smallImages[0];
        }
		for(var i=0;i<smallImages.length;i++){
		frmPDP["FlexContainer067018dc9d32f45"+i].skin =  "sknFlexBgBorderGray";// "sknFlexBgTransBorderLtGray";
		}
		this.skin = "sknFlxBlackBorder2px";
		var selectedIndex = this.id.split("FlexContainer067018dc9d32f45")[1];
      	selectedImageIndex = selectedIndex;
		var defaultImage = imagesGroup.largeImages || [];
      	if(defaultImage.length > 0){
        defaultImage= defaultImage[0];
        }
      	var selImage= "";
		if (defaultImage.length > 0) {
			var selImageTemp = defaultImage[selectedIndex] || "";
			selImage = selImageTemp.link || "";
		}
		frmPDP.Image0f0a05398956349.src = selImage;  
	  }
	}

	function setHeaderText(headerText) {
		if (headerText.length > 1) {
			frmPDP.btnCategoryName.text = headerText[0];
			frmPDP.imgCategoryImg.src = headerText[1];
		}
	}

	function setProductsData(recs, size, prods) {
		productsData = recs;
		recCount = size;
		products = prods;
	}

	function getProductsData() {
		var segData = [];
		//  alert(recCount+" , "+products+" , "+JSON.stringify(productsData));
		frmPDP.segProductsList.widgetDataMap = {
			"imgItem": "ImageUrl",
			"lblItemName": "Product Name",
			"imgStar1": "imgStar1",
			"imgStar2": "imgStar2",
			"imgStar3": "imgStar3",
			"imgStar4": "imgStar4",
			"imgStar5": "imgStar5"
		};
		for (i = products; i < productsData.length; i++) {
			if (i == products) {
				products = products + 3;
			}
			var temp = productsData[i];
			if (i < products) {
				var priceTobeDisplayed = MVCApp.Toolbox.common.addDollarsToNumber(temp.Price);
				temp.priceProduct = {
					text: priceTobeDisplayed,
					skin: "sknLblGothamRegular32pxDarkGray"
				};
              	var tempImageURL = MVCApp.Toolbox.common.replaceWithHttps(temp["Full Image URL"]);
				temp.ImageUrl = removeFitInURL(tempImageURL)+"?fit=inside%7C220%3A220";
				temp.masterProductId = temp["Master Product ID"];
				var starRating = temp["BV AVERAGE RATING"] || 0;              	
				starRating = parseFloat(starRating);
				starRating = starRating.toFixed(1);
				var starFloor = Math.floor(starRating);
				var starDecimal = starRating.toString().split(".")[1];
   				starDecimal = parseInt(starDecimal);
				for (var j = 1; j < 5; j++) {
					if (j <= starFloor) {
						temp["imgStar" + j] = {src: "star.png"};
					} else {
						temp["imgStar" + j] = {src: "starempty.png"};
					}
				}
              	var starFlag = starFloor+1;
      			if(starDecimal>0){
      			temp["imgStar" + starFlag] = { src: "star"+starDecimal+".png"};
      			}
				segData.push(temp);
			}
			if (i == (products - 1)) {
				break;
			}
		}
		frmPDP.segProductsList.addAll(segData);
		frmPDP.ShowMoreProducts.setVisibility(false);
		if (recCount > products) {
			frmPDP.ShowMoreProducts.setVisibility(true);
		}
	}
	//Here we expose the public variables and functions
	function updateRecommendedProducts(results) {
		frmPDP.segProductsList.removeAll();
     	 results = results || {};
		var count = results.rec_count || "";
		var recs = results.recs || [];
		var productLen = 3;
		var segData = [];
		if (count > 0) {
			setProductsData(recs, count, productLen);
			frmPDP.segProductsList.widgetDataMap = {
				"imgItem": "ImageUrl",
				"lblItemName": "Product Name",
				"imgStar1": "imgStar1",
				"imgStar2": "imgStar2",
				"imgStar3": "imgStar3",
				"imgStar4": "imgStar4",
				"imgStar5": "imgStar5",
			};
			for (i = 0; i < recs.length; i++) {
				var temp = recs[i];
				var priceTobeDisplayed = MVCApp.Toolbox.common.addDollarsToNumber(temp.Price);
              	var tempImageURL = MVCApp.Toolbox.common.replaceWithHttps(temp["Full Image URL"]);
				temp.ImageUrl = removeFitInURL(tempImageURL)+"?fit=inside%7C220%3A220";
				temp.priceProduct = {
					text: priceTobeDisplayed,
					skin: "sknLblGothamRegular32pxDarkGray"
				};
				var starRating = temp["BV AVERAGE RATING"] || 0;
              	starRating = parseFloat(starRating);
				starRating = starRating.toFixed(1);
				var starFloor = Math.floor(starRating);
				var starDecimal = starRating.toString().split(".")[1];
   				starDecimal = parseInt(starDecimal);
				for (var j = 1; j <=  5; j++) {
					if (j <= starFloor) {
						temp["imgStar" + j] = {src: "star.png"};
					} else {
						temp["imgStar" + j] = {src: "starempty.png"};
					}
				}
              	var starFlag = starFloor+1;
      			if(starDecimal>0){
      			temp["imgStar" + starFlag] = { src: "star"+starDecimal+".png"};
      			}
				temp.masterProductId = temp["Master Product ID"];
				segData.push(temp);
				if (i == 2) {
					break;
				}
			}
			frmPDP.segProductsList.setData(segData);
			frmPDP.segProductsList.setVisibility(true);
			frmPDP.lblRecomProducts.setVisibility(true);
			frmPDP.ShowMoreProducts.setVisibility(false);
			if (count > productLen) {
				frmPDP.ShowMoreProducts.setVisibility(true);
			}
		} else {
			frmPDP.segProductsList.setVisibility(false);
			frmPDP.ShowMoreProducts.setVisibility(false);
			frmPDP.lblRecomProducts.setVisibility(false);
		}
	}

	function getSelectedItem() {
		var selectedItem = frmPDP.segProductsList.selectedRowItems[0] || {};
		var master = selectedItem["Master or Variant"] || "";
		var variant = true;
		if (master === "MASTER") {
			variant = false;
		} else {
			variant = true;
		}
		var headerText = [];
      	var defaultProductId = selectedItem.c_default_product_id || "";
		headerText[0] = frmPDP.btnCategoryName.text;
		headerText[1] = frmPDP.imgCategoryImg.src;
		MVCApp.getPDPController().setFromBarcode(false); 
		MVCApp.getPDPController().load(selectedItem.id, variant, false, headerText,"frmPDP",defaultProductId,true);
	}

	function updateRecommendedProjets() {
		frmPDP.segProjectList.removeAll();
		var projData = getProjectData();
		var segProjData = [];
		frmPDP.segProjectList.widgetDataMap = {
			lblName: "name",
			imgProject: "picProject",
			imgExpertise: "skillLevel",
			lblExpertiseDesc: "c_skillLevel",
			imgDuration: "creftTime",
			lblDuration: "c_ecomCraftTime"
		};
		projCount = projData.length;
		if (projCount > 0) {
			for (i = 0; i < projData.length; i++) {
				projects = 3;
				var temp = projData[i];
				var skillLevel = temp.c_skillLevel || {};
				if (skillLevel === "Beginner") temp.skillLevel = "skill_new_1.png";
				else if (skillLevel === "Intermediate") temp.skillLevel = "skill_new_2.png";
				else if (skillLevel === "Expert" || skillLevel === "Advanced") temp.skillLevel = "skill_new_3.png";
				else temp.skillLevel = "skill_new_1.png";
				var creftTime = temp.c_ecomCraftTime || {};
				if (creftTime === "About an hour" || creftTime === "Over an hour") temp.creftTime = "time_4.png";
				else if (creftTime === "About 45 minutes") temp.creftTime = "time_3.png";
				else if (creftTime === "About 30 minutes") temp.creftTime = "time_2.png";
				else if (creftTime === "About 15 minutes ") temp.creftTime = "time_1.png";
				else temp.creftTime = "time_1.png";
				temp.picProject = removeFitInURL(temp.image)+"?fit=inside%7C220%3A220";
				segProjData.push(temp);
				if (i == 2) {
					break;
				}
			}
			frmPDP.segProjectList.setData(segProjData);
			frmPDP.segProjectList.setVisibility(true);
			frmPDP.lblRecommended.setVisibility(true);
			if (projData.length > projects) {
				frmPDP.ShowMoreProjects.setVisibility(true);
			} else {
				frmPDP.ShowMoreProjects.setVisibility(false);
			}
		}
	}

	function showMoreProjects() {
		var segProjData = [];
		frmPDP.segProjectList.widgetDataMap = {
			lblName: "name",
			imgProject: "picProject",
			imgExpertise: "skillLevel",
			lblExpertiseDesc: "c_skillLevel",
			imgDuration: "creftTime",
			lblDuration: "c_ecomCraftTime"
		};
		for (i = projects; i < ProjectsData.length; i++) {
			if (i == projects) {
				projects = projects + 3;
			}
			var temp = ProjectsData[i];
			var skillLevel = temp.c_skillLevel || {};
			if (skillLevel === "Beginner") temp.skillLevel = "skill_new_1.png";
			else if (skillLevel === "Intermediate") temp.skillLevel = "skill_new_2.png";
			else if (skillLevel === "Expert" || skillLevel === "Advanced") temp.skillLevel = "skill_new_3.png";
			else temp.skillLevel = "skill_new_1.png";
			var creftTime = temp.c_ecomCraftTime || {};
			if (creftTime === "About an hour" || creftTime === "Over an hour") temp.creftTime = "time_4.png";
			else if (creftTime === "About 45 minutes") temp.creftTime = "time_3.png";
			else if (creftTime === "About 30 minutes") temp.creftTime = "time_2.png";
			else if (creftTime === "About 15 minutes ") temp.creftTime = "time_1.png";
			else temp.creftTime = "time_1.png";
			temp.picProject = removeFitInURL(temp.image)+"?fit=inside%7C220%3A220";
			segProjData.push(temp);
			if (i == (projects - 1)) {
				break;
			}
		}
		frmPDP.segProjectList.addAll(segProjData);
		frmPDP.ShowMoreProjects.setVisibility(false);
		if (projCount > projects) {
			frmPDP.ShowMoreProjects.setVisibility(true);
		}
	}

	function getSelectedProjectItem() {
      	var queryText=MVCApp.getPDPController().getQueryString();
      	MVCApp.getPJDPController().setQueryString(queryText);
		var selectedData = frmPDP.segProjectList.selectedRowItems[0];
		var productId = selectedData.ID;
		MVCApp.getPJDPController().load(productId, true,"frmPDP");
	}

	function setGeoLocation(flag) {
		geoFlag = flag;
	}
  
  	function promptUserToSelectValueFromVariantDropdown(){
      var lVariantSelectionPromptText = "";
      var lDropdownsCountWithoutSelVal = 0;
      var lVisibleDropdownsCount = 0;
      var lDropdownType = "";
      if(varientsLength === 1){
        if(frmPDP.listboxColor0.isVisible && frmPDP.listboxColor0.masterData && (frmPDP.listboxColor0.selectedKey === frmPDP.listboxColor0.masterData[0][0])){
          lVariantSelectionPromptText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.selectA")+" "+frmPDP.listboxColor0.masterData[0][1]+" "+MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.fromTheDropDown");
        }
      }else if(varientsLength > 1){
        for(var v = 0;v < varientsLength;v++){
          if(frmPDP["listboxColor" + v].isVisible && frmPDP["listboxColor" + v].masterData){
            lVisibleDropdownsCount = lVisibleDropdownsCount + 1;
            if(frmPDP["listboxColor" + v].selectedKey === frmPDP["listboxColor" + v].masterData[0][0]){
              lDropdownsCountWithoutSelVal = lDropdownsCountWithoutSelVal + 1;
              lDropdownType = frmPDP["listboxColor" + v].masterData[0][1];
            }
          }
          if(v === varientsLength - 1){
            if((lVisibleDropdownsCount === lDropdownsCountWithoutSelVal) && (lDropdownsCountWithoutSelVal === 1) && (lVisibleDropdownsCount === 1)){
              lVariantSelectionPromptText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.selectA")+" "+lDropdownType+" "+MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.fromTheDropDown");
            }else{
              lVariantSelectionPromptText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.variantSelectionPromptText");
            }
          }
        }
      }
      if(lVariantSelectionPromptText){
        MVCApp.Toolbox.common.customAlertNoTitle(lVariantSelectionPromptText, "");
      }
    }

  function checkNearbyStorePDP() {

    var  checkNearbyStoreSkin= frmPDP.checkNearByStores.skin;
    checkStoreClicked=true;
    if (checkNearbyStoreSkin === "sknBtnDisable") {
      promptUserToSelectValueFromVariantDropdown();
    } else {
      var locationString="";
      var locationDetails={};
      storeIDPast="";
      if (MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
        try {
          locationString=kony.store.getItem("myLocationDetails");
          locationDetails=JSON.parse(locationString);
          locationDetails.OnHand=gblStockInfo;
          storeIDPast=locationDetails.clientkey;
          var locationDetailsStr=JSON.stringify(locationDetails);
          kony.store.setItem("myLocationDetails",locationDetailsStr);
        } catch(e) {
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewPDP.js on checkNearbyStorePDP for locationString:" + JSON.stringify(e)}]);
          if(e){
            TealiumManager.trackEvent("POI Parse Exception While Checking The NearBy Stores In PDP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }
      MVCApp.getPDPController().searchCheckNearby();
    }
  }
  
  function removeFitInURL(url){
	if(url.indexOf("?fit") > -1){
	var res = url.substring( 0, url.indexOf("?fit"));
	return res;
	}else{
	return url;
	}
	}
  
 
  
	return {
		show: show,
		bindEvents: bindEvents,
		updateScreen: updateScreen,
		loadAllImages: loadAllImages,
		setHeaderText: setHeaderText,
		updateRecommendedProducts: updateRecommendedProducts,
		updateRecommendedProjets: updateRecommendedProjets,
		getSelectedItem: getSelectedItem,
		setGeoLocation: setGeoLocation,
		backHistory: backHistory,
		addToProductListCallBack: addToProductListCallBack,
      	showPDP:showPDP,
      	defaultPDP:defaultPDP
	};
});