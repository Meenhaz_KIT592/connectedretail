/**
 * PUBLIC
 * This is the model for frmCreateAccount2 form
 */
var MVCApp = MVCApp || {};
MVCApp.CreateAccountTwoModel = (function(){
 var serviceType = MVCApp.serviceType.Registration;
  var operationId = MVCApp.serviceEndPoints.signUp;
  function createAccount(callback,requestParams){
    kony.print("CreateAccountTwoModel.js");
   MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
          callback(results);
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
      MVCApp.sendMetricReport(frmCreateAccount,[{"UnsuccessfulAccountCreation":JSON.stringify(error)}]);
    }
                   );
    
  }
 	return{
      createAccount:createAccount
    };
});
