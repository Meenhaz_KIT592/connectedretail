//Type your code here
var beaconResults;
var gExitMsg = "";
var gblBeaconMajor="";
var gblEntryType="";
var gblApplicationState="active";
var gblAppinBackground=false;
function createLocalnotification(results,pStoreID,pStoreName,messagesForWifiAndGeofence){
  try{
    var lStoreID = "";
    var lStoreName = "";
    var messages= [];
    gExitMsg = "";
    //var results = {"opstatus":0,"BEACONLIST":[{"ibeacon_name":"iBeacon","country":"United States","identifier":"e745b3dfd25189409ac4996d930d0113","formatted_address":"","tags.0":"entrance","minor":"899","city":"Wilmington","latitude":"34.2448","uuid":"b9407f30-f5f8-466e-aff9-25556b57fe6d","street_name":"Monument Drive","location_id":"113152","zipcode":"28405","major":"8998","real_id":"457958","ibeacon_enabled":"TRUE","street_number":"6881","name":"Michaels 4716","interval":"600","power":"-12","state":"North Carolina","id":"469485","state_code":"NC","longitude":"-77.8292"}],"httpStatusCode":0};
    kony.print("createLocalnotification results---------"+JSON.stringify(results));
    //#ifdef iphone
    setIphoneConfig();
    //#endif
    if(results === null){
      lStoreID = pStoreID;
      lStoreName = pStoreName;
      if(messagesForWifiAndGeofence && messagesForWifiAndGeofence.length>0){
      	createMessageForLocalNotification(true,lStoreID,lStoreName,messagesForWifiAndGeofence)
      }else{
      createMessageForLocalNotification(false,lStoreID,lStoreName);
      }
    }else{
      if(results["BEACONLIST"] && results["BEACONLIST"][0]){
        lStoreID = results["BEACONLIST"][0].major;
        lStoreName = results["BEACONLIST"][0].city;
        gblBeaconMajor=lStoreID;
      }
      if(results["WELCOME_MESSAGE"] && results["WELCOME_MESSAGE"].length>0){
        messages= results["WELCOME_MESSAGE"];
        createMessageForLocalNotification(true,lStoreID,lStoreName,messages);
      }else{
        createMessageForLocalNotification(false,lStoreID,lStoreName);
      }
    }
  }catch(e){
    kony.print("exception in createLocalnotification---"+JSON.stringify(e));
    if(e){
      TealiumManager.trackEvent("Exception While Creating The Local Notification", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  }
}

function createMessageForLocalNotification(messageFromDataBase,lStoreID,lStoreName,messages){
    var message;
    var cust_name = "";
    var notificationId = Math.floor(Math.random() * 20).toString(); //"01";
    var date = add10SecondsToNow();//d.getDate() + " " + m_names[d.getMonth()]+ " "     + d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+" GMT+0530";//"19 jul 2017 12:00:00 GMT+0530";//"18 jul 2017 12:55:00 +0530";
    var format = "yyyy-MM-dd HH:mm:ss";
  	var userObject = kony.store.getItem("userObj");
  	var deeplink="";
  	var messageAndDeeplink;
  	var okLabel="";
    var cancelLabel="";
  	storeChangeInStoreMode = false;
    if(userObject != null && userObject != undefined){
      var first_name = userObject.first_name || "";
      cust_name = first_name;
      if(cust_name && lStoreName){
        if(messageFromDataBase){
          message=findMessageText("LoggedInUserWithLocation",messages);
        }else{
          message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.storeMode.message.loggedInUserWithLocation");
        }
        message = message.replace("<location>", lStoreName);
      }else if(cust_name){
        if(messageFromDataBase){
          message= findMessageText("LoggedInUserNoLocation",messages);
        }else{
          message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.storeMode.message.loggedInUser");
        }
      }else if(lStoreName){
        if(messageFromDataBase){
          message= findMessageText("AnonymousUserWithLocation",messages);
        }else{
          message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.storeMode.message.locationOnly");
        }
        message = message.replace("<location>", lStoreName);
      }else{
        if(messageFromDataBase){
          message= findMessageText("DefaultAnonymousUserMessage",messages);
        }else{
          message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.storeMode.message.default");
        }
      }
      message = message.replace("<cust_name>", cust_name);
    }else if(lStoreName){
      if(messageFromDataBase){
        message= findMessageText("AnonymousUserWithLocation",messages);
      }else{
        message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.storeMode.message.locationOnly");
      }
      message = message.replace("<location>", lStoreName);
    }else{
      if(messageFromDataBase){
        message= findMessageText("DefaultAnonymousUserMessage",messages);
        
      }else{
        message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.storeMode.message.default");
      }
    }
  if(messageFromDataBase){
    deeplink=findMessageText("URL",messages);
  }
  if(deeplink && deeplink!=""){
    okLabel=findMessageText("OK",messages);
    cancelLabel=findMessageText("Cancel", messages)
  }
    var localNotificatype="Default Message App Delivered";
  	var localNotificatypeAppLaunch= "Default Message App Launched";
  	if(messageFromDataBase){
      localNotificatype="Custom Message App Delivered";
      localNotificatypeAppLaunch="Default Message App Launched";
    }
    gblInStoreModeDetails.storeID = lStoreID;
    gblInStoreModeDetails.storeName = lStoreName;
    gblInStoreModeDetails.storeMessage = message;
    gblInStoreModeDetails.notificationID = notificationId;
    gblInStoreModeDetails.date = date;
    gblInStoreModeDetails.format = format;
  	gblInStoreModeDetails.yesLabel=okLabel;
  	gblInStoreModeDetails.noLabel=cancelLabel;
  	gblInStoreModeDetails.URL=deeplink;
    gblInStoreModeDetails.categoryID = "invitation";
    gblTealiumTagObj.storeid = lStoreID;
  	gblInStoreModeDetails.notificationType=localNotificatypeAppLaunch;
  	MVCApp.getHomeController().fetchInStoreModeStoreDetails();
  	sendAnalyticsForNotificationDelivery(localNotificatype);
  	//#ifdef android
  	gblAppinBackground=!appInForeground;
  	//#endif
  	//#ifdef iphone
  	var returnedValue=ApplicationState.getApplicationState()
  	if(returnedValue=="inactive")
  		gblAppinBackground=true ;
  	//#endif
}

function sendAnalyticsForNotificationDelivery(localNotificatype){
  TealiumManager.trackEvent("Welcome Message Delivery", {conversion_category:"Welcome Message",conversion_id:localNotificatype,conversion_action:2});  
  var properties={"conversion_id":localNotificatype,"conversion_action":2};
  MVCApp.Customer360.sendInteractionEvent("Welcome Message Delivery", properties);
}

function sendAppLaunchOnLocalNotification(localNotificatype){
  TealiumManager.trackEvent("Welcome Message App Launch", {conversion_category:"Welcome Message",conversion_id:localNotificatype,conversion_action:2});  
  var properties={"conversion_id":localNotificatype,"conversion_action":2};
  MVCApp.Customer360.sendInteractionEvent("Welcome Message App Launch", properties);
}

function findMessageText(mKey,messages){
  var regionLang= MVCApp.Toolbox.Service.getLocale();
  for(var i=0;i<messages.length;i++){
    if(mKey===messages[i].message_key){
      var messageObj= messages[i];
      if(regionLang=="en-CA"){
        return messageObj.canada_english;
      }else if(regionLang=="fr-CA"){
        return messageObj.canada_french;
      }else{
        return messageObj.us_english;
      }
    }
  }
  return "";
} 

function createLocalNotificationForStoreMode(){
  try{
    if(!gblInStoreModeDetails || !gblInStoreModeDetails.isInStoreModeEnabled || !gblInStoreModeDetails.storeModeExtensionFlag || gblInStoreModeDetails.differentStoreFlag){
      kony.localnotifications.create({
        "id": gblInStoreModeDetails.notificationID,
        "dateTime": {
          "date": gblInStoreModeDetails.date,
          "format": gblInStoreModeDetails.format
        },
        "message": gblInStoreModeDetails.storeMessage,
        "title": " ",
        "categoryId": gblInStoreModeDetails.categoryID,
        "pspConfig":{
          "badge":0,
          "sound": kony.localnotifications.DEFAULT_SOUND
        }
      });
      var deviceID= MVCApp.Toolbox.common.getDeviceID();
      var loyaltyID="";
      var deviceName=kony.os.deviceInfo().name;
      var userObject = kony.store.getItem("userObj");
      if (userObject && userObject.c_loyaltyMemberID !==null && userObject.c_loyaltyMemberID !==undefined){
       loyaltyID=userObject.c_loyaltyMemberID;
      }
      var beaconMgmtObj ={};
      beaconMgmtObj.major = gblInStoreModeDetails.storeID;
      beaconMgmtObj.deviceID=deviceID;
      beaconMgmtObj.loyaltyID=loyaltyID;
      beaconMgmtObj.deviceName=deviceName;
      beaconMgmtObj.deviceTimeStamp=new Date();
    
      beaconMgmtObj.batteryPercentage = "";
      beaconMgmtObj.batteryLifetime = "";
      beaconMgmtObj.batteryVoltage = "";
      beaconMgmtObj.minor = "";
      beaconMgmtObj.proximityUUID = "";
      MVCApp.sendMetricReport(frmHome,[{"beacon":JSON.stringify(beaconMgmtObj)}]);
      gblBeaconMgmtArr.push(beaconMgmtObj);
      sendMetricForBeacons();
      TealiumManager.trackEvent("Store Visit Beacon", {event_type:"registration", conversion_category:"Store Visit",conversion_id:"Beacon",conversion_action:2,storeid:beaconMgmtObj.major,device_time_stamp:JSON.stringify(beaconMgmtObj.deviceTimeStamp),device_id:beaconMgmtObj.deviceID,loyalty_id:beaconMgmtObj.loyaltyID,device_type:beaconMgmtObj.deviceName});
    }
    var d = new Date();
    gblInStoreModeDetails.entryTimestamp = Math.round(d.getTime()/1000);
    gblInStoreModeDetails.isInStoreModeEnabled = true;
    kony.store.setItem("inStoreModeDetails",gblInStoreModeDetails);
    setStoreMode();
    callBeaconManagement();
    var beaconString= kony.store.getItem("beaconData")||"";
    var beaconObj={};
    var beaconObjArr="";
    var timeStampOfdetection=0;
    var timeOfNotification=d.getTime();
    if(beaconString!=""){
      beaconObjArr=JSON.parse(beaconString);
      for(var i=0;i<beaconObjArr.length;i++){
        beaconObj=beaconObjArr[i];
        if(beaconObj.hasOwnProperty("uniqueID") &&beaconObj.uniqueID ==gblUniqueID){
          beaconObj.status= "notified";
          timeStampOfdetection=beaconObj.timeStamp;
          beaconObj.timeTaken= timeOfNotification-timeStampOfdetection;
        }
      }
      gblBeaconObjArr=beaconObjArr;
      kony.store.setItem("beaconData",JSON.stringify(beaconObjArr));
      
     
    }
    
  }catch(pError){
    kony.print("LOG:createLocalNotificationForStoreMode-pError:"+JSON.stringify(pError));
    if(pError){
      TealiumManager.trackEvent("UI Exception Of Setting Active Skin For a Button On Footer", {exception_name:pError.name,exception_reason:pError.message,exception_trace:pError.stack,exception_type:pError});
    }
  }
}

function callBeaconManagement()
{
   //#ifdef iphone
  InstantiateBeaconManagerObject.startDiscoveryForBeacon(
		callMgmtCallback);
   //#endif
    //#ifdef android
  BeaconManagerObject.startDiscovery();
   //#endif
}
function notificationHandler(response){
  if(response){
    var url=gblInStoreModeDetails.URL ||"";
    MVCApp.Toolbox.common.openApplicationURL(gblInStoreModeDetails.URL);
    if(MVCApp.Toolbox.common.determineIfExternal(url)){
      //FacebookAnalytics.reportEvents("", "", url, "LocalNotification");
    }
  }
}

function localNotCallBacks() {
    try {
      kony.print("localNotCallBacks");
        kony.localnotifications.setCallbacks({
            "offlinenotification": offlinenotificationbeacons,
            "onlinenotification": onlinenotificationbeacons
    });
    } catch (err) {
      kony.print("Error Code " + err.errorCode + " Message " + err.message);
      if(err){
        TealiumManager.trackEvent("Exception While Setting The Callbacks For Local Notification", {exception_name:err.name,exception_reason:err.message,exception_trace:err.stack,exception_type:err});
      }
    }
}

/* Notification callback handlers. These are invoked automatically when their respective notifications are fired. */
function offlinenotificationbeacons(notificationobject, actionid ){
  // This will trigger if the user clicks on the notification after a considerable time
 var cancellbl= gblInStoreModeDetails.noLabel||"";
 var cust360Obj=MVCApp.Customer360.getGlobalAttributes();
 cust360Obj.launchType="beacon";
 cust360Obj.isFirstLaunch="false";
  if(appInForeground){
    MVCApp.Customer360.sendInteractionEvent("Launch",cust360Obj);
  }
  appLaunchCust360Sent=true;
    if(gblInStoreModeDetails.storeMessage){
      kony.print("offline notifications fired");
      if(gblInStoreModeDetails.URL){
          if(cancellbl!==""){
              MVCApp.Toolbox.common.customAlert(gblInStoreModeDetails.storeMessage,"",constants.ALERT_TYPE_CONFIRMATION,notificationHandler,gblInStoreModeDetails.yesLabel,gblInStoreModeDetails.noLabel);
          }else{
              MVCApp.Toolbox.common.customAlert(gblInStoreModeDetails.storeMessage,"",constants.ALERT_TYPE_INFO,notificationHandler,gblInStoreModeDetails.yesLabel,"");
          }
      }else{
        MVCApp.Toolbox.common.customAlertNoTitle(gblInStoreModeDetails.storeMessage,"");
      }
    }
}

function onlinenotificationbeacons(notificationobject,actionid){
  var cancellbl= gblInStoreModeDetails.noLabel||"";
  var localNofificationType=gblInStoreModeDetails.notificationType;
  //#ifdef iphone
  if(gblEntryType=="beacon"){
    var cust360Obj=MVCApp.Customer360.getGlobalAttributes();
    cust360Obj.launchType="beacon";
    cust360Obj.isFirstLaunch="false";
    if(gblApplicationState=="inactive"){
      MVCApp.Customer360.sendInteractionEvent("Launch",cust360Obj);
    }
  }
  //#endif
  appLaunchCust360Sent=true;
  kony.print("online notifications fired"+JSON.stringify(notificationobject));
  if(gblInStoreModeDetails.storeMessage){
    if(gblInStoreModeDetails.URL){
        if(cancellbl!==""){
            MVCApp.Toolbox.common.customAlert(gblInStoreModeDetails.storeMessage,"",constants.ALERT_TYPE_CONFIRMATION,notificationHandler,gblInStoreModeDetails.yesLabel,gblInStoreModeDetails.noLabel);
        }else{
            MVCApp.Toolbox.common.customAlert(gblInStoreModeDetails.storeMessage,"",constants.ALERT_TYPE_INFO,notificationHandler,gblInStoreModeDetails.yesLabel,"");
        }
    }else{
      MVCApp.Toolbox.common.customAlertNoTitle(gblInStoreModeDetails.storeMessage,"");
    }
  }
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  if(gblAppinBackground){
    sendAppLaunchOnLocalNotification(localNofificationType);
  }
}

Date.prototype.addSeconds = function(seconds) {
    this.setSeconds(this.getSeconds() + seconds);
    return this;
};
function add10SecondsToNow(){
	var newdate = new Date();
	kony.print("Current Time:"+newdate);
	var now = newdate.addSeconds(10);
	 year = "" + now.getFullYear();
  	 month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
     day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
     hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
     minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
     second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
     var uDate= year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
     return uDate;
}

function setIphoneConfig(){
  var accept = kony.notificationsettings.createAction({
        "id": "Accept",
        "label": "Accept",
        "pspConfig": {
            "activationMode": kony.notificationsettings.ACTIVATION_MODE_FORWARDS,
            "authenticationRequired": true,
            "destructive": true,
            "visibleOn" : kony.notificationsettings.BOTH
        }
    });
    var reject = kony.notificationsettings.createAction({
        "id": "Reject",
        "label": "Reject",
        "pspConfig": {
            "activationMode": kony.notificationsettings.ACTIVATION_MODE_BACKWARDS,
            "authenticationRequired": false,
            "destructive": false,
            "visibleOn" : kony.notificationsettings.BOTH
        }
    });
	var decline = kony.notificationsettings.createAction({
        "id": "Decline",
        "label": "Decline",
        "pspConfig": {
            "activationMode": kony.notificationsettings.ACTIVATION_MODE_BACKWARDS,
            "authenticationRequired": true,
            "destructive": false,
            "visibleOn" : kony.notificationsettings.BOTH
        }
    });
    
    //Using kony.notificationsettings.createCategory
    var defaultActionContextArr = [accept, reject, decline];
    var minimalActionContextArr = [accept, reject];

    var categoryObj = kony.notificationsettings.createCategory({
            "categoryId": "invitation",
            "actions": defaultActionContextArr,
            "pspConfig": {
                "minimalActions": minimalActionContextArr
            }
        });
        
   //Using kony.notificationsettings.registerCategory 

    var categoryArr = [categoryObj];
	kony.notificationsettings.registerCategory({
        "categories": categoryArr,
        "pspConfig": {
            "types": [0, 1, 2]
        }
    }); 
}
