
/**
 * PUBLIC
 * This is the controller for the RewardsProgramInfo form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var fromMoreFlagRewards = false;
var MVCApp = MVCApp || {};
MVCApp.RewardsProgramInfoController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.RewardsProgramInfoModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.RewardsProgramInfoView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function load(fromMoreFlag){
        fromMoreFlagRewards = fromMoreFlag;
        MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("RewardsProgramInfo Controller.load");
      	init();
      	if(gblBrightNessrewardsValue > 0){
      	var createInstanceObject = new bright.createInstance();
      	 var returnedValueStr=gblBrightNessrewardsValue+"";	
    	 MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValueStr);
      	 createInstanceObject.setBrightness(returnedValueStr);
          }
        var requestParams = {};
        var lang = kony.store.getItem("languageSelected");
		 if(lang!=null && lang!==undefined && lang.indexOf("_")>0){
			 var lan= lang.replace("_","-");
			 requestParams.appLocale=lan;
			 
		 }
      	requestParams.refine = MVCApp.serviceConstants.rewardInfoRefine;
      	requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        model.loadData(view.updateScreen,requestParams);
    }
          		
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init: init,
    	load: load
    };
});

