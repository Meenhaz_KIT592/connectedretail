
/**
 * PUBLIC
 * This is the controller for the ProjectsList form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProjectsListController = (function(){
  var lServiceRequestObj = {};
  var _prodCountFromImgSearch=0;
  var _projCountFromImgSearch=0;
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction

  /**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
  function init(theme,locale){
    if (_isInit===true) return;

    //Init the model which exposes the backend services to this controller
    model = new MVCApp.ProjectsListModel();

    //Enables us to use different UI interactions depending on form factor
    view = new MVCApp.ProjectsListView();
    //}

    //Bind events to UI
    view.bindEvents();

    _isInit = true;
  }
  var _params;
  var fromSearchFlag=false;
  var _isFromImageSearch=false;
  function _getRequestParams(){
    return _params;
  }
  function _setRequestParams(reqParams){
    _params=reqParams;
  }
  function _checkIfFromSearch(){
    return fromSearchFlag;
  }
  function _setFromSearch(fromSearch){
    fromSearchFlag=fromSearch;
  }
  function getQueryString(){
    var queryString=_getRequestParams().q;
    return queryString;
  }
  var _query="";
  function setQuery(query){
    _query=query;
  }
  function getQuery(){
    return _query;
  }
  var queryText="";
  function getQueryText(){
    return queryText;
  }
  function setQueryText(value){
    queryText=value;
  }
  /**
	 * PUBLIC
	 * Open the form.
	 */
  var _total=0;
  var _start=0;
  function setTotal(total){
    _total=total;
  }
  function getTotal(){
    return _total;
  }

  function setStart(start){
    _start=start;
  }
  var backParamsVal={};
  function setBackParams(paramsBack){
    backParamsVal=paramsBack;
  }
  function getBackParams(){
    return backParamsVal;
  }
  var storeID="";
  var isStoreModeAlreadyEnabled = false;
  function setStoreModeStatus(pIsStoreModeAlreadyEnabled){
    isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
  }
  function getStoreModeStatus(){
    return isStoreModeAlreadyEnabled;
  }
  function setStoreID(storeNo){
    storeID=storeNo;
  }
  function getStoreID(){
    return storeID;
  }
  
  function updateScreen(pProjectsData,pIsFromSearch){
    view.updateScreen(pProjectsData,pIsFromSearch);
  }
  
  function load(query,fromSearch,fromRefine,refineString){
    lServiceRequestObj.query = query;
    lServiceRequestObj.fromSearch = fromSearch;
    lServiceRequestObj.fromRefine = fromRefine;
    lServiceRequestObj.refineString = refineString;
    init();
    setFromRefineFlag(false);
    view.blankPageShow();
    var backParams={};
    backParams.query=query;
    backParams.fromSearch=fromSearch;
    backParams.fromRefine=fromRefine;
    backParams.refineString=refineString;
    setBackParams(backParams);
    _setFromSearch(fromSearch);
    MVCApp.Toolbox.common.showLoadingIndicator("");
    kony.print("ProjectsList Controller.load");
  
    var fromRefinePage=fromRefine||false;
    var requestParams = MVCApp.Service.getCommonInputParamaters();
    if(fromRefinePage){
      requestParams.refineString=refineString;
    }else{
      requestParams.refineString=fromSearch ? MVCApp.Service.Constants.SearchProjects.refineString : "refine=cgid="+query.id;
    }
    requestParams.q=fromSearch ? query :"";
    requestParams.expand=MVCApp.Service.Constants.Search.expand;
    requestParams.sort="";// the sort value has been removed
    requestParams.start=MVCApp.Service.Constants.Search.start;
    requestParams.count=MVCApp.Service.Constants.Search.count;
    var storeID="";
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeID = gblInStoreModeDetails.storeID;
      setStoreModeStatus(true);
    }else{
      if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
        var locDetailsStr=kony.store.getItem("myLocationDetails");
        try {
          var LocDetails=JSON.parse(locDetailsStr);
          storeID=LocDetails.clientkey;
        } catch(e) {
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlProjectsList.js on load for locDetailsStr:" + JSON.stringify(e)}]);
          storeID="";
          if(e){
            TealiumManager.trackEvent("POI Parse Exception While Loading PJLP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }else{
        storeID="";
      }
    }
    setStoreID(storeID);
    gblTealiumTagObj.storeid = storeID;
    _setRequestParams(requestParams);
    setQuery(query);
    setQueryText(query);
    frmProjectsList.segProjectList.removeAll();
    frmProjectsList.flxRefineSearchWrap.isVisible = false;//flxCategory
    frmProjectsList.flxCategory.isVisible = false;//flxCategory
    MVCApp.getSortProjectController().setEntryQueryParam(requestParams.q);
    var count=0;
    model.showSearchResults(view.updateScreen,requestParams,MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchProduct,fromSearch,count);
  }
  function loadFromVoiceSearch(query,refineString){
    lServiceRequestObj.query = query;
    lServiceRequestObj.fromSearch = true;
    lServiceRequestObj.fromRefine = false;
    lServiceRequestObj.refineString = refineString;
    setEntryType("VoiceSearch");
    init();
    setFromRefineFlag(false);
    view.blankPageShow();
    var backParams={};
    backParams.query=query;
    backParams.fromSearch=true;
    backParams.fromRefine=false;
    backParams.refineString=refineString;
    setBackParams(backParams);
    _setFromSearch(true);
    MVCApp.Toolbox.common.showLoadingIndicator("");
    kony.print("ProjectsList Controller.load");
  
    var fromRefinePage=false;
    var requestParams = MVCApp.Service.getCommonInputParamaters();
    requestParams.refineString=MVCApp.Service.Constants.SearchProjects.refineString;
    requestParams.q=query||"";
    requestParams.expand=MVCApp.Service.Constants.Search.expand;
    requestParams.sort="";// the sort value has been removed
    requestParams.start=MVCApp.Service.Constants.Search.start;
    requestParams.count=MVCApp.Service.Constants.Search.count;
    var storeID="";
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeID = gblInStoreModeDetails.storeID;
      setStoreModeStatus(true);
    }else{
      if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
        var locDetailsStr=kony.store.getItem("myLocationDetails");
        try {
          var LocDetails=JSON.parse(locDetailsStr);
          storeID=LocDetails.clientkey;
        } catch(e) {
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlProjectsList.js on load for locDetailsStr:" + JSON.stringify(e)}]);
          storeID="";
          if(e){
            TealiumManager.trackEvent("POI Parse Exception While Loading PJLP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }else{
        storeID="";
      }
    }
    setStoreID(storeID);
    gblTealiumTagObj.storeid = storeID;
    _setRequestParams(requestParams);
    setQuery(query);
    setQueryText(query);
    frmProjectsList.segProjectList.removeAll();
    frmProjectsList.flxRefineSearchWrap.isVisible = false;//flxCategory
    frmProjectsList.flxCategory.isVisible = false;//flxCategory
    MVCApp.getSortProjectController().setEntryQueryParam(requestParams.q);
    var count=0;
    model.showSearchResults(view.updateScreen,requestParams,MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchProduct,true,count);
  }
  function checkIfRequired(){
    if((_start+25)>_total){
      return false;
    }else{
      return true;
    }
  }

  function showMore(){
    if(checkIfRequired()){
      var requestParams= _getRequestParams();
      requestParams.start=requestParams.start + MVCApp.Service.Constants.Search.count;
      var count=requestParams.start;
      MVCApp.Toolbox.common.showLoadingIndicator("");
      model.showSearchResults(view.showMore,requestParams,MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchProduct,_checkIfFromSearch(),count);
    }

  }
  function loadForSortRefine(query,refineString){
    MVCApp.Toolbox.common.showLoadingIndicator("");
    var requestParams = MVCApp.Service.getCommonInputParamaters();
    requestParams.q=query;
    requestParams.refineString=refineString;
    requestParams.expand=MVCApp.Service.Constants.Search.expand;
    requestParams.sort="";//MVCApp.Service.Constants.Search.sort;
    requestParams.start=MVCApp.Service.Constants.Search.start;
    requestParams.count=MVCApp.Service.Constants.Search.count;
    _setRequestParams(requestParams);
    var fromSearch=_checkIfFromSearch();
    setQuery(query);
    var count=0;
    model.refreshForSortRefine(MVCApp.getSortProjectController().loadNext,requestParams,MVCApp.serviceType.DemandwareService,MVCApp.serviceEndPoints.searchProduct,fromSearch,count);
  }
  function loadListWOService(dataObj){
    var fromSearch=_checkIfFromSearch();
    var count=0;
    view.updateScreen(dataObj,fromSearch,count);
  }


  function showUI(){
    init();
    view.showUI();
  }
  
  var refineCount=0;
  function setRefineCount(value){
    refineCount=value;
  }
  function getRefineCount(){
    return refineCount;
  }
  var productCount=0;
  function getProductCount(){
    return productCount;
  }
  function setProductCount(value){
    productCount=value;
  }
  var fromRefineFlag=false;
  function setFromRefineFlag(value){
    fromRefineFlag=false;
  }
  function getFromRefineFlag(){
    return fromRefineFlag;
  }
  var queryStringSearch="";
  function getQueryStringForSearch(){
    return queryStringSearch;
  }
  function setQueryStringForSearch(value){
    queryStringSearch=value;
  }

  function updateProductCount(totalProducts) {
    view.updateProductCount(totalProducts);
  }
  
  function setFromImageSearch(value){
    _isFromImageSearch=value;
  }
  function getFromImageSearch(){
    return _isFromImageSearch;
  }
  function getProductsCountFromImageSearch(){
    return _prodCountFromImgSearch;
  }
  function setProductsCountFromImageSearch(value){
    _prodCountFromImgSearch=value;
  }
  function getProjectsCountFromImageSearch(){
    return _projCountFromImgSearch;
  }
  function setProjectsCountFromImageSearch(value){
    _projCountFromImgSearch=value;
  }
  
  var entryType="";
  function getEntryType(){
    return entryType;
  }
  function setEntryType(value){
    entryType=value;
  }
  var projectIds=[];
  function getProjectIds(){
    return projectIds;
  }
  function setProjectIds(values){
    projectIds=values;
  }
  
  /**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

  //Here we expose the public variables and functions
  return {
    init: init,
    load: load,
    getQuery:getQuery,
    showMore:showMore,
    showUI:showUI,
    setTotal:setTotal,
    getTotal:getTotal,
    setStart:setStart,
    loadForSortRefine:loadForSortRefine,
    loadListWOService:loadListWOService,
    setRefineCount:setRefineCount,
    getRefineCount:getRefineCount,
    _checkIfFromSearch:_checkIfFromSearch,
    _getRequestParams:_getRequestParams,
    getBackParams:getBackParams,
    setBackParams:setBackParams,
    getStoreID:getStoreID,
    setStoreID:setStoreID,
    setQueryText:setQueryText,
    getQueryText:getQueryText,
    setProductCount:setProductCount,
    getProductCount:getProductCount,
    getFromRefineFlag:getFromRefineFlag,
    setFromRefineFlag:setFromRefineFlag,
    setQueryStringForSearch:setQueryStringForSearch,
    getQueryStringForSearch:getQueryStringForSearch,
    getQueryString:getQueryString,
    lServiceRequestObj: lServiceRequestObj,
    getStoreModeStatus: getStoreModeStatus,
    updateScreen: updateScreen,
    updateProductCount: updateProductCount,
    setFromImageSearch:setFromImageSearch,
    getFromImageSearch:getFromImageSearch,
    setProductsCountFromImageSearch:setProductsCountFromImageSearch,
    getProductsCountFromImageSearch:getProductsCountFromImageSearch,
    getProjectsCountFromImageSearch:getProjectsCountFromImageSearch,
    setProjectsCountFromImageSearch:setProjectsCountFromImageSearch,
    getProjectIds:getProjectIds,
    setProjectIds:setProjectIds,
    getEntryType:getEntryType,
    setEntryType:setEntryType,
    loadFromVoiceSearch:loadFromVoiceSearch
  };
});

