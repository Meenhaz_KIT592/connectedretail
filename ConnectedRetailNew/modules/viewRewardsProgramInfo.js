/**
 * PUBLIC
 * This is the view for the RewardsProgramInfo form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var rewardsInfoFlag = false;
var MVCApp = MVCApp || {};

MVCApp.RewardsProgramInfoView = (function(){
var prevFormName="";  
  /**
	 * PUBLIC
	 * Open the RewardsProgramInfo form
	 */
  
  function show(){
  var createAccountResObj = kony.store.getItem("userObj");
  if( createAccountResObj ==="" || createAccountResObj ===null ){
	frmRewardsProgramInfo.btnJoinRewards.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProgramInfo.joinMichaelRewards");
  }else{
     var c_loyaltyPhoneNo = createAccountResObj.c_loyaltyPhoneNo || "";
    if(c_loyaltyPhoneNo ===""){
      frmRewardsProgramInfo.btnJoinRewards.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProgramInfo.joinMichaelRewards");
    }else{
      frmRewardsProgramInfo.btnJoinRewards.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProgramInfo.viewMyRewards");
    }
  }
   //#ifdef android
   MVCApp.Toolbox.common.setTransition(frmRewardsProgramInfo,3);
   //#endif
  frmRewardsProgramInfo.show();
   MVCApp.tabbar.bindEvents(frmRewardsProgramInfo);
	MVCApp.tabbar.bindIcons(frmRewardsProgramInfo,"frmMore"); 
 }
  function savePreviousScreenForm(){
    MVCApp.Toolbox.common.destroyStoreLoc();
    var prevForm = kony.application.getPreviousForm().id;
    if(prevForm =="frmCreateAccountStep2"){
      prevFormName = prevForm;
    }else if(prevForm =="frmSignIn"){
      prevFormName = prevForm;
    }else if(prevForm =="frmMore"){
      prevFormName = prevForm;
    }else if(prevForm =="frmCreateAccount"){
      prevFormName = prevForm;
    }else if(prevForm === "frmHome"){
      prevFormName = prevForm;
    }
    
    var lTealiumTagObj = gblTealiumTagObj;
    lTealiumTagObj.page_id = "Rewards Program Information: Michaels Mobile App";
    lTealiumTagObj.page_name = "Rewards Program Information: Michaels Mobile App";
    lTealiumTagObj.page_type = "Rewards";
    lTealiumTagObj.page_category_name = "Rewards";
    lTealiumTagObj.page_category = "Rewards";
    TealiumManager.trackView("Rewards Program Information screen", lTealiumTagObj);
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    if(!formFromDeeplink)
    	MVCApp.Customer360.sendInteractionEvent("RewardsProgramInfo", cust360Obj);
    else
      formFromDeeplink=false;
  }
   
 function showPreviousScreen(){
   if(prevFormName == "frmCreateAccountStep2"){
     //#ifdef android
   	MVCApp.Toolbox.common.setTransition(frmCreateAccountStep2,2);
   //#endif
     frmCreateAccountStep2.show(); 
   }else if(prevFormName == "frmSignIn"){
      //#ifdef android
   	MVCApp.Toolbox.common.setTransition(frmSignIn,2);
   //#endif
     frmSignIn.show(); 
     MVCApp.tabbar.bindEvents(frmSignIn);
	MVCApp.tabbar.bindIcons(frmSignIn,"frmMore");
   }else if(prevFormName == "frmMore"){
     //#ifdef android
   MVCApp.Toolbox.common.setTransition(frmMore,2);
   //#endif
     frmMore.show(); 
   }
   else if(prevFormName == "frmCreateAccount"){
     if(MVCApp.Toolbox.common.getRegion() === "US"){
        var url = "Account";
     MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + url);

     } else {
       //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmCreateAccount,2);
    //#endif
      frmCreateAccount.show(); 
     }      
        }else if(prevFormName === "frmHome"){
     MVCApp.getHomeController().load();
   }else if(entryBrowserFormName=="frmCartView"){
		MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
     }else if(entryBrowserFormName==="frmWebView"){
       var urlBack= MVCApp.Toolbox.common.findWebViewUrlToGoBack();
       MVCApp.getProductsListWebController().loadWebView(urlBack);
     }
 }
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function bindEvents(){
   frmRewardsProgramInfo.postShow = savePreviousScreenForm;
     MVCApp.Toolbox.common.setBrightness("frmRewardsProgramInfo");
   frmRewardsProgramInfo.btnHeaderLeft.onClick = function(){
     if(fromMoreFlagRewards){
       MVCApp.getMoreController().load();
     }else{
       showPreviousScreen();
     }
   };
   frmRewardsProgramInfo.btnJoinRewards.onClick = function(){
    var createAccountResObj = kony.store.getItem("userObj");
     if(createAccountResObj ==="" || createAccountResObj ===null) {
       MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProgramInfo.signInToJoinMichaelsRewards"), "" ,constants.ALERT_TYPE_CONFIRMATION,onClickSignInRewardsInfo,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.SignInBtn"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
     } else{
       if(frmRewardsProgramInfo.btnJoinRewards.text == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProgramInfo.joinMichaelRewards")){
         MVCApp.getJoinRewardsController().load(createAccountResObj);
       }else{
        MVCApp.getHomeController().loadCoupons(); 
       frmObject=frmRewardsProgramInfo;
       }
       
     }
   };
   frmRewardsProgramInfo.btnShare.onClick = function(){
     share(MVCApp.serviceConstants.rewardsShareURL);
   };
    frmRewardsProgramInfo.btnFAQ.onClick = function(){
       MVCApp.getBrowserController().load(MVCApp.serviceConstants.mobileRewardsFAQ);
    };
    frmRewardsProgramInfo.btnTermsandConditions.onClick = function(){
      MVCApp.getBrowserController().load(MVCApp.serviceConstants.rewardTermsandConditions);
    };
    
  }
  function onClickSignInRewardsInfo(responseValue){
    if(responseValue){
    rewardsInfoFlag = true;
      isFromShoppingList = false;
      fromCoupons=false;
    MVCApp.getSignInController().load();
    }
  }
  
  function updateScreen(results){
    if(results.hasOwnProperty("hits")){
      var rewardsInfo = results.hits ||[];
      if(rewardsInfo.length > 0){
        frmRewardsProgramInfo.imgBanner.src = rewardsInfo[0].c_image;
        frmRewardsProgramInfo.rtBannerTxt.text = rewardsInfo[0].name;
        var tempArray =[];
        for(var i=1;i<rewardsInfo.length;i++){
          var temp={};
          temp.name = rewardsInfo[i].name;
          temp.c_image =rewardsInfo[i].c_image;
          temp.c_body = rewardsInfo[i].c_body;
          tempArray.push(temp);
        }
        frmRewardsProgramInfo.segRewardsInfo.widgetDataMap ={
          imgLogo:"c_image",
          lblHeader:"name",
          rchTxtBody:"c_body"
        };
        frmRewardsProgramInfo.segRewardsInfo.data = tempArray;
      }else{
        alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.rtxtSearchNotFound"));
      }
      
    }else{
      alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.rtxtSearchNotFound"));
    }
    
    show();
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  function share(shareTxt){
    var shareArray = new Array(frmRewardsProgramInfo.lblFormTitle.text+"\n"+shareTxt, "", "",frmRewardsProgramInfo.lblFormTitle.text);
    if(kony.os.deviceInfo().name==="android"){
	var ShareTestObject = new shareFFI.ShareTest();
	//ShareTestObject.shareSomething(shareArray);
       requestPermissionAtRuntime(kony.os.RESOURCE_EXTERNAL_STORAGE,function(){ShareTestObject.shareSomething(shareArray);});
    }else{
	var ShareIphoneObject = new shareFFI.ShareIphone();
	ShareIphoneObject.ShareonSocialMedia(shareArray);
    }  
  }
  
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen
    };
  
});




