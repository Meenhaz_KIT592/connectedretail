
/**
 * PUBLIC
 * This is the controller for the PJDP form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
MVCApp.PJDPController = (function(){
  var lServiceRequestObj = {};
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.PJDPModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.PJDPView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  
  function showUpdateScreen(res)
  {
    view.updateScreen(res);
  }
  var storeIdNew="";
  var isStoreModeAlreadyEnabled = false;
  function setStoreModeStatus(pIsStoreModeAlreadyEnabled){
    isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
  }
  function getStoreModeStatus(){
    return isStoreModeAlreadyEnabled;
  }
  function getStoreID(){
    return storeIdNew;
  }
  function setStoreID(storeID){
    storeIdNew=storeID;
  }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  
  	function load(productId,fromPJDPFlag,formName){
      	lServiceRequestObj.productId = productId;
      	lServiceRequestObj.fromPJDPFlag = fromPJDPFlag;
      	lServiceRequestObj.formName = formName;
        MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("PJDP Controller.load");
      	init();
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        //requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParams.expand = MVCApp.serviceConstants.defaultProjectExpand;
        requestParams.productID =productId; 
        requestParams.all_images = "false";
      	setProjectIDForTracking(productId);
      	var storeNo ="";
      	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeNo = gblInStoreModeDetails.storeID;
          setStoreModeStatus(true);
        }else{
          var storeString=kony.store.getItem("myLocationDetails") ||"";
          if(storeString!=="" && storeString!=null && storeString!="null"){
            try {
              var storeObj=JSON.parse(storeString);
              storeNo =storeObj.clientkey;
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlPJDP.js on load for storeString:" + JSON.stringify(e)}]);
              storeNo = "";
              if(e){
                TealiumManager.trackEvent("POI Parse Exception In PJDP", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          }else{
          	storeNo ="";
          }
        }
      	setStoreID(storeNo);
      	gblTealiumTagObj.storeid = storeNo;
      	requestParams.store_id = storeNo;
        model.loadPJDP(view.updateScreen,requestParams,formName);
      	var prevScreenForm = kony.application.getPreviousForm() ||{};
      	var prevScreen=prevScreenForm.id||"";
        var query="";
    	if(prevScreen =="frmProjectsCategory" || prevScreen =="frmPJDP" ||  prevScreen === "frmRefineProjects"){
      	query=MVCApp.getProjectsListController().getQuery();
    	}else{
          query ="";
        }

      	view.setCategoryName(query);
    }
  function  addToProductListCallBack(results)
  {
     view.addToProductListCallBack(results);
  } 
  
  function addToProjectListCallBack(results)
  {
     view.addToProjectListCallBack(results);
  }
  var queryString="";
  function setQueryString(value){
    queryString=value;
  }
  function getQueryString(){
    return queryString;
  }
  var projectIDForTracking="";
  function setProjectIDForTracking(value){
    projectIDForTracking=value;
  }
  function getProjectIDForTracking(){
    return projectIDForTracking;
  }
  var entryType="";
  var entrySearchType="";
  function setSearchEntryType(value){
    entrySearchType=value;
  }
  function setEntryType(value){
    entryType=value;
    if(value=="pjlp"){
      entrySearchType= MVCApp.getProjectsListController().getEntryType();
      setSearchEntryType(entrySearchType);
    }else{
      setSearchEntryType(value);
    }
  }
  function getEntryType(){
    return entryType;
  }
  function getSearchEntryType(){
    return entrySearchType;
  }
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
      init: init,
      load: load,
      addToProductListCallBack:addToProductListCallBack,
      showUpdateScreen:showUpdateScreen,
      addToProjectListCallBack:addToProjectListCallBack,
      getStoreID:getStoreID,
      setStoreID:setStoreID,
      setQueryString:setQueryString,
      getQueryString:getQueryString,
      lServiceRequestObj: lServiceRequestObj,
      getStoreModeStatus: getStoreModeStatus,
      getProjectIDForTracking:getProjectIDForTracking,
      setProjectIDForTracking:setProjectIDForTracking,
      setEntryType:setEntryType,
      getEntryType:getEntryType,
      getSearchEntryType:getSearchEntryType
    };
});

