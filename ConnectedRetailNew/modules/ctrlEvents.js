
/**
 * PUBLIC
 * This is the controller for the Events form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var fromMoreFlag = false;
var MVCApp = MVCApp || {};
MVCApp.EventsController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
  
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.EventsModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.EventsView();
  	   
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	
  	function setFromMoreFlag(fromMore){
     fromMoreFlag = fromMore;
    }
  	
  	function getFromMoreFlag(){
      return fromMoreFlag;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function load(fromMore){
		setFromMoreFlag(fromMore);
		init();
      	var requestParams = MVCApp.Service.getCommonInputParamaters();
        var language=kony.store.getItem("languageSelected");
  
		if(MVCApp.Toolbox.common.getRegion()==="US"){
                        var language=kony.store.getItem("languageSelected");
            requestParams.region="US";
              if(language != null && language !="" ){
                   requestParams.language=language;
              }
		}else{
            requestParams.region="Canada";
              if(language != null && language !="" ){
                   requestParams.language=language;
              }

              

		}
		if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          frmEvents.segEvents.removeAll();
         	if(fromMore===true){
            frmEvents.show();
           }
          MVCApp.Toolbox.common.showLoadingIndicator("");
          requestParams.store_id = gblInStoreModeDetails.storeID;
        }else{
          var storeString=kony.store.getItem("myLocationDetails") ||"";
          if(storeString != ""){
            frmEvents.segEvents.removeAll();
           	if(fromMore===true){
            frmEvents.show();
           }
            MVCApp.Toolbox.common.showLoadingIndicator("");
            try {
              var storeObj=JSON.parse(storeString);
              requestParams.store_id = storeObj.clientkey;
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlEvents.js on lInStoreModeDetails for storeString:" + JSON.stringify(e)}]);
              if(e){
                TealiumManager.trackEvent("POI Parse Exception in Events Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          } else{
              var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"events"};
              whenStoreNotSet = JSON.stringify(whenStoreNotSet);
              kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
              MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreEvents") ,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
              return;
          }
        }
      	if(fromMore===true)
        {
          model.getEvents(view.updateScreen,requestParams);
        }else{
		model.getEvents(view.updateScreen,requestParams,fromMore);
        }
      	view.clearForm();
    }
  	var eventResult=[];
  	function setResultEvents(value){
      eventResult=value;
    }
  	function getResultEvents(){
      return eventResult;
    }
  var entryFromWebView=false;
  function setFormEntryFromCartOrWebView(value){
    entryFromWebView=value;
  }
  function getFormEntryFromCartOrWebView(){
    return entryFromWebView;
  }
  
  	
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init: init,
    	load: load,
      	setFromMoreFlag:setFromMoreFlag,
      	getFromMoreFlag:getFromMoreFlag,
      	setResultEvents:setResultEvents,
      	getResultEvents:getResultEvents,
      	setFormEntryFromCartOrWebView:setFormEntryFromCartOrWebView,
      	getFormEntryFromCartOrWebView:getFormEntryFromCartOrWebView
    };
});