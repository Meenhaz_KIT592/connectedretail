/**
 * PUBLIC
 * This is the view for the Sign In form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};

MVCApp.SortProductView = (function(){
  var gblsortArr=[];
  var gblCategoryField=[];
  var gblRefineArr=[];
  var gblSubRefineArr=[];
  var isSortChanged=false;
  var gblSelectedRefine=[];
  var gblAttribute=[];
  var refineOpen="";
  var latestRefSelected="";
  var extraFiltersAdded=false;

  /**
	 * PUBLIC
	 * Open the More form
	 */

  function show(){
    latestRefSelected="";
    refineOpen="";
    extraFiltersAdded=false;
    if(frmSortAndRefine.flxFooterWrap != undefined &&frmSortAndRefine.flxFooterWrap != null){
      frmSortAndRefine.flxFooterWrap.bottom = "-54dp";
    }
    //#ifdef android
		MVCApp.Toolbox.common.setTransition(frmSortAndRefine,2);
   //#endif	
    frmSortAndRefine.show();
  } 


  /**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function backToProducts(){
    if(!extraFiltersAdded)
    	MVCApp.getProductsListController().backToScreen();
    else{
      var reqParams=MVCApp.getSortProductController().goToDefaultProdList();
    }
  }
  function setFiltersToDefault(){
    var refinementArr=[];
    MVCApp.getSortProductController().setSelectedRefinesAndAttributes(refinementArr);
    MVCApp.getSortProductController().removeAllFilters();
  }
  function onClickDoneToProducts(){
    if(isSortChanged){
      MVCApp.getSortProductController().goToProductList(); 
    }else{
      MVCApp.getSortProductController().loadListWithoutService();
    }
  }

  function onClickTickCategory(eventObj){
    if(eventObj.text=="D"){
      showCategoryFields();
    }else{
      hideCategoryFields();
    }
  }
  function showSaleClearance(){
    frmSortAndRefine.flxContainerSaleClearance.setVisibility(true);
    frmSortAndRefine.lblTickSaleClearance.text="U";
  }

  function hideSaleClearance(){
    frmSortAndRefine.flxContainerSaleClearance.setVisibility(false);
    frmSortAndRefine.lblTickSaleClearance.text="D";
  }
  function onClickTickSale(eventObj){
    if(eventObj.text=="D"){
      showSaleClearance();
    }else{
      hideSaleClearance();
    }
  }
  function showShopBy(){
    frmSortAndRefine.flxContainerShopOptions.setVisibility(true);
    frmSortAndRefine.btnExpandShopBy.text="U";
  }
  function hideShopBy(){
    frmSortAndRefine.flxContainerShopOptions.setVisibility(false);
    frmSortAndRefine.btnExpandShopBy.text="D";
  }
  function expandCollapseShop(eventObj){
    if(eventObj.text=="D"){
      showShopBy();
    }else{
      hideShopBy();
    }
  }
  function executeSaleforRefineString(saleorClearance){
    var refinementArr=MVCApp.getSortProductController().getSelectedRefinesAndAttributes();
    var saleFound=false;
    var refineArr=[];
    refineArr.push(saleorClearance);
    for(var i=0;i<refinementArr.length;i++){
      if(refinementArr[i].attribute=="pmid"){
        var refineSale=refinementArr[i].refineValues[0];
        if(refineSale==saleorClearance){
          saleFound=true;
        }
        refinementArr.splice(i,1);
      } 
    }
    if(!saleFound){
      var record={"attribute":"pmid","refineValues":refineArr};
      refinementArr.push(record);
    }
    kony.print("refineMent arr "+JSON.stringify(refinementArr));
    MVCApp.getSortProductController().setSelectedRefinesAndAttributes(refinementArr);
    MVCApp.getSortProductController().prepareForJustServiceCall();
  }

  function selectSaleforRefineString(){
    executeSaleforRefineString("onsale");
  }

  function selectClearanceforRefineString(){
    executeSaleforRefineString("onclearance");
  }
  function bindEvents(){
    frmSortAndRefine.postShow = function(){
      MVCApp.Toolbox.common.destroyStoreLoc();

      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Sort and Refine: Michaels Mobile App";
      lTealiumTagObj.page_name = "Sort and Refine: Michaels Mobile App";
      lTealiumTagObj.page_type = "Filter";
      lTealiumTagObj.page_category_name = "Sort and Refine";
      lTealiumTagObj.page_category = "Sort and Refine";
      TealiumManager.trackView("Sort and Refine Screen", lTealiumTagObj);
	};
    frmSortAndRefine.btnCancel.onClick=backToProducts;
    frmSortAndRefine.lblTickCategory.onTouchEnd=onClickTickCategory;
    frmSortAndRefine.btnClearAll.onClick=setFiltersToDefault;
    frmSortAndRefine.flxProjects.onClick=MVCApp.getSortProductController().goToProjects;
    frmSortAndRefine.btnDone.onClick=onClickDoneToProducts;
    frmSortAndRefine.lblTickSaleClearance.onTouchEnd=onClickTickSale;
    frmSortAndRefine.btnExpandRefine.onClick=expandCollapseRefine;
    frmSortAndRefine.btnExpandShopBy.onClick=expandCollapseShop;
    frmSortAndRefine.btnExpandSortBy.onClick =expandCollapseSortFields;
    frmSortAndRefine.flxSaleCont.onClick=selectSaleforRefineString;
    frmSortAndRefine.flxClearanceCont.onClick=selectClearanceforRefineString;
  }

  function updateScreen(results){
    show();
  }
  function changeSort(eventObj){
    var idSort=eventObj.id.split("flxSortField")[1];
    var sortIdSelected=gblsortArr[idSort].id;
    updateSelectedSortOptions(sortIdSelected);
    var alreadySelectedSort=MVCApp.getSortProductController().getSelectedSort();
    if(sortIdSelected===alreadySelectedSort){
      kony.print("do nothing");
    }else{
      MVCApp.getSortProductController().setSelectedSort(sortIdSelected);
      isSortChanged=true;
      extraFiltersAdded=true;
    }
    
    if(sortIdSelected){
      var lEventName = gblsortArr[idSort].label + " Sort by Feature";
      var lElementId = "Sort By: " + gblsortArr[idSort].label;
      TealiumManager.trackEvent(lEventName, {element_id:lElementId,element_category:"Sort By"});
    }
  }


  function selectCategories(eventObj){
    var idCategory=eventObj.id.split("flxCategory")[1];
    var catIdSelected=gblCategoryField[idCategory];
    var alreadySelectedCategory=MVCApp.getSortProductController().getSelectedCategory();
    if(catIdSelected===alreadySelectedCategory){
      MVCApp.getSortProductController().removeSelectedCategory();
    }else{
      MVCApp.getSortProductController().setSelectedCategory(catIdSelected);
    }
    MVCApp.getSortProductController().prepareForJustServiceCall();
    latestRefSelected="";
  }
  function updateSelectedCategories(selCatId){
    for(var i=0;i<gblCategoryField.length;i++){
      if(selCatId==gblCategoryField[i]){
        frmSortAndRefine["lblCategory"+i].skin="sknLbl24pxRedGothamBook";
        frmSortAndRefine["lblCategorySelected"+i].setVisibility(true);
      }else{
        frmSortAndRefine["lblCategory"+i].skin="sknLblGothamBook24pxBlack";
        frmSortAndRefine["lblCategorySelected"+i].setVisibility(false);
      }
    }
    extraFiltersAdded=true;
  }
  function updateSortingFileds(sortingArr){
    frmSortAndRefine.flxContainerSortOptions.removeAll();
    gblsortArr=[];
    if(sortingArr.length !==0){
      for(var i=0;i<sortingArr.length;i++){
        var temp=sortingArr[i];
        var id=temp.id;
        var flxSortField1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "30dp",
          "id": "flxSortField"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1,
          "onClick":changeSort
        }, {}, {});
        flxSortField1.setDefaultUnit(kony.flex.DP);
        var lblSortField1 = new kony.ui.Label({
          "centerY": "50%",
          "id": "lblSortField1"+i,
          "isVisible": true,
          "left": "10dp",
          "skin": "sknLblGothamBold24pxBlack",
          "text": temp.label,
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": kony.flex.USE_PREFFERED_SIZE,
          "zIndex": 1
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        var lblSelectedSort1 = new kony.ui.Image2({
          "right": "10dp",
          "height": "100%",
          "id": "lblSelectedSort"+i,
          "isVisible": false,
          "skin": "slImage",
          "src": "ic_tick_check.png",
          "top": "0dp",
          "width": "30dp",
          "zIndex": 2
        }, {
          "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {});
        var flxGapSort1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "1dp",
          "id": "flxGapSort"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "sknFlexBgBlackOpaque20",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1
        }, {}, {});
        flxGapSort1.add();
        gblsortArr.push({"id":temp.id,"label":temp.label});
        flxSortField1.add(lblSortField1, lblSelectedSort1);
        frmSortAndRefine.flxContainerSortOptions.add(flxSortField1,flxGapSort1);
      }

    }

  }
  function findAttributeId(headerCat){
    var attribute="";
    for(var i=0;i<gblRefineArr.length;i++){
      if(gblRefineArr[i].label==headerCat){
        attribute=gblRefineArr[i].attribute;
        return attribute;
      }
    }
    return attribute;
  }
  function findAttribteLabel(attrId){
    var attLabel="";
    for(var i=0;i<gblRefineArr.length;i++){
      if(gblRefineArr[i].attribute==attrId){
        attLabel=gblRefineArr[i].label;
        return attLabel;
      }
    }
    return attLabel;
  }
  function findIfrefinedAlready(refstring,refineAttr){
    var newString="";
    if(refineAttr.length!==0){
      for(var i=0;i<refineAttr.length;i++){
        if(refstring===refineAttr[i]){
          return true;
        }
      }
    }else{
      return false;
    }

  }
  function setAttributes(attribute){
    for(var i=0;i<gblAttribute.length;i++){
      if(gblAttribute[i]!==attribute){
        gblAttribute.push(attribute);
      }
    }
    MVCApp.getSortProductController().setAttributes(gblAttribute);
  }
  function removeRefinements(refstring,refineAttr){
    for(var i=0;i<refineAttr.length;i++){
      if(refstring===refineAttr[i]){
        refineAttr.splice(i,1);
      }
    }
    return refineAttr;
  }
  function selectRefineMents(eventObj){
    var idRef=eventObj.id.split("flxRefineSubCat")[1];
    var idStr=idRef.substring(idRef.length-2,idRef.length);
    var headerCat=idRef.substring(0,idRef.length-2);
    latestRefSelected=headerCat;
    var attrObjCat=findAttributeId(headerCat);
    var refstring="";
    var refineValuesSubCat=[];
    for(var k=0;k<gblSubRefineArr.length;k++){
      if(gblSubRefineArr[k].attr==attrObjCat){
        refineValuesSubCat=gblSubRefineArr[k].allValues;
      }
    }
    setAttributes(attrObjCat);
    kony.print("idStr is"+idStr);
    var idStrNum=Number(idStr);
    refstring=refineValuesSubCat[idStrNum];
    var refineArrAll=MVCApp.getSortProductController().getSelectedRefinesAndAttributes();
    kony.print("refine  old "+JSON.stringify(refineArrAll));
    var refineAttr=[];
    for(var i=0;i<refineArrAll.length;i++){
      var temp=refineArrAll[i] || "";
      var attribute=temp.attribute ||"";
      if(attribute===attrObjCat){
        var refinetemp=temp.refineValues||[];
        for(var j=0;j<refinetemp.length;j++){
          refineAttr.push(refinetemp[j]); 
        }
      }
    }
    if(refineAttr.length!==0){
      var refinementIfExists=findIfrefinedAlready(refstring,refineAttr);
      if(refinementIfExists){
        refineAttr=removeRefinements(refstring,refineAttr);
      }else{
        refineAttr.push(refstring);
      } 
    }
    var refineArrNew=MVCApp.getSortProductController().getSelectedRefinesAndAttributes();
    var foundKey=false;
    kony.print("refine Arr  "+JSON.stringify(refineAttr));
    if(refineArrNew.length!==0){
      for(var i=0;i<refineArrNew.length;i++){
        var tempNew=refineArrNew[i] ||"";
        var attriNew=refineArrNew[i].attribute;
        if(attriNew===attrObjCat){
          if(refineAttr.length!==0){
            tempNew.refineValues=refineAttr; 
          }else{
            refineArrNew.splice(i,1);
          }
          foundKey=true;
        }
      }
    }
    if(!foundKey){
      refineAttr.push(refstring);
      var record={"attribute":attrObjCat,"refineValues":refineAttr};
      refineArrNew.push(record);
    }

    kony.print("refine "+JSON.stringify(refineArrNew));
    MVCApp.getSortProductController().setSelectedRefinesAndAttributes(refineArrNew);
    MVCApp.getSortProductController().prepareForJustServiceCall();
    extraFiltersAdded=true;
  }
  function updateSubCategoryForRefine(refineInnerValues,headerCat,attribute){
    frmSortAndRefine["flxRefine"+headerCat].removeAll();
    var refineArr=[];
    if(refineInnerValues.length!==0){
      for(var i=0;i<refineInnerValues.length;i++){
        var temp=refineInnerValues[i];
        var count=i;
        refineArr.push(temp.value);
        if(i<10){
          count="0"+i;
        }
        var gapVisible=false;
        if(i+1===refineInnerValues.length){
          gapVisible=true;
        }
        var flxRefineSubCat1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "30dp",
          "id": "flxRefineSubCat"+headerCat+count,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1,
          "onClick":selectRefineMents
        }, {}, {});
        flxRefineSubCat1.setDefaultUnit(kony.flex.DP);
        var lblRefineSubCat1 = new kony.ui.Label({
          "centerY": "50%",
          "id": "lblRefineSubCat"+headerCat+count,
          "isVisible": true,
          "left": "20dp",
          "skin": "sknLblGothamBook24pxBlack",
          "text": temp.label+" ("+temp.hit_count+")",
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": kony.flex.USE_PREFFERED_SIZE,
          "zIndex": 1
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        var lblSelectedRefineSubCat1 = new kony.ui.Label({
          "centerY": "50%",
          "height": "100%",
          "id": "lblSelectedRefineSubCat"+headerCat+count,
          "isVisible": false,
          "right": "10dp",
          "skin": "sknLblIcon32pxRed",
          "text": "u",
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": "50dp",
          "zIndex": 2
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
          "padding": [0, 0, 1, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        var flxGapSubRefine1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "1dp",
          "id": "flxGapRefineSubCat"+count,
          "isVisible": gapVisible,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "sknFlexBgBlackOpaque20",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1
        }, {}, {});
        flxGapSubRefine1.add();
        flxRefineSubCat1.add(lblRefineSubCat1, lblSelectedRefineSubCat1);
        frmSortAndRefine["flxRefine"+headerCat].add(flxRefineSubCat1,flxGapSubRefine1);
      }
      var temp={"attr":attribute,"allValues":refineArr};
      gblSubRefineArr.push(temp);
    }
  }
  function showRefineSubContainer(idRefine){
    var idName=gblRefineArr[idRefine].label;
    kony.print("the names are"+"flxRefine"+idName +"lbl id is"+"lblRefineSelected"+idRefine);
    frmSortAndRefine["flxRefine"+idName].setVisibility(true);
    frmSortAndRefine["lblRefineSelected"+idRefine].text="U";
    if(refineOpen && refineOpen!="" && refineOpen!==idRefine){
      hideRefineSubContainer(refineOpen);
    }
    hideCategoryFields();
    refineOpen=idRefine;
    if(idName){
      var lEventName = "Refine by "+idName+" Feature";
      TealiumManager.trackEvent(lEventName, {element_id:idName,element_category:"Refine by"});
    }
  }

  function hideRefineSubContainer(idRefine){
    var idName=gblRefineArr[idRefine].label;
    kony.print("the names are"+"flxRefine"+idName +"lbl id is"+"lblRefineSelected"+idRefine);
    frmSortAndRefine["flxRefine"+idName].setVisibility(false);
    frmSortAndRefine["lblRefineSelected"+idRefine].text="D";
    refineOpen="";
  }
  function expandCollapseRefineSubCats(eventobj){
    var idRefine=eventobj.id.split("lblRefineSelected")[1];
    if(eventobj.text=="D"){
      showRefineSubContainer(idRefine);
    }else{
      hideRefineSubContainer(idRefine);
    }
  }
  function updateRefineFields(refineCatArr){
    // kony.print("the refine Category is"+JSON.stringify(refineCatArr));
    frmSortAndRefine.flxRefineContainer.removeAll();
    gblRefineArr=[];
    gblSubRefineArr=[];
    if(refineCatArr.length!==0){
      for(var i=0;i<refineCatArr.length;i++){
        var tempData=refineCatArr[i] ||{};
        var temp=tempData.data;
        var tempObj={"label":temp.label,
                     "attribute":temp.attribute_id};
        gblRefineArr.push(tempObj);
        var flxRefine1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "30dp",
          "id": "flxRefine"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1
        }, {}, {});
        flxRefine1.setDefaultUnit(kony.flex.DP);
        var lblRefine1 = new kony.ui.Label({
          "centerY": "50%",
          "height": "100%",
          "id": "lblRefine"+i,
          "isVisible": true,
          "left": "10dp",
          "skin": "sknLblGothamBold24pxBlack",
          "text": temp.label,
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": kony.flex.USE_PREFFERED_SIZE,
          "zIndex": 1
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        var lblRefineSelected1 = new kony.ui.Label({
          "centerY": "50%",
          "height": "100%",
          "id": "lblRefineSelected"+i,
          "isVisible": true,
          "right": "10dp",
          "skin": "sknLblIcon34px333333",
          "onTouchEnd":expandCollapseRefineSubCats,
          "text": "D",
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": "94%",
          "zIndex": 2
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        flxRefine1.add(lblRefine1, lblRefineSelected1);
        var flxGapRefine1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "1dp",
          "id": "flxGapCategory"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "sknFlexBgBlackOpaque20",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1
        }, {}, {});
        flxGapRefine1.setDefaultUnit(kony.flex.DP);
        flxGapRefine1.add();
        var flxSubcategoryRefineOptions = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
          "clipBounds": true,
          "id": "flxRefine"+temp.label,
          "isVisible": false,
          "layoutType": kony.flex.FLOW_VERTICAL,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1
        }, {}, {});
        flxSubcategoryRefineOptions.setDefaultUnit(kony.flex.DP);
        flxSubcategoryRefineOptions.add();
        frmSortAndRefine.flxRefineContainer.add(flxRefine1, flxGapRefine1,flxSubcategoryRefineOptions);
        updateSubCategoryForRefine(temp.values||[],temp.label,temp.attribute_id);
      }
    }
  }
  function updateProjectProductFileds(otherCount){
      frmSortAndRefine.flxProjects.setVisibility(false);

  }
  function updateCategoryFields(categoryArr){
    frmSortAndRefine.flxContainerCategoryOptions.removeAll();
    gblCategoryField=[];
    if(categoryArr.length!==0){
      var dataCat=categoryArr[0].data ||{};
      var innerValues=dataCat.values ||[];
      for(var i=0;i<innerValues.length;i++){
        var temp=innerValues[i];
        gblCategoryField.push(temp.value);
        var flxCategory1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "30dp",
          "id": "flxCategory"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1,
          "onClick":selectCategories
        }, {}, {});
        flxCategory1.setDefaultUnit(kony.flex.DP);
        var lblCategory1 = new kony.ui.Label({
          "centerY": "50%",
          "height": "100%",
          "id": "lblCategory"+i,
          "isVisible": true,
          "left": "20dp",
          "skin": "sknLblGothamBook24pxBlack",
          "text": temp.label+" ("+temp.hit_count+")",
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": kony.flex.USE_PREFFERED_SIZE,
          "zIndex": 1
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        var lblCategorySelected1 = new kony.ui.Label({
          "centerY": "50%",
          "height": "100%",
          "id": "lblCategorySelected"+i,
          "isVisible": false,
          "right": "10dp",
          "skin": "sknLblIcon32pxRed",
          "text": "u",
          "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
          },
          "width": "50dp",
          "zIndex": 1
        }, {
          "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
        }, {
          "textCopyable": false
        });
        flxCategory1.add(lblCategory1, lblCategorySelected1);
        var flxGapCategory1 = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "1dp",
          "id": "flxGapCategory"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "sknFlexBgBlackOpaque20",
          "top": "0dp",
          "width": "100%",
          "zIndex": 1
        }, {}, {});
        flxGapCategory1.setDefaultUnit(kony.flex.DP);
        flxGapCategory1.add();
        frmSortAndRefine.flxContainerCategoryOptions.add(flxCategory1, flxGapCategory1);
      }

    }
  }
  function setUIforSelectedRefineMents(category,i){
    kony.print("  "+category+"   "+i);
    if(i<10){
      i="0"+i;
    }
    frmSortAndRefine["lblRefineSubCat"+category+i].skin="sknLbl24pxRedGothamBook";
    frmSortAndRefine["lblSelectedRefineSubCat"+category+i].setVisibility(true);
  }
  function setUIForSaleAndClearance(refValue){
    if(refValue==="onsale"){
      frmSortAndRefine.lblSaleCont.skin="sknLbl24pxRedGothamBook";
      frmSortAndRefine.lblSaleSelected.setVisibility(true);
      frmSortAndRefine.lblClearance.skin="sknLblGothamBook24pxBlack";
      frmSortAndRefine.lblClearanceSelected.setVisibility(false);
      TealiumManager.trackEvent("Shop by Sale Feature", {element_id:"Shop By: Sale",element_category:"Shop By"});
    }else if(refValue==="onclearance"){
      frmSortAndRefine.lblSaleCont.skin="sknLblGothamBook24pxBlack";
      frmSortAndRefine.lblSaleSelected.setVisibility(false);
      frmSortAndRefine.lblClearance.skin="sknLbl24pxRedGothamBook";
      frmSortAndRefine.lblClearanceSelected.setVisibility(true);
      TealiumManager.trackEvent("Shop by Clearance Feature", {element_id:"Shop By: Clearance",element_category:"Shop By"});
    }else{
      frmSortAndRefine.lblSaleCont.skin="sknLblGothamBook24pxBlack";
      frmSortAndRefine.lblSaleSelected.setVisibility(false);
      frmSortAndRefine.lblClearance.skin="sknLblGothamBook24pxBlack";
      frmSortAndRefine.lblClearanceSelected.setVisibility(false);
    }
  }
  function updateSelectedRefinements(selRef){
    try {
      var selRefObj=JSON.parse(selRef);
      var refValuePmid="";
      for(var key in selRefObj){
        if(key!=="cgid"){
          if(key ==="pmid"){
            refValuePmid=selRefObj[key];
          }
            var attribute=key;
            var refValue=selRefObj[key];
            var refValueSelArr=refValue.split("|");
            var attributeName=findAttribteLabel(attribute);
            for(var j=0;j<gblSubRefineArr.length;j++){
              if(attribute==gblSubRefineArr[j].attr){
                var refValuesArr=gblSubRefineArr[j].allValues || [];
                for(var k=0;k<refValuesArr.length;k++){
                  for(var m=0;m<refValueSelArr.length;m++){
                    if(refValueSelArr[m]===refValuesArr[k]){
                      setUIforSelectedRefineMents(attributeName,k);
                    }
                  }
                }
              }
            }
        }
      }
      setUIForSaleAndClearance(refValuePmid);
      kony.print("sel ref is "+JSON.stringify(selRefObj));
    } catch(e) {
      MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewSortProducts.js on updateSelectedRefinements for selRefObj:" + JSON.stringify(e)}]);
      if(e){
        TealiumManager.trackEvent("Selected Refinement Object Parse Exception While Sorting Products", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
  }

  function updateSelectedSortOptions(selSort){
    for(var i=0;i<gblsortArr.length;i++){
      if(selSort===gblsortArr[i].id){
        frmSortAndRefine["lblSelectedSort"+i].setVisibility(true);
        frmSortAndRefine["lblSortField1"+i].skin="sknLblGothamBold24pxRed";
      }else{
        frmSortAndRefine["lblSelectedSort"+i].setVisibility(false);
        frmSortAndRefine["lblSortField1"+i].skin="sknLblGothamBold24pxBlack";
      }
    }

  }
  function showSortField(){
    frmSortAndRefine.flxContainerSortOptions.setVisibility(true);
    frmSortAndRefine.btnExpandSortBy.text="U";
  }

  function hideSortFields(){
    frmSortAndRefine.flxContainerSortOptions.setVisibility(false);
    frmSortAndRefine.btnExpandSortBy.text="D";
  }

  function expandCollapseSortFields(eventObj){
    if(eventObj.text=="D"){
      showSortField();
    }else{
      hideSortFields();
    }
  }

  function hideRefineFields(){
    frmSortAndRefine.flxRefineContainer.setVisibility(false);
    frmSortAndRefine.btnExpandRefine.text="D";
  }
  function showRefineFields(){
    frmSortAndRefine.flxRefineContainer.setVisibility(true);
    frmSortAndRefine.btnExpandRefine.text="U";
  }

  function expandCollapseRefine(eventObj){
    if(eventObj.text=="D"){
      showRefineFields();
    }else{
      hideRefineFields();
    }
  }
  function hideAnyRefineFields(){
    if(refineOpen!==""){
      hideRefineSubContainer(refineOpen);
    }
  }

  function hideCategoryFields(){
    frmSortAndRefine.lblTickCategory.text="D";
    frmSortAndRefine.flxContainerCategoryOptions.setVisibility(false);
  }
  function showCategoryFields(){
    frmSortAndRefine.lblTickCategory.text="U";
    frmSortAndRefine.flxContainerCategoryOptions.setVisibility(true);
    hideAnyRefineFields();
  }
  function updateRefineLabelText(){
    var noOfRefines=gblRefineArr.length;
    frmSortAndRefine.lblRefine.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnRefineSearch")+" ("+noOfRefines+")";
  }
  
  function showSelectedRefineSubContainer(idName){
    try{
      frmSortAndRefine["flxRefine"+idName].setVisibility(true);
    }catch(e){
      kony.print("the flex doesnt exist");
      if(e){
        TealiumManager.trackEvent("Flex Doesnt Exist Exception in Sort Products Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
    var idRefine=-1;
    for(var i=0;i<gblRefineArr.length;i++){
      if(gblRefineArr[i].label==idName){
        idRefine=i;
      }
    }
    if(idRefine!==-1){
      frmSortAndRefine["lblRefineSelected"+idRefine].text="U";
    }
    
    
  }
  function showCategoryHeader(categoryArr){
    var dataCat=categoryArr[0].data ||{};
      var innerValues=dataCat.values ||[];
    if(innerValues.length!==0){
      frmSortAndRefine.flxCategory.setVisibility(true);
    }else{
      frmSortAndRefine.flxCategory.setVisibility(false);
    }
  }
  function showSortPage(refObj,selSort,selRef,selectedCategory,firstTime){
    isSortChanged=false;
    var sortArr=refObj.sortArr || [];
    var refCatArr=refObj.categoriesArr || [];
    var otherCount=refObj.projectCount || "";
    var prodCatArr=refObj.prodCategoryArr || [];
    var selectedSort=selSort;
    var selectedRef=selRef;
    var selectedCgId=selectedCategory;
    updateSortingFileds(sortArr);
    updateRefineFields(refCatArr);
    updateCategoryFields(prodCatArr);
    showCategoryHeader(prodCatArr);
    updateProjectProductFileds(otherCount);
    updateSelectedCategories(selectedCgId);
    updateSelectedRefinements(selRef);
    updateSelectedSortOptions(selSort);
    hideCategoryFields();
    hideSaleClearance();
    //hideRefineFields();
    updateRefineLabelText();
    if(latestRefSelected!=""){
      showSelectedRefineSubContainer(latestRefSelected);
    }
    //hideShopBy();
    showSortField();
    if(firstTime){
      show();
      hideCategoryFields();
      hideSaleClearance();
      //hideRefineFields();
      //hideShopBy();
      showSortField();
    }

    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  function clearAllGbls(){
    refineOpen="";
  	latestRefSelected="";
  }




  //Here we expose the public variables and functions

  return{
    show: show,
    bindEvents: bindEvents,
    updateScreen:updateScreen,
    showSortPage:showSortPage,
    clearAllGbls:clearAllGbls
  };

});



