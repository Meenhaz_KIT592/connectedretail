var weeklyAdId = "";
var weeklyAdFormID = "";
var gblLaunchMode=0;
var firstLaunchFromPrevKonyVersionFlag = "";
var gblIsFromImageSearch = false;
var gblFromHomeTile = false;
var formFromDeeplink=false;
var deeplinkEnabled = false;
var frombrowserdeeplink = false;
var dealId = "";
function appservicereq(params)
{

kony.print("App service request triggred");
kony.print(JSON.stringify(params));
var test=params;
var lparams=test.launchparams;
gblLaunchMode=params.launchmode;
if(params.launchmode==3)
{
  deeplinkEnabled = true;
  var currentForm=kony.application.getCurrentForm()||"";
  var currentFormID= currentForm.id||"";
  var lMyLocationFlag = false;
  if (params.launchparams.formToOpen=="frmRewardsProgramInfo"){
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.rewardTermsandConditions);
    //#ifdef iphone
    return true;
    //#endif
  }
  if (params.launchparams.formToOpen=="frmPrivacyPolicy"){
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
    //#ifdef iphone
    return true;
    //#endif
  }
  if (params.launchparams.formToOpen=="frmHelp"){
    MVCApp.getHelpDetailsController().load();
    //#ifdef iphone
    return true;
    //#endif
  }
  if (params.launchparams.formToOpen=="frmTermsConditions") {
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.createTermsandConditions);
    //#ifdef iphone
    return true;
    //#endif
  }
  if (params.launchparams.formToOpen=="frmSignIn") {
    rewardsInfoFlag = false;
    isFromShoppingList=false;
    MVCApp.getSignInController().load();
    //#ifdef iphone
    return true;
    //#endif
  }
  if (params.launchparams.formToOpen=="frmCreateAccount") {
    fromCoupons = false;
    MVCApp.getCreateAccountController().load();
    //#ifdef iphone
    return true;
    //#endif
  }
if(params.launchparams.formToOpen=="frmWeeklyAds"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  frombrowserdeeplink = true;
  if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
	lMyLocationFlag = "true";
  }else{
	lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
  }
  if(fromBrowserViews){
      MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(true);
    }
  if(lMyLocationFlag === "true"){
    loadWeeklyAds = true;
    frmWeeklyAdHome.segAds.removeAll();
    frmWeeklyAdHome.defaultAnimationEnabled = false;  
    weeklyAdEntryFromVoiceSearch=false;
    MVCApp.getWeeklyAdHomeController().load();
    //#ifdef iphone
    if(currentFormID!=="frmWeeklyAds")
    	formFromDeeplink=true;
    return frmWeeklyAdHome;
    //#endif
   }else{
     var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"weeklyAds"};
     whenStoreNotSet = JSON.stringify(whenStoreNotSet);
     kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
     MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
   }
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}else if(params.launchparams.formToOpen=="frmPJDP"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  frombrowserdeeplink = true;
  var formName = kony.application.getCurrentForm().id;
  MVCApp.Toolbox.common.clearPdpGlobalVariables();
  MVCApp.getPJDPController().setQueryString("");
  MVCApp.getPJDPController().setEntryType("deeplink");
  MVCApp.getPJDPController().load(params.launchparams.productId, false,formName);
  //#ifdef iphone
  if(currentFormID!=="frmPJDP")
  	formFromDeeplink=true;
  return frmPJDP;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
else if(params.launchparams.formToOpen=="frmCoupons"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  couponsEntryFromVoiceSearch=false;
  if(params.launchparams.fromHomeTile){
    gblFromHomeTile = true;
  }else{
    gblFromHomeTile = false;
  }
  frmObject = frmHome;
  MVCApp.getHomeController().loadCoupons();
  frombrowserdeeplink = true;
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
else if(params.launchparams.formToOpen=="frmPDP")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  frombrowserdeeplink = true;
  var headerText = [];
      headerText[0] = "";
      headerText[1] = "";
  MVCApp.getPDPController().setQueryString("");
  var productType = params.launchparams.product_type || "";
  var productId = params.launchparams.productId || "";
  var defaultProductId = params.launchparams.c_default_product_id || "";
  var sessionId = params.launchparams.sessionId ||"";
  var invokeForm = "frmHome";
  var currForm = kony.application.getCurrentForm();
  if(currForm!=null && currForm!=undefined){
    var currFormId = currForm.id || "";
    if(currFormId =="frmChatBox"){
      MVCApp.getChatBoxController().setSession(sessionId);
      invokeForm = "frmChatBox";
	        MVCApp.getPDPController().setFromBarcode(true);
    }
  }
  if(productType == "item"){
   if(productId !=="" || productId!="null" || productId!==null){
     MVCApp.getPDPController().load(productId,true,false,headerText,invokeForm);  
   }  
  }else if(productType == "master"){
    if((productId !=="" || productId!="null" || productId!==null) && (defaultProductId !=="" || defaultProductId!="null" || defaultProductId!==null)){
      MVCApp.getPDPController().load(productId,false,false,headerText,invokeForm,defaultProductId) ;  
    } 
  }else{
     if(productId !=="" || productId!="null" || productId!==null){
     MVCApp.getPDPController().load(productId,true,false,headerText,invokeForm);  
   }  
  }
  MVCApp.getPDPController().setEntryType("deeplink");
   //#ifdef iphone
  
  formFromDeeplink=true;
  return frmPDP;
   //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
 else if(params.launchparams.formToOpen=="frmProductsList")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  frombrowserdeeplink = true;
  var query = params.launchparams.q ||"";
  var cgid=params.launchparams.cgid ||"";
  var source = params.launchparams.source ||""
  var queryObj={};
  gblIsFromImageSearch = false;
  imageSearchProjectBasedFlag = false;
  frmProductsList.segProductsList.removeAll();
  if(cgid!==""){
    queryObj={"id":cgid};
    MVCApp.getProductsListController().setFromDeepLink(true);
    MVCApp.getProductsListController().load(queryObj,false);
  }else if(query!==""){
     MVCApp.getProductsListController().load(decodeURI(decodeURI(query)),true);
     }
  if(source == "QR"){
   MVCApp.getProductsListController().setEntryType("QRCodeSearch"); 
  } else {
    MVCApp.getProductsListController().setEntryType("deeplink");
  }
  //#ifdef iphone
  if(currentFormID!=="frmPJDP"){
    formFromDeeplink=true;
  }
  return frmProductsList;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
 else if(params.launchparams.formToOpen=="frmEvents"){
   //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
   if(fromBrowserViews){
      MVCApp.getEventsController().setFormEntryFromCartOrWebView(true);
    }
  MVCApp.getEventsController().load(false);
  frombrowserdeeplink = true;
  //#ifdef iphone
  if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
    return frmEvents;
  }else{
    var storeString = kony.store.getItem("myLocationDetails") ||"";
    if(storeString != ""){
      return frmEvents;
    }
  }
  //#endif
   //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
  
else if(params.launchparams.formToOpen=="frmEventDetail"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  var reqParams = {};
  if(fromBrowserViews){
      MVCApp.getEventDetailsController().setFormEntryFromCartOrWebView(true);
    }
  reqParams.eventid = params.launchparams.Id || "";
  MVCApp.getEventsController().load(reqParams);
  frombrowserdeeplink = true;

}
else if(params.launchparams.formToOpen=="frmWeeklyAdDetail"){
  frombrowserdeeplink = true;
  dealId = params.launchparams.Id || "";
  gblIsWeeklyAdDetailDeepLink = true;
  if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
    lMyLocationFlag = "true";
  }else{
    lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
  }
  if(lMyLocationFlag === "true"){
    //#ifdef android
    MVCApp.Toolbox.common.showLoadingIndicator("");
    //#endif
    MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId,"",weeklyAdFormID,dealId); 
  }else{
    var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"weeklyAdDetail"};
    whenStoreNotSet = JSON.stringify(whenStoreNotSet);
    kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
  }

}
 else if(params.launchparams.formToOpen=="frmChatBox")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
	MVCApp.getChatBoxController().load();
    frombrowserdeeplink = true;
  //#ifdef iphone
    return frmChatBox;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
  
 else if(params.launchparams.formToOpen=="frmProducts")
{
	//#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif	
  MVCApp.getProductsController().updateScreen();
  	MVCApp.getProductsListController().setFromDeepLink(false);
  frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProducts"){
  		formFromDeeplink=true;
    }
    return frmProducts;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}  
  else if(params.launchparams.formToOpen=="frmProductCategory")
{ 
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  	if(fromBrowserViews){
      MVCApp.getProductCategoryController().setFormEntryFromCartOrWebView(true);
    }
	var reqParams = {};
  	reqParams.productId =  params.launchparams.productId || "";
  	MVCApp.getProductsListController().setFromDeepLink(false);
  	MVCApp.getProductCategoryController().load(reqParams);
  	frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProductCategory"){
  		formFromDeeplink=true;
    }
    return true;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}  
   else if(params.launchparams.formToOpen=="frmProductCategoryMore")
{
  if(fromBrowserViews){
      MVCApp.getProductCategoryMoreController().setFormEntryFromCartOrWebView(true);
    }
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif	
  var reqParams1 = {};
  	MVCApp.getProductsListController().setFromDeepLink(false);
  	reqParams1.id = params.launchparams.productId || "";
  	reqParams1.name = "";
  	reqParams1.headerImage ="";
	MVCApp.getProductCategoryMoreController().load(reqParams1);
    frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProductCategoryMore"){
  		formFromDeeplink=true;
    }
    return true;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}   
  else if(params.launchparams.formToOpen=="frmProjects")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
	 MVCApp.getProjectsController().updateScreen();
  	 frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProjects"){
  		formFromDeeplink=true;
    }
    return frmProjects;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}  
   else if(params.launchparams.formToOpen=="frmProjectsCategory")
{
	 
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  var reqParams2 = {};
  	reqParams2.id = params.launchparams.projectId || "";
  	reqParams2.image = "";
  	reqParams2.name = "";
  	MVCApp.getProjectsCategoryController().load(reqParams2);
  //  frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProjectsCategory"){
      formFromDeeplink=true;
    }
    return frmProjectsCategory;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}else if(params.launchparams.formToOpen=="frmWeeklyAdsList"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  if(fromBrowserViews){
      MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(true);
    }
  var currentForm =kony.application.getCurrentForm();
  if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
	lMyLocationFlag = "true";
  }else{
    lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag(); 
  }
  if(currentForm!=null && currentForm != undefined){
    weeklyAdFormID = currentForm.id;
  }
  if(lMyLocationFlag === "true"){
    weeklyAdId = params.launchparams.Id || "";
    MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId,"",weeklyAdFormID);
    frombrowserdeeplink = true;
    //#ifdef android
    if(currentFormID!=="frmWeeklyAd"){
    	formFromDeeplink=true;
    }
    //#endif
    //#ifdef iphone
    if(currentFormID!=="frmWeeklyAd"){
    	formFromDeeplink=true;
    }
    return frmWeeklyAd;
    //#endif
  }else{
    var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"weeklyAdsList"};
    whenStoreNotSet = JSON.stringify(whenStoreNotSet);
    kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
  }
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}  else if(params.launchparams.formToOpen=="frmProjectsList")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif	
  var queryProjectList ={};
  	queryProjectList.id = params.launchparams.Id || "";
  	gblIsFromImageSearch = false;
  	imageSearchProjectBasedFlag = false;
  	MVCApp.getProjectsListController().setEntryType("deeplink"); 
	MVCApp.getProjectsListController().load(queryProjectList,false,false);
    frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProjectsList"){
  	formFromDeeplink=true;
    }
    return frmProjectsList;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}  
  
 else if(params.launchparams.formToOpen=="frmSearch")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  var searchQuery = params.launchparams.searchText || "";
	kony.store.setItem("searchedProdId",searchQuery); 
  	frmProductsList.segProductsList.removeAll();
   	MVCApp.getProductsListController().load(searchQuery,true);
    gblIsFromImageSearch = false;
  	imageSearchProjectBasedFlag = false;
  	frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentFormID!=="frmProductsList"){
  		formFromDeeplink=true;
    }
    return frmProductsList;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}  
  else if(params.launchparams.formToOpen=="frmRewards")
{
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
  MVCApp.getRewardsProgramInfoController().load(false);
  frombrowserdeeplink = true;
  //#ifdef iphone
  if(currentFormID!=="frmRewardsProgramInfo"){
  	formFromDeeplink=true;
  }
  return frmRewardsProgramInfo;
  //#endif
 //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
} else if(params.launchparams.formToOpen=="frmHome"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
	MVCApp.searchBar.homePageLoadingHandler();
  //#ifdef iphone
  return frmHome;
  //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
} else if(params.launchparams.formToOpen=="frmSettings"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif
    MVCApp.getSettingsController().load();
    MVCApp.tabbar.bindEvents(frmSettings);
    MVCApp.tabbar.bindEvents(frmMore);
	MVCApp.tabbar.bindIcons(frmSettings,"frmMore");
  	frombrowserdeeplink = true;
    //#ifdef iphone
    return frmSettings;
    //#endif
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
} else if(params.launchparams.formToOpen=="frmFindStoreMapView"){
  //#ifdef android
  MVCApp.Toolbox.common.showLoadingIndicator("");
  //#endif	
  var currentformId = "";
    try{
      currentformId = kony.application.getCurrentForm().id;
    } catch (e){
      currentformId = "frmHome";
    }
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    cust360Obj.entryPoint="Deeplink";
    MVCApp.Customer360.sendInteractionEvent("ChooseStore", cust360Obj);
    MVCApp.getLocatorMapController().load(currentformId);
    frombrowserdeeplink = true;
  //#ifdef iphone
  	if(currentformId !== "frmFindStoreMapView"){
  		formFromDeeplink=true;
    }
    return frmFindStoreMapView;
  //#endif
} else {
  //#ifdef android
  MVCApp.Toolbox.common.dismissLoadingIndicator();
  //#endif
}
  //End of storeHours deeplinking Code.
  
  
  
  var currentForm = kony.application.getCurrentForm();
  
  if(currentForm === null){
    currentForm = "";
  }
  return currentForm;
}else if(params.launchmode==2){
  deeplinkEnabled = true;
  var currentForm = kony.application.getCurrentForm();
  //#ifdef android
  if(!currentForm){
    return frmHome;
  }
  //#endif
  appLaunchCust360Sent=true;
  
  kony.print("do not go to home Form 1231"+deeplinkEnabled);
}else{
  deeplinkEnabled = false;
  //#ifdef android
   try{
    frmHome.onDeviceBack = onDeviceBackOfHomeForm;
   }catch(e){
     if(e){
        TealiumManager.trackEvent("Exception On the click of back in VoiceSearch from frmHome", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
   }
    //#endif
  if(MVCApp.Toolbox.Service.getFlagForInstallation()=="true"){
    initLaunchFlag = true;
    kony.store.setItem("firstLaunchIntallation","true");
    kony.store.setItem("firstIntallation","false");
    kony.store.setItem("showWhatsNew","true");
    kony.store.setItem("firstLaunchFromPrevKonyVersionFlag", "false");
    MVCApp.getGuideController().init();
    //#ifdef iphone
    var cust360Obj=MVCApp.Customer360.getGlobalAttributes();
    cust360Obj.launchType="appIconClick";
    cust360Obj.isFirstLaunch="true";
    MVCApp.Customer360.sendInteractionEvent("Launch", cust360Obj);
    //#endif
    return frmGuide;
  }else{
    kony.store.setItem("firstLaunchIntallation","false");
    kony.store.setItem("showWhatsNew","false");
    //#ifdef iphone
    var returnedValue =  ApplicationState.getApplicationState();
    gblApplicationState=returnedValue;
    if(returnedValue=="active"){
      var cust360Obj=MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.launchType="appIconClick";
      cust360Obj.isFirstLaunch="false";
      MVCApp.Customer360.sendInteractionEvent("Launch",cust360Obj);
      registerPushForVersion85Plus();
    }
    //#endif
    firstLaunchFromPrevKonyVersionFlag=kony.store.getItem("firstLaunchFromPrevKonyVersionFlag");
    if(!firstLaunchFromPrevKonyVersionFlag || (firstLaunchFromPrevKonyVersionFlag == "false" && (!isPushRegisterInvokedInPrevVersion && initLaunchFlagForVersion72))){
      kony.store.setItem("firstLaunchFromPrevKonyVersionFlag", "false");
      MVCApp.getGuideController().init();
      return frmGuide;
    }else if(firstLaunchFromPrevKonyVersionFlag == "false"){
      if(kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)){
        MVCApp.Toolbox.common.showLoadingIndicator("");
        
        frmHome.flxSearchOverlay.flxVisual.cmrImageSearch.cameraOptions = {
          hideControlBar: true
          //#ifdef android
          ,
          focusMode:constants.CAMERA_FOCUS_MODE_CONTINUOUS
          //#endif
        };
        frmHome.flxHeaderWrap.cmrImageSearch.cameraOptions = {
          hideControlBar: true
          //#ifdef android
          ,
          focusMode:constants.CAMERA_FOCUS_MODE_CONTINUOUS
          //#endif
        };
        
        //#ifdef android
        frmHome.flxSearchOverlay.flxVisual.cmrImageSearch.setEnabled(false);
        frmHome.flxHeaderWrap.cmrImageSearch.setEnabled(false);
        //#endif
      }else{
        //#ifdef android
        if(appInForeground){
          homePreShowHandler();
          MVCApp.getHomeController().load();
        }
        //#endif
        //#ifdef iphone
        homePreShowHandler();
        MVCApp.getHomeController().load();
        //#endif
      }
      //return frmHome;
//       //#ifdef iphone
//       return frmHome;
//       //#endif
    }
  }
}
}