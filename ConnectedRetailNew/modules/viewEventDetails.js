/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.EventDetailsView = (function(){
 
  /**
	 * PUBLIC
	 * Open the Events form
	 */
  
  function show(){
    //#ifdef android
    	MVCApp.Toolbox.common.setTransition(frmEventDetail,3);
    //#endif
    //#ifdef iphone
        frmProducts.defaultAnimationEnabled = true;
        MVCApp.Toolbox.common.setTransition(frmEventDetail, 2);
    //#endif
  	frmEventDetail.show();
  } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   
  function bindEvents(){
	frmEventDetail.btnCall.onClick = callStore;
    frmEventDetail.btnHeaderLeft.onClick = goBackToEvents;
    frmEventDetail.btnDirections.onClick = getDirections;
    frmEventDetail.postShow = function(){
      //#ifdef iphone
	  MVCApp.Toolbox.common.requestReview();
      //#endif
      MVCApp.Toolbox.common.setBrightness("frmEventDetails");
      MVCApp.Toolbox.common.destroyStoreLoc();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Event Detail: Michaels Mobile App";
      lTealiumTagObj.page_name = "Event Detail: Michaels Mobile App";
      lTealiumTagObj.page_type = "Event";
      lTealiumTagObj.page_category_name = "Event";
      lTealiumTagObj.page_category = "Event";
      TealiumManager.trackView("Event Detail screen", lTealiumTagObj);
    };
    frmEventDetail.flxMainContainer.onScrollStart = onreachWidgetEnd;
    frmEventDetail.flxMainContainer.onScrollEnd = onreachScrollEnd;
    MVCApp.tabbar.bindEvents(frmEventDetail);
    MVCApp.tabbar.bindIcons(frmEventDetail,"frmMore");
  }
  
  function updateScreen(details){
 
    details = details || "";
    if(details !== ""){
      	kony.print("the event details are "+JSON.stringify(details));
      	frmEventDetail.lblTitle.text = details.eventTilte || "";
      	frmEventDetail.lblDate.text = details.lblEventDate || "";
        frmEventDetail.lblTime.text = details.lblEventTime || "";
        frmEventDetail.lblCost.text = details.cost || "";
        frmEventDetail.lblDescription.text = details.description || "";
        frmEventDetail.lblAddress1.text = details.address1 || "";      	
      	frmEventDetail.lblAddress2.text = details.city+", "+details.state+" "+details.postalcode;
        frmEventDetail.btnCall.text = details.phone || "";
      	frmEventDetail.imgEvent.height = getImageHeight()+"%";
        frmEventDetail.imgEvent.src = details.imgEvent.src || "";
      	var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      	cust360Obj.eventid=details.eventIDTrack||"";
      	MVCApp.Customer360.sendInteractionEvent("EventDetails", cust360Obj);
      	show();
      
    }else{
      kony.print("event details are blank");
    }
  }
  
 
  function goBackToEvents(){
    var previousForm = kony.application.getPreviousForm();
    
    if(!ifEventDetailDeeplink){
         MVCApp.getHomeController().load();	 
    }
    
    else if(previousForm.id === "frmEvents"){
      //#ifdef android
		MVCApp.Toolbox.common.setTransition(previousForm,2);
   //#endif	
      previousForm.show();
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.storeid= MVCApp.Toolbox.common.getStoreID();
      var eventsList= MVCApp.getEventsController().getResultEvents();
      for(var m=0;m<eventsList.length;m++){
        cust360Obj["eventid"+m]= eventsList[m];
      }
      MVCApp.Customer360.sendInteractionEvent("EventsList", cust360Obj);
      MVCApp.tabbar.bindEvents(previousForm);
      MVCApp.tabbar.bindIcons(previousForm,"frmMore");
    }
  }
 
  function getDirections(){
    MVCApp.getEventDetailsController().getDirections();
  }
   
  function callStore(){
      MVCApp.getEventDetailsController().callStore();
  }

  function getImageHeight(){
    var height = kony.os.deviceInfo().screenHeight || 0;
    return ((265/height)*100);
  }
	
  //Here we expose the public variables and functions
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen	
    };
  
});