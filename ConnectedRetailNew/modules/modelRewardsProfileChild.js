var MVCApp = MVCApp || {};
MVCApp.RewardsProfileChildModel = (function(){
  
  
   var serviceType =  MVCApp.serviceType.SigninRewardsProfile;
  var operationId = MVCApp.serviceType.fetchCustomerInfo;
  
  function load(callback,requestParams){
     MakeServiceCall( serviceType,operationId,requestParams,function(results){
       
      kony.print("Response is "+JSON.stringify(results));       
      var rewardsDataObj = new MVCApp.data.RewardsProfileChild(results);            
          callback(rewardsDataObj);       
      
    },function(error){
    kony.print("Response is "+JSON.stringify(error));
    });
    
    
  }
  return {
    
    load:load
  };
  });