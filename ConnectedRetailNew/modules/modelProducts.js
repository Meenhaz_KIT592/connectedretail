/**
 * PUBLIC
 * This is the model for formProducts form
 */
var MVCApp = MVCApp || {};
MVCApp.ProductsModel = (function(){
  var serviceType = MVCApp.serviceType.getAisleNumbers;
  var operationId = MVCApp.serviceEndPoints.getAisleNumbersForCategory;   // MVCApp.serviceEndPoints.ProductCategory;
  
  function getProducts(callback,requestParams){
    
    kony.print("modelProducts.js");
   
                    MakeServiceCall( serviceType,operationId,requestParams,
                            function(results)
                            {

              if(results.opstatus===0 || results.opstatus==="0" ){
                  var productsDataObj = new MVCApp.data.Products(results);   
                  callback(productsDataObj);
              }   
            },function(error){
              kony.print("Response is "+JSON.stringify(error));
            }
                           );
    

  }
  return{
    getProducts:getProducts
  };
});
