/**
 * PUBLIC
 * This is the view for the Projects form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.ProjectsView = (function(){
 var noWidgets=-1; 
  /**
	 * PUBLIC
	 * Open the Projects form
	 */
  
  function show(){
  frmProjects.flxMainContainer.removeAll();
  frmProjects.segMarketingAds.pageSkin = "seg2Normal";
  noWidgets=-1;
  frmProjects.flxHeaderWrap.textSearch.text="";
  frmProjects.flxSearchResultsContainer.setVisibility(false);
  //#ifdef android
  	frmProjects.flxHeaderWrap.btnClearSearch.setVisibility(false);
    MVCApp.Toolbox.common.setTransition(frmProjects,0);
  //#endif
  frmProjects.defaultAnimationEnabled = false;
  frmProjects.show();
 } 
  	
  /**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function displayListOfProjects(){
    //#ifdef android
  	 MVCApp.Toolbox.common.setTransition(frmProjects,2);
     //#endif
     //#ifdef iphone
     frmProjects.defaultAnimationEnabled = true;
  	 MVCApp.Toolbox.common.setTransition(frmProjects,2);
     //#endif
     frmProjects.show();
  }
  function displayProjectsScreen(){
     frmProjects.flxFooterWrap.bottom = "0dp";
      frmProjects.show();
  }
  function bindEvents(){
  MVCApp.searchBar.bindEvents(frmProjects, "frmProjects");
  MVCApp.tabbar.bindEvents(frmProjects);
  MVCApp.tabbar.bindIcons(frmProjects,"frmProjects");
  frmProjects.flxMainContainerouter.onScrollStart = onreachWidgetEnd;
  frmProjects.flxMainContainerouter.onScrollEnd = onreachScrollEnd;
    frmProjects.flxTransLayout.onClick = function(){
      kony.print("do Nothing");
    };
           frmProjects.segMarketingAds.onRowClick = function(){
    var selectedIndex = frmProjects.segMarketingAds.selectedRowIndex[1];
    var segData = frmProjects.segMarketingAds.data;
    var currentIndicator = 0;
      for(var i =0;i<indicatorsLength;i++){
        if(currentIndicator == i){
          frmProjects["img"+i].src = 'pager_on1.png';
        }else{
          frmProjects["img"+i].src = 'pager_off1.png';
        }
      }
    var selectedData = segData[selectedIndex];
    var contentURL = selectedData.c_contentUrl || "";
    if(contentURL !== ""){
      
     //#ifdef iphone
     		contentURL = decodeURI(contentURL);
     		contentURL = contentURL.replace(/%2C/g,",");
    		contentURL = encodeURI(contentURL);
     //#endif 
    MVCApp.Toolbox.common.openApplicationURL(contentURL); 
    }
    };
    frmProjects.segMarketingAds.onSwipe = function(eventObj, sectionIndex, rowIndex){
      var segData = frmProjects.segMarketingAds.data;
      if(null != segData && undefined != segData && segData.length>0 && 
        null != segData[rowIndex] && undefined != segData[rowIndex]){
      var currentIndicator = segData[rowIndex].indicator;
      for(var i =0;i<indicatorsLength;i++){
        if(currentIndicator == i){
          frmProjects["img"+i].src = 'pager_on1.png';
        }else{
          frmProjects["img"+i].src = 'pager_off1.png';
        }
      }}  
    };
    frmProjects.preShow = function(){
      try{
        var numItems= MVCApp.Toolbox.common.getNumberOfItemsInCart();
        if(numItems==0)
        {
          frmProjects.flxHeaderWrap.lblItemsCount.isVisible = false;
        }else{
          frmProjects.flxHeaderWrap.lblItemsCount.isVisible = true;
        }
        frmProjects.flxSearchOverlay.setVisibility(false);
        frmProjects.flxVoiceSearch.setVisibility(false);
      }catch(e){
        kony.print("Exception in frmWeeklyAd preShow "+e);
      }
    };
    frmProjects.postShow=function(){
          var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
    frmProjects.flxHeaderWrap.lblItemsCount.text=cartValue;
      MVCApp.Toolbox.common.setBrightness("frmProjects");
      MVCApp.Toolbox.common.destroyStoreLoc();
      
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Projects Navigation: Michaels Mobile App";
      lTealiumTagObj.page_name = "Projects Navigation: Michaels Mobile App";
      lTealiumTagObj.page_type = "Navigation";
      lTealiumTagObj.page_category_name = "Projects";
      lTealiumTagObj.page_category = "Projects";
      //artf5479
      lTealiumTagObj.category_ID = "Projects";
      lTealiumTagObj.page_category_name="";
      TealiumManager.trackView("Projects Tab", lTealiumTagObj);
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      if(!formFromDeeplink)
      	MVCApp.Customer360.sendInteractionEvent("ProjectsCategories1", cust360Obj);
      else
        formFromDeeplink=false;
    };
  }
  
  function updateScreen(results){
    MVCApp.getProjectsController().setGlblProjectsResult(results);
      setCarouselData();
   var projectsList = results.getProjectsListData();
    kony.store.setItem("GetProjectCategories",projectsList); // this is kept for to getsubcategories 
    if(projectsList.length > 0){
     for(var i=0;i<projectsList.length;i++){
     var bgImage = projectsList[i].c_mobileThumbnail || "";
      var flxDepartment1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxDepartment1"+i,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlexBgLtGray",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDepartment1.setDefaultUnit(kony.flex.DP);
//     var Image04716670c6f474e = new kony.ui.Image2({
//         "id": "Image04716670c6f474e"+i,
//         "imageWhenFailed": "notavailable_1080x142",
//         "imageWhileDownloading": "white_1080x142",
//         "isVisible": true,
//       	"height": "42dp",
//         "left": "0dp",
//         "skin": "slImage",
//         "src": bgImage,
//         "top": "0dp",
//         "width": "100%",
//         "zIndex": 1
//     }, {
//         "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
//         "padding": [0, 0, 0, 0],
//         "paddingInPixel": false
//     }, {});
       var lblBasic04716670c6f474e = {
         "id": "lblBasic04716670c6f474e" + i,
         "skin": "sknLblIcon34px999999",
         "text": "K",
         "isVisible": true,
         "right": "10dp",
         "zIndex": 4,
         "top": "0dp",
          "width": "20%",
         "height": "100%"

       };
       var lblLayout = {

         "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
         "padding": [0, 0, 3, 0],
         "paddingInPixel": false

       };
       var lblLayout1 = {
         "renderAsAnchor": true

       };
       var lbl = new kony.ui.Label(lblBasic04716670c6f474e, lblLayout, lblLayout1);
    var btnCategoryName = new kony.ui.Button({
        "height": "100%",
        "focusSkin":"sknLblGothamRegular32pxDarkGray",
        "id": "btnCategoryName"+i,
        "isVisible": true,
        "left": "10dp",
        "onClick": onSelectProjectCategory,
        "skin": "sknLblGothamRegular32pxDarkGray",
        "text": projectsList[i].name,
        "top": "0dp",
        "width": "98%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    },{});
    flxDepartment1.add(btnCategoryName,lbl);   
    frmProjects.flxMainContainer.add(flxDepartment1);     
    }
     var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    frmProjects.flxMainContainer.add(flxBottomSpacerDND);  
     
    MVCApp.Toolbox.common.dismissLoadingIndicator();    
   
    for(var j=0;j<projectsList.length;j++){
     var data = {};
     data.id = projectsList[j].id || "";
     data.name = projectsList[j].name || "";
     data.image = projectsList[j].c_mobileThumbnail || "";
     frmProjects["btnCategoryName"+j].info = {"data":data};
    }
      //frmProjects.flxTransLayout.setVisibility(true);
      MVCApp.Toolbox.common.dispFooter();
     //#ifdef iphone
      kony.timer.schedule("ProjectsAnimation",AnimCallback, 0.1, false);
      //#elseif
      AnimCallback();
      //#endif
    }else{
      kony.print("No Projects Found");
      MVCApp.Toolbox.common.dismissLoadingIndicator();    
    }
  }

  function onSelectProjectCategory(){
    kony.print("Selected id"+this.info.data);
    var info = this.info || {};
    var data = info.data || {};
    MVCApp.getProjectsCategoryController().load(data);
  }
  
   
  function AnimCallback() {
  noWidgets=noWidgets+1;
  if(noWidgets<=(frmProjects.flxMainContainer.widgets().length-2)){
    var ref = "flxDepartment1"+noWidgets;
   // alert("ref:"+ref);
    animate(frmProjects[ref]);
  }
  else
    {
      noWidgets = -1;
    }
}
  function animate(widgetRef){
   var trans100 = kony.ui.makeAffineTransform();
  trans100.rotate3D(0, 1, 0, 0);
  trans100.setPerspective(900.0);
  var trans000 = kony.ui.makeAffineTransform();
    trans000.rotate3D(90, 1, 0, 0);
            trans000.setPerspective(900.0);
  widgetRef.isVisible=true;
    var animDuration = 0.07;
    //#ifdef android
    	animDuration = 0.03;
    //#endif
    widgetRef.animate(
    kony.ui.createAnimation({
       "0": {
            "anchorPoint": {
                "x": 0.5,
                "y": 0.1
            },
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans000,
            "opacity":0
        },
        "100": {
            "anchorPoint": {
                "x": 0.5,
                "y": 0.5
            },
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100,
            "opacity":1
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": animDuration
    }, {
        "animationEnd": AnimCallback
    });
}
  
  function setCarouselData(){
    var featureData = kony.store.getItem("featureProject");
    featureData = featureData || [];
    if(featureData.length>0)
      {
              var tempData = [];
          for(var i=0;i<featureData.length;i++){
            var contentSequence = featureData[i].c_contentSequence;
            if(contentSequence !== undefined){
              tempData.push(featureData[i]);   
            }
          }
        
         frmProjects.flxMarketingAds.setVisibility(true);
        
           featureData.sort(function(a, b){return a.c_contentSequence-b.c_contentSequence;}); // need be uncomment when we get contentSequence
          loadProjectsCarouselData(featureData);
      }
    else
      {
        
         frmProjects.flxMarketingAds.setVisibility(false);
      }
    
  } 

    
  function loadProjectsCarouselData(carouselData){
    indicatorsLength = carouselData.length;
    frmProjects.flxCarouselInd.width = carouselData.length * 12;
    frmProjects.flxCarouselInd.removeAll();
    for(var i=0;i<carouselData.length;i++){
      var img0 = new kony.ui.Image2({
        "height": "100%",
        "id": "img"+i,
        "isVisible": true,
        "left": "2dp",
        "skin": "slImage",
        "src": "pager_off1.png",
        "top": 0,
        "width": "10dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
      
     frmProjects.flxCarouselInd.add(img0);
    }
    for(var j=0;j<carouselData.length;j++){
      carouselData[j].indicator = j;
      if(0 === j){
          frmProjects["img"+j].src = 'pager_on1.png';
        }else{
          frmProjects["img"+j].src = 'pager_off1.png';
        }
    }
    frmProjects.segMarketingAds.widgetDataMap = {
      imgBanner:"c_contentImage",
      featureName:"name"
    };
    var carData = "";
    if(carouselData.length ==2){
      carData = carouselDataLengthTwo(carouselData);
    }else{
      carData = carouselData;
    }
    frmProjects.segMarketingAds.data = carData;
    var screenWidth = getWidthFromStore();
    var imgHeight = parseInt((screenWidth*0.94)/1.8);
    var imgHeight1 = imgHeight.toString()+"dp";
    var labelTop = (imgHeight-40).toString()+"dp";
    CopyflxCarousel0e3d3ae00cb5f4d.height = imgHeight1;
    CopyflxCarousel0e3d3ae00cb5f4d.imgBanner.height = imgHeight1;
    //#ifdef android
    CopyflxCarousel0e3d3ae00cb5f4d.forceLayout();
    //#endif
    frmProjects.segMarketingAds.height = imgHeight1;
    //#ifdef android
    frmProjects.flxMarketingAds.forceLayout();
    //#endif
  }
  
  function carouselDataLengthTwo(carouselData){
    var tempData = carouselData;
    tempData.push(carouselData[0]);
    tempData.push(carouselData[1]);
    return tempData;
  }
  
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	displayListOfProjects:displayListOfProjects,
    	displayProjectsScreen:displayProjectsScreen
    };
  
});



