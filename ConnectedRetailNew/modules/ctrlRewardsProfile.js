/**
 * PUBLIC
 * This is the controller for the Help form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.RewardsProfileController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	var custProfileData= null;
        /**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.RewardsProfileModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.RewardsProfileView();
  	   
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function load(createAccountResObj){
		init();
      MVCApp.Toolbox.common.showLoadingIndicator("");
     var customerId= createAccountResObj.customer_id; 
      kony.print("cre : "+JSON.stringify(createAccountResObj));
      var requestParams = {};
      
if(kony.store.getItem("touchIdSaved") && fromTouchId) {
  if(kony.store.getItem("userObj") !== null || kony.store.getItem("userObj") !== "" ){
    requestParams.user = createAccountResObj.email;
    requestParams.password = gblDecryptedPassword;
  } else {
      requestParams.user = kony.store.getItem("storeFirstUserForTouchId");
     requestParams.password = kony.store.getItem("storeFirstPwdForTouchId");
  }
}
      	
 else
 {
   kony.print("email : "+createAccountResObj.email);
   kony.print(" gbluser : "+gblDecryptedUser+" gblpass : "+gblDecryptedPassword);

     requestParams.user = createAccountResObj.email; //gblDecryptedUser;//userObject.login;
        requestParams.password = gblDecryptedPassword;
      	
 }
      requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParams.customer_id = customerId;
       
      	requestParams.country = MVCApp.Service.Constants.Common.country;
		model.load(view.updateScreen,requestParams);
     
    }
  
  function updateData(nxtFlag){    
    view.updateData(nxtFlag);
  }
  
  function setRewardProfileData(profileData)
  {
    custProfileData = profileData;
  }
   function getRewardProfileData()
  {
    return custProfileData;
  }
  
  
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init : init,
    	load : load,
      setRewardProfileData:setRewardProfileData,
      getRewardProfileData:getRewardProfileData,
      updateData:updateData
    };
});