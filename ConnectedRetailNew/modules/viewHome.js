/**
 * PUBLIC
 * This is the view for the Home form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var prevContentOffset=0;
var searchWidth;
var counter=0;
var gblTheme="defaultTheme";
//common vars
var scrollPosY=null;

//accordian vars
var overlayCardHeight=null;

//#ifdef android
var cardPerspective=1500;
//#else
var cardPerspective=200;
//#endif
var cardEndAngle=90;
//var cardContentHeight=190;
var cardContentHeight=230;
var firstCardTop=null;

//footer vars
var frmHomeScrollStartY=null;
var footerPosStart=null;
var footerState="visible";
var footerLock=false;

var storeHt;
var carouselFlexArray = [];
var nextCarouselIndicator = 1;
var currentCarouselIndicator = 0;
var checkStoreModeAtAppLaunch = true;
var defaultExpandedVersionOfMyStore=true;
var homeFormHideFlag = true;


//data vars
var eventImageData=[];//["event1.png","event2.png","event1.png","event2.png","event1.png","event2.png","event1.png","event2.png","event1.png","event2.png"];
var eventLabelData=[];//["Craft Show this Saturday","Candle Making Seminar","Cake Decorating Thursdays","Picture Framing Special"];
var MVCApp = MVCApp || {};

MVCApp.HomeView = (function(){
var toggleChevronFlag = true; 
var indicatorsLength = "";
//#ifdef android
var carouselTouchFlag = false;
//#endif
  /**
	 * PUBLIC
	 * Open the Home form
	 */
  	 //frmHome.flxHeaderWrap.imgCouponIcon.isVisible = false;
    /// frmHome.flxHeaderWrap.btnCoupon.isVisible = false;
    MVCApp.Toolbox.common.updateWeeklyAdd();
  function show(){
    frmHome.flxMainContainer.showFadingEdges = false;
    frmHome.flxHeaderWrap.textSearch.text="";
    frmHome.flxSearchResultsContainer.setVisibility(false);
    if(frmHome.flxSearchResultsContainer.isVisible === false){
      frmHome.flxHeaderWrap.flxSearchContainer.skin="slFbox";  
    }
    //#ifdef android
    frmHome.flxHeaderWrap.btnClearSearch.setVisibility(false);
    //#endif
    var theme=kony.theme.getCurrentTheme();
    gblTheme=theme;
    var locale= MVCApp.Toolbox.Service.getLocale();
    if(theme=="iPhone67PlusTheme"){
      frmHome.btnChangeStore.width="115dp";
      frmHome.imgFindStores.right="120dp";
    }else{
      frmHome.btnChangeStore.width="105dp";
      frmHome.imgFindStores.right="110dp";
    }
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      MVCApp.getHomeController().setStoreModeDetails();
    }else{
      frmHome.flxStoreInfo.isVisible = true;
      var chevronClosed=kony.ui.makeAffineTransform();
      //frmHome.flxMyStoreContainer.height = "60dp";
 	  if(MVCApp.Toolbox.Service.getMyLocationFlag()==="true" && defaultExpandedVersionOfMyStore){
        if(theme=="iPhone67PlusTheme" && locale.indexOf("fr")>-1){
          frmHome.flxMyStoreContainer.height="238dp";
          frmHome.flxStoreInfo.height="178dp";
        }else{
          frmHome.flxMyStoreContainer.height="223dp";
          frmHome.flxStoreInfo.height="163dp";
        }
        chevronClosed.rotate(180);
      }else{
        frmHome.flxMyStoreContainer.height="60dp";
        chevronClosed.rotate(0);
      }
      frmHome.lblChevron.transform = chevronClosed;
    }
    
    findCardTop();
    var deviceInfo = kony.os.deviceInfo();
    var name = deviceInfo.name;
    var model = deviceInfo.model;
    kony.print("###name"+name);
    kony.print("###model"+model);
    var osversion= deviceInfo.osversion;

    //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmHome,2);
    //#endif
    
    var screenWidth = getWidthFromStore();
    var imgHeight = parseInt((screenWidth*0.94)/1.8);
    var imgHeight1 = imgHeight.toString()+"dp";
    frmHome.fxlContainer.bottom = "55dp";
    //#ifdef android
    frmHome.fxlContainer.forceLayout();
    //#endif
    frmHome.flxMainContainer.FlexContainer0d102dc2c865346.top = "-250dp";
    frmHome.flxMainContainer.scrollToWidget(frmHome.FlexContainer0d102dc2c865346);
    frmHome.flxSearchResultsContainer.flxTopSpacerDNDSearch.height = "97dp";
    //#ifdef android
    frmHome.flxHeaderWrap.flxSearchContainer.top="50dp";
    frmHome.flxHeaderWrap.flxSearchContainer.left = "0dp";
    frmHome.flxHeaderWrap.flxSearchContainer.right = "0dp";
    frmHome.flxHeaderWrap.flxSearchContents.left = "10dp";
    frmHome.flxHeaderWrap.flxSearchContents.right = "10dp";
    frmHome.flxHeaderWrap.flxSearchContents.skin = "sknFlexBgLtGray";
    frmHome.flxHeaderWrap.forceLayout();
    //#endif
    frmHome.show();
    var footerBot=frmHome.flxFooterWrap.bottom;
    if(footerBot != "0dp"){
      frmHome.flxFooterWrap.animate(
        kony.ui.createAnimation({
          0:{bottom:footerBot,"stepConfig":{}},
          100:{bottom:"0dp","stepConfig":{}}}),
        {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.25},
        {animationEnd: function() {
          footerState="visible";
          frmHomeFooterDoublecheck();
        }});
    }
    
    //cart icon count
    // MVCApp.Toolbox.common.setCartItemValue("15");

    
    //#ifdef android
    frmHome.forceLayout();
    //#endif
 }
  
  function homeViewOneCallback(){
    kony.print("***homeViewOneCallback");
  }
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  function showStoreInfo(eventobj){
    frmHome.flxStoreInfo.isVisible = true;
    frmHome.flxMyStoreContainer.doLayout = function(){
        storeHt = frmHome.flxMyStoreContainer.frame["height"];
        //alert(storeHt);
      };
    if (frmHome.flxMyStoreContainer.height=="60dp"){
      var chevronOpen=kony.ui.makeAffineTransform();
      chevronOpen.rotate(180);
	  var heightOfTotalContainer="223dp";
      var locale= MVCApp.Toolbox.Service.getLocale();
      if(gblTheme=="iPhone67PlusTheme" && locale.indexOf("fr")>-1){
          heightOfTotalContainer="238dp";
        }
      frmHome.flxMyStoreContainer.animate(
      kony.ui.createAnimation({
        0:{height:"60dp","stepConfig":{}},
        100:{height:heightOfTotalContainer,"stepConfig":{}}}),
      {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.3},
      {animationEnd: function() {defaultExpandedVersionOfMyStore=true;
                                 findCardTop();}});

      frmHome.lblChevron.animate(
      kony.ui.createAnimation({
        100:{transform:chevronOpen,"stepConfig":{}}}),
      {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.3},
      {animationEnd: function() {findCardTop();}});
      
      
    }else{
      var chevronClosed=kony.ui.makeAffineTransform();
      chevronClosed.rotate(0);
		
      frmHome.flxMyStoreContainer.animate(
      kony.ui.createAnimation({
        0:{height:"223dp","stepConfig":{}},
        100:{height:"60dp","stepConfig":{}}}),
      {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.3},
      {animationEnd: function() {
        defaultExpandedVersionOfMyStore=false;
        findCardTop();}});

      frmHome.lblChevron.animate(
      kony.ui.createAnimation({
        100:{transform:chevronClosed,"stepConfig":{}}}),
      {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.3},
      {animationEnd: function() {findCardTop();}});
    }
    //#ifdef android
		frmHome.flxMyStoreContainer.forceLayout();
   	//#endif 
  }
  
  function toggleChevron(state, widgetFlex, widgetLbl){
    var flexHeight = "";
    if(widgetFlex == frmHome.flxStoreName){
      flexHeight = "60dp";
    }else{
      flexHeight = "630dp";
    }
    if(state === "D"){
      	widgetFlex.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":flexHeight}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
      	var trans100 = kony.ui.makeAffineTransform();
    	trans100.rotate(180);
      	widgetLbl.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
    } 	else {
        widgetFlex.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":flexHeight}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
      	var trans101 = kony.ui.makeAffineTransform();
    	trans101.rotate(0);
      	widgetLbl.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans101}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
    }
  }
  
  function dialPhone(eventobj){
    var text=eventobj.text;
    try{
      kony.phone.dial(text);
    }catch(e){
      kony.print("unhandled error from iOS");
      if(e){
        TealiumManager.trackEvent("Dial Phone Exception In Home Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
  }
  
  function onPreShowOfHomeForm(){
   
    frmHome.flxVoiceSearch.setVisibility(false);
    if(checkStoreModeAtAppLaunch){
      checkStoreModeAtAppLaunch = false;
      if(gblInStoreModeDetails.isInStoreModeEnabled){
        setStoreModeReEntry();
      }else{
        checkWiFiAndGeoFenceForStoreMode();
        startForegroundStoreModeTimer();
      }
    }
    var country= MVCApp.Toolbox.common.getRegion();
    frmHome.lblCountry.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.common.address.country");
    if(country==="US"){
      frmHome.imgCountryFlag.src="flag_united_states.png";
    }else if(country==="CA"){
      frmHome.imgCountryFlag.src="flag_canada.png";
    }
    var autoSwipeTimeInSeconds = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.carousel.autoSwipeTimeInSeconds"));
    try{
      //#ifdef android
      currentCarouselIndicator = 0;
      if(carouselFlexArray.length > 1){
        frmHome.flexCarouselContainer.scrollToWidget(frmHome.flexCarouselContainer["flexCarousel" + currentCarouselIndicator]);
        for(var i=0; i < carouselFlexArray.length; i++){
          if(i === 0){
            frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i].src = 'pager_on1.png';
          }else{
            frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i].src = 'pager_off1.png';
          }
        }
      }
      //#elseif
      currentCarouselIndicator = -1;
      carouselSwipe();
      //#endif
      kony.timer.schedule("HomeCarouselTimer", carouselSwipe, autoSwipeTimeInSeconds, true);
    }catch(e){
      kony.print("carousel auto indicator "+JSON.stringify(e));
      if(e){
        TealiumManager.trackEvent("Timer Exception For WIFI Switch", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
    frmHome.flexCardAnimate.isVisible=false;
    frmHome.flxHeaderWrap.flxSearchContainer.doLayout = function(){
      searchWidth = 100;
    };
    if(MVCApp.Toolbox.Service.getMyLocationFlag()==="true"){
      frmHome.lblFindStores.doLayout = function(){
        if(sfmcCustomObject.Region == "fr"){
          frmHome.flxFindStores.btnChangeStore.width =  (frmHome.flxFindStores.lblFindStores.frame.width/2) + 20;
          frmHome.flxFindStores.imgFindStores.right = (frmHome.flxFindStores.lblFindStores.frame.width/2) + 25;
        }else{
          frmHome.flxFindStores.btnChangeStore.top = "";
          frmHome.flxFindStores.btnChangeStore.bottom = "0dp";
          frmHome.flxFindStores.btnChangeStore.width =  frmHome.flxFindStores.lblFindStores.frame.width;
          frmHome.flxFindStores.imgFindStores.right = frmHome.flxFindStores.lblFindStores.frame.width + 5;
        }
      };
      //#ifdef iphone
      frmHome.lblCountry.doLayout = function(){
        frmHome.imgCountryFlag.top = "-5dp";
      };
      //#endif
    }
  }
  
  
  function onClickBtnChangeStore(){
    frmFindStoreMapView.segOptionalStoreResults.removeAll();
    frmFindStoreMapView.segOptionalStoreResults.data = [];
    frmFindStoreMapView.flxOptionalResultsContainer.isVisible = false;
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    MVCApp.Customer360.sendInteractionEvent("ChooseStore", cust360Obj);
    MVCApp.getLocatorMapController().loadfromChange("frmHome");
    storeMapFromVoiceSearch=false;
  }
  
  function onClickBtnChooseStore(){
    var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":false,"fromWhichScreen":""};
    whenStoreNotSet = JSON.stringify(whenStoreNotSet);
    kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
    frmFindStoreMapView.segOptionalStoreResults.removeAll();
    frmFindStoreMapView.segOptionalStoreResults.data = [];
    frmFindStoreMapView.flxOptionalResultsContainer.isVisible = false;
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    MVCApp.Customer360.sendInteractionEvent("ChooseStore", cust360Obj);
    MVCApp.getLocatorMapController().load("frmHome");
    storeMapFromVoiceSearch=false;
  }
  
  function onClickBtnStoreInfo(){
    var locDetails=kony.store.getItem("myLocationDetails") || "";
 	var locObj={};
    var storeId="";
    if(locDetails!=""){
      locObj=JSON.parse(locDetails);
      storeId=locObj.clientkey||"";
    }
    TealiumManager.trackEvent("Store Info:"+storeId, {conversion_category:"Store Info",conversion_id:"Home Page",conversion_action:2});
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    cust360Obj.storeid=storeId;
    MVCApp.Customer360.sendInteractionEvent("StoreInfo", cust360Obj);
    MVCApp.getLocatorMapController().updateAndLoadStoreInfo(MVCApp.getLocatorMapController().loadStoreInfo);
  }
  
  function onClickBtnStoreMap(){
    var params = {};
    params.aisleNo = "";
    params.productName = "";
    params.calloutFlag = false;
    MVCApp.sendMetricReport(frmHome, [{"storemap":"click"}]);
    MVCApp.getStoreMapController().setEntryPoint("Home");
    MVCApp.getStoreMapController().load(params); 
  }
  
  function onPostShowOfHomeForm(){
     MVCApp.Toolbox.common.updateWeeklyAdd();
    frmHome.flxSearchOverlay.setVisibility(false);
    frmHome.flexCardAnimate1a.anchorPoint={"x":0.5, "y":0};
    frmHome.flexCardAnimate1b.anchorPoint={"x":0.5, "y":1};
    findCardTop();
    MVCApp.Toolbox.common.setBrightness("frmFindAStoreMapView");
    flexCardAnimatePostshow();
    var createInstanceObject = new bright.createInstance();
    var returnedValue = createInstanceObject.getBrightness();
    var returnedValueStr=returnedValue+"";
    MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValueStr);
    //#ifdef iphone
    if(homeFormHideFlag){
      homeFormHideFlag = false;
      MVCApp.Toolbox.common.requestReview();
    }
    if(carouselFlexArray.length > 1){
      frmHome.flexCarouselContainer.scrollToWidget(frmHome.flexCarouselContainer["flexCarousel" + 0]);
    }
    try{MVCApp.destroyStoreLocator();}catch(e){
      if(e){
        TealiumManager.trackEvent("Exception While Destroying The Store Locator Form From Home Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
    //#endif
    //#ifdef android
    counter++;
      frmHome.flxHeaderWrap.cmrImageSearch.setEnabled(true);
      frmHome.flxSearchOverlay.flxVisual.cmrImageSearch.setEnabled(true);
      if(deeplink){
        if(MVCApp.Toolbox.common.determineIfExternal(deeplink)){
          //FacebookAnalytics.reportEvents("", "", deeplink, "PushNotification");
        }
        MVCApp.Toolbox.common.openApplicationURL(deeplink);
        deeplink=null;
        counter=0;
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    //#endif
    var lTealiumTagObj = gblTealiumTagObj;
    lTealiumTagObj.page_id = "Home Screen: Michaels Mobile App";
    lTealiumTagObj.page_name = "Home Screen: Michaels Mobile App";
    lTealiumTagObj.page_type = "Home Screen";
    lTealiumTagObj.page_category_name = "Home";
    lTealiumTagObj.page_category = "Home";
    TealiumManager.trackView("Home Screen", lTealiumTagObj);
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    var storeid= MVCApp.Toolbox.common.getFavouriteStore();
    var loginStatus= MVCApp.Toolbox.common.getLoginStatus();
    cust360Obj.favouriteStore=storeid;
    cust360Obj.loginStatus=loginStatus;
    if(!gblFromHomeTile){
    	TealiumManager.trackView("Home Screen", lTealiumTagObj);
      	MVCApp.Customer360.sendInteractionEvent("Home", cust360Obj);
    }else{
      gblFromHomeTile = false;
    }
  }
  
  function onHideOfHomeForm(){
    MVCApp.Toolbox.common.dismissLoadingIndicator();
    homeFormHideFlag = true;
    try{
      kony.timer.cancel("HomeCarouselTimer");
    }catch(e){
      if(e){
        TealiumManager.trackEvent("Timer Exception For Carousel From Home Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
  }
  
  function assignEventToWidget(pWidget,pEvent,pEventValue){
    if(pWidget){
      pWidget[pEvent] = pEventValue;
    }else{
      TealiumManager.trackEvent("Home Screen Widget Exception", {exception_name:"Exception While Loading The Home Screen Widget",exception_reason:"Improper Form Initialization",exception_trace:"viewHome.js:assignEventToWidget:Line # 338",exception_type:"TypeError"});
    }
  }
  
  function bindEvents(){
    assignEventToWidget(frmHome.flxMainContainer,"onScrollStart",frmHomeOnScrollStart);
  //  assignEventToWidget(frmHome.flxMainContainer,"onScrolling",frmHomeOnScrolling);
    assignEventToWidget(frmHome.flxMainContainer,"onScrollEnd",frmHomeFooterOnScrollEnd);
    assignEventToWidget(frmHome.btnRewards,"onClick",onClickBtnRewards);
    assignEventToWidget(frmHome.lblRewards,"onTouchEnd",onClickBtnRewards);
    assignEventToWidget(frmHome.buttonStoreMap,"onClick",onClickBtnStoreMap);
    assignEventToWidget(frmHome.lblStoreMap,"onTouchEnd",onClickBtnStoreMap);
    assignEventToWidget(frmHome.flexCardAnimate,"height",cardContentHeight);
    assignEventToWidget(frmHome.flexShadowTop,"opacity",0);
    assignEventToWidget(frmHome.flexShadowBottom,"opacity",0);
    assignEventToWidget(frmHome.btnChangeStore,"onClick",onClickBtnChangeStore);
    assignEventToWidget(frmHome.btnChooseStore,"onClick",onClickBtnChooseStore);
    assignEventToWidget(frmHome.btnStoreInfo,"onClick",onClickBtnStoreInfo);
    assignEventToWidget(frmHome.btnMyStore,"onClick",showStoreInfo);
    assignEventToWidget(frmHome.lblContactNo,"onTouchEnd",dialPhone);
    assignEventToWidget(frmHome.lblChevron,"onTouchEnd",showStoreInfo);
    assignEventToWidget(frmHome.btnStoreMap,"onClick",onClickBtnStoreMap);
    assignEventToWidget(frmHome,"preShow",onPreShowOfHomeForm);
    assignEventToWidget(frmHome,"postShow",onPostShowOfHomeForm);
    assignEventToWidget(frmHome,"onHide",onHideOfHomeForm);
    assignEventToWidget(frmHome,"onDeviceBack",onDeviceBackOfHomeForm);
    assignEventToWidget(frmHome.btnGotIt,"onClick",onClickGotIt);
    //assignEventToWidget(frmHome.flxWhatsNew,"onTouchEnd",function(){});
    assignEventToWidget(frmHome.flxWhatsNew,"onClick",function(){});
    MVCApp.tabbar.bindEvents(frmHome);
    MVCApp.tabbar.bindIcons(frmHome,"frmHome");
    MVCApp.searchBar.bindEvents(frmHome,"frmHome");
    
    frmHome.flexCardAnimate.height=cardContentHeight;
    frmHome.flexShadowTop.opacity=0;
    frmHome.flexShadowBottom.opacity=0;
  }
  
  function onClickCarousel(eventObj){
    frmObject = kony.application.getCurrentForm();
    var storeModeEnabled=false;
    var contentURL = eventObj.info ? eventObj.info.c_contentUrl : null;
    if(contentURL && contentURL !== ""){
      var selectedIndex = "";
      try{
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeModeEnabled=true;
        }
        selectedIndex = eventObj.id.length - Number(eventObj.id.charAt(eventObj.id.length - 1));
      }catch(e){
        if(e){
          TealiumManager.trackEvent("Exception While Calculating The Selected Index In Home Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
      contentURL = MVCApp.Toolbox.common.formatURLBeforeLoading(contentURL,MVCApp.Toolbox.common.findIfWebviewUrlisExternal(contentURL));      
      if(MVCApp.Toolbox.common.findIfWebviewUrlisExternal(contentURL)){
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18.common.webview.redirectMessage"),"",constants.ALERT_TYPE_CONFIRMATION,function(response){
          if(response){
            kony.application.openURL(contentURL);
          }else{
            kony.print("do nothing");
          }
        },MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel"));
      }else if(contentURL.indexOf("appservices")<0){
        MVCApp.getProductsListWebController().loadWebView(contentURL);
      }else
        if(contentURL.indexOf("frmEvents") > -1 || contentURL.indexOf("frmCoupons") > -1 || contentURL.indexOf("frmWeeklyAds") > -1){
          var storeString = kony.store.getItem("myLocationDetails") ||"";
          if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
            MVCApp.Toolbox.common.openApplicationURL(contentURL);
          }else{
            if(storeString !== ""){
              MVCApp.Toolbox.common.openApplicationURL(contentURL);   
            }else{
              var lAlertTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.noStoreSet");
              if(contentURL.indexOf("frmEvents") > -1){
                lAlertTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreEvents");
              }else if(contentURL.indexOf("frmCoupons") > -1){
                lAlertTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreCoupons");
              }else if(contentURL.indexOf("frmWeeklyAds") > -1){
                lAlertTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd");
              }
              var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"home","contentURL":contentURL};
              whenStoreNotSet = JSON.stringify(whenStoreNotSet);
              kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
              MVCApp.Toolbox.common.customAlert(lAlertTitle,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
          }
        } else{
          //#ifdef iphone
          contentURL = decodeURI(contentURL);
          contentURL = contentURL.replace(/%2C/g,",");
          contentURL = encodeURI(contentURL);
          //#endif
          if(MVCApp.Toolbox.common.determineIfExternal(contentURL)){
            //FacebookAnalytics.reportEvents("", "", contentURL, "frmHome");
          }
          MVCApp.Toolbox.common.openApplicationURL(contentURL); 
        } 
      var carousalData={};
      carousalData.index=selectedIndex;
      carousalData.contentURL=contentURL;
      carousalData.storeMode=storeModeEnabled;
      kony.print("carousalData::"+JSON.stringify([{"carousalData":JSON.stringify(carousalData)}]));  
      MVCApp.sendMetricReport(frmHome,[{"carousalData":JSON.stringify(carousalData)}]);
      TealiumManager.trackEvent("Corousal click From Home Page", {conversion_category:"Home Page",conversion_id:"Corousal",conversion_action:2});
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.bannerLink=contentURL;
      MVCApp.Customer360.sendInteractionEvent("HomeBanner", cust360Obj);
    }
  }

	function flexCardAnimatePostshow(){
   	var newCards=frmHome.fxlContainer.widgets();
    var firstNewCard=newCards[0];
    //egLogger("firstNewCard ID = "+firstNewCard.id);
    if(firstNewCard && firstNewCard.frame){
      var cardHeight = firstNewCard.frame.height;
      if(cardHeight != 0 && cardHeight != undefined && cardHeight !=""){
       cardContentHeight=firstNewCard.frame.height; 
      }
      overlayCardHeight=cardContentHeight;  
      //egLogger("cardContentHeight = "+cardContentHeight);
      frmHome.flexCardAnimate.height=cardContentHeight;
      isHomeFormLoaded=true;
    }
  }
  function hideAndShowHeader(){
  var offsetY = frmHome.fxlContainer.contentOffsetMeasured.y;
  offsetY=parseInt(offsetY);
  var contentHeight = frmHome.fxlContainer.contentSizeMeasured.height;
  var viewPortHeight = frmHome.fxlContainer.frame.height;
  if(contentHeight == offsetY + viewPortHeight){
  	//alert("normal");
   // addHomeHeader();
  }else if (offsetY == 0){
  	//alert("normal");
    addHomeHeader(offsetY);
  }else if(offsetY > prevContentOffset){
    //alert("do hide");
    removeHomeHeader(offsetY);
  }else if(offsetY < prevContentOffset){
   // alert("normal");
   // addHomeHeader();
  }
  prevContentOffset = offsetY; 
  }
  
  function addHomeHeader(offsetY) {  
    var topValue = "";
    if(offsetY<0){
      topValue ="0dp";
    }else{
      topValue = "-"+offsetY.toString()+"";
    }
  frmHome.flxHeaderWrap.flxSearchContainer.top = topValue;
  //frmHome.flxHeaderWrap.flxSearchContainer.width = "100%";
  frmHome.flxHeaderWrap.flxSearchContainer.left = "0dp"; 
  frmHome.flxMyStoreContainer.top = "97dp";
  	//function test_CALLBACK() { }
  	/*frmHome.flxHeaderWrap.flxSearchContainer.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"0dp","left":"0dp", "width":"100%"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1}
    );frmHome.flxMyStoreContainer.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"97dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1}
    );*/
  
}

function removeHomeHeader(offsetY) {  
  var topValue = "";
  if(offsetY<45){
    topValue = "-"+offsetY.toString()+"dp";
  }else{
    topValue = "-45dp";
  }
  frmHome.flxHeaderWrap.flxSearchContainer.top = topValue;
  frmHome.flxHeaderWrap.flxSearchContainer.width = "73%";
  frmHome.flxHeaderWrap.flxSearchContainer.left = "-45dp"; 
  frmHome.flxMyStoreContainer.top = "50dp";
  	//function test_CALLBACK() { }
  /*	frmHome.flxHeaderWrap.flxSearchContainer.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"-45dp","left":"45dp", "width":"73%"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1}
    );
  frmHome.flxMyStoreContainer.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "top":"50dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":1}
    );*/
  
}
  
  
  
  function updateScreen(results){
	if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
      var locDetails=kony.store.getItem("myLocationDetails") || "";
      try {
        locDetails=JSON.parse(locDetails);
        loadMyStore(locDetails);
      } catch(e) {
        MVCApp.sendMetricReport({"Parse_Error_DW":"viewHome.js on updateScreen for locDetails:" + JSON.stringify(e)});
        loadMyStore("");
        if(e){
          TealiumManager.trackEvent("Exception While Updating The Home Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
    }else{
      loadMyStore("");
    }   
     //carousel hieght changes 
    var screenWidth = getWidthFromStore();
    kony.print("the screen Width is in this call   ___________  "+screenWidth);
    var imgHeight = parseInt((screenWidth*0.94)/1.8);
    var imgHeight1 = imgHeight.toString()+"dp";
    flxCarousel.height = imgHeight1;
    flxCarousel.imgBanner.height = imgHeight1;
    //#ifdef android
	flxCarousel.forceLayout();
   //#endif 
    arrangeHomeData();
    var enableWhatsNew = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.enable.whatsNew");
    var displayNew = kony.store.getItem("VersionUpdate") !== null ? kony.store.getItem("VersionUpdate") : "true" ;
    var showWhatsNew = kony.store.getItem("showWhatsNew");
    kony.print("deeplinkEnabled*****"+ deeplinkEnabled );
    kony.print("displayNew*****"+ displayNew );
    kony.print("showWhatsNew*****"+ showWhatsNew );
    if (enableWhatsNew === "true" && !deeplinkEnabled ){
       if(displayNew === "true" || showWhatsNew == "true"){
    	whatsNewInVerson();
    	}
    }
    if(!frombrowserdeeplink)
    {
    	show();
    }
   MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  function arrangeHomeData(){
    var homeData = kony.store.getItem("homeContent");
    var tempData = [];
    var tempNotContSeqData = [];
    for(var i=0;i<homeData.length;i++){
      var contentSequence = homeData[i].c_contentSequence;
      if(contentSequence != undefined){
        tempData.push(homeData[i]);
      }else{
        tempNotContSeqData.push(homeData[i]);
      }
    }
    var newArray = tempData.concat(tempNotContSeqData);
    //tempData.sort(function(a, b){return a.c_contentSequence-b.c_contentSequence;});
    loadHomeData(newArray);
  }

  function loadHomeData(homeData){
    var langSetNew=kony.store.getItem("languageSelected");
  	MVCApp.sendMetricReport(frmHome, [{"device_Locale":langSetNew}]);
    var tilesData = [];
    var carouselData = [];
    for(var j=0;j<homeData.length;j++){
      if(homeData[j].c_contentGroup == "carousel"){
        if(homeData[j].c_contentImage){
          carouselData.push(homeData[j]);
        }
      }else{
        tilesData.push(homeData[j]);
      }
    }
    tilesData.sort(function(a, b){return a.c_contentSequence-b.c_contentSequence;});
    carouselData.sort(function(a, b){return a.c_contentSequence-b.c_contentSequence;});
    loadCarouselData(carouselData);
    frmHome.fxlContainer.removeAll();
    var screenWidth = getWidthFromStore();
    kony.print("the screen Width is ___________ "+screenWidth);
    //#ifdef iphone
    var imgHeight = parseInt((screenWidth*0.94)/2.18); 
    //#elseif
    var imgHeight = parseInt((screenWidth*0.94)/2.18);
    //#endif
    var imgHeight1 = imgHeight.toString()+"dp";
    kony.print("image height----"+imgHeight1);
    eventImageData = [];
    eventLabelData = [];
    for(var i=0;i<tilesData.length;i++){
      eventImageData.push(tilesData[i].c_contentImage || "");
      eventLabelData.push(tilesData[i].name || "");
      var FlexContainer0fce981fc42ff4b = new kony.ui.FlexContainer({
        //"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "FlexContainer0fce981fc42ff4b"+i,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
		"height": cardContentHeight,
        "onClick" :onClickTiles,
        "zIndex": 1
    }, {}, {});
    FlexContainer0fce981fc42ff4b.setDefaultUnit(kony.flex.DP);
    var Image0fc33bd2b89234e = new kony.ui.Image2({
      	"height": imgHeight1,
        "id": "Image0fc33bd2b89234e"+i,
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
      	"onDownloadComplete":function(ref,imagesrc, issuccess){downloadCompleteTile(ref,imagesrc, issuccess)},
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": tilesData[i].c_contentImage || "",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer09c499ee536f149 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
      	"top": "0dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "FlexContainer09c499ee536f149"+i,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "sknFlexEventTab",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer09c499ee536f149.setDefaultUnit(kony.flex.DP);
    var Label08863e71c862e4d = new kony.ui.Label({
        "height": "100%",
        "id": "Label08863e71c862e4d"+i,
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px",
        "text": tilesData[i].name || "",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Label005e1b1668e8f43 = new kony.ui.Label({
        "height": "100%",
        "id": "Label005e1b1668e8f43"+i,
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblIcon24px606060",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer09c499ee536f149.add(Label08863e71c862e4d, Label005e1b1668e8f43);
    FlexContainer0fce981fc42ff4b.add(Image0fc33bd2b89234e, FlexContainer09c499ee536f149);
    frmHome.fxlContainer.add(FlexContainer0fce981fc42ff4b);
    }
    var flxSpace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxSpace",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "30dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpace.setDefaultUnit(kony.flex.DP);
    frmHome.fxlContainer.add(flxSpace);
    
    for(j=0;j<tilesData.length;j++){
      var cURL = tilesData[j].c_contentUrl || "";
      var data = {};
       data.contentURL = cURL;
       frmHome["FlexContainer0fce981fc42ff4b"+j].info = {"data":data,"index":j};
    }
  }
  
  function loadCarouselData(carouselData){
    carouselFlexArray = [];
    nextCarouselIndicator = 1;
    currentCarouselIndicator = 0;
    frmHome.flexCarouselContainer.removeAll();
    frmHome.flexCarouselIndicatorContainer.removeAll();
    indicatorsLength = carouselData.length;
    var flexCarouselIndicator = null;
    if(carouselData.length > 0){
      frmHome.flexCarouselIndicatorContainer.width = carouselData.length * 12 + "dp";
    }
	for(var i=0;i<carouselData.length;i++){
      carouselData[i].indicator = i;
      var imgCarouselIndicatorSrc;
      if(0 == i){
        imgCarouselIndicatorSrc = 'pager_on1.png';
      }else{
        imgCarouselIndicatorSrc = 'pager_off1.png';
      }
      var flexCarouselMainObj = {
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "kony.flex.USE_PREFERED_SIZE",
          "width": "100%",
          "id": "flexCarousel"+i,
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "skin": "slFbox",
          "top": "0dp",
          "bounces": false,
          "zIndex": carouselData.length-i,
          "onClick": onClickCarousel
      };
      var flexCarouselMain = new kony.ui.FlexContainer(flexCarouselMainObj, {}, {});
      flexCarouselMain.info = {"c_contentUrl": carouselData[i].c_contentUrl};
      flexCarouselMain.setDefaultUnit(kony.flex.DP);
      flexCarouselMainObj.height = "95%"
      flexCarouselMainObj.onClick = MVCApp.getHomeController().onClickCarousel;
      var imgCarouselMainObj = {
          "height": "kony.flex.USE_PREFERED_SIZE",
          "id": "imgCarousel"+i,
          "isVisible": true,
          "left": "0dp",
          "skin": "slImage",
          "src": carouselData[i].c_contentImage,
          "imageWhileDownloading": "white_1080x142.png",
          //"onDownloadComplete":function(ref,imagesrc, issuccess){downloadCompleteCarousel(ref,imagesrc, issuccess)},
          "top": "0dp",
          "bottom": "10dp",
          "width": "100%",
          "zIndex": 1
      };
      var imgCarouselMain = new kony.ui.Image2(imgCarouselMainObj, {
          "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
      }, {});
      var imgScrollCarouselObj = imgCarouselMainObj;
      imgScrollCarouselObj.id = imgScrollCarouselObj.id.slice(0,imgScrollCarouselObj.id.length-1) + "Scroll" + i;
      var imgScrollCarouselMain = new kony.ui.Image2(imgCarouselMainObj, {
          "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
      }, {});
      imgCarouselMainObj.height = "100%";
      flexCarouselMain.add(imgCarouselMain);
      frmHome.flexCarouselContainer.add(flexCarouselMain);
      frmHome.flexCarouselContainer.onScrollEnd = onScrollEndHomeCarouselCallback;
      //#ifdef android
      frmHome.flexCarouselContainer.onScrollTouchReleased = onScrollTouchReleasedCarouselCallback;
      //#endif
      var imgCarouselIndicatorObj = {
          "height": "100%",
          "id": "imgCarouselIndicator"+i,
          "isVisible": true,
          "left": "2dp",
          "skin": "slImage",
          "src": imgCarouselIndicatorSrc,
          "top": 0,
          "width": "10dp",
          "zIndex": 1
      };
      var imgCarouselIndicator = new kony.ui.Image2(imgCarouselIndicatorObj, {
          "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
          "padding": [0, 0, 0, 0],
          "paddingInPixel": false
      }, {});
      frmHome.flexCarouselIndicatorContainer.add(imgCarouselIndicator);
      carouselFlexArray.push(flexCarouselMain);
    }

    var carData = "";
    if(carouselData.length ==2){
      carData = carouselDataLengthTwo(carouselData);
    }else{
      carData = carouselData;
    }
    if(carData.length > 0){
      frmHome.flexCarouselContainer.setVisibility(true);
    }else{
      frmHome.flexCarouselContainer.setVisibility(false);
    }
  }

  function carouselSwipe(){
    try{
      if(carouselFlexArray!== null && carouselFlexArray !== undefined){
        if(currentCarouselIndicator == carouselFlexArray.length-1){
          nextCarouselIndicator = 0;
        }else if(currentCarouselIndicator == 1 && carouselFlexArray.length == 2){
          nextCarouselIndicator = 0;
        }else if(currentCarouselIndicator == carouselFlexArray.length){
          currentCarouselIndicator = 0;
          nextCarouselIndicator = 1;
        }else if(currentCarouselIndicator == -1){
          currentCarouselIndicator = carouselFlexArray.length-1;
          nextCarouselIndicator = 0;
        }else{
          nextCarouselIndicator = currentCarouselIndicator + 1;
        }
        if(carouselFlexArray.length > 1){
          var shiftedObj = carouselFlexArray.shift();
          carouselFlexArray.push(shiftedObj);
          for(var i=0; i < carouselFlexArray.length; i++){
            if(frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i]){
              if(i == nextCarouselIndicator){
                frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i].src = 'pager_on1.png';
              }else{
                frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i].src = 'pager_off1.png';
              }
            }
          }
          if(frmHome.flexCarouselContainer["flexCarousel"+nextCarouselIndicator]){
            //#ifdef android
            frmHome.flexCarouselContainer.scrollToWidget(frmHome.flexCarouselContainer["flexCarousel"+nextCarouselIndicator],true);
            //#else
            frmHome.flexCarouselContainer.scrollToWidget(frmHome.flexCarouselContainer["flexCarousel"+nextCarouselIndicator]);
            //#endif
          }
          currentCarouselIndicator++;
          nextCarouselIndicator++;
        }
      }
    }catch(error){
      kony.print("Error in carouselSwipe - "+JSON.stringify(error));
      if(error){
        TealiumManager.trackEvent("Exception While Swiping The Carousel In Home Screen", {exception_name:error.name,exception_reason:error.message,exception_trace:error.stack,exception_type:error});
      }
    }
  }

  function carouselDataLengthTwo(carouselData){
    var tempData = carouselData;
    tempData.push(carouselData[0]);
    tempData.push(carouselData[1]);
    return tempData;
  }
  
  //#ifdef android
  function onScrollTouchReleasedCarouselCallback(){
    kony.print("***onScrollTouchReleasedCarouselCallback");
    carouselTouchFlag = true;
  }
  //#endif
  
  function onScrollEndHomeCarouselCallback(scrolledObj){
    //#ifdef android
    if(carouselTouchFlag){
      carouselTouchFlag = false;
    //#endif
      try{
        kony.timer.cancel("HomeCarouselTimer");
      }catch(error){
        kony.print("Error in onScrollEndHomeCarouselCallback while canceling HomeCarouselTimer - "+JSON.stringify(error));
        if(error){
          TealiumManager.trackEvent("Home Carousel Timer Exception In Home Screen", {exception_name:error.name,exception_reason:error.message,exception_trace:error.stack,exception_type:error});
        }
      }
      try{
        currentCarouselIndicator = Math.round(scrolledObj.contentOffsetMeasured.x/scrolledObj.frame.width);
        for(var i=0; i < carouselFlexArray.length; i++){
          if(frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i]){
            if(i == currentCarouselIndicator){
              frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i].src = 'pager_on1.png';
            }else{
              frmHome.flexCarouselIndicatorContainer["imgCarouselIndicator" + i].src = 'pager_off1.png';
            }
          }
        }
      }catch(error){
        kony.print("Error in onScrollEndHomeCarouselCallback - "+JSON.stringify(error));
        if(error){
          TealiumManager.trackEvent("Exception On Scrolling Carousel To End In Home Screen", {exception_name:error.name,exception_reason:error.message,exception_trace:error.stack,exception_type:error});
        }
      }
    //#ifdef android
    }
    //#endif
  }


  function loadMyStore(location){
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      MVCApp.getHomeController().setStoreModeDetails();
    }else{
      frmHome.flxMyStoreContainer.isVisible = true;
      frmHome.flxStoreModeContainer.isVisible = false;
      if(location!= null && location != ""){
        frmHome.flxChooseAStore.isVisible = false;
        frmHome.flxStoreName.isVisible = true;
        frmHome.flxStoreInfo.setVisibility(true);
        // If location has no store name, use city
        if(location.address2 !== "") {
          frmHome.lblStoreName.text=location.address2; 
        } else {
          frmHome.lblStoreName.text=location.city;
        }
        frmHome.lblAdressStore.text=location.address1;
        var province = location.province || "";
        if(location.country === "US" || location.country_code === "US"){
        let state = location.state ? location.state : location.state_code;
        let postalCode = location.postalcode ? location.postalcode : location.postal_code;
        frmHome.lblAdressStore1.text=location.city+", "+(state ? state : "")+" "+(postalCode ? postalCode : "");
        }else{
        let postalCode = location.postalcode ? location.postalcode : location.postal_code;
        frmHome.lblAdressStore1.text=location.city+", "+location.province+" "+(postalCode ? postalCode : "");  
       }
        frmHome.lblContactNo.text=location.phone; 
        frmHome.lblOPenUntil.text=" ";
      }else{
        frmHome.flxChooseAStore.isVisible = true;
        frmHome.flxStoreName.isVisible = false;
        frmHome.flxStoreInfo.isVisible = false;
        frmHome.lblChevron.text = "D";
      }
    }
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  function onClickTiles(eventObj){
    var data = eventObj.info["data"];
    var index = eventObj.info["index"];
    var contentURL = data.contentURL || "";

    var storeModeEnabled=false;
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeModeEnabled=true;
    }
    var region=MVCApp.Toolbox.common.getRegion();
    if(contentURL !== ""){
      contentURL = MVCApp.Toolbox.common.formatURLBeforeLoading(contentURL,MVCApp.Toolbox.common.findIfWebviewUrlisExternal(contentURL));
      kony.print("formatURLBeforeLoading---------->" + contentURL);
      if(MVCApp.Toolbox.common.findIfWebviewUrlisExternal(contentURL)){
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18.common.webview.redirectMessage"),"",constants.ALERT_TYPE_CONFIRMATION,function(response){
          if(response){
            kony.application.openURL(contentURL);
          }else{
            kony.print("do nothing");
          }
        },MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel"));
      }else if(contentURL.indexOf("appservices")<0){
        MVCApp.getProductsListWebController().loadWebView(contentURL);
      }else{
        if(contentURL.indexOf("frmChatBox")>-1){
          //KNYMetricsService.sendEvent("Custom", null,"frmHome", "", null, {"ChatboxEntry":"frmHome",region:region});
          var reportData={"ChatboxEntry":"frmHome",region:region,"storeModeEnabled":storeModeEnabled};
          MVCApp.sendMetricReport(frmChatBox,[{"ChatBotEntry":JSON.stringify(reportData)}]);
        }else if(contentURL.indexOf("frmCoupons")>-1){
          contentURL += '&fromHomeTile=true';
        }
         //#ifdef iphone
           contentURL = decodeURI(contentURL);
           contentURL = contentURL.replace(/%2C/g,",");
           contentURL = encodeURI(contentURL);
        //#endif
        MVCApp.Toolbox.common.openApplicationURL(contentURL); 
      }

      //#ifdef iphone
      contentURL = decodeURI(contentURL);
      contentURL = contentURL.replace(/%2C/g,",");
      contentURL = encodeURI(contentURL);
      //#endif
      if(MVCApp.Toolbox.common.determineIfExternal(contentURL)){
        //FacebookAnalytics.reportEvents("", "", contentURL, "frmHome");
      }


      var tileData={};
      tileData.index=index;
      tileData.contentURL=contentURL;
      tileData.storeMode=storeModeEnabled;
      kony.print("tileData::"+JSON.stringify([{"tileData":JSON.stringify(tileData)}]));  
      MVCApp.sendMetricReport(frmHome,[{"tileData":JSON.stringify(tileData)}]);
      //var metricsServiceObj = servicesObj.getMetricsService();
      KNYMetricsService.sendEvent("Custom",null, "frmHome", "", null, tileData);

    }
  }
  
  
  function downloadCompleteTile(ref,imagesrc, issuccess){
    var id = ref.id || "";
    if(id!= null && id !=""){
      id = id.replace("Image0fc33bd2b89234e", "");
    }
    if(!issuccess){
      if(frmHome["FlexContainer0fce981fc42ff4b" + id]){
				frmHome["FlexContainer0fce981fc42ff4b" + id].setVisibility(false);
		}
    }
  }
  
  function downloadCompleteCarousel(ref,imagesrc, issuccess){
    if(kony.application.getCurrentForm() && kony.application.getCurrentForm().id == "frmHome"){
      var id = ref.id || "";
      if(id!= null && id !=""){
        id = id.replace("imgCarousel", "");
      }
      if(!issuccess){
        frmHome.flexCarouselContainer.remove(frmHome["flexCarousel"+id]);
        var indicatorId = carouselFlexArray.length-1;
        frmHome.flexCarouselContainer.remove(frmHome["imgCarouselIndicator"+indicatorId]);
        carouselFlexArray.splice(id,1);
      }
    }
  }
  
  function onClickBtnRewards(){
    var lUserObject = kony.store.getItem("userObj");
    var storeModeEnabled=false;
    if(lUserObject){
      gblNewBrightnessValueStoreMode = gblBrightNessValue;
      gblIsNewBrightnessValuePresent = true;
      MVCApp.getHomeController().onClickRewards();
      frmObject = frmHome;
    }else{
      MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.signInToViewRewardsCard") ,"",constants.ALERT_TYPE_CONFIRMATION,onClickRewardsAlert,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSignIn"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
    }
    var region = MVCApp.Toolbox.common.getRegion();
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
         storeModeEnabled=true;
     }
    var reportData = {"region":region,"storeMode":storeModeEnabled};
    MVCApp.sendMetricReport(frmHome,[{"StoreModeRewardsClick":JSON.stringify(reportData)}]);
  }
  
  function onClickRewardsAlert(pResponse){
    if(pResponse){
      gblIsFromHomeScreen = true;
      rewardsInfoFlag = false;
      isFromShoppingList = false;
      MVCApp.getSignInController().load();
    }
  }
  
  function onClickBtnStoreMap(){
    var lParams = {};
    lParams.aisleNo = "";
    lParams.productName = "";
    lParams.calloutFlag = false;
    MVCApp.getStoreMapController().setEntryPoint("Home");
    MVCApp.getStoreMapController().load(lParams);
    var storeModeEnabled=false;
    var region = MVCApp.Toolbox.common.getRegion();
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
         storeModeEnabled=true;
     }
    var reportData = {"region":region,"storeModeEnabled":storeModeEnabled};
    MVCApp.sendMetricReport(frmHome,[{"StoreModeStoreMapClick":JSON.stringify(reportData)}]);
  }
  
  function whatsNewInVerson(){
    var data = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.NewFeature");
    kony.print("data*********"+ data);
    var dataArr = data ? data.split("#") : [];
    var dataSet = [];
    frmHome.segNewFeature.removeAll();
    if(dataArr.length > 0){
      for(var i =0; i < dataArr.length; i++){
        var dataRecord={};
        dataRecord.lblTitle = dataArr[i];
        dataSet.push(dataRecord);
      }
      kony.print("dataSet****"+JSON.stringify(dataSet));
    }
     frmHome.segNewFeature.widgetDataMap = {lblTitle:"lblTitle"};
     frmHome.segNewFeature.setData(dataSet);
    	var appVersion = "";
      if(appConfig.secureurl && appConfig.secureurl.indexOf('-') > 0){
        appVersion = appConfig.secureurl.substring(appConfig.secureurl.indexOf('-')+1, appConfig.secureurl.indexOf('.')).toUpperCase() + " - ";
      }
     appVersion += appConfig.appVersion;
     frmHome.lblHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.whatsNew")+appVersion;
     frmHome.flxNewFeature.forceLayout();	
     frmHome.flxWhatsNew.forceLayout();
     frmHome.flxWhatsNew.setVisibility(true);
  }
 
  function onClickGotIt(){
    kony.store.setItem("VersionUpdate","false");
    kony.store.setItem("showWhatsNew","false");
    frmHome.flxWhatsNew.setVisibility(false);
  }
  
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	loadMyStore:loadMyStore,
    	onDeviceBackOfHomeForm:onDeviceBackOfHomeForm,
    	onClickCarousel:onClickCarousel
    };
  
});
function onDeviceBackOfHomeForm(){
    if(voiceSearchFlag){
      onXButtonClick(voiceSearchForm);
          if(voiceSearchForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")){
          	voiceSearchFlag = false;
            voiceSearchForm.flxVoiceSearch.setVisibility(false);
          } else {
            voiceSearchFlag = true;
            tapToCancelFlag = true;
            deviceBackFlag = true;
          }
      MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
    }else{
      //Creates an object of class 'minimizeApplication'
      //#ifdef android
      var minimizeApplicationObject = new minimiseapp.minimizeApplication();
      //Invokes method 'minimise' on the object
      minimizeApplicationObject.minimise();
      //#endif
    }
  }


