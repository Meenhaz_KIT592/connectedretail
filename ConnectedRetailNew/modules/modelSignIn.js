/**
 * PUBLIC
 * This is the model for SignIn form
 */
var MVCApp = MVCApp || {};
MVCApp.SignInModel = (function(){
 var serviceType ="signupforloyaltyservice"; //MVCApp.serviceType.Registration;
  var operationId = "loyaltySignin";//MVCApp.serviceEndPoints.signInUser;
  function validateLogin(callback,requestParams){
   kony.print("SignInModel.js");
    requestParams.isLoyalty="true";
   MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
         gblCouponPageValue = true;
          callback(results);
      }   
    },function(error){
     var errStatus= error.Status || [];
     var errCode= "";
     if(errStatus.length>0){
       var errCodeObj=errStatus[0]||"";
       errCode=errCodeObj.errCode||"";
       MVCApp.SignInView().loginResponse(error);
     }
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      cust360Obj.signinStatus="failure";
      cust360Obj.failureCode =errCode;
      MVCApp.Customer360.sendInteractionEvent("SignIn", cust360Obj);
      kony.print("Response is "+JSON.stringify(error));
    }
                   );
  }
 	return{
      validateLogin:validateLogin
    };
});
