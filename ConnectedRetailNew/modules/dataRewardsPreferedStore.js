
/**
 * PUBLIC
 * This is the data object for the preferred store form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.dataRewardsPreferedStore = (function(json){

  var typeOf = "dataRewardsPreferedStore";
  var _data = json || {};	

  /**
	 * PUBLIC - setter for the model
	 * Set data from the JSON retrieved by the model
	 */
  
  function getListSegmentData(){
    try{
    var jSeg=0;
    var listSegData =[];
    var collection=_data.collection || [];
    var collectionArr=[];
    var resultsExist=false;
    var sugStoreData = [];
    if(MVCApp.Toolbox.common.isArray(collection)){
      collectionArr=collection;
    }else{
      collectionArr.push(collection);
    }
    var favStore = rewardsFavouriteStore;
    var favStoreObj = "";
    if(favStore!="" && favStore!=null && favStore!=undefined){
      favStoreObj = JSON.parse(favStore);
      if(favStoreObj!=""){
        listSegData.push(prepareSegmentData(favStoreObj,jSeg,true));
        MVCApp.getRewardsProfilePreferedStoreController().setPreviousLocation(0);
        jSeg++;
      }
    }  
       for(var i=0;i<collectionArr.length;i++){
       if (collectionArr[i].suggestedaddress){
          resultsExist=false;
          sugStoreData.push(collectionArr[i].suggestedaddress);
        } else {
          resultsExist=true;        
          var poi=collectionArr[i].poi || {};
          var favClientKey = favStoreObj.clientkey || "";
         if(poi.clientkey!=favClientKey){
         listSegData.push(prepareSegmentData(poi,jSeg,false));
         jSeg++;
         }         
       }
     }    
     return {
       		sugStoreData:sugStoreData,
           	listSegData:listSegData,
            resultsExist:resultsExist
           };
    }catch(e){
      kony.print("exception in dataRewardsPreferedStore"+JSON.stringify(e));
      if(e){
        TealiumManager.trackEvent("Get List Segment Data Exception in Rewards Preferred Store Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
     return [];
    } 
  }
  
  function setFavorite(eventobj){
    kony.print("info is "+JSON.stringify(eventobj.info));
    var prev=MVCApp.getRewardsProfilePreferedStoreController().getPreviousLocation();
    var i=eventobj.info.index;
    var dataSeg=frmRewardsProfileStep4.segStoresListView.data;
    var currData=dataSeg[i];
    if(currData.btnFavorite.text=="F"){
      currData.btnFavorite.text="f";
      //frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
      rewardsFavouriteStore="";
      frmRewardsProfileStep4.segStoresListView.setDataAt(currData,i,0);
    }else{
      currData.btnFavorite.text="F";
      var currDataString = JSON.stringify(currData);
      rewardsFavouriteStore = currDataString;
      //frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
      frmRewardsProfileStep4.segStoresListView.setDataAt(currData,i,0);
  }
   if(prev!==null && prev !== i){
      var prevData=dataSeg[prev];
      prevData.btnFavorite.text="f";
      frmRewardsProfileStep4.segStoresListView.setDataAt(prevData,prev,0);
    }
    prev=i;
    MVCApp.getRewardsProfilePreferedStoreController().setPreviousLocation(prev);
    var editStoreFlag = MVCApp.getRewardsProfilePreferedStoreController().getEditFlag();
    if(editStoreFlag){
      frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
    }else{
      frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
    }
  } 
  
  
    function prepareSegmentData(poi,i,isFavourite){
    var temp1={};
    var temp=poi;
    var temp2=poi;  
    var favText="f";
    if(isFavourite){
      	favText="F";
    }   
    temp.lbladdress1=poi.address1;  
    var province = poi.province || "";
    if(poi.country === "US")
    temp.lbladdress2=poi.city+", "+poi.state+" "+poi.postalcode;
    else 
    temp.lbladdress2=poi.city+", "+province+" "+poi.postalcode;
      
    temp.lblPhone={text:poi.phone,onClick:callToStore};
    var distance = poi._distance || "";
    temp.distanceinMiles=distance+" mi";
    temp.btnFavorite={text:favText,onClick:setFavorite,info:{"index":i}};
    
    return temp;
  }
  
  function callToStore(eventobj){
    var text=eventobj.text;
    try{
      kony.phone.dial(text);
    }catch(e){
      kony.print("unhandled error from iOS");
      if(e){
        TealiumManager.trackEvent("Call To Store Exception in Rewards Preferred Store Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
  }
  
  function getFavouriteStoreData(){
      try{
    var jSeg=0;
    var listSegData =[];
    var favStore = rewardsFavouriteStore;
    if(favStore!="" && favStore!=null && favStore!=undefined){
      var favStoreObj = JSON.parse(favStore);
      if(favStoreObj!=""){
        listSegData.push(prepareSegmentData(favStoreObj,jSeg,true));
        jSeg++;
      }
    }  
     return listSegData;
    }catch(e){
      kony.print("exception in dataRewardsPreferedStore"+JSON.stringify(e));
      if(e){
        TealiumManager.trackEvent("Favourite Store Parse Exception in Rewards Preferred Store Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
     return [];
    } 
  }
  
  
  function getDefaultFavouriteStoreData(){
      try{
    var jSeg=0;
    var listSegData =[];
    var favStore = rewardsDefaultFavouriteStore;
    var favStoreObj = "";
    if(favStore!="" && favStore!=null && favStore!=undefined){
      favStoreObj = JSON.parse(favStore);
    }
    if(favStoreObj!=""){
      listSegData.push(prepareSegmentData(favStoreObj,jSeg,true));
      jSeg++;
     }
    var collectionArr =  frmRewardsProfileStep4.segStoresListView.data;    
   for(var i=0;i<collectionArr.length;i++){
    	 var temp = collectionArr[i];
         var clientkey =collectionArr[i].clientkey || {};
         var favClientKey = favStoreObj.clientkey || "";
         if(clientkey!=favClientKey){
         temp.btnFavorite={text:"f",onClick:setFavorite,info:{"index":jSeg}};
         listSegData.push(temp);
         jSeg++;
         }         
     }
      
 	return listSegData;
    }catch(e){
      kony.print("exception in dataRewardsPreferedStore"+JSON.stringify(e));
      if(e){
        TealiumManager.trackEvent("Default Favourite Store Parse Exception in Rewards Preferred Store Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
     return [];
    } 
  }
  
  //Here we expose the public variables and functions
  return {
    typeOf: typeOf,
    getListSegmentData:getListSegmentData,
    getFavouriteStoreData:getFavouriteStoreData,
    getDefaultFavouriteStoreData:getDefaultFavouriteStoreData
  };
});
