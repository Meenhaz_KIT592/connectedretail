/**
 * PUBLIC
 * This is the model for Events form
 */
var MVCApp = MVCApp || {};
MVCApp.HelpModel = (function(){

  var serviceType = MVCApp.serviceType.MichaelJavaService;
  var operationId = MVCApp.serviceEndPoints.getHelpData;
  
  var requestParams ={};
  
  function getHelpDetails(callback){
    
    
var resultData = MVCApp.getHelpDetailsController().getHelpData() || "";
    
    if(resultData === ""){
             try{
               
            MVCApp.Toolbox.common.showLoadingIndicator("");
               
            MakeServiceCall(serviceType,operationId,requestParams,
                function(results){//successCallBack
              if(results.opstatus===0 || results.opstatus==="0" ){
                  MVCApp.getHelpDetailsController().setHelpData(results);
                  callback();
              } else{
                kony.print("Some error in Help service data " + JSON.stringify(results));
              }
               MVCApp.Toolbox.common.dismissLoadingIndicator();
            },function(error){//errorCallBack
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("Error response while getting help "+JSON.stringify(error));
            }
            );

          }catch(e){
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("excpetion occurred while getting help - "+JSON.stringify(e));
            if(e){
              TealiumManager.trackEvent("Get Help Details Exception in Help Service Call Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
          }
       }else{
         callback();
       }
  }
  
  function openCCPA()
  {
     var ccpaLink = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.CCPA.link");
     kony.application.openURL(ccpaLink);
   
  }
  
  function reviewApp(){
    
    var title = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.hearFrom");
    var message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertMessage");
    var yesLabel = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertYes");
    var noLabel = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertNo");
    TealiumManager.trackEvent("Review The App Button click From Help Page", {conversion_category:"Help",conversion_id:"Review The App",conversion_action:2});    
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    MVCApp.Customer360.sendInteractionEvent("ReviewApp", cust360Obj);
    MVCApp.Toolbox.common.customAlert(message,title,constants.ALERT_TYPE_CONFIRMATION,reviewAlertCallBack,yesLabel,noLabel);  
  }
  
  function reviewAlertCallBack(response){
    if(response){
      var resultData = MVCApp.getHelpDetailsController().getHelpData() || "";
      var reviewURL = "";
      //#ifdef iphone
          reviewURL = resultData !== "" ? resultData.iPhoneAppStore : "";
      //#else
          reviewURL = resultData !== "" ? resultData.androidPlayStoreURL : "";
      //#endif 

      if(reviewURL !== ""){
        try{
          //#ifdef iphone
    	  reviewURL = decodeURI(reviewURL);
   		  reviewURL = reviewURL.replace(/%2C/g,",");
    	  reviewURL = encodeURI(reviewURL);
    	  //#endif
          MVCApp.Toolbox.common.openApplicationURL(reviewURL);
        }catch(e){
          kony.print("expetion in opening URL "+JSON.stringify(e));
          if(e){
            TealiumManager.trackEvent("Review Alert Exception in Help Model Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
    }
    }
    if(!response){
      MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertMessageRatingNoOptionSelected"),"",constants.ALERT_TYPE_INFO,reviewAlertNoOptionCallBack,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),"");
    }
  }
  
  function reviewAlertNoOptionCallBack(response){
    if(response){
      try {
        var sub = "";
        var msgbody = "";
        var resultDat = MVCApp.getHelpDetailsController().getHelpData() || "";
        var to = resultDat !== "" ? resultDat.customerEmail : "";
        var cc = [""];
        var bcc = [""];
        var deviceInfo = kony.os.deviceInfo();
        var osVersion;
        var handSet = deviceInfo.model;
        var appVersion = appConfig.appVersion;
        //#ifdef iphone
        osVersion = deviceInfo.osversion;
        sub = "iOS "+MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.help.feedback")+" - "+osVersion+", "+handSet+", "+appVersion;
        //#endif

        //#ifdef android
        osVersion = deviceInfo.version;
        sub = "Android "+MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.help.feedback")+" - "+osVersion+", "+handSet+", "+appVersion;
        //#endif         	
        kony.phone.openEmail([to],cc,bcc,sub,msgbody,false,[]); 
      }
      catch(err){ 
        alert("error in opening Email:: "+err); 
        if(err){
          TealiumManager.trackEvent("Review Alert No Option Exception in Help Model Flow", {exception_name:err.name,exception_reason:err.message,exception_trace:err.stack,exception_type:err});
        }
      }
    }
  }
  
  function emailCustomerCare(){
    var resultData = MVCApp.getHelpDetailsController().getHelpData() || "";
    var emailId = resultData !== "" ? resultData.customerEmail : "";
    
    if(emailId !== ""){
      TealiumManager.trackEvent("Email Customer Service Button click From Help Page", {conversion_category:"Help",conversion_id:"Email Customer Service",conversion_action:2});    
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      MVCApp.Customer360.sendInteractionEvent("EmailCustomerService", cust360Obj);
      try{
        kony.phone.openEmail([emailId]);
      }catch(e){
        //2100 - Unable to send the Message
        // 2101 - Insufficient Permissions
        // 2102 - Cannot open mail, mail not configured
        // 2103 - Cannot open media gallery
        alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.mailNotConfig"));
        kony.print("ecpetion in invoking email  "+JSON.stringify(e));
        if(e){
          TealiumManager.trackEvent("Email Customer Care Exception in Help Model Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
    }
  }
  
  function callCustomerCare(){
    var resultData = MVCApp.getHelpDetailsController().getHelpData() || "";
    var dial = resultData !== "" ? resultData.customerContactNumber : "";
    if(dial !== ""){
      TealiumManager.trackEvent("Call Customer Service Button click From Help Page", {conversion_category:"Help",conversion_id:"Call Customer Service",conversion_action:2});    
      var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
      MVCApp.Customer360.sendInteractionEvent("CallCustomerService", cust360Obj);
      try{
        kony.phone.dial(dial);
      }catch(e){
        //returns 2101 error code - Insufficient Permissions
        kony.print("ecpetion in invoking dial pad "+JSON.stringify(e));
        if(e){
          TealiumManager.trackEvent("Call Customer Care Exception in Help Model Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
    }
  }
  
 	return{
      getHelpDetails:getHelpDetails,
      reviewApp:reviewApp,
      emailCustomerCare:emailCustomerCare,
      callCustomerCare:callCustomerCare,
      openCCPA:openCCPA
    };
});
