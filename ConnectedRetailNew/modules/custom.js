//SLASH Animation Script

function openCoupon() {
  	function anim_COUPON_Popup() {
      	//function test_CALLBACK() { }
      alert("in home");
      	var form1 = kony.application.getCurrentForm();
      	form1.flexCouponContainer.flxCouponDetailsPopup.isVisible = false;
		form1.flexCouponContainer.animate(
          kony.ui.createAnimation({"100":{"right":"0%", "stepConfig":{"timingFunction":kony.anim.EASE}}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25}
        );
    }
	var form = kony.application.getCurrentForm();
  	var transZoomOut = kony.ui.makeAffineTransform();
    transZoomOut.scale(0.5,0.5);
  	form.flexCouponContainer.flxCouponDetailsPopup.animate(
      kony.ui.createAnimation({"100":{"right":"0%", "stepConfig":{"timingFunction":kony.anim.EASE}, "transform":transZoomOut, "opacity":"0"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": anim_COUPON_Popup }
    );
}

function closeCoupon() {
	//function test_CALLBACK() { }
	var form = kony.application.getCurrentForm();
	form.flexCouponContainer.animate(
      kony.ui.createAnimation({"100":{"right":"-100%", "stepConfig":{"timingFunction":kony.anim.EASE}}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25}
    );
}

function openCouponDetails() {
	var form = kony.application.getCurrentForm();
  	form.flexCouponContainer.flxCouponDetailsPopup.isVisible = true;
  	var transZoomIn = kony.ui.makeAffineTransform();
    transZoomIn.scale(1,1);
  	form.flexCouponContainer.flxCouponDetailsPopup.animate(
      kony.ui.createAnimation({"100":{"right":"0%", "stepConfig":{"timingFunction":kony.anim.EASE}, "transform":transZoomIn, "opacity":"1"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1}
    );
}

function closeCouponDetails() {
  	function hide_COUPON_Details() {
      	var form2 = kony.application.getCurrentForm();
      	form2.flexCouponContainer.flxCouponDetailsPopup.isVisible = false;
    }
	var form3 = kony.application.getCurrentForm();
  	var transZoomOut = kony.ui.makeAffineTransform();
    transZoomOut.scale(0.5,0.5);
  	form3.flexCouponContainer.flxCouponDetailsPopup.animate(
      kony.ui.createAnimation({"100":{"right":"0%", "stepConfig":{"timingFunction":kony.anim.EASE}, "transform":transZoomOut, "opacity":"0"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": hide_COUPON_Details }
    );
}

/* Toggle My Store */
var storeToggle = true;
function myStoreToggle() {
  	//function test_CALLBACK() { }
  	var form = kony.application.getCurrentForm();
  	if(storeToggle === true){
      	form.flxMyStoreContainer.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":"45dp"}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
      	var trans100 = kony.ui.makeAffineTransform();
    	trans100.rotate(180);
      	form.lblChevron.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
      	Alert_InStoreMode();
    }
  	else {
        form.flxMyStoreContainer.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":"190dp"}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
      	var trans101 = kony.ui.makeAffineTransform();
    	trans101.rotate(0);
      	form.lblChevron.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans101}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5}
        );
    }
  	storeToggle = !storeToggle;
}


/* Search Results */
function checkSearchTyped(eventobject) {
  	/*var chkSearchText = this.text;
  	if(chkSearchText === '') {
    	searchBlur();
    }
  	else {
      	searchType();
    }*/
}

function searchFocus() {
/*  var form = kony.application.getCurrentForm();
  form.flxSearchResultsContainer.isVisible = true;
  form.segSearchOffers.isVisible = true;
  form.segSearchResults.isVisible = false; */
}
function searchBlur() {
 /* var form = kony.application.getCurrentForm();
  form.flxSearchResultsContainer.isVisible = false;
  form.segSearchOffers.isVisible = true;
  form.segSearchResults.isVisible = false;*/
}
function searchType() {
  /*var form = kony.application.getCurrentForm();
  form.flxSearchResultsContainer.isVisible = true;
  form.segSearchOffers.isVisible = true;
  form.segSearchResults.isVisible = true;*/
}

