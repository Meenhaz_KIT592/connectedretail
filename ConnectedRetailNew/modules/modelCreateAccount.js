/**
 * PUBLIC
 * This is the model for frmCreateAccount form
 */
var MVCApp = MVCApp || {};
MVCApp.CreateAccountModel = (function(){
 var serviceType = "";
  var operationId = "";
  var serviceType1 =   MVCApp.serviceType.SigninRewardsProfile;
  var operationId1 = MVCApp.serviceType.signUpWithRewards;
  function loadData(callback,requestParams){
    
    kony.print("modelCreateAccount.js");
   
    callback("");
  }
  
  
   
  function createAccount(callback,requestParams){
    kony.print("CreateAccountTwoModel.js");
     requestParams.isLoyalty="true";
    
    var langReg=kony.store.getItem("languageSelected");
    	 if(langReg=="en_CA" )
           {
             requestParams.division="MIKC";
           }
           else if( langReg=="fr_CA"){
      		  requestParams.division="MIKQ";
    	}
    
   MakeServiceCall( serviceType1,operationId1,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
          callback(results,"success");
      }   
    },function(results){
      kony.print("Response is "+JSON.stringify(results));
       callback(results,"failure");		
    }
                   );
    
  }
 	return{
      loadData:loadData,
      createAccount:createAccount
    };
});
