/** 
* Starts the initialisation of the first form 
* all subsequent initialisation code
* including event binding happens in the respective
* controllers which can be found in /modules/js.
* These controllers are named ctrl[name of the form].
* formHome comes with two controllers in order to
* reflect the two distinct screens which it offers.
* 
* This function is UI-linked to formMain.init in order to start the whole app.
*/
var isNoNetworkPopupAcknowledgePending = false;

function MakeServiceCall(serviceName,operationId,inputParams,callbackSuccess,callbackFailure,fromPDPFlag){
  if(MVCApp.Toolbox.common.checkIfNetworkExists()){
    var integrationObj = KNYMobileFabric.getIntegrationService(serviceName);
    inputParams.regionLang=MVCApp.Toolbox.common.getRegion();
    inputParams.appVersion = appConfig.appVersion;
    inputParams.app_Locale=kony.store.getItem("languageSelected")||"";
    inputParams.isStoreModeEnabled = (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) || false;
    kony.print("-------->Making Service Call to SERVICE NAME: "+ serviceName +" ; with OPERATION ID: "+ operationId);
    kony.print("-------->Input/Request parameters: "+JSON.stringify(inputParams));
   // var startDate = new Date();// this is only to track the time taken for the service
    integrationObj.invokeOperation(operationId, {},inputParams,
                                   function (results) {
      if(voiceSearchFlag && tapToCancelFlag){
        tapToCancelFlag = false;
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        if(deviceBackFlag){
          kony.print("VOICE SEARCH FORM::"+voiceSearchForm.id);
          voiceSearchForm.flxVoiceSearch.setVisibility(false);
          voiceSearchFlag = false;
          deviceBackFlag = false;
        }
        else
        	showMicPanel(voiceSearchForm);
        //return;
      }else{
      //  kony.print("--------->Total time to finish success:"+ new Date() - startDate);
        tapToCancelFlag = false;
        deviceBackFlag = false;
        results = results || {};
        if(results.opstatus === null || results.opstatus === undefined)
          results.opstatus = 0;
        kony.print("-------->Response Success result: "+JSON.stringify(results));
        networkAlertPopupFlag = false;
        callbackSuccess(results);
      }

    },
                                   function (results) {
      kony.print("-------->Response Failure result: "+JSON.stringify(results));
      if(voiceSearchFlag && tapToCancelFlag){
        tapToCancelFlag = false;
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        if(deviceBackFlag){
          voiceSearchForm.flxVoiceSearch.setVisibility(false);
          voiceSearchFlag = false;
          deviceBackFlag = false;
        }
        else
        	showMicPanel(voiceSearchForm);
      }else{
        tapToCancelFlag = false;
        deviceBackFlag = false;
        var opstatusTimeoutFlag = getOpstatus9001(results);
        //kony.print("--------->Total time to finish failure:"+ new Date() - startDate);
        var failureCallback = callbackFailure || function(result){
          if(operationId== MVCApp.serviceEndPoints.merge || operationId== MVCApp.serviceEndPoints.getCartDetails || operationId== MVCApp.serviceEndPoints.AddItemToBasket || operationId== MVCApp.serviceEndPoints.addCouponToCart || operationId== "GetBasketForUser"||operationId ===MVCApp.serviceType.signUpWithRewards || operationId === MVCApp.serviceType.GetRecommendations || operationId === MVCApp.serviceEndPoints.getProductProjectCategory || operationId === MVCApp.serviceType.fetchINStoreDetailsByID || operationId === MVCApp.serviceEndPoints.getBlockedStore || operationId === MVCApp.serviceEndPoints.searchProject || (serviceName === MVCApp.serviceType.imagesearch && operationId === MVCApp.serviceEndPoints.searchProduct) || (serviceName==="signupforloyaltyservice" && operationId==="loyaltySignin") || (serviceName==="signupforloyaltyservice" && operationId==="loyaltyRegistration") || operationId==MVCApp.serviceEndPoints.addCouponToCart
            ){
            callbackFailure(results);
          }else if(result.opstatus != "1011"){
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.serviceCurrentlyDown"), "");
          }
        };
        if(opstatusTimeoutFlag){
          var findIfErrorAuthorizationVal= findIfErrorAuthorization(results);
          if(findIfErrorAuthorizationVal){
            // suppressing the alert for 9001 status
          }else if(results.opstatus == "1011"){
            kony.print("*** isNoNetworkPopupAcknowledgePending = " + isNoNetworkPopupAcknowledgePending);
            if(appInForeground && !isNoNetworkPopupAcknowledgePending && !networkAlertPopupFlag){
              networkAlertPopupFlag = true;
              isNoNetworkPopupAcknowledgePending = true;
              MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), 
                        constants.ALERT_TYPE_INFO,acknowledgeNoNetworkPopupCallback,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
            }
            if(appInForeground && !isNoNetworkPopupAcknowledgePending && (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) && !networkAlertInStore){
              	networkAlertInStore = true;
              	isNoNetworkPopupAcknowledgePending = true;
                	MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailableInStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), 
                        constants.ALERT_TYPE_INFO,acknowledgeNoNetworkPopupCallback,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.NoNetworkInStoreAlert"));
      		}
            failureCallback(results);
          }else{
           if(operationId== MVCApp.serviceEndPoints.merge || operationId== MVCApp.serviceEndPoints.getCartDetails || operationId== MVCApp.serviceEndPoints.AddItemToBasket || operationId== "GetBasketForUser"|| operationId === MVCApp.serviceType.GetRecommendations || operationId === MVCApp.serviceEndPoints.getProductProjectCategory || operationId === MVCApp.serviceType.fetchINStoreDetailsByID || operationId === MVCApp.serviceEndPoints.getBlockedStore || operationId === MVCApp.serviceType.AddProductListLooping || operationId === MVCApp.serviceEndPoints.searchProject || operationId === MVCApp.serviceEndPoints.fetchStoreWifiDetails || operationId === MVCApp.serviceEndPoints.fetchWelcomeMessageByStoreId || (serviceName === MVCApp.serviceType.imagesearch && operationId === MVCApp.serviceEndPoints.searchProduct)|| (serviceName === "signupforloyaltyservice" && operationId === "loyaltySignin") || (serviceName === "signupforloyaltyservice" && operationId === "loyaltyRegistration")|| operationId==MVCApp.serviceEndPoints.addCouponToCart
             ){
              callbackFailure(results);
            }else if(operationId==="couponAndroidKM" || operationId==="couponIosKM"){
              callbackSuccess(results);// this is coded so that addtowalllet works fine without KM working
            }else{
              if(!voiceSearchFlag){
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.serviceCurrentlyDown"), "");
              } else {
                callbackFailure(results);
              }
            }
            if(fromPDPFlag){
              failureCallback(results,true); 
            }
          }
        }else{
           failureCallback(results);
         }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
      }
    }
                                  );
  }else{
    kony.print("IN NO NETWROK SCENARIO");
    MVCApp.Toolbox.common.dismissLoadingIndicator();
        /*if(operationId === MVCApp.serviceEndPoints.getProductProjectCategory)
      {
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"),"Alert",constants.ALERT_TYPE_INFO, 
              MVCApp.Toolbox.common.exit,"OK","");  
       
      }
    else
      {
        alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"));
      }
    //customAlert(MVCAppToolbox_geti18nValueVA("i18n.phone.Common.NetworkAvailabilityAlert"));
      }*/
    if(appInForeground && !isNoNetworkPopupAcknowledgePending && !networkAlertPopupFlag){
      networkAlertPopupFlag = true;
      isNoNetworkPopupAcknowledgePending = true;
      kony.print("*** isNoNetworkPopupAcknowledgePending = " + isNoNetworkPopupAcknowledgePending);
      	MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), 
                constants.ALERT_TYPE_INFO,acknowledgeNoNetworkPopupCallback,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
    }else{
      kony.print("VOICE SEARCH FLAG::"+voiceSearchFlag);
      if(voiceSearchFlag){
        kony.print("IN VOICE SEARCH");
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        kony.print("CURRENT FROM in MFCLIENT::"+currentForm.id);
        onXButtonClick(currentForm);
        currentForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
        currentForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable");
        currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
        currentForm.flxVoiceSearch.forceLayout();
      }
    }
  }
}

function acknowledgeNoNetworkPopupCallback(){
  isNoNetworkPopupAcknowledgePending = false;
  kony.print("*** isNoNetworkPopupAcknowledgePending = " + isNoNetworkPopupAcknowledgePending);
  if(voiceSearchFlag){
      kony.print("IN VOICE SEARCH of ALert popup");
      MVCApp.Toolbox.common.dismissLoadingIndicator();
      kony.print("CURRENT FROM in MFCLIENT::"+currentForm.id);
      kony.print("FRMOBJECT FROM in MFCLIENT::"+frmObject.id);
      onXButtonClick(currentForm);
      currentForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
      currentForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable");
      currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
      currentForm.flxVoiceSearch.setVisibility(true);
      currentForm.show();
      currentForm.flxVoiceSearch.forceLayout();
    }
}

function getOpstatus9001(resultTable)
{
var flag=false;
	
if(resultTable!=null && resultTable!=undefined){
	for (key in resultTable) {
			if ((key.match(/opstatus/) && (resultTable[key]=="9001" || resultTable[key]== 9001 ||resultTable[key]=="1582" ||resultTable[key]== 1582 || resultTable[key]=="5001" ||resultTable[key]== 5001 || resultTable[key]=="1011" || resultTable[key]== 1011)) || (key.match(/httpStatusCode/) && (resultTable[key] >="400" || resultTable[key]>= 400))){
		            flag= true;
		            break;
		        }
	}
	}
	return flag;
}
function findIfErrorAuthorization(resultTable){
  var flag=false;
  if(resultTable!=null && resultTable!=undefined){
    for (key in resultTable) {
      if((key.match(/errMsg/) && (resultTable[key].indexOf("Session already invalidated")>-1)))
        flag= true;
      break;
    }
  }
  return flag;
}