var MVCApp = MVCApp || {};

MVCApp.data = MVCApp.data || {};
MVCApp.data.ShoppingList = (function(json){
  
  var data = []; 
  var itemsData = {};
  if(json){
    try {
      itemsData = JSON.parse(json);
      data = itemsData.data;
    } catch(e) {
      MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"dataShoppingList.js on Line 13 for json:" + JSON.stringify(e)}]);
      if(e){
        TealiumManager.trackEvent("json Parse Exception in My List Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
  }
  
  var prevIndex = 0;
  gshoppingList = data;
function getDataForShoppingList() {
  var deleteBtnText =  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.delete");
  var segShoppingListData=[];
  var myLocationFlag = false;
  var isBlocked = false;
  var projText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.projectsHeader");
  var storeObj = {};
  var storeString=kony.store.getItem("myLocationDetails") ||"";
  if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.POI){
    storeObj = gblInStoreModeDetails.POI;
    myLocationFlag = "true";
  }else{
	if(storeString!=="") {
      try {
        storeObj = JSON.parse(storeString);
        myLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
      } catch(e) {
        MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"dataShoppingList.js on getDataForShoppingList for storeString:" + JSON.stringify(e)}]);
        if(e){
          TealiumManager.trackEvent("POI Parse Exception in My List Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
        }
      }
    }
  }
  
  if(storeObj && storeObj.isblocked){
    isBlocked = storeObj.isblocked || "";
  }
  if(data && data.length>0) {
    
    var headerCount =0;      
    var productExists = false;          
    var projectIdList = [];
   
  
    for(var j=0;j<data.length;j++) {
      
       
      var dataValue1=data[j] || {};
      var cRelatedProject = dataValue1.c_relatedProjects || [];
      if(cRelatedProject.length>0) {
        for(var c=0;c<cRelatedProject.length;c++) {
                var projIdFlag = false;
                var projectInfo = cRelatedProject[c];
                var tempText =    projText.concat(" ", projectInfo.name);
                if(projectIdList.length>0)
                  {
                    
                 
                   for(var p=0;p<projectIdList.length;p++)
                     {
                        var projId = projectIdList[p];
                        
                       if(projId ===tempText )
                         {
                         
                           projIdFlag =true;
                             break;
                         }


                     }
                    if(!projIdFlag)
                      {
                         
                        projectIdList.push(tempText);
                      }
                    projIdFlag = false;
                  }
                else
                  {
                     
                    projectIdList.push( tempText);
                    
                  }
               
                
              }
         }
       else
         {
             productExists= true;
           
         }
       
       }
      if(productExists)
        {
            headerCount++;
        }
    if(projectIdList.length>0)
      {
        headerCount = headerCount+projectIdList.length;
      }
      
      var HeaderList = [];
    
           
          
     for(var h=0;h<headerCount;h++)
       {
         var tempHeader=[];
          var list = []  ;
          var subList=[];
           
         var projHeader = "";
          if(h===0 && productExists )
            {
              projHeader= MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.products");// "Products"; 
              tempHeader.lblShoppingListHdr = {text :projHeader};
               tempHeader.btnExpand = {text:"D",onClick:expandSegment,info:{index:h,status:true}};
              tempHeader.flexMyListHeader = {onClick:expandSegment,info:{index:h,status:true}};
            }
          else if(h===0)
            {
              projHeader = projectIdList[0];
              var projectHeaderText =  projectIdList[0]; //MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.projectsHeader")+" "+projectIdList[0]
               tempHeader.lblShoppingListHdr = {text :projectHeaderText};
               tempHeader.btnExpand = {text:"D",onClick:expandSegment,info:{index:h,status:true}};
               tempHeader.flexMyListHeader = {onClick:expandSegment,info:{index:h,status:true}};
              
            
            }
          if(h!==0)
            {
             
              if(productExists)
                {
                 projHeader =   projectIdList[h-1];
                }
              else
                {
                   projHeader =  projectIdList[h];
                }
               tempHeader.lblShoppingListHdr = {text :projHeader};
               tempHeader.btnExpand = {text:"D",onClick:expandSegment,info:{index:h,status:true}}; // D,true
               tempHeader.flexMyListHeader = {onClick:expandSegment,info:{index:h,status:true}};
            }
        // tempHeader.lblShoppingListHdr.doLayout = callScHdrBack;
         list.push(tempHeader);
         
      

   for(var i=0;i<data.length;i++)
     {
       
       gblWrap_Clip_ID =""; 
        var dataValue=data[i] || {};
        var temp = [];      
        var product = dataValue.product || {};
       	var productType=product.type ||{};
       	var setType=productType.set || false;
       	if(setType==="true" || setType===true){
          continue;
        }
        var imageGroups = product.image_groups || [];   
        var inventories = product.inventories || [];
       	var imageVariant=dataValue.c_default_image||"";
       	var c_master_id=dataValue.c_master_id ||"";
       	if(imageVariant==""){
         var imagesArray =   MVCApp.Toolbox.common.getImages(imageGroups);      
        var defaultImage = imagesArray.largeImages || [];
    if(defaultImage.length >0){
      defaultImage = defaultImage[0] || [];
      defaultImage = defaultImage[0].link || "";
    } else
         {
           defaultImage = "";
         }
          imageVariant=defaultImage;
        }else{
          imageVariant=imageVariant+"";
          //imageVariant=imageVariant.toString();
          kony.print("image Variant is ========"+imageVariant);
          imageVariant=MVCApp.Toolbox.common.replacePipesAndColons(imageVariant);
          //imageVariant=imageVariant.replace(":","%3A");
          kony.print("new image Variant is ========"+imageVariant);
        }
       var itemId = dataValue.id;
       var productID = dataValue.product_id;
      var type = product.type || [];
     var variantFlag = false;
       if(c_master_id!==""){
         variantFlag=false;
       }else{
         variantFlag = type.variant ||false;
       }
       var prodTitle=dataValue.c_default_image_title ||"";
       prodTitle=product.name || "";
       
   temp.id= itemId;
   temp.imgProduct = {src:imageVariant};
   temp.productId = productID;
   temp.lblProductName =prodTitle || "";
       var cStoreInventory  = dataValue.c_store_inventory  || [];
      
         //temp. lblAisle = {text:"Aisle"};
        
              	var aisleDisplayName="";
        var stockLevel = "";
       var aisleNumber = "" ;
           // added for aisle info 
      if(myLocationFlag==="true" )
      {
  
   			
        
               var wrapClipId="";
	 
	 if(cStoreInventory.length !== 0){		
		       
               if( cStoreInventory.hasOwnProperty("wrap_clip_id") ){
                     wrapClipId=cStoreInventory["wrap_clip_id"] || "";
					 aisleDisplayName = wrapClipId;
                            kony.print("wrapClipId:"+wrapClipId);

           
                    }
	 
	 }
	  var cType =  dataValue.c_itemType  ||[];
    var inventory =  product.inventories  || [];
      if(inventory.length > 0){
       inventory = inventory[0] || "";
     }
	var ats="";
	
	var inStoreOrOnlineStatus="";
	var aisleFlag = false;

		var aisleDisplayNum = "";
	   for (var key in inventory) {
      var value = inventory[key] ;
      if (key == "ats" && value !== "" && value !== null ) {
        ats = value;
      }
      if (key == "stock_level" && value !== "" && value !== null) {
        stockLevel = value;
      }
    }
    var locale=kony.store.getItem("Region");
        //if(locale=="US"){
        if (cType.indexOf("Sellable") > -1) {
          inStoreOrOnlineStatus = "Sellable";
        } else if (cType.indexOf("Online") > -1) {
          inStoreOrOnlineStatus = "Online";
        } else if (cType.indexOf("InStore") > -1) {
          inStoreOrOnlineStatus = "InStore";
        }
        else if (cType.indexOf("Dropship") > -1) {
          inStoreOrOnlineStatus = "Dropship";
        }
        var cAltProdutId = dataValue.c_alt_product_id || "";
        if (inStoreOrOnlineStatus != "Sellable" && (cType.indexOf("Online") > -1 || cType.indexOf("Dropship") > -1 )) {
          if(cAltProdutId !== "" && cAltProdutId !== null && cAltProdutId !== "null" && cType.indexOf("Dropship") > -1){
            inStoreOrOnlineStatus = "Sellable"; 
          }
        }
        /*}
        else{
          // this is instore only for canada regions 
          inStoreOrOnlineStatus = "InStore";
        }*/
	var aisleInfoObj="";
	for (var key1 in cStoreInventory) {
				var value2 = cStoreInventory[key1] ;
				if (key1 == "aisle" && value2 !== "" && value2!== null) {
					aisleFlag = true;
				
                  aisleNumber = MVCApp.Toolbox.common.getAisleNumber(value2);
                  	if(aisleDisplayName===""){
                      aisleDisplayName=value2;
                      var aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(aisleDisplayName);
					aisleDisplayName = aisleInfo.name ;
					aisleDisplayNum =  aisleInfo.number;
                    }else{
                      var aisleDisplayNameArr=aisleDisplayName.split("|");
                      var aisleDisplayNameString="";
					   var aisleDisplayNumString="";
                      for(var k=0;k<aisleDisplayNameArr.length;k++){
                        var aisleInfo1=MVCApp.Toolbox.common.getStoreMapElementName(aisleDisplayNameArr[k]);
                        if(k==aisleDisplayNameArr.length-1){
                          //aisleDisplayString=aisleDisplayString+aisleInfo.name.toUpperCase() + " " + aisleInfo.number;
						  aisleDisplayNameString = aisleDisplayNameString+aisleInfo1.name  + " " + aisleDisplayNumString+aisleInfo1.number;
					      aisleDisplayNumString =  "";
                        }else{
                       //   aisleDisplayString=aisleDisplayString+aisleInfo.name.toUpperCase() + " " + aisleInfo.number+",";
						   aisleDisplayNameString = aisleDisplayNameString+aisleInfo1.name + " " + aisleDisplayNumString+aisleInfo1.number +",";
					      aisleDisplayNumString =  "";
                        }
                        
                      }
                      aisleDisplayName=aisleDisplayNameString.trim();
					  aisleDisplayNum=aisleDisplayNumString.trim();
                      
                    }                    
				}
			}
			//lblAisleNo -assign aisle number
	 var stockInfo = MVCApp.Toolbox.common.getStockInfo(inStoreOrOnlineStatus, stockLevel, ats,cStoreInventory);
	 if (!stockInfo.aisleNoStatus && !stockInfo.storeInventoryFlag) {
          temp.flxStockAisleInfo={isVisible:false};
    } else {
	       

          if(stockInfo.storeInventoryStatus == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.limitedQuantities")){
                        stockInfo.storeInventoryStatus =  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.ltdQty");
                      }
                       if(stockInfo.storeInventoryStatus == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.inStock")){
                        stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.smallInStock");
                      }
                      if(stockInfo.storeInventoryStatus == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.outOfStock")){
                        stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.outOfStock");
                      }
                      if(stockInfo.storeInventoryStatus ==  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.notAvailable")){
                        stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.notAvailable");
                      }
					   if(stockInfo.storeInventoryStatus ==  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly")){
                        stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.smallOnlieOnly");
                      }
                      if(stockInfo.storeInventoryStatus ==  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly")){
                                        stockInfo.storeInventoryStatus =  "In Store Only";// MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.inStoreOnly");
                                      }
	 if(stockInfo.aisleNoStatus  && aisleFlag  && stockInfo.storeInventoryFlag){
          if( aisleDisplayName === "" && aisleDisplayNum === "" ){	
		     temp.flxAisleInfo={isVisible:false};
		     temp.lblStockInfo = {"text":stockInfo.storeInventoryStatus,width:"100%",isVisible:stockInfo.storeInventoryFlag};    
			 temp.flxStockAisleInfo = {isVisible:true,width:"15%"};
		  }
		  else
		  {
		     temp.lblAisleNo = {text:aisleDisplayNum};
              if(isBlocked!="true"){
                temp.lblStockInfo = {"text":stockInfo.storeInventoryStatus,isVisible:stockInfo.storeInventoryFlag};
                temp.flxAisleInfo={onClick:getAisleStoreMap,info:{product_name:temp.lblProductName,aisleNum:aisleNumber,wrap_clip_id:wrapClipId,"productId":productID} };
				temp.lblAisle = {"text":aisleDisplayName};    
              }else{
                temp.flxAisleInfo={isVisible:false};
                temp.flxStockAisleInfo = {isVisible:true,width:"15%"};
                temp.lblStockInfo = {"text":stockInfo.storeInventoryStatus,width:"100%",isVisible:stockInfo.storeInventoryFlag};    
              } 
		  }
        
    }
      else if((!stockInfo.aisleNoStatus  || !aisleFlag ) && stockInfo.storeInventoryFlag)
        {
             temp.flxAisleInfo={isVisible:false};
		     temp.lblStockInfo = {"text":stockInfo.storeInventoryStatus,width:"100%"};    
			 temp.flxStockAisleInfo = {isVisible:true,width:"15%"};
        }
	 
  }
      } // end of mylocation flag check
	  else
      {
        temp.flxStockAisleInfo={isVisible:false};
      }      
      // end
      
       temp.flxShoppingListContainer = {isVisible:true};
        var cRelatedProj = dataValue.c_relatedProjects || [];
       
            
        if(h===0 && productExists )
            {
                if(cRelatedProj.length === 0)
                  {
                      
                     temp.projectId = "";
                    temp.btnDelete ={text:deleteBtnText,onClick:onClickDelete,info:{id: itemId,projectId:temp.projectId}};
                      temp.flxShoppingListInner ={onClick:getSelectedItem,info:{id: productID,projectId:temp.projectId,variant:variantFlag,master:c_master_id}}; 
                      subList.push(temp);
                    
                  }
                 
            }
       else if(h===0)
         {
           if(cRelatedProj.length > 0)
                  
           {
                for( var q=0;q<cRelatedProj.length;q++)
                  {
						var projIdInfo= cRelatedProj[q];
                    if(projHeader==  projText.concat(" ", projIdInfo.name))
                      {
                        temp.projectId = projIdInfo.id;
                         temp.btnDelete ={text:deleteBtnText,onClick:onClickDelete,info:{id: itemId,projectId:temp.projectId}};
                          temp.flxShoppingListInner ={onClick:getSelectedItem,info:{id: productID,projectId:temp.projectId,variant:variantFlag}};
                        subList.push(temp);
                      }
                  }
           }
         }
       if(h!==0)
         {
              if(cRelatedProj.length > 0)
                  
           {
                for( var r=0;r<cRelatedProj.length;r++)
                  {
						var projIdInfo1= cRelatedProj[r];
                    if(projHeader== projText.concat(" ", projIdInfo1.name))
                      {
                        temp.projectId = projIdInfo1.id;
                         temp.btnDelete ={text:deleteBtnText,onClick:onClickDelete,info:{id: itemId,projectId:temp.projectId}}; 
                          temp.flxShoppingListInner ={onClick:getSelectedItem,info:{id: productID,projectId:temp.projectId,variant:variantFlag}};
                        subList.push(temp);
                      }
                   
                  }
           }
         }
         
        // segShoppingListData.push(temp);   
         
     }
         
  list.push(subList);   
      
     
  segShoppingListData.push(list);  
       }
  deletedFromProjects=false;
  kony.print(" data object : "+JSON.stringify(segShoppingListData));
    }
     return segShoppingListData;
  
   
}
  
  function onClickDelete(eventObj){
 
    var info = eventObj.info || {};
    var itemId = info.id || "";
    var projectId =info.projectId || "";
 	if(projectId!=""){
      deletedFromProjects=true;
    }
    projectIdDeleted=projectId;
   MVCApp.getShoppingListController().removeFromProductList(itemId,projectId);
  
   
  }
   function getSelectedItem(eventObj)
  {
    
    if( gswipeFlag)
      {
         gswipeFlag=false;
      }
    else
      {
        
         kony.print("############getSelectedItem################");
             var info = eventObj.info || {};
    var productId = info.id || "";
    var defaultVariantId="";//shopping List variant ID
    var produtMaster=info.master || "";
        if(produtMaster!==""){
          defaultVariantId=productId;
          productId=produtMaster;
        }
     var projectId =info.projectId || "";
        var variantFlag = info.variant || false;
  //    var selectedItem=frmShoppingList.segShoppingList.selectedRowItems[0] || {};
   
     var headerText = [];
   
      headerText[0] = "";
      headerText[1] = "";
     MVCApp.Toolbox.common.clearPdpGlobalVariables();
    
    if(projectId==="")
      {
         MVCApp.getPDPController().setQueryString("");
         MVCApp.getPDPController().setFromBarcode(false);
         MVCApp.getPDPController().setEntryType("mylist");
    	 MVCApp.getPDPController().load(productId,variantFlag,false,headerText,"frmShoppingList",defaultVariantId,true) ;
      }
    else
      {
          MVCApp.getPJDPController().setQueryString("");
          MVCApp.getPJDPController().setEntryType("mylist");
          MVCApp.getPJDPController().load(projectId, false,"frmShoppingList");
      }
      }
  
       
  }
  
  function expandSegment(eventObj){
    try{
      var info = eventObj.info || {};
      var i=eventObj.info.index;
      var visibility=eventObj.info.status;
      var segmentData=frmShoppingList.segShoppingList.data;
      var currData=segmentData[i];
      var data1 = getDataForShoppingList();
      if(currData[1].length === undefined || currData[1].length ===0){
        currData[0].btnExpand={text:"U",onClick:expandSegment,info:{index:i,status:false}};
        currData[0].flexMyListHeader = {onClick:expandSegment,info:{index:i,status:false}};
        frmShoppingList.segShoppingList.removeSectionAt(i);
        frmShoppingList.segShoppingList.addSectionAt([[currData[0],data1[i][1]]],i);
      }else{
        currData[0].btnExpand={text:"D",onClick:expandSegment,info:{index:i,status:true}};
        currData[0].flexMyListHeader = {onClick:expandSegment,info:{index:i,status:true}};
        frmShoppingList.segShoppingList.removeSectionAt(i);
        frmShoppingList.segShoppingList.addSectionAt([[currData[0],[]]],i);
      }
      if(i>0){
        frmShoppingList.segShoppingList.selectedRowIndex=[i,0];
      }
    } //end of try
    catch(e){
      alert(e);
      if(e){
        TealiumManager.trackEvent("Expand Segment Exception in My List Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    }
  }
  
  
   function getAisleStoreMap(eventObj)
  {

    var wishListData = getDataForShoppingList();
	var selectedRow = frmShoppingList.segShoppingList.selectedIndex;
    
    var totalRec = 0;
	var temp = [];
    
    var firstIndex = 0;
    var lastIndex = 0;
    
    var flxAisleInfo;
    var category;
    var hasAisle;
    var entered = false;
    
    for(j=0;j<wishListData.length;j++){
          for(var k=0;k<wishListData[j][1].length;k++){
            temp.push(wishListData[j][1][k]);
          }
         
		  if(j<selectedRow[0]){
			totalRec+= wishListData[j][1].length;
		  }
		}
    
    for(var i=0 ; i < temp.length ; i++){
      
      	   flxAisleInfo = temp[i].flxAisleInfo || "";
           category = flxAisleInfo.info || "";
           hasAisle = category === "" ? false : true;
      
      	   if(hasAisle){
             if(!entered){
               firstIndex = i;
               entered = !entered;
             }
             lastIndex = i; 
           }
    }
    
    totalRec = totalRec + selectedRow[1];
	

    
    var params = {};

    gblWrap_Clip_ID = eventObj.info.wrap_clip_id||"";
    params.aisleNo =eventObj.info.aisleNum;
    params.productName = eventObj.info.product_name;
    params.calloutFlag = true;
    params.currentAisle = totalRec;
    params.isFromMyList = true;
    MVCApp.sendMetricReport(frmShoppingList, [{"storemap":"click"}]);
    MVCApp.getStoreMapController().load(params);
    MVCApp.getStoreMapController().setFirstLastIndex({"firstIndex":firstIndex,"lastIndex":lastIndex});
    MVCApp.getStoreMapController().enableButtons(totalRec);
	MVCApp.getStoreMapController().setCategoryData(temp);
    TealiumManager.trackEvent("Wayfinding click From MyList", {conversion_category:"wayfinding",conversion_id:"My List",conversion_action:2});    
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    var storeId= MVCApp.Toolbox.common.getStoreID();
    cust360Obj.productId=eventObj.info.productId ||"";
    cust360Obj.store_id=storeId;
    cust360Obj.entry_type="mylist";
    MVCApp.Customer360.sendInteractionEvent("ProductLocationInStore", cust360Obj);
  }
 
  return {
    getDataForShoppingList: getDataForShoppingList,
    expandSegment:expandSegment
  };
  
});