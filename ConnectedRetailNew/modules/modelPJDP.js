/**
 * PUBLIC
 * This is the model for formPDP form
 */
var MVCApp = MVCApp || {};
MVCApp.PJDPModel = (function(){
var serviceType = MVCApp.serviceType.DemandwareService;
  var operationId = "getProjectByID";//MVCApp.serviceEndPoints.getProductByID;
  function loadPJDP(callback,requestParams,formName){
    
   MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
         MVCApp.Toolbox.common.indexArrayPushResults(results,"","","frmPJDP",false,formName);
          var pjdpDataObj = new MVCApp.data.PJDP(results);   
          callback(pjdpDataObj);
      }   
    },function(error){
     
     MVCApp.Toolbox.common.dismissLoadingIndicator();
      kony.print("error response in modelPJDP "+JSON.stringify(error));
     alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
    }
                   );
    
  }
 	return{
      loadPJDP:loadPJDP
    };
});
