
/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.Reviews = (function(json,req,nam,flag){

  var typeOf = "Reviews";
  var _data = json || {};	
  var _dataReq = req;
  var _name = nam;
  var _flag = flag;

  /**
	 * PUBLIC - setter for the model
	 * Set data from the JSON retrieved by the model
	 */
  
  function getValue(str){
    if(str!="null")
      return str;
    else{str = " ";return str;}
  }
  
  function ratingBars(){
       var rvwData = _data.ReviewStatistics || [];
       if(rvwData.length > 0){
         rvwData = rvwData[0];
       }
  	   var totRevwCount = _data.TotalResults || [];
       var ratingDistribution = rvwData.RatingDistribution || [];
       var flag1=0; 
       var flag2=0;
       var flag3=0;
       var flag4=0;
       var flag5=0;
       var displayRatngList = {};
      displayRatngList.totRevwCount =totRevwCount;
     for(var i=0; i< ratingDistribution.length; i++){              		
        var ratTemp =  ratingDistribution[i];
       	var ratingValue = ratTemp.RatingValue || "";
       	if(ratingValue == 1){
          flag1 = ratTemp.Count|| "";
        }else if(ratingValue == 2){
          flag2 = ratTemp.Count|| "";
        }else if(ratingValue == 3){
          flag3 = ratTemp.Count|| "";
        }else if(ratingValue == 4){
          flag4 = ratTemp.Count|| "";
        }else if(ratingValue == 5){
          flag5 = ratTemp.Count|| "";
        }
        }
     displayRatngList.flag1 = flag1;
    displayRatngList.flag2 = flag2;
    displayRatngList.flag3 = flag3;
    displayRatngList.flag4 = flag4;
    displayRatngList.flag5 = flag5;
     return displayRatngList;
  }
  
  function getReviewsData(){
    try{
     var originalData = _data.Results || [];
     var displayReviewsList = [];
     var totReviewCount = _data.TotalResults || [];
     var avgOverallRating = _data.AverageOverallRating || []; 
     displayReviewsList.avgOverallRating = avgOverallRating;
     displayReviewsList.totReviewCount = totReviewCount;
     if(originalData.length>0){
     var name = "";
     var loc  = "";
     displayReviewsList.name = _name;
     displayReviewsList.flag = _flag; 
      
      if(avgOverallRating >="1" && avgOverallRating<"2"){
          	displayReviewsList.skin1 ="sknLblIconStarLrgActive";			
			displayReviewsList.skin2 ="sknLblIconStarLrg";
			displayReviewsList.skin3 ="sknLblIconStarLrg";
			displayReviewsList.skin4 ="sknLblIconStarLrg";
			displayReviewsList.skin5 ="sknLblIconStarLrg";
        }
		if(avgOverallRating >="2" && avgOverallRating<"3"){
			displayReviewsList.skin1 ="sknLblIconStarLrgActive";
			displayReviewsList.skin2 ="sknLblIconStarLrgActive";
			displayReviewsList.skin3 ="sknLblIconStarLrg";
			displayReviewsList.skin4 ="sknLblIconStarLrg";
			displayReviewsList.skin5 ="sknLblIconStarLrg";
        }
        if(avgOverallRating >="3" && avgOverallRating<"4"){
          	displayReviewsList.skin1 ="sknLblIconStarLrgActive";
			displayReviewsList.skin2 ="sknLblIconStarLrgActive";
			displayReviewsList.skin3 ="sknLblIconStarLrgActive";
			displayReviewsList.skin4 ="sknLblIconStarLrg";
			displayReviewsList.skin5 ="sknLblIconStarLrg";
        }
		if(avgOverallRating >="4" && avgOverallRating<"5"){
			displayReviewsList.skin1 ="sknLblIconStarLrgActive";
			displayReviewsList.skin2 ="sknLblIconStarLrgActive";
			displayReviewsList.skin3 ="sknLblIconStarLrgActive";
			displayReviewsList.skin4 ="sknLblIconStarLrgActive";
			displayReviewsList.skin5 ="sknLblIconStarLrg";
        }
        if(avgOverallRating =="5"){
			displayReviewsList.skin1 ="sknLblIconStarLrgActive";
			displayReviewsList.skin2 ="sknLblIconStarLrgActive";
			displayReviewsList.skin3 ="sknLblIconStarLrgActive";
			displayReviewsList.skin4 ="sknLblIconStarLrgActive";
			displayReviewsList.skin5 ="sknLblIconStarLrgActive";
        }
      
      
      var rvwList = MVCApp.Toolbox.common.isArray(originalData) ? originalData :[originalData];
      
      
      for(var i=0; i< rvwList.length; i++){
                		
        var temp1= originalData[i];
        name = getValue(temp1.UserNickname);
        loc = getValue(temp1.UserLocation);
        temp1.nameNLoc = "<a href=''>"+name+"</a> "+loc+" ";
        temp1.ReviewText = getValue(temp1.ReviewText);
        temp1.Title = getValue(temp1.Title);
        if(temp1.SubmissionTime !="null"){
          var dateRvw = new Date(temp1.SubmissionTime);
		  var dd = dateRvw.getDate(); 
          var mm = dateRvw.getMonth()+1;
		  var yy = dateRvw.getFullYear().toString().substr(2,2);
		  if(dd<10){dd='0'+dd;} 
		  if(mm<10){mm='0'+mm;}		    
		  temp1.SubmissionTime = {text: mm+"/"+dd+"/"+yy, skin: "sknLblGothamBold24pxBlack"};          	
        }else temp1.SubmissionTime = {text: " ", skin: "sknLblGothamBold24pxBlack"};
        
		if(temp1.Rating =="1"){
          	temp1.Star1	= {text: "Y", skin: "sknLblIconStarLrgActive"};
       		temp1.Star2 = {text: "Y", skin: "sknLblIconStarLrg"};
            temp1.Star3 = {text: "Y", skin: "sknLblIconStarLrg"};
            temp1.Star4 = {text: "Y", skin: "sknLblIconStarLrg"};
            temp1.Star5 = {text: "Y", skin: "sknLblIconStarLrg"};
        }
		if(temp1.Rating =="2"){
          	temp1.Star1	= {text: "Y", skin: "sknLblIconStarLrgActive"};
       		temp1.Star2 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star3 = {text: "Y", skin: "sknLblIconStarLrg"};
            temp1.Star4 = {text: "Y", skin: "sknLblIconStarLrg"};
            temp1.Star5 = {text: "Y", skin: "sknLblIconStarLrg"};
        }
        if(temp1.Rating =="3"){
          	temp1.Star1	= {text: "Y", skin: "sknLblIconStarLrgActive"};
       		temp1.Star2 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star3 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star4 = {text: "Y", skin: "sknLblIconStarLrg"};
            temp1.Star5 = {text: "Y", skin: "sknLblIconStarLrg"};
        }
		if(temp1.Rating =="4"){
          	temp1.Star1	= {text: "Y", skin: "sknLblIconStarLrgActive"};
       		temp1.Star2 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star3 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star4 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star5 = {text: "Y", skin: "sknLblIconStarLrg"};
        }
        if(temp1.Rating =="5"){
          	temp1.Star1	= {text: "Y", skin: "sknLblIconStarLrgActive"};
       		temp1.Star2 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star3 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star4 = {text: "Y", skin: "sknLblIconStarLrgActive"};
            temp1.Star5 = {text: "Y", skin: "sknLblIconStarLrgActive"};
        }
        displayReviewsList.push(temp1);  
      }
      displayReviewsList.datReq = _dataReq;
      return displayReviewsList;
      }else{
        displayReviewsList.name = _name;
        ++_flag;
        displayReviewsList.flag = _flag; 
        return displayReviewsList;}
    }catch(e){
      kony.print("Exception in getReviewsData"+JSON.stringify(e));
      if(e){
        TealiumManager.trackEvent("Get Reviews Data Exception", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
      }
    } 
  }
  
  /**
   * @function
   *
   */
  function afterSubmitData(){
    alert("submitted!");
    MVCApp.getReviewsController().load(productId,frmPDP.lblName1.text,"");
    show();
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  
  //Here we expose the public variables and functions
  return {
    typeOf: typeOf,
    getReviewsData:getReviewsData,
    ratingBars:ratingBars,
    afterSubmitData : afterSubmitData
  };
});


