/**
 * PUBLIC
 * This is the model for formPDP form
 */
var MVCApp = MVCApp || {};
MVCApp.PDPModel = (function(){
  var serviceType = MVCApp.serviceType.getProductDetailsService;//MVCApp.serviceType.DemandwareService;
  var operationId = MVCApp.serviceEndPoints.getProductDetailsOperation;
  var getAllImages = MVCApp.serviceConstants.getProductImagesByID;
  function loadPDP(callback,requestParams,varientFlag,varientChangeFlag,barCodeFlag,formName){
    /* if(varientFlag){
      operationId = MVCApp.serviceEndPoints.getProductByID;
    }else{
      operationId = MVCApp.serviceEndPoints.getProductByID;
    }*/

    kony.print("modelPDP.js");
    MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print(" pdp Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){

        var pdpDataObj = new MVCApp.data.PDP(results);  
        if(!varientChangeFlag){
          MVCApp.Toolbox.common.indexArrayPushResults(results,varientFlag,varientChangeFlag,"frmPDP",barCodeFlag,formName);
        }
        callback(pdpDataObj,varientFlag,varientChangeFlag);
      }   
    },function(error,timeoutFlag){
      kony.print("Response is "+JSON.stringify(error));
      MVCApp.Toolbox.common.indexArrayPushResults("noResults",varientFlag,varientChangeFlag,"frmPDP",barCodeFlag,formName);
      if(timeoutFlag){
        //timeout Error message is displaying
      }else{
      alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
      }
      
    },true);

  }

  function loadAllImages(callback,requestParams,productId){

    kony.print("loadAllImages.js");
    MakeServiceCall(MVCApp.serviceType.DemandwareService,getAllImages,requestParams,
                    function(results)
                    {
      kony.print("load all images Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
        MVCApp.Toolbox.common.imagesPush(results,productId);
        callback(results);
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
    }
                   );

  }

  function getRecommendedProducts(serviceType,callback,requestParams,varientChangeFlag)
  {
    var operationId = MVCApp.serviceType.GetRecommendations;
	var locale=kony.store.getItem("Region");
    if(locale=="US"){
      MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print(" recommended products Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
        MVCApp.Toolbox.common.recommendedProductPush(results,varientChangeFlag);

        callback(results);
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
      MVCApp.Toolbox.common.recommendedProductPush("noResults",varientChangeFlag);
      frmPDP.lblRecomProducts.isVisible = false;
      frmPDP.segProductsList.removeAll();
      frmPDP.segProductsList.isVisible = false;
      frmPDP.ShowMoreProducts.isVisible = false;
    }
                   );
    }else{
      callback([]);
    }
  }

  return{
    loadPDP:loadPDP,
    loadAllImages:loadAllImages,

    getRecommendedProducts:getRecommendedProducts
  };
});
