/**
 * PUBLIC
 * This is the model for formImageSearch form
 */
var MVCApp = MVCApp || {};                        
MVCApp.ImageSearchModel = (function(){
 var serviceType = "";
  var operationId = "";
  function loadData(callback,requestParams){
    
    kony.print("modelImageSearch.js");
   /* MVCApp.makeServiceCall( serviceType,operationId,requestParams,
                           function(results)
                           {
      kony.print("Response is "+JSON.stringify(results)); 
      callback(results,requestParams);
      
    },function(error){
    kony.print("Response is "+JSON.stringify(error));
    }
                          );*/
    callback("");
  }
  
  function fetchPLPDataForImageSearch(pRequestParams,pCallback){
    kony.print("LOG:fetchPLPDataForImageSearch-START");
    MakeServiceCall(MVCApp.serviceType.imagesearch,MVCApp.serviceEndPoints.imageSearchProduct,pRequestParams,
      function(pResults){
        kony.print("LOG:fetchPLPDataForImageSearch-pResults:"+JSON.stringify(pResults));
      	MVCApp.getProductsListController().setEntryType("imageSearch");
      	pCallback(pResults);
      },function(pError){
        kony.print("LOG:fetchPLPDataForImageSearch-pError:"+JSON.stringify(pError));
      	imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.onErrorReceipt"));
      	imageSearchSuccessFlag = false;
      	imageSearchWaitFlag = false;
      }
    );
  } 
  
  function fetchPJLPDataForImageSearch(pRequestParams,pCallback){
    kony.print("LOG:fetchPJLPDataForImageSearch-START");
    MakeServiceCall(MVCApp.serviceType.imagesearch,MVCApp.serviceEndPoints.imageSearchProject,pRequestParams,
      function(pResults){
        kony.print("LOG:fetchPJLPDataForImageSearch-pResults:"+JSON.stringify(pResults)); 
      	pCallback(pResults);
      },function(pError){
        kony.print("LOG:fetchPJLPDataForImageSearch-pError:"+JSON.stringify(pError));
      	MVCApp.Toolbox.common.dismissLoadingIndicator();
      }
    );
  }
  
  return{
    loadData:loadData,
    fetchPLPDataForImageSearch:fetchPLPDataForImageSearch,
    fetchPJLPDataForImageSearch:fetchPJLPDataForImageSearch
  };
});
