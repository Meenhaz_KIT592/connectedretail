//Type your code here
var authAlert;
var count;
var fromCancelAuth = false;
var fromTouchId = false;
var isTouchID=true;
function isTouchSupported(){
  var status = 5008;
  //#ifdef iphone
  	status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
  //#else
  	var Build = java.import('android.os.Build');
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
      var KonyMain = java.import('com.konylabs.android.KonyMain');
      var appContext = KonyMain.getAppContext();
      var Context = java.import('android.content.Context');
      var fingerPrintManager = appContext.getSystemService(Context.FINGERPRINT_SERVICE);
      if(fingerPrintManager){
        if(!fingerPrintManager.isHardwareDetected()){
          // Device doesn't support fingerprint authentication
          status = 5006;
        }else if(!fingerPrintManager.hasEnrolledFingerprints()){
          // User hasn't enrolled any fingerprints to authenticate with
          status = 5007;
        }else{ 
          // Everything is ready for fingerprint authentication
          status = 5000;
        }
      }
    }
  //#endif
  if(status == 5000){
    kony.store.setItem("touchIdSupport", true);
  }else{
	kony.store.setItem("touchIdSupport", false);
  }
}
var conf_alert = null;
function statusCallback(status,message){
  kony.ui.dismissAlert(authAlert);
  fromTouchId = true;
  //alert("status--"+status);
  if(status == 5000){
     var reqParams = {};
     reqParams.user = kony.store.getItem("storeFirstUserForTouchId");
     reqParams.password = kony.store.getItem("storeFirstPwdForTouchId");
     kony.print("reqParams---"+JSON.stringify(reqParams));
     MVCApp.getSignInController().validateLogin(reqParams);
     kony.store.setItem("switch", true);
     frmSettings.switchTouch.src = "on.png";
  }else{
    if(fromCancelAuth != true){
       count++;
         var messg;
        if(count == 1 || count ==2){
          messg = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.secondTry");
        }else if(count == 3){
          messg = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.lastTry");
          cancelAuth();
        }
        if(undefined != messg && null != messg && messg.length>0){
		  if(conf_alert != null && conf_alert != undefined){
			kony.ui.dismissAlert(conf_alert);
			conf_alert = null;
		  }
		  conf_alert = kony.ui.Alert({message: messg, 
						   alertType: constants.ALERT_TYPE_INFO, 
						   yesLabel:MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),
						   alertHandler :  function(response){
					   }}, {});
		}
    }
    
  }
}

function iOSStatusCallback(status, message) {
  fromTouchId = true;
  if(status == 5000){
     var reqParams = {};
     reqParams.user = kony.store.getItem("storeFirstUserForTouchId");
     reqParams.password = kony.store.getItem("storeFirstPwdForTouchId");
     kony.print("reqParams---"+JSON.stringify(reqParams));
     MVCApp.getSignInController().validateLogin(reqParams);
     kony.store.setItem("switch", true);
     frmSettings.switchTouch.src = "on.png";
  } else if (status == 5001) {
    kony.ui.Alert({message: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.lastTry"), 
                   alertTitle: "",
                         alertType: constants.ALERT_TYPE_INFO, 
                         yesLabel:MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),
                         alertHandler :  function(response){
                     }}, {});
  } else if (status == 5002 || 5003) {
    // User pressed "Cancel" or "Password"
    frmSignIn.txtEmail.text ="";
    frmSignIn.txtPassword.text ="";
    toggleRewards = false;
  }
}

function authenticate(){
  if(kony.store.getItem("storeFirstUserForTouchId") != null && kony.application.getCurrentForm() == frmSignIn && frmSignIn.flxOverlay.isVisible == false){
    count = 0;
    fromCancelAuth = false;
    var configMap = {"promptMessage" : MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.logInAs")};
    var alertTitleMessage = "";
    //#ifdef iphone
    kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, iOSStatusCallback, configMap);
    if(redirectToSettings.supportFaceID()){
      isTouchID=false;
      alertTitleMessage = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.touchOrFaceIdforMichaels");
    }else{
      isTouchID=true;
      alertTitleMessage = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.touchIdforMichaels");
    }
    //#endif
    //#ifdef android
    kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, statusCallback, configMap);
    var devName=kony.os.deviceInfo().model;
   alertTitleMessage = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.touchIdforMichaels");
    authAlert = kony.ui.Alert({message: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.logInAs"),//MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.logInAs")+" "+kony.store.getItem("storeFirstUserForTouchId")+", PLACE YOUR FINGER ON HOME BUTTON", 
                   alertTitle : alertTitleMessage,
                   alertType: constants.ALERT_TYPE_INFO,
                   yesLabel: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel"), 
                   //#ifdef android
                   "noLabel": "",
                   //#endif
                   alertHandler :  function(response1){
                     if(count != 1 && count != 2){
                       cancelAuth();
                     }
                   }            
                   }, {});
    //#endif
  }
}


function cancelAuth(){
  kony.localAuthentication.cancelAuthentication();
  fromCancelAuth = true;
}