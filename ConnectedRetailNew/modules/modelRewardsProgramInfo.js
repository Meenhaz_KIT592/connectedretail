/**
 * PUBLIC
 * This is the model for formRewardsProgramInfo form
 */
var MVCApp = MVCApp || {};
MVCApp.RewardsProgramInfoModel = (function(){
 var serviceType = MVCApp.serviceType.DemandwareService;
  var operationId = MVCApp.serviceEndPoints.rewardslanding;
  function loadData(callback,requestParams){
    kony.print("modelRewardsProgramInfo.js");
    MakeServiceCall( serviceType,operationId,requestParams,
                           function(results)
                           {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
       callback(results); 
      }
    },function(error){
    kony.print("Response is "+JSON.stringify(error));
    }
                          );
  }
 	return{
      loadData:loadData
    };
});
