/**
 * PUBLIC
 * This is the controller for the VoiceSearchVideos form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.VoiceSearchVideosController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    //model = new MVCApp.VoiceSearchVideosModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.VoiceSearchVideosView();
  	   
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
    function load(urlToBeOpened){
      	init();
        if(voiceSearchFlag && tapToCancelFlag){
          tapToCancelFlag = false;
          MVCApp.Toolbox.common.dismissLoadingIndicator();
          showMicPanel(voiceSearchForm);
        }else{
		  view.updateScreen(urlToBeOpened);
        }
    }
  
    return {
 		init : init,
    	load : load,
    };
  
  });