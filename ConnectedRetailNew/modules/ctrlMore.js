
/**
 * PUBLIC
 * This is the controller for the More form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.MoreController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.MoreModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.MoreView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function load(){
      if(MVCApp.Toolbox.common.getRegion()==="US"){
        //frmMore.segMenuList.rowSkin="sknLblIconMoreMenuBlack";          
      //frmMore.segMenuList.rowTemplate="tempMenuListUS";
        var data= [
           {
            "lblIconMenuList": {
                "text": "P","skin":"sknLblIconMoreMenu66666629px"
            },
            "lblMenuListItem": {
                "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.Accounts"),
                 "i18nkey": "i18n.phone.frmMore.Accounts",
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px"
            }
          },
        {
            "lblIconMenuList": {
                "text": "E","skin":"sknLblIconMoreMenu66666629px"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.masterFooter.lblEvents",
                "text":  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblEvents")
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px"
            }
        },
        {
            "lblIconMenuList": {
                "text": "T","skin":"sknLblIconMoreMenu66666629px"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.frmSettings.smallSettings",
                "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSettings.smallSettings"),
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px"
            }
        },
        {
            "lblIconMenuList": {
                "text": "l","skin":"sknLblIconMoreMenu66666629px"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.frmHelp.smallHelp",
                "text":  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.smallHelp")
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px"
            }
        }
//         },
//         {
//             "lblIconMenuList": {
//                 "text": "I","skin":"sknLblIconMoreMenu66666629px"
//             },
//             "lblMenuListItem": {
//                 "i18nkey": "i18n.phone.lblMyLists",
//                 "text":  MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.lblMyLists")
//             },
//             "lblIconMenuArrow": {
//                 "text": "K","skin":"sknLblIconMoreMenu66666616px"
//             }
//         }
    ];
          frmMore.segMenuList.setData(data);
          frmMore.flxScrlSegment.top = "20dp";
          frmMore.btnSignUp.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.btnCreateAccount");
          frmMore.btnSignUp.skin = "sknBtnMontserratSemiBold13pxBlueBorder";
          frmMore.btnSignUp.focusSkin = "sknBtnMontserratSemiBold13pxBlueBorder";
          //frmMore.btnSignUp.width="150dp";
       f=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.lblWelcome");
           //frmMore.btnSignIn.left="2%";
			 frmMore.btnSignIn.left="2%";
            frmMore.btnSignIn.width = "47%";
           // frmMore.btnSignIn.right = "51%";
            //frmMore.btnSignIn.centerY = "50%";
            frmMore.btnSignUp.left = "51%";
            frmMore.btnSignUp.width = "47%";
            //frmMore.btnSignUp.centerY = "50%";
			frmMore.flxStoreInfo.isVisible=false;
			frmMore.flxStoreMap.isVisible=false;
			frmMore.flxRewardMainContainer.isVisible=false;
			frmMore.flxRewardMainContainer.isVisible=false;
			frmMore.flexRewardsProfile.isVisible=false;
			frmMore.lblStoreRewardsGap.isVisible=false;
             frmMore.lblSeparator1.isVisible=false;
          
    
      }
      else
        {
              
           var data= [
        
        {
            "lblIconMenuList": {
                "text": "E","skin":"sknLblIconMoreMenu"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.masterFooter.lblEvents",
                "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblEvents")
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px","isVisible":false
            }
        },
        {
            "lblIconMenuList": {
                "text": "T","skin":"sknLblIconMoreMenu"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.frmSettings.smallSettings",
                "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSettings.smallSettings")
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px","isVisible":false
            }
        },
        {
            "lblIconMenuList": {
                "text": "l","skin":"sknLblIconMoreMenu"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.frmHelp.smallHelp",
                "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.smallHelp")
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px","isVisible":false
            }
        },
        {
            "lblIconMenuList": {
                "text": "I","skin":"sknLblIconMoreMenu"
            },
            "lblMenuListItem": {
                "i18nkey": "i18n.phone.lblMyLists",
                "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.lblMyLists")
            },
            "lblIconMenuArrow": {
                "text": "K","skin":"sknLblIconMoreMenu66666616px","isVisible":false
            }
        }
    ];
          frmMore.segMenuList.setData(data);
            frmMore.flxScrlSegment.top = "0dp";
            frmMore.btnSignUp.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.btnSignUp");
            frmMore.btnSignUp.skin = "sknBtnRed24pxMed";
            frmMore.btnSignUp.focusSkin = "sknBtnRed24pxMedFoc";
            frmMore.btnSignIn.left="2%";
            frmMore.btnSignIn.width = "47%";
            frmMore.btnSignUp.left = "51%";
          frmMore.btnSignUp.width = "47%";
          frmMore.flxStoreInfo.isVisible=true;
          if(MVCApp.Toolbox.common.getRegion()!=="US"){
            frmMore.flxStoreMap.setVisibility(true);
          }
          frmMore.flxRewardMainContainer.isVisible=true;
			frmMore.flexRewardsProfile.isVisible=true;
			frmMore.lblStoreRewardsGap.isVisible=true;
            frmMore.lblSeparator1.isVisible=true;
            frmMore.lblWelcome.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.lblWelcomeBack");
        }
      	kony.print("More Controller.load");
      	init();
        var requestParams = {};
        model.loadData(view.updateScreen,requestParams);
    }
     function loadMore(){
       init();
        var reqParams = {};
        model.loadData(view.showPreviousScreen,reqParams);
     }  
  function show()
  {
    view.show();
  }
  	/**
  	 * @function
  	 *
  	 */
  	function rewards(){
      init();
      kony.print("rewards----");
      var inputParams = {};
      var time = Math.floor(Date.now() / 1000);
      
      if(MVCApp.Toolbox.Service.getMyLocationFlag()==="true" || (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled)){
        var storeString=kony.store.getItem("myLocationDetails") ||"";
		var userObject = kony.store.getItem("userObj");
        if(storeString!=="" || (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled)){
		  if(userObject != "" && userObject !=null && userObject !=undefined){
             if(userObject.hasOwnProperty("email")){
                inputParams.email= userObject.email || "";
             }
			  if(userObject.hasOwnProperty("c_loyaltyMemberID")){
				  inputParams.loyaltyID = userObject.c_loyaltyMemberID+"";
				  var serviceType = "couponservice";
				  var operation = "couponsrewards";
				  try {
                    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
                      inputParams.storeNo =gblInStoreModeDetails.storeID;
                    }
                    else if(storeString!=""){
                      var storeObj=JSON.parse(storeString);
                      inputParams.storeNo =storeObj.clientkey;
                    } 
                    inputParams.currentTimeValue = time+"";
                    MVCApp.Toolbox.common.showLoadingIndicator("");
                    kony.print("inputParams---"+JSON.stringify(inputParams));
                    gblIsRewardsBeingShown = true;
                    MVCApp.HomeModel().getCoupons(serviceType,operation, MVCApp.CouponsView().coupons,inputParams); 
                  } catch(e) {
                    MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlMore.js on rewards for storeString:" + JSON.stringify(e)}]);
                    if(e){
                      TealiumManager.trackEvent("POI Parse Exception in More Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
                    }
                  }
			  }else{
				  MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.signUp"), "");
			  }
		  }else{
			   MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.signIn"), "");
		  }
        }else{
          var whenStoreNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"rewards"};
          whenStoreNotSet = JSON.stringify(whenStoreNotSet);
          kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
          MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreCoupons") ,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
          inputParams.storeNo ="";
        }
      }else{
        var whenStoreIsNotSet = {"doesLoadCouponsWhenStoreNotSet":true,"fromWhichScreen":"rewards"};
        whenStoreIsNotSet = JSON.stringify(whenStoreIsNotSet);
        kony.store.setItem("whenStoreNotSet", whenStoreIsNotSet);
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.selectStore") ,"",constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
        inputParams.storeNo ="";
      }
    }
  
  	function addLoyalityToWallet(){
      model.androidAddLoyalityToWallet();
    }
  
  	function addLoyalityToWalletIPhone(callback){
      model.iPhoneAddLoyalityToWallet(callback);
    }
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init: init,
    	load: load,
        loadMore: loadMore,
        rewards : rewards,
        show : show,
        addLoyalityToWallet:addLoyalityToWallet,
      	addLoyalityToWalletIPhone:addLoyalityToWalletIPhone
    };
});

