/**
 * PUBLIC
 * This is the view for the join Rewards form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};

MVCApp.JoinRewardsView = (function(){
 var prevFormName = ""; 
  var isAccCreated = false;
  /**
	 * PUBLIC
	 * Open the More form
	 */
  
  function show(){
  frmRewardsJoin.txtPhoneNo.text = "";
  frmRewardsJoin.imgCheck.src ="checkbox_off.png";
  //#ifdef android
  frmRewardsJoin.btnClearPhoneNo.setVisibility(false);
   //#endif
  var screenWidth = kony.os.deviceInfo().screenWidth;
  if(screenWidth == 414 || screenWidth == 375){
     frmRewardsJoin.rtTermsandConditions.padding=[0,0.5,0,0];
   }
   frmRewardsJoin.flxOverlay.setVisibility(false);
  frmRewardsJoin.txtEmail.setEnabled(false);
  frmRewardsJoin.txtBxHide.top = "0dp";  
  frmRewardsJoin.txtBxHide.height = "0dp";    
  frmRewardsJoin.txtBxHide.setFocus(true);  
  frmRewardsJoin.txtPhoneNo.setFocus(false);  
  frmRewardsJoin.show();
 } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  
  
  function savePreviousScreenForm(){
    MVCApp.Toolbox.common.destroyStoreLoc();
    var prevForm = kony.application.getPreviousForm().id;
    if(prevForm =="frmCreateAccountStep2"){
      prevFormName = prevForm;
    }else if(prevForm =="frmRewardsProgramInfo"){
      prevFormName = prevForm;
    }else if(prevForm =="frmSignIn"){
      prevFormName = prevForm;
    }else if(prevForm =="frmMore"){
      prevFormName = prevForm;
    }else if(prevForm =="frmCreateAccount"){
      prevFormName = prevForm;
    }else if(prevForm =="frmRewardsProgram"){
      
    }else{
      prevFormName = kony.application.getPreviousForm();
    }
    MVCApp.Toolbox.common.setBrightness("frmJoinRewards");
    
    var lTealiumTagObj = gblTealiumTagObj;
    lTealiumTagObj.page_id = "Join Rewards: Michaels Mobile App";
    lTealiumTagObj.page_name = "Join Rewards: Michaels Mobile App";
    lTealiumTagObj.page_type = "Rewards";
    lTealiumTagObj.page_category_name = "Rewards";
    lTealiumTagObj.page_category = "Rewards";
    TealiumManager.trackView("Join Rewards screen", lTealiumTagObj);
  }
   
 function showPreviousScreen(){
   var userObject = kony.store.getItem("userObj") || "";
    if(userObject !== ""){
       isAccCreated= true;
    }
       if(fromCoupons){
           if(isAccCreated )
                    {
                        MVCApp.getHomeController().loadCoupons();
                      isAccCreated = false;
                    }
                else
                  {
                          //#ifdef android
                    MVCApp.Toolbox.common.setTransition(kony.application.getPreviousForm(),2);
                    //#endif
                    frmSignIn.show(); 
                  }

    }
 else if(rewardsInfoFlag)
    {
        entryBrowserFormName="";
        MVCApp.getRewardsProgramInfoController().load(false);
    }
   else
     {
             if(prevFormName == "frmCreateAccountStep2"){
           frmCreateAccountStep2.show(); 
         }else if(prevFormName == "frmRewardsProgramInfo"){
           frmRewardsProgramInfo.show();
           MVCApp.tabbar.bindEvents(frmRewardsProgramInfo);
          MVCApp.tabbar.bindIcons(frmRewardsProgramInfo,"frmMore");
         }else if(prevFormName == "frmSignIn"){
            if(MVCApp.Toolbox.common.getRegion() === "US"){
             var url = "Account";
           MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + url);
           } else {
             if(toggleRewards === false){
                rewardsToggle();
            }
          rewardsInfoFlag = false;
          isFromShoppingList=false;
          MVCApp.getSignInController().load();
           }
           
         }else if(prevFormName == "frmMore"){
          // frmMore.show(); 
            MVCApp.getMoreController().load();
         }else if(prevFormName == "frmCreateAccount"){
            if(MVCApp.Toolbox.common.getRegion() === "US"){
           var url = "Account";
           MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + url);
            }    
         }else{
           if(fromCoupons){
             prevFormName.show();
             MVCApp.Toolbox.common.setBrightness("frmCoupons");
           } else{
             var form = kony.application.getPreviousForm();
             form.show(); 
          	 MVCApp.tabbar.bindEvents(form);
          	 MVCApp.tabbar.bindIcons(form,"frmMore");
           }
         }
     }
   
 }
  
   
  function bindEvents(){
 
    frmRewardsJoin.btnCheckBox.onClick = onClickAgree;   
    frmRewardsJoin.postShow = savePreviousScreenForm;
    frmRewardsJoin.btnHeaderLeft.onClick = showPreviousScreen;
    frmRewardsJoin.rtTermsandConditions.onClick = showTermsandConditions;
    frmRewardsJoin.btnJoinRewards.onClick = onClickJoinReward;
        frmRewardsJoin.flxOverlay.onTouchEnd = function(){};
    
    
    frmRewardsJoin.btnYes.onClick = function(){
      createAccountResObj = kony.store.getItem("userObj");
      if(MVCApp.Toolbox.common.getRegion() === "US"){
     	        if(gblUrl !== ""){
        var temp=[];
      if(MVCApp.Toolbox.common.getProducts().length>0){
         if(gblUrl !== "" && gblUrl.indexOf("favorites")!==-1){
            var urlArr1 = gblUrl.split(/&/g);
            var p_id = "";
           for(var i=0;i<urlArr1.length;i++){
							if(urlArr1[i].indexOf("pid")!==-1){
								p_id=urlArr1[i].split("=")[1];
							}
						}
           var products = MVCApp.Toolbox.common.getProducts()||[];
            for (var i = 0; i < products.length; i++) {
             if (products[i].product_id == p_id) {
               		temp.push(products[i])
             }
           }
           kony.print("gblUrl"+temp);
        if(MVCApp.Toolbox.common.getProducts().length==temp.length)
						MVCApp.Toolbox.common.setMerge(false);
					else
               MVCApp.Toolbox.common.setMerge(true);
         }else
         MVCApp.Toolbox.common.setMerge(true);
      }
      
      
      if(temp.length>0){
         var product =temp[0];
        // MVCApp.Toolbox.common.removeItemFromBasket(product.item_id,  MVCApp.Toolbox.common.getBasket(), MVCApp.Toolbox.common.getJWTToken(), function(){
                                                      MVCApp.Toolbox.common.createBasketforUser(function(){
        if(gblUrl !== ""){
          if(gblUrl.indexOf("pid") !== -1){
            var urlArr = gblUrl.split(/&/g);
            var pid = "";
            kony.print("fasfdsaf"+gblUrl.split(/&/g));
            for(var i=0;i<urlArr.length;i++){
              if(urlArr[i].indexOf("pid")!==-1){
                pid=urlArr[i].split("=")[1];
              }
            }
            if(pid){
              var storeId= MVCApp.Toolbox.common.getStoreID();
              if(fromForm == "frmCartView"){
                MVCApp.getCartController().loadCart(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Cart-MobileAppAddToWishlist?pid=" + pid + "", true);
              }else
                MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Add?pid="+pid+"");
            }
            fromForm = "";
          } 
          gblUrl = "";
        }

      }
       );
     // });      
      }
      else
      MVCApp.Toolbox.common.createBasketforUser(function(){
        if(gblUrl !== ""){
          if(gblUrl.indexOf("pid") !== -1){
            var urlArr = gblUrl.split(/&/g);
            var pid = "";
            kony.print("fasfdsaf"+gblUrl.split(/&/g));
            for(var i=0;i<urlArr.length;i++){
              if(urlArr[i].indexOf("pid")!==-1){
                pid=urlArr[i].split("=")[1];
              }
            }
            if(pid){
              var storeId= MVCApp.Toolbox.common.getStoreID();
              if(fromForm == "frmCartView"){
                MVCApp.getCartController().loadCart(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Cart-MobileAppAddToWishlist?pid=" + pid + "", true);
              }else
                MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+"on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Add?pid="+pid+"");
            }
            fromForm = "";
          }
          gblUrl = "";
        }

      }
       );
        } else {
           var url = "Account";
           MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + url);
        }
      } else {
       MVCApp.getRewardsProfileController().load(createAccountResObj);
      } 
    };
    frmRewardsJoin.btnRewardProgramInfo.onClick =function(){
      entryBrowserFormName="";
      MVCApp.getRewardsProgramInfoController().load(false);
    };
    frmRewardsJoin.btnNotRightNow.onClick =function(){
      isAccCreated= true;
      showPreviousScreen();
     }
    
    
    //#ifdef android
 	frmRewardsJoin.txtPhoneNo.onTextChange = function(){
      if(frmRewardsJoin.txtPhoneNo.text){
        frmRewardsJoin.btnClearPhoneNo.isVisible = true;
      }else{
        frmRewardsJoin.btnClearPhoneNo.isVisible = false;
      }
    };
    frmRewardsJoin.btnClearPhoneNo.onClick = function(){
     frmRewardsJoin.txtPhoneNo.text = "";
     frmRewardsJoin.btnClearPhoneNo.isVisible = false;
    };
    frmRewardsJoin.onDeviceBack = showPreviousScreen;
   //#endif
  }
  
  
  function onClickJoinReward(){
    if(validateFields()){
      var phoneNo = frmRewardsJoin.txtPhoneNo.text;
      MVCApp.getJoinRewardsController().joinRewards(phoneNo);
      TealiumManager.trackEvent("Join Rewards Button", {conversion_category:"Rewards",conversion_id:"Join Rewards Confirmation",conversion_action:2});
    }
  }
  
  function validateFields(){
    var phoneNo = frmRewardsJoin.txtPhoneNo.text;
    var numberRegEx=/^[0-9-]+$/;
    var errorMsg = [];
    var errFlag = false;
    if(phoneNo === null || phoneNo.trim() === "" && frmRewardsJoin.imgCheck.src !="checkbox_on.png"){
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.phoneOrTermsAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
      return false;
    }
    if(phoneNo === null || phoneNo.trim() === ""){
       MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.phone"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
      errFlag = true;
    }else{
     if(phoneNo.length == 10){
     if(!numberRegEx.test(phoneNo.trim())){
     alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContainsNumbers"));
     return false;
  	}
    }else{
      alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContains"));
      return false;
    }
    }   
    if(frmRewardsJoin.imgCheck.src !="checkbox_on.png"){
      MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.TermsCon"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
      errFlag = true;
    }    
    if(errFlag){
      return false;
    }
    return true;
  }
  
  function showTermsandConditions(){
    MVCApp.getBrowserController().load(MVCApp.serviceConstants.rewardTermsandConditions);
  }
  
  function onClickAgree(){
    if(frmRewardsJoin.imgCheck.src =="checkbox_off.png"){
      frmRewardsJoin.imgCheck.src ="checkbox_on.png";
    }else{
      frmRewardsJoin.imgCheck.src ="checkbox_off.png";
    }
  } 
  
  function updateScreen(createUserResponse){
    var email = createUserResponse.login || "";
    frmRewardsJoin.txtEmail.text = email;
    frmRewardsJoin.txtEmail.setEnabled(false);
    show();
  }
  
  function joinRewardsResponse(response){
    if(response.hasOwnProperty("c_loyaltyPhoneNo")){
      if(response.c_loyaltyPhoneNo){
        kony.store.setItem("userObj", response);
         var loyalty_id=response.c_loyaltyMemberID;
          kony.store.setItem("loyaltyMemberId",loyalty_id);
        //var reportObj={"loyaltyPhone":response.c_loyaltyMemberID,"loyaltyID":response.c_loyaltyPhoneNo};
        //reportObj=JSON.stringify(reportObj);
        //MVCApp.sendMetricReport(frmRewardsJoin,[{"SignUpRewardsOnly":reportObj}]);
         frmRewardsJoin.flxOverlay.setVisibility(true);
        
        if(rewardsInfoFlag)
    {
        frmRewardsJoin.btnYes.setVisibility(true);
         frmRewardsJoin.btnRewardProgramInfo.setVisibility(false);
         frmRewardsJoin.flxLine.setVisibility(true);
         frmRewardsJoin.flxLine2.setVisibility(true);
      	if(MVCApp.Toolbox.common.getRegion() === "US"){
           frmRewardsJoin.btnNotRightNow.setVisibility(false);
          frmRewardsJoin.flxLine3.setVisibility(false);
          frmRewardsJoin.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileCreate");
        } else {
          frmRewardsJoin.btnNotRightNow.setVisibility(true);
          frmRewardsJoin.flxLine3.setVisibility(true);
          frmRewardsJoin.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileSetup");
        }
         frmRewardsJoin.btnOk.setVisibility(false);
    }
  else
    {
            frmRewardsJoin.btnYes.setVisibility(true);
         frmRewardsJoin.btnRewardProgramInfo.setVisibility(true);
         frmRewardsJoin.flxLine.setVisibility(true);
         frmRewardsJoin.flxLine2.setVisibility(true);
      	if(MVCApp.Toolbox.common.getRegion() === "US"){
           frmRewardsJoin.btnNotRightNow.setVisibility(false);
          frmRewardsJoin.flxLine3.setVisibility(false);
          frmRewardsJoin.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileCreate");
        } else {
          frmRewardsJoin.btnNotRightNow.setVisibility(true);
          frmRewardsJoin.flxLine3.setVisibility(true);
          frmRewardsJoin.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileSetup");
        }
          
         frmRewardsJoin.btnOk.setVisibility(false);
    }
      }else{
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.serviceDown"), "");
      }
    }else{
      
      if(response.hasOwnProperty("UpdateUserErrorMessage")){
       var errText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.noRewardsAccountRewardsError")+response.UpdateUserErrorMessage;
          MVCApp.Toolbox.common.customAlertNoTitle(errText,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
    }
    else
      {
      	MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.emailphonecantverified"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
      }
    }
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  //Here we expose the public variables and functions
  
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	joinRewardsResponse:joinRewardsResponse
    	
    };
  
});



