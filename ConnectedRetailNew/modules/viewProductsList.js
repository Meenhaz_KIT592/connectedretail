/**
 * PUBLIC
 * This is the view for the ProductsList form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var pdpIndexArray = [];
var pdpRecomProducts = [];
var pdpImages = [];

var MVCApp = MVCApp || {};
var headerName;
var cust360valueSent= false;
MVCApp.ProductsListView = (function(){
  var areNoProjectsPresent = false;
  /**
	 * PUBLIC
	 * Open the ProductsList form
	 */
  function defaultForm(){
    frmProductsList.flxCategory.setVisibility(false);
    frmProductsList.flxProdProjTabs.isVisible = false;
    frmProductsList.flxRefineSearchWrap.setVisibility(false);
    frmProductsList.flxNoResults.setVisibility(false);
    frmProductsList.segProductsList.removeAll();
    frmProductsList.flxSearchResultsContainer.setVisibility(false);
	frmProductsList.flxHeaderWrap.textSearch.text = MVCApp.getProductsListController().getQueryString();
	//#ifdef android
    frmProductsList.flxHeaderWrap.btnClearSearch.setVisibility(false);
    MVCApp.Toolbox.common.setTransition(frmProductsList,3);
    //#endif
    frmProductsList.flxHeaderWrap.btnBarCode.setFocus(true);
    frmProductsList.show();
  }
  function findCategoryHeader(){
    var labelHeader=MVCApp.getProductsListController().getHeaderNameDeeplink() ||"";
    frmProductsList.btnCategoryName.text=labelHeader;
    
  }
  function determineContentOfHeader(fromSearch,fromDeeplink){
      if(fromSearch){
        frmProductsList.flxCategory.isVisible = false;
        frmProductsList.btnProducts.skin = "sknBtnTabsActive";
        frmProductsList.btnProducts.focusSkin = "sknBtnTabsActive";
        frmProductsList.btnProjects.skin = "sknBtnTabsDefault";
        frmProductsList.btnProjects.focusSkin = "sknBtnTabsDefault";
        if(gblWhenTheProductScannedIsNotPresent){
          gblWhenTheProductScannedIsNotPresent = false;
          frmProductsList.flxProdProjTabs.isVisible = false;
        }else{
          frmProductsList.flxProdProjTabs.isVisible = true;
        }
      }else{
        frmProductsList.flxCategory.isVisible = true;
        frmProductsList.flxProdProjTabs.isVisible = false;
      }
    //#ifdef android
    frmProductsList.flxMainContainer.forceLayout();
    //#endif
  }
  function staticShow(){
    //#ifdef android
	MVCApp.Toolbox.common.setTransition(frmProductsList,2);
    //#endif	
    // [artf3280] - Fix for issue - White screen is displaying on search results.
    if(frmProductsList.flxSearchResultsContainer.isVisible){
      frmProductsList.flxSearchResultsContainer.setVisibility(false);
    }
    frmProductsList.show();
    frmProductsList.flxHeaderWrap.btnBarCode.setFocus(true);
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }
  
  function setTopForNoResults(fromSearch){
    if(fromSearch){
      frmProductsList.flxNoResults.top="72dp";
      //#ifdef android
      frmProductsList.flxMainContainer.forceLayout();
      //#endif
    }else{
      frmProductsList.flxNoResults.top="114dp";
      //#ifdef android
      frmProductsList.flxMainContainer.forceLayout();
      //#endif
    }
    
  }
  function show(fromSearch){
	var isFromDeeplink=MVCApp.getProductsListController().getFromDeepLink();
    determineContentOfHeader(fromSearch,isFromDeeplink);
    frmProductsList.flxSearchResultsContainer.setVisibility(false);
    frmProductsList.segProductsList.removeAll();
    frmProductsList.flxNoResults.setVisibility(false);
    frmProductsList.flxMainContainer.contentOffset = {
			x: 0,
			y: 0
		};
    frmProductsList.flxHeaderWrap.textSearch.text="";
    var quertText = MVCApp.getProductsListController().getQueryString();
    //#ifdef android
    MVCApp.Toolbox.common.setTransition(frmProductsList,3);
    if(quertText!=""){
    frmProductsList.flxHeaderWrap.btnClearSearch.setVisibility(false);  
    }else{
    frmProductsList.flxHeaderWrap.btnClearSearch.setVisibility(false);    
    }
    //#endif
    frmProductsList.flxHeaderWrap.textSearch.text=quertText;
    
    frmProductsList.flxHeaderWrap.btnBarCode.setFocus(true);
    frmProductsList.show();
  }
  function getSelectedItem(){
    var prodTypeObj = {};
    var variant = true;
    var headerText = [];
    var master;
    var defaultProductId = "";
    var productId = "";
    var selectedItem = frmProductsList.segProductsList.selectedRowItems[0];
    var productIdForTracking="";
    if(selectedItem){
      if(selectedItem.product_type){
        try {
          prodTypeObj = JSON.parse(selectedItem.product_type);
          master = prodTypeObj.master;
        } catch(e) {
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"viewProductsList.js on getSelectedItem for selectedItem.product_type:" + JSON.stringify(e)}]);
          if(e){
            TealiumManager.trackEvent("Product Type Parse Exception In PLP Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }
      defaultProductId = selectedItem.c_default_product_id||"";
      productId = selectedItem.product_id;
    }
    
    if(master){
      variant = false;
    }else{
      variant = true;
    }
    
    if(frmProductsList.flxCategory.isVisible === true){
      headerText[0] = frmProductsList.btnCategoryName.text;
      headerText[1] = frmProductsList.imgCategoryImg.src;
    }else{
      headerText = [];
      headerText[0] = "";
      headerText[1] = "";
    }
      
    pdpIndexArray = [];
    pdpRecomProducts = [];
    pdpImages = [];
    
    MVCApp.getPDPController().setFromBarcode(false);
    if(!gblIsFromImageSearch){
      var queryString = MVCApp.getProductsListController().getQueryString();
      MVCApp.getPDPController().setQueryString(queryString);
    }
    MVCApp.getPDPController().setEntryType("plp");
    
    MVCApp.getPDPController().load(productId,variant,false,headerText,"frmProductsList",defaultProductId,true);
    if(frmProductsList.flxProdProjTabs.isVisible){
      kony.print("searchResultClick:ProductsList");
      MVCApp.sendMetricReport(frmProductsList, [{"searchResultClick":"ProductsList"}]);
    }
    var entryType=MVCApp.getProductsListController().getEntryType();
    if(defaultProductId==""){
      productIdForTracking=productId;
    }else{
      productIdForTracking=defaultProductId;
    }
    MVCApp.Toolbox.common.sendTealiumTagsForSearchAnalytics(entryType,productIdForTracking);
  }


  /**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
	
  function showPreviousScreen(){
    var storeID=MVCApp.getProductCategoryMoreController().getStoreID();
    var lIsStoreModeAlreadyEnabled = MVCApp.getProductCategoryMoreController().getStoreModeStatus();
    var storeIDNEW="";
    var lIsStoreModeAlreadyEnabledNew = false;
    var fromDeeplink=MVCApp.getProductsListController().getFromDeepLink();
    if(fromDeeplink){
     MVCApp.getHomeController().load(); 
    }else{
      if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      storeIDNEW = gblInStoreModeDetails.storeID;
      lIsStoreModeAlreadyEnabledNew = gblInStoreModeDetails.isInStoreModeEnabled;
    }else{
      if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
        var myLocationString=kony.store.getItem("myLocationDetails") || "";
        var locationDetails=JSON.parse(myLocationString);
        storeIDNEW=locationDetails.clientkey;
      }
    }
    
    var isFromProdSubCategory=MVCApp.getProductsListController().getFromProdSubCat();
    if((storeID !== storeIDNEW) || (lIsStoreModeAlreadyEnabled !== lIsStoreModeAlreadyEnabledNew)){
      if(isFromProdSubCategory){
        var backParams=MVCApp.getProductCategoryController().getSelectedSubCategory();
        MVCApp.getProductCategoryController().load(backParams);
      }else{
        var backParams=MVCApp.getProductCategoryMoreController().getSelectedSubCategory();
        MVCApp.getProductCategoryMoreController().load(backParams);
      }
    }else{
      if(isFromProdSubCategory){
        MVCApp.getProductCategoryController().displayPage();
      }else{
        //#ifdef android
        MVCApp.Toolbox.common.setTransition(frmProductCategoryMore,2);
        //#endif
        frmProductCategoryMore.show(); 
      }
    }
    }
  }
  
  function onDeviceBackFrmProductsList(){
    if(voiceSearchFlag){
      onXButtonClick(voiceSearchForm);
          if(voiceSearchForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")){
          	voiceSearchFlag = false;
            voiceSearchForm.flxVoiceSearch.setVisibility(false);
          } else if(sorryPageFlag){
            tapToCancelFlag = false;
          } else {
            voiceSearchFlag = true;
            tapToCancelFlag = true;
            deviceBackFlag = true;
          }
      MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
    }
    if(frmProductsList.flxCategory.isVisible){
      showPreviousScreen();
    }
  }
  
  function bindEvents(){
    frmProductsList.onHide = productsListOnHideCallback;
    frmProductsList.segProductsList.onRowClick = getSelectedItem;
    frmProductsList.btnCategoryName.onClick = showPreviousScreen;
    frmProductsList.lblCategoryBack.onTouchEnd = showPreviousScreen;
    frmProductsList.btnRefineSearch.onClick = onClickBtnRefineSearch;
    frmProductsList.btnProducts.onClick = onClickBtnProducts;
    frmProductsList.btnProjects.onClick = onClickBtnProjects;
    MVCApp.tabbar.bindIcons(frmProductsList,"frmProducts");
    MVCApp.tabbar.bindEvents(frmProductsList);
    MVCApp.searchBar.bindEvents(frmProductsList,"frmProductsList");
    frmProductsList.segProductsList.onTouchStart = function(eventObj,x,y){
   		checkonTouchStart(eventObj,x,y);
 	};
	frmProductsList.segProductsList.onTouchEnd = function(eventObj,x,y){
  	 checkonTouchEnd(eventObj,x,y);
	};
    frmProductsList.postShow = productsListPostShowCallback;
    frmProductsList.preShow = productsListPreShowCallback;

    //Fix for [artf3239]
    //If previous form is image search form then device back takes to image search invoked form
    //else just displays previous form.
    //#ifdef android
    frmProductsList.onDeviceBack = function(){
      if(voiceSearchFlag){
      onXButtonClick(voiceSearchForm);
          if(frmProductsList.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")){
          	voiceSearchFlag = false;
          } else if(frmProductsList.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain")){
            kony.print(" No results ...");
            voiceSearchFlag = false;
            tapToCancelFlag = false;
          } else {
            voiceSearchFlag = true;
            tapToCancelFlag = true;
            deviceBackFlag = true;
          }
      voiceSearchForm.flxVoiceSearch.setVisibility(false);
      MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
      MVCApp.searchBar.showPreviousForm(); 
      frmProductsList.flxVoiceSearch.setVisibility(false);  
    } else {
      var previousForm = kony.application.getPreviousForm();
      if(previousForm && ((previousForm.id == "frmImageOverlay") || (previousForm.id == "frmImageSearch"))){
        MVCApp.getImageSearchController().onClickImageSearchBack();
      }else if(frmProductsList.flxCategory.isVisible){
        showPreviousScreen();
      } 
    }
    };
    //#endif
  }
  
  function onClickBtnRefineSearch(){
    MVCApp.getProductsListController().setFromRefineFlag(true);
    var prodList = MVCApp.getProductsListController().getDataObjForProductList();
    MVCApp.getSortProductController().setDataForProductList(prodList);
    MVCApp.getSortProductController().load(true);
  }
  
  function onClickBtnProducts(){
    if(areNoProjectsPresent){
      areNoProjectsPresent = false;
      frmProductsList.btnProducts.skin = "sknBtnTabsActive";
      frmProductsList.btnProducts.focusSkin = "sknBtnTabsActive";
      frmProductsList.btnProjects.skin = "sknBtnTabsDefault";
      frmProductsList.btnProjects.focusSkin = "sknBtnTabsDefault";
      frmProductsList.flxNoResults.isVisible = false;
      if(gblIsFromImageSearch){
        frmProductsList.segProductsList.top = "100dp";
      }
      //#ifdef android
      frmProductsList.flxMainContainer.forceLayout();
      //#endif
    }
  }
  
  function onClickBtnProjects(){
    var totalProjects = null;
    if(gblIsFromImageSearch){
      totalProjects = projectsFromImageSearch.length === 0 ? false : projectsFromImageSearch.length;
    }else{
      totalProjects = MVCApp.getProductsListController().getProjectCount();
    }
    if(typeof totalProjects === "string"){
      totalProjects = parseInt(totalProjects);
    }
    var queryString = MVCApp.getProductsListController().getQueryString();
    var prevForm = kony.application.getPreviousForm();
    if((prevForm && prevForm != null && prevForm !== undefined) && (prevForm.id === "frmSortAndRefine")){
      gblIsFromSortAndFilter = true;
      var projStrWithCount = frmProductsList.btnProjects.text;
      if(projStrWithCount){
        var arrStr = projStrWithCount.split(/[()]/);
        if(arrStr[1] !== "0"){
          frmProductsList.flxNoResults.isVisible = false;
          MVCApp.getProjectsListController().setQueryStringForSearch(queryString);
          if(!gblIsFromImageSearch){
            MVCApp.getProjectsListController().setEntryType("textSearch");
          }else{
            MVCApp.getProjectsListController().setEntryType("imageSearch");
          }
          MVCApp.getProductsListController().goToProjects();
        }else{
          sorryPageFlag = false;  
          frmProductsList.btnProducts.skin = "sknBtnTabsDefault";
          frmProductsList.btnProducts.focusSkin = "sknBtnTabsDefault";
          frmProductsList.btnProjects.skin = "sknBtnTabsActive";
          frmProductsList.btnProjects.focusSkin = "sknBtnTabsActive";
          frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noProjectsFound")+"&nbsp;<b>"+queryString+"</b>";
          frmProductsList.flxNoResults.top = "142dp";
          frmProductsList.flxMainContainer.forceLayout();
          frmProductsList.flxNoResults.isVisible = true;
        }
      }else{
        sorryPageFlag = false;
        voiceSearchFlag = false;
        frmProductsList.flxNoResults.isVisible = false;
        frmProductsList.flxSearchOverlay.isVisible = false;
        MVCApp.getProjectsListController().setQueryStringForSearch(queryString);
        MVCApp.getProductsListController().goToProjects();
      }
      sorryPageFlag = false;
      voiceSearchFlag = false;
      frmProductsList.flxNoResults.isVisible = false;
      frmProductsList.flxSearchOverlay.isVisible = false;
      MVCApp.getProjectsListController().setQueryStringForSearch(queryString);
      MVCApp.getProductsListController().goToProjects();
    }else{
      gblIsFromSortAndFilter = false; 
      var entryType= MVCApp.getProductsListController().getEntryType();
      MVCApp.getProjectsListController().setEntryType(entryType);
      if(totalProjects){
        if(gblIsFromImageSearch){
          MVCApp.getImageSearchController().returnToFormFlag = true;
          MVCApp.getImageSearchController().showProjectsListFromImageSearch(true);
        }else{
          sorryPageFlag = false;
          voiceSearchFlag = false;
          frmProductsList.flxNoResults.isVisible = false;
          frmProductsList.flxSearchOverlay.isVisible = false;
          MVCApp.getProjectsListController().setQueryStringForSearch(queryString);
          MVCApp.getProductsListController().goToProjects();
        }
      }else{
        areNoProjectsPresent = true;
        if(voiceSearchFlag){
          sorryPageFlag = true;
          var queryString=MVCApp.getProductsListController().getQueryString();
          frmProductsList.flxVoiceSearch.lblUtterenace2.text = queryString;
          frmProductsList.flxVoiceSearch.lblUtterence1.setVisibility(false);
          frmProductsList.flxVoiceSearch.lblUtterance3.setVisibility(false);
          frmProductsList.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
          frmProductsList.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noProjectsFound");
          frmProductsList.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
          frmProductsList.flxVoiceSearch.setVisibility(true);
        }else{
        sorryPageFlag = false;
        frmProductsList.btnProducts.skin = "sknBtnTabsDefault";
        frmProductsList.btnProducts.focusSkin = "sknBtnTabsDefault";
        frmProductsList.btnProjects.skin = "sknBtnTabsActive";
        frmProductsList.btnProjects.focusSkin = "sknBtnTabsActive";
        if(gblIsFromImageSearch){
          frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noMatchingProjectsFound");
          frmProductsList.lbltextNoResultsFound.isVisible = false;
        }else{
          frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noProjectsFound")+"&nbsp;<b>"+queryString+"</b>";
          frmProductsList.lbltextNoResultsFound.isVisible = true;
        }
        
        if(gblIsFromImageSearch){
          frmProductsList.segProductsList.top = "142dp";
        }
        frmProductsList.flxNoResults.top = "142dp";
        //#ifdef android
        frmProductsList.flxMainContainer.forceLayout();
        //#endif
        frmProductsList.flxNoResults.isVisible = true;
        }
      }
    }
  }
  
  function productsListOnHideCallback(){
    frmProductsList.btnRefineSearch.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSortAndRefine.refine");
  }
  
  function productsListPreShowCallback(){
    frmProductsList.flxSearchOverlay.setVisibility(false);
    if(gblIsFromImageSearch){
      frmProductsList.segProductsList.top = "100dp";
      frmProductsList.flxNoResults.top = "100dp";
      frmProductsList.flxCategory.isVisible = false;
    }else{
      frmProductsList.segProductsList.top = "142dp";
      frmProductsList.flxNoResults.top = "142dp";
    }
  }
  
  function productsListPostShowCallback(){
        var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
    frmProductsList.flxHeaderWrap.lblItemsCount.text=cartValue;
    MVCApp.Toolbox.common.setBrightness("frmProductsList");
    MVCApp.Toolbox.common.destroyStoreLoc();

    var lPreviousForm = kony.application.getPreviousForm();
    var lPrevFormName = "";
    if(lPreviousForm){
      lPrevFormName = lPreviousForm.id;
    }
    setTealiumTagsForPLPPage(lPrevFormName);
    setCustomer360Tags();
  }
  function setCustomer360Tags(){
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    cust360Obj.entryType= MVCApp.getProductsListController().getEntryType();
    var productIds= MVCApp.getProductsListController().getProductIds();
    for(var m=0;m<productIds.length;m++){
      cust360Obj["productid"+m]=productIds[m];
    }
    if(!formFromDeeplink)
    MVCApp.Customer360.sendInteractionEvent("PLP", cust360Obj);
    else
      formFromDeeplink=false;
  }
  
  function setTealiumTagsForPLPPage(pPrevFormName){
    var lPageName = "";
    var lPageSubcategoryName = "";
    var lPageCategoryName = "";
    if(frmProductsList.btnCategoryName.text){
      lPageName = "Category:"+frmProductsList.btnCategoryName.text; 
      lPageSubcategoryName = frmProductsList.btnCategoryName.text;
      if(!frmProductsList.flxHeaderWrap.textSearch.text && (pPrevFormName === "frmProductCategoryMore" || pPrevFormName === "frmProductCategory")){
        if(pPrevFormName === "frmProductCategoryMore"){
          lPageCategoryName = frmProductCategory.btnCategoryName.text+":"+frmProductCategoryMore.btnCategoryName.text+":"+frmProductsList.btnCategoryName.text;
        }else if(pPrevFormName === "frmProductCategory"){
          lPageCategoryName = frmProductCategory.btnCategoryName.text+":"+frmProductsList.btnCategoryName.text;
        }
      }else if(!frmProductsList.flxHeaderWrap.textSearch.text && pPrevFormName === "frmHome"){
        lPageCategoryName = frmProductsList.btnCategoryName.text;
      }
    }
    MVCApp.getPDPController().setPageCategoryNameForPDP(lPageCategoryName);

    var lTealiumTagObj = gblTealiumTagObj;
    lTealiumTagObj.page_id = "Products Listing";
    lTealiumTagObj.page_name = lPageName;
    lTealiumTagObj.page_type = "shop category";
    lTealiumTagObj.page_category_name = lPageCategoryName;
    lTealiumTagObj.page_category = lPageCategoryName;
    lTealiumTagObj.page_subcategory_name = lPageSubcategoryName;
    var lProductsCount = 0;
    var lProjectsCount = 0;
    if(frmProductsList.flxHeaderWrap.textSearch.text && frmProductsList.flxProdProjTabs.isVisible){
      if(MVCApp.getProductsListController().getTotal()){
        lProductsCount = MVCApp.getProductsListController().getTotal();
      }
      if(MVCApp.getProductsListController().getProjectCount()){
        lProjectsCount = parseInt(MVCApp.getProductsListController().getProjectCount()); 
      }
      lTealiumTagObj.search_term = frmProductsList.flxHeaderWrap.textSearch.text;
      lTealiumTagObj.product_search_results = lProductsCount;
      lTealiumTagObj.project_search_results = lProjectsCount;
      lTealiumTagObj.search_results = lProductsCount + lProjectsCount;
      lTealiumTagObj.search_status = "successful";
    }else if(MVCApp.getProductsListController().getFromImageSearch()){
      if(MVCApp.getProductsListController().getProductTotalFromImageSearch()){
        lProductsCount = parseInt(MVCApp.getProductsListController().getProductTotalFromImageSearch());
      }
      if(MVCApp.getProductsListController().getProjectTotalFromImageSearch()){
        lProjectsCount = parseInt(MVCApp.getProductsListController().getProjectTotalFromImageSearch()); 
      }
      lTealiumTagObj.page_id = "Products Listing";
      lTealiumTagObj.page_name = "";
      lTealiumTagObj.page_category_name = "";
      lTealiumTagObj.page_category = "";
      lTealiumTagObj.page_subcategory_name = "";
      lTealiumTagObj.search_term = "";
      lTealiumTagObj.product_image_search_results = lProductsCount;
      lTealiumTagObj.project_image_search_results = lProjectsCount;
      lTealiumTagObj.search_results = lProductsCount + lProjectsCount;
      lTealiumTagObj.search_status = "successful";
      MVCApp.getProductsListController().setFromImageSearch(false);
    }else{
      lTealiumTagObj.search_term = frmProductsList.flxHeaderWrap.textSearch.text;
      lTealiumTagObj.search_results = 0;
      lTealiumTagObj.product_search_results = 0;
      lTealiumTagObj.project_search_results = 0;
      lTealiumTagObj.search_status = "unsuccessful";
    }
    TealiumManager.trackView("Shop Tier 3 Category Page", lTealiumTagObj);
  }

  function checkIfRequired(){

  }
  function updateProjectsCount(totalProjects,fromSearch){
    var totProjects = 0;
    if(totalProjects){
      totProjects = totalProjects;
    }
    var fromRefine = MVCApp.getProductsListController().getFromRefineFlag();
    if(!fromRefine){
      if(fromSearch){
        frmProductsList.btnProjects.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnProjects")+" ("+totProjects+")";
      	frmProductsList.btnProducts.skin = "sknBtnTabsActive";
      	frmProductsList.btnProducts.focusSkin = "sknBtnTabsActive";
      	frmProductsList.btnProjects.skin = "sknBtnTabsDefault";
      	frmProductsList.btnProjects.focusSkin = "sknBtnTabsDefault";
      	frmProductsList.flxProdProjTabs.isVisible = true;
      }else{
        frmProductsList.flxProdProjTabs.isVisible = false;
      }
    }
    //#ifdef android
    frmProductsList.flxMainContainer.forceLayout();
    //#endif
  }
  function updateScreen(results,fromSearch,count){
    kony.print("in Update Screen");
    frmProductsList.segProductsList.removeAll();
    frmProductsList.segProductsList.widgetDataMap={
      "imgItem":"picProduct",
      "lblItemName":"product_name",
      "lblItemPrice":"priceProduct",
      "lblItemPriceOld":"markedPriceProduct",
      "lblItemStock":"stockNumber",
      "btnStoreMap" : "btnStoreMap",
      "lblMapIcon":"lblMapIcon",
      "flxItemLocation":"flxItemLocation",
      "lblItemNumberReviews":"reviewNumber",
      "flxStarRating":"flxStarRating",
      "lblAisleNo":"lblAisleNo",
      "flxClearanceText":"flxClearanceText",
      "imgStar1":"imgStar1",
      "imgStar2":"imgStar2",
      "imgStar3":"imgStar3",
      "imgStar4":"imgStar4",
      "imgStar5":"imgStar5",
    };
    if(count===0){
      show(fromSearch);  
    }
    var dataProducts=results.getDataForProductList();
    var totalNumberSearchResults=0;
    var fromDeeplink=MVCApp.getProductsListController().getFromDeepLink();
    if(fromDeeplink){
      findCategoryHeader();
    }
    if(fromSearch || MVCApp.getProductsListController().getEntryType() === "QRCodeSearch"){
      if(!MVCApp.getProductsListController().getFromImageSearch()){
        var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
        var productIds= results.getProductIds();
        var totalProjectsResults = MVCApp.getProductsListController().getProjectCount();
        var conversionResult="App SearchSuccessful";
  		var productsCount=0;
        var searchStatus="successful";
        var searchType = "TextSearchResult";
        kony.print("the category array is"+productIds.length);
        if(productIds.length>0){
          for(var m=0;m<productIds.length;m++){
          cust360Obj["resultid"+m]= productIds[m];
        }
        productsCount=results.getTotalResults() ||0;
        cust360Obj.productsCount=productsCount;
        cust360Obj.projectsCount=totalProjects;
        cust360Obj.searchResult="success";
        }else{
          conversionResult="App SearchUnsuccessful";
          cust360Obj.failureCode="No Products Found";
          cust360Obj.productsCount=0;
          cust360Obj.projectsCount=0;
          cust360Obj.searchResult="failure";
          searchStatus="unsuccessful";
        }
        totalNumberSearchResults=parseInt(productsCount)+parseInt(totalProjectsResults);
        var searchTerm= MVCApp.getProductsListController().getQueryString();
        if(!voiceSearchFlag){
          if(fromDeeplink){
            if(MVCApp.getProductsListController().getEntryType() === "QRCodeSearch"){
              searchType = "QRcodeResult";
              var backParams=MVCApp.getProductsListController().getParamsForBack();
              cust360Obj.link=MVCApp.getProductsListController().getQRlink();
              cust360Obj.category=backParams.query.id;
             TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {conversion_category:conversionResult,conversion_id:"QRCodeSearch",conversion_action:2,qrcode_search_link:MVCApp.getProductsListController().getQRlink(),qrcode_search_status:searchStatus,qrcode_search_category:backParams.query.id}); 
            }
          } else {
            TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {conversion_category:conversionResult,conversion_id:"TextSearch",conversion_action:2,text_search_results:totalNumberSearchResults,text_search_status:searchStatus,text_search_term:searchTerm});
          }
        }
        MVCApp.Customer360.sendInteractionEvent(searchType, cust360Obj);
      }
    }
    frmProductsList.flxVoiceSearch.setVisibility(false);
    if(dataProducts.length === 0){
      if(voiceSearchFlag){
        sorryPageFlag = true;
        var queryString=MVCApp.getProductsListController().getQueryString();
        frmProductsList.flxVoiceSearch.lblUtterenace2.text = queryString;
        frmProductsList.flxVoiceSearch.lblUtterence1.setVisibility(false);
        frmProductsList.flxVoiceSearch.lblUtterance3.setVisibility(false);
        frmProductsList.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
        frmProductsList.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.NoResultsFoundForVoiceSearch");
        frmProductsList.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
        frmProductsList.flxVoiceSearch.setVisibility(true);
        MVCApp.Customer360.callVoiceSearchResult("failure","No Products Found","");
      }
      else{
      sorryPageFlag = false;  
      frmProductsList.flxNoResults.setVisibility(true);
      frmProductsList.flxRefineSearchWrap.setVisibility(false);
      var queryString=MVCApp.getProductsListController().getQueryString();
      frmProductsList.lbltextNoResultsFound.setVisibility(true);
      frmProductsList.flxProdProjTabs.isVisible = false;
      if(queryString === ''){
      queryString = headerName;  
      frmProductsList.lbltextNoResultsFound.setVisibility(false);
      }
   if(MVCApp.getProductsListController().getEntryType() === "QRCodeSearch" ){
      frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"); 
    } else {
      frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noResultsFound")+" <b>"+queryString+"</b>";
    }  
      }
     setTopForNoResults(fromSearch); 
    }else{
      if(voiceSearchFlag){
        MVCApp.Customer360.callVoiceSearchResult("success","","PLP",totalNumberSearchResults);
      }
      sorryPageFlag = false;
      voiceSearchFlag = false;
      frmProductsList.flxNoResults.setVisibility(false);
      frmProductsList.flxVoiceSearch.setVisibility(false);
      frmProductsList.flxSearchOverlay.isVisible = false;
      currentForm.flxVoiceSearch.setVisibility(false);
   	  currentForm.flxSearchOverlay.setVisibility(false); 
      frmProductsList.flxRefineSearchWrap.setVisibility(true);
      frmProductsList.segProductsList.setData(dataProducts);
      var queryString=MVCApp.getProductsListController().getQueryString();
      frmProductsList.flxHeaderWrap.textSearch.text = queryString;
      var totalResults=results.getTotalResults();
      var start=results.getStart();
      MVCApp.getProductsListController().setStart(parseInt(start));
      if(!gblIsFromImageSearch){
        MVCApp.getProductsListController().setTotal(parseInt(totalResults));
        if(kony.application.getCurrentForm().id !== "frmSortAndRefine"){
          var totalProjects = MVCApp.getProductsListController().getProjectCount();
          updateProjectsCount(totalProjects,fromSearch);
        }
        frmProductsList.btnProducts.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnProduts")+" ("+ totalResults+")";
        var refineCount = MVCApp.getProductsListController().getRefineCount();
        if(refineCount){
          frmProductsList.btnRefineSearch.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSortAndRefine.refine")+" ("+refineCount+")";
        }else{
          frmProductsList.btnRefineSearch.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSortAndRefine.refine");
        }
      }
    }

    MVCApp.Toolbox.common.dismissLoadingIndicator();
    showLoading(false);
  }
  function showForNoResults(showForNoResults){
    frmProductsList.flxProdProjTabs.isVisible = false;
    frmProductsList.flxRefineSearchWrap.isVisible = false;
    var formId = kony.application.getCurrentForm().id;
    if(formId !== "frmProductsList"){
      setTopForNoResults(showForNoResults);
      show(showForNoResults);
    }else{
      frmProductsList.flxSearchResultsContainer.isVisible = false;
      frmProductsList.segProductsList.removeAll();
      setTopForNoResults(showForNoResults);
    }
    frmProductsList.flxHeaderWrap.textSearch.text="";
    frmProductsList.flxNoResults.isVisible = true;
    frmProductsList.lbltextNoResultsFound.isVisible = false;
    var queryString = MVCApp.getProductsListController().getQueryString();
    if(queryString.startsWith("appservices") ||queryString.startsWith("http") ){
      frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"); 
    } else {
      frmProductsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noResultsFound")+" <b>"+queryString+"</b>";
    }
    frmProductsList.flxHeaderWrap.btnBarCode.setFocus(true);
    if(formId == "frmProductsList"){
      frmProductsList.show();
    }
  }

  function showMore(results){
    var dataProducts=results.getDataForProductList();
    var start=results.getStart();
    MVCApp.getProductsListController().setStart(parseInt(start));
    frmProductsList.segProductsList.addAll(dataProducts);
     showLoading(false);
    setCustomer360Tags();
    MVCApp.Toolbox.common.dismissLoadingIndicator();
  }

  function pushMoreRecords(results){
    
  		var dataProducts=results.getDataForProductList();
        frmProductsList.segProductsList.addAll(dataProducts);
    	showLoading(false);
	}  
  
    function showLoading(load){
    var bottom = -40;
    var delay = 0.25;
    if(load){
    	bottom = 0;
        delay = 0;

    }
      //MVCApp.Toolbox.common.dispFooter();   
    frmProductsList["flexLoading"].animate(kony.ui.createAnimation({"100":{"bottom":bottom+"dp","stepConfig":{"timingFunction":kony.anim.EASE}}}),
									{"delay":delay,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
 									{});
   
  }
  
  
  function setHeaderText(headerText){
    if(headerText){
      headerName = headerText.name;
      frmProductsList.btnCategoryName.text = headerText.name;
      frmProductsList.imgCategoryImg.src = headerText.headerImage;
    }
  }  //Here we expose the public variables and functions

   
   //Code for Footer Animation
  var checkX;
  var checkY;
  
  function checkonTouchStart(eventObj, x, y){  
    checkX=x;
    checkY=y; 
  }
  
  function checkonTouchEnd(eventObj,x,y){  
    var tempX;
    var tempY;
    tempX=checkX;
    tempY=checkY;
    checkX=x;
    checkY=y;
    var checkScroll;
    checkScroll=tempY-checkY;
    if (checkScroll>1){
        MVCApp.Toolbox.common.hideFooter();
        frmProductsList.segProductsList.bottom = "0dp";
    }else{
        MVCApp.Toolbox.common.dispFooter();
        frmProductsList.segProductsList.bottom = "54dp";
    }
  }

  
  return{
    show: show,
    defaultForm:defaultForm,
    bindEvents: bindEvents,
    updateScreen:updateScreen,
    showMore:showMore,
    checkIfRequired:checkIfRequired,
    setHeaderText:setHeaderText,
    staticShow:staticShow,
    showForNoResults:showForNoResults,
    pushMoreRecords:pushMoreRecords,
	showLoading:showLoading,
    getSelectedItem:getSelectedItem,
    updateProjectsCount:updateProjectsCount
  };

});



