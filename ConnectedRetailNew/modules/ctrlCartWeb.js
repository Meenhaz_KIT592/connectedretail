
/**
 * PUBLIC
 * This is the controller for the ProductsList form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
MVCApp.CartController = (function(){
  	var _isInit = false;
  	 var model = null;//Backend interaction
    var view = null;//UI interaction
 	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */
	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.CartViewModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.CartView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  
  	function load(searchString){
      init();
      frmCartView.flxCart.isVisible=true;
      frmCartView.lblTitle.text ="CART";
      model.load(searchString,view.loadPdpView);
      _isInit=false;
    }
  
  /**
     * PUBLIC
     * Open the form.
     */
    function loadWebView(url,flag) {
           init();
        frmCartView.flxCart.isVisible=true;
      	frmCartView.lblTitle.text ="CART";
         //#ifdef iphone
          frmCartView.brwrView.isVisible=false;
          frmCartView.btnBack.isVisible = true;
          //#endif
      	var storeId= MVCApp.Toolbox.common.getStoreID();
          	var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
          	var appRequest= "?appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
          	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
              	if(storeChangeInStoreMode){
                  	appRequest = "?appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                }
                    appRequest = appRequest + "&storevisit="+storeId;
     		}
          	if(url.indexOf("?") !== -1){
              appRequest = appRequest.replace(/\?/g, "&");
            }
      	url = url+appRequest;
      
        model.loadData(view.loadPdpView,url,flag);
      
    }
  
  function loadCart(url,flag) {
           init();
     frmCartView.flxCart.isVisible=true;
      frmCartView.lblTitle.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.cart");
         //#ifdef iphone
          frmCartView.brwrView.isVisible=false;
          //#endif
      	var storeId= MVCApp.Toolbox.common.getStoreID();
          	var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
          	var appRequest= "?appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
          	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
              	if(storeChangeInStoreMode){
                  	appRequest = "?appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                }
                    appRequest = appRequest + "&storevisit="+storeId;
     		}
          	if(url.indexOf("?") !== -1){
              appRequest = appRequest.replace(/\?/g, "&");
            }
    	url = url+appRequest;
        model.loadData(view.loadPdpView,url,flag);
      
    }
  
  function getAppSrc(){
      var region= "mik_us_mobile";
      var locale= kony.store.getItem("languageSelected");
      if(locale == "en_US" || locale == "en"){
        
      }else if(locale =="en_CA"){
        region="mik_cn_en_mobile"
      }else if(locale=="fr_CA"){
        region="mik_cn_fr_mobile"
      }
      return region;
    }
    //Here we expose the public variables and functions
 	return {
      init: init,
      load: load,
      loadWebView:loadWebView,
      loadCart:loadCart
    };
});

