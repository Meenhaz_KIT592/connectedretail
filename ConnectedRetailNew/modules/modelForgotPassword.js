/**
 * PUBLIC
 * This is the model for JoinRewards form
 */
var MVCApp = MVCApp || {};
MVCApp.ForgotPasswordModel = (function(){
 var serviceType = MVCApp.serviceType.DemandwareService;
  var operationId = MVCApp.serviceEndPoints.resetPassword;
  function submitEmail(callback,requestParams){
    kony.print("ForgotPasswordModel.js");
   MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
          callback(results);
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
    }
                   );
   // callback("");
  }
 	return{
      submitEmail:submitEmail
    };
});
