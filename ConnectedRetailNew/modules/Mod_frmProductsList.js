//Type your code here

function tapProductsList() {
	frmProductsList.flxFooterWrap.flxProducts.skin = 'sknFlexBgBlackOpaque10';
	frmProductsList.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmProductsList.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmProductsList.flxFooterWrap.flxEvents.skin = 'slFbox';
	frmProductsList.flxFooterWrap.flxMore.skin = 'slFbox';
}

function pre_frmProductsList() {
	searchBlur();
	frmProductsList.flxHeaderWrap.btnHome.isVisible = true;
	frmProductsList.flxHeaderWrap.textSearch.text = '';
}

function NAV_frmProductsList() { frmProductsList.show(); pre_frmProductsList(); tapProductsList(); }