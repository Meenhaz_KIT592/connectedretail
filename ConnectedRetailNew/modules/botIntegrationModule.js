//Type your code here
function createSingletons() {
  createVoiceAndSpeechObjectsFlag = true;
  // Common
  audioSession = objc.import('AVAudioSession').sharedInstance();

  // For speech synthesis
  synthesizer = objc.import('AVSpeechSynthesizer').jsnew();
  SynthesizerDelegate = objc.newClass('SynthesizerDelegate', 'NSObject', [ 'AVSpeechSynthesizerDelegate ' ], {
    speechSynthesizerDidFinishSpeechUtterance: function (s, u) {
     speakTextDone();
    }
  });
  synthesizerDelegate = SynthesizerDelegate.jsnew();
  synthesizer.delegate = synthesizerDelegate;
  
  englishVoice = objc.import('AVSpeechSynthesisVoice').voiceWithLanguage("en-US");
  AVSpeechUtterance = objc.import('AVSpeechUtterance');

  // For speech recognition
  SFSpeechRecognizer = objc.import('SFSpeechRecognizer');
  
  //recognizer.initWithLocale(nsLocale);
  RecognizerDelegate = objc.newClass('RecognizerDelegate', 'NSObject', [ 'SFSpeechRecognizerDelegate' ], {
    speechRecognizerAvailabilityDidChange: function (available) {
      //frm.recoButton.setEnabled(available);
    }
  });
  recognizerDelegate = RecognizerDelegate.jsnew();
  
  recognitionTask = null;
  recognitionRequest = null;
  audioEngine = objc.import('AVAudioEngine').jsnew();

  SFSpeechAudioBufferRecognitionRequest = objc.import('SFSpeechAudioBufferRecognitionRequest');

  SFSpeechRecognizer.requestAuthorization(function (authStatus) {
    if (authStatus == SFSpeechRecognizerAuthorizationStatusAuthorized) {
      //frm.recoButton.setEnabled(true);
    } else {
      //frm.recoButton.setEnabled(false);
    }
  });

  NSBundle = objc.import('NSBundle');
  NSURL = objc.import('NSURL');
  AVAudioPlayer = objc.import('AVAudioPlayer');
  PlayerDelegate = objc.newClass('PlayerDelegate', 'NSObject', [ 'AVAudioPlayerDelegate' ], {
    audioPlayerDidFinishPlayingSuccessfully: function (p, f) {
      kony.print('Finished playing');
      kony.print("finished playing sound ");
      if(p.callback){
        kony.print("callback getting called");
        var c = p.callback;
        kony.print("calling the callback funtion");
        c();
      }
    }
  });
  playerDelegate = PlayerDelegate.jsnew();

}
var stopReco = false;
function stopSpeechReco() {
  synthesizer.delegate = null;
  if (recognitionRequest) {
    //kony.print('Ending audio to reco');
    recognitionRequest.endAudio();
    stopReco = true;
    return true;
  }
  
  return false;
}

// The purpose of this variable is to make sure that startSpeechReco
// cannot be invoked unless a prior invocation is balanced with stopSpeechReco
var inReco = false;

// The purpose of this variable is to make sure that canStopCallback is
// invoked exactly once during a startSpeechReco invocation
var canStopCBInvoked = false;
// The purpose of this variable is to make sure that timer is scheduled and
// cancelled properly
var timerScheduled = false;

function startSpeechReco(partialResultCallback, finalCallback, timeout, canStopCallback) {
  function doStartSpeechReco() {
    audioSession.setCategoryError(AVAudioSessionCategoryRecord, undefined);
    audioSession.setModeError(AVAudioSessionModeMeasurement, undefined);
    audioSession.setActiveWithOptionsError(true, AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation, undefined);
	var locale = MVCApp.Toolbox.Service.getLocale();
  	locale  = locale == "default" ? "en-US" : locale;
    var recognizer = SFSpeechRecognizer.alloc().initWithLocale(objc.import('NSLocale').localeWithLocaleIdentifier(locale));
    recognizer.delegate = recognizerDelegate;
    var inputNode = audioEngine.inputNode;
    recognitionRequest = SFSpeechAudioBufferRecognitionRequest.jsnew();
    recognitionRequest.shouldReportPartialResults = true;
    
    if(recognitionTask !== null){
    	recognitionTask.cancel();
    	recognitionTask = null;
    }
    
    recognitionTask = recognizer.recognitionTaskWithRequestResultHandler(recognitionRequest, function (result, error) {
      if (result) {
        //kony.print('received result');
        if (timerScheduled) {
          kony.timer.cancel('recoTimer');
          timerScheduled = false;
        }

        partialResultCallback(result.bestTranscription.formattedString);

        if (!result.final) {
          kony.timer.schedule('recoTimer', function () {
            kony.print('Time out');
            recognizer.delegate = null;
            stopSpeechReco();
          }, timeout, false);
          timerScheduled = true;
        }
      }

      if (error || (result && result.final)) {
        if (error) {
          //kony.print('Error in reco');
        } else {
        //kony.print('received final result');
        }
        audioEngine.stop();
        //kony.print('1');
        inputNode.removeTapOnBus(0);
        //kony.print('2');
        audioSession.setCategoryError(AVAudioSessionCategorySoloAmbient , undefined);
        audioSession.setModeError(AVAudioSessionModeDefault, undefined);
        audioSession.setActiveWithOptionsError(true, AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation, undefined);
        //kony.print('3');
        recognitionRequest = null;
        recognitionTask.cancel();
        //kony.print('4');
        recognitionTask = null;
        //kony.print('Cleanup done');

        if (!error) {
          inReco = false;
          finalCallback();
        } else {
          if (!stopReco) {
            // Apple servers returned error: retry
          doStartSpeechReco();
          } else {
            // Error may arise due to stopped speech
            // which is an OK case
            inReco = false;
            finalCallback();
          }
        }
      }
    });

    inputNode.installTapOnBusBufferSizeFormatBlock(0, 1024, inputNode.outputFormatForBus(0), function (buf, when) {
      //kony.print('Got bytes');
      recognitionRequest.appendAudioPCMBuffer(buf);
    });
    audioEngine.prepare();
    //kony.print('Starting audio engine');
    inReco = true;
    canStopCBInvoked = false;
    timerScheduled = false;
    audioEngine.startAndReturnError(undefined);
    if (!canStopCBInvoked) {
      if (canStopCallback) canStopCallback();
      canStopCBInvoked = true;
    }
  }

  if (!inReco) {
    stopReco = false;
    doStartSpeechReco();
  }
}

var speakTextCallBack = null;
function speakTextDone(){

  if(speakTextCallBack){
    kony.print("speakTextDone speakTextCallBack getting called");
    speakTextCallBack();
  }
}
function speakText(text,callback) {
  speakTextCallBack = callback;
  var utterance = AVSpeechUtterance.speechUtteranceWithString(text);
  var localeVoice=englishVoice;
  var language= kony.i18n.getCurrentLocale();
  utterance.voice = englishVoice;
  kony.print("in speaktext speakUtterance getting called");
  synthesizer.speakUtterance(utterance);
}

var recoTimerFired = false;
var fromSpeakButtonClick = false;
function speakButtonClick() {
  kony.print("in speakButtonClick");
  recoTimerFired = false;
  if (!audioEngine.running) {
     kony.print("in speakButtonClick  audioEngine is not running");
    kony.print('Audio Engine is not running!');
    if (recognitionTask) {
      recognitionTask.cancel();
      recognitionTask = null;
    }

    audioSession.setCategoryError(AVAudioSessionCategoryRecord, undefined);
    audioSession.setModeError(AVAudioSessionModeMeasurement, undefined);
    audioSession.setActiveWithOptionsError(true, AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation, undefined);

    var inputNode = audioEngine.inputNode;
    recognitionRequest = SFSpeechAudioBufferRecognitionRequest.jsnew();
    recognitionRequest.shouldReportPartialResults = true;
    recognitionTask = recognizer.recognitionTaskWithRequestResultHandler(recognitionRequest, function (result, error) {
      kony.print("in speakButtonClick  in result handler");
      kony.print('in result handler');
      if (result) {
         kony.print("in speakButtonClick  result");
        kony.print('result');
        kony.runOnMainThread(function (text) {
           kony.print("in speakButtonClick  runOnMainThread function");
          if(currentBotSetting === botSettings.handsFreeWithVisualCues){
            frmMain.lblHandsFreeUserSays.text = text;
          }else if(currentBotSetting === botSettings.handsFree){

          }else{
            frmMain.txtUserSays.text = text;
          }
          
        }, [result.bestTranscription.formattedString]);

        if (!recoTimerFired) {
          kony.print("in speakButtonClick  recoTimerFired not fired yet");
          try {
             kony.print("in speakButtonClick  recoTimerFired  fired,caneling the timer");
            kony.timer.cancel('recoTimer');
          } catch (err) {}
           kony.print("in speakButtonClick  scheduling recoTimer");
            kony.timer.schedule('recoTimer', function () {
            recoTimerFired = true;
             kony.print("in speakButtonClick  timer expired");
            kony.print('Timer expired');
            kony.timer.cancel('recoTimer');
            audioEngine.stop();
            recognitionRequest.endAudio();
            kony.print("in speakButtonClick  recognitionRequest endAudio called");
          } , 1, false);
        }
      }

      if (error || (result && result.final)) {
        var recoText = result.bestTranscription.formattedString;
        kony.print('error or end');
        //audioEngine.stop();
        inputNode.removeTapOnBus(0);

        audioSession.setCategoryError(AVAudioSessionCategorySoloAmbient , undefined);
        audioSession.setModeError(AVAudioSessionModeDefault, undefined);
        audioSession.setActiveWithOptionsError(true, AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation, undefined);

        recognitionRequest = null;
        recognitionTask = null;
          kony.print("in speakButtonClick  onTextReco callback getting called");
          if(currentBotSetting === botSettings.handsFreeWithVisualCues) {
            fromSpeakButtonClick = true;
            delayCall(function(){onTextReco(recoText)},.5);
          }else if(currentBotSetting === botSettings.handsFree){
            delayCall(function(){onTextRecoHandsFreeOnly(recoText)},.5);
          }else{
            fromSpeakButtonClick = true;
            delayCall(function(){startBotThinkingAnimation(recoText)},.5);
          }
      }
    });

    inputNode.installTapOnBusBufferSizeFormatBlock(0, 1024, inputNode.outputFormatForBus(0), function (buf, when) {
      recognitionRequest.appendAudioPCMBuffer(buf);
    });
    kony.print("in speakButtonClick  audioEngine.prepare()  getting called");
    audioEngine.prepare();
    audioEngine.startAndReturnError(undefined);
    kony.print("in speakButtonClick  audioEngine.startAndReturnError  called");
  } else {
    kony.print('Audio Engine is running!');
    kony.print("Audio Engine is running!");
  }
}

function firstEntity(entities, entity) {
  var val = entities && entities[entity] &&
    Array.isArray(entities[entity]) &&
    entities[entity].length > 0 &&
    entities[entity][0]
  ;
  if (!val) {
    return null;
  }
  return val;
}


