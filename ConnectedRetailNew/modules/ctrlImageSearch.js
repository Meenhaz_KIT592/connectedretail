var MVCApp = MVCApp || {};
var productsFromImageSearch=[];
var projectsFromImageSearch=[];
MVCApp.ImageSearchController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
  var tryAgainAlert = null;
  var lProjectIds = "";
  var lProductsCount = "";
  var lPLPServiceRequestObj = {};
  var lPJLPServiceRequestObj = {};
  var returnToFormFlag = false;
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.ImageSearchModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.ImageSearchView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  
  	function load(){
      init();
    }
  
    function didCaptureImage(selectedImg){
//       imageSearchWaitFlag = true;
//       imageSearchSuccessFlag = false;
//       imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.quarterTimeInSeconds"));
//       var cust360obj= MVCApp.Customer360.getGlobalAttributes();
//       var lProjectsCount=0;
//       if(imageSearchGalleryFlag){
//         imageSearchProgressForm = frmImageSearch;
//         cust360obj.searchType= "gallery";
//       }else{
//         imageSearchProgressForm = frmImageOverlay;
//         cust360obj.searchType= "camera";
//       }
//       MVCApp.Customer360.sendInteractionEvent("ImageSearchCapture", cust360obj);
//       if (!_isInit){
//       	init();
//       }
//       //#ifdef iphone
//         imageSearchProgressForm.imgImageSearch.rawBytes = selectedImg.rawBytes;
//       //#elseif
//       	if(imageSearchGalleryFlag){
//           imageSearchProgressForm.imgImageSearch.rawBytes = selectedImg.rawBytes;
//         }else{
//           build = java.import('android.os.Build');
// 		 var returnedValue = "";
// 		if (build.VERSION.SDK_INT >= build.VERSION_CODES.M){
//     		returnedValue=	cameraWidgetRef.base64;
// 		} else{
//     		returnedValue =imagerotateforandroid.getRotatedImage(cameraWidgetRef.base64,2000,100);
// 		}
         
//          	 imageSearchProgressForm.imgImageSearch.base64 = returnedValue;
//         }
//       //#endif
//       if(imageSearchGalleryFlag){
//         view.bindEvents();
//         view.show();
//       }else{
//       	imageSearchProgressForm.imgImageSearch.setVisibility(true);
//       	view.imageSearchPreShow();
//       }
//       if(SlyceManager && SlyceManager.isSlyceOpened()){
//       //#ifdef iphone
//       	if(selectedImg.rawBytes.livePhotoResourcePaths && selectedImg.rawBytes.livePhotoResourcePaths.imageURL){
//          // SlyceManager.getSlyceResults(selectedImg.rawBytes.livePhotoResourcePaths.imageURL, getCountryForSlyce(), imageSearchResultsCallback)
//         }else{
//       //#endif
//       var imageSearchFilePath = kony.io.FileSystem.getDataDirectoryPath() + "/imageSearch.png";
//       kony.print("imageSearchFilePath = " + imageSearchFilePath);
//       imageSearchPNG = new kony.io.File(imageSearchFilePath);
//       var isImageSearchFileCreated = imageSearchPNG.createFile();
//       if(isImageSearchFileCreated){
//         kony.print("File Created @ " + imageSearchPNG.exists());
//         //#ifdef iphone
//           imageSearchPNG.write(selectedImg.rawBytes);
//         //#elseif
//           if(imageSearchGalleryFlag){
//             imageSearchPNG.write(selectedImg.rawBytes);
//           }else{
//             imageSearchPNG.write(cameraWidgetRef.rawBytes);
//           }
//           cameraWidgetRef.releaseRawBytes(cameraWidgetRef.rawBytes);
//         //#endif
//       //  SlyceManager.getSlyceResults(imageSearchFilePath, getCountryForSlyce(), imageSearchResultsCallback);
//       }
//       //#ifdef iphone
//       }
//       //#endif
//       }else{
//         imageSearchResultsCallback("error", null);
//       }
    }
    
    function isJson(str) {
      try {
        JSON.parse(str);
      } catch (e) {
        return false;
      }
      return true;
    }

    function imageSearchResultsCallback(status, results){
      kony.print("imageSearchResultsCallback: status = " + status);
      kony.print("imageSearchResultsCallback: results = " + results);
      kony.print("imageSearchResultsCallback: results is json " + isJson(results));
      imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.postSlyceResults"));
      var cust360obj= MVCApp.Customer360.getGlobalAttributes();
      var resultsetFromSlyce=[];
      var productsCount=0;
      var projectsCount=0;
      var totalCount=0;
      var conversionResult="App SearchSuccessful";
      var imageSearchStatus="successful";
      projectsFromImageSearch = [];
      productsFromImageSearch = [];
      if(imageSearchProgressFlag){
      if(status == "success"){
        cust360obj.searchResult="success";
        var resultsObj = null;
        //#ifdef android
        if(isJson(results)){
          resultsObj = JSON.parse(results);
        }
        //#elseif
        if(results){
          resultsObj = results;
        }
        //#endif
        var searchKeywords=resultsObj.searchKeywords||"";
        cust360obj.searchKeyword=searchKeywords;
        if(resultsObj && resultsObj.products){
          for (var i = 0; i < resultsObj.products.length; i++) {
            var productIdStr = resultsObj.products[i].productId;
            resultsetFromSlyce.push(productIdStr);
            if (kony.string.startsWith(productIdStr, "B") || kony.string.startsWith(productIdStr, "C")) {
              if (i == 0) {
                imageSearchProjectBasedFlag = true;
              }
              projectsFromImageSearch.push(resultsObj.products[i]);
            } else {
              productsFromImageSearch.push(resultsObj.products[i]);
            }
          }
          gblIsFromImageSearch = true;
          if (imageSearchProjectBasedFlag) {
            lProductsCount = productsFromImageSearch.length;
            showProjectsListFromImageSearch(false);
          } else {
            showProductsListFromImageSearch();
          }
        }else{
          imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.onErrorReceipt"));
          imageSearchSuccessFlag = false;
          imageSearchWaitFlag = false;
          cust360obj.searchResult="failure";
		  cust360obj.productsCount=0;
		  cust360obj.projectsCount=0;
          cust360obj.failureCode=results;
          imageSearchStatus="unsuccessful";
          conversionResult="App SearchUnsuccessful";
        }
      }else{
        imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.onErrorReceipt"));
        imageSearchSuccessFlag = false;
        imageSearchWaitFlag = false;
        setTeliumDataForImageSearch();
        cust360obj.searchResult="failure";
		cust360obj.productsCount=0;
        cust360obj.projectsCount=0;
        conversionResult="App SearchUnsuccessful";
        imageSearchStatus="unsuccessful";
        if(results!=null){
          cust360obj.failureCode=results;
        }else{
          cust360obj.failureCode="Error Processing Image"
        }
      } 
        projectsCount=projectsFromImageSearch.length||0;
        productsCount=productsFromImageSearch.length||0;
        totalCount=projectsCount+productsCount;
        MVCApp.Customer360.sendInteractionEvent("ImageSearchResult", cust360obj);
        TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {conversion_category:conversionResult,conversion_id:"ImageSearch",conversion_action:2,image_search_results:totalCount,image_search_status:imageSearchStatus});
      }
    }

    function showProductsListFromImageSearch() {
        //Initiating the request object
        var lRequestParams = {};
        //Getting product_ids and master_product_ids values
        var lProduct_ids = "";
        var lMaster_product_ids = "";
        for (var k = 0; k < productsFromImageSearch.length; k++) {
            if (productsFromImageSearch.length === 1) {
                lProduct_ids = "(" + productsFromImageSearch[k].productId + ")";
                if (productsFromImageSearch[k].productAltID) {
                    lMaster_product_ids = "(" + productsFromImageSearch[k].productAltID + ")";
                } else {
                    lMaster_product_ids = "()";
                }
            } else {
                if (k === 0) {
                    lProduct_ids = "(" + productsFromImageSearch[k].productId;
                    if (productsFromImageSearch[k].productAltID) {
                        lMaster_product_ids = "(" + productsFromImageSearch[k].productAltID;
                    } else {
                        lMaster_product_ids = "(";
                    }
                } else if (k === productsFromImageSearch.length - 1) {
                    lProduct_ids = lProduct_ids + "," + productsFromImageSearch[k].productId + ")";
                    if (productsFromImageSearch[k].productAltID) {
                        lMaster_product_ids = lMaster_product_ids + "," + productsFromImageSearch[k].productAltID + ")";
                    } else {
                        lMaster_product_ids = lMaster_product_ids + ")";
                    }
                } else if (k > 0 && k < productsFromImageSearch.length - 1) {
                    lProduct_ids = lProduct_ids + "," + productsFromImageSearch[k].productId;
                    if (productsFromImageSearch[k].productAltID) {
                        lMaster_product_ids = lMaster_product_ids + "," + productsFromImageSearch[k].productAltID;
                    }
                }
            }
        }
        lRequestParams.product_ids = lProduct_ids;
        lRequestParams.master_product_ids = lMaster_product_ids;
        lPLPServiceRequestObj.product_ids = lProduct_ids;
        lPLPServiceRequestObj.master_product_ids = lMaster_product_ids;
        imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.preDWServiceCall"));
        getPLPDataForImageSearch(lRequestParams);
    }

    function showProjectsListFromImageSearch(showLoadingFlag) {
        //Initiating the request object
        var lRequestParams = {};
        var lMaster_product_ids = "";
        for (var k = 0; k < projectsFromImageSearch.length; k++) {
            if (projectsFromImageSearch.length === 1) {
                lProjectIds = "(" + projectsFromImageSearch[k].productId + ")";
            } else {
                if (k === 0) {
                    lProjectIds = "(" + projectsFromImageSearch[k].productId;
                } else if (k === projectsFromImageSearch.length - 1) {
                    lProjectIds = lProjectIds + "," + projectsFromImageSearch[k].productId + ")";
                } else if (k > 0 && k < projectsFromImageSearch.length - 1) {
                    lProjectIds = lProjectIds + "," + projectsFromImageSearch[k].productId;
                }
            }
        }
        fetchDataForPJLPScreen(showLoadingFlag);
    }
  
  	function getPLPDataForImageSearch(pRequestParams){
      if(imageSearchProgressFlag){
      pRequestParams.store_id = getCurrentStoreId();
      pRequestParams.client_id = MVCApp.Service.Constants.Common.clientId;
      pRequestParams.expand = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.expand");
      pRequestParams.locale=MVCApp.Toolbox.Service.getLocale();
      completeData = [];
      lastRecordIndex = 0;
      completeResponse = {};
      firstRecordIndex = 0;
      model.fetchPLPDataForImageSearch(pRequestParams,populateDataInPLPScreen);
      }
    }
  
    function populateDataInPLPScreen(pResults){
      frmProductsList.segProductsList.removeAll();
      imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.postDWResults"));
      if(pResults && pResults.hits && imageSearchProgressFlag){
        var productsListController = MVCApp.getProductsListController();
        kony.print(JSON.stringify(pResults));
        lProductsCount = pResults.hits.length;
        var lProductsData = MVCApp.data.ProductsList(pResults);
        productsListController.init();
        setStoreIdForImageSearchFlow();
        productsListController.setDataForProductList(lProductsData);
        completeData = pResults.hits;
        lastRecordIndex = completeData.length;
        productsListController.setFromImageSearch(true);
        productsListController.updateScreen(lProductsData,true,pResults.count);
        productsListController.updateProjectsCount(projectsFromImageSearch.length, true);
        productsListController.setProjectCount(projectsFromImageSearch.length);
        productsListController.setProjectTotalFromImageSearch(projectsFromImageSearch.length ||0);
        productsListController.setProductTotalFromImageSearch(lProductsData.getTotalResults()||0);
        frmProductsList.btnProducts.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnProduts") + " (" + productsFromImageSearch.length + ")";
        frmProductsList.flxHeaderWrap.textSearch.text = "";
        frmProductsList.flxRefineSearchWrap.isVisible = false;
        imageSearchSuccessFlag = true;
        imageSearchWaitFlag = false;
        if(MVCApp.getImageSearchController().returnToFormFlag){
          MVCApp.getImageSearchController().returnToFormFlag = false;
          imageSearchProgressFlag = false;
          productsListController.backToScreen();
        }
      }
    }
  
  	function setStoreIdForImageSearchFlow(){
      var storeID = "";
      if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
        storeID = gblInStoreModeDetails.storeID;
        MVCApp.getProductsListController().setStoreModeStatus(true);
      }else{
        if(MVCApp.Toolbox.Service.getMyLocationFlag()=="true"){
          var locDetailsStr = kony.store.getItem("myLocationDetails");
          try {
            var LocDetails = JSON.parse(locDetailsStr);
            storeID = LocDetails.clientkey;
          } catch(e) {
            MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlImageSearch.js on setStoreIdForImageSearchFlow for locDetailsStr:" + JSON.stringify(e)}]);
            storeID = "";
            if(e){
              TealiumManager.trackEvent("POI Parse Exception In setStoreIdForImageSearchFlow function", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
          }
        }else{
          storeID = "";
        }
      }
      MVCApp.getProductsListController().setStoreId(storeID);
    }
  
  	function fetchDataForPJLPScreen(showLoadingFlag){
      if(showLoadingFlag){
        MVCApp.Toolbox.common.showLoadingIndicator("");
      }
      var lRequestParams = {};
      lRequestParams.project_ids = lProjectIds;
      lRequestParams.product_count = lProductsCount;
      lPJLPServiceRequestObj.project_ids = lProjectIds;
      lPJLPServiceRequestObj.product_count = lProductsCount;
      getPJLPDataForImageSearch(lRequestParams);
    }
  
  	function getPJLPDataForImageSearch(pRequestParams){
      pRequestParams.client_id = MVCApp.Service.Constants.Common.clientId;
      pRequestParams.expand = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.expand");
      pRequestParams.store_id = getCurrentStoreId();
      pRequestParams.locale=MVCApp.Toolbox.Service.getLocale();
      model.fetchPJLPDataForImageSearch(pRequestParams,populateDataInPJLPScreen);
    }
  
  	function populateDataInPJLPScreen(pResults){
      if(pResults && pResults.hits){
        kony.print("LOG:populateDataInPJLPScreen-pResults:"+JSON.stringify(pResults));
        var projectsListController = MVCApp.getProjectsListController();
        imageSearchProgressAnimateDuration = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.progressAnimation.postDWResults"));
        var lProjectsData = MVCApp.data.ProjectsList(pResults);
        projectsListController.init();
        MVCApp.getSortProjectController().setDataForProjList(lProjectsData);
        frmProjectsList.flxHeaderWrap.textSearch.text = "";
        projectsListController.updateProductCount(productsFromImageSearch.length);
        projectsListController.setProductsCountFromImageSearch(productsFromImageSearch.length||0);
        projectsListController.setProjectsCountFromImageSearch(lProjectsData.getTotalResults()||0);
        frmProjectsList.btnProjects.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnProjects") + " (" + projectsFromImageSearch.length + ")";
        projectsListController.setFromImageSearch(true);
        MVCApp.getProjectsListController().setEntryType("imageSearch");
        MVCApp.getProjectsListController().updateScreen(lProjectsData,true);
        projectsListController.setFromImageSearch(true);
        imageSearchSuccessFlag = true;
        imageSearchWaitFlag = false;
        if (MVCApp.getImageSearchController().returnToFormFlag) {
          MVCApp.getImageSearchController().returnToFormFlag = false;
          imageSearchProgressFlag = false;
          projectsListController.showUI();
        }
      }else{
         MVCApp.Toolbox.common.dismissLoadingIndicator();
      }
    }
  
  	function getCurrentStoreId(){
      var lStore_id = "";
      if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
        lStore_id = gblInStoreModeDetails.storeID;
      }else{
        var lFavStoreString = kony.store.getItem("myLocationDetails") || "";
        if(lFavStoreString !== ""){
          try {
            var lFavStoreObj = JSON.parse(lFavStoreString);
            lStore_id = lFavStoreObj.clientkey;
          } catch(e) {
            if(e){
              TealiumManager.trackEvent("POI Parse Exception in Image Search Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
          }
        }
      }
      return lStore_id;
    }
  
  	function getCountryForSlyce(){
    	var langReg=kony.store.getItem("languageSelected");
    	if(langReg=="en"){
      		return "us";
    	}else if(langReg=="en_CA" || langReg=="fr_CA"){
      		return "ca";
    	}
  	}
  
  	function onClickImageSearchErrorTryAgain(){
      if(imageSearchInvokedForm && cameraWidgetRef){
        onClickImageSearchBack();
        cameraWidgetRef.openCamera();
      }
    }
  
  	function onTouchEndImageSearchCamera(){
      //#ifdef iphone
      if(!redirectToSettings.checkCamAuthorizationStatus()){
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.CustomCameraUsageDescription"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.CustomCameraUsageTitle"),constants.ALERT_TYPE_CONFIRMATION,getCameraPopup,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.DontAllow"));
      }
      //#endif
    }
  
  	function getCameraPopup(pResponse){
      if(pResponse){
        redirectToSettings.redirectUserToCameraSettings();
      }
    }
  
  	function onClickImageSearchBack(){
      if(!_isInit){
        init();
      }
      view.onClickImageSearchBack();
    }
  	function setTeliumDataForImageSearch(){
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "";
      lTealiumTagObj.page_name = "";
      lTealiumTagObj.page_type = "shop category";
      lTealiumTagObj.page_category_name = "";
      lTealiumTagObj.page_category = "";
      lTealiumTagObj.page_subcategory_name = "";
      lTealiumTagObj.search_term = "";
      lTealiumTagObj.search_results = 0;
      lTealiumTagObj.product_image_search_results = 0;
      lTealiumTagObj.project_image_search_results = 0;
      lTealiumTagObj.search_status = "unsuccessful";
      TealiumManager.trackView("Shop Tier 3 Category Page", lTealiumTagObj);
    }
  
  	return {
      init: init,
      load: load,
      didCaptureImage: didCaptureImage,
      onTouchEndImageSearchCamera: onTouchEndImageSearchCamera,
      onClickImageSearchBack: onClickImageSearchBack,
      showProjectsListFromImageSearch: showProjectsListFromImageSearch,
      getPLPDataForImageSearch:getPLPDataForImageSearch,
      lPLPServiceRequestObj:lPLPServiceRequestObj,
      lPJLPServiceRequestObj:lPJLPServiceRequestObj,
      getPJLPDataForImageSearch:getPJLPDataForImageSearch,
      returnToFormFlag : returnToFormFlag,
      showProductsListFromImageSearch: showProductsListFromImageSearch
    };
});
