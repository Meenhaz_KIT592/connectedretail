/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
addressflag = false;
MVCApp.RewardsProfileAdrsView = (function(){
 var addressData= {};
  /**
	 * PUBLIC
	 * Open the Events form
	 */
  
  function show(nxtFlag){
    setLocaleData();
    frmRewardsProfileStep2.txtBxHide.setFocus(true);
    frmRewardsProfileStep2.txtBxHide.setEnabled(true);
    frmRewardsProfileStep2.txtAddress1.setFocus(false);
    if(nxtFlag == true){
      frmRewardsProfileStep2.btnReset.isVisible = false;
      frmRewardsProfileStep2.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
    }else{
      frmRewardsProfileStep2.btnReset.isVisible = true;
      frmRewardsProfileStep2.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
    }
    frmRewardsProfileStep2.show();    
  } 
  
  
  function setLocaleData()
	{
		frmRewardsProfileStep2.lblFormTitle.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.rewardsprofile");
        frmRewardsProfileStep2.Label0c5601159c0bb4c.text=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.step2");
        frmRewardsProfileStep2.txtAddress1.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.address1");
        frmRewardsProfileStep2.txtAddress2.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.address2");
        frmRewardsProfileStep2.txtCity.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.city");
        frmRewardsProfileStep2.txtZipCan.placeholder=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.zip");
        frmRewardsProfileStep2.btnReset.text=MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.exitAndFinishLater");
	}
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
  
  function clearScreen(){
    
    frmRewardsProfileStep2.txtAddress1.text = "";
    frmRewardsProfileStep2.txtAddress2.text = "";
    frmRewardsProfileStep2.txtCity.text = "";
    frmRewardsProfileStep2.txtZip.text = "";   
     frmRewardsProfileStep2.txtZipCan.text = "";   
    
  }
  
  function onClickBtnResetRewardsProfileStep2(){
    rewardsProfileData.address1 = frmRewardsProfileStep2.txtAddress1.text;
    rewardsProfileData.address2 = frmRewardsProfileStep2.txtAddress2.text;
    rewardsProfileData.city = frmRewardsProfileStep2.txtCity.text;
    if(MVCApp.Toolbox.common.getRegion()=="US"){
    rewardsProfileData.zip = frmRewardsProfileStep2.txtZip.text;
    }else
      {
         rewardsProfileData.zip = frmRewardsProfileStep2.txtZipCan.text;
      }
    rewardsProfileData.state = frmRewardsProfileStep2.lboxState.selectedKeyValue[0];
    
    if(frmRewardsProfileStep2.btnNext.skin == "sknBtnRed"){
      if(step2Validations()){
        MVCApp.getReviewProfileController().updateRewardProfile("step2");
      }
    }else{
      frmRewardsProfileStep2.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.rewardsProfile.exitFinish");
      frmRewardsProfileStep2.lbl1.isVisible = false;
      frmRewardsProfileStep2.flxOverlay.isVisible = true;
    }
  }
  
  function navigateToMoreScreen(){
    frmRewardsProfileStep2.flxOverlay.isVisible = false;
    MVCApp.getMoreController().load();
  }
  
  function dummyOverlay(){
    kony.print("LOG:dummyOverlay-viewRewardsProfileAdrs.js-START");
  }
  
  function bindEvents(){
    frmRewardsProfileStep2.postShow = function(){
      var lTealiumTagObj = gblTealiumTagObj;
      lTealiumTagObj.page_id = "Rewards Profile Creation: Step 2: Michaels Mobile App";
      lTealiumTagObj.page_name = "Rewards Profile Creation: Step 2: Michaels Mobile App";
      lTealiumTagObj.page_type = "Rewards";
      lTealiumTagObj.page_category_name = "Rewards";
      lTealiumTagObj.page_category = "Rewards";
      TealiumManager.trackView("Rewards Profile Creation Step 2 Screen", lTealiumTagObj);
    };
    frmRewardsProfileStep2.btnYes.onClick = navigateToMoreScreen;
    frmRewardsProfileStep2.flxOverlay.onTouchEnd = dummyOverlay;
    
     if(MVCApp.Toolbox.common.getRegion()=="US"){
   
     frmRewardsProfileStep2.flxZip.setVisibility(true);
         frmRewardsProfileStep2.flxZipCan.setVisibility(false);
    }else if(MVCApp.Toolbox.common.getRegion()=="CA"){
        frmRewardsProfileStep2.flxZipCan.setVisibility(true);
       frmRewardsProfileStep2.flxZip.setVisibility(false);
       }
    
     //#ifdef iphone    
    frmRewardsProfileStep2.lboxState.width = "98.5%";   
    frmRewardsProfileStep2.lboxState.padding =[0,3,0,0];    
    frmRewardsProfileStep2.lboxState.centerY = "40%";
    
    //#endif
    //#ifdef android
    frmRewardsProfileStep2.lboxState.left = "3%";
    frmRewardsProfileStep2.lboxState.width = "94%";   
    frmRewardsProfileStep2.lboxState.padding =[0,0,0,0];
    //#endif
    frmRewardsProfileStep2.btnHeaderLeft.onClick = showPreviousScreen;
    frmRewardsProfileStep2.btnReset.onClick = onClickBtnResetRewardsProfileStep2;//resetValues;
    frmRewardsProfileStep2.btnNext.onClick = rewardsProfileChild;  
    MVCApp.tabbar.bindEvents(frmRewardsProfileStep2);
	MVCApp.tabbar.bindIcons(frmRewardsProfileStep2,"frmMore");
    frmRewardsProfileStep2.preShow=onTextChange;
    frmRewardsProfileStep2.txtAddress1.onTextChange = function(){
      var text = frmRewardsProfileStep2.txtAddress1.text;
      
      if(text.length > 0){
       //#ifdef android
        frmRewardsProfileStep2.btnClearAddress1.setVisibility(true);
        //#endif
        frmRewardsProfileStep2.btnNext.skin = "sknBtnRed";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnRed";
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep2.btnClearAddress1.setVisibility(false);
       //#endif
        frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      }
      
    }; 
    frmRewardsProfileStep2.txtAddress2.onTextChange = function(){
      var text = frmRewardsProfileStep2.txtAddress2.text;
      
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep2.btnClearAddress2.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep2.btnClearAddress2.setVisibility(false);
        //#endif
        frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      }
     
    }; 
    frmRewardsProfileStep2.txtCity.onTextChange = function(){
      var text = frmRewardsProfileStep2.txtCity.text;
     
      if(text.length > 0){
       //#ifdef android
        frmRewardsProfileStep2.btnClearCity.setVisibility(true);
       //#endif
       		onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep2.btnClearCity.setVisibility(false);
        //#endif
        frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      }
      
    }; 
    frmRewardsProfileStep2.txtZip.onTextChange = function(){
      var text = frmRewardsProfileStep2.txtZip.text;
      
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep2.btnClearZip.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep2.btnClearZip.setVisibility(false);
        //#endif
        frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      }
      
    };
    
    frmRewardsProfileStep2.txtZipCan.onTextChange = function(){
      var text = frmRewardsProfileStep2.txtZipCan.text;
      
      if(text.length > 0){
        //#ifdef android
        frmRewardsProfileStep2.btnClearZipCan.setVisibility(true);
        //#endif
        onTextChange();
      }else{
        //#ifdef android
        frmRewardsProfileStep2.btnClearZipCan.setVisibility(false);
        //#endif
        frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      }
      
    };
     frmRewardsProfileStep2.lboxState.onSelection = onTextChange;
    
    //#ifdef android
      frmRewardsProfileStep2.btnClearAddress1.onClick = function(){
      frmRewardsProfileStep2.txtAddress1.text ="";
      frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
         frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
        
      frmRewardsProfileStep2.btnClearAddress1.setVisibility(false);
        
    };
    frmRewardsProfileStep2.btnClearAddress2.onClick = function(){
      frmRewardsProfileStep2.txtAddress2.text ="";
     
      
      frmRewardsProfileStep2.btnClearAddress2.setVisibility(false); 
     
    };
    frmRewardsProfileStep2.btnClearCity.onClick = function(){
      frmRewardsProfileStep2.txtCity.text ="";
      frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      
      frmRewardsProfileStep2.btnClearCity.setVisibility(false); 
      
    };
    frmRewardsProfileStep2.btnClearZip.onClick = function(){
      frmRewardsProfileStep2.txtZip.text ="";
      frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      
      frmRewardsProfileStep2.btnClearZip.setVisibility(false); 
      
    };  
    frmRewardsProfileStep2.btnClearZipCan.onClick = function(){
      frmRewardsProfileStep2.txtZipCan.text ="";
      frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
       frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      
      frmRewardsProfileStep2.btnClearZipCan.setVisibility(false); 
      
    };   
    //#endif
	
  }
  
  function step2Validations(){
     var addr1 = frmRewardsProfileStep2.txtAddress1.text;
     var addr2 = frmRewardsProfileStep2.txtAddress2.text;
     var city = frmRewardsProfileStep2.txtCity.text;
   //  var zipcode = frmRewardsProfileStep2.txtZip.text;
      var zipcode;
     var errFlag = false;
    var errMsg = [];
    var regExAddr = /^[a-zA-Z0-9+\s/.\-/,//]+$/;
     var regCity = /^[a-zA-Z-_\s]+$/;
     var zipCodeRegEx ="";
    var len=5;
    
     if(MVCApp.Toolbox.common.getRegion()=="US"){
      // zipcode = frmCreateAccount.txtZipCode.text;
       zipcode = frmRewardsProfileStep2.txtZip.text;
       zipCodeRegEx = /^[0-9-]+$/;
       len=5;
    }else if(MVCApp.Toolbox.common.getRegion()=="CA"){
     // zipcode = frmCreateAccount.txtZipCodeCan.text; 
        zipcode = frmRewardsProfileStep2.txtZipCan.text;
        zipCodeRegEx = /^[0-9a-zA-Z]+$/;
      len=6;
    }
    
    
     if(addr1 === ""|| addr1 === null){
       errMsg.push("address1");
        errFlag = true;
       }
    //removed validations for address1 & address2
  /*  else{
         if(!regExAddr.test(addr1.trim())){
            alert("Please enter proper address for address1 field");
            return false;
         }
       }
       
      if(addr2 !== "" &&  addr2 !== null){
         if(!regExAddr.test(addr2)){
            alert("Please enter proper address for address2 field");
           return false;
         }
      }*/
       //end
       if(city === ""|| city === null){
       errMsg.push("city name");
       errFlag = true;
       }else{
         if(!regCity.test(city.trim())){
            alert("Please enter city name properly");
            return false;
         }
       }
    
     if(zipcode === ""|| zipcode === null){
       errMsg.push("zipcode ");
       errFlag = true;
       }else  if(zipcode.trim().length == len){
          if(!zipCodeRegEx.test(zipcode.trim())){
            alert("please enter valid zipcode");
            return false;
          }      
        }else{
          alert("please enter valid zipcode");
            return false;
      }
    var state = frmRewardsProfileStep2.lboxState.selectedKeyValue;
    state = state[1];
	if(state == "STATE"){
      frmRewardsProfileStep2.btnNext.skin = "sknBtnDisable";
       frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      errMsg.push("State");
      errFlag = true;
    }else{
      frmRewardsProfileStep2.btnNext.skin = "sknBtnRed";
       frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnRed";
    }
    
    if(errFlag === true){
      var result = "";
      for(var i=0;i<errMsg.length;i++){
        
        if(i==0){
           result = result+errMsg[i];
        }else{
          if(i>0 && (i == (errMsg.length -1))){
            result = result+","+errMsg[i];
          }else{
            result = result+", "+errMsg[i];
          } 
        }
      }
   MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.plsEnter")+" "+result+" "+
           MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.toContinue"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
      return false;
    }else{
    
    
    return true;
    }
   } 
  
  function onTextChange(){
  
    var text1 = frmRewardsProfileStep2.txtAddress1.text || "";
    var text2 = frmRewardsProfileStep2.txtAddress2.text || "";
    var text3 = frmRewardsProfileStep2.txtCity.text || "";
    var text4="";
    if(frmRewardsProfileStep2.flxZip.isVisible)
     text4 = frmRewardsProfileStep2.txtZip.text || "";
    else if(frmRewardsProfileStep2.flxZipCan.isVisible)
       text4 = frmRewardsProfileStep2.txtZipCan.text || "";
    var text5 = frmRewardsProfileStep2.lboxState.selectedKey;
    if((text1.length > 0) && (text3.length > 0) && (text4.length > 0) && (text5 !=="state")){
           frmRewardsProfileStep2.btnNext.skin = "sknBtnRed";
      frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnRed";
      }else{
        frmRewardsProfileStep2.btnNext.skin = "sknBtnDisable";
        frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
      }
        
  }
  
  function getStateData(){
    
    if(MVCApp.Toolbox.common.getRegion()=="US"){ 
    
    return [[MVCApp.Toolbox.common.geti18nValueVA("state.us.state.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.state")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.alabama.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.alabama")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.alaska.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.alaska")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.arizona.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.arizona")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.arkansas.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.arkansas")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.california.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.california")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.colorado.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.colorado")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.connecticut.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.connecticut")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.delaware.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.delaware")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.dc.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.dc")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.florida.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.florida")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.georgia.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.georgia")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.hawaii.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.hawaii")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.idaho.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.idaho")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.illinois.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.illinois")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.indiana.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.indiana")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.iowa.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.iowa")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.kansas.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.kansas")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.kentucky.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.kentucky")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.louisiana.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.louisiana")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.maine.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.maine")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.maryland.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.maryland")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.massachusetts.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.massachusetts")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.michigan.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.michigan")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.minnesota.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.minnesota")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.mississippi.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.mississippi")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.missouri.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.missouri")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.montana.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.montana")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.nebraska.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.nebraska")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.nevada.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.nevada")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.newhampshire.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.newhampshire")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.newjersey.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.newjersey")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.newmexico.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.newmexico")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.newyork.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.newyork")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.northcarolina.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.northcarolina")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.northdakota.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.northdakota")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.ohio.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.ohio")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.oklahoma.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.oklahoma")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.oregon.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.oregon")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.pennsylvania.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.pennsylvania")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.rhodeisland.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.rhodeisland")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.southcarolina.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.southcarolina")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.southdakota.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.southdakota")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.tennessee.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.tennessee")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.texas.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.texas")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.utah.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.utah")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.vermont.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.vermont")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.virginia.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.virginia")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.washington.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.washington")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.westvirginia.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.westvirginia")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.wisconsin.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.wisconsin")],
[MVCApp.Toolbox.common.geti18nValueVA("state.us.wyoming.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.wyoming")]];
    }
    else
      {
       return  [
         [MVCApp.Toolbox.common.geti18nValueVA("state.us.state.key"),MVCApp.Toolbox.common.geti18nValueVA("state.us.state")],
         ["BC","British Columbia"],
		 ["MB","Manitoba"],
		 ["QC","Quebec"],
		 ["SK","Saskatchewan"],
         ["AB","Alberta"],
         ["NB","New Brunswick"],
         ["NL","Newfoundland"],
         ["NS","Nova Scotia"],
         ["ON","Ontario"],
         ["PE","Prince Edward Island"]
         
		];
      }
  }
  
  function updateScreen(){
    frmRewardsProfileStep2.lboxState.masterData = getStateData();
    if (MVCApp.Toolbox.common.getRegion() == "US") {
            frmRewardsProfileStep2.flxZip.setVisibility(true);
            frmRewardsProfileStep2.flxZipCan.setVisibility(false);
        } else if (MVCApp.Toolbox.common.getRegion() == "CA") {
            frmRewardsProfileStep2.flxZipCan.setVisibility(true);
            frmRewardsProfileStep2.flxZip.setVisibility(false);
        }
      addressData = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
     var addressDetails =  addressData.getAddressDetails();
    var stateFlag = false; 
    if(rewardsProfileData.isAddressExists)
      {
        frmRewardsProfileStep2.txtAddress1.text  = addressDetails.address1;
        var addr2 = addressDetails.address2 || "";
        if(addr2=="" || addr2==null || addr2=="null"){
          addr2 ="";
        }
        frmRewardsProfileStep2.txtAddress2.text  = addr2;
        frmRewardsProfileStep2.txtCity.text  = addressDetails.city;
        if (MVCApp.Toolbox.common.getRegion() == "US")
				frmRewardsProfileStep2.txtZip.text = addressDetails.postal_code;
			else
				 if (MVCApp.Toolbox.common.getRegion() == "CA")
				frmRewardsProfileStep2.txtZipCan.text = addressDetails.postal_code;
        
        
        
        
        if(addressDetails.state_code!="" && addressDetails.state_code != undefined && addressDetails.state_code != null){
        var stateKeys = frmRewardsProfileStep2.lboxState.masterData;         
        for(var i=0;i<stateKeys.length;i++){
          if(stateKeys[i][0] ==addressDetails.state_code){
          	frmRewardsProfileStep2.lboxState.selectedKey = addressDetails.state_code;
            stateFlag = true;                        
          }
        }
        }
      }
    else
      {
         addressData. getStep2Data();
      }    
    if( stateFlag === false || ( addressDetails !== undefined && (addressDetails !== null  && addressDetails.state_code === undefined)) || addressDetails.state_code === "" )
    frmRewardsProfileStep2.lboxState.selectedKey = MVCApp.Toolbox.common.geti18nValueVA("state.us.state.key");
    

    
    show(false);
  }
  
  function showPreviousScreen(){        
	var formId = kony.application.getPreviousForm().id;    
    if(formId == "frmReviewProfile")
      MVCApp.getReviewProfileController().load();
    else
    MVCApp.getRewardsProfileController().updateData();
  }
  
  function resetValues(){
    frmRewardsProfileStep2.txtAddress1.text = "";
    frmRewardsProfileStep2.txtAddress2.text = "";
    frmRewardsProfileStep2.txtCity.text = "";
    frmRewardsProfileStep2.txtZip.text = "";
    frmRewardsProfileStep2.lboxState.selectedKey = MVCApp.Toolbox.common.geti18nValueVA("state.us.state.key");
    frmRewardsProfileStep2.btnNext.skin="sknBtnDisable";
     frmRewardsProfileStep2.btnNext.focusSkin = "sknBtnDisable";
       //#ifdef android
      frmRewardsProfileStep2.btnClearAddress1.setVisibility(false); 
      frmRewardsProfileStep2.btnClearAddress2.setVisibility(false); 
      frmRewardsProfileStep2.btnClearCity.setVisibility(false); 
      frmRewardsProfileStep2.btnClearZip.setVisibility(false);
    //#endif
    var formId = kony.application.getPreviousForm().id;    
    if(formId == "frmReviewProfile")
    show(true);
      else
    show(false);
  }
  
  function rewardsProfileChild(){    
    rewardsProfileData.address1 =  frmRewardsProfileStep2.txtAddress1.text;
    rewardsProfileData.address2 =  frmRewardsProfileStep2.txtAddress2.text;
    rewardsProfileData.city =  frmRewardsProfileStep2.txtCity.text;
    rewardsProfileData.zip =  MVCApp.Toolbox.common.getRegion()=="US"?frmRewardsProfileStep2.txtZip.text:frmRewardsProfileStep2.txtZipCan.text;
    var state = frmRewardsProfileStep2.lboxState.selectedKeyValue;
    rewardsProfileData.state = state[0];
     var formId = kony.application.getPreviousForm().id;
     if(frmRewardsProfileStep2.btnNext.skin=="sknBtnRed"){
       if(step2Validations()){
       if(MVCApp.Toolbox.common.getRegion() == "CA"){
         			updateFlag = true;
					MVCApp.getReviewProfileController().load();
				}else{
				
						if (formId == "frmReviewProfile") {
							updateFlag = true;
							MVCApp.getReviewProfileController().load();
						} else if (Childflg === true) MVCApp.getRewardsProfileChildController().updateData();
						else {
							addressflag = true;
							MVCApp.getRewardsProfileChildController().load();
						}
				}
       } 
    }
    
    
  }
    
  function updateData(nxtFlag){
    frmRewardsProfileStep2.lboxState.masterData = getStateData();
    if (MVCApp.Toolbox.common.getRegion() == "US") {
            frmRewardsProfileStep2.flxZip.setVisibility(true);
            frmRewardsProfileStep2.flxZipCan.setVisibility(false);
        } else if (MVCApp.Toolbox.common.getRegion() == "CA") {
            frmRewardsProfileStep2.flxZipCan.setVisibility(true);
            frmRewardsProfileStep2.flxZip.setVisibility(false);
        }
    if(rewardsProfileData.address1 === undefined || rewardsProfileData.address1===""){
      updateScreen();
    }
    else{
    if(rewardsProfileData.address1 !=null)
      frmRewardsProfileStep2.txtAddress1.text = rewardsProfileData.address1
    else 
      frmRewardsProfileStep2.txtAddress1.text = "";
    
    if(rewardsProfileData.address2 !=null)
      frmRewardsProfileStep2.txtAddress2.text = rewardsProfileData.address2
    else 
      frmRewardsProfileStep2.txtAddress2.text = "";
    
    if(rewardsProfileData.city !=null)
      frmRewardsProfileStep2.txtCity.text = rewardsProfileData.city
    else 
      frmRewardsProfileStep2.txtCity.text = "";
    if(rewardsProfileData.state !=null)
      frmRewardsProfileStep2.lboxState.selectedKey = rewardsProfileData.state
    else 
      frmRewardsProfileStep2.lboxState.selectedKey = MVCApp.Toolbox.common.geti18nValueVA("state.us.state.key");   
   if (rewardsProfileData.zip !== null && MVCApp.Toolbox.common.getRegion() == "US") 
				frmRewardsProfileStep2.txtZip.text = rewardsProfileData.zip;
       else  if (rewardsProfileData.zip != null && MVCApp.Toolbox.common.getRegion() == "CA") 
				frmRewardsProfileStep2.txtZipCan.text = rewardsProfileData.zip;
		else
				frmRewardsProfileStep2.txtZip.text = "";
    }
    show(nxtFlag);
  }
  
  //Here we expose the public variables and functions
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	updateData:updateData,
    	clearScreen:clearScreen
    };
  
});