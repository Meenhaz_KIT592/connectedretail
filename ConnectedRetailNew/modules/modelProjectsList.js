/**
 * PUBLIC
 * This is the model for formProjectsList form
 */
var MVCApp = MVCApp || {};
MVCApp.ProjectsListModel = (function(){
 var serviceType = "";
  var operationId = "";
  function loadData(callback,requestParams){
    
    kony.print("modelProjectsList.js");
   /* MVCApp.makeServiceCall( serviceType,operationId,requestParams,
                           function(results)
                           {
      kony.print("Response is "+JSON.stringify(results)); 
      callback(results,requestParams);
      
    },function(error){
    kony.print("Response is "+JSON.stringify(error));
    }
                          );*/
    callback("");
  }
  function showSearchResults(successCallBack,requestParams,serviceName,operation,fromSearch,count){
    kony.print("in search results "+JSON.stringify(requestParams));
    MakeServiceCall(serviceName, operation, requestParams, function(results){
      //kony.print("response   "+JSON.stringify(results));
      var dataProjects=MVCApp.data.ProjectsList(results);
      var totalProjects = dataProjects.getTotalResults();
      if(totalProjects == 0){
        if(voiceSearchFlag){
          sorryPageFlag =true;
          var queryString=MVCApp.getProjectsListController().getQueryStringForSearch();
          frmProductsList.flxVoiceSearch.lblUtterenace2.text = queryString;
          frmProductsList.flxVoiceSearch.lblUtterence1.setVisibility(false);
          frmProductsList.flxVoiceSearch.lblUtterance3.setVisibility(false);
          frmProductsList.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
          frmProductsList.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noProjectsFound");
          frmProductsList.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
          frmProductsList.flxVoiceSearch.setVisibility(true);
          var projectIds=[];
          MVCApp.getProjectsListController().setProjectIds(projectIds);
          MVCApp.Customer360.callVoiceSearchResult("failure", "No Projects Found","");
          MVCApp.Toolbox.common.dismissLoadingIndicator();
          MVCApp.getProductsListController().init();
          frmProductsList.show();
        }else{
          sorryPageFlag = false;
      	  MVCApp.getSortProjectController().setDataForProjList(dataProjects);
      	  successCallBack(dataProjects,fromSearch,count);
        }
      }else{
        sorryPageFlag = false;
      	MVCApp.getSortProjectController().setDataForProjList(dataProjects);
      	successCallBack(dataProjects,fromSearch,count);
      }
    },function(error){
      MVCApp.Toolbox.common.dismissLoadingIndicator();
      kony.print("Error is "+JSON.stringify(error));
    });
  }
  
  function refreshForSortRefine(successCallBack,requestParams,serviceName,operation,fromSearch,count){
    kony.print("in search results "+JSON.stringify(requestParams));
    MakeServiceCall(serviceName, operation, requestParams, function(results){
      //kony.print("response   "+JSON.stringify(results));
      var dataProjects=MVCApp.data.ProjectsList(results);
      var dataForSeg=dataProjects.getDataForProjects() || [];
      successCallBack(dataProjects);
    },function(error){
      MVCApp.Toolbox.common.dismissLoadingIndicator();
      kony.print("Error is "+JSON.stringify(error));
    });
  }
  
 	return{
      loadData:loadData,
      showSearchResults:showSearchResults,
      refreshForSortRefine:refreshForSortRefine
    };
});
