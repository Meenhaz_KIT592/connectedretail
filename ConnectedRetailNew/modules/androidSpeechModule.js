//Type your code here
function createSingletonsAndroid(){
  createVoiceAndSpeechObjectsFlag = true;
  KonyMain = java.import('com.konylabs.android.KonyMain');
  RecognizerIntent = java.import('android.speech.RecognizerIntent');
  speechRecognizer = java.import("android.speech.SpeechRecognizer");  
  Intent = java.import('android.content.Intent');
  IntentFilter = java.import('android.content.IntentFilter');
  Locale = java.import('java.util.Locale');
  Uri = java.import('android.net.Uri');
  MediaPlayer = java.import('android.media.MediaPlayer');
  AudioManager = java.import('android.media.AudioManager');
  AudioAttributes = java.import('android.media.AudioAttributes');
  manifest = java.import("android.Manifest");
  packageManager = java.import("android.content.pm.PackageManager");
  activityCompact = java.import("android.support.v4.app.ActivityCompat");
  contextCompat = java.import("android.support.v4.content.ContextCompat");
	permissions= java.import("com.michaels.settinglib.Permissions");
	permissionWrapper= java.import("com.michaels.settinglib.PermissionWrapper");
  context = KonyMain.getActivityContext();
 
  recognitionListener = java.newClass('recognitionListener', 'java.lang.Object', ['android.speech.RecognitionListener'], {
                onReadyForSpeech: function(bundle){
                 kony.print("Ready for speech");
                 mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mStreamVolume, 0); 
                } ,
                onBeginningOfSpeech: function(){
                  kony.print("Begin of speech"); 
                },
                onRmsChanged: function(v){},
                onPartialResults: function(bundle) {
                  kony.print("Partial results");
                  partialResultCallback(bundle.getStringArrayList(speechRecognizer.RESULTS_RECOGNITION).get(0));
                },
                onResults: function(bundle) {
                  kony.print("On results");
                  partialResultCallback(bundle.getStringArrayList(speechRecognizer.RESULTS_RECOGNITION).get(0));
                  stopRecognition();
                  finalCallback(bundle.getStringArrayList(speechRecognizer.RESULTS_RECOGNITION).get(0));
                },
        		onError: function(code) {
            	kony.print("On Error" + code);
          		if(code == 7 ){
                 if(voiceSearchFlag === true){
                   if (requestPermission()) {
                       mSpeechRecognizer = null;
                       kony.runOnMainThread(startListening(), []);
                       kony.print("Started listening..." );
                   } 
                 } 
                } else if (code == 6 ){
					restartRecognition();	
				}
                },
                onBufferReceived: function(bytes) {
                  kony.print("On Buffer Received");
                },
                onEndOfSpeech: function() {
                  kony.print("On End of speech");
                },
                onEvent: function(i, bundle) {
                  kony.print("On Event");
                }
            });
} 


 
function playSoundAndroid(filename) {
  try{
  	kony.print("**** in play sound");
  	kony.print("****Media Player creation ***"+ context.getPackageName());
  	var strURl = "android.resource://"+context.getPackageName()+"/raw/"+filename;
    var url = Uri.parse(strURl);
    if(filename === "ding"){
      var  MediaCompletionListner = java.newClass('MediaCompletionListener', 'java.lang.Object', ['android.media.MediaPlayer$OnCompletionListener'], {
    onCompletion: function(mediaPlayer) {
        kony.print("Media On complete Result Called" );
        mediaPlayer.stop();
       	mediaPlayer.reset();
      	if (requestPermission()) {
           mSpeechRecognizer = null;
           kony.print("Media On complete Result Called 1" );
           kony.runOnMainThread(startListening(), []);
           kony.print("Media On complete Result Called 2" );
       }
    }
	});
      if(typeof(mediaPlayer1) === "undefined"){
      	mediaPlayer1 =  new MediaPlayer(); 
      } 
      var attributes = new AudioAttributes.Builder();
      attributes .setUsage(AudioAttributes.USAGE_MEDIA);
      attributes.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
      mediaPlayer1.setAudioAttributes(attributes.build());
      kony.print("****Media Player creation2 ***"+ mediaPlayer1);
      //mediaPlayer1.setAudioStreamType(AudioManager.STREAM_MUSIC);
      mediaPlayer1.setDataSource(context,url);
	  mediaPlayer1.prepare();// might take long! (for buffering, etc)
      kony.print("****Media Player creation3 ***"+ mediaPlayer1);
	  mediaPlayer1.start();
  	  kony.print("Starting playing ding");
      mediaPlayer1.setOnCompletionListener(new MediaCompletionListner());
      
    } else {
      var  mediaCompletionListner1 = java.newClass('mediaCompletionListener1', 'java.lang.Object', ['android.media.MediaPlayer$OnCompletionListener'], {
    onCompletion: function(mediaPlayer) {
        kony.print("Media On complete Result Called" );
        mediaPlayer.stop();
      	mediaPlayer.reset();
    }
	});
      if(typeof(mediaPlayer2) === "undefined"){
      mediaPlayer2 =  new MediaPlayer();
      }  
      var attributesOther = new AudioAttributes.Builder();
      attributesOther .setUsage(AudioAttributes.USAGE_MEDIA);
      attributesOther.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
      mediaPlayer2.setAudioAttributes(attributesOther.build());
	  mediaPlayer2.setDataSource(context,url);
	  mediaPlayer2.prepare(); // might take long! (for buffering, etc)
	  mediaPlayer2.start();
  	  kony.print("Starting playing dong");
      mediaPlayer2.setOnCompletionListener(new mediaCompletionListner1());
      //stopRecognition();
    }
  } catch (e){
    kony.print("In exception"+JSON.stringify(e));
  }
}



function startListening() {
        try {
            kony.print("----------Entering startListening Function---------");
			var locale = MVCApp.Toolbox.Service.getLocale();
          	locale  = locale == "default" ? "en-US" : locale;
          	bCancel = false;
         if (mSpeechRecognizer != null) {
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
        }
            mSpeechRecognizer = speechRecognizer.createSpeechRecognizer(context);
            mAudioManager = context.getSystemService("audio");
            mStreamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC); // getting system volume into var for later un-muting   
          	mSpeechRecognizer.setRecognitionListener(new recognitionListener());
            var voiceintent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            voiceintent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
            voiceintent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
            voiceintent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, locale);
            voiceintent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            voiceintent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getBasePackageName());
            mSpeechRecognizer.startListening(voiceintent);
          	mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            kony.print("----------Exiting startListening Function---------");
        } catch (exception) {
            kony.print(JSON.stringify(exception));
        }
    }

function stopRecognition() {
        try {
            kony.print("----------Entering stopRecognition Function---------");
            kony.runOnMainThread(function() {
                mSpeechRecognizer.stopListening();
              	mSpeechRecognizer.cancel();
              	mSpeechRecognizer.destroy();
            }, "");
            kony.print("----------Exiting stopRecognition Function---------");
        } catch (exception) {
            kony.print(JSON.stringify(exception));
        }
    }

function restartRecognition() {
    try {
        kony.print("----------Entering stopRecognition Function---------");
        kony.runOnMainThread(function() {
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
          frmObject.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
        }, "");
        kony.print("----------Exiting stopRecognition Function---------");
    } catch (exception) {
        kony.print(JSON.stringify(exception));
    }
}

function requestPermission() {
  		kony.print("----------Entering requestPermission Function---------");
        try {
            if (contextCompat.checkSelfPermission(context, manifest.permission.RECORD_AUDIO) !== packageManager.PERMISSION_GRANTED) {
              kony.print("----------Entering requestPermission Function---------");  
  			 return false;
            }
          	kony.print("----------Entering requestPermission Function---------");
            return true;
        } catch (exception) {
          
            kony.print(JSON.stringify(exception));
           
        }
    }


