
/**
 * PUBLIC
 * This is the controller for the Reviews form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ReviewsController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction  
  var reviewTitle;
  var reviewText = "";
  var fromPDPFlag = true;
  var productIdTracking="";
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.ReviewsModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.ReviewsView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
    var _params=[];
    var _ovrAlRtngs;
    var requestParams = [];
	function getRequestParams(){
      return _params;
    }
  	function _setRequestParams(reqParams){
      _params=reqParams;
    }
    function _setOverAllRatings(ratngParams){
      _ovrAlRtngs=ratngParams;
	}
	function _getOverAllRatings(){
      return _ovrAlRtngs;
	}
  	function load(id,name,reviewTextFromPDP){
      reviewText = reviewTextFromPDP;
      fromPDPFlag = true;
//           var id = "M10014390";
//           var name = "KONY";
        var flag = "1";
        MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("Reviews Controller.load");
      	init();
        requestParams = MVCApp.Service.getCommonReviewsInputParamaters();
      	var locale=kony.store.getItem("languageSelected");
      	if(locale=="en" || locale=="en_US" || locale == "en_CA"){
          locale="en_US";
        }
        requestParams.limit = MVCApp.Service.Constants.Reviews.limit;
        requestParams.offset = MVCApp.Service.Constants.Reviews.offset;
        requestParams.sort = MVCApp.Service.Constants.Reviews.sort;
        requestParams.filter = "ProductID:"+id;
        requestParams.name = name;
      	reviewTitle = name;
      	requestParams.localeReviews=locale;
        _setRequestParams(requestParams);        
        model.getReviews(view.updateScreen,requestParams,name,flag);
      	setProductIDForReviewsTracking(id);
		         
    }
  	function getFromPDP(){
     return fromPDPFlag;
    }
    function loadSubmitReview(reviewObj){
        var flag = "1";
        MVCApp.Toolbox.common.showLoadingIndicator("");
      	init();
        model.submitReview(view.updateAfterSubmit,reviewObj);      
		         
    }
    function showMore(){ 
    MVCApp.Toolbox.common.showLoadingIndicator("");
    requestParams = getRequestParams();    
    requestParams.offset = requestParams.offset + MVCApp.Service.Constants.Reviews.limit;  
    _setRequestParams(requestParams);
    model.getReviews(view.showMore,requestParams);
  }
  
  function ratingBars(_prmss){
   var _ovrAlRtngss = _getOverAllRatings();
    _ovrAlRtngss = parseInt(_ovrAlRtngss);	    
    if(_ovrAlRtngss>0){
      init();
    requestParams = _prmss;
    requestParams.limit = _ovrAlRtngss;
    model.getReviews(view.ratingBars,requestParams);
    }    
  }
  function reviewSort(){ 
    fromPDPFlag = false;
    var str = frmReviews.lblReviewCount.text;
    var n = str.length;
    var res = str.substr(1, n-2);
    if(res>0){
    MVCApp.Toolbox.common.showLoadingIndicator("");
    var sortValue = frmReviews.lstbxReviewSort.selectedKeyValue; 
    requestParams = getRequestParams();
    requestParams.limit = MVCApp.Service.Constants.Reviews.limit;
    var name = reviewTitle;
    requestParams.offset = MVCApp.Service.Constants.Reviews.offset;
    var sValue;
    if(sortValue[1] == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.lb2RatingsLowtoHigh")  ) sValue = "Rating:ASC";
    if(sortValue[1] == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.lb3RatingsHightoLow")  ) sValue = "Rating:desc";
    if(sortValue[1] == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.lb1MostRecent")  ) sValue = "SubmissionTime:desc";
    kony.print("sValue----"+sValue);  
    requestParams.sort = sValue;    
    var locale=kony.store.getItem("languageSelected");
      if(locale=="en" || locale=="en_US"){
        locale="en_US";
      }
      kony.print("productIDForReview---"+productIDForReview);
    requestParams.filter = "ProductID:"+productIDForReview;  
    kony.print("requestParams----"+JSON.stringify(requestParams));  
    model.getReviews(view.updateScreen,requestParams,name);}
  }
  
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */
	function getReviewText(){
      return reviewText;
    }
  
  	function checkNickNameLength() {
  		if (frmReviews.txtNickName.text === "" || frmReviews.txtNickName.text.length < 4) {
    	frmReviews.lblNickName.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.nickNameTooShort");
    	frmReviews.lblNickName.skin = "sknLblGothamBold28pxRed";
  	} else {
    	frmReviews.lblNickName.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.nickName");
    	frmReviews.lblNickName.skin = "sknLblGothamBold28pxDark";
  		}
	}
  
  	function checkReviewLength() {
  		if (frmReviews.txtReview.text === "" || frmReviews.txtReview.text.length < 50) {
    	frmReviews.lblReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTooShort");
    	frmReviews.lblReview.skin = "sknLblGothamBold28pxRed";
  	} else {
    	frmReviews.lblReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.review");
    	frmReviews.lblReview.skin = "sknLblGothamBold28pxDark";
  		}
	}
  
  	function reviewTitleLength() {
      if (frmReviews.txtReviewTitle.text && frmReviews.txtReviewTitle.text.length > 0) {
        frmReviews.lblReviewTitle1.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTitle");
    	frmReviews.lblReviewTitle1.skin = "sknLblGothamBold28pxDark";
      } else {
        frmReviews.lblReviewTitle1.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTitleMissing");
    	frmReviews.lblReviewTitle1.skin = "sknLblGothamBold28pxRed";
      }
    }
  
  	function reviewEmailLength() {
      var regularEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (frmReviews.txtEmail.text === "" || !regularEx.test(frmReviews.txtEmail.text.trim())) {
        frmReviews.lblEmailReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.emailMissing");
    	frmReviews.lblEmailReview.skin = "sknLblGothamBold28pxRed";
      } else {
        frmReviews.lblEmailReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.email");
    	frmReviews.lblEmailReview.skin = "sknLblGothamBold28pxDark";
      }
    }
  
  	function checkTerms() {
      if (frmReviews.imgCheckAgree.src != "checkbox_on.png") {
         frmReviews.lblBottomMessage.setVisibility(true);
      } else {
         frmReviews.lblBottomMessage.setVisibility(false);
      }
    }
  	function setProductIDForReviewsTracking(id){
      productIdTracking=id;
    }
  	function getProductIDForReviewsTracking(){
      return productIdTracking;
    }
  
    //Here we expose the public variables and functions
 	return {
 		init: init,
    	load: load,
      	showMore:showMore,
        _setRequestParams:_setRequestParams,
        _setOverAllRatings:_setOverAllRatings,
        ratingBars:ratingBars,        
        reviewSort:reviewSort,
        loadSubmitReview : loadSubmitReview,
      	getReviewText:getReviewText,
        getFromPDP:getFromPDP,
      	getRequestParams:getRequestParams,
      	checkNickNameLength:checkNickNameLength,
      	checkReviewLength:checkReviewLength,
      	reviewTitleLength:reviewTitleLength,
      	reviewEmailLength:reviewEmailLength,
      	checkTerms:checkTerms,
      	getProductIDForReviewsTracking:getProductIDForReviewsTracking
    };
});

