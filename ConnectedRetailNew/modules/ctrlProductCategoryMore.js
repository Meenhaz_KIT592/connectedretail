
/**
 * PUBLIC
 * This is the controller for the ProductCategoryMore form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProductCategoryMoreController = (function(){
  var lServiceRequestObj = {};
  var subCatObj = {};
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.ProductCategoryMoreModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.ProductCategoryMoreView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  var categoryId_tracking="";
  var selectedCategory={};
  function setSelectedSubCategory(category){
    selectedCategory=category;
  }
  function getSelectedSubCategory(){
    return selectedCategory;
  }
  var storeNum="";
  var isStoreModeAlreadyEnabled = false;
  function setStoreModeStatus(pIsStoreModeAlreadyEnabled){
    isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
  }
  function getStoreModeStatus(){
    return isStoreModeAlreadyEnabled;
  }
  function setStoreID(store){
    storeNum=store;
  }
  function getStoreID(){
    return storeNum;
  }
  function getSubCategoryObject(){
    return subCatObj;
  }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function load(selectedSubCategory){
      lServiceRequestObj.selectedSubCategory = selectedSubCategory;
       subCatObj.id = selectedSubCategory.id; 
       subCatObj.name = selectedSubCategory.name;
       subCatObj.headerImage = selectedSubCategory.headerImage;
      	kony.print("ProductCategoryMore Controller.load");
      	init();
      frmProductCategoryMore.flxTransLayout.setVisibility(true);
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.id = selectedSubCategory.id;
      	setSelectedSubCategory(selectedSubCategory);
      	setCategoryIdFroTracking(selectedSubCategory.id ||"");
      	var storeNo ="";
      	var storeID="";
        if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeNo = gblInStoreModeDetails.storeID;
          storeID = gblInStoreModeDetails.storeID;
          setStoreModeStatus(true);
        }else{
          var storeString=kony.store.getItem("myLocationDetails") ||"";
          if(storeString!==""){
            try {
              var storeObj=JSON.parse(storeString);
              storeID=storeObj.clientkey;
              storeNo = storeObj.clientkey;
            } catch(e) {
              MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlProductCategoryMore.js on load for storeString:" + JSON.stringify(e)}]);
              storeNo ="112222";
              if(e){
                TealiumManager.trackEvent("POI Parse Exception In Tier 2 Screen", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
              }
            }
          }else{
            storeNo ="112222";
          }
        }
      	setStoreID(storeID);
        gblStoreForSkuSearch= storeID;
      	gblTealiumTagObj.storeid = storeID;
        requestParams.storeNo=storeNo;
        requestParams.productId=selectedSubCategory.id;
      	view.show(selectedSubCategory);
        model.loadSubSubCategories(view.updateScreen,requestParams);
    }
  	function getCategoryIdForTracking(){
      return categoryId_tracking;
    }
  	function setCategoryIdFroTracking(categoryId){
      categoryId_tracking=categoryId;
    }
  	function displaySamePage(){
      init();
      view.displaySamePage();
    }
  	var entryFromWebViews=false;
  	function getFormEntryFromCartOrWebView(){
      return entryFromWebViews;
    }
  	function setFormEntryFromCartOrWebView(value){
       entryFromWebViews=value;
    }
          		
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
      init: init,
      load: load,
      getSubCategoryObject:getSubCategoryObject,
      getSelectedSubCategory:getSelectedSubCategory,
      setSelectedSubCategory:setSelectedSubCategory,
      getStoreID:getStoreID,
      setStoreID:setStoreID,
      lServiceRequestObj: lServiceRequestObj,
      getStoreModeStatus: getStoreModeStatus,
      getCategoryIdForTracking:getCategoryIdForTracking,
      displaySamePage:displaySamePage,
      setFormEntryFromCartOrWebView:setFormEntryFromCartOrWebView,
      getFormEntryFromCartOrWebView:getFormEntryFromCartOrWebView
    };
});

