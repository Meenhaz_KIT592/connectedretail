/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */

var MVCApp = MVCApp || {};
skillsflag=false;
MVCApp.RewardsProfileSkillsView = (function(){
var skillData = "";
  /**
	 * PUBLIC
	 * Open the Events form
	 */
  function postShow()
  {
    var formId = kony.application.getPreviousForm().id;
    if(formId === "frmRewardsProfileStep4"){
      var intrst = rewardsProfileData.c_surveyQA;
      if(rewardsProfileData.displaySkillData.length !== 0 || (intrst !==undefined && intrst.length !== 0 && intrst[0] !== "defaults")){
        frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
      }
      else{
        frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip");               
      }
    }
    
    var lTealiumTagObj = gblTealiumTagObj;
    lTealiumTagObj.page_id = "Rewards Profile Creation: Step 4: Michaels Mobile App";
    lTealiumTagObj.page_name = "Rewards Profile Creation: Step 4: Michaels Mobile App";
    lTealiumTagObj.page_type = "Rewards";
    lTealiumTagObj.page_category_name = "Rewards";
    lTealiumTagObj.page_category = "Rewards";
    TealiumManager.trackView("Rewards Profile Creation Step 4 Screen", lTealiumTagObj);
  }
  function show(){    
   frmRewardsProfileSkills.flxMainContainer.contentOffset = {x: 0,	y: 0 };
   frmRewardsProfileSkills.show();
     
    
  } 
  	/**
	 * PUBLIC
	 * Here we define and attach all event handlers.
	 */
   
  function bindEvents(){
    flexSkillsOuter.onClick = dummyFun;
    MVCApp.tabbar.bindEvents(frmRewardsProfileSkills);
	MVCApp.tabbar.bindIcons(frmRewardsProfileSkills,"frmMore");
    frmRewardsProfileSkills.btnReset.onClick = onClickBtnResetRewardsProfileSkills;//resetValues;
	frmRewardsProfileSkills.btnHeaderLeft.onClick = showPreviousScreen;
     frmRewardsProfileSkills.btnNext.onClick = preferedStore;
    frmRewardsProfileSkills.postShow = postShow;   
	frmRewardsProfileSkills.lstInterests.onSelection = listOnSelect;
    frmRewardsProfileSkills.btnYes.onClick = navigateToMoreScreen;
    frmRewardsProfileSkills.flxOverlay.onTouchEnd = dummyOverlay;
  }
  
  function navigateToMoreScreen(){
    frmRewardsProfileSkills.flxOverlay.isVisible = false;
    MVCApp.getMoreController().load();
  }
  
  function dummyOverlay(){
    kony.print("LOG:dummyOverlay-viewRewardsProfileSkills.js-START");
  }
  
  function onClickBtnResetRewardsProfileSkills(){
    rewardsProfileData.c_surveyQA = [];
    var selctdValue = frmRewardsProfileSkills.lstInterests.selectedKey;
    var str;    
    if(selctdValue != "select"){
      str = frmRewardsProfileSkills.rtxtQuestion.text+":"+selctdValue;    
      rewardsProfileData.c_surveyQA.push(str);
    }else{
      str = "defaults";    
      rewardsProfileData.c_surveyQA.push(str);
    }    
    rewardsProfileData.interestData = frmRewardsProfileSkills.lstInterests.masterData;
    rewardsProfileData.interests = selctdValue;
    var segData = frmRewardsProfileSkills.segSkills.data || [];
    rewardsProfileData.c_interests = [];
    rewardsProfileData.displaySkillData = [];
    rewardsProfileData.segSelectedSkillData = segData;
    var intrestString = "";
    for(i=0;i<segData.length;i++){
      var temp = [];
      var levelValue = segData[i].levelValue;
      var skillName = segData[i].skillName;
      var skillMapValue = segData[i].skillMapValue;
      var intrestValue = "\""+skillName+":"+levelValue+"\"";
      var intrestValue1 = skillName+":"+levelValue;
      if(levelValue !== 0 ){
        temp.name= skillName;
        temp.skillMapValue= skillMapValue;
        if(intrestString === ""){
          intrestString = intrestValue;
        }
        else{
          intrestString=intrestString+","+intrestValue ;
        }
        rewardsProfileData.displaySkillData.push(temp);
        rewardsProfileData.c_interests.push(intrestValue1);
      }
    }
    
    MVCApp.getReviewProfileController().updateRewardProfile("step4");
  }
  
  function dummyFun(){

  }  
  function listOnSelect(){
	skillData.onSelectionSkill();
  }
  function resetValues()
  {
    var segData =  skillData.resetProfileIntrestSkillData();
    frmRewardsProfileSkills.segSkills.setData(segData);
    var interestsDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    interestsDat = interestsDat.getRewardsProfileInterestsData(); 
    var selectDefault = [["select","SELECT AN ANSWER"]];
    var interestsData = selectDefault.concat(interestsDat);
    frmRewardsProfileSkills.lstInterests.masterData = interestsData;
    frmRewardsProfileSkills.lstInterests.selectedKey = "select";

    var formId = kony.application.getPreviousForm().id;
    if(formId == "frmReviewProfile"){
      frmRewardsProfileSkills.btnReset.isVisible = false;
      frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
    }
    else{
      frmRewardsProfileSkills.btnReset.isVisible = true;
      frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip");
    }
  }
  function updateScreen(){
    var interestsDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    
    interestsDat = interestsDat.getRewardsProfileInterestsData(); 
    if(null !== interestsDat  && undefined !== interestsDat)
      {
               var selectDefault = [["select","SELECT AN ANSWER"]];
          var interestsData = selectDefault.concat(interestsDat);
          frmRewardsProfileSkills.lstInterests.masterData = interestsData;
          frmRewardsProfileSkills.lstInterests.selectedKey = "select";
        if(null !== interestsDat.questions && 
           undefined !==  interestsDat.questions && 
           interestsDat.questions.length >0)
          {
            frmRewardsProfileSkills.rtxtQuestion.text = interestsDat.questions[0]; 
          }
          
      }
   
    
 skillData = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
   var segData =  skillData.getProfileIntrests();
    rewardsProfileData.segSelectedSkillData=segData;
    frmRewardsProfileSkills.segSkills.setData(segData);
    skillsflag=true;
    show();
  }
  
  function showPreviousScreen(){    
      var prevForm = kony.application.getPreviousForm().id;
       if(prevForm=="frmReviewProfile")
      frmReviewProfile.show();
    else    
    MVCApp.getRewardsProfileChildController().updateData(); 
  }
    
  function preferedStore(){
    rewardsProfileData.c_surveyQA = [];
    var selctdValue = frmRewardsProfileSkills.lstInterests.selectedKey;
    var str;    
    if(selctdValue != "select"){
    str= frmRewardsProfileSkills.rtxtQuestion.text+":"+selctdValue;    
    rewardsProfileData.c_surveyQA.push(str);
    }else{
      str= "defaults";    
    rewardsProfileData.c_surveyQA.push(str);
    }    
    rewardsProfileData.interestData = frmRewardsProfileSkills.lstInterests.masterData;
    rewardsProfileData.interests = selctdValue;
    
     var segData = frmRewardsProfileSkills.segSkills.data ||[];
        rewardsProfileData.c_interests=[];
         rewardsProfileData.displaySkillData = [];
    if(frmRewardsProfileSkills.btnNext.text !== MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip"))
      {
         
            rewardsProfileData.segSelectedSkillData = segData;
          var intrestString = "";
          for(i=0;i<segData.length;i++)
            {
               var temp = [];
               var levelValue = segData[i].levelValue;
               var skillName = segData[i].skillName;
              var skillMapValue = segData[i].skillMapValue;
              var intrestValue = "\""+skillName+":"+levelValue+"\"";
              var intrestValue1 = skillName+":"+levelValue;
              if(levelValue !== 0 )
                {
                  temp.name= skillName;
                  temp.skillMapValue= skillMapValue;
                     if(intrestString === "")
                    {
                       intrestString = intrestValue;
    
                    }
                    else
                      {
                         intrestString=intrestString+","+intrestValue ;
                      }
                  rewardsProfileData.displaySkillData.push(temp);
                  rewardsProfileData.c_interests.push(intrestValue1);
                }
              
            }

          //rewardsProfileData.c_interests.push(intrestString);
      }else{
        rewardsProfileData.segSelectedSkillData = segData;
      }
      var formId = kony.application.getPreviousForm().id;
      if(formId == "frmReviewProfile"){
        updateFlag = true;
        MVCApp.getReviewProfileController().load();
      }
      else
      MVCApp.getRewardsProfilePreferedStoreController().load();
  }
    
  function updateData(skillFlag){
   		   skillData = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    		var segDat = rewardsProfileData.segSelectedSkillData;
    		var btnFlag = false;
    		var intrst = rewardsProfileData.c_surveyQA;
    		if( null !== segDat && segDat!=undefined){
    		for(i=0;i<segDat.length;i++)
            {               
              if(segDat[i].levelValue !== 0 ||(intrst !==undefined &&  intrst.length !== 0 && intrst[0] !== "defaults"))
                btnFlag = true;
             }
            }
    		
    		if(rewardsProfileData.segSelectedSkillData!==undefined || (intrst !==undefined && intrst[0] !== "defaults" && intrst.length !== 0)){
              if(rewardsProfileData.segSelectedSkillData!==undefined)
           frmRewardsProfileSkills.segSkills.setData( rewardsProfileData.segSelectedSkillData);
              else{
                skillData = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
   				var segData =  skillData.getProfileIntrests();
    			rewardsProfileData.segSelectedSkillData=segData;
    			frmRewardsProfileSkills.segSkills.setData(segData);
              }
                      
           var interestsDat = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
    		interestsDat = interestsDat.getRewardsProfileInterestsData(); 
    		var selectDefault = [["select","SELECT AN ANSWER"]];
    		var interestsData = selectDefault.concat(interestsDat);   
             frmRewardsProfileSkills.rtxtQuestion.text = interestsDat.questions[0]; 
      		if(intrst !==undefined && intrst.length !== 0 && intrst[0] !== "defaults"){
      		var intrstArray=intrst[0].split(":");      
      		frmRewardsProfileSkills.rtxtQuestion.text = intrstArray[0];
            frmRewardsProfileSkills.lstInterests.masterData =interestsData;  
            frmRewardsProfileSkills.lstInterests.selectedKey = rewardsProfileData.interests;
            }else{
              frmRewardsProfileSkills.lstInterests.masterData =interestsData;
              frmRewardsProfileSkills.lstInterests.selectedKey = "select";               
            }
            }   
    		else
              {
                updateScreen();
              }
         	if(skillFlag){
              frmRewardsProfileSkills.btnReset.isVisible = false;
              frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
            }else if(btnFlag){
              frmRewardsProfileSkills.btnReset.isVisible = true;
              frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
            }
    		else{
              frmRewardsProfileSkills.btnReset.isVisible = true;
              frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip");
            }
    show();
  }
  
  //Here we expose the public variables and functions
  return{
 		show: show,
 		bindEvents: bindEvents,
        updateScreen:updateScreen,
    	updateData:updateData
    };
  
});