/**
 * PUBLIC
 * This is the model for frmWeeklyAd form
 */
var MVCApp = MVCApp || {};
MVCApp.WeeklyAdModel = (function(){
 var serviceType = MVCApp.serviceType.CofactorService;
  var operationId = MVCApp.serviceEndPoints.getListings;
  function getWeeklyAdsDepartments(callback,requestParams,dealId){ 
    kony.print("getWeeklyAdsDepartments.js");
   MakeServiceCall( serviceType,operationId,requestParams,
                    function(results)
                    {
      kony.print("Response is "+JSON.stringify(results)); 
      if(results.opstatus===0 || results.opstatus==="0" ){
         var weeklyAdsDataObj = new MVCApp.data.WeeklyAds(results.results || []);
         callback(weeklyAdsDataObj,dealId);
      }   
    },function(error){
      kony.print("Response is "+JSON.stringify(error));
    }
    );
  }
 	return{
      getWeeklyAdsDepartments:getWeeklyAdsDepartments
    };
});
