//Type your code here
var MVCApp = MVCApp || {};
MVCApp.StoreLocatorModel = (function(){
 var serviceType = "";
  var operationId = "";
  function loadData(requestParams,callback){
   var serviceType=MVCApp.serviceType.checkNearbyProviders;
   var operation=MVCApp.serviceEndPoints.searchByLatLon;
   MakeServiceCall( serviceType,operation,requestParams,
                           function(results)
                           {
      kony.print("Response is "+JSON.stringify(results));
      var storeData=new MVCApp.data.StoreLocator(results);
      //var nearestStore=storeData.getNearestStore();
      callback(storeData);
      
    });
    //callback("");
  }
  function loadNearest(requestParams,callback){
    var serviceType=MVCApp.serviceType.LocatorService;
    var operation=MVCApp.serviceEndPoints.searchByLatLon;
    MakeServiceCall( serviceType,operation,requestParams,
                           function(results)
                           {
      kony.print("Response is "+JSON.stringify(results));
      //alert("input Params is  "+JSON.stringify(requestParams));
      var storeData=new MVCApp.data.StoreLocator(results);
      var nearestStore=storeData.getNearestStore();
      //callback(results,requestParams);
      if(nearestStore!== "" && nearestStore!== null && nearestStore!== undefined){
         nearestStore.timelastUpdated=new Date().getTime();
         kony.store.setItem("myLocationDetails",JSON.stringify(nearestStore));
         MVCApp.sendMetricReport(frmHome, [{"STORESELECTED":nearestStore.clientkey+""}]);

        MVCApp.Toolbox.Service.setMyLocationFlag(true);
        if(gblFavStoreRelocated){
          gblFavStoreRelocated = false;
          MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.RelocatedFavStoreLocationEnabled"), "", constants.ALERT_TYPE_CONFIRMATION,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.RelocatedFavStoreLocaterPage"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"));
        }
        }else{
          if(gblFavStoreRelocated){
            gblFavStoreRelocated = false;
            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.RelocatedFavStoreLocationDisabled"),"",constants.ALERT_TYPE_INFO,MVCApp.Toolbox.common.redirectToStoreLocation,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"),"");
          }
           //createLocalnotification(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHome.lblNoStorefound"));
            //MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHome.lblNoStorefound"), "");
           //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHome.lblNoStorefound"));
         }
      callback(nearestStore);
    });
  }
  function loadDataFromSearch(requestParams,callback){
    var serviceType=MVCApp.serviceType.checkNearbyProviders;
   var operation=MVCApp.serviceEndPoints.searchByZip;
  	MVCApp.Toolbox.common.showLoadingIndicator();
   MakeServiceCall( serviceType,operation,requestParams,
                           function(results)
                           {
      kony.print("Response in search "+JSON.stringify(results));
      var storeData=new MVCApp.data.StoreLocator(results);
      //var nearestStore=storeData.getNearestStore();
      callback(storeData);
      //return nearestStore;
      
    },function(error){
     MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"), "");
     //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"));
     MVCApp.Toolbox.common.dismissLoadingIndicator();
   });
  }
  function loadCheckNearbyData(requestParams,callback){
    var serviceType=MVCApp.serviceType.checkNearbyProviders;
   var operation=MVCApp.serviceEndPoints.fetchNearByProviders;
  	MVCApp.Toolbox.common.showLoadingIndicator();
   MakeServiceCall( serviceType,operation,requestParams,
                           function(results)
                           {
      kony.print("Response in search "+JSON.stringify(results));
      var storeData=new MVCApp.data.StoreLocator(results);
      //var nearestStore=storeData.getNearestStore();
      callback(storeData);
      //return nearestStore;
      
    });
  }
  function loadCheckNearbySearch(requestParams,callback){
    var serviceType=MVCApp.serviceType.checkNearbyProviders;
   var operation=MVCApp.serviceEndPoints.fetchNearByZip;
  	MVCApp.Toolbox.common.showLoadingIndicator();
   MakeServiceCall( serviceType,operation,requestParams,
                           function(results)
                           {
      kony.print("Response in search "+JSON.stringify(results));
      var storeData=new MVCApp.data.StoreLocator(results);
      //var nearestStore=storeData.getNearestStore();
      callback(storeData);
      //return nearestStore;
      
    },function(error){
     alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"));
     MVCApp.Toolbox.common.dismissLoadingIndicator();
   }
                  );
  }
  function fetchStoreDetails(successCallback,requestParams){
    var operationId = MVCApp.serviceType.fetchINStoreDetailsByID;
    var serviceType = MVCApp.serviceType.LocatorService;
    MakeServiceCall(serviceType,operationId,requestParams,
      function(results){
        kony.print("------fetchStoreDetails-Results:"+JSON.stringify(results)); 
        if(results.opstatus === 0 || results.opstatus === "0"){
          MVCApp.Toolbox.common.dismissLoadingIndicator();
          if(results){
            if(results.collection && results.collection.length > 0){
              if(results.collection[0] && results.collection[0].poi){
                var poi=results.collection[0].poi;
                poi.timelastUpdated=new Date().getTime();
                kony.store.setItem("myLocationDetails",JSON.stringify(poi));
              }
            }
          }
          MVCApp.Toolbox.common.dismissLoadingIndicator();
          successCallback();
        }   
      },function(pError){
        kony.print("LOG:fetchInStoreModeStoreDetails-pError:"+JSON.stringify(pError));
        MVCApp.Toolbox.common.dismissLoadingIndicator();
      }
    );
  }
 	return{
      loadData:loadData,
      loadNearest:loadNearest,
      loadDataFromSearch:loadDataFromSearch,
      loadCheckNearbyData:loadCheckNearbyData,
      loadCheckNearbySearch:loadCheckNearbySearch,
      fetchStoreDetails:fetchStoreDetails
    };
});
