
/**
 * PUBLIC
 * This is the controller for the frmWeeklyAd form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.WeeklyAdController = (function(){
  
  var _isInit = false;
  var model = null;//Backend interaction
  var view = null;//UI interaction
	
	/**
	 * PUBLIC
	 * Init all that the model and view need.
	 * Usually such init methods would be private, but for this main form
	 * we grant public access so that we can init the form before it is being loaded.
	 * This enables us to predefine the theme and locale.
	 */
   	function init(theme,locale){
  	    if (_isInit===true) return;
  	    
  	    //Init the model which exposes the backend services to this controller
  	    model = new MVCApp.WeeklyAdModel();
  	    
  	    //Enables us to use different UI interactions depending on form factor
	  	view = new MVCApp.WeeklyAdView();
  	    //}
  	    
  	    //Bind events to UI
		view.bindEvents();
		
  		_isInit = true;
    }
  	/**
	 * PUBLIC
	 * Open the form.
	 */
  	function loadFeaturedDetails(data,headerTxt,frmWhichForm){
      	MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("Weekly Ads Controller.load");
      	init();  
      	var weeklyAdsDataObj = new MVCApp.data.WeeklyAds(data); 
      	view.show(headerTxt,frmWhichForm);
      	view.updateScreen(weeklyAdsDataObj,headerTxt);    
    }
     
  function loadWeeklyAds(id,headerTxt,frmWhichForm,dealId){
    	MVCApp.Toolbox.common.showLoadingIndicator("");
      	kony.print("Weekly Ads Controller.load");
      	init();
    	var requestParams = {};
      	var storeNo = "";
      	var storeString=kony.store.getItem("myLocationDetails") ||"";
    	if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
          storeNo = gblInStoreModeDetails.storeID;
        }else{
          if(storeString!==""){
          try {
            var storeObj=JSON.parse(storeString);
            storeNo =storeObj.clientkey;
          } catch(e) {
            MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"ctrlWeeklyAd.js on loadWeeklyAds for storeString:" + JSON.stringify(e)}]);
            storeNo ="";
            if(e){
              TealiumManager.trackEvent("POI Parse Exception in weekly Ad Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
          }
    	}else{
          storeNo ="";
    	}
        }
    	
    	setDepartmentID(id);
     	requestParams.storeref = storeNo;
      	requestParams.limit = "100";
      	requestParams.languageid =MVCApp.Service.getLanguageID();
      	requestParams.sort = "1";
    	requestParams.departmentid = id;
    	if(changeInFrenchLanguage){
          requestParams.languageid ="1";
          changeInFrenchLanguage=false;
        }
    	view.show(headerTxt,frmWhichForm);
    	model.getWeeklyAdsDepartments(view.updateScreen,requestParams,dealId);
  }
  var deptID="";
  function setDepartmentID(id){
    deptID=id;
  }
  function getDepartmentID(id){
    return deptID;
  }
  function showUI(){
    init();
    view.showFormFromVoiceSearch();
  }
  var entryFromWebViews=false;
  	function getFormEntryFromCartOrWebView(){
      return entryFromWebViews;
    }
  	function setFormEntryFromCartOrWebView(value){
       entryFromWebViews=value;
    } 
	/**
	 * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
	 */

    //Here we expose the public variables and functions
 	return {
 		init: init,
    	loadFeaturedDetails: loadFeaturedDetails,
      	loadWeeklyAds:loadWeeklyAds,
        showUI:showUI,
      	getDepartmentID:getDepartmentID,
      	setFormEntryFromCartOrWebView:setFormEntryFromCartOrWebView,
      	getFormEntryFromCartOrWebView:getFormEntryFromCartOrWebView
    };
});

