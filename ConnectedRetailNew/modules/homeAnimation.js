function findCardTop(){
  try{
    firstCardTop=frmHome.fxlContainer.frame.y; 
  }catch(e){
    kony.print(e);
    if(e){
      TealiumManager.trackEvent("Find Card Top Exception in Home Animation Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  }
}

function frmHomeOnScrollStart(){
  frmHomeScrollStartY=frmHome.flxMainContainer.contentOffsetMeasured.y;
  firstCardTop=frmHome.fxlContainer.frame.y;
}

function frmHomeFooterShow(){
  footerState = "visible";
  frmHome.flxFooterWrap.animate(
    kony.ui.createAnimation({
      0:{bottom:"-65dp","stepConfig":{}},
      100:{bottom:"0dp","stepConfig":{}}}),
    {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:0.25},
    {animationEnd: function() {}
    });
}

function frmHomeFooterHide(){
  footerState = "hidden";
  frmHome.flxFooterWrap.animate(
    kony.ui.createAnimation({
      0:{bottom:"0dp","stepConfig":{}},
      100:{bottom:"-65dp","stepConfig":{}}}),
    {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:0.25},
    {animationEnd: function() {}
    });
}

function frmHomeOnScrolling(){
  try{
  var scrollCurrentPos = parseInt(frmHome.flxMainContainer.contentOffsetMeasured.y);
  var footerCurrentState = "visible";
  footerCurrentState = (scrollCurrentPos < scrollPosY) ? "visible" : "hidden";
  if(frmHome.flxHeaderWrap.flxSearchContainer.top == "50dp"){
    if(footerState == "hidden"){
      frmHomeFooterShow();
    }
  }else if((footerCurrentState != footerState) && (scrollCurrentPos != scrollPosY)){
    if(footerCurrentState == "visible"){
      frmHomeFooterShow();
    }else if(footerCurrentState == "hidden"){
      frmHomeFooterHide();
    }
  }
  scrollPosY=scrollCurrentPos;
  try{
    if (scrollPosY>0){
      frmHome.flxHeaderWrap.flxSearchContainer.top= 50+Math.max(-43,(scrollPosY*-1));
      frmHome.flxHeaderWrap.flxSearchContainer.left = 10-Math.max(-35,(scrollPosY*-1));
      frmHome.flxHeaderWrap.flxSearchContainer.right = 10-Math.max(-46,(scrollPosY*-1));
      frmHome.flxHeaderWrap.flxSearchContents.left = "0dp";
      frmHome.flxHeaderWrap.flxSearchContents.right = "0dp";
      frmHome.flxHeaderWrap.flxSearchContents.skin = "sknFlexBgLtGrayBdr2";
    }else {
      frmHome.flxHeaderWrap.flxSearchContainer.top="50dp";
      frmHome.flxHeaderWrap.flxSearchContainer.left = "0dp";
      frmHome.flxHeaderWrap.flxSearchContainer.right = "0dp";
      frmHome.flxHeaderWrap.flxSearchContents.left = "10dp";
      frmHome.flxHeaderWrap.flxSearchContents.right = "10dp";
      frmHome.flxHeaderWrap.flxSearchContents.skin = "sknFlexBgLtGray";
    }
  }catch(e){
    if(e){
      TealiumManager.trackEvent("Dynamic Setting of UI When Scrolling Exception in Home Animation Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  }
  //#ifdef android
  	frmHome.flxHeaderWrap.flxSearchContents.forceLayout();
  //#endif
  //card overlay folding  
  if (scrollPosY <=firstCardTop){
    frmHome.flexCardAnimate.isVisible=false;
  }
  else {
    //#ifdef iphone
    frmHome.flexCardAnimate.setEnabled(false);
    //#endif
    frmHome.flexCardAnimate.isVisible=true;
    //#ifdef android
    	kony.print("width-----"+kony.os.deviceInfo().screenWidth);
    //#endif
    //alert(overlayCardHeight);
    //var currentCard=parseInt((scrollPosY-firstCardTop)/190);
    var currentCard=parseInt((scrollPosY-firstCardTop)/cardContentHeight);
    var scrollPosYNorm=Math.min(1,((cardContentHeight-((scrollPosY-firstCardTop)-(cardContentHeight*currentCard)))/cardContentHeight));
    frmHome.flexCardAnimateBG.bottom=cardContentHeight-(scrollPosYNorm*cardContentHeight);
	//#ifdef android
        //frmHome.flexCardAnimateBG.forceLayout();
    	//frmHome.forceLayout();
    //#endif
	var angleDef = (1-scrollPosYNorm)*cardEndAngle;
	var topTransform=kony.ui.makeAffineTransform();
	topTransform.setPerspective(cardPerspective);
	//#ifdef android
    	topTransform.rotate3D(angleDef*(-1),1,0,0);
    //#else
		topTransform.rotate3D(angleDef,-1,0,0);
	//#endif
    frmHome.flexCardAnimate1b.transform=topTransform;
    //#ifdef android
      //frmHome.flexCardAnimate1b.forceLayout();
      //frmHome.forceLayout();
    //#endif
    var bottomTransform=kony.ui.makeAffineTransform();
	bottomTransform.setPerspective(cardPerspective);
	bottomTransform.rotate3D(angleDef,1,0,0);
    frmHome.flexCardAnimate1a.transform=bottomTransform;
    //#ifdef android
      //frmHome.flexCardAnimate1a.forceLayout();
      //frmHome.forceLayout();
    //#endif
    frmHome.flexShadowTop.opacity=(1-scrollPosYNorm);
    frmHome.flexShadowBottom.opacity=(1-scrollPosYNorm);
    frmHome.lblTileText.text=eventLabelData[currentCard];
    
    }
    if(frmHome.flxSearchResultsContainer.isVisible){
      frmHome.flexCardAnimate.isVisible = false;
    }
  }catch(e){
    kony.print("error---"+e);
    if(e){
      TealiumManager.trackEvent("Home On Scrolling Exception in Home Animation Flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
    }
  } 
}
function frmHomeFooterOnScrolling(){
  var scrollDelta=parseInt(scrollPosY-frmHomeScrollStartY);
  	
  if (scrollDelta < 0 && footerLock==false){
    if (scrollDelta < 1  && footerState=="hidden"){
        frmHome.flxFooterWrap.bottom=Math.min(0,(-57+Math.abs(scrollDelta)));
    }
    if (scrollDelta < -57  && footerState=="hidden"){
        footerState="visible";
    }
  }else if (scrollDelta>0 && footerLock==false){
     if (scrollDelta < 57  && footerState=="visible"){
        frmHome.flxFooterWrap.bottom=0-scrollDelta;    
      }
      if (scrollDelta>57 && footerState=="visible"){
          footerState="hidden";
          frmHome.flxFooterWrap.bottom="-57dp";
      }   
  }
  if (scrollPosY<0){
  	footerLock=true;
    frmHome.flxFooterWrap.bottom="0dp";
  }
  //#ifdef android
		frmHome.flxFooterWrap.forceLayout();
		frmHome.forceLayout();
  //#endif
}

function frmHomeFooterOnScrollTouchReleased(){
  var footerBot=frmHome.flxFooterWrap.bottom;
  if (footerBot < 0 && footerBot > -57 && footerState=="visible"){
    if (footerBot>-26){
      frmHome.flxFooterWrap.animate(
      kony.ui.createAnimation({
        0:{bottom:footerBot,"stepConfig":{}},
        100:{bottom:"0dp","stepConfig":{}}}),
      {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.25},
      {animationEnd: function() {
        	footerState="visible";
        	frmHomeFooterDoublecheck();
      }});
    }
    else{
      frmHome.flxFooterWrap.animate(
      kony.ui.createAnimation({
        0:{bottom:footerBot,"stepConfig":{}},
        100:{bottom:"-45dp","stepConfig":{}}}),
      {delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:.25},
      {animationEnd: function() {
        	footerState="hidden";
        	frmHomeFooterDoublecheck();
      }});
    }
  }
  //#ifdef android
  	//frmHome.flxFooterWrap.forceLayout();
  	//frmHome.forceLayout();
  //#endif
}

function frmHomeFooterDoublecheck(){
  var footerBot=frmHome.flxFooterWrap.bottom;
  if (footerBot==0){
    footerState="visible";
  }
}
function frmHomeFooterOnScrollEnd(){
  footerLock=false;
  frmHomeFooterDoublecheck();
  if(frmHome.flxSearchResultsContainer.isVisible){
    frmHome.flexCardAnimate.isVisible = false;
    frmHome.flxHeaderWrap.textSearch.setFocus(true);
  }
  if(footerState == "hidden" && ((scrollPosY +  250) > frmHome.flxMainContainer.fxlContainer.frame.height)){
    frmHomeFooterShow();
  }
}

function isOdd(num) { 
  return num % 2;
}