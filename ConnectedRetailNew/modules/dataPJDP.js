
/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.PJDP = (function(json){

  var typeOf = "Projects";
  var _data = json || {};
  _data = _data.projects || [];
  
  if(_data.length >0 ){
    _data = _data[0] || {};
  }
  /**
	 * PUBLIC - setter for the model
	 * Set data from the JSON retrieved by the model
	 */
  function getTitle(){
      return _data.title ? _data.title : "";
  }
  
  function getStepQuantity(){
      return _data.stepQuantity ? _data.stepQuantity : "";
  }
  
  function getLongDescription(){
      return _data.longDescription  ? _data.longDescription  : "";
  }
  
  function getMinimumOrderQuantity(){
      return _data.minOrder  ? _data.minOrder  : "";
  }
  
  function getStockLevel(){
      var inventory = _data.inventory || {};   
      return inventory.stockLevel  ? inventory.stockLevel  : "";
  }
  
  function getSKUId(){
      return _data.sku ? _data.sku  : "";
  }
  
  function getReviewsCount(){
      return _data.reviewCount ? _data.reviewCount  : "";
  }
  
  function getRating(){
    return _data.avgRating ? _data.avgRating  : "";
  }
  
  function getCAProp64Message(){
    var propFlag = _data.prop65Flag ? _data.prop65Flag :"";
    if(propFlag == "true"){
      return _data.prop65FlagMessage ? _data.prop65FlagMessage  : "";
    }else{
      return "";
    }
  }
  function getMasterId(){
    var master = _data.master || "";
    return master.masterId ? master.masterId :"";
  }
  
 function getProjectId(){
   return _data.id ? _data.id :"";
 }
  
  function getReportId(){
   return _data.report_id ? _data.report_id :"NA";
 } 
  
  function getImages(){
    
    var smallImagesArray=[];
    var mediumImagesArray=[];
    var largeImagesArray=[];
    var zoomImagesArray=[];
     
    var imageGroups = _data.image_groups || [];
    
    for(var i=0;i<imageGroups.length;i++){
      if((imageGroups[i].view_type || "") == "small"){
        smallImagesArray.push(imageGroups[i].images);
      }
      if((imageGroups[i].view_type || "" )== "medium"){
        mediumImagesArray.push(imageGroups[i].images);
      }
      if((imageGroups[i].view_type  || "")== "large"){
        largeImagesArray.push(imageGroups[i].images);
      }
      if((imageGroups[i].view_type || "" )== "zoom"){
        zoomImagesArray.push(imageGroups[i].images);
      }
    }
    var imagesArray = {};
     imagesArray.smallImages = smallImagesArray;
     imagesArray.mediumImages = mediumImagesArray;
     imagesArray.largeImages = largeImagesArray;
     imagesArray.zoomImages = zoomImagesArray;
    return imagesArray;
  }
  
  
  function getVariants(){
   return _data.variationAttributes || _data.variationAttributes || "";
  }
  
  function getPrice(){
    return _data.price ? _data.price:"";
  }
  
  function getOrderbaleStatus(){
    var inventory = _data.inventory || {};   
    return inventory.oderable  ? inventory.oderable  : "";
  }
  function getCtype(){
    return _data.c_type ? _data.c_type : "";
  }
  
  function getSkillLevel(){
    return _data.skillLevel ? _data.skillLevel : "";
  }
  
  function getCraftTime(){
    return _data.CraftTime ? _data.CraftTime : "";
  }
  
  function getProjectInstructions(){
     var projInstructions = _data.ProjectInstructions || [];
     var projInstructionsText = [];
     var language=kony.store.getItem("languageSelected");
     var showInstrunctionNumber = false;
     if(language=="en" || language =="en_US" || language ==""){
        showInstrunctionNumber = true;
     }else{
        showInstrunctionNumber = false;
     }
     for(var i=0;i < projInstructions.length ; i++){
       var count = i+1;
       var projinstructData = {
         "lblSno":{text:count.toFixed(),isVisible:showInstrunctionNumber},
         "rtxtProjInstructions":"<div style='line-height:120%'>"+projInstructions[i]+"</div>"//projInstructions[i]
       };
       //alert("projinstructData--"+JSON.stringify(projinstructData));
       projInstructionsText.push(projinstructData);
       //projInstructionsText = projInstructionsText + projInstructions[i];
     }
    return projInstructionsText;
  }
  
  function getAdditionalMaterials(){
    return _data.AdditionalMaterials || [];
  }
  function onClickAislePJDP(eventObj){
     var params = {};
    gblWrap_Clip_ID=eventObj.info.wrapClipId || "";
    params.aisleNo = eventObj.info.aisleNum;
    params.productName = eventObj.info.name;
    params.calloutFlag = true;
    MVCApp.sendMetricReport(frmPJDP, [{"storemap":"click"}]);
    var cust360Obj= MVCApp.Customer360.getGlobalAttributes();
    var storeId= MVCApp.Toolbox.common.getStoreID();
    cust360Obj.productid=eventObj.info.productId;
    cust360Obj.store_id=storeId;
    cust360Obj.entry_type="pjdp";
    MVCApp.Customer360.sendInteractionEvent("ProductLocationInStore", cust360Obj);
    MVCApp.getStoreMapController().load(params);
  }
  function getPrimaryProductsList(){
    var lMyLocationFlag = false;
    var storeBlockFlag = "";
    var storeObj = {};
    var storeString=kony.store.getItem("myLocationDetails") ||"";
    if(gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled){
      if(gblInStoreModeDetails.POI){
        storeObj = gblInStoreModeDetails.POI;
      }
      lMyLocationFlag = "true";
    }else{
      lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
      if(storeString!==""){
        try{
          storeObj = JSON.parse(storeString);
        }catch(e){
          MVCApp.sendMetricReport(frmHome, [{"Parse_Error_DW":"dataPJDP.js on getPrimaryProductsList for storeString:" + JSON.stringify(e)}]);
          if(e){
            TealiumManager.trackEvent("POI Parse Exception in PJDP flow", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
          }
        }
      }
    }
   	
    if(storeObj && storeObj.isblocked){
      storeBlockFlag = storeObj.isblocked || "";
    }
      
    kony.print(JSON.stringify(_data.set_products));
    var productsList = _data.set_products || [];
    gProductList = [];
    var cSets = _data.c_sets || [];
    var primaryProducts = [];
    for(var i = 0;i < productsList.length;i++){
      var c_online=productsList[i].c_online;
      if(c_online== null || c_online==undefined){
        c_online=true;
      }
      if(c_online===true || c_online==="true"){
      var smallImagesArray=[];
      var mediumImagesArray=[];
      var largeImagesArray=[];
      var zoomImagesArray=[];
      var imageGroups = productsList[i].image_groups || [];
      gProductList.push(productsList[i]);
      for(var j=0;j<imageGroups.length;j++){
          if((imageGroups[j].view_type || "") == "small"){
            smallImagesArray.push(imageGroups[j].images);
          }
          if((imageGroups[j].view_type || "" )== "medium"){
            mediumImagesArray.push(imageGroups[j].images);
          }
          if((imageGroups[j].view_type  || "")== "large"){
            largeImagesArray.push(imageGroups[j].images);
          }
          if((imageGroups[j].view_type || "" )== "zoom"){
            zoomImagesArray.push(imageGroups[j].images);
          }
      }
     var imagesArray = {};
     imagesArray.smallImages = smallImagesArray;
     imagesArray.mediumImages = mediumImagesArray;
     imagesArray.largeImages = largeImagesArray;
     imagesArray.zoomImages = zoomImagesArray;
      var imageToBeShown="";
      var c_imageShown=productsList[i].c_default_image ||"";
      var defaultImage = imagesArray.smallImages || [];
      if(defaultImage.length >0){
        defaultImage = defaultImage[0] || [];
        defaultImage = defaultImage[0].link || "";
      }
      if(c_imageShown!==""){
        c_imageShown=MVCApp.Toolbox.common.replacePipesAndColons(c_imageShown);
        imageToBeShown=c_imageShown;
      }else{
        imageToBeShown=defaultImage;
      }
      var title=productsList[i].c_default_image_title ||"";
      var titleName = productsList[i].name || "";
      var c_master_id = productsList[i].c_master_id || "";
      title=titleName;
      var price = productsList[i].price || "";
      var id = productsList[i].id || "";
      var priceCurr = "";
      var rvwNumber = productsList[i].c_bvAverageRating || "0";
      if(price !== undefined && price!== "" && price !== null){
        price = MVCApp.Toolbox.common.showPriceDecimals(price);
          var langReg=kony.store.getItem("languageSelected");
    	if(langReg==="fr_CA"){
          if(price!==null && price!==undefined){
             price = parseFloat(price);
             priceCurr =Number(price).toLocaleString('fr')+"$";
          }
           // priceCurr = price+"$";
        }
        else
        priceCurr = "$"+price;
      }
      var prodType = productsList[i].type || {};
      for(var k=0;k<cSets.length;k++){
        var cSetsId = cSets[k].ID || "";
       if(cSetsId == id) {
        productsList[i].c_store_inventory = cSets[k].c_store_inventory || {};
        break;
       } 
      }  
      var storeInventory = productsList[i].c_store_inventory || {};
      var location = storeInventory.aisle || "";
      var aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(location);
      var aisleNo =  aisleInfo.number;
      var aisleName = aisleInfo.name;
      var aisleNumber = storeInventory.aisle || "";
      aisleNo = aisleName.toUpperCase()+" "+aisleNo;
      var stockQty=storeInventory.ats ||"";
      var rvwNumberStr=rvwNumber+"";
      var reviewLen = rvwNumberStr.length;      
      var widthAlign = 90;
      var wrapClipId= "";
        if( storeInventory.hasOwnProperty("wrap_clip_id") ){
                     wrapClipId=storeInventory["wrap_clip_id"] || "";
                            kony.print("wrapClipId:"+wrapClipId);

                    
                    }
      if(reviewLen === 2)
        widthAlign=100;
      if(reviewLen > 2){
        var screenWidth = kony.os.deviceInfo().screenWidth;
    if(screenWidth == "414"){
      widthAlign=100+(reviewLen-2)*23;
    }else if(screenWidth == 375){
      widthAlign=100+(reviewLen-2)*7;
    }else{
      widthAlign=100+(reviewLen-2)*5;  
    }
        
      }      
      widthAlign = widthAlign.toString();
      widthAlign = widthAlign+"dp";
      var storeMapData = {text:MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempProdList.btnStoreMap"),onClick:onClickAislePJDP,info:{name:productsList[i].name,aisleNum:MVCApp.Toolbox.common.getAisleNumber(aisleNumber),wrapClipId:wrapClipId,"productId":id}};
      var flxItemLocation={};
       if(lMyLocationFlag === "true" && stockQty!=="" && storeBlockFlag!="true" && aisleNo!=" "){
         flxItemLocation={isVisible:true};
       }else{
         flxItemLocation={isVisible:false};
       }
      
      var primaryProductDetails = {};
      var ratings = [];
      var starReview;
      if(null != productsList[i].c_bvAverageRating && undefined != productsList[i].c_bvAverageRating && "" != productsList[i].c_bvAverageRating){
        starReview=productsList[i].c_bvAverageRating;
      }else{
        starReview = 0;
      }
      starReview=parseFloat(starReview);
      starReview = starReview.toFixed(1);
      var starFloor = Math.floor(starReview);
	  var starDecimal = starReview.toString().split(".")[1];
      starDecimal = parseInt(starDecimal);
      for(var j=1;j<=5;j++){
        if(j<=starFloor){
          primaryProductDetails["imgStar"+j]={src:"star.png"};
        }else{
          primaryProductDetails["imgStar"+j]={src:"starempty.png"};
        }
        
      } 
      var starFlag = starFloor+1;
      if(starDecimal>0){
      primaryProductDetails["imgStar" + starFlag] = { src: "star"+starDecimal+".png"};
      }
      primaryProductDetails["btnAddtoList"] = {text:MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempProdList.btnAddtoList"),onClick:addToShoppingList,info:{id: id}};    
      primaryProductDetails["title"] = title;
      primaryProductDetails["price"] = priceCurr;
      primaryProductDetails["image"] = imageToBeShown;
      primaryProductDetails["markedPriceProduct"] = {isVisible:false};
      primaryProductDetails["stockNumber"] = {isVisible:false};
      primaryProductDetails["reviewNumber"] = "("+rvwNumber+") ";
      primaryProductDetails["id"] = id;
      primaryProductDetails["prodType"] = prodType;
      primaryProductDetails["aisleNo"] = aisleNo;
      primaryProductDetails["storeMapData"] = storeMapData;
      primaryProductDetails["flxStarRatingAlignment"] = {"width":widthAlign};
      primaryProductDetails["flxItemLocation"] = flxItemLocation;
      primaryProductDetails["c_master_id"] = c_master_id; 
        primaryProducts.push(primaryProductDetails);
      }
    }
    return primaryProducts || [];
  }
  
  function addToShoppingList(eventObj)
  {
    currentFormForPasswordExpiryCase=frmPJDP;
     var info = eventObj.info || {};
    var productId = info.id || "";   
	var type =MVCApp.Service.Constants.ShoppingList.pjdpType;
	 MVCApp.getShoppingListController().addToProductList(productId,type);
  }
  //Here we expose the public variables and functions
  return {
    typeOf: typeOf,
    getTitle:getTitle,	
    getStepQuantity:getStepQuantity,
    getLongDescription:getLongDescription,
    getMinimumOrderQuantity:getMinimumOrderQuantity,
    getStockLevel:getStockLevel,
    getSKUId:getSKUId,
    getReviewsCount:getReviewsCount,
    getRating:getRating,
    getCAProp64Message:getCAProp64Message,
    getImages:getImages,
    getMasterId:getMasterId,
    getProjectId:getProjectId,
    getVariants:getVariants,
    getPrice:getPrice,
    getOrderbaleStatus:getOrderbaleStatus,
    getCtype:getCtype,
    getAdditionalMaterials:getAdditionalMaterials,
    getProjectInstructions:getProjectInstructions,
    getCraftTime:getCraftTime,
    getSkillLevel:getSkillLevel,
    getPrimaryProductsList:getPrimaryProductsList,
    getReportId:getReportId
    
  };
});