function onPullSegProductsList(eventobject) {
    return AS_Segment_8f2f6dbb0ab7487abbef6bbb40824df2(eventobject);
}

function AS_Segment_8f2f6dbb0ab7487abbef6bbb40824df2(eventobject) {
    showMenuBar(frmProductsList.segProductsList);

    function showMenuBar(obj) {
        try {
            var form = kony.application.getCurrentForm();
            form.flxFooterWrap.animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "bottom": "0dp"
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.5
            });
            MVCApp.getProductsListController().showMore(frmProductsList.segProductsList);
        } catch (e) {
            kony.print("exception---" + e);
        }
    }
}