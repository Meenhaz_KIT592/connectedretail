function AS_Button_e167edbd01ba46d79512ad20dafeaaa9(eventobject) {
    function ROTATE_ACTION_3D____7ae557a63c9d4342a6c7eb5157301cc3_Callback() {}
    //#ifdef winphone8
    //Rotate 3D not supported for windows devices
    //#else
    var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate3D(90, 1, 0, 0);
    undefined.animate(kony.ui.createAnimation({
        "100": {
            "anchorPoint": {
                "x": 0.5,
                "y": 0.5
            },
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": ROTATE_ACTION_3D____7ae557a63c9d4342a6c7eb5157301cc3_Callback
    });
    //#endif
}