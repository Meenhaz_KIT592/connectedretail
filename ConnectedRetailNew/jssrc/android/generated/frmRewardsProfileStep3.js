function addWidgetsfrmRewardsProfileStep3() {
    frmRewardsProfileStep3.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var FlexContainer0b4738c7579e24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "FlexContainer0b4738c7579e24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    FlexContainer0b4738c7579e24b.setDefaultUnit(kony.flex.DP);
    var Label0c5601159c0bb4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0c5601159c0bb4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxWhite",
        "text": "Step 3 - Survey (Optional)",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0b4738c7579e24b.add(Label0c5601159c0bb4c);
    var Segment023bc100d5aa64c = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblChevron": "K",
            "lblSurveyName": "CHILDREN",
            "lblSurveySelected": "EDIT"
        }, {
            "lblChevron": "K",
            "lblSurveyName": "INTEREST",
            "lblSurveySelected": ""
        }, {
            "lblChevron": "K",
            "lblSurveyName": "SKILLS",
            "lblSurveySelected": ""
        }],
        "groupCells": false,
        "id": "Segment023bc100d5aa64c",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowF7F7F7",
        "rowSkin": "segMenuListNormal",
        "rowTemplate": CopyFlexContainer00204013b520742,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "d1d1d100",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyFlexContainer00204013b520742": "CopyFlexContainer00204013b520742",
            "lblChevron": "lblChevron",
            "lblSurveyName": "lblSurveyName",
            "lblSurveySelected": "lblSurveySelected"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBuyOnline2 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnBuyOnline2",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": "RESET",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopybtnBuyOnline069ef7a9f8bf74c = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "CopybtnBuyOnline069ef7a9f8bf74c",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": "NEXT",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMainContainer.add(flxTopSpacerDND, FlexContainer0b4738c7579e24b, Segment023bc100d5aa64c, btnBuyOnline2, CopybtnBuyOnline069ef7a9f8bf74c);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": "REWARDS PROFILE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderTitleContainer.add(lblFormTitle, lblChevron, btnHeaderLeft);
    frmRewardsProfileStep3.add(flxMainContainer, flxHeaderTitleContainer);
};

function frmRewardsProfileStep3Globals() {
    frmRewardsProfileStep3 = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRewardsProfileStep3,
        "enabledForIdleTimeout": true,
        "id": "frmRewardsProfileStep3",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};