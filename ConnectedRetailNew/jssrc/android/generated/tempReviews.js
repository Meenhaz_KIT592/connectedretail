function initializetempReviews() {
    flxReviewContainerWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxReviewContainerWrap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxReviewContainerWrap.setDefaultUnit(kony.flex.DP);
    var flxReviewContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxReviewContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%"
    }, {}, {});
    flxReviewContainer.setDefaultUnit(kony.flex.DP);
    var flxItemStock = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxItemStock",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemStock.setDefaultUnit(kony.flex.DP);
    var flxStarRating = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStarRating",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100dp"
    }, {}, {});
    flxStarRating.setDefaultUnit(kony.flex.DP);
    var lblStar1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrgActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrgActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar3",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrgActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar4",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrgActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar5",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStarRating.add(lblStar1, lblStar2, lblStar3, lblStar4, lblStar5);
    flxItemStock.add(flxStarRating);
    var lblReviewTitle = new kony.ui.Label({
        "id": "lblReviewTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxBlackF",
        "text": "Title",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReviewComment = new kony.ui.Label({
        "id": "lblReviewComment",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24pxDark",
        "text": "Review Comment",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxReivewerWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxReivewerWrap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%"
    }, {}, {});
    flxReivewerWrap.setDefaultUnit(kony.flex.DP);
    var lblReviewDate = new kony.ui.Label({
        "id": "lblReviewDate",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": "Date",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var rchtxtNameLoc = new kony.ui.RichText({
        "id": "rchtxtNameLoc",
        "isVisible": true,
        "left": "0dp",
        "linkSkin": "sknRTGothamBold24pxDark",
        "skin": "sknRTGothamRegular24pxDark",
        "text": "<a href=\"javascript:void()\">Name</a> Place",
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxReivewerWrap.add(lblReviewDate, rchtxtNameLoc);
    var flxSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpacer.setDefaultUnit(kony.flex.DP);
    flxSpacer.add();
    flxReviewContainer.add(flxItemStock, lblReviewTitle, lblReviewComment, flxReivewerWrap, flxSpacer);
    flxReviewContainerWrap.add(flxReviewContainer);
}