function addWidgetsfrmStoreMap() {
    frmStoreMap.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "53dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxMyStoreHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxMyStoreHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyStoreHeader.setDefaultUnit(kony.flex.DP);
    var lblChevron = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "lblChevron",
        "isVisible": false,
        "right": "2%",
        "skin": "sknLblIcon32pxWhite",
        "text": "U",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStoreName = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblStoreName",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamRegular24pxWhite",
        "text": "Prestonwood Town Center",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChange = new kony.ui.Label({
        "height": "100%",
        "id": "lblChange",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnChange"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 5],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMyStore = new kony.ui.Button({
        "focusSkin": "sknBtnTrans32pxWhiteFocus",
        "height": "100%",
        "id": "btnMyStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTrans32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterHamMenu.lblMyStore"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 5],
        "paddingInPixel": false
    }, {});
    var btnChooseStore = new kony.ui.Button({
        "focusSkin": "sknBtnTrans32pxWhiteFocus",
        "height": "100%",
        "id": "btnChooseStore",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTrans32pxWhite",
        "text": "CHOOSE A STORE NEAR YOU",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMyStoreHeader.add(lblChevron, lblStoreName, lblChange, btnMyStore, btnChooseStore);
    var flxSearchShadow1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "44dp",
        "id": "flxSearchShadow1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSearchShadow1.setDefaultUnit(kony.flex.DP);
    var lblPrev = new kony.ui.Label({
        "centerY": "50%",
        "height": "17dp",
        "id": "lblPrev",
        "isVisible": true,
        "left": "9dp",
        "skin": "sknLblIcon28pxBlack",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtLeft = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "txtLeft",
        "isVisible": true,
        "skin": "sknLblGothamMedium28pxDark",
        "text": "SHOPPING LIST",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtShoppingList = new kony.ui.Label({
        "centerY": "50%",
        "id": "txtShoppingList",
        "isVisible": true,
        "left": "27dp",
        "skin": "sknLblGothamMedium28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmStoreMap.previ"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var txtRight = new kony.ui.Label({
        "centerY": "50%",
        "id": "txtRight",
        "isVisible": true,
        "right": "30dp",
        "skin": "sknLblGothamMedium28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnNext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNext = new kony.ui.Label({
        "centerY": "50%",
        "height": "17dp",
        "id": "lblNext",
        "isVisible": true,
        "right": "9dp",
        "skin": "sknLblIcon28pxBlack",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "lblLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "21dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblRight = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "lblRight",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSearchShadow1.add(lblPrev, txtLeft, txtShoppingList, txtRight, lblNext, lblLeft, lblRight);
    var flxStoreMap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxStoreMap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "126dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 4
    }, {}, {});
    flxStoreMap.setDefaultUnit(kony.flex.DP);
    var browserDepartments = new kony.ui.Browser({
        "bottom": "0dp",
        "detectTelNumber": false,
        "enableZoom": true,
        "htmlString": "Departments map appear here",
        "id": "browserDepartments",
        "isVisible": false,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    var btnCancel = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxRed",
        "id": "btnCancel",
        "isVisible": false,
        "right": "5dp",
        "skin": "sknBtnIcon36pxRed",
        "text": "X ",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 2, 0],
        "paddingInPixel": false
    }, {});
    var flxLeft = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxLeft",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "50%",
        "width": "40dp",
        "zIndex": 1
    }, {}, {});
    flxLeft.setDefaultUnit(kony.flex.DP);
    var btnL4 = new kony.ui.Button({
        "focusSkin": "sknBtnSecondaryGrayFoc",
        "height": "30dp",
        "id": "btnL4",
        "isVisible": true,
        "left": 0,
        "skin": "sknBtnSecondaryGray",
        "text": "L-3",
        "top": 0,
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnL3 = new kony.ui.Button({
        "focusSkin": "sknBtnSecondaryGrayFoc",
        "height": "30dp",
        "id": "btnL3",
        "isVisible": true,
        "left": 0,
        "skin": "sknBtnSecondaryGray",
        "text": "L-2",
        "top": 10,
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnL2 = new kony.ui.Button({
        "focusSkin": "sknBtnSecondaryGrayFoc",
        "height": "30dp",
        "id": "btnL2",
        "isVisible": true,
        "left": 0,
        "skin": "sknBtnSecondaryGray",
        "text": "L-1",
        "top": 10,
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnL1 = new kony.ui.Button({
        "focusSkin": "sknBtnSecondaryRedFoc",
        "height": "30dp",
        "id": "btnL1",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBtnSecondaryRed",
        "text": "L-0",
        "top": 10,
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLeft.add(btnL4, btnL3, btnL2, btnL1);
    var flxCancel = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxCancel",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "5dp",
        "skin": "slFbox",
        "top": "10dp",
        "width": "30dp",
        "zIndex": 4
    }, {}, {});
    flxCancel.setDefaultUnit(kony.flex.DP);
    flxCancel.add();
    var imgCancel = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgCancel",
        "isVisible": true,
        "right": "10dp",
        "skin": "slImage",
        "src": "crossmark.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStoreMap.add(browserDepartments, btnCancel, flxLeft, flxCancel, imgCancel);
    var lblNoStoreMap = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "id": "lblNoStoreMap",
        "isVisible": true,
        "left": "119dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmStoreMap.storeMapNotAvailable"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "240dp",
        "width": "80%",
        "zIndex": 4
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMainContainer.add(flxMyStoreHeader, flxSearchShadow1, flxStoreMap, lblNoStoreMap);
    var flxButtons = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "54dp",
        "id": "flxButtons",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxButtons.setDefaultUnit(kony.flex.DP);
    var btnDepartments = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnSecondaryRedFoc",
        "height": "30dp",
        "id": "btnDepartments",
        "isVisible": true,
        "left": "3%",
        "onClick": AS_Button_5c4e5dbecee14702a411eae12cd55183,
        "skin": "sknBtnSecondaryRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.btnDepartments"),
        "width": "45.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAisles = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnSecondaryGrayFoc",
        "height": "30dp",
        "id": "btnAisles",
        "isVisible": true,
        "onClick": AS_Button_66c6bb1dd91b4c58845c8ecc8d0d580d,
        "right": "3%",
        "skin": "sknBtnSecondaryGray",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.btnAisles"),
        "width": "45.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxButtons.add(btnDepartments, btnAisles);
    frmStoreMap.add(flxMainContainer, flxButtons);
};

function frmStoreMapGlobals() {
    frmStoreMap = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmStoreMap,
        "enabledForIdleTimeout": true,
        "id": "frmStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};