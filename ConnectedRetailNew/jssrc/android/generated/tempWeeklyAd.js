function initializetempWeeklyAd() {
    flxWeeklyAdRowContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxWeeklyAdRowContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxWeeklyAdRowContainer.setDefaultUnit(kony.flex.DP);
    var flxWeeklyAdHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknFlexBgRed",
        "height": "32dp",
        "id": "flxWeeklyAdHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flxWeeklyAdHeader.setDefaultUnit(kony.flex.DP);
    var lblNewExclusive = new kony.ui.Label({
        "height": "100%",
        "id": "lblNewExclusive",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "Testes",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxWeeklyAdHeader.add(lblNewExclusive);
    var flxWeeklyAdContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxWeeklyAdContents",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWeeklyAdContents.setDefaultUnit(kony.flex.DP);
    var lblAdditionalDeal = new kony.ui.Label({
        "height": "20dp",
        "id": "lblAdditionalDeal",
        "isVisible": true,
        "left": "10dp",
        "skin": "CopyslLabel0ff95af497fd142",
        "text": "BUY 1 GET 1 FREE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxContents.setDefaultUnit(kony.flex.DP);
    var imgProduct = new kony.ui.Image2({
        "height": "100dp",
        "id": "imgProduct",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "10dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLabels = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxLabels",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "110dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "14dp",
        "zIndex": 1
    }, {}, {});
    flxLabels.setDefaultUnit(kony.flex.DP);
    var lblProductName = new kony.ui.Label({
        "id": "lblProductName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxDark",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRegPrice = new kony.ui.Label({
        "id": "lblRegPrice",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxLtGray",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPrice = new kony.ui.Label({
        "id": "lblPrice",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStoreInfo = new kony.ui.Label({
        "id": "lblStoreInfo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium20pxDark",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDateRange = new kony.ui.Label({
        "id": "lblDateRange",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24px818181",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLabels.add(lblProductName, lblRegPrice, lblPrice, lblStoreInfo, lblDateRange);
    flxContents.add(imgProduct, flxLabels);
    var flxSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpacer.setDefaultUnit(kony.flex.DP);
    flxSpacer.add();
    flxWeeklyAdContents.add(lblAdditionalDeal, flxContents, flxSpacer);
    var flxBottomSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxBottomSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacer.setDefaultUnit(kony.flex.DP);
    flxBottomSpacer.add();
    flxWeeklyAdRowContainer.add(flxWeeklyAdHeader, flxWeeklyAdContents, flxBottomSpacer);
}