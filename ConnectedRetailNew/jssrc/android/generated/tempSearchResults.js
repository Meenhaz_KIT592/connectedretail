function initializetempSearchResults() {
    flxSearchResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSearchResults",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxSearchResults.setDefaultUnit(kony.flex.DP);
    var lblSearchResults = new kony.ui.Label({
        "height": "100%",
        "id": "lblSearchResults",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular32pxDarkGray",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [10, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSearchResultsCount = new kony.ui.Label({
        "height": "100%",
        "id": "lblSearchResultsCount",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblGothamRegular32pxBlue",
        "text": "Count",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [1, 0, 5, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSearchResults.add(lblSearchResults, lblSearchResultsCount);
}