function initializetempShoppingList() {
    flxShoppingListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxShoppingListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxShoppingListContainer.setDefaultUnit(kony.flex.DP);
    var flxShoppingListInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxShoppingListInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "130%",
        "zIndex": 1
    }, {}, {});
    flxShoppingListInner.setDefaultUnit(kony.flex.DP);
    var flxShoppingListContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxShoppingListContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "77%",
        "zIndex": 1
    }, {}, {});
    flxShoppingListContents.setDefaultUnit(kony.flex.DP);
    var imgProduct = new kony.ui.Image2({
        "centerY": "50%",
        "height": "50dp",
        "id": "imgProduct",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "15dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "50dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblProductName = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblProductName",
        "isVisible": true,
        "left": "75dp",
        "skin": "sknLblGothamMedium24px818181",
        "text": "Label",
        "textStyle": {
            "lineSpacing": 8,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "48%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxStockAisleInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStockAisleInfo",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "31%",
        "zIndex": 1
    }, {}, {});
    flxStockAisleInfo.setDefaultUnit(kony.flex.DP);
    var lblStockInfo = new kony.ui.Label({
        "height": "100%",
        "id": "lblStockInfo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium18px646464",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAisleInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAisleInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {}, {});
    flxAisleInfo.setDefaultUnit(kony.flex.DP);
    var flxSeparator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "70%",
        "id": "flxSeparator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "1dp",
        "zIndex": 1
    }, {}, {});
    flxSeparator.setDefaultUnit(kony.flex.DP);
    flxSeparator.add();
    var lblAisle = new kony.ui.Label({
        "height": "50%",
        "id": "lblAisle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblGothamMedium20pxBlue",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAisleNo = new kony.ui.Label({
        "height": "48%",
        "id": "lblAisleNo",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblGothamMedium24pxBlue",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "52%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAisleInfo.add(flxSeparator, lblAisle, lblAisleNo);
    flxStockAisleInfo.add(lblStockInfo, flxAisleInfo);
    flxShoppingListContents.add(imgProduct, lblProductName, flxStockAisleInfo);
    var btnDelete = new kony.ui.Button({
        "focusSkin": "sknBtnBgRedBold28pxWhiteFoc",
        "height": "100%",
        "id": "btnDelete",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBtnBgRedBold28pxWhite",
        "text": "Button",
        "top": "0dp",
        "width": "23%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxShoppingListInner.add(flxShoppingListContents, btnDelete);
    var FlexContainer0e885e995a5cc4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "FlexContainer0e885e995a5cc4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgE0E0E0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0e885e995a5cc4e.setDefaultUnit(kony.flex.DP);
    FlexContainer0e885e995a5cc4e.add();
    flxShoppingListContainer.add(flxShoppingListInner, FlexContainer0e885e995a5cc4e);
}