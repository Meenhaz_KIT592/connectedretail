function initializetempEvents() {
    flxEventContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxEventContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxEventContainer.setDefaultUnit(kony.flex.DP);
    var imgEvent = new kony.ui.Image2({
        "height": "70dp",
        "id": "imgEvent",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "10dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLabels = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxLabels",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "90dp",
        "isModalContainer": false,
        "right": "30dp",
        "skin": "slFbox",
        "top": "10dp",
        "zIndex": 1
    }, {}, {});
    flxLabels.setDefaultUnit(kony.flex.DP);
    var lblEventName = new kony.ui.Label({
        "id": "lblEventName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": "Label Label Label Label Label Label Label Label Label Label Label Label ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEventDate = new kony.ui.Label({
        "bottom": "15dp",
        "height": "15dp",
        "id": "lblEventDate",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxDark",
        "text": "Sat Feb 25,2017 01:30 PM -",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEventTime = new kony.ui.Label({
        "bottom": "0dp",
        "height": "20dp",
        "id": "lblEventTime",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxDark",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLabels.add(lblEventName, lblEventDate, lblEventTime);
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblIcon34px333333",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "20dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0e885e995a5cc4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "FlexContainer0e885e995a5cc4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgE0E0E0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0e885e995a5cc4e.setDefaultUnit(kony.flex.DP);
    FlexContainer0e885e995a5cc4e.add();
    flxEventContainer.add(imgEvent, flxLabels, lblChevron, FlexContainer0e885e995a5cc4e);
}