function initializetempProdList() {
    CopyflxProductItem0bcc8e4a5dec444 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyflxProductItem0bcc8e4a5dec444",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxProductItem0bcc8e4a5dec444.setDefaultUnit(kony.flex.DP);
    var FlexGroup0b8478a21c4a44a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexGroup0b8478a21c4a44a",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "top": "10dp",
        "width": "100%"
    }, {}, {});
    FlexGroup0b8478a21c4a44a.setDefaultUnit(kony.flex.DP);
    var flxItemDetailsWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemDetailsWrap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemDetailsWrap.setDefaultUnit(kony.flex.DP);
    var imgItem = new kony.ui.Image2({
        "height": "100dp",
        "id": "imgItem",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "notavailable_1012x628.png",
        "top": "10dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxItemDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "120dp",
        "isModalContainer": false,
        "right": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "zIndex": 1
    }, {}, {});
    flxItemDetails.setDefaultUnit(kony.flex.DP);
    var lblItemName = new kony.ui.Label({
        "id": "lblItemName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxDark",
        "text": "Complete Art Easel & Portfolio Set By Artist’s Loft",
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemPrice = new kony.ui.Label({
        "id": "lblItemPrice",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": "$26.99  - $29.99",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemPriceOld = new kony.ui.Label({
        "id": "lblItemPriceOld",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxLtGray",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxItemStock = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemStock",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemStock.setDefaultUnit(kony.flex.DP);
    var lblItemStock = new kony.ui.Label({
        "height": "15dp",
        "id": "lblItemStock",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamMedium20pxDark",
        "text": "12 IN STOCK",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxItemStock.add(lblItemStock);
    flxItemDetails.add(lblItemName, lblItemPrice, lblItemPriceOld, flxItemStock);
    var flxStarRating = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStarRating",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "94dp",
        "width": "110dp"
    }, {}, {});
    flxStarRating.setDefaultUnit(kony.flex.DP);
    var lblStar1 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar2 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar2",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStar",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar3 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar3",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar4 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar4",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStar",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar5 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar5",
        "isVisible": false,
        "right": "1dp",
        "skin": "sknLblIconStar",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStar1 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar1",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar2 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar2",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar3 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar3",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar4 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar4",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar5 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar5",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "13%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblItemNumberReviews = new kony.ui.Label({
        "height": "15dp",
        "id": "lblItemNumberReviews",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblGothamMedium20pxDark",
        "text": "(88)   ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStarRating.add(lblStar1, lblStar2, lblStar3, lblStar4, lblStar5, imgStar1, imgStar2, imgStar3, imgStar4, imgStar5, lblItemNumberReviews);
    var flxAddtoList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxAddtoList",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "120dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "87dp",
        "width": "30%"
    }, {}, {});
    flxAddtoList.setDefaultUnit(kony.flex.DP);
    var lblAddtolist = new kony.ui.Label({
        "height": "15dp",
        "id": "lblAddtolist",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxBlue",
        "text": "+",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAddtoList = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMed18pxBlueFoc",
        "height": "30dp",
        "id": "btnAddtoList",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnGothamMed18pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempProdList.btnAddtoList"),
        "top": "0dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {});
    flxAddtoList.add(lblAddtolist, btnAddtoList);
    flxItemDetailsWrap.add(imgItem, flxItemDetails, flxStarRating, flxAddtoList);
    var flxItemLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "35dp",
        "id": "flxItemLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemLocation.setDefaultUnit(kony.flex.DP);
    var lblAisleNo1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblAisleNo1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempProdList.lblAisleNo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMapIcon = new kony.ui.Label({
        "height": "100%",
        "id": "lblMapIcon",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon40pxBlue",
        "text": "c",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnStoreMap = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnStoreMap",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempProdList.btnStoreMap"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 12, 0],
        "paddingInPixel": false
    }, {});
    var btnBuyOnline = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnBuyOnline",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnBuyOnline"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    var lblAisleNo = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlue",
        "height": "100%",
        "id": "lblAisleNo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": "AISLE 55",
        "top": "0dp",
        "width": "45%",
        "zIndex": 4
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxItemLocation.add(lblAisleNo1, lblMapIcon, btnStoreMap, btnBuyOnline, lblAisleNo);
    FlexGroup0b8478a21c4a44a.add(flxItemDetailsWrap, flxItemLocation);
    CopyflxProductItem0bcc8e4a5dec444.add(FlexGroup0b8478a21c4a44a);
}