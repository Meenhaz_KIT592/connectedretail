function initializetempCategories() {
    flxCategoryList = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxCategoryList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    flxCategoryList.setDefaultUnit(kony.flex.DP);
    var lblCategoryName = new kony.ui.Label({
        "height": "100%",
        "id": "lblCategoryName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular32pxDarkGray",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 15, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCategoryCount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCategoryCount",
        "isVisible": true,
        "right": "5dp",
        "skin": "sknLblIcon32pxLIghtGray",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAisle = new kony.ui.Label({
        "centerY": "33%",
        "id": "lblAisle",
        "isVisible": true,
        "right": "5dp",
        "skin": "sknLblGothamMedium20pxBlue",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "23%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNumber = new kony.ui.Label({
        "centerY": "67%",
        "id": "lblNumber",
        "isVisible": true,
        "right": "5dp",
        "skin": "sknLblGothamRegular28pxBlue",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "17%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxWhiteLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxWhiteLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWhiteLine.setDefaultUnit(kony.flex.DP);
    flxWhiteLine.add();
    var btnAisle = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "btnAisle",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCategoryList.add(lblCategoryName, lblCategoryCount, lblAisle, lblNumber, flxWhiteLine, btnAisle);
}