function addWidgetsfrmImageOverlay() {
    frmImageOverlay.setDefaultUnit(kony.flex.DP);
    var flexHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flexHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flexHeader.setDefaultUnit(kony.flex.DP);
    var flexBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flexBack.setDefaultUnit(kony.flex.DP);
    var imgBack = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_back.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexBack.add(imgBack);
    var flexInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": 0,
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flexInfo.setDefaultUnit(kony.flex.DP);
    var imgInfo = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgInfo",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_info.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexInfo.add(imgInfo);
    flexHeader.add(flexBack, flexInfo);
    var flexCrossHairs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "45%",
        "id": "flexCrossHairs",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgTrans",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flexCrossHairs.setDefaultUnit(kony.flex.DP);
    var imgCrossHairs = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCrossHairs",
        "isVisible": true,
        "left": 0,
        "skin": "slImage",
        "src": "crosshairs.png",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexCrossHairs.add(imgCrossHairs);
    var flexFooter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "75dp",
        "id": "flexFooter",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexImgSearchFooter",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexFooter.setDefaultUnit(kony.flex.DP);
    var flexGallery = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGallery",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "100dp",
        "zIndex": 1
    }, {}, {});
    flexGallery.setDefaultUnit(kony.flex.DP);
    var imgGallery = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "40%",
        "height": "26dp",
        "id": "imgGallery",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "icon_gallery.png",
        "top": "0dp",
        "width": "35dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblGalleryFooter = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblGalleryFooter",
        "isVisible": true,
        "skin": "sknLblGothamMedium10ptWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageSearch.GalleryLbl"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGallery.add(imgGallery, lblGalleryFooter);
    flexFooter.add(flexGallery);
    var flexSnap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "88%",
        "clipBounds": true,
        "height": "75dp",
        "id": "flexSnap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRound",
        "width": "75dp",
        "zIndex": 1
    }, {}, {});
    flexSnap.setDefaultUnit(kony.flex.DP);
    var flexSnapInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flexSnapInner",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRoundInner",
        "width": "60dp",
        "zIndex": 2
    }, {}, {});
    flexSnapInner.setDefaultUnit(kony.flex.DP);
    flexSnapInner.add();
    var btnSnap = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknBtnRedRoundFoc",
        "height": "55dp",
        "id": "btnSnap",
        "isVisible": true,
        "skin": "sknBtnRedRound",
        "width": "55dp",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexSnap.add(flexSnapInner, btnSnap);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialog = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDialog",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "13%",
        "isModalContainer": false,
        "right": "13%",
        "skin": "sknFlxWhiteBg5pxBorderRadius",
        "top": "25%",
        "width": "74%",
        "zIndex": 1
    }, {}, {});
    flxDialog.setDefaultUnit(kony.flex.DP);
    var lblPopUpMessage = new kony.ui.Label({
        "id": "lblPopUpMessage",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknLblImageSearchPopupMessage",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageOverlay.popUpMessage"),
        "textStyle": {
            "lineSpacing": 10,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnGotIt = new kony.ui.Button({
        "focusSkin": "sknBtnRedBg5pxBorderRadius",
        "height": "50dp",
        "id": "btnGotIt",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknBtnRedBg5pxBorderRadius",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageOverlay.popUpButtonText"),
        "top": "20dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblGapDialog = new kony.ui.Label({
        "height": "1dp",
        "id": "lblGapDialog",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDialog.add(lblPopUpMessage, btnGotIt, lblGapDialog);
    flxOverlay.add(flxDialog);
    var flexImageSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flexImageSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100dp",
        "isModalContainer": false,
        "right": "-100dp",
        "skin": "sknFlexBgTrans",
        "top": "-100dp",
        "zIndex": 1
    }, {}, {});
    flexImageSearch.setDefaultUnit(kony.flex.DP);
    var flexBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransImageSearch",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexBody.setDefaultUnit(kony.flex.DP);
    var imgImageSearch = new kony.ui.Image2({
        "bottom": "0dp",
        "id": "imgImageSearch",
        "isVisible": true,
        "left": 0,
        "skin": "slImage",
        "src": "white_1012x628.png",
        "top": 0,
        "width": "100%",
        "zIndex": 2
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSearchStatus = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblSearchStatus",
        "isVisible": true,
        "skin": "sknLblGothamMedium36pxWhite",
        "text": "Processing...",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexBody.add(imgImageSearch, lblSearchStatus);
    var customAnimator = new customWidget.CustomAnimWidget({
        "id": "customAnimator",
        "isVisible": false,
        "width": "200dp",
        "height": "200dp",
        "centerX": "50.00%",
        "centerY": "52.68%",
        "zIndex": 5
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "widgetName": "CustomAnimWidget"
    });
    var lblAndSearchStatus = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblAndSearchStatus",
        "isVisible": false,
        "skin": "sknLblGothamMedium36pxWhite",
        "text": "Processing...",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexStatusBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "180dp",
        "id": "flexStatusBar",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRound",
        "width": "180dp",
        "zIndex": 3
    }, {}, {});
    flexStatusBar.setDefaultUnit(kony.flex.DP);
    var flex1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex1.setDefaultUnit(kony.flex.DP);
    var flexInner1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner1.setDefaultUnit(kony.flex.DP);
    flexInner1.add();
    flex1.add(flexInner1);
    var flex2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex2.setDefaultUnit(kony.flex.DP);
    var flexInner2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner2.setDefaultUnit(kony.flex.DP);
    flexInner2.add();
    flex2.add(flexInner2);
    var flex3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex3.setDefaultUnit(kony.flex.DP);
    var flexInner3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner3.setDefaultUnit(kony.flex.DP);
    flexInner3.add();
    flex3.add(flexInner3);
    var flex4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex4.setDefaultUnit(kony.flex.DP);
    var flexInner4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner4.setDefaultUnit(kony.flex.DP);
    flexInner4.add();
    flex4.add(flexInner4);
    var flexInnerRing = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "155dp",
        "id": "flexInnerRing",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRound",
        "width": "155dp",
        "zIndex": null
    }, {}, {});
    flexInnerRing.setDefaultUnit(kony.flex.DP);
    var imgInnerRing = new kony.ui.Image2({
        "height": "100%",
        "id": "imgInnerRing",
        "isVisible": true,
        "left": 0,
        "skin": "slImage",
        "src": "white_1012x628.png",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexInnerRing.add(imgInnerRing);
    flexStatusBar.add(flex1, flex2, flex3, flex4, flexInnerRing);
    var flexErrorMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexErrorMessage",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flexErrorMessage.setDefaultUnit(kony.flex.DP);
    var flexMessageContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "53%",
        "clipBounds": true,
        "id": "flexMessageContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlxWhiteBg5pxBorderRadius",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flexMessageContainer.setDefaultUnit(kony.flex.DP);
    var lblErrorMessage = new kony.ui.Label({
        "id": "lblErrorMessage",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknLblImageSearchErroMessage",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageSearch.errorMessage"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnTryAgain = new kony.ui.Button({
        "focusSkin": "sknBtnRedBgCustomFoc",
        "height": "50dp",
        "id": "btnTryAgain",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknBtnRedBgCustom5px",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageSearch.tryAgain"),
        "top": "20dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCancel = new kony.ui.Button({
        "focusSkin": "sknBtnTransMedGreyFoc",
        "height": "50dp",
        "id": "btnCancel",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknBtnTransMedGrey",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.smallCancel"),
        "top": "5dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblGap = new kony.ui.Label({
        "height": "1dp",
        "id": "lblGap",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexMessageContainer.add(lblErrorMessage, btnTryAgain, btnCancel, lblGap);
    flexErrorMessage.add(flexMessageContainer);
    flexImageSearch.add(flexBody, customAnimator, lblAndSearchStatus, flexStatusBar, flexErrorMessage);
    frmImageOverlay.add(flexHeader, flexCrossHairs, flexFooter, flexSnap, flxOverlay, flexImageSearch);
};

function frmImageOverlayGlobals() {
    frmImageOverlay = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmImageOverlay,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmImageOverlay",
        "init": AS_Form_fe3434d6dffa4639a7b837893fe782ea,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormBgTrans"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};