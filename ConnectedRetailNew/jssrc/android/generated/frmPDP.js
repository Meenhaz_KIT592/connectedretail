function addWidgetsfrmPDP() {
    frmPDP.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "54dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCategory.setDefaultUnit(kony.flex.DP);
    var imgCategoryImg = new kony.ui.Image2({
        "id": "imgCategoryImg",
        "imageWhenFailed": "notavailable_1080x142.png",
        "imageWhileDownloading": "white_1080x142.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "white_1080x142.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCategoryName = new kony.ui.Button({
        "focusSkin": "sknBtnCategorytabMain",
        "height": "100%",
        "id": "btnCategoryName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnCategorytabMain",
        "text": "Art Supplies",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [8, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    var lblCategoryBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblCategoryBack",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon34px000000",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCategory.add(imgCategoryImg, btnCategoryName, lblCategoryBack);
    var flxBtnBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxBtnBack",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "top": "100dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBtnBack.setDefaultUnit(kony.flex.DP);
    var lblIconBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblIconBack",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBackTab = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg16pxWhiteFoc",
        "height": "100%",
        "id": "btnBackTab",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS__eb55c2f3a58f405590d6c8083a91a713,
        "skin": "sknBtnTransReg16pxWhite",
        "text": "Back",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [10, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBtnBack.add(lblIconBack, btnBackTab);
    var flexSeg = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flexSeg",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "96dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flexSeg.setDefaultUnit(kony.flex.DP);
    var flxProductDetails1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxProductDetails1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "sknFlexBgTransBorderLtGray1234",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxProductDetails1.setDefaultUnit(kony.flex.DP);
    var flxClearanceText = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "flxClearanceText",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray1234",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxClearanceText.setDefaultUnit(kony.flex.DP);
    var CopyflxClearance04dd36e9de23549 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "36dp",
        "id": "CopyflxClearance04dd36e9de23549",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxClearance04dd36e9de23549.setDefaultUnit(kony.flex.DP);
    var CopylblIconBack04eaa8b2f367a48 = new kony.ui.Label({
        "bottom": "8dp",
        "id": "CopylblIconBack04eaa8b2f367a48",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMedium32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.pdp.clearancetext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxClearance04dd36e9de23549.add(CopylblIconBack04eaa8b2f367a48);
    var CopylblIconBack0b2b53bc248d74b = new kony.ui.Label({
        "height": "20dp",
        "id": "CopylblIconBack0b2b53bc248d74b",
        "isVisible": true,
        "left": "10dp",
        "right": "5dp",
        "skin": "sknLblGouthamMed16",
        "text": kony.i18n.getLocalizedString("i18n.phone.pdp.clearacedisclaimer"),
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "39dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxClearanceText.add(CopyflxClearance04dd36e9de23549, CopylblIconBack0b2b53bc248d74b);
    var lblName1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblName1",
        "isVisible": true,
        "skin": "sknLblGothamMedium32pxDark",
        "text": "Prismacolor Premier Soft Cored Colored Pencil",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemNumber = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblItemNumber",
        "isVisible": true,
        "skin": "sknLblgothamBook28Grey",
        "text": "#845160064",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPrice = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxPrice",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxPrice.setDefaultUnit(kony.flex.DP);
    var lblPrice = new kony.ui.Label({
        "id": "lblPrice",
        "isVisible": true,
        "skin": "sknLblGothamMedium52pxDark",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRegPrice = new kony.ui.Label({
        "id": "lblRegPrice",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamMedium32pxLtGray",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPrice.add(lblPrice, lblRegPrice);
    var flxRatingReview = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxRatingReview",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-5dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxRatingReview.setDefaultUnit(kony.flex.DP);
    var flxStarRating = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStarRating",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "33%"
    }, {}, {});
    flxStarRating.setDefaultUnit(kony.flex.DP);
    var lblStarP1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStarP1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarMedActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStarP2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStarP2",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarMedActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStarP3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStarP3",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarMedActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStarP4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStarP4",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarMedActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStarP5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStarP5",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarMed",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStarP1 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStarP1",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStarP2 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStarP2",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStarP3 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStarP3",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStarP4 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStarP4",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStarP5 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStarP5",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStarRating.add(lblStarP1, lblStarP2, lblStarP3, lblStarP4, lblStarP5, imgStarP1, imgStarP2, imgStarP3, imgStarP4, imgStarP5);
    var btnReview = new kony.ui.Button({
        "focusSkin": "sknBtnGothamBold22pxBlueFoc",
        "height": "100%",
        "id": "btnReview",
        "isVisible": true,
        "right": 0,
        "skin": "sknBtnGothamBold22pxBlue",
        "text": "READ/WRITE REVIEWS (6)",
        "top": "0dp",
        "width": "63.60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0bc568f84bac444 = new kony.ui.Label({
        "height": "100%",
        "id": "Label0bc568f84bac444",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblIcon34pxBlue",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRatingReview.add(flxStarRating, btnReview, Label0bc568f84bac444);
    var flxLargThumb = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.01%",
        "clipBounds": true,
        "height": "210dp",
        "id": "flxLargThumb",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxLargThumb.setDefaultUnit(kony.flex.DP);
    var Image0f0a05398956349 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "210dp",
        "id": "Image0f0a05398956349",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "skin": "slImage",
        "src": "pdp_large_thumb.png",
        "width": "286dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnZoom = new kony.ui.Button({
        "focusSkin": "sknBtnIcon50pxRed",
        "height": "36dp",
        "id": "btnZoom",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnIcon50pxRed",
        "text": "z",
        "top": 0,
        "width": "36dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnImageZoom = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "height": "210dp",
        "id": "btnImageZoom",
        "isVisible": true,
        "skin": "sknBtnPopupCloseTransparentBg",
        "width": "286dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxLargThumb.add(Image0f0a05398956349, btnZoom, btnImageZoom);
    var FlexContainer05867041df84c49 = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "32dp",
        "horizontalScrollIndicator": true,
        "id": "FlexContainer05867041df84c49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "top": "10dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer05867041df84c49.setDefaultUnit(kony.flex.DP);
    var flxThumbs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "32dp",
        "id": "flxThumbs",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "120dp",
        "zIndex": 1
    }, {}, {});
    flxThumbs.setDefaultUnit(kony.flex.DP);
    var FlexContainer067018dc9d32f45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "28dp",
        "id": "FlexContainer067018dc9d32f45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "30%",
        "isModalContainer": false,
        "skin": "sknFlxBlackBorder2px",
        "width": "28dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer067018dc9d32f45.setDefaultUnit(kony.flex.DP);
    var Image0fe184711e4e043 = new kony.ui.Image2({
        "height": "100%",
        "id": "Image0fe184711e4e043",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pdp_thumb.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer067018dc9d32f45.add(Image0fe184711e4e043);
    var CopyFlexContainer002fff2e762d942 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "28dp",
        "id": "CopyFlexContainer002fff2e762d942",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "28dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer002fff2e762d942.setDefaultUnit(kony.flex.DP);
    var CopyImage0e706939fb1ff4f = new kony.ui.Image2({
        "height": "100%",
        "id": "CopyImage0e706939fb1ff4f",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pdp_thumb.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer002fff2e762d942.add(CopyImage0e706939fb1ff4f);
    var CopyFlexContainer0dfd2eb13018549 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "28dp",
        "id": "CopyFlexContainer0dfd2eb13018549",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "28dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0dfd2eb13018549.setDefaultUnit(kony.flex.DP);
    var CopyImage09b183313d86645 = new kony.ui.Image2({
        "height": "100%",
        "id": "CopyImage09b183313d86645",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pdp_thumb.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0dfd2eb13018549.add(CopyImage09b183313d86645);
    flxThumbs.add(FlexContainer067018dc9d32f45, CopyFlexContainer002fff2e762d942, CopyFlexContainer0dfd2eb13018549);
    FlexContainer05867041df84c49.add(flxThumbs);
    var flxVarients = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxVarients",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "15dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxVarients.setDefaultUnit(kony.flex.DP);
    var listboxColor = new kony.ui.ListBox({
        "centerX": "50%",
        "focusSkin": "sknListboxMedium24px1pxBdr",
        "id": "listboxColor",
        "isVisible": true,
        "masterData": [
            ["lb1", "COLORS"],
            ["lb2", "RED"],
            ["lb3", "GREEN"],
            ["lb4", "BLUE"],
            ["lb5", "YELLOW"],
            ["lb6", "BLACK"]
        ],
        "selectedKey": "lb1",
        "selectedKeyValue": ["lb1", "COLORS"],
        "skin": "sknListboxMedium24px1pxBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "dropDownImage": "dropdown.png",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    flxVarients.add(listboxColor);
    var btnAddToShoppingList = new kony.ui.Button({
        "bottom": "10dp",
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnAddToShoppingList",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.btnAddToShoppingList"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBuyOnline2 = new kony.ui.Button({
        "bottom": "17dp",
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnBuyOnline2",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.shopOnline"),
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblFreeShipping = new kony.ui.Label({
        "bottom": 17,
        "centerX": "50%",
        "id": "lblFreeShipping",
        "isVisible": false,
        "skin": "sknLblGouthamMed28Red",
        "text": "Free Shipping For Orders $49 & Over",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStockInfo = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblStockInfo",
        "isVisible": false,
        "skin": "sknLblGothamMedium28pxDark",
        "text": "5 IN STOCK",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxItemLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "flxItemLocation",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemLocation.setDefaultUnit(kony.flex.DP);
    var lblAisleNo = new kony.ui.Label({
        "height": "100%",
        "id": "lblAisleNo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.btnAisleNum"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMapIcon = new kony.ui.Label({
        "height": "100%",
        "id": "lblMapIcon",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon34pxBlue",
        "text": "e",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnStoreMap = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnStoreMap",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 12, 0],
        "paddingInPixel": false
    }, {});
    var btnBuyOnline = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnBuyOnline",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.buyOnline"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    flxItemLocation.add(lblAisleNo, lblMapIcon, btnStoreMap, btnBuyOnline);
    flxProductDetails1.add(flxClearanceText, lblName1, lblItemNumber, flxPrice, flxRatingReview, flxLargThumb, FlexContainer05867041df84c49, flxVarients, btnAddToShoppingList, btnBuyOnline2, lblFreeShipping, lblStockInfo, flxItemLocation);
    var flxStore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxStore",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexEventTab",
        "top": "-1dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxStore.setDefaultUnit(kony.flex.DP);
    var flxMyStoreHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "55dp",
        "id": "flxMyStoreHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyStoreHeader.setDefaultUnit(kony.flex.DP);
    var lblChevron = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "lblChevron",
        "isVisible": false,
        "right": "2%",
        "skin": "sknLblIcon32pxWhite",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStoreName = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblStoreName",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamRegular24pxWhite",
        "text": "Prestonwood Town Center",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "86%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChange = new kony.ui.Label({
        "height": "100%",
        "id": "lblChange",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "CHANGE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 5],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMyStore = new kony.ui.Button({
        "focusSkin": "sknBtnTrans32pxWhiteFocus",
        "height": "100%",
        "id": "btnMyStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTrans32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.btnMyStore"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 4],
        "paddingInPixel": false
    }, {});
    var btnChooseStore = new kony.ui.Button({
        "focusSkin": "sknBtnTrans32pxWhiteFocus",
        "height": "100%",
        "id": "btnChooseStore",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTrans32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.btnChooseStore"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMyStoreHeader.add(lblChevron, lblStoreName, lblChange, btnMyStore, btnChooseStore);
    var flxInventoryLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "flxInventoryLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxInventoryLocation.setDefaultUnit(kony.flex.DP);
    var lblOnlineStock = new kony.ui.Label({
        "height": "100%",
        "id": "lblOnlineStock",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.inStock"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMapLocation = new kony.ui.Label({
        "height": "100%",
        "id": "lblMapLocation",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon34pxBlue",
        "text": "e",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnAisleNum = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnAisleNum",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 12, 0],
        "paddingInPixel": false
    }, {});
    var CopybtnBuyOnline06f32e025294848 = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "CopybtnBuyOnline06f32e025294848",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.buyOnline"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    flxInventoryLocation.add(lblOnlineStock, lblMapLocation, btnAisleNum, CopybtnBuyOnline06f32e025294848);
    flxStore.add(flxMyStoreHeader, flxInventoryLocation);
    var checkNearByStores = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "40dp",
        "id": "checkNearByStores",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnBlueBorder",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.checkNearByStores"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var rtDescription = new kony.ui.RichText({
        "centerX": "50%",
        "id": "rtDescription",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBlack86",
        "text": "This premium pencil features a creamy, soft core of color that’s perfect for blending or shading. A thick core of color lasts long to bring bold statements to life, while a soft lead makes it the best tool to blend in or shade away. Vivid and intense in one stroke and subtle and soft in the next, this pencil is perfect for any project, big or small.",
        "top": "20dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBrowser = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxBrowser",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "113dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 2
    }, {}, {});
    flxBrowser.setDefaultUnit(kony.flex.DP);
    var brwsrDescription = new kony.ui.Browser({
        "centerX": "50%",
        "detectTelNumber": true,
        "enableZoom": false,
        "htmlString": "Browser",
        "id": "brwsrDescription",
        "isVisible": true,
        "left": "3%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBrowser.add(brwsrDescription);
    var lblDescTemp = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDescTemp",
        "isVisible": false,
        "skin": "sknLblGothamRegular24pxBlack",
        "text": "this is text",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segWarnings = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblWarningDesc": "sjkfs jsdfhskjfhsfhsfhsdkddfjdsfdsfkjsfjdfdfhsjkfdhdkfsjfhsjfhsfjkshfsjfhskfjshfkjdshfjksfskfdshfkjsdfhskdjdfsk",
            "warningImg": "imagedrag.png",
            "warningText": "WARNING"
        }, {
            "lblWarningDesc": "sjkfs jsdfhskjfhsfhsfhsdkddfjdsfdsfkjsfjdfdfhsjkfdhdkfsjfhsjfhsfjkshfsjfhskfjshfkjdshfjksfskfdshfkjsdfhskdjdfsk",
            "warningImg": "imagedrag.png",
            "warningText": "WARNING"
        }, {
            "lblWarningDesc": "sjkfs jsdfhskjfhsfhsfhsdkddfjdsfdsfkjsfjdfdfhsjkfdhdkfsjfhsjfhsfjkshfsjfhskfjshfkjdshfjksfskfdshfkjsdfhskdjdfsk",
            "warningImg": "imagedrag.png",
            "warningText": "WARNING"
        }],
        "groupCells": false,
        "id": "segWarnings",
        "isVisible": true,
        "left": "10dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": FlexContainer077c1e8f5d52649,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "10dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyFlexContainer08bef204c42384d": "CopyFlexContainer08bef204c42384d",
            "FlexContainer077c1e8f5d52649": "FlexContainer077c1e8f5d52649",
            "lblWarningDesc": "lblWarningDesc",
            "warningImg": "warningImg",
            "warningText": "warningText"
        },
        "width": "90%",
        "zIndex": 2
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblverbiage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblverbiage",
        "isVisible": true,
        "skin": "sknLbl28pxGothamBookRed",
        "text": "Pricing and participation varies by store.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxShare = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxShare",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxShare.setDefaultUnit(kony.flex.DP);
    var lblIconShare = new kony.ui.Label({
        "height": "100%",
        "id": "lblIconShare",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon42pxBlue",
        "text": "s",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [20, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnShare = new kony.ui.Button({
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "100%",
        "id": "btnShare",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnShare"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {});
    flxShare.add(lblIconShare, btnShare);
    var lblRecommended = new kony.ui.Label({
        "height": "42dp",
        "id": "lblRecommended",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblPDPSubTab",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.recommendedProjects"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segProjectList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "imgDuration": "time_2.png",
            "imgExpertise": "skill_new_2.png",
            "imgProject": "project_thumb.png",
            "lblDuration": "About 30 Minutes",
            "lblExpertiseDesc": "Intermeditate",
            "lblName": "Day of the Dead Sugar Skull"
        }, {
            "imgDuration": "time_2.png",
            "imgExpertise": "skill_new_2.png",
            "imgProject": "project_thumb.png",
            "lblDuration": "About 30 Minutes",
            "lblExpertiseDesc": "Intermeditate",
            "lblName": "Day of the Dead Sugar Skull"
        }, {
            "imgDuration": "time_2.png",
            "imgExpertise": "skill_new_2.png",
            "imgProject": "project_thumb.png",
            "lblDuration": "About 30 Minutes",
            "lblExpertiseDesc": "Intermeditate",
            "lblName": "Day of the Dead Sugar Skull"
        }],
        "groupCells": false,
        "id": "segProjectList",
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Normal",
        "rowSkin": "seg2Normal",
        "rowTemplate": FlexContainer09240b69cf4c54f,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlexContainer09240b69cf4c54f": "FlexContainer09240b69cf4c54f",
            "flxItemLocation": "flxItemLocation",
            "flxProductDetails": "flxProductDetails",
            "flxSkillsLevels": "flxSkillsLevels",
            "imgDuration": "imgDuration",
            "imgExpertise": "imgExpertise",
            "imgProject": "imgProject",
            "lblDuration": "lblDuration",
            "lblExpertiseDesc": "lblExpertiseDesc",
            "lblName": "lblName"
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var ShowMoreProjects = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "30dp",
        "id": "ShowMoreProjects",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.ShowMoreProducts"),
        "top": "10dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblRecomProducts = new kony.ui.Label({
        "height": "42dp",
        "id": "lblRecomProducts",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblPDPSubTab",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.recommendedProducts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var segProductsList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": "10dp",
        "centerX": "50%",
        "data": [{
            "imgItem": "sample_product.png",
            "imgStar1": "imagedrag.png",
            "imgStar2": "imagedrag.png",
            "imgStar3": "imagedrag.png",
            "imgStar4": "imagedrag.png",
            "imgStar5": "imagedrag.png",
            "lblItemName": "Complete Art Easel & Portfolio Set By Artist’s Loft",
            "lblItemNumberReviews": "()",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y"
        }, {
            "imgItem": "sample_product.png",
            "imgStar1": "imagedrag.png",
            "imgStar2": "imagedrag.png",
            "imgStar3": "imagedrag.png",
            "imgStar4": "imagedrag.png",
            "imgStar5": "imagedrag.png",
            "lblItemName": "Complete Art Easel & Portfolio Set By Artist’s Loft",
            "lblItemNumberReviews": "()",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y"
        }, {
            "imgItem": "sample_product.png",
            "imgStar1": "imagedrag.png",
            "imgStar2": "imagedrag.png",
            "imgStar3": "imagedrag.png",
            "imgStar4": "imagedrag.png",
            "imgStar5": "imagedrag.png",
            "lblItemName": "Complete Art Easel & Portfolio Set By Artist’s Loft",
            "lblItemNumberReviews": "()",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y"
        }],
        "groupCells": false,
        "id": "segProductsList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": CopyflxProductItem0677dbad2027649,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "10dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyflxProductItem0677dbad2027649": "CopyflxProductItem0677dbad2027649",
            "FlexContainer082ed50f8d1314b": "FlexContainer082ed50f8d1314b",
            "FlexGroup0b8478a21c4a44a": "FlexGroup0b8478a21c4a44a",
            "flxItemDetails": "flxItemDetails",
            "flxItemDetailsWrap": "flxItemDetailsWrap",
            "flxStarRating": "flxStarRating",
            "imgItem": "imgItem",
            "imgStar1": "imgStar1",
            "imgStar2": "imgStar2",
            "imgStar3": "imgStar3",
            "imgStar4": "imgStar4",
            "imgStar5": "imgStar5",
            "lblItemName": "lblItemName",
            "lblItemNumberReviews": "lblItemNumberReviews",
            "lblStar1": "lblStar1",
            "lblStar2": "lblStar2",
            "lblStar3": "lblStar3",
            "lblStar4": "lblStar4",
            "lblStar5": "lblStar5"
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var ShowMoreProducts = new kony.ui.Button({
        "bottom": "20dp",
        "centerX": "50.00%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "30dp",
        "id": "ShowMoreProducts",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.ShowMoreProducts"),
        "top": "0dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "flxBottomSpacerDND",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flexSeg.add(flxProductDetails1, flxStore, checkNearByStores, rtDescription, flxBrowser, lblDescTemp, segWarnings, lblverbiage, flxShare, lblRecommended, segProjectList, ShowMoreProjects, lblRecomProducts, segProductsList, ShowMoreProducts, flxBottomSpacerDND);
    flxMainContainer.add(flxTopSpacerDND, flxCategory, flxBtnBack, flexSeg);
    var kmdf2399a31e94a7ab3f90450eccf9189 = new kony.ui.FlexScrollContainer({
        "isMaster": true,
        "id": "flxSearchResultsContainer",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    kmdf2399a31e94a7ab3f90450eccf9189.setDefaultUnit(kony.flex.DP);
    var kmc58bf4960ea4eb99eb93d98218254a5 = new kony.ui.FlexContainer({
        "id": "flxTopSpacerDNDSearch",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmc58bf4960ea4eb99eb93d98218254a5.setDefaultUnit(kony.flex.DP);
    kmc58bf4960ea4eb99eb93d98218254a5.add();
    var kmd9025a35fb74d299acaa22045676338 = new kony.ui.SegmentedUI2({
        "id": "segSearchOffers",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km73b9b83a513474b9d343b1425af30bd = new kony.ui.SegmentedUI2({
        "id": "segSearchResults",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmdf2399a31e94a7ab3f90450eccf9189.add(kmc58bf4960ea4eb99eb93d98218254a5, kmd9025a35fb74d299acaa22045676338, km73b9b83a513474b9d343b1425af30bd);
    var flxPinchandZoom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxPinchandZoom",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPinchandZoom.setDefaultUnit(kony.flex.DP);
    var btnCanel = new kony.ui.Button({
        "focusSkin": "sknBtnGothamBold22pxBlueFoc",
        "height": "8%",
        "id": "btnCanel",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamBold22pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.close"),
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 7, 0],
        "paddingInPixel": false
    }, {});
    var flxScrlZoomImages = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "80%",
        "horizontalScrollIndicator": true,
        "id": "flxScrlZoomImages",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "pagingEnabled": true,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "top": "2%",
        "verticalScrollIndicator": true,
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxScrlZoomImages.setDefaultUnit(kony.flex.DP);
    flxScrlZoomImages.add();
    flxPinchandZoom.add(btnCanel, flxScrlZoomImages);
    var kmfef6266e80f4e7f93738a1a533ea5c8 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    kmfef6266e80f4e7f93738a1a533ea5c8.setDefaultUnit(kony.flex.DP);
    var km9ced539efd24c2cae79a62d80a2e282 = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km9ced539efd24c2cae79a62d80a2e282.setDefaultUnit(kony.flex.DP);
    km9ced539efd24c2cae79a62d80a2e282.add();
    var km229d1b526ff454497c617f946cfe363 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km229d1b526ff454497c617f946cfe363.setDefaultUnit(kony.flex.DP);
    var km6a9bc727de547bf8f22a53207ded12e = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3920c71fdc74b7595a36fb79d37ceed = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    var km9b643b8e1ca4fb29f2003394f0a2bf4 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km804628839e74e7d8fe68af0ebbc2456 = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    km804628839e74e7d8fe68af0ebbc2456.setDefaultUnit(kony.flex.DP);
    km804628839e74e7d8fe68af0ebbc2456.add();
    var kmdd11e4333ca4d20bc030bf2d98496d7 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km229d1b526ff454497c617f946cfe363.add(km6a9bc727de547bf8f22a53207ded12e, km3920c71fdc74b7595a36fb79d37ceed, km9b643b8e1ca4fb29f2003394f0a2bf4, km804628839e74e7d8fe68af0ebbc2456, kmdd11e4333ca4d20bc030bf2d98496d7);
    var km3b26315993d44d28f861013ae2d49a2 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km3b26315993d44d28f861013ae2d49a2.setDefaultUnit(kony.flex.DP);
    var kmf54fc1130cb43669c314f654a554f5f = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmf54fc1130cb43669c314f654a554f5f.setDefaultUnit(kony.flex.DP);
    var km85374c357df4780873505d9728e53e8 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km85374c357df4780873505d9728e53e8.setDefaultUnit(kony.flex.DP);
    var kmc33996b9cf24f2ebd5d6a3293cf8c66 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km85374c357df4780873505d9728e53e8.add(kmc33996b9cf24f2ebd5d6a3293cf8c66);
    var km93c9795930a4d94b50d9605e977480b = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    km93c9795930a4d94b50d9605e977480b.setDefaultUnit(kony.flex.DP);
    var kme602b6b98044873a6789be431948301 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km93c9795930a4d94b50d9605e977480b.add(kme602b6b98044873a6789be431948301);
    var km68835056d3b475fa0f834e5d6786e91 = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km68835056d3b475fa0f834e5d6786e91.setDefaultUnit(kony.flex.DP);
    var kme9547616225436995ae7e85fadde73c = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmce33fcbc2644b4dbad147cae786a634 = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km68835056d3b475fa0f834e5d6786e91.add(kme9547616225436995ae7e85fadde73c, kmce33fcbc2644b4dbad147cae786a634);
    var km37f9af93e274d859e39640cbce817ae = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmf54fc1130cb43669c314f654a554f5f.add(km85374c357df4780873505d9728e53e8, km93c9795930a4d94b50d9605e977480b, km68835056d3b475fa0f834e5d6786e91, km37f9af93e274d859e39640cbce817ae);
    var kme219eed9c4e4c7ca48f1e1de4bd696f = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kme219eed9c4e4c7ca48f1e1de4bd696f.setDefaultUnit(kony.flex.DP);
    kme219eed9c4e4c7ca48f1e1de4bd696f.add();
    km3b26315993d44d28f861013ae2d49a2.add(kmf54fc1130cb43669c314f654a554f5f, kme219eed9c4e4c7ca48f1e1de4bd696f);
    var kma049f8176b14097ae29c96a7902cb30 = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kma049f8176b14097ae29c96a7902cb30.setDefaultUnit(kony.flex.DP);
    var km9d2b2a53d3447ea9d2b6225256fbe25 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km9d2b2a53d3447ea9d2b6225256fbe25.setDefaultUnit(kony.flex.DP);
    var kmcb64c7bafd3472bb4fd22c9ea96e058 = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    kmcb64c7bafd3472bb4fd22c9ea96e058.setDefaultUnit(kony.flex.DP);
    var km4dc328dfd2c4c1d8c188f5e40acb726 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km2b8186ed9cc4b53b7a332dc7ddc9b2d = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmcb64c7bafd3472bb4fd22c9ea96e058.add(km4dc328dfd2c4c1d8c188f5e40acb726, km2b8186ed9cc4b53b7a332dc7ddc9b2d);
    var km03e88d7b89145a7b3f1dc7ad75bac0c = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km03e88d7b89145a7b3f1dc7ad75bac0c.setDefaultUnit(kony.flex.DP);
    var kmf5ec45463c74959a50009ee107117bd = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmf5ec45463c74959a50009ee107117bd.setDefaultUnit(kony.flex.DP);
    var km602044e1af54cc4a3ae955ce4f13c86 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km37518fa978b4ea0872438939812d64e = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2acf2aec6b74eafab077c9455dafa61 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbd2f61e80624532a125c170e75dcf8d = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2a02541591e4d9db2de66c92f08dc2d = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3f70e49092c4f74b80422fae3e12605 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf5ec45463c74959a50009ee107117bd.add(km602044e1af54cc4a3ae955ce4f13c86, km37518fa978b4ea0872438939812d64e, km2acf2aec6b74eafab077c9455dafa61, kmbd2f61e80624532a125c170e75dcf8d, km2a02541591e4d9db2de66c92f08dc2d, km3f70e49092c4f74b80422fae3e12605);
    var km50dcad1163941e3802fdc326e69acd3 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km50dcad1163941e3802fdc326e69acd3.setDefaultUnit(kony.flex.DP);
    var kmb18d215990049c6bca5a85c4646d74c = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km57651e54dc94681b90d6a1eded9e2a8 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5db7ca6c51a4cdc9fcdd5589da0ad8f = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km50dcad1163941e3802fdc326e69acd3.add(kmb18d215990049c6bca5a85c4646d74c, km57651e54dc94681b90d6a1eded9e2a8, km5db7ca6c51a4cdc9fcdd5589da0ad8f);
    km03e88d7b89145a7b3f1dc7ad75bac0c.add(kmf5ec45463c74959a50009ee107117bd, km50dcad1163941e3802fdc326e69acd3);
    var kmb3ba45ec0754e8ea8513d7f4734c9bd = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmec699980cd44fdc88bb1a85a32192fc = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km9d2b2a53d3447ea9d2b6225256fbe25.add(kmcb64c7bafd3472bb4fd22c9ea96e058, km03e88d7b89145a7b3f1dc7ad75bac0c, kmb3ba45ec0754e8ea8513d7f4734c9bd, kmec699980cd44fdc88bb1a85a32192fc);
    kma049f8176b14097ae29c96a7902cb30.add(km9d2b2a53d3447ea9d2b6225256fbe25);
    kmfef6266e80f4e7f93738a1a533ea5c8.add(km9ced539efd24c2cae79a62d80a2e282, km229d1b526ff454497c617f946cfe363, km3b26315993d44d28f861013ae2d49a2, kma049f8176b14097ae29c96a7902cb30);
    var flxChatBoxTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxChatBoxTitleContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxChatBoxTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.chatbot.returntojoy"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0ee00528ffdfe4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0ee00528ffdfe4c",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChatBoxTitleContainer.add(lblFormTitle, btnHeaderLeft, Label0ee00528ffdfe4c);
    var km8a81f93d026448993e18dcee902003a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km8a81f93d026448993e18dcee902003a.setDefaultUnit(kony.flex.DP);
    var kme68c3bd9a2847c3bd24c5e4ed6cc3a6 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    kme68c3bd9a2847c3bd24c5e4ed6cc3a6.setDefaultUnit(kony.flex.DP);
    var kme78d80db56b4bbeb09764f2510a94a3 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme78d80db56b4bbeb09764f2510a94a3.setDefaultUnit(kony.flex.DP);
    var km0aef0911d23411b87c7cc13075b16c6 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km11bcbae52a245fd97700725e9e3f7bd = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km11bcbae52a245fd97700725e9e3f7bd.setDefaultUnit(kony.flex.DP);
    var km9762790bd1d43ac84d1d4eb31874f4e = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km6c83c4db1e84afd89ea9f836f6e4bb7 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3fc7ca84bd24da89f12786efffcf422 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km11bcbae52a245fd97700725e9e3f7bd.add(km9762790bd1d43ac84d1d4eb31874f4e, km6c83c4db1e84afd89ea9f836f6e4bb7, km3fc7ca84bd24da89f12786efffcf422);
    var km42253e3e65e49f08f5a61eede3a1ece = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmd87a2ccb0414749aaf98709b85bb4a3 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kme78d80db56b4bbeb09764f2510a94a3.add(km0aef0911d23411b87c7cc13075b16c6, km11bcbae52a245fd97700725e9e3f7bd, km42253e3e65e49f08f5a61eede3a1ece, kmd87a2ccb0414749aaf98709b85bb4a3);
    var km341da3d7fd842be86857d4f97bb4d41 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    km341da3d7fd842be86857d4f97bb4d41.setDefaultUnit(kony.flex.DP);
    var kmc2bb6595c9f4e42a0aad14e6ce7fcf0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc2bb6595c9f4e42a0aad14e6ce7fcf0.setDefaultUnit(kony.flex.DP);
    var km10a011035b54e61b4957803ed890521 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmc2bb6595c9f4e42a0aad14e6ce7fcf0.add(km10a011035b54e61b4957803ed890521);
    km341da3d7fd842be86857d4f97bb4d41.add(kmc2bb6595c9f4e42a0aad14e6ce7fcf0);
    var km5b47f6aa92948539e0bca6ecc891588 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kme68c3bd9a2847c3bd24c5e4ed6cc3a6.add(kme78d80db56b4bbeb09764f2510a94a3, km341da3d7fd842be86857d4f97bb4d41, km5b47f6aa92948539e0bca6ecc891588);
    km8a81f93d026448993e18dcee902003a.add(kme68c3bd9a2847c3bd24c5e4ed6cc3a6);
    var km3047bf135494de9870c97c55095271d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3047bf135494de9870c97c55095271d.setDefaultUnit(kony.flex.DP);
    var kmd255540b2284dfbaf4075c84cbfedd0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    kmd255540b2284dfbaf4075c84cbfedd0.setDefaultUnit(kony.flex.DP);
    var km45a6502ce8443ca8d5e4f54c2e67b59 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km45a6502ce8443ca8d5e4f54c2e67b59.setDefaultUnit(kony.flex.DP);
    var km193c1d65ffa4adaa5fb24eace9317e3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    km193c1d65ffa4adaa5fb24eace9317e3.setDefaultUnit(kony.flex.DP);
    km193c1d65ffa4adaa5fb24eace9317e3.add();
    var kmdc9c399fa89491dbce0895ded9309c8 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kma06b49fcfe34888b1586f6615dee31a = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km798cf670868417dbc605391d0621c07 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km798cf670868417dbc605391d0621c07.setDefaultUnit(kony.flex.DP);
    var kma7b64270a0f4f88b4d2679ff57e96ed = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km935c14cb4334909af564fd1df46bd34 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km798cf670868417dbc605391d0621c07.add(kma7b64270a0f4f88b4d2679ff57e96ed, km935c14cb4334909af564fd1df46bd34);
    var kmb8c07e8dac04dba9ea6bb03e2ad5cc5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb8c07e8dac04dba9ea6bb03e2ad5cc5.setDefaultUnit(kony.flex.DP);
    var kme3ecce2c5ca42a4916430d83af62934 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km9cd54e34074466d9e252fc6279683b2 = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    kmb8c07e8dac04dba9ea6bb03e2ad5cc5.add(kme3ecce2c5ca42a4916430d83af62934, km9cd54e34074466d9e252fc6279683b2);
    var km24366b3bdc944daa1ab8b45683d728e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km24366b3bdc944daa1ab8b45683d728e.setDefaultUnit(kony.flex.DP);
    var km9e9abd04471493c8f31630a4fb2cfa7 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km188f0de85fb4bc49dc6eef292e0e4ed = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km24366b3bdc944daa1ab8b45683d728e.add(km9e9abd04471493c8f31630a4fb2cfa7, km188f0de85fb4bc49dc6eef292e0e4ed);
    var km91bad63da964a4285acfb3436ef731d = new kony.ui.Button({
        "centerY": "15dp",
        "height": "13.93%",
        "id": "btnClearVoiceSearch",
        "right": "9dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km45a6502ce8443ca8d5e4f54c2e67b59.add(km193c1d65ffa4adaa5fb24eace9317e3, kmdc9c399fa89491dbce0895ded9309c8, kma06b49fcfe34888b1586f6615dee31a, km798cf670868417dbc605391d0621c07, kmb8c07e8dac04dba9ea6bb03e2ad5cc5, km24366b3bdc944daa1ab8b45683d728e, km91bad63da964a4285acfb3436ef731d);
    kmd255540b2284dfbaf4075c84cbfedd0.add(km45a6502ce8443ca8d5e4f54c2e67b59);
    km3047bf135494de9870c97c55095271d.add(kmd255540b2284dfbaf4075c84cbfedd0);
    var km18d5b356d9d4dfc81947396b2828e3c = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km18d5b356d9d4dfc81947396b2828e3c.setDefaultUnit(kony.flex.DP);
    var kmbeae9996da243c0a6852f0639eb0c0c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbeae9996da243c0a6852f0639eb0c0c.setDefaultUnit(kony.flex.DP);
    var km0bd00c62365418f9537be1fdfd33b33 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmeec04776e4c4c4b8905d379048533c9 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmbeae9996da243c0a6852f0639eb0c0c.add(km0bd00c62365418f9537be1fdfd33b33, kmeec04776e4c4c4b8905d379048533c9);
    var km1631cc6c71c48648f6ce140c870e079 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km1631cc6c71c48648f6ce140c870e079.setDefaultUnit(kony.flex.DP);
    var kmecd80ebf23c4ec8b3141c1fa9acffcb = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km28a492afc584fe39282f87072b16ae1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var km6ddefc03b0a4ee688826d2bad3efb47 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km1631cc6c71c48648f6ce140c870e079.add(kmecd80ebf23c4ec8b3141c1fa9acffcb, km28a492afc584fe39282f87072b16ae1, km6ddefc03b0a4ee688826d2bad3efb47);
    var km780529a93ad4172b7e781ad4951bcb4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km780529a93ad4172b7e781ad4951bcb4.setDefaultUnit(kony.flex.DP);
    var km504399126a04857ba0947fae06225f9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km6a874862a0f413db15365f2426d959d = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km780529a93ad4172b7e781ad4951bcb4.add(km504399126a04857ba0947fae06225f9, km6a874862a0f413db15365f2426d959d);
    var km3eb50a41e4b4fe2b5e50efe6c300b19 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3eb50a41e4b4fe2b5e50efe6c300b19.setDefaultUnit(kony.flex.DP);
    var km38fd9da31f04dc499f91ef444488bcb = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbf857a1fac547a988d5f136d1a36ff4 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km3eb50a41e4b4fe2b5e50efe6c300b19.add(km38fd9da31f04dc499f91ef444488bcb, kmbf857a1fac547a988d5f136d1a36ff4);
    var kmcc82782b0484a30b7633013fc583c3c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmcc82782b0484a30b7633013fc583c3c.setDefaultUnit(kony.flex.DP);
    var km635e1ca8936461f8730501d4cb7519e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km996ba1b820b4f57a3395c018a937af7 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmcc82782b0484a30b7633013fc583c3c.add(km635e1ca8936461f8730501d4cb7519e, km996ba1b820b4f57a3395c018a937af7);
    var km1e7809e1aa947439cb7829dc273981b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km1e7809e1aa947439cb7829dc273981b.setDefaultUnit(kony.flex.DP);
    var kmfe01a46426c45bc8d78281441dac6dd = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5c40fa01ff4482887235f2c80122be2 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km1e7809e1aa947439cb7829dc273981b.add(kmfe01a46426c45bc8d78281441dac6dd, km5c40fa01ff4482887235f2c80122be2);
    var km9ced30f3f694909b7577f4097730a58 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9ced30f3f694909b7577f4097730a58.setDefaultUnit(kony.flex.DP);
    var kmf8b2267e105474aa85a44aadc4c7a93 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmcf26f3c067e48fcb80be449efa182bb = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km9ced30f3f694909b7577f4097730a58.add(kmf8b2267e105474aa85a44aadc4c7a93, kmcf26f3c067e48fcb80be449efa182bb);
    var kme1d425a0d7c432cbf1259a18445589b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme1d425a0d7c432cbf1259a18445589b.setDefaultUnit(kony.flex.DP);
    var km00dba717d1b434bb2ddafc49d096c7d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc7b6e3cad7b424a855e6e812fe14699 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kme1d425a0d7c432cbf1259a18445589b.add(km00dba717d1b434bb2ddafc49d096c7d, kmc7b6e3cad7b424a855e6e812fe14699);
    km18d5b356d9d4dfc81947396b2828e3c.add(kmbeae9996da243c0a6852f0639eb0c0c, km1631cc6c71c48648f6ce140c870e079, km780529a93ad4172b7e781ad4951bcb4, km3eb50a41e4b4fe2b5e50efe6c300b19, kmcc82782b0484a30b7633013fc583c3c, km1e7809e1aa947439cb7829dc273981b, km9ced30f3f694909b7577f4097730a58, kme1d425a0d7c432cbf1259a18445589b);
    var kmd822c528f4a4582a53a05c793a51206 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    kmd822c528f4a4582a53a05c793a51206.setDefaultUnit(kony.flex.DP);
    var km5f0c0a835e74e60ab1e54b6e8e669e5 = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km7ada2d467444c9ab3261b3065a57c95 = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    km7ada2d467444c9ab3261b3065a57c95.setDefaultUnit(kony.flex.DP);
    var km7f5b1bd90ab4cf2b2ecd56333ec0026 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km7f5b1bd90ab4cf2b2ecd56333ec0026.setDefaultUnit(kony.flex.DP);
    var kme8b698a0156407b984907455310f8c8 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km7f5b1bd90ab4cf2b2ecd56333ec0026.add(kme8b698a0156407b984907455310f8c8);
    var km1c4283f00e940688359615a3879bbf4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km1c4283f00e940688359615a3879bbf4.setDefaultUnit(kony.flex.DP);
    var km06c5414344f4c6eb11c1ee78dddf79d = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var kmdbb40603de3484d8a8b021225d4788c = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClearSearch",
        "right": "2%",
        "text": "X",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km1c4283f00e940688359615a3879bbf4.add(km06c5414344f4c6eb11c1ee78dddf79d, kmdbb40603de3484d8a8b021225d4788c);
    var kmde0bd2bd5e54ddab4233f79e37b5181 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmde0bd2bd5e54ddab4233f79e37b5181.setDefaultUnit(kony.flex.DP);
    var km99ff4be68f74500976313202b101b21 = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    kmde0bd2bd5e54ddab4233f79e37b5181.add(km99ff4be68f74500976313202b101b21);
    var kmd1f594f44b847bb8bc37d2e9f0149cd = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmd1f594f44b847bb8bc37d2e9f0149cd.setDefaultUnit(kony.flex.DP);
    var km229af844f284601ac6b127e04c55074 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmd1f594f44b847bb8bc37d2e9f0149cd.add(km229af844f284601ac6b127e04c55074);
    var km869e1800def450b90bf890640e97ac4 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCameraImageSearch",
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "isVisible": false,
        "skin": "sknBtnPopupCloseTransparentBg"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmbfc4aa6f70447a08a09a8c8b7bd9a4d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbfc4aa6f70447a08a09a8c8b7bd9a4d.setDefaultUnit(kony.flex.DP);
    var km68be7a67be14054a4f5b888f6620b07 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmbfc4aa6f70447a08a09a8c8b7bd9a4d.add(km68be7a67be14054a4f5b888f6620b07);
    km7ada2d467444c9ab3261b3065a57c95.add(km7f5b1bd90ab4cf2b2ecd56333ec0026, km1c4283f00e940688359615a3879bbf4, kmde0bd2bd5e54ddab4233f79e37b5181, kmd1f594f44b847bb8bc37d2e9f0149cd, km869e1800def450b90bf890640e97ac4, kmbfc4aa6f70447a08a09a8c8b7bd9a4d);
    var km17129d7dc534ce3ae3065fc8b190610 = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km97b8eb19ef848e4b72d185ada93ab6d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km97b8eb19ef848e4b72d185ada93ab6d.setDefaultUnit(kony.flex.DP);
    var km5cbc0c61a5c4f9ab987a5610a100eca = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd7845673adf450cb62b5240bc4f86b4 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km97b8eb19ef848e4b72d185ada93ab6d.add(km5cbc0c61a5c4f9ab987a5610a100eca, kmd7845673adf450cb62b5240bc4f86b4);
    kmd822c528f4a4582a53a05c793a51206.add(km5f0c0a835e74e60ab1e54b6e8e669e5, km7ada2d467444c9ab3261b3065a57c95, km17129d7dc534ce3ae3065fc8b190610, km97b8eb19ef848e4b72d185ada93ab6d);
    frmPDP.add(flxMainContainer, kmdf2399a31e94a7ab3f90450eccf9189, flxPinchandZoom, kmfef6266e80f4e7f93738a1a533ea5c8, flxChatBoxTitleContainer, km8a81f93d026448993e18dcee902003a, km3047bf135494de9870c97c55095271d, km18d5b356d9d4dfc81947396b2828e3c, kmd822c528f4a4582a53a05c793a51206);
};

function frmPDPGlobals() {
    frmPDP = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPDP,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmPDP",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};