function addWidgetsfrmRewardsProfileStep1() {
    frmRewardsProfileStep1.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var FlexContainer0b4738c7579e24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "FlexContainer0b4738c7579e24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    FlexContainer0b4738c7579e24b.setDefaultUnit(kony.flex.DP);
    var Label0c5601159c0bb4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0c5601159c0bb4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxWhite",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.step1"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0b4738c7579e24b.add(Label0c5601159c0bb4c);
    var flxFirstName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxFirstName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxFirstName.setDefaultUnit(kony.flex.DP);
    var txtBxHide = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "0dp",
        "id": "txtBxHide",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtFstName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50.01%",
        "focusSkin": "sknTextbox",
        "height": "100%",
        "id": "txtFstName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18.phone.rewards.firstname"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearFirstName = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnClearFirstName",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    flxFirstName.add(txtBxHide, txtFstName, btnClearFirstName);
    var flxLastName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxLastName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxLastName.setDefaultUnit(kony.flex.DP);
    var txtLstName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "100%",
        "id": "txtLstName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18.phone.rewards.lastname"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearLastName = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnClearLastName",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    flxLastName.add(txtLstName, btnClearLastName);
    var flxEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxEmail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxEmail.setDefaultUnit(kony.flex.DP);
    var txtEmlAddrss = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "100%",
        "id": "txtEmlAddrss",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18.phone.rewards.emailAddress"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearEmail = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnClearEmail",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    flxEmail.add(txtEmlAddrss, btnClearEmail);
    var flxPhone = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxPhone",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxPhone.setDefaultUnit(kony.flex.DP);
    var txtPhone = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "100%",
        "id": "txtPhone",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
        "maxTextLength": 10,
        "placeholder": kony.i18n.getLocalizedString("i18.phone.rewards.phone"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearPhone = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnClearPhone",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    flxPhone.add(txtPhone, btnClearPhone);
    var flxDateOfBirth = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxDateOfBirth",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxDateOfBirth.setDefaultUnit(kony.flex.DP);
    var txtDateOfBirth = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "100%",
        "id": "txtDateOfBirth",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 5,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.rewardsProfile.mmdd"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearDateOfBirth = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnClearDateOfBirth",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    flxDateOfBirth.add(txtDateOfBirth, btnClearDateOfBirth);
    var btnReset = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnReset",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.exitAndFinishLater"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnNext = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnNext",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.next"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "64dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flxMainContainer.add(flxTopSpacerDND, FlexContainer0b4738c7579e24b, flxFirstName, flxLastName, flxEmail, flxPhone, flxDateOfBirth, btnReset, btnNext, flxBottomSpacerDND);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.rewardsprofile"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderTitleContainer.add(lblFormTitle, lblChevron, btnHeaderLeft);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialogbox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxDialogbox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxDialogbox.setDefaultUnit(kony.flex.DP);
    var lbl1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl1",
        "isVisible": true,
        "skin": "sknLblSFNSBold",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.confirmationAlert"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lbl2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSFNSRegular",
        "text": kony.i18n.getLocalizedString("i18n.rewardsProfile.confirmMessage"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "40dp",
        "id": "btnOk",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.ok"),
        "top": "10dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    var btnYes = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.okayButton"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDialogbox.add(lbl1, lbl2, btnOk, flxLine, btnYes);
    flxOverlay.add(flxDialogbox);
    var km30791cd80af434ebc36ffe1e7705bfe = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km30791cd80af434ebc36ffe1e7705bfe.setDefaultUnit(kony.flex.DP);
    var kmefcdb3016e740f4bf46fedec4ebc3c8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmefcdb3016e740f4bf46fedec4ebc3c8.setDefaultUnit(kony.flex.DP);
    var km463cce2f47b4491864347d52635f439 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5d46d75d75f4a8a97cb92147b507d4b = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmefcdb3016e740f4bf46fedec4ebc3c8.add(km463cce2f47b4491864347d52635f439, km5d46d75d75f4a8a97cb92147b507d4b);
    var km7f17c4e6b134c17a4e4efe80e600d21 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7f17c4e6b134c17a4e4efe80e600d21.setDefaultUnit(kony.flex.DP);
    var km0060cb33b5447dab29e3f3bd8f501be = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km002aa928a2048e49b93a756cbd7afc8 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var kmd22717654b74650a0fc96220f4e7985 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km7f17c4e6b134c17a4e4efe80e600d21.add(km0060cb33b5447dab29e3f3bd8f501be, km002aa928a2048e49b93a756cbd7afc8, kmd22717654b74650a0fc96220f4e7985);
    var km0b6dac7151a4bdb9a8296c7a2d199b0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0b6dac7151a4bdb9a8296c7a2d199b0.setDefaultUnit(kony.flex.DP);
    var km402437f74774d4aa0c9d2ec82959b4b = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc8bc2e0721a4a9b8c9011c7ed3f06b6 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km0b6dac7151a4bdb9a8296c7a2d199b0.add(km402437f74774d4aa0c9d2ec82959b4b, kmc8bc2e0721a4a9b8c9011c7ed3f06b6);
    var kmb6028131bf24f01b92bc9783c7f1d04 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb6028131bf24f01b92bc9783c7f1d04.setDefaultUnit(kony.flex.DP);
    var kmbace842d5ac4a1f9b21f9182dedb499 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3df85c9598c4818bce6f016c31b006e = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmb6028131bf24f01b92bc9783c7f1d04.add(kmbace842d5ac4a1f9b21f9182dedb499, km3df85c9598c4818bce6f016c31b006e);
    var km4344ad82c4c49ed9077119819843234 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4344ad82c4c49ed9077119819843234.setDefaultUnit(kony.flex.DP);
    var km154e80c99ea4a0eb3d1a68df7650e1f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc5e1792b275408fbca98b8671f848e5 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km4344ad82c4c49ed9077119819843234.add(km154e80c99ea4a0eb3d1a68df7650e1f, kmc5e1792b275408fbca98b8671f848e5);
    var kmc9f22c91f52489d9b843c0e17c7dd36 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc9f22c91f52489d9b843c0e17c7dd36.setDefaultUnit(kony.flex.DP);
    var kmac5cafa23714e53b8ffbf88ab7b4041 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km17da706352f4ab4b44affdaa2fc798a = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmc9f22c91f52489d9b843c0e17c7dd36.add(kmac5cafa23714e53b8ffbf88ab7b4041, km17da706352f4ab4b44affdaa2fc798a);
    var km8caf264e3b04a4b9cbac783ee81860f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8caf264e3b04a4b9cbac783ee81860f.setDefaultUnit(kony.flex.DP);
    var kmbe16dd35dc845c3969c009672dd64f6 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km15bd3b97fe949f09614126461fd6143 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km8caf264e3b04a4b9cbac783ee81860f.add(kmbe16dd35dc845c3969c009672dd64f6, km15bd3b97fe949f09614126461fd6143);
    var km6a618d657414537aa0ef7b75d907b4c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km6a618d657414537aa0ef7b75d907b4c.setDefaultUnit(kony.flex.DP);
    var km26bc0ed53eb4aeaa49807db5880fdde = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbe03b17b7314fe58a5c295754e0de57 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km6a618d657414537aa0ef7b75d907b4c.add(km26bc0ed53eb4aeaa49807db5880fdde, kmbe03b17b7314fe58a5c295754e0de57);
    km30791cd80af434ebc36ffe1e7705bfe.add(kmefcdb3016e740f4bf46fedec4ebc3c8, km7f17c4e6b134c17a4e4efe80e600d21, km0b6dac7151a4bdb9a8296c7a2d199b0, kmb6028131bf24f01b92bc9783c7f1d04, km4344ad82c4c49ed9077119819843234, kmc9f22c91f52489d9b843c0e17c7dd36, km8caf264e3b04a4b9cbac783ee81860f, km6a618d657414537aa0ef7b75d907b4c);
    frmRewardsProfileStep1.add(flxMainContainer, flxHeaderTitleContainer, flxOverlay, km30791cd80af434ebc36ffe1e7705bfe);
};

function frmRewardsProfileStep1Globals() {
    frmRewardsProfileStep1 = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRewardsProfileStep1,
        "enabledForIdleTimeout": true,
        "id": "frmRewardsProfileStep1",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};