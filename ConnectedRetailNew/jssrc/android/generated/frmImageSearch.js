function addWidgetsfrmImageSearch() {
    frmImageSearch.setDefaultUnit(kony.flex.DP);
    var flexHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "50dp",
        "id": "flexHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "zIndex": 5
    }, {}, {});
    flexHeader.setDefaultUnit(kony.flex.DP);
    var flexBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flexBack.setDefaultUnit(kony.flex.DP);
    var imgBack = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_back.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexBack.add(imgBack);
    var flexInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": 0,
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flexInfo.setDefaultUnit(kony.flex.DP);
    var imgInfo = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgInfo",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_info.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexInfo.add(imgInfo);
    flexHeader.add(flexBack, flexInfo);
    var flexImageSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flexImageSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-100dp",
        "isModalContainer": false,
        "right": "-100dp",
        "skin": "sknFlexBgTrans",
        "top": "-100dp",
        "zIndex": 1
    }, {}, {});
    flexImageSearch.setDefaultUnit(kony.flex.DP);
    var flexBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransImageSearch",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexBody.setDefaultUnit(kony.flex.DP);
    var imgImageSearch = new kony.ui.Image2({
        "bottom": "0dp",
        "id": "imgImageSearch",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "white_1012x628.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSearchStatus = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblSearchStatus",
        "isVisible": true,
        "skin": "sknLblGothamMedium36pxWhite",
        "text": "Processing...",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAndSearchStatus = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblAndSearchStatus",
        "isVisible": false,
        "skin": "sknLblGothamMedium36pxWhite",
        "text": "Processing...",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var customAnimator = new customWidget.CustomAnimWidget({
        "id": "customAnimator",
        "isVisible": false,
        "width": "200dp",
        "height": "200dp",
        "centerX": "50.00%",
        "centerY": "52.68%",
        "zIndex": 5
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "widgetName": "CustomAnimWidget"
    });
    flexBody.add(imgImageSearch, lblSearchStatus, lblAndSearchStatus, customAnimator);
    var flexStatusBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "180dp",
        "id": "flexStatusBar",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRound",
        "width": "180dp",
        "zIndex": 3
    }, {}, {});
    flexStatusBar.setDefaultUnit(kony.flex.DP);
    var flex1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex1.setDefaultUnit(kony.flex.DP);
    var flexInner1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner1.setDefaultUnit(kony.flex.DP);
    flexInner1.add();
    flex1.add(flexInner1);
    var flex2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex2.setDefaultUnit(kony.flex.DP);
    var flexInner2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner2.setDefaultUnit(kony.flex.DP);
    flexInner2.add();
    flex2.add(flexInner2);
    var flex3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex3.setDefaultUnit(kony.flex.DP);
    var flexInner3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner3.setDefaultUnit(kony.flex.DP);
    flexInner3.add();
    flex3.add(flexInner3);
    var flex4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flex4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex4.setDefaultUnit(kony.flex.DP);
    var flexInner4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexInner4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "width": "100%"
    }, {}, {});
    flexInner4.setDefaultUnit(kony.flex.DP);
    flexInner4.add();
    flex4.add(flexInner4);
    var flexInnerRing = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "155dp",
        "id": "flexInnerRing",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRound",
        "width": "155dp",
        "zIndex": null
    }, {}, {});
    flexInnerRing.setDefaultUnit(kony.flex.DP);
    var imgInnerRing = new kony.ui.Image2({
        "height": "100%",
        "id": "imgInnerRing",
        "isVisible": true,
        "left": 0,
        "skin": "slImage",
        "src": "white_1012x628.png",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexInnerRing.add(imgInnerRing);
    flexStatusBar.add(flex1, flex2, flex3, flex4, flexInnerRing);
    flexImageSearch.add(flexBody, flexStatusBar);
    var flexErrorMessage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexErrorMessage",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flexErrorMessage.setDefaultUnit(kony.flex.DP);
    var flexMessageContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flexMessageContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlxWhiteBg5pxBorderRadius",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flexMessageContainer.setDefaultUnit(kony.flex.DP);
    var lblErrorMessage = new kony.ui.Label({
        "id": "lblErrorMessage",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknLblImageSearchErroMessage",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageSearch.errorMessage"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnTryAgain = new kony.ui.Button({
        "focusSkin": "sknBtnRedBgCustomFoc",
        "height": "50dp",
        "id": "btnTryAgain",
        "isVisible": false,
        "left": "10%",
        "right": "10%",
        "skin": "sknBtnRedBgCustom5px",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageSearch.tryAgain"),
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCancel = new kony.ui.Button({
        "focusSkin": "sknBtnTransMedGreyFoc",
        "height": "50dp",
        "id": "btnCancel",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknBtnTransMedGrey",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.smallCancel"),
        "top": "5dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblGap = new kony.ui.Label({
        "height": "1dp",
        "id": "lblGap",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 6
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexMessageContainer.add(lblErrorMessage, btnTryAgain, btnCancel, lblGap);
    flexErrorMessage.add(flexMessageContainer);
    frmImageSearch.add(flexHeader, flexImageSearch, flexErrorMessage);
};

function frmImageSearchGlobals() {
    frmImageSearch = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmImageSearch,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmImageSearch",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};