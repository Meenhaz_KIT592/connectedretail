function initializetempWeeklyAdHeader() {
    flxWeeklyAdHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "32dp",
        "id": "flxWeeklyAdHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    flxWeeklyAdHeader.setDefaultUnit(kony.flex.DP);
    var lblNewExclusive = new kony.ui.Label({
        "height": "100%",
        "id": "lblNewExclusive",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxWhite",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxWeeklyAdHeader.add(lblNewExclusive);
}