function addWidgetsfrmRefineProjects() {
    frmRefineProjects.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblHeader",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRefineProjects.lblHeader"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCancel = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransReg16pxWhiteFoc",
        "height": "50dp",
        "id": "btnCancel",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknBtnTransReg16pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnCancel"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnDone = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransReg16pxWhiteFoc",
        "height": "100%",
        "id": "btnDone",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransReg16pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnDone"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(lblHeader, btnCancel, btnDone);
    var flxClearAll = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxClearAll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxBgGray818181op100",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxClearAll.setDefaultUnit(kony.flex.DP);
    var lblFilter = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblFilter",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMedium32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRefineProjects.lblFilter"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnClearAll = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed32pxWhiteFoc",
        "height": "100%",
        "id": "btnClearAll",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransMed32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnClearAll"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxClearAll.add(lblFilter, btnClearAll);
    var flxScrlMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxScrlMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "92dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrlMain.setDefaultUnit(kony.flex.DP);
    var flxProducts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxProducts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProducts.setDefaultUnit(kony.flex.DP);
    var lblProducts = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "lblProducts",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknRTGothamBold24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRefineProjects.lblProducts"),
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxProducts.add(lblProducts);
    var flxGap0 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGap0",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGap0.setDefaultUnit(kony.flex.DP);
    flxGap0.add();
    var flxSkillLevel = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "37dp",
        "id": "flxSkillLevel",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "right": "10dp",
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSkillLevel.setDefaultUnit(kony.flex.DP);
    var lblSkillLevel = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSkillLevel",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRefineProjects.lblSkillLevel"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTickSkill = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTickSkill",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblIcon34px333333",
        "text": "D",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxGap1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGap1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGap1.setDefaultUnit(kony.flex.DP);
    flxGap1.add();
    flxSkillLevel.add(lblSkillLevel, lblTickSkill, flxGap1);
    var flxSkillLevelCategories = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxSkillLevelCategories",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSkillLevelCategories.setDefaultUnit(kony.flex.DP);
    flxSkillLevelCategories.add();
    var flxTimeReq = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "37dp",
        "id": "flxTimeReq",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTimeReq.setDefaultUnit(kony.flex.DP);
    var lblTimeReq = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTimeReq",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRefineProjects.lblTimeReq"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTickTime = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTickTime",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblIcon34px333333",
        "text": "D",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxGap2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGap2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGap2.setDefaultUnit(kony.flex.DP);
    flxGap2.add();
    flxTimeReq.add(lblTimeReq, lblTickTime, flxGap2);
    var flxTimeReqCategories = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTimeReqCategories",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTimeReqCategories.setDefaultUnit(kony.flex.DP);
    flxTimeReqCategories.add();
    var flxEmptySpace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxEmptySpace",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEmptySpace.setDefaultUnit(kony.flex.DP);
    flxEmptySpace.add();
    flxScrlMain.add(flxProducts, flxGap0, flxSkillLevel, flxSkillLevelCategories, flxTimeReq, flxTimeReqCategories, flxEmptySpace);
    frmRefineProjects.add(flxHeader, flxClearAll, flxScrlMain);
};

function frmRefineProjectsGlobals() {
    frmRefineProjects = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRefineProjects,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmRefineProjects",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};