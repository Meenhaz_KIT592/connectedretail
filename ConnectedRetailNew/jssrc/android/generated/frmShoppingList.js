function addWidgetsfrmShoppingList() {
    frmShoppingList.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxSubHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxSubHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSubHeader.setDefaultUnit(kony.flex.DP);
    var lblShoppingList = new kony.ui.Label({
        "height": "100%",
        "id": "lblShoppingList",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamBold28pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmHome.myList"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDeleteAll = new kony.ui.Button({
        "focusSkin": "sknBtnTransBold28pxWhiteFoc",
        "height": "100%",
        "id": "btnDeleteAll",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransBold28pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmShoppingList.deleteAll"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    flxSubHeader.add(lblShoppingList, btnDeleteAll);
    var segShoppingList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "groupCells": false,
        "id": "segShoppingList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxShoppingListContainer,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "sectionHeaderTemplate": flxPpoducts,
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "92dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlexContainer0e885e995a5cc4e": "FlexContainer0e885e995a5cc4e",
            "btnDelete": "btnDelete",
            "btnExpand": "btnExpand",
            "flexMyListHeader": "flexMyListHeader",
            "flxAisleInfo": "flxAisleInfo",
            "flxPpoducts": "flxPpoducts",
            "flxSeparator": "flxSeparator",
            "flxShoppingListContainer": "flxShoppingListContainer",
            "flxShoppingListContents": "flxShoppingListContents",
            "flxShoppingListInner": "flxShoppingListInner",
            "flxStockAisleInfo": "flxStockAisleInfo",
            "imgProduct": "imgProduct",
            "lblAisle": "lblAisle",
            "lblAisleNo": "lblAisleNo",
            "lblProductName": "lblProductName",
            "lblSeparator": "lblSeparator",
            "lblShoppingListHdr": "lblShoppingListHdr",
            "lblStockInfo": "lblStockInfo"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNoList = new kony.ui.Label({
        "id": "lblNoList",
        "isVisible": false,
        "left": 0,
        "skin": "sknLblGothamBold24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmShoppingList.noItemsInList"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "92dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMainContainer.add(flxSubHeader, segShoppingList, lblNoList);
    var km5b85d6cb03e44d9b80505e3e1fcc0e9 = new kony.ui.FlexScrollContainer({
        "isMaster": true,
        "id": "flxSearchResultsContainer",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    km5b85d6cb03e44d9b80505e3e1fcc0e9.setDefaultUnit(kony.flex.DP);
    var km62de28eee75414dbb0fb2f6e3ebb6bd = new kony.ui.FlexContainer({
        "id": "flxTopSpacerDNDSearch",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km62de28eee75414dbb0fb2f6e3ebb6bd.setDefaultUnit(kony.flex.DP);
    km62de28eee75414dbb0fb2f6e3ebb6bd.add();
    var km0048367821b4947adb72f679b553bce = new kony.ui.SegmentedUI2({
        "id": "segSearchOffers",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmc51ed64342f4a3cbd1aedd23e719552 = new kony.ui.SegmentedUI2({
        "id": "segSearchResults",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km5b85d6cb03e44d9b80505e3e1fcc0e9.add(km62de28eee75414dbb0fb2f6e3ebb6bd, km0048367821b4947adb72f679b553bce, kmc51ed64342f4a3cbd1aedd23e719552);
    var km0b06a1ba1e543e4a90eba4239357123 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    km0b06a1ba1e543e4a90eba4239357123.setDefaultUnit(kony.flex.DP);
    var km459c74745b74dd8b0ffc4425c6229ab = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km459c74745b74dd8b0ffc4425c6229ab.setDefaultUnit(kony.flex.DP);
    km459c74745b74dd8b0ffc4425c6229ab.add();
    var km840987ddd864f81ba8b846e62d0fdb9 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km840987ddd864f81ba8b846e62d0fdb9.setDefaultUnit(kony.flex.DP);
    var km55b93db7654416da5715bb39d60e198 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km93a1cec05d94cb4b46fb2f4518c556f = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    var kmfc89bc730c848219e4a1af4b1548609 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km415440ccbc741488c3b587e215dad51 = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    km415440ccbc741488c3b587e215dad51.setDefaultUnit(kony.flex.DP);
    km415440ccbc741488c3b587e215dad51.add();
    var kmec075ab7f9b400689d827c607c548f4 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km840987ddd864f81ba8b846e62d0fdb9.add(km55b93db7654416da5715bb39d60e198, km93a1cec05d94cb4b46fb2f4518c556f, kmfc89bc730c848219e4a1af4b1548609, km415440ccbc741488c3b587e215dad51, kmec075ab7f9b400689d827c607c548f4);
    var kmfaf6e99f31d4854b60df1bd1a154349 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmfaf6e99f31d4854b60df1bd1a154349.setDefaultUnit(kony.flex.DP);
    var kmbe724f2bc6245c8bffa2c3e42f7b0fa = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmbe724f2bc6245c8bffa2c3e42f7b0fa.setDefaultUnit(kony.flex.DP);
    var kmd4d8afb63dd46c1b80e0b4567ebeb84 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    kmd4d8afb63dd46c1b80e0b4567ebeb84.setDefaultUnit(kony.flex.DP);
    var kmba25a07c1a9426ca0930b2fb61c0391 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmd4d8afb63dd46c1b80e0b4567ebeb84.add(kmba25a07c1a9426ca0930b2fb61c0391);
    var km91f244939be475098c7ba346fc586f2 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    km91f244939be475098c7ba346fc586f2.setDefaultUnit(kony.flex.DP);
    var km6f01f7e38db4a5e991ce22e2e759012 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km91f244939be475098c7ba346fc586f2.add(km6f01f7e38db4a5e991ce22e2e759012);
    var km5a77adb137a454eb0ba59c18e88d400 = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km5a77adb137a454eb0ba59c18e88d400.setDefaultUnit(kony.flex.DP);
    var km3728e78becf458aa806f34565d0eb9c = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km17f8566656d4475a923c1825c89aa51 = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km5a77adb137a454eb0ba59c18e88d400.add(km3728e78becf458aa806f34565d0eb9c, km17f8566656d4475a923c1825c89aa51);
    var km7fe43cc40da4b288b269feff16eeb18 = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmbe724f2bc6245c8bffa2c3e42f7b0fa.add(kmd4d8afb63dd46c1b80e0b4567ebeb84, km91f244939be475098c7ba346fc586f2, km5a77adb137a454eb0ba59c18e88d400, km7fe43cc40da4b288b269feff16eeb18);
    var kme035a0c684d4ff092fceecff8646ef6 = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kme035a0c684d4ff092fceecff8646ef6.setDefaultUnit(kony.flex.DP);
    kme035a0c684d4ff092fceecff8646ef6.add();
    kmfaf6e99f31d4854b60df1bd1a154349.add(kmbe724f2bc6245c8bffa2c3e42f7b0fa, kme035a0c684d4ff092fceecff8646ef6);
    var km40d5648048e462a9dedd20a38d01728 = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km40d5648048e462a9dedd20a38d01728.setDefaultUnit(kony.flex.DP);
    var km9b452856ac140a3b7a350fe1e2827c4 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km9b452856ac140a3b7a350fe1e2827c4.setDefaultUnit(kony.flex.DP);
    var km6768122ebd344b181e8a782a1bf876f = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km6768122ebd344b181e8a782a1bf876f.setDefaultUnit(kony.flex.DP);
    var km51713d63045455589c4c463ed76ddfa = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kma6289897fe94e368b4f6093accd87ac = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km6768122ebd344b181e8a782a1bf876f.add(km51713d63045455589c4c463ed76ddfa, kma6289897fe94e368b4f6093accd87ac);
    var kmb8cbd65c0864abcb686957d04137b69 = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmb8cbd65c0864abcb686957d04137b69.setDefaultUnit(kony.flex.DP);
    var km62ce5a6c6cd4b8484dd0d9b34b6879a = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km62ce5a6c6cd4b8484dd0d9b34b6879a.setDefaultUnit(kony.flex.DP);
    var kmd72bc55b0184982afd177098ab09bea = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbafa108400f4a359d619d5bc8deac86 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kme597bd0fea8414e91d97e0b90151143 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km77436bdf5fd43919932b3637aee18aa = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km0128225537748108fe4b0a86d8b0a48 = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmb26ceb6a73a4243b22c827564b903f7 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km62ce5a6c6cd4b8484dd0d9b34b6879a.add(kmd72bc55b0184982afd177098ab09bea, kmbafa108400f4a359d619d5bc8deac86, kme597bd0fea8414e91d97e0b90151143, km77436bdf5fd43919932b3637aee18aa, km0128225537748108fe4b0a86d8b0a48, kmb26ceb6a73a4243b22c827564b903f7);
    var km4590fabdac3477eb4cf706a45aa0b88 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km4590fabdac3477eb4cf706a45aa0b88.setDefaultUnit(kony.flex.DP);
    var kmd2d607c3ff6477bb97f35ac475672dc = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km84a353f117b423981209c6d69054e27 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km93a0a0a1279421cb5fb17a22b38a6b2 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km4590fabdac3477eb4cf706a45aa0b88.add(kmd2d607c3ff6477bb97f35ac475672dc, km84a353f117b423981209c6d69054e27, km93a0a0a1279421cb5fb17a22b38a6b2);
    kmb8cbd65c0864abcb686957d04137b69.add(km62ce5a6c6cd4b8484dd0d9b34b6879a, km4590fabdac3477eb4cf706a45aa0b88);
    var kmba6e606096b4bb297a9bb1ab84fe435 = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd8101b5b30f4ea8a3c43559adc77eab = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km9b452856ac140a3b7a350fe1e2827c4.add(km6768122ebd344b181e8a782a1bf876f, kmb8cbd65c0864abcb686957d04137b69, kmba6e606096b4bb297a9bb1ab84fe435, kmd8101b5b30f4ea8a3c43559adc77eab);
    km40d5648048e462a9dedd20a38d01728.add(km9b452856ac140a3b7a350fe1e2827c4);
    km0b06a1ba1e543e4a90eba4239357123.add(km459c74745b74dd8b0ffc4425c6229ab, km840987ddd864f81ba8b846e62d0fdb9, kmfaf6e99f31d4854b60df1bd1a154349, km40d5648048e462a9dedd20a38d01728);
    var kma426b973afb42da86ecd356d2dbf3ec = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kma426b973afb42da86ecd356d2dbf3ec.setDefaultUnit(kony.flex.DP);
    var kmee32d7760ad41df87da6a3e60d0848e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    kmee32d7760ad41df87da6a3e60d0848e.setDefaultUnit(kony.flex.DP);
    var km9d14d9a0c4e46468b4a0b9dc5ab3932 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9d14d9a0c4e46468b4a0b9dc5ab3932.setDefaultUnit(kony.flex.DP);
    var km60a687f546a4c268f5f38695ca8877b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5ddd5ac38d44d4da4b5e45bd44e0e00 = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km5ddd5ac38d44d4da4b5e45bd44e0e00.setDefaultUnit(kony.flex.DP);
    var kme645ac1f6cd430fa2116c557cc531fa = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km95ce6a9f6c348fbb615df3a962c6512 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd51d69fc64b486c90700aa7fd9dc50b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km5ddd5ac38d44d4da4b5e45bd44e0e00.add(kme645ac1f6cd430fa2116c557cc531fa, km95ce6a9f6c348fbb615df3a962c6512, kmd51d69fc64b486c90700aa7fd9dc50b);
    var km93ba9ec2142467891ca24f24091ae85 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km13b3c2a6aa341798b277446af59b351 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km9d14d9a0c4e46468b4a0b9dc5ab3932.add(km60a687f546a4c268f5f38695ca8877b, km5ddd5ac38d44d4da4b5e45bd44e0e00, km93ba9ec2142467891ca24f24091ae85, km13b3c2a6aa341798b277446af59b351);
    var kmb063fd777ff4f5f86c5f3776e594b6a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    kmb063fd777ff4f5f86c5f3776e594b6a.setDefaultUnit(kony.flex.DP);
    var km414a3f960e34213b77f306be63aec44 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km414a3f960e34213b77f306be63aec44.setDefaultUnit(kony.flex.DP);
    var km29921c0fecd494daa16e543538240c3 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km414a3f960e34213b77f306be63aec44.add(km29921c0fecd494daa16e543538240c3);
    kmb063fd777ff4f5f86c5f3776e594b6a.add(km414a3f960e34213b77f306be63aec44);
    var km8e9caac050141e49bd17fc6f6a3a405 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmee32d7760ad41df87da6a3e60d0848e.add(km9d14d9a0c4e46468b4a0b9dc5ab3932, kmb063fd777ff4f5f86c5f3776e594b6a, km8e9caac050141e49bd17fc6f6a3a405);
    kma426b973afb42da86ecd356d2dbf3ec.add(kmee32d7760ad41df87da6a3e60d0848e);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialog = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxDialog",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "13%",
        "isModalContainer": false,
        "right": "13%",
        "skin": "sknFlxWhiteBg5pxBorderRadius",
        "width": "74%",
        "zIndex": 1
    }, {}, {});
    flxDialog.setDefaultUnit(kony.flex.DP);
    var lblPopUpMessage = new kony.ui.Label({
        "id": "lblPopUpMessage",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknLblImageSearchPopupMessage",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageOverlay.popUpMessage"),
        "textStyle": {
            "lineSpacing": 10,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnGotIt = new kony.ui.Button({
        "focusSkin": "sknBtnRedBg5pxBorderRadius",
        "height": "50dp",
        "id": "btnGotIt",
        "isVisible": true,
        "left": "10%",
        "right": "10%",
        "skin": "sknBtnRedBg5pxBorderRadius",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageOverlay.popUpButtonText"),
        "top": "20dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblGapDialog = new kony.ui.Label({
        "height": "1dp",
        "id": "lblGapDialog",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDialog.add(lblPopUpMessage, btnGotIt, lblGapDialog);
    flxOverlay.add(flxDialog);
    var kmdf699fe5922454e80aca3a18bfbe49d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmdf699fe5922454e80aca3a18bfbe49d.setDefaultUnit(kony.flex.DP);
    var km87a0833383c4874afd9924839a0bd71 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    km87a0833383c4874afd9924839a0bd71.setDefaultUnit(kony.flex.DP);
    var kmafb451b316f4d7da3615f862298cd00 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kmafb451b316f4d7da3615f862298cd00.setDefaultUnit(kony.flex.DP);
    var km11177f237044374a959f523f01a3193 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    km11177f237044374a959f523f01a3193.setDefaultUnit(kony.flex.DP);
    km11177f237044374a959f523f01a3193.add();
    var km69336b9c5ac462c98a37fc4804d8781 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmbc88f276e5043309915fbb8767f93c0 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km4702bd48b634375b671bdf2d93fb226 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4702bd48b634375b671bdf2d93fb226.setDefaultUnit(kony.flex.DP);
    var km7bca23b273a4e41a72eaeececbbef7c = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmb6b74972ffa4ff8b34802beb044335f = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km4702bd48b634375b671bdf2d93fb226.add(km7bca23b273a4e41a72eaeececbbef7c, kmb6b74972ffa4ff8b34802beb044335f);
    var kmc97ab38482d48a2ad32cf43005318c0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc97ab38482d48a2ad32cf43005318c0.setDefaultUnit(kony.flex.DP);
    var kmcc9f5f8a21f42cebed4e0d35f2a0242 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km1126706c1434094bcd1844a8c65806c = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    kmc97ab38482d48a2ad32cf43005318c0.add(kmcc9f5f8a21f42cebed4e0d35f2a0242, km1126706c1434094bcd1844a8c65806c);
    var km4c843d6c1b741d386c92273822b48c0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4c843d6c1b741d386c92273822b48c0.setDefaultUnit(kony.flex.DP);
    var km5e5cedec926498296dd50ab32d78b42 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km4c50a1c3c324ebd871448f46faea3ff = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km4c843d6c1b741d386c92273822b48c0.add(km5e5cedec926498296dd50ab32d78b42, km4c50a1c3c324ebd871448f46faea3ff);
    var kmab2d43305864704a58cab6499a6f5b1 = new kony.ui.Button({
        "centerY": "15dp",
        "height": "13.93%",
        "id": "btnClearVoiceSearch",
        "right": "9dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmafb451b316f4d7da3615f862298cd00.add(km11177f237044374a959f523f01a3193, km69336b9c5ac462c98a37fc4804d8781, kmbc88f276e5043309915fbb8767f93c0, km4702bd48b634375b671bdf2d93fb226, kmc97ab38482d48a2ad32cf43005318c0, km4c843d6c1b741d386c92273822b48c0, kmab2d43305864704a58cab6499a6f5b1);
    km87a0833383c4874afd9924839a0bd71.add(kmafb451b316f4d7da3615f862298cd00);
    kmdf699fe5922454e80aca3a18bfbe49d.add(km87a0833383c4874afd9924839a0bd71);
    var km5278ed80a8047b2bf4784a6db8dcc29 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km5278ed80a8047b2bf4784a6db8dcc29.setDefaultUnit(kony.flex.DP);
    var km05a5544bd834e6faf8730663c1f82fd = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km05a5544bd834e6faf8730663c1f82fd.setDefaultUnit(kony.flex.DP);
    var kma2c1ab612af452aa434c081dfabbb99 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmdb406f204484d289e414323c6df9bc2 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km05a5544bd834e6faf8730663c1f82fd.add(kma2c1ab612af452aa434c081dfabbb99, kmdb406f204484d289e414323c6df9bc2);
    var kmd0114971dfd483aa3fb353b0df13401 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd0114971dfd483aa3fb353b0df13401.setDefaultUnit(kony.flex.DP);
    var kmdfa727b2cea4b71a766aa9498c0d99a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kma085a1cf7ca4ba3938ca1388f3ed02f = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var kme47e1d3e32d4566a76544fe146f3b91 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmd0114971dfd483aa3fb353b0df13401.add(kmdfa727b2cea4b71a766aa9498c0d99a, kma085a1cf7ca4ba3938ca1388f3ed02f, kme47e1d3e32d4566a76544fe146f3b91);
    var km2edf36072a24ebdb0067e60acc0c7d6 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2edf36072a24ebdb0067e60acc0c7d6.setDefaultUnit(kony.flex.DP);
    var kmd88c1c4dba74f988f8620d19351f52e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2fb65e1ee334a69a142c353281a044a = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km2edf36072a24ebdb0067e60acc0c7d6.add(kmd88c1c4dba74f988f8620d19351f52e, km2fb65e1ee334a69a142c353281a044a);
    var kme137ca9f4aa43b88736defa3888f8f7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme137ca9f4aa43b88736defa3888f8f7.setDefaultUnit(kony.flex.DP);
    var km390b475ae9041f9a71ce09f0e7b7c36 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km9a26cc2237c45bbbe544e1eee4db333 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kme137ca9f4aa43b88736defa3888f8f7.add(km390b475ae9041f9a71ce09f0e7b7c36, km9a26cc2237c45bbbe544e1eee4db333);
    var kmc73014a179e40d2bd4a1943a197d819 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc73014a179e40d2bd4a1943a197d819.setDefaultUnit(kony.flex.DP);
    var km3f22b9b8b9e4c1b9ff9f7fc02a6d929 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km12b40473396434eafbb7f5ada607d48 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmc73014a179e40d2bd4a1943a197d819.add(km3f22b9b8b9e4c1b9ff9f7fc02a6d929, km12b40473396434eafbb7f5ada607d48);
    var km0f34eeb7df64294bbc3c8c67ee77a4a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0f34eeb7df64294bbc3c8c67ee77a4a.setDefaultUnit(kony.flex.DP);
    var km7b2b57c7b0242eeb7453a1825769496 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kme2727a110704a8193f94154758cc92d = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km0f34eeb7df64294bbc3c8c67ee77a4a.add(km7b2b57c7b0242eeb7453a1825769496, kme2727a110704a8193f94154758cc92d);
    var km7f1cdeb7f13475c87148161a5888580 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7f1cdeb7f13475c87148161a5888580.setDefaultUnit(kony.flex.DP);
    var kmf423611fe694906ac04502bceec2961 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km48a9bfadd974fd68f3851cd3bda75d6 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km7f1cdeb7f13475c87148161a5888580.add(kmf423611fe694906ac04502bceec2961, km48a9bfadd974fd68f3851cd3bda75d6);
    var kmbddd0abcb234f30bebcf25d418483dd = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbddd0abcb234f30bebcf25d418483dd.setDefaultUnit(kony.flex.DP);
    var km0a0729cd820430f85a6661c9468d491 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kma8f6db944af491aa5d7e1c83db122c7 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmbddd0abcb234f30bebcf25d418483dd.add(km0a0729cd820430f85a6661c9468d491, kma8f6db944af491aa5d7e1c83db122c7);
    km5278ed80a8047b2bf4784a6db8dcc29.add(km05a5544bd834e6faf8730663c1f82fd, kmd0114971dfd483aa3fb353b0df13401, km2edf36072a24ebdb0067e60acc0c7d6, kme137ca9f4aa43b88736defa3888f8f7, kmc73014a179e40d2bd4a1943a197d819, km0f34eeb7df64294bbc3c8c67ee77a4a, km7f1cdeb7f13475c87148161a5888580, kmbddd0abcb234f30bebcf25d418483dd);
    var kmc2efc532bb447a295d2d890e695ab4b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    kmc2efc532bb447a295d2d890e695ab4b.setDefaultUnit(kony.flex.DP);
    var kmb77400bc6844366ac962066a820f7cb = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kma2da90a26dd49cebd8b4ff35ada675f = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    kma2da90a26dd49cebd8b4ff35ada675f.setDefaultUnit(kony.flex.DP);
    var km4a349f175154fb6946b0cafaaebca3d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km4a349f175154fb6946b0cafaaebca3d.setDefaultUnit(kony.flex.DP);
    var km2bfb568d917489d981c1128f972e7ca = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km4a349f175154fb6946b0cafaaebca3d.add(km2bfb568d917489d981c1128f972e7ca);
    var km8afe03e251c4544bf26cae416cac84e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km8afe03e251c4544bf26cae416cac84e.setDefaultUnit(kony.flex.DP);
    var km580905c7900419fb6b1f2c4bf12ebeb = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var kmae32da15eb4445298459817f8e0c750 = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClearSearch",
        "right": "2%",
        "text": "X",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km8afe03e251c4544bf26cae416cac84e.add(km580905c7900419fb6b1f2c4bf12ebeb, kmae32da15eb4445298459817f8e0c750);
    var km895da94645b4c5889860dd5348b74f7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km895da94645b4c5889860dd5348b74f7.setDefaultUnit(kony.flex.DP);
    var km7b58464c32f481abd02eaa76201a0fe = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    km895da94645b4c5889860dd5348b74f7.add(km7b58464c32f481abd02eaa76201a0fe);
    var kmc7f8b563a9b4bb3b7beb2640bf0b70b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmc7f8b563a9b4bb3b7beb2640bf0b70b.setDefaultUnit(kony.flex.DP);
    var km9c91d1b6d6b49cfbe5fc38f2f7a3088 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmc7f8b563a9b4bb3b7beb2640bf0b70b.add(km9c91d1b6d6b49cfbe5fc38f2f7a3088);
    var km9dd4461e46343d1b820781c090a4339 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCameraImageSearch",
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "isVisible": false,
        "skin": "sknBtnPopupCloseTransparentBg"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km2297f17b25f42eeab2e5a14f9977899 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2297f17b25f42eeab2e5a14f9977899.setDefaultUnit(kony.flex.DP);
    var km17b1a9ae29c4efb8d42ac1fb304e7be = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km2297f17b25f42eeab2e5a14f9977899.add(km17b1a9ae29c4efb8d42ac1fb304e7be);
    kma2da90a26dd49cebd8b4ff35ada675f.add(km4a349f175154fb6946b0cafaaebca3d, km8afe03e251c4544bf26cae416cac84e, km895da94645b4c5889860dd5348b74f7, kmc7f8b563a9b4bb3b7beb2640bf0b70b, km9dd4461e46343d1b820781c090a4339, km2297f17b25f42eeab2e5a14f9977899);
    var km49d461b73f04971a0e111b4eae94702 = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km84ca905f6c14e3ea0c0995c95426c4c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km84ca905f6c14e3ea0c0995c95426c4c.setDefaultUnit(kony.flex.DP);
    var km219d6ad41d04f6db2fa315c101ef8f7 = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km0b60abeee62455cbef5dcd4656b260d = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km84ca905f6c14e3ea0c0995c95426c4c.add(km219d6ad41d04f6db2fa315c101ef8f7, km0b60abeee62455cbef5dcd4656b260d);
    kmc2efc532bb447a295d2d890e695ab4b.add(kmb77400bc6844366ac962066a820f7cb, kma2da90a26dd49cebd8b4ff35ada675f, km49d461b73f04971a0e111b4eae94702, km84ca905f6c14e3ea0c0995c95426c4c);
    frmShoppingList.add(flxMainContainer, km5b85d6cb03e44d9b80505e3e1fcc0e9, km0b06a1ba1e543e4a90eba4239357123, kma426b973afb42da86ecd356d2dbf3ec, flxOverlay, kmdf699fe5922454e80aca3a18bfbe49d, km5278ed80a8047b2bf4784a6db8dcc29, kmc2efc532bb447a295d2d890e695ab4b);
};

function frmShoppingListGlobals() {
    frmShoppingList = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmShoppingList,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmShoppingList",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};