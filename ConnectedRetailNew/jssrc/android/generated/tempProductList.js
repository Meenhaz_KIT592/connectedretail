function initializetempProductList() {
    flxProductItem = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxProductItem",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProductItem.setDefaultUnit(kony.flex.DP);
    var FlexContainer0679bd4f1bb6746 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer0679bd4f1bb6746",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray1234",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0679bd4f1bb6746.setDefaultUnit(kony.flex.DP);
    var flxClearanceText = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "64dp",
        "id": "flxClearanceText",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxClearanceText.setDefaultUnit(kony.flex.DP);
    var CopyflxClearance04dd36e9de23549 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "36dp",
        "id": "CopyflxClearance04dd36e9de23549",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexRedNoBorder",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxClearance04dd36e9de23549.setDefaultUnit(kony.flex.DP);
    var CopylblIconBack04eaa8b2f367a48 = new kony.ui.Label({
        "bottom": "8dp",
        "id": "CopylblIconBack04eaa8b2f367a48",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMedium32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.pdp.clearancetext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "8dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxClearance04dd36e9de23549.add(CopylblIconBack04eaa8b2f367a48);
    var CopylblIconBack0b2b53bc248d74b = new kony.ui.Label({
        "height": "20dp",
        "id": "CopylblIconBack0b2b53bc248d74b",
        "isVisible": true,
        "left": "10dp",
        "right": "27dp",
        "skin": "sknLblGouthamMed16",
        "text": kony.i18n.getLocalizedString("i18n.phone.pdp.clearacedisclaimer"),
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxClearanceText.add(CopyflxClearance04dd36e9de23549, CopylblIconBack0b2b53bc248d74b);
    var FlexGroup0b8478a21c4a44a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexGroup0b8478a21c4a44a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    FlexGroup0b8478a21c4a44a.setDefaultUnit(kony.flex.DP);
    var flxItemDetailsWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemDetailsWrap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemDetailsWrap.setDefaultUnit(kony.flex.DP);
    var imgItem = new kony.ui.Image2({
        "height": "100dp",
        "id": "imgItem",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "sample_product.png",
        "top": "10dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [5, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxItemDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "120dp",
        "isModalContainer": false,
        "right": "10dp",
        "skin": "slFbox",
        "top": "12dp",
        "zIndex": 1
    }, {}, {});
    flxItemDetails.setDefaultUnit(kony.flex.DP);
    var lblItemName = new kony.ui.Label({
        "id": "lblItemName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxDark",
        "text": "Complete Art Easel & Portfolio Set By Artist’s Loft",
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemPrice = new kony.ui.Label({
        "id": "lblItemPrice",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": "$26.99  - $29.99",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItemPriceOld = new kony.ui.Label({
        "id": "lblItemPriceOld",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxLtGray",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxItemStock = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemStock",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemStock.setDefaultUnit(kony.flex.DP);
    var lblItemStock = new kony.ui.Label({
        "height": "15dp",
        "id": "lblItemStock",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamMedium20pxDark",
        "text": "12 IN STOCK",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxItemStock.add(lblItemStock);
    flxItemDetails.add(lblItemName, lblItemPrice, lblItemPriceOld, flxItemStock);
    var flxStarRating = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": "13dp",
        "clipBounds": true,
        "id": "flxStarRating",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "width": "110dp"
    }, {}, {});
    flxStarRating.setDefaultUnit(kony.flex.DP);
    var lblStar1 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar2 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar2",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar3 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar3",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar4 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar4",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStar5 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar5",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStar",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStar1 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar1",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar2 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar2",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar3 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar3",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar4 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar4",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar5 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar5",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblItemNumberReviews = new kony.ui.Label({
        "height": "15dp",
        "id": "lblItemNumberReviews",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamMedium20pxDark",
        "text": "()",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStarRating.add(lblStar1, lblStar2, lblStar3, lblStar4, lblStar5, imgStar1, imgStar2, imgStar3, imgStar4, imgStar5, lblItemNumberReviews);
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "11dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "110dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flxItemDetailsWrap.add(imgItem, flxItemDetails, flxStarRating, flxBottomSpacerDND);
    var flxItemLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "35dp",
        "id": "flxItemLocation",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "121dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemLocation.setDefaultUnit(kony.flex.DP);
    var lblAisleNo = new kony.ui.Label({
        "height": "100%",
        "id": "lblAisleNo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxBlack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMapIcon = new kony.ui.Label({
        "height": "100%",
        "id": "lblMapIcon",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon40pxBlue",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnStoreMap = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnStoreMap",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 12, 0],
        "paddingInPixel": false
    }, {});
    var btnBuyOnline = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnBuyOnline",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": "BUY ONLINE",
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    flxItemLocation.add(lblAisleNo, lblMapIcon, btnStoreMap, btnBuyOnline);
    FlexGroup0b8478a21c4a44a.add(flxItemDetailsWrap, flxItemLocation);
    FlexContainer0679bd4f1bb6746.add(flxClearanceText, FlexGroup0b8478a21c4a44a);
    flxProductItem.add(FlexContainer0679bd4f1bb6746);
}