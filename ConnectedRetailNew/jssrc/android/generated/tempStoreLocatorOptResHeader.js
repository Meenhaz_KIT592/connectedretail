function initializetempStoreLocatorOptResHeader() {
    flxOptResHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxOptResHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    flxOptResHeader.setDefaultUnit(kony.flex.DP);
    var lblOptResHeader = new kony.ui.Label({
        "height": "100%",
        "id": "lblOptResHeader",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "sknLblOptionalResultsHeader",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxOptResHeader.add(lblOptResHeader);
}