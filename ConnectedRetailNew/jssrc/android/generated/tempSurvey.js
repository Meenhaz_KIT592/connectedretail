function initializetempSurvey() {
    CopyFlexContainer00204013b520742 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer00204013b520742",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    CopyFlexContainer00204013b520742.setDefaultUnit(kony.flex.DP);
    var lblSurveyName = new kony.ui.Label({
        "height": "100%",
        "id": "lblSurveyName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSurveySelected = new kony.ui.Label({
        "height": "100%",
        "id": "lblSurveySelected",
        "isVisible": true,
        "left": "50%",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "41%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon34px333333",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer00204013b520742.add(lblSurveyName, lblSurveySelected, lblChevron);
}