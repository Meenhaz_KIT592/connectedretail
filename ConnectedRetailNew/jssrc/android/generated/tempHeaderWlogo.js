function initializetempHeaderWlogo() {
    flxHeaderWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxHeaderWrap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite"
    }, {}, {});
    flxHeaderWrap.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "24dp",
        "id": "imgLogo",
        "isVisible": true,
        "skin": "slImage",
        "src": "logo.png",
        "width": "50%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenu = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnMenuFoc",
        "height": "40dp",
        "id": "btnMenu",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnMenu",
        "text": "A",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCouponMain = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "40dp",
        "id": "btnCouponMain",
        "isVisible": true,
        "onClick": AS_Button_f361424f81c84367840daf398747dbf9,
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(imgLogo, btnMenu, btnCouponMain);
    var flxSearchContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxSearchContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxSearchContainer.setDefaultUnit(kony.flex.DP);
    var flxSearchContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "3dp",
        "width": "95%"
    }, {}, {});
    flxSearchContents.setDefaultUnit(kony.flex.DP);
    var lblSearchIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSearchIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconSearch",
        "text": "S",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var textSearch = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTextSearch",
        "height": "100%",
        "id": "textSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": "What will you make today?",
        "secureTextEntry": false,
        "skin": "sknTextSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [10, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnBarCode = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnMenuFoc",
        "height": "40dp",
        "id": "btnBarCode",
        "isVisible": true,
        "right": "0%",
        "skin": "sknBtnMenu",
        "text": "B",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSearchContents.add(lblSearchIcon, textSearch, btnBarCode);
    var flxSearchShadow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "3dp",
        "id": "flxSearchShadow",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexShadow",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSearchShadow.setDefaultUnit(kony.flex.DP);
    flxSearchShadow.add();
    flxSearchContainer.add(flxSearchContents, flxSearchShadow);
    flxHeaderWrap.add(flxHeader, flxSearchContainer);
}