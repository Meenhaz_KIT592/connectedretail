function addWidgetsfrmMyRewards() {
    frmMyRewards.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var TextField0355b4ebd46864c = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "TextField0355b4ebd46864c",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtFirstName"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyTextField0ba96bd9329b74e = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField0ba96bd9329b74e",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtLastName"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyTextField09deae8d19a5240 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField09deae8d19a5240",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyTextField0af624fa000114e = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField0af624fa000114e",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtConfrimEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var Label01c395a531eb648 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label01c395a531eb648",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknLblGothamRegular18pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.emailServiceRewards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": "200dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyTextField0906a8a8c16db46 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField0906a8a8c16db46",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtPassword"),
        "secureTextEntry": true,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyTextField012d3918e4d8448 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField012d3918e4d8448",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtPassword"),
        "secureTextEntry": true,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyTextField0d6172d59a5c444 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField0d6172d59a5c444",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.zipPostalcode"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var CopyTextField0ba7858de0d4449 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "CopyTextField0ba7858de0d4449",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtPhoneNo"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "sknTextbox",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var FlexContainer08545d32dca7842 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35dp",
        "id": "FlexContainer08545d32dca7842",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    FlexContainer08545d32dca7842.setDefaultUnit(kony.flex.DP);
    var RichText03a24ea0b7b4d43 = new kony.ui.RichText({
        "height": "100%",
        "id": "RichText03a24ea0b7b4d43",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.termsCondition"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgCheck = new kony.ui.Image2({
        "centerY": "50%",
        "height": "14dp",
        "id": "imgCheck",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Button02ba1ec28a8e843 = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "Button02ba1ec28a8e843",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer08545d32dca7842.add(RichText03a24ea0b7b4d43, imgCheck, Button02ba1ec28a8e843);
    var CopyFlexContainer008a1f175435b45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer008a1f175435b45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer008a1f175435b45.setDefaultUnit(kony.flex.DP);
    var CopyRichText07aa66578d43141 = new kony.ui.RichText({
        "height": "100%",
        "id": "CopyRichText07aa66578d43141",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.ageVerify"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyimgCheck00632c2b0ef474f = new kony.ui.Image2({
        "centerY": "50%",
        "height": "14dp",
        "id": "CopyimgCheck00632c2b0ef474f",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyButton0aea953982c4049 = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "CopyButton0aea953982c4049",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer008a1f175435b45.add(CopyRichText07aa66578d43141, CopyimgCheck00632c2b0ef474f, CopyButton0aea953982c4049);
    var CopyFlexContainer0178e706c216d49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer0178e706c216d49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0178e706c216d49.setDefaultUnit(kony.flex.DP);
    var CopyRichText0a9944077f28e4d = new kony.ui.RichText({
        "id": "CopyRichText0a9944077f28e4d",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.rtPrivacyPolicy"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyimgCheck07a50df0a7f854b = new kony.ui.Image2({
        "height": "14dp",
        "id": "CopyimgCheck07a50df0a7f854b",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "top": "0dp",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyButton0f090c761b9764f = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "CopyButton0f090c761b9764f",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0178e706c216d49.add(CopyRichText0a9944077f28e4d, CopyimgCheck07a50df0a7f854b, CopyButton0f090c761b9764f);
    var btnBuyOnline2 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnBuyOnline2",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.joinRewards"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flxMainContainer.add(flxTopSpacerDND, TextField0355b4ebd46864c, CopyTextField0ba96bd9329b74e, CopyTextField09deae8d19a5240, CopyTextField0af624fa000114e, Label01c395a531eb648, CopyTextField0906a8a8c16db46, CopyTextField012d3918e4d8448, CopyTextField0d6172d59a5c444, CopyTextField0ba7858de0d4449, FlexContainer08545d32dca7842, CopyFlexContainer008a1f175435b45, CopyFlexContainer0178e706c216d49, btnBuyOnline2, flxBottomSpacerDND);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.myRewards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnCancel"),
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderTitleContainer.add(lblFormTitle, btnHeaderLeft);
    frmMyRewards.add(flxMainContainer, flxHeaderTitleContainer);
};

function frmMyRewardsGlobals() {
    frmMyRewards = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMyRewards,
        "enabledForIdleTimeout": true,
        "id": "frmMyRewards",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};