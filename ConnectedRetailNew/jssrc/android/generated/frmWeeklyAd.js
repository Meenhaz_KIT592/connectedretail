function addWidgetsfrmWeeklyAd() {
    frmWeeklyAd.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxSubHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxSubHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSubHeader.setDefaultUnit(kony.flex.DP);
    var lblIconChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblIconChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon34px333333",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "97%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDeleteAll = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransMed32px818181Foc",
        "height": "100%",
        "id": "btnDeleteAll",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransMed32px818181",
        "text": "Featured Deals",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [10, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    var lblBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblGothamMedium32pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnBack"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSubHeader.add(lblIconChevron, btnDeleteAll, lblBack);
    var segShoppingList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "centerX": "50%",
        "data": [{
            "imgOffer": "imagedrag.png",
            "imgProduct": "project_thumb.png",
            "lblDateRange": "FEB 21 - FEB 28",
            "lblNewExclusive": "Label",
            "lblNewExclusiveImage": "white_1012x628.png",
            "lblPrice": "40% OFF",
            "lblProductName": "All Floral Stems",
            "lblRegPrice": "",
            "lblStoreInfo": ""
        }, {
            "imgOffer": "imagedrag.png",
            "imgProduct": "project_thumb.png",
            "lblDateRange": "FEB 21 - FEB 28",
            "lblNewExclusive": "Label",
            "lblNewExclusiveImage": "white_1012x628.png",
            "lblPrice": "NOW $14.99 - $64.99",
            "lblProductName": "Baskets & Decorative Boxes",
            "lblRegPrice": "Reg. $29.99  - $129.99",
            "lblStoreInfo": "IN-STORE ONLY"
        }, {
            "imgOffer": "imagedrag.png",
            "imgProduct": "project_thumb.png",
            "lblDateRange": "Label",
            "lblNewExclusive": "Label",
            "lblNewExclusiveImage": "white_1012x628.png",
            "lblPrice": "Label",
            "lblProductName": "Label",
            "lblRegPrice": "Label",
            "lblStoreInfo": "Label"
        }, {
            "imgOffer": "imagedrag.png",
            "imgProduct": "project_thumb.png",
            "lblDateRange": "Label",
            "lblNewExclusive": "Label",
            "lblNewExclusiveImage": "white_1012x628.png",
            "lblPrice": "Label",
            "lblProductName": "Label",
            "lblRegPrice": "Label",
            "lblStoreInfo": "Label"
        }],
        "groupCells": false,
        "id": "segShoppingList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxWeeklyAdRowContainer,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "102dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxBottomSpacer": "flxBottomSpacer",
            "flxContents": "flxContents",
            "flxLabels": "flxLabels",
            "flxSpacer": "flxSpacer",
            "flxWeeklyAdContents": "flxWeeklyAdContents",
            "flxWeeklyAdHeader": "flxWeeklyAdHeader",
            "flxWeeklyAdRowContainer": "flxWeeklyAdRowContainer",
            "imgOffer": "imgOffer",
            "imgProduct": "imgProduct",
            "lblAdditionalDeal": "lblAdditionalDeal",
            "lblDateRange": "lblDateRange",
            "lblNewExclusive": "lblNewExclusive",
            "lblNewExclusiveImage": "lblNewExclusiveImage",
            "lblPrice": "lblPrice",
            "lblProductName": "lblProductName",
            "lblRegPrice": "lblRegPrice",
            "lblStoreInfo": "lblStoreInfo"
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMainContainer.add(flxSubHeader, segShoppingList);
    var kmb71dcfc85d94826943a6b51a5e7e2f1 = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "isMaster": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxSearchResultsContainer",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": 2,
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6,
        "isVisible": false,
        "skin": "sknFormWhite"
    }, {}, {});
    kmb71dcfc85d94826943a6b51a5e7e2f1.setDefaultUnit(kony.flex.DP);
    var kma3c719b47c5414882f78261c9a5ec75 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDNDSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma3c719b47c5414882f78261c9a5ec75.setDefaultUnit(kony.flex.DP);
    kma3c719b47c5414882f78261c9a5ec75.add();
    var kmeab9978c8a047febcdd7b0fd55b5900 = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "id": "segSearchOffers",
        "left": "0dp",
        "scrollingEvents": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "groupCells": false,
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        }
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km3e5ddd07f814e728b37c6c47d50430b = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "id": "segSearchResults",
        "left": "0dp",
        "scrollingEvents": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "groupCells": false,
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        }
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb71dcfc85d94826943a6b51a5e7e2f1.add(kma3c719b47c5414882f78261c9a5ec75, kmeab9978c8a047febcdd7b0fd55b5900, km3e5ddd07f814e728b37c6c47d50430b);
    var km3afbcc4332148b78998adbc88af7422 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    km3afbcc4332148b78998adbc88af7422.setDefaultUnit(kony.flex.DP);
    var kmaaac64b59204f82bf39ea40d10670f8 = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmaaac64b59204f82bf39ea40d10670f8.setDefaultUnit(kony.flex.DP);
    kmaaac64b59204f82bf39ea40d10670f8.add();
    var km10b29776b7e4a5f870500b5fbbda654 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km10b29776b7e4a5f870500b5fbbda654.setDefaultUnit(kony.flex.DP);
    var km9fed55e87f44662a4f0df8bfb3c3621 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd3562e889324037966b494cffa81fdf = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    var km552081f1b4c4697b5edc841442b8307 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km1fa902de8204591af48edd384fd31ef = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    km1fa902de8204591af48edd384fd31ef.setDefaultUnit(kony.flex.DP);
    km1fa902de8204591af48edd384fd31ef.add();
    var km7dabd4ee5574e50b6e812020a0fc624 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km10b29776b7e4a5f870500b5fbbda654.add(km9fed55e87f44662a4f0df8bfb3c3621, kmd3562e889324037966b494cffa81fdf, km552081f1b4c4697b5edc841442b8307, km1fa902de8204591af48edd384fd31ef, km7dabd4ee5574e50b6e812020a0fc624);
    var km96a2af6e2e14fcca6f740bbcb8c9061 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km96a2af6e2e14fcca6f740bbcb8c9061.setDefaultUnit(kony.flex.DP);
    var km0dd61f2bdea4b4d97aeda2d7f92ab50 = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km0dd61f2bdea4b4d97aeda2d7f92ab50.setDefaultUnit(kony.flex.DP);
    var kmc4790a05585475e9ba2b7b32fe4ade8 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    kmc4790a05585475e9ba2b7b32fe4ade8.setDefaultUnit(kony.flex.DP);
    var km63c3fffdcb149ebade5d7e678b66d47 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmc4790a05585475e9ba2b7b32fe4ade8.add(km63c3fffdcb149ebade5d7e678b66d47);
    var km10dfda5ff1f43a09bd01270fe8915dc = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    km10dfda5ff1f43a09bd01270fe8915dc.setDefaultUnit(kony.flex.DP);
    var kmeb842d08df14e2f8e89bb657e0f8db1 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km10dfda5ff1f43a09bd01270fe8915dc.add(kmeb842d08df14e2f8e89bb657e0f8db1);
    var km7e3204a0b664658bdaad4180d775f02 = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km7e3204a0b664658bdaad4180d775f02.setDefaultUnit(kony.flex.DP);
    var kmefeb68f84794b93b80e8dd144dd2d47 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km4f98524196e4c64a4ed2c5ecda9e00f = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km7e3204a0b664658bdaad4180d775f02.add(kmefeb68f84794b93b80e8dd144dd2d47, km4f98524196e4c64a4ed2c5ecda9e00f);
    var kmb608440f29e431e875d6c2c958c6ce6 = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km0dd61f2bdea4b4d97aeda2d7f92ab50.add(kmc4790a05585475e9ba2b7b32fe4ade8, km10dfda5ff1f43a09bd01270fe8915dc, km7e3204a0b664658bdaad4180d775f02, kmb608440f29e431e875d6c2c958c6ce6);
    var kmbd17302d92c4193b58e5b3a50994c96 = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmbd17302d92c4193b58e5b3a50994c96.setDefaultUnit(kony.flex.DP);
    kmbd17302d92c4193b58e5b3a50994c96.add();
    km96a2af6e2e14fcca6f740bbcb8c9061.add(km0dd61f2bdea4b4d97aeda2d7f92ab50, kmbd17302d92c4193b58e5b3a50994c96);
    var kmcad16c26fba4540a071148683344bfb = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmcad16c26fba4540a071148683344bfb.setDefaultUnit(kony.flex.DP);
    var km5c135a581c4471d8d1924ddd8aa958e = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km5c135a581c4471d8d1924ddd8aa958e.setDefaultUnit(kony.flex.DP);
    var km44d5217e6074c31b5a5eb470f2f7e75 = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km44d5217e6074c31b5a5eb470f2f7e75.setDefaultUnit(kony.flex.DP);
    var km1d2fc3f9948425c9422c1ce6eacaf7c = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km94ced1ba8354de0918851aa6ddc2077 = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km44d5217e6074c31b5a5eb470f2f7e75.add(km1d2fc3f9948425c9422c1ce6eacaf7c, km94ced1ba8354de0918851aa6ddc2077);
    var kmbd0a4137f9e439486061be7c7ddae82 = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmbd0a4137f9e439486061be7c7ddae82.setDefaultUnit(kony.flex.DP);
    var kmfa623d3262e48dc859c5f067657a5e7 = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmfa623d3262e48dc859c5f067657a5e7.setDefaultUnit(kony.flex.DP);
    var kme89c38d330d47edb61306548219a57a = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmcfbce35d3cb47e4ba214ad8d14ff08f = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km796f06fa8ea49758024c614a5924326 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km02c04cab9274b5fb7a05b7781be001e = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km635bad1627748fd9f6a12aec2e04d10 = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km437767690a648f295451ae180c8c046 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmfa623d3262e48dc859c5f067657a5e7.add(kme89c38d330d47edb61306548219a57a, kmcfbce35d3cb47e4ba214ad8d14ff08f, km796f06fa8ea49758024c614a5924326, km02c04cab9274b5fb7a05b7781be001e, km635bad1627748fd9f6a12aec2e04d10, km437767690a648f295451ae180c8c046);
    var km0cf70880e984f1bb63a1d94a783c918 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km0cf70880e984f1bb63a1d94a783c918.setDefaultUnit(kony.flex.DP);
    var km4c55c9ab0b74ffbbf1b6ce4c6b530f1 = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km526077a2786478b945b074ba89f9c36 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc83480f6e0844d49c69a0f8d0f7c982 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km0cf70880e984f1bb63a1d94a783c918.add(km4c55c9ab0b74ffbbf1b6ce4c6b530f1, km526077a2786478b945b074ba89f9c36, kmc83480f6e0844d49c69a0f8d0f7c982);
    kmbd0a4137f9e439486061be7c7ddae82.add(kmfa623d3262e48dc859c5f067657a5e7, km0cf70880e984f1bb63a1d94a783c918);
    var km6dc7f4596af43f48c9ce464206f5a9b = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km516de80b3804d44a4147d6e2d168089 = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km5c135a581c4471d8d1924ddd8aa958e.add(km44d5217e6074c31b5a5eb470f2f7e75, kmbd0a4137f9e439486061be7c7ddae82, km6dc7f4596af43f48c9ce464206f5a9b, km516de80b3804d44a4147d6e2d168089);
    kmcad16c26fba4540a071148683344bfb.add(km5c135a581c4471d8d1924ddd8aa958e);
    km3afbcc4332148b78998adbc88af7422.add(kmaaac64b59204f82bf39ea40d10670f8, km10b29776b7e4a5f870500b5fbbda654, km96a2af6e2e14fcca6f740bbcb8c9061, kmcad16c26fba4540a071148683344bfb);
    var km551fec8b96e41d9be0f24bbcd5181c5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km551fec8b96e41d9be0f24bbcd5181c5.setDefaultUnit(kony.flex.DP);
    var km67ec9ea63c049c385d4cc86b3a8f1d5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    km67ec9ea63c049c385d4cc86b3a8f1d5.setDefaultUnit(kony.flex.DP);
    var kmf32c247493146fa8f2f98f822e6aab9 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf32c247493146fa8f2f98f822e6aab9.setDefaultUnit(kony.flex.DP);
    var kma4aa5c175574ac382ff54e41b2be2ce = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km1b908baa6b84d66abfdcf0e11fe7a3a = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km1b908baa6b84d66abfdcf0e11fe7a3a.setDefaultUnit(kony.flex.DP);
    var km29506ea276d42f0a452732cc218aeae = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kma61a105643c475cbf30a26c2d4fee21 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km555d34c59c64b348bf674f8c21e2ef6 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km1b908baa6b84d66abfdcf0e11fe7a3a.add(km29506ea276d42f0a452732cc218aeae, kma61a105643c475cbf30a26c2d4fee21, km555d34c59c64b348bf674f8c21e2ef6);
    var kme1c9cff35734b28bb9cc60432893933 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmfd0f9d91d7348d98d7c122ef0ca4c3d = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmf32c247493146fa8f2f98f822e6aab9.add(kma4aa5c175574ac382ff54e41b2be2ce, km1b908baa6b84d66abfdcf0e11fe7a3a, kme1c9cff35734b28bb9cc60432893933, kmfd0f9d91d7348d98d7c122ef0ca4c3d);
    var km8fab5d84e1a4f4390eb42b377843603 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    km8fab5d84e1a4f4390eb42b377843603.setDefaultUnit(kony.flex.DP);
    var kme4328d3a943479e8d1976157d5a1a54 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme4328d3a943479e8d1976157d5a1a54.setDefaultUnit(kony.flex.DP);
    var km7a155c82df94cae9a79103bb4760670 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kme4328d3a943479e8d1976157d5a1a54.add(km7a155c82df94cae9a79103bb4760670);
    km8fab5d84e1a4f4390eb42b377843603.add(kme4328d3a943479e8d1976157d5a1a54);
    var km0660c04a13c4b8b958466c1b9667164 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km67ec9ea63c049c385d4cc86b3a8f1d5.add(kmf32c247493146fa8f2f98f822e6aab9, km8fab5d84e1a4f4390eb42b377843603, km0660c04a13c4b8b958466c1b9667164);
    km551fec8b96e41d9be0f24bbcd5181c5.add(km67ec9ea63c049c385d4cc86b3a8f1d5);
    var kma6de36e15d54c39ba97db0e400fd4bd = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma6de36e15d54c39ba97db0e400fd4bd.setDefaultUnit(kony.flex.DP);
    var kmb8968e320124d80b582eba62d7ea88c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    kmb8968e320124d80b582eba62d7ea88c.setDefaultUnit(kony.flex.DP);
    var kmf241f4a4dd143c1985b166bf2746f6c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kmf241f4a4dd143c1985b166bf2746f6c.setDefaultUnit(kony.flex.DP);
    var kmc1186ab06734be8b6269936036b1480 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    kmc1186ab06734be8b6269936036b1480.setDefaultUnit(kony.flex.DP);
    kmc1186ab06734be8b6269936036b1480.add();
    var km3914aed433641779cbfcb510529cc91 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmb1aeca3f1524a95b5ab9ddca47671f9 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km67b6541226c4f51930c54eb2cacbc22 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km67b6541226c4f51930c54eb2cacbc22.setDefaultUnit(kony.flex.DP);
    var km71335d017364439b4fafa3969d7a0f4 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km4038a40ae534ed194e00a7d66e7c9e6 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km67b6541226c4f51930c54eb2cacbc22.add(km71335d017364439b4fafa3969d7a0f4, km4038a40ae534ed194e00a7d66e7c9e6);
    var km0ee89accb364f3799918503d578be66 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0ee89accb364f3799918503d578be66.setDefaultUnit(kony.flex.DP);
    var km37c83b6a4d74a62bd208092efe6c649 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd158ad1ca064f7dbad3832b77a4ca45 = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    km0ee89accb364f3799918503d578be66.add(km37c83b6a4d74a62bd208092efe6c649, kmd158ad1ca064f7dbad3832b77a4ca45);
    var km030423a0fec46e4a9e96ca4083eef42 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km030423a0fec46e4a9e96ca4083eef42.setDefaultUnit(kony.flex.DP);
    var km9027771a00248a4bd59edfaa7f9f35e = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km77418b3b59545b79e86ff724a309b91 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km030423a0fec46e4a9e96ca4083eef42.add(km9027771a00248a4bd59edfaa7f9f35e, km77418b3b59545b79e86ff724a309b91);
    var kmc02f3fc7e0549a7a0b9e3ca4a73d849 = new kony.ui.Button({
        "centerY": "15dp",
        "height": "13.93%",
        "id": "btnClearVoiceSearch",
        "right": "9dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf241f4a4dd143c1985b166bf2746f6c.add(kmc1186ab06734be8b6269936036b1480, km3914aed433641779cbfcb510529cc91, kmb1aeca3f1524a95b5ab9ddca47671f9, km67b6541226c4f51930c54eb2cacbc22, km0ee89accb364f3799918503d578be66, km030423a0fec46e4a9e96ca4083eef42, kmc02f3fc7e0549a7a0b9e3ca4a73d849);
    kmb8968e320124d80b582eba62d7ea88c.add(kmf241f4a4dd143c1985b166bf2746f6c);
    kma6de36e15d54c39ba97db0e400fd4bd.add(kmb8968e320124d80b582eba62d7ea88c);
    var km9e74bdd36c64d5384b45a6ff683c929 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km9e74bdd36c64d5384b45a6ff683c929.setDefaultUnit(kony.flex.DP);
    var km9a1b0b63cbe403eb615603afb3bc59a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9a1b0b63cbe403eb615603afb3bc59a.setDefaultUnit(kony.flex.DP);
    var km6412b1779414362968292d80624e9c8 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km4ac87a478704f4d8d985cb255eb345f = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km9a1b0b63cbe403eb615603afb3bc59a.add(km6412b1779414362968292d80624e9c8, km4ac87a478704f4d8d985cb255eb345f);
    var km75805c31a3c4e1faf2a49f082272320 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km75805c31a3c4e1faf2a49f082272320.setDefaultUnit(kony.flex.DP);
    var kmf3ac36ffde9493eb1c8982c11f35874 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc98b68d7b394a588118e1e188ac4d8b = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var km083bbf0bda941c490e4e81b2a61f7c7 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km75805c31a3c4e1faf2a49f082272320.add(kmf3ac36ffde9493eb1c8982c11f35874, kmc98b68d7b394a588118e1e188ac4d8b, km083bbf0bda941c490e4e81b2a61f7c7);
    var km3f110ba898e4375a59c79af9bc88c2a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3f110ba898e4375a59c79af9bc88c2a.setDefaultUnit(kony.flex.DP);
    var kmf1885f405d94d83a7c81e8fb61ef0f0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km773c5d5370c4ff5a2bf1442853d014f = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km3f110ba898e4375a59c79af9bc88c2a.add(kmf1885f405d94d83a7c81e8fb61ef0f0, km773c5d5370c4ff5a2bf1442853d014f);
    var km8e6d76b23e243259de52bc8fa99349d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8e6d76b23e243259de52bc8fa99349d.setDefaultUnit(kony.flex.DP);
    var kmaa41e9b46914875b313ca4bcc64e35e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km42335d3ceb1493b87222ae0bffcd367 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km8e6d76b23e243259de52bc8fa99349d.add(kmaa41e9b46914875b313ca4bcc64e35e, km42335d3ceb1493b87222ae0bffcd367);
    var km66e9215313f4e93a63e753e57bc297b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km66e9215313f4e93a63e753e57bc297b.setDefaultUnit(kony.flex.DP);
    var kmbb490e4efab46edae91d707d105057a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km1a54a0bd66a46a4b88aa4b44863e85a = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km66e9215313f4e93a63e753e57bc297b.add(kmbb490e4efab46edae91d707d105057a, km1a54a0bd66a46a4b88aa4b44863e85a);
    var kma0585a690674ea8a95a980241d01c6a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma0585a690674ea8a95a980241d01c6a.setDefaultUnit(kony.flex.DP);
    var kma465bf1c0e0416c93e11f13cb88dc29 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km673350f9f5b48fa80c2c1a12ef950c9 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kma0585a690674ea8a95a980241d01c6a.add(kma465bf1c0e0416c93e11f13cb88dc29, km673350f9f5b48fa80c2c1a12ef950c9);
    var kmdf66c2e7cd242f28e11cd04e606c2c3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmdf66c2e7cd242f28e11cd04e606c2c3.setDefaultUnit(kony.flex.DP);
    var km3b2c00a34cc40d594aa4e120bce17a7 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2ecc908af3842a8acfa2350b36006ff = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmdf66c2e7cd242f28e11cd04e606c2c3.add(km3b2c00a34cc40d594aa4e120bce17a7, km2ecc908af3842a8acfa2350b36006ff);
    var kmbcffd41d32f443db0c58a8c898a5c6c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbcffd41d32f443db0c58a8c898a5c6c.setDefaultUnit(kony.flex.DP);
    var kmeffb3377b3541ae92054dc7655f2951 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km52318d2578b4fbba6f1b1873c6e4600 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmbcffd41d32f443db0c58a8c898a5c6c.add(kmeffb3377b3541ae92054dc7655f2951, km52318d2578b4fbba6f1b1873c6e4600);
    km9e74bdd36c64d5384b45a6ff683c929.add(km9a1b0b63cbe403eb615603afb3bc59a, km75805c31a3c4e1faf2a49f082272320, km3f110ba898e4375a59c79af9bc88c2a, km8e6d76b23e243259de52bc8fa99349d, km66e9215313f4e93a63e753e57bc297b, kma0585a690674ea8a95a980241d01c6a, kmdf66c2e7cd242f28e11cd04e606c2c3, kmbcffd41d32f443db0c58a8c898a5c6c);
    var km6044463b433455987488f9e06b2130f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    km6044463b433455987488f9e06b2130f.setDefaultUnit(kony.flex.DP);
    var kmb57df22901a4dfeadd972ac8b6802fc = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km7a46ddd1a554416a736779a264a01ca = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    km7a46ddd1a554416a736779a264a01ca.setDefaultUnit(kony.flex.DP);
    var km2850a1b57e84966bea7636e7f2470f1 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km2850a1b57e84966bea7636e7f2470f1.setDefaultUnit(kony.flex.DP);
    var kmb1030e1a87a4ff0a880bb813d9f47b3 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km2850a1b57e84966bea7636e7f2470f1.add(kmb1030e1a87a4ff0a880bb813d9f47b3);
    var km5d8298f338c4a6e853559a13aebc108 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km5d8298f338c4a6e853559a13aebc108.setDefaultUnit(kony.flex.DP);
    var kmd19ad047bdb4a449bd3107a6e933d53 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km6747bae4c8a49d98b3a86289880b089 = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClearSearch",
        "right": "2%",
        "text": "X",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km5d8298f338c4a6e853559a13aebc108.add(kmd19ad047bdb4a449bd3107a6e933d53, km6747bae4c8a49d98b3a86289880b089);
    var km3ed2f44238745ec9b37d2d4dce44763 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km3ed2f44238745ec9b37d2d4dce44763.setDefaultUnit(kony.flex.DP);
    var km38389eeb1994005993a1ab1b177946b = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    km3ed2f44238745ec9b37d2d4dce44763.add(km38389eeb1994005993a1ab1b177946b);
    var km977dcccdaee49949fb7708012e2bd70 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km977dcccdaee49949fb7708012e2bd70.setDefaultUnit(kony.flex.DP);
    var km770f4201f7545a29f0a99e862716443 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km977dcccdaee49949fb7708012e2bd70.add(km770f4201f7545a29f0a99e862716443);
    var km45660a7270242b891b5dd787352180b = new kony.ui.Button({
        "height": "100%",
        "id": "btnCameraImageSearch",
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "isVisible": false,
        "skin": "sknBtnPopupCloseTransparentBg"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km2741ae76a8d4b6c9548a9f41e8c0b1e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2741ae76a8d4b6c9548a9f41e8c0b1e.setDefaultUnit(kony.flex.DP);
    var km244896cebb746aaac05918b8d2f4d64 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km2741ae76a8d4b6c9548a9f41e8c0b1e.add(km244896cebb746aaac05918b8d2f4d64);
    km7a46ddd1a554416a736779a264a01ca.add(km2850a1b57e84966bea7636e7f2470f1, km5d8298f338c4a6e853559a13aebc108, km3ed2f44238745ec9b37d2d4dce44763, km977dcccdaee49949fb7708012e2bd70, km45660a7270242b891b5dd787352180b, km2741ae76a8d4b6c9548a9f41e8c0b1e);
    var km9c83a6c88704b5e891c4d5462bb8d1b = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km26796ea2bc649a1ad5e2446c28f4e22 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km26796ea2bc649a1ad5e2446c28f4e22.setDefaultUnit(kony.flex.DP);
    var kmd070f745e094628b49662f03984d01b = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km231821a306f4fc2ab007ce02611c5b9 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km26796ea2bc649a1ad5e2446c28f4e22.add(kmd070f745e094628b49662f03984d01b, km231821a306f4fc2ab007ce02611c5b9);
    km6044463b433455987488f9e06b2130f.add(kmb57df22901a4dfeadd972ac8b6802fc, km7a46ddd1a554416a736779a264a01ca, km9c83a6c88704b5e891c4d5462bb8d1b, km26796ea2bc649a1ad5e2446c28f4e22);
    frmWeeklyAd.add(flxMainContainer, kmb71dcfc85d94826943a6b51a5e7e2f1, km3afbcc4332148b78998adbc88af7422, km551fec8b96e41d9be0f24bbcd5181c5, kma6de36e15d54c39ba97db0e400fd4bd, km9e74bdd36c64d5384b45a6ff683c929, km6044463b433455987488f9e06b2130f);
};

function frmWeeklyAdGlobals() {
    frmWeeklyAd = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmWeeklyAd,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};