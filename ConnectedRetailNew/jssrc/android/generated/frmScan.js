function addWidgetsfrmScan() {
    frmScan.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var cusBarCodeWid = new customWidget.barcodescanner({
        "id": "cusBarCodeWid",
        "isVisible": true,
        "bottom": "70dp",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "widgetName": "barcodescanner"
    });
    var flxBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottom.setDefaultUnit(kony.flex.DP);
    flxBottom.add();
    flxMainContainer.add(flxTopSpacerDND, cusBarCodeWid, flxBottom);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var btnBack = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIcon36pxWhiteFocus",
        "height": "50dp",
        "id": "btnBack",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "text": "x",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgBarcode = new kony.ui.Image2({
        "centerY": "50%",
        "height": "25dp",
        "id": "imgBarcode",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "barimage.png",
        "width": "25dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBarcodeText = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblBarcodeText",
        "isVisible": true,
        "skin": "sknLblGothamRegular28pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmScan.scanTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(btnBack, imgBarcode, lblBarcodeText);
    frmScan.add(flxMainContainer, flxHeader);
};

function frmScanGlobals() {
    frmScan = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmScan,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmScan",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};