function initializetempMenuList() {
    flexMenuListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flexMenuListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    flexMenuListContainer.setDefaultUnit(kony.flex.DP);
    var lblIconMenuList = new kony.ui.Label({
        "centerY": "50%",
        "height": "45dp",
        "id": "lblIconMenuList",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblIconMoreMenu",
        "text": "A",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMenuListItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMenuListItem",
        "isVisible": true,
        "left": "60dp",
        "skin": "sknLblMontserratSemiBold33333316px",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIconMenuArrow = new kony.ui.Label({
        "centerY": "50%",
        "height": "25dp",
        "id": "lblIconMenuArrow",
        "isVisible": true,
        "right": "4%",
        "skin": "sknLblIconMoreMenu66666616px",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "20dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexMenuListContainer.add(lblIconMenuList, lblMenuListItem, lblIconMenuArrow);
}