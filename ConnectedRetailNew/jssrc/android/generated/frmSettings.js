function addWidgetsfrmSettings() {
    frmSettings.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var FlexPushNotification = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "FlexPushNotification",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexPushNotification.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0be8f23751c864a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0be8f23751c864a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0be8f23751c864a.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0be8f23751c864a.add();
    var CopyFlexContainer0cad3f9962b7843 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopyFlexContainer0cad3f9962b7843",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0cad3f9962b7843.setDefaultUnit(kony.flex.DP);
    var CopylblRegionLanguageValue09da8bdc00f454a = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblRegionLanguageValue09da8bdc00f454a",
        "isVisible": true,
        "left": "3%",
        "right": "0%",
        "skin": "sknLblGothamRegular24pxDark",
        "text": "Notifications",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0ee5eaa8b8d3641 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0ee5eaa8b8d3641",
        "isVisible": false,
        "left": "5dp",
        "skin": "sknLblGothamBook24pxLtGray",
        "text": "More Info",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0cad3f9962b7843.add(CopylblRegionLanguageValue09da8bdc00f454a, CopyLabel0ee5eaa8b8d3641);
    var pushSwitch = new kony.ui.Image2({
        "height": "30dp",
        "id": "pushSwitch",
        "isVisible": true,
        "right": "3%",
        "skin": "slImage",
        "src": "on.png",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 15
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    FlexPushNotification.add(CopyFlexContainer0be8f23751c864a, CopyFlexContainer0cad3f9962b7843, pushSwitch);
    var flexStoreAlerts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flexStoreAlerts",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexStoreAlerts.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0f2325f72b3da4f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0f2325f72b3da4f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0f2325f72b3da4f.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0f2325f72b3da4f.add();
    var FlexContainer03b7981d727654a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlexContainer03b7981d727654a",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    FlexContainer03b7981d727654a.setDefaultUnit(kony.flex.DP);
    var CopylblRegionLanguageValue0eb09316535ee42 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblRegionLanguageValue0eb09316535ee42",
        "isVisible": true,
        "left": "3%",
        "right": "0%",
        "skin": "sknLblGothamRegular24pxDark",
        "text": "Store Alerts",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0bb4c5dae702749 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0bb4c5dae702749",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBook24pxLtGray",
        "text": "More Info",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer03b7981d727654a.add(CopylblRegionLanguageValue0eb09316535ee42, CopyLabel0bb4c5dae702749);
    var CopypushSwitch0c03cca77622545 = new kony.ui.Image2({
        "height": "30dp",
        "id": "CopypushSwitch0c03cca77622545",
        "isVisible": true,
        "right": "3%",
        "skin": "slImage",
        "src": "on.png",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 15
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexStoreAlerts.add(CopyFlexContainer0f2325f72b3da4f, FlexContainer03b7981d727654a, CopypushSwitch0c03cca77622545);
    var CopyFlexContainer0f662b4c647104a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "41dp",
        "id": "CopyFlexContainer0f662b4c647104a",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0f662b4c647104a.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer03dbde56a1f2641 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer03dbde56a1f2641",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer03dbde56a1f2641.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer03dbde56a1f2641.add();
    var Switch0709139a7570a49 = new kony.ui.Switch({
        "centerY": "50%",
        "height": "26dp",
        "id": "Switch0709139a7570a49",
        "isVisible": true,
        "leftSideText": "ON",
        "right": "3%",
        "rightSideText": "OFF",
        "selectedIndex": 1,
        "skin": "slSwitch",
        "top": "0dp",
        "width": "43dp",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0f8467cd0293047 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlexContainer0f8467cd0293047",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0f8467cd0293047.setDefaultUnit(kony.flex.DP);
    var Label084be9f6e859741 = new kony.ui.Label({
        "height": "100%",
        "id": "Label084be9f6e859741",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Notifications",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel08331c6c588954b = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel08331c6c588954b",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBook24pxLtGray",
        "text": "More Info",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "35%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0f8467cd0293047.add(Label084be9f6e859741, CopyLabel08331c6c588954b);
    CopyFlexContainer0f662b4c647104a.add(CopyFlexContainer03dbde56a1f2641, Switch0709139a7570a49, FlexContainer0f8467cd0293047);
    var flexTouchToggle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flexTouchToggle",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexTouchToggle.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer09d393a2a728243 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer09d393a2a728243",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer09d393a2a728243.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer09d393a2a728243.add();
    var CopylblRegionLanguageValue09a67f5778b7946 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblRegionLanguageValue09a67f5778b7946",
        "isVisible": true,
        "left": "2.97%",
        "right": "0%",
        "skin": "sknLblGothamRegular24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.Settings.touchId"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var switchTouch = new kony.ui.Image2({
        "height": "30dp",
        "id": "switchTouch",
        "isVisible": true,
        "right": "3%",
        "skin": "slImage",
        "src": "on.png",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 15
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexTouchToggle.add(CopyFlexContainer09d393a2a728243, CopylblRegionLanguageValue09a67f5778b7946, switchTouch);
    var CopyFlexContainer065db0f06273b43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "41dp",
        "id": "CopyFlexContainer065db0f06273b43",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer065db0f06273b43.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0a6adf42e65a54a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0a6adf42e65a54a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0a6adf42e65a54a.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0a6adf42e65a54a.add();
    var CopyLabel01f6055d2890e49 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel01f6055d2890e49",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Use TouchID to Sign In",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopySwitch02182ca9db1e949 = new kony.ui.Switch({
        "centerY": "50%",
        "height": "26dp",
        "id": "CopySwitch02182ca9db1e949",
        "isVisible": true,
        "leftSideText": "ON",
        "right": "3%",
        "rightSideText": "OFF",
        "selectedIndex": 1,
        "skin": "slSwitch",
        "top": "0dp",
        "width": "43dp",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer065db0f06273b43.add(CopyFlexContainer0a6adf42e65a54a, CopyLabel01f6055d2890e49, CopySwitch02182ca9db1e949);
    var flxRegionLanguage = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flxRegionLanguage",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRegionLanguage.setDefaultUnit(kony.flex.DP);
    var flxRegionGap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxRegionGap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRegionGap.setDefaultUnit(kony.flex.DP);
    flxRegionGap.add();
    var lblRegionLanguageValue = new kony.ui.Label({
        "height": "100%",
        "id": "lblRegionLanguageValue",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblGothamRegular24pxDark",
        "text": "United States",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnRegionLanguage = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnRegionLanguage",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.btnRegionLang"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRegionLanguage.add(flxRegionGap, lblRegionLanguageValue, btnRegionLanguage);
    var CopyFlexContainer0792d6ad92f2e4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopyFlexContainer0792d6ad92f2e4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0792d6ad92f2e4b.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0b0f6bc1c5f384c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0b0f6bc1c5f384c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0b0f6bc1c5f384c.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0b0f6bc1c5f384c.add();
    var CopyLabel0824bb7e3e95b4d = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0824bb7e3e95b4d",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnTermsandConditions = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnTermsandConditions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.btnTermsandConditions"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0792d6ad92f2e4b.add(CopyFlexContainer0b0f6bc1c5f384c, CopyLabel0824bb7e3e95b4d, btnTermsandConditions);
    var CopyFlexContainer005dece4959884b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "CopyFlexContainer005dece4959884b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer005dece4959884b.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer05286a7f1cdcc43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer05286a7f1cdcc43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer05286a7f1cdcc43.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer05286a7f1cdcc43.add();
    var CopyLabel0d1d1de78068e44 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0d1d1de78068e44",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnPrivacyPolicy = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnPrivacyPolicy",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.btnPrivacyPolicy"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer005dece4959884b.add(CopyFlexContainer05286a7f1cdcc43, CopyLabel0d1d1de78068e44, btnPrivacyPolicy);
    var flexCCPALink = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flexCCPALink",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexCCPALink.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0d5d35ac1a9144c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0d5d35ac1a9144c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0d5d35ac1a9144c.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0d5d35ac1a9144c.add();
    var CopyLabel0j2155dbf3b7a46 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0j2155dbf3b7a46",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnCCPALink = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnCCPALink",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.lblCCPA"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexCCPALink.add(CopyFlexContainer0d5d35ac1a9144c, CopyLabel0j2155dbf3b7a46, btnCCPALink);
    var flxSignOut = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "38dp",
        "id": "flxSignOut",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSignOut.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0e6622d8b3bb94b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0e6622d8b3bb94b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0e6622d8b3bb94b.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0e6622d8b3bb94b.add();
    var btnSignout = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnSignout",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.btnSignout"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSignOut.add(CopyFlexContainer0e6622d8b3bb94b, btnSignout);
    var Label01c395a531eb648 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label01c395a531eb648",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknLblGothamBook20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.copyRights"),
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAppVersion = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblAppVersion",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknLblGothamRegular24pxDark",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30dp",
        "width": "250dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMainContainer.add(flxTopSpacerDND, FlexPushNotification, flexStoreAlerts, CopyFlexContainer0f662b4c647104a, flexTouchToggle, CopyFlexContainer065db0f06273b43, flxRegionLanguage, CopyFlexContainer0792d6ad92f2e4b, CopyFlexContainer005dece4959884b, flexCCPALink, flxSignOut, Label01c395a531eb648, lblAppVersion);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.settings"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0ee00528ffdfe4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0ee00528ffdfe4c",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, btnHeaderLeft, Label0ee00528ffdfe4c);
    var km78db149dd394eb2adc0ca060cba037d = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km78db149dd394eb2adc0ca060cba037d.setDefaultUnit(kony.flex.DP);
    var km2ae07e8e1a345e68a07c95da20aca82 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2ae07e8e1a345e68a07c95da20aca82.setDefaultUnit(kony.flex.DP);
    var km13b363847b244fb92057905cb0e4029 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km57f0f9d2cf54ee98e59671e93de289b = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km2ae07e8e1a345e68a07c95da20aca82.add(km13b363847b244fb92057905cb0e4029, km57f0f9d2cf54ee98e59671e93de289b);
    var kmf76ccc6c9ed4d97bd4dd6f60a25fa61 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf76ccc6c9ed4d97bd4dd6f60a25fa61.setDefaultUnit(kony.flex.DP);
    var kmf89f95a30b54c6193312d78de90f20f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km63f8077243f420f94b18ae948b98f57 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var km96a1dfba852467eb5a8bf002a13b7f8 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf76ccc6c9ed4d97bd4dd6f60a25fa61.add(kmf89f95a30b54c6193312d78de90f20f, km63f8077243f420f94b18ae948b98f57, km96a1dfba852467eb5a8bf002a13b7f8);
    var kmc4171cbb54645babcaaac794affae6e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc4171cbb54645babcaaac794affae6e.setDefaultUnit(kony.flex.DP);
    var km673bf24e25649d79849c2939d62b6a0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd00d8e76fc246319bac6d04ef69864b = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmc4171cbb54645babcaaac794affae6e.add(km673bf24e25649d79849c2939d62b6a0, kmd00d8e76fc246319bac6d04ef69864b);
    var km35d1c50fb7a4a10b5d077ea64b99be5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km35d1c50fb7a4a10b5d077ea64b99be5.setDefaultUnit(kony.flex.DP);
    var km97393712c4a4d058080cfb231ef3a3d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd753e7bbee4438483e8e87023205357 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km35d1c50fb7a4a10b5d077ea64b99be5.add(km97393712c4a4d058080cfb231ef3a3d, kmd753e7bbee4438483e8e87023205357);
    var km59bd4bbc64946f18431932dbdb7dcc3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km59bd4bbc64946f18431932dbdb7dcc3.setDefaultUnit(kony.flex.DP);
    var kmb7a9f4885cb41b7a83b22cc81bd80fc = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc41d38cf3984959908b9abc9c0f24ae = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km59bd4bbc64946f18431932dbdb7dcc3.add(kmb7a9f4885cb41b7a83b22cc81bd80fc, kmc41d38cf3984959908b9abc9c0f24ae);
    var km46b66738bcc4a97bfaaed8694f81a16 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km46b66738bcc4a97bfaaed8694f81a16.setDefaultUnit(kony.flex.DP);
    var km5889ad8b639494ebdaaa65e494da9c4 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km15b184f3aa4421498b9465c37b6f5b0 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km46b66738bcc4a97bfaaed8694f81a16.add(km5889ad8b639494ebdaaa65e494da9c4, km15b184f3aa4421498b9465c37b6f5b0);
    var km442df96fcb04e3f9858315cfc5dc078 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km442df96fcb04e3f9858315cfc5dc078.setDefaultUnit(kony.flex.DP);
    var kmdfdf08b7b2a4f80ac1e01de9511e3b5 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kme85533fe3e24ae69b376db16147c18f = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km442df96fcb04e3f9858315cfc5dc078.add(kmdfdf08b7b2a4f80ac1e01de9511e3b5, kme85533fe3e24ae69b376db16147c18f);
    var km11b3fe87f0646e3b1cd3c93246e4f34 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km11b3fe87f0646e3b1cd3c93246e4f34.setDefaultUnit(kony.flex.DP);
    var kmc3890db238f47e69abddd7b654dbf61 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2432070c2454813b313467bea1b9258 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km11b3fe87f0646e3b1cd3c93246e4f34.add(kmc3890db238f47e69abddd7b654dbf61, km2432070c2454813b313467bea1b9258);
    km78db149dd394eb2adc0ca060cba037d.add(km2ae07e8e1a345e68a07c95da20aca82, kmf76ccc6c9ed4d97bd4dd6f60a25fa61, kmc4171cbb54645babcaaac794affae6e, km35d1c50fb7a4a10b5d077ea64b99be5, km59bd4bbc64946f18431932dbdb7dcc3, km46b66738bcc4a97bfaaed8694f81a16, km442df96fcb04e3f9858315cfc5dc078, km11b3fe87f0646e3b1cd3c93246e4f34);
    frmSettings.add(flxMainContainer, flxHeaderTitleContainer, km78db149dd394eb2adc0ca060cba037d);
};

function frmSettingsGlobals() {
    frmSettings = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSettings,
        "allowHorizontalBounce": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmSettings",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};