function addWidgetsfrmHome() {
    frmHome.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "onScrollEnd": AS_FlexScrollContainer_68af9d661fd04b088293b82a16118f29,
        "onScrollStart": AS_FlexScrollContainer_3d38c04723774d40bcf7a23f51833512,
        "pagingEnabled": false,
        "right": "0dp",
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "50dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var FlexContainer0d102dc2c865346 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "297dp",
        "id": "FlexContainer0d102dc2c865346",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2cNoBdr",
        "top": "-250dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    FlexContainer0d102dc2c865346.setDefaultUnit(kony.flex.DP);
    FlexContainer0d102dc2c865346.add();
    var flxMyStoreContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxMyStoreContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyStoreContainer.setDefaultUnit(kony.flex.DP);
    var flxMyStoreHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxMyStoreHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxMyStoreHeader.setDefaultUnit(kony.flex.DP);
    var flxChooseAStore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxChooseAStore",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxChooseAStore.setDefaultUnit(kony.flex.DP);
    var btnChooseStore = new kony.ui.Button({
        "focusSkin": "sknBtnTrans32pxWhiteFocus",
        "height": "100%",
        "id": "btnChooseStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTrans32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmHome.btnChooseStore"),
        "top": "0%",
        "width": "100%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblChevronRight = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "lblChevronRight",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblIcon32pxWhite",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "30dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [1, 0, 2, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxChooseAStore.add(btnChooseStore, lblChevronRight);
    var flxStoreName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreName",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxStoreName.setDefaultUnit(kony.flex.DP);
    var lblChevron = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblIcon32pxWhite",
        "text": "D",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [2, 0, 2, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStoreName = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblStoreName",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamRegular24pxWhite",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "82%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnMyStore = new kony.ui.Button({
        "focusSkin": "sknBtnTrans32pxWhiteFocus",
        "height": "100%",
        "id": "btnMyStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTrans32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.btnMyStore"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 5],
        "paddingInPixel": false
    }, {});
    flxStoreName.add(lblChevron, lblStoreName, btnMyStore);
    flxMyStoreHeader.add(flxChooseAStore, flxStoreName);
    var flxStoreInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "163dp",
        "id": "flxStoreInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray2R2",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreInfo.setDefaultUnit(kony.flex.DP);
    var flxStoreData = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreData",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 3
    }, {}, {});
    flxStoreData.setDefaultUnit(kony.flex.DP);
    var flxStoreDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flxStoreDetails.setDefaultUnit(kony.flex.DP);
    var lblAdressStore = new kony.ui.Label({
        "id": "lblAdressStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "snnLblGothamLight24pxBlack",
        "textStyle": {
            "lineSpacing": 7,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "98%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAdressStore1 = new kony.ui.Label({
        "id": "lblAdressStore1",
        "isVisible": true,
        "left": "0dp",
        "skin": "snnLblGothamLight24pxBlack",
        "textStyle": {
            "lineSpacing": 7,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "98%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCountryAndFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxCountryAndFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCountryAndFlag.setDefaultUnit(kony.flex.DP);
    var flxcountryFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxcountryFlag",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxcountryFlag.setDefaultUnit(kony.flex.DP);
    var lblCountry = new kony.ui.Label({
        "id": "lblCountry",
        "isVisible": true,
        "left": "3dp",
        "skin": "snnLblGothamLight24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.common.address.country"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgCountryFlag = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgCountryFlag",
        "isVisible": true,
        "left": "5dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "15dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxcountryFlag.add(lblCountry, imgCountryFlag);
    flxCountryAndFlag.add(flxcountryFlag);
    var lblOPenUntil = new kony.ui.Label({
        "id": "lblOPenUntil",
        "isVisible": false,
        "left": "0dp",
        "skin": "snnLblGothamBook24pxBlack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxContactAndFindStore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContactAndFindStore",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContactAndFindStore.setDefaultUnit(kony.flex.DP);
    var lblContactNo = new kony.ui.Label({
        "id": "lblContactNo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxBlue",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxFindStores = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxFindStores",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxFindStores.setDefaultUnit(kony.flex.DP);
    var lblFindStores = new kony.ui.Label({
        "id": "lblFindStores",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnFindStores"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgFindStores = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgFindStores",
        "isVisible": true,
        "right": "110dp",
        "skin": "slImage",
        "src": "map_pin_red.png",
        "top": "0dp",
        "width": "20dp",
        "zIndex": 2
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnChangeStore = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlue",
        "id": "btnChangeStore",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnFindStores"),
        "top": "0dp",
        "width": "105dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxFindStores.add(lblFindStores, imgFindStores, btnChangeStore);
    flxContactAndFindStore.add(lblContactNo, flxFindStores);
    flxStoreDetails.add(lblAdressStore, lblAdressStore1, flxCountryAndFlag, lblOPenUntil, flxContactAndFindStore);
    var flxGapPhone = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxGapPhone",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGapPhone.setDefaultUnit(kony.flex.DP);
    flxGapPhone.add();
    var flxStoreMapAndInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxStoreMapAndInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreMapAndInfo.setDefaultUnit(kony.flex.DP);
    var flxDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "sknFlexBgE0E0E0",
        "top": "0dp",
        "width": "1dp",
        "zIndex": 2
    }, {}, {});
    flxDivider.setDefaultUnit(kony.flex.DP);
    flxDivider.add();
    var btnStoreMap = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxWhiteFoc",
        "height": "100%",
        "id": "btnStoreMap",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnRed24pxMedFoc",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnStoreInfo = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxWhiteFoc",
        "height": "100%",
        "id": "btnStoreInfo",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnRed24pxMedFoc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmHome.btnStoreInfo"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStoreMapAndInfo.add(flxDivider, btnStoreMap, btnStoreInfo);
    flxStoreData.add(flxStoreDetails, flxGapPhone, flxStoreMapAndInfo);
    flxStoreInfo.add(flxStoreData);
    flxMyStoreContainer.add(flxMyStoreHeader, flxStoreInfo);
    var flxStoreModeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25%",
        "id": "flxStoreModeContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreModeContainer.setDefaultUnit(kony.flex.DP);
    var flxStoreModeInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreModeInfo",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxStoreModeDarkRedBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreModeInfo.setDefaultUnit(kony.flex.DP);
    var lblYouAreShoppingAt = new kony.ui.Label({
        "centerX": "50%",
        "height": "20%",
        "id": "lblYouAreShoppingAt",
        "isVisible": true,
        "skin": "sknLblGothamMedium10ptWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.YouAreShoppingAt"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var labelStoreName = new kony.ui.Label({
        "centerX": "50%",
        "height": "20%",
        "id": "labelStoreName",
        "isVisible": true,
        "skin": "sknLblGothamMedium13ptWhite",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxRSTabs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60%",
        "id": "flxRSTabs",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2cNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRSTabs.setDefaultUnit(kony.flex.DP);
    var flxRewards = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "flxRewards",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "25%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxRewards.setDefaultUnit(kony.flex.DP);
    var btnRewards = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnStoreModeRewards",
        "height": "65%",
        "id": "btnRewards",
        "isVisible": true,
        "skin": "sknBtnStoreModeRewards",
        "text": "R",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblRewards = new kony.ui.Label({
        "centerX": "50%",
        "height": "35%",
        "id": "lblRewards",
        "isVisible": true,
        "skin": "sknLblGothamMedium10ptWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmHomeStoreMode.Rewards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRewards.add(btnRewards, lblRewards);
    var flxStoreMap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "flxStoreMap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "right": "25%",
        "skin": "slFbox",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxStoreMap.setDefaultUnit(kony.flex.DP);
    var buttonStoreMap = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnStoreModeStoreMap",
        "height": "65%",
        "id": "buttonStoreMap",
        "isVisible": true,
        "skin": "sknBtnStoreModeStoreMap",
        "text": "e",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStoreMap = new kony.ui.Label({
        "centerX": "50%",
        "height": "35%",
        "id": "lblStoreMap",
        "isVisible": true,
        "skin": "sknLblGothamMedium10ptWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.storeMap"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStoreMap.add(buttonStoreMap, lblStoreMap);
    flxRSTabs.add(flxRewards, flxStoreMap);
    flxStoreModeInfo.add(lblYouAreShoppingAt, labelStoreName, flxRSTabs);
    flxStoreModeContainer.add(flxStoreModeInfo);
    var flexCarouselContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": false,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": false,
        "id": "flexCarouselContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "pagingEnabled": true,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "top": "10dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flexCarouselContainer.setDefaultUnit(kony.flex.DP);
    flexCarouselContainer.add();
    var flexCarouselIndicatorContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "4dp",
        "id": "flexCarouselIndicatorContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100dp",
        "zIndex": 1
    }, {}, {});
    flexCarouselIndicatorContainer.setDefaultUnit(kony.flex.DP);
    flexCarouselIndicatorContainer.add();
    var fxlContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "fxlContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    fxlContainer.setDefaultUnit(kony.flex.DP);
    fxlContainer.add();
    flxMainContainer.add(FlexContainer0d102dc2c865346, flxMyStoreContainer, flxStoreModeContainer, flexCarouselContainer, flexCarouselIndicatorContainer, fxlContainer);
    var kmf471a922e5b4f5ab4e9d0abe432b5a3 = new kony.ui.FlexScrollContainer({
        "isMaster": true,
        "id": "flxSearchResultsContainer",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    kmf471a922e5b4f5ab4e9d0abe432b5a3.setDefaultUnit(kony.flex.DP);
    var km43b1238e143432b989256ebe6d62893 = new kony.ui.FlexContainer({
        "id": "flxTopSpacerDNDSearch",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "97dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km43b1238e143432b989256ebe6d62893.setDefaultUnit(kony.flex.DP);
    km43b1238e143432b989256ebe6d62893.add();
    var kmf528ea3e7b04189811975101a8b5c44 = new kony.ui.SegmentedUI2({
        "id": "segSearchOffers",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km6061eb7be6e4d6c86f05779d7eccfa6 = new kony.ui.SegmentedUI2({
        "id": "segSearchResults",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf471a922e5b4f5ab4e9d0abe432b5a3.add(km43b1238e143432b989256ebe6d62893, kmf528ea3e7b04189811975101a8b5c44, km6061eb7be6e4d6c86f05779d7eccfa6);
    var kmd4d81543a6a45b5a81a54ff5e61c0fa = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    kmd4d81543a6a45b5a81a54ff5e61c0fa.setDefaultUnit(kony.flex.DP);
    var km88875600e61440cafa31d5b756cfc9e = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km88875600e61440cafa31d5b756cfc9e.setDefaultUnit(kony.flex.DP);
    km88875600e61440cafa31d5b756cfc9e.add();
    var km46e00b6fd764616b86eedc2fe4f3745 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km46e00b6fd764616b86eedc2fe4f3745.setDefaultUnit(kony.flex.DP);
    var kmf4d6abfb27745de9298973d748619cc = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km774a000bdc04600a8af59f82b7f739a = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    var km3cbb98888af4c8c955653d7ef8ecfb5 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmbae84aae4a044c9b317530e64150a2e = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    kmbae84aae4a044c9b317530e64150a2e.setDefaultUnit(kony.flex.DP);
    kmbae84aae4a044c9b317530e64150a2e.add();
    var km9308e5d7b5549f4b6ca252f201ed3fc = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km46e00b6fd764616b86eedc2fe4f3745.add(kmf4d6abfb27745de9298973d748619cc, km774a000bdc04600a8af59f82b7f739a, km3cbb98888af4c8c955653d7ef8ecfb5, kmbae84aae4a044c9b317530e64150a2e, km9308e5d7b5549f4b6ca252f201ed3fc);
    var km79ad886b6344d9589eb27aa18ff4580 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km79ad886b6344d9589eb27aa18ff4580.setDefaultUnit(kony.flex.DP);
    var kmdce08446e374c69aa67e875379213d4 = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmdce08446e374c69aa67e875379213d4.setDefaultUnit(kony.flex.DP);
    var km9aea025aa1b4b40ba2e9075b4f76931 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km9aea025aa1b4b40ba2e9075b4f76931.setDefaultUnit(kony.flex.DP);
    var km54344addee045fb927db7cc014a9977 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km9aea025aa1b4b40ba2e9075b4f76931.add(km54344addee045fb927db7cc014a9977);
    var kmb1943d0be68446fa94113398227dfe4 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    kmb1943d0be68446fa94113398227dfe4.setDefaultUnit(kony.flex.DP);
    var km7ee937211ef4b96be66e29658a56543 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb1943d0be68446fa94113398227dfe4.add(km7ee937211ef4b96be66e29658a56543);
    var kme3ed91ff2754ba289c1ad1fc8e7419e = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kme3ed91ff2754ba289c1ad1fc8e7419e.setDefaultUnit(kony.flex.DP);
    var kmd14f9260e0a41a48fe7feba9fe14cb5 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km0245f80a6c640a9a40f325ae19924d6 = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kme3ed91ff2754ba289c1ad1fc8e7419e.add(kmd14f9260e0a41a48fe7feba9fe14cb5, km0245f80a6c640a9a40f325ae19924d6);
    var kmbf3dc0717e64e45be47ad620c34983b = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmdce08446e374c69aa67e875379213d4.add(km9aea025aa1b4b40ba2e9075b4f76931, kmb1943d0be68446fa94113398227dfe4, kme3ed91ff2754ba289c1ad1fc8e7419e, kmbf3dc0717e64e45be47ad620c34983b);
    var km6a87e5bb1d8431c8ab09ebdfcc7f949 = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km6a87e5bb1d8431c8ab09ebdfcc7f949.setDefaultUnit(kony.flex.DP);
    km6a87e5bb1d8431c8ab09ebdfcc7f949.add();
    km79ad886b6344d9589eb27aa18ff4580.add(kmdce08446e374c69aa67e875379213d4, km6a87e5bb1d8431c8ab09ebdfcc7f949);
    var km7672c4137b4499598cddaccd0ef8bdd = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km7672c4137b4499598cddaccd0ef8bdd.setDefaultUnit(kony.flex.DP);
    var km6133fc39c344853937c552b35710421 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km6133fc39c344853937c552b35710421.setDefaultUnit(kony.flex.DP);
    var km8f70d4724be43e3aba78c14cafd8abf = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km8f70d4724be43e3aba78c14cafd8abf.setDefaultUnit(kony.flex.DP);
    var kma8d3dd4e6cf4da7949ff232927e6023 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmf733c0666b441aca811b3a9ccdc2bbe = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km8f70d4724be43e3aba78c14cafd8abf.add(kma8d3dd4e6cf4da7949ff232927e6023, kmf733c0666b441aca811b3a9ccdc2bbe);
    var kmdf6a2cbd04246b4805bf268a033b90e = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmdf6a2cbd04246b4805bf268a033b90e.setDefaultUnit(kony.flex.DP);
    var kmb348f27423344c0adaab2c6bf105abf = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmb348f27423344c0adaab2c6bf105abf.setDefaultUnit(kony.flex.DP);
    var kma6545a2844f47dc9768c20a127b1ad7 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3e76838b7ef4b339f7951a727530844 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km268c2d98ff94936b0053bd85a1abfc5 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmb0ea5970d644d928a86742b49830fa9 = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmfdf3391cdf741acacd7a1ab7a347372 = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kme2ed884b0b64e8584747629da569dbf = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb348f27423344c0adaab2c6bf105abf.add(kma6545a2844f47dc9768c20a127b1ad7, km3e76838b7ef4b339f7951a727530844, km268c2d98ff94936b0053bd85a1abfc5, kmb0ea5970d644d928a86742b49830fa9, kmfdf3391cdf741acacd7a1ab7a347372, kme2ed884b0b64e8584747629da569dbf);
    var km22816edb84e45fbb0ca09e0ab1025d9 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km22816edb84e45fbb0ca09e0ab1025d9.setDefaultUnit(kony.flex.DP);
    var km73fd8fc6214468d8909e2f0444844a5 = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km8b1e337f50249d69adf9f1ad22ce2c7 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd9ed7e2273e49a09fca2e12c0676ed0 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km22816edb84e45fbb0ca09e0ab1025d9.add(km73fd8fc6214468d8909e2f0444844a5, km8b1e337f50249d69adf9f1ad22ce2c7, kmd9ed7e2273e49a09fca2e12c0676ed0);
    kmdf6a2cbd04246b4805bf268a033b90e.add(kmb348f27423344c0adaab2c6bf105abf, km22816edb84e45fbb0ca09e0ab1025d9);
    var km316132fab24438da24f41c8924747c1 = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmf5a98a6f08e4974adfd3279277e2d44 = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km6133fc39c344853937c552b35710421.add(km8f70d4724be43e3aba78c14cafd8abf, kmdf6a2cbd04246b4805bf268a033b90e, km316132fab24438da24f41c8924747c1, kmf5a98a6f08e4974adfd3279277e2d44);
    km7672c4137b4499598cddaccd0ef8bdd.add(km6133fc39c344853937c552b35710421);
    kmd4d81543a6a45b5a81a54ff5e61c0fa.add(km88875600e61440cafa31d5b756cfc9e, km46e00b6fd764616b86eedc2fe4f3745, km79ad886b6344d9589eb27aa18ff4580, km7672c4137b4499598cddaccd0ef8bdd);
    var flexCardAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "190dp",
        "id": "flexCardAnimate",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "50dp",
        "width": "94%",
        "zIndex": 20
    }, {}, {});
    flexCardAnimate.setDefaultUnit(kony.flex.DP);
    var flexCardAnimateMover = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexCardAnimateMover",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flexCardAnimateMover.setDefaultUnit(kony.flex.DP);
    var flexCardAnimateBG = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "100%",
        "id": "flexCardAnimateBG",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flexCardAnimateBG.setDefaultUnit(kony.flex.DP);
    var flexCardAnimate1b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "50%",
        "id": "flexCardAnimate1b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flexCardAnimate1b.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer07d65da5db45745 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 10,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer07d65da5db45745",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexEventTab",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer07d65da5db45745.setDefaultUnit(kony.flex.DP);
    var lblTileText = new kony.ui.Label({
        "height": "100%",
        "id": "lblTileText",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel07b2097efda3042 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel07b2097efda3042",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblIcon24px606060",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer07d65da5db45745.add(lblTileText, CopyLabel07b2097efda3042);
    var flexShadowBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexShadowBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "flexShadow1",
        "top": "0dp",
        "width": "100%",
        "zIndex": 3
    }, {}, {});
    flexShadowBottom.setDefaultUnit(kony.flex.DP);
    flexShadowBottom.add();
    flexCardAnimate1b.add(CopyFlexContainer07d65da5db45745, flexShadowBottom);
    flexCardAnimateBG.add(flexCardAnimate1b);
    var flexCardAnimate1a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexCardAnimate1a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "100%",
        "zIndex": 900
    }, {}, {});
    flexCardAnimate1a.setDefaultUnit(kony.flex.DP);
    var flexShadowTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexShadowTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "flexShadow2",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2000
    }, {}, {});
    flexShadowTop.setDefaultUnit(kony.flex.DP);
    flexShadowTop.add();
    flexCardAnimate1a.add(flexShadowTop);
    flexCardAnimateMover.add(flexCardAnimateBG, flexCardAnimate1a);
    flexCardAnimate.add(flexCardAnimateMover);
    var maroon5Buffer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "maroon5Buffer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "97dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    maroon5Buffer.setDefaultUnit(kony.flex.DP);
    maroon5Buffer.add();
    var km88ce13ca80647e280c547f303997eec = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km88ce13ca80647e280c547f303997eec.setDefaultUnit(kony.flex.DP);
    var km1b397872819423abe96b692101b93b8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    km1b397872819423abe96b692101b93b8.setDefaultUnit(kony.flex.DP);
    var km2a8d4b701f742f08815a87a25e395b0 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2a8d4b701f742f08815a87a25e395b0.setDefaultUnit(kony.flex.DP);
    var kmfbc7e8aacc14cfa8685c5d647ee94ee = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km39d5830605940aa91cb8331772c2bee = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km39d5830605940aa91cb8331772c2bee.setDefaultUnit(kony.flex.DP);
    var kmfe5ba035e7144249e01384fd2012a8a = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km053dee563ee4a918bef1761b21d90d7 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km83ab5517d6d4d91b458a0b78950b8e5 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km39d5830605940aa91cb8331772c2bee.add(kmfe5ba035e7144249e01384fd2012a8a, km053dee563ee4a918bef1761b21d90d7, km83ab5517d6d4d91b458a0b78950b8e5);
    var kme24bacfc861450e892c188b1b166cc5 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmc2ead27a9a648ef9481c136b0ce12d2 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km2a8d4b701f742f08815a87a25e395b0.add(kmfbc7e8aacc14cfa8685c5d647ee94ee, km39d5830605940aa91cb8331772c2bee, kme24bacfc861450e892c188b1b166cc5, kmc2ead27a9a648ef9481c136b0ce12d2);
    var km24ee6561caf4eb2b9be1e168ae165ce = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    km24ee6561caf4eb2b9be1e168ae165ce.setDefaultUnit(kony.flex.DP);
    var kmf644e0539e442bf83f3a2b5d1af3573 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf644e0539e442bf83f3a2b5d1af3573.setDefaultUnit(kony.flex.DP);
    var km48bb50053db42d9976c59f809f7cf3b = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf644e0539e442bf83f3a2b5d1af3573.add(km48bb50053db42d9976c59f809f7cf3b);
    km24ee6561caf4eb2b9be1e168ae165ce.add(kmf644e0539e442bf83f3a2b5d1af3573);
    var km4c40b64f6e543bcb9f04d06659e3660 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km1b397872819423abe96b692101b93b8.add(km2a8d4b701f742f08815a87a25e395b0, km24ee6561caf4eb2b9be1e168ae165ce, km4c40b64f6e543bcb9f04d06659e3660);
    km88ce13ca80647e280c547f303997eec.add(km1b397872819423abe96b692101b93b8);
    var flxWhatsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100.04%",
        "id": "flxWhatsNew",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 12
    }, {}, {});
    flxWhatsNew.setDefaultUnit(kony.flex.DP);
    var flxNewFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxNewFeature",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "84.00%",
        "zIndex": 12
    }, {}, {});
    flxNewFeature.setDefaultUnit(kony.flex.DP);
    var flxHeading = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxHeading",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "100%",
        "zIndex": 12
    }, {}, {});
    flxHeading.setDefaultUnit(kony.flex.DP);
    var lblHeading = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblHeading",
        "isVisible": true,
        "left": "5%",
        "right": "5%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18.phone.whatsNew"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 12
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSpace = new kony.ui.Label({
        "height": "20dp",
        "id": "lblSpace",
        "isVisible": true,
        "left": "5%",
        "right": "5%",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 12
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeading.add(lblHeading, lblSpace);
    var flxSegData = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxSegData",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 12
    }, {}, {});
    flxSegData.setDefaultUnit(kony.flex.DP);
    var segNewFeature = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSpace": "",
            "lblTitle": ""
        }, {
            "lblSpace": "",
            "lblTitle": ""
        }, {
            "lblSpace": "",
            "lblTitle": ""
        }],
        "groupCells": false,
        "id": "segNewFeature",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": FlxSegmentData,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlxSegmentData": "FlxSegmentData",
            "lblSpace": "lblSpace",
            "lblTitle": "lblTitle"
        },
        "width": "100%",
        "zIndex": 12
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSegData.add(segNewFeature);
    var flxGotIt = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": "20dp",
        "clipBounds": true,
        "id": "flxGotIt",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 12
    }, {}, {});
    flxGotIt.setDefaultUnit(kony.flex.DP);
    var btnGotIt = new kony.ui.Button({
        "focusSkin": "sknBtnRedBg5pxBorderRadius",
        "height": "50dp",
        "id": "btnGotIt",
        "isVisible": true,
        "left": "5%",
        "right": "5%",
        "skin": "sknBtnRedBg5pxBorderRadius",
        "text": kony.i18n.getLocalizedString("i18n.phone.imageOverlay.popUpButtonText"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxGotIt.add(btnGotIt);
    flxNewFeature.add(flxHeading, flxSegData, flxGotIt);
    flxWhatsNew.add(flxNewFeature);
    var km41fb8ca9148454dbc23ba06756410c1 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km41fb8ca9148454dbc23ba06756410c1.setDefaultUnit(kony.flex.DP);
    var km09b3e2b5bb54fce920f7a4f7b7b53a8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    km09b3e2b5bb54fce920f7a4f7b7b53a8.setDefaultUnit(kony.flex.DP);
    var km8887eea875843f489632177eb719ff3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km8887eea875843f489632177eb719ff3.setDefaultUnit(kony.flex.DP);
    var km260593b08a247ec844c23a3316e0ab0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    km260593b08a247ec844c23a3316e0ab0.setDefaultUnit(kony.flex.DP);
    km260593b08a247ec844c23a3316e0ab0.add();
    var km276590f6bfb440eae3a5ae94c4e09cb = new kony.ui.Image2({
        "height": "40dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 10,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km69634d2d0054f219000448f53e38ea2 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "50dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 10,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km243c7ff47bb45d382f87b58d4578300 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km243c7ff47bb45d382f87b58d4578300.setDefaultUnit(kony.flex.DP);
    var kma9b462d94c54bbc91cc3e1519cd536c = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kme89a40bc27f4675a717123e9a38dc47 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km243c7ff47bb45d382f87b58d4578300.add(kma9b462d94c54bbc91cc3e1519cd536c, kme89a40bc27f4675a717123e9a38dc47);
    var km73e49ecebd2451aa6e58fb8eae4320c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km73e49ecebd2451aa6e58fb8eae4320c.setDefaultUnit(kony.flex.DP);
    var km0b9dc21a9ed407cb66d98a77c0ee764 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km701762613ac4d76a0ade12d8e24f0cb = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    km73e49ecebd2451aa6e58fb8eae4320c.add(km0b9dc21a9ed407cb66d98a77c0ee764, km701762613ac4d76a0ade12d8e24f0cb);
    var km3e346c015334d27873c44dc456d357e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3e346c015334d27873c44dc456d357e.setDefaultUnit(kony.flex.DP);
    var km6683f15f3314d0d8a6e843b837572ba = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km2410a8e465647a18a57ebeb65f91ac3 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km3e346c015334d27873c44dc456d357e.add(km6683f15f3314d0d8a6e843b837572ba, km2410a8e465647a18a57ebeb65f91ac3);
    var km403f49e483741778d02e0ee00f433fd = new kony.ui.Button({
        "centerY": "15dp",
        "height": "13.93%",
        "id": "btnClearVoiceSearch",
        "right": "9dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km8887eea875843f489632177eb719ff3.add(km260593b08a247ec844c23a3316e0ab0, km276590f6bfb440eae3a5ae94c4e09cb, km69634d2d0054f219000448f53e38ea2, km243c7ff47bb45d382f87b58d4578300, km73e49ecebd2451aa6e58fb8eae4320c, km3e346c015334d27873c44dc456d357e, km403f49e483741778d02e0ee00f433fd);
    km09b3e2b5bb54fce920f7a4f7b7b53a8.add(km8887eea875843f489632177eb719ff3);
    km41fb8ca9148454dbc23ba06756410c1.add(km09b3e2b5bb54fce920f7a4f7b7b53a8);
    var km189378f277e40bc8eb93123919ee069 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isMaster": true,
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km189378f277e40bc8eb93123919ee069.setDefaultUnit(kony.flex.DP);
    var kmc864d6f23b547a7849933da82b88b36 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeader",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2cNoBdr"
    }, {}, {});
    kmc864d6f23b547a7849933da82b88b36.setDefaultUnit(kony.flex.DP);
    var km7f5eb1e6093431a83efedaa3f26ec60 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "imgLogo",
        "src": "logo_white.png",
        "width": "50%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmb5ff43b035f462da3b0eb143b40f20a = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km416dc0b200a488ca3c9e3ce94a7c6df = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "0.00%",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmb426f7c17f4495786b87c6504486e62 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb426f7c17f4495786b87c6504486e62.setDefaultUnit(kony.flex.DP);
    var km0468a6beaa643e9bdab3cc10a71f92e = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "10%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": false,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km8341cb3cdf740ae90fe2aff2bfdc599 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "20%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb426f7c17f4495786b87c6504486e62.add(km0468a6beaa643e9bdab3cc10a71f92e, km8341cb3cdf740ae90fe2aff2bfdc599);
    kmc864d6f23b547a7849933da82b88b36.add(km7f5eb1e6093431a83efedaa3f26ec60, kmb5ff43b035f462da3b0eb143b40f20a, km416dc0b200a488ca3c9e3ce94a7c6df, kmb426f7c17f4495786b87c6504486e62);
    var kmfc91b1585974b559e9760cdba761f5f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "47dp",
        "id": "flxSearchContainer",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "right": "0%",
        "top": "50dp",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmfc91b1585974b559e9760cdba761f5f.setDefaultUnit(kony.flex.DP);
    var km842de650b6a4c7b86758254aa399912 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "47dp",
        "id": "flexToShrink",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "top": "0dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km842de650b6a4c7b86758254aa399912.setDefaultUnit(kony.flex.DP);
    var km66812eec3bd4d0a981d48cf843ab5c9 = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "right": "10dp",
        "top": "1dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km66812eec3bd4d0a981d48cf843ab5c9.setDefaultUnit(kony.flex.DP);
    var km3efedeb9cfd46a88228bbdcb7cb12b4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km3efedeb9cfd46a88228bbdcb7cb12b4.setDefaultUnit(kony.flex.DP);
    var kme5d333c3e2f4272a3fda9d5475d578b = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km3efedeb9cfd46a88228bbdcb7cb12b4.add(kme5d333c3e2f4272a3fda9d5475d578b);
    var kmc8ace2ea4af47d2b8bb943deae19a1a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "right": "30%",
        "top": "0dp",
        "width": "58%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmc8ace2ea4af47d2b8bb943deae19a1a.setDefaultUnit(kony.flex.DP);
    var km2126b8dec61487a923ebc879d267b82 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "9%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "91%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km86bb73e947349df9b33f822afa791ea = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClearSearch",
        "right": "0dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmc8ace2ea4af47d2b8bb943deae19a1a.add(km2126b8dec61487a923ebc879d267b82, km86bb73e947349df9b33f822afa791ea);
    var km69c8ce09b43426eabc5d8992f927f35 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "13.50%",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km69c8ce09b43426eabc5d8992f927f35.setDefaultUnit(kony.flex.DP);
    var km00ed9c0e93c4840b5a415d5ad86a86e = new kony.ui.Camera({
        "centerX": "52%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "10%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": false,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    km69c8ce09b43426eabc5d8992f927f35.add(km00ed9c0e93c4840b5a415d5ad86a86e);
    var km1d3a1c5b5ca474c8adb0d8d25494e5b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km1d3a1c5b5ca474c8adb0d8d25494e5b.setDefaultUnit(kony.flex.DP);
    var km122c14d798f40f28161d89912243562 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFoc",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km1d3a1c5b5ca474c8adb0d8d25494e5b.add(km122c14d798f40f28161d89912243562);
    var km425470912cd4168be04df24de9ec8c1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCameraImageSearch",
        "right": "13.50%",
        "top": "0dp",
        "width": "12.50%",
        "zIndex": 2,
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "isVisible": false,
        "skin": "sknBtnPopupCloseTransparentBg"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km9ead57a4ed1446c990e052e1ba6da75 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "10%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9ead57a4ed1446c990e052e1ba6da75.setDefaultUnit(kony.flex.DP);
    var km87f4873ac9447778a38ce6ef1f1b7ee = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "width": "10dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km9ead57a4ed1446c990e052e1ba6da75.add(km87f4873ac9447778a38ce6ef1f1b7ee);
    km66812eec3bd4d0a981d48cf843ab5c9.add(km3efedeb9cfd46a88228bbdcb7cb12b4, kmc8ace2ea4af47d2b8bb943deae19a1a, km69c8ce09b43426eabc5d8992f927f35, km1d3a1c5b5ca474c8adb0d8d25494e5b, km425470912cd4168be04df24de9ec8c1, km9ead57a4ed1446c990e052e1ba6da75);
    km842de650b6a4c7b86758254aa399912.add(km66812eec3bd4d0a981d48cf843ab5c9);
    kmfc91b1585974b559e9760cdba761f5f.add(km842de650b6a4c7b86758254aa399912);
    var km2e457d335ae46f997a4efa47a362a77 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "3dp",
        "id": "flxSearchShadow",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "100dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexShadow"
    }, {}, {});
    km2e457d335ae46f997a4efa47a362a77.setDefaultUnit(kony.flex.DP);
    km2e457d335ae46f997a4efa47a362a77.add();
    km189378f277e40bc8eb93123919ee069.add(kmc864d6f23b547a7849933da82b88b36, kmfc91b1585974b559e9760cdba761f5f, km2e457d335ae46f997a4efa47a362a77);
    var km75b63ccacef49818107dbe200302104 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km75b63ccacef49818107dbe200302104.setDefaultUnit(kony.flex.DP);
    var km572299aa8284842b32dcb75d9969adc = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km572299aa8284842b32dcb75d9969adc.setDefaultUnit(kony.flex.DP);
    var kmf8c6d2e11734ebbadb56afe2e987532 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5f9f116d14f44e2b06c6f10c80fcce1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km572299aa8284842b32dcb75d9969adc.add(kmf8c6d2e11734ebbadb56afe2e987532, km5f9f116d14f44e2b06c6f10c80fcce1);
    var km65e04da2b1048b0a8e3b75214febf77 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km65e04da2b1048b0a8e3b75214febf77.setDefaultUnit(kony.flex.DP);
    var km56aa2df2a464c15af862a561154f3ac = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km469776abd074213bb7704c0d9fd556e = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var kma6dc0fae64e42aca23aa08a518531e4 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km65e04da2b1048b0a8e3b75214febf77.add(km56aa2df2a464c15af862a561154f3ac, km469776abd074213bb7704c0d9fd556e, kma6dc0fae64e42aca23aa08a518531e4);
    var km615db89f0b2499da8af1e7db6135c3a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km615db89f0b2499da8af1e7db6135c3a.setDefaultUnit(kony.flex.DP);
    var kmba2c8dd9ec04b55881ead4447427b42 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kme9fe2b1a436436795eac090ebc4a80f = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km615db89f0b2499da8af1e7db6135c3a.add(kmba2c8dd9ec04b55881ead4447427b42, kme9fe2b1a436436795eac090ebc4a80f);
    var km4b67bd209e845c6acd236a45684ce4b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4b67bd209e845c6acd236a45684ce4b.setDefaultUnit(kony.flex.DP);
    var kmd6e0650e7964aaa9d132b54878dec0c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km6c41b87b8834e69a50f289b475f0e53 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km4b67bd209e845c6acd236a45684ce4b.add(kmd6e0650e7964aaa9d132b54878dec0c, km6c41b87b8834e69a50f289b475f0e53);
    var km7e0941567444a6aa7c946ce99f691da = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7e0941567444a6aa7c946ce99f691da.setDefaultUnit(kony.flex.DP);
    var kmbe9c4f86f6b44009b86af89bc3e2ee9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbcc9611b96c45a6be4236ceb97fec58 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km7e0941567444a6aa7c946ce99f691da.add(kmbe9c4f86f6b44009b86af89bc3e2ee9, kmbcc9611b96c45a6be4236ceb97fec58);
    var km0b92aa82dd04914a847c46679cbec09 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0b92aa82dd04914a847c46679cbec09.setDefaultUnit(kony.flex.DP);
    var kmd641b7defa04a13a76ed8eed77f36f4 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kme3ab3c06ffa49f4aa7cc637737b78b1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km0b92aa82dd04914a847c46679cbec09.add(kmd641b7defa04a13a76ed8eed77f36f4, kme3ab3c06ffa49f4aa7cc637737b78b1);
    var kme1db3ca47f14ea395c49e193a54ca91 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme1db3ca47f14ea395c49e193a54ca91.setDefaultUnit(kony.flex.DP);
    var kme716d82c0234ae3a032ec9c60900c78 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd6d126668814e7983ee55e3c8c3f9ff = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kme1db3ca47f14ea395c49e193a54ca91.add(kme716d82c0234ae3a032ec9c60900c78, kmd6d126668814e7983ee55e3c8c3f9ff);
    var kmee83f0531eb4c7896c43d41cf598f44 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmee83f0531eb4c7896c43d41cf598f44.setDefaultUnit(kony.flex.DP);
    var km14c12a9eb8d4853b85b0e1ad41fca81 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km317628014f04aee91b097ffa18ce572 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmee83f0531eb4c7896c43d41cf598f44.add(km14c12a9eb8d4853b85b0e1ad41fca81, km317628014f04aee91b097ffa18ce572);
    km75b63ccacef49818107dbe200302104.add(km572299aa8284842b32dcb75d9969adc, km65e04da2b1048b0a8e3b75214febf77, km615db89f0b2499da8af1e7db6135c3a, km4b67bd209e845c6acd236a45684ce4b, km7e0941567444a6aa7c946ce99f691da, km0b92aa82dd04914a847c46679cbec09, kme1db3ca47f14ea395c49e193a54ca91, kmee83f0531eb4c7896c43d41cf598f44);
    frmHome.add(flxMainContainer, kmf471a922e5b4f5ab4e9d0abe432b5a3, kmd4d81543a6a45b5a81a54ff5e61c0fa, flexCardAnimate, maroon5Buffer, km88ce13ca80647e280c547f303997eec, flxWhatsNew, km41fb8ca9148454dbc23ba06756410c1, km189378f277e40bc8eb93123919ee069, km75b63ccacef49818107dbe200302104);
};

function frmHomeGlobals() {
    frmHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmHome,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmHome",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_512158be75fa4b7daf683f7058cfd1be(eventobject);
        },
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};