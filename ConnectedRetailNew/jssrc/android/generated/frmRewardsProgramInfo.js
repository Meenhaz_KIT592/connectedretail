function addWidgetsfrmRewardsProgramInfo() {
    frmRewardsProgramInfo.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "9%",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxRewardsBanner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxRewardsBanner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxBgBlack",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRewardsBanner.setDefaultUnit(kony.flex.DP);
    var imgBanner = new kony.ui.Image2({
        "centerX": "50%",
        "height": "75dp",
        "id": "imgBanner",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "skin": "slImage",
        "src": "ic_rewardslogo.png",
        "top": "20dp",
        "width": "230dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var rtBannerTxt = new kony.ui.RichText({
        "centerX": "50%",
        "id": "rtBannerTxt",
        "isVisible": true,
        "skin": "sknRTGothamMedium24pxWhite",
        "text": "A <strong>FREE</strong> perks program<br>for all our makers. ",
        "top": "12dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0e78c81c9d33d44 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20dp",
        "id": "FlexContainer0e78c81c9d33d44",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0e78c81c9d33d44.setDefaultUnit(kony.flex.DP);
    FlexContainer0e78c81c9d33d44.add();
    flxRewardsBanner.add(imgBanner, rtBannerTxt, FlexContainer0e78c81c9d33d44);
    var FlexContainer06bdc793499af41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "FlexContainer06bdc793499af41",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    FlexContainer06bdc793499af41.setDefaultUnit(kony.flex.DP);
    var Image0e4ff7dc22df943 = new kony.ui.Image2({
        "height": "70dp",
        "id": "Image0e4ff7dc22df943",
        "imageWhenFailed": "picture.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_couponrewards.png",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0ebaf1a4b1fea49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer0ebaf1a4b1fea49",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "95dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer0ebaf1a4b1fea49.setDefaultUnit(kony.flex.DP);
    var FlexContainer0d609b1d81f1345 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer0d609b1d81f1345",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0d609b1d81f1345.setDefaultUnit(kony.flex.DP);
    var Label0a61c395176a840 = new kony.ui.Label({
        "id": "Label0a61c395176a840",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxRed",
        "text": "EXCLUSIVE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel083a53ad9992845 = new kony.ui.Label({
        "id": "CopyLabel083a53ad9992845",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "OFFERS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0d609b1d81f1345.add(Label0a61c395176a840, CopyLabel083a53ad9992845);
    var Label004b8fba8ea924e = new kony.ui.Label({
        "id": "Label004b8fba8ea924e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "We’ll deliver members-only offers straight to your inbox. Save on the décor and DIYs you love most!",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0ebaf1a4b1fea49.add(FlexContainer0d609b1d81f1345, Label004b8fba8ea924e);
    FlexContainer06bdc793499af41.add(Image0e4ff7dc22df943, FlexContainer0ebaf1a4b1fea49);
    var CopyFlexContainer039f8ce96f8e040 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer039f8ce96f8e040",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer039f8ce96f8e040.setDefaultUnit(kony.flex.DP);
    var CopyImage09b883faf693041 = new kony.ui.Image2({
        "height": "70dp",
        "id": "CopyImage09b883faf693041",
        "imageWhenFailed": "picture.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_rewardsmember.png",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyFlexContainer0289ff9f50c5e4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexContainer0289ff9f50c5e4e",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "95dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0289ff9f50c5e4e.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0f6073647edfb4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexContainer0f6073647edfb4e",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0f6073647edfb4e.setDefaultUnit(kony.flex.DP);
    var CopyLabel062c22a36d8c044 = new kony.ui.Label({
        "id": "CopyLabel062c22a36d8c044",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxRed",
        "text": "MEMBERS-ONLY",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0a16ceddfd33d48 = new kony.ui.Label({
        "id": "CopyLabel0a16ceddfd33d48",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "EVENTS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0f6073647edfb4e.add(CopyLabel062c22a36d8c044, CopyLabel0a16ceddfd33d48);
    var CopyLabel0851592fa5f1e40 = new kony.ui.Label({
        "id": "CopyLabel0851592fa5f1e40",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Meet, mingle and make with members like you. Enjoy special shopping hours and more!",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0289ff9f50c5e4e.add(CopyFlexContainer0f6073647edfb4e, CopyLabel0851592fa5f1e40);
    CopyFlexContainer039f8ce96f8e040.add(CopyImage09b883faf693041, CopyFlexContainer0289ff9f50c5e4e);
    var CopyFlexContainer0b62a419d550e45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer0b62a419d550e45",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0b62a419d550e45.setDefaultUnit(kony.flex.DP);
    var CopyImage06894411c85a14e = new kony.ui.Image2({
        "height": "70dp",
        "id": "CopyImage06894411c85a14e",
        "imageWhenFailed": "picture.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_rewardsreturns.png",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyFlexContainer01bd17a76811a49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexContainer01bd17a76811a49",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "95dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer01bd17a76811a49.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer07fe90db225fd4f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexContainer07fe90db225fd4f",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer07fe90db225fd4f.setDefaultUnit(kony.flex.DP);
    var CopyLabel0a8bc7fb24f564a = new kony.ui.Label({
        "id": "CopyLabel0a8bc7fb24f564a",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxRed",
        "text": "RECEIPT-FREE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0f53e02cfd45648 = new kony.ui.Label({
        "id": "CopyLabel0f53e02cfd45648",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "RETURNS ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer07fe90db225fd4f.add(CopyLabel0a8bc7fb24f564a, CopyLabel0f53e02cfd45648);
    var RichText063b60beaad2d44 = new kony.ui.RichText({
        "id": "RichText063b60beaad2d44",
        "isVisible": true,
        "left": "0dp",
        "linkSkin": "sknRTGothamRegular24pxBlue",
        "skin": "sknRTGothamRegular24pxDark",
        "text": "No more digging for receipts.\nSimply bring in returns, and\nwe’ll take care of the rest.\nSee our <a href=\"#\">Return Policy</a>.",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer01bd17a76811a49.add(CopyFlexContainer07fe90db225fd4f, RichText063b60beaad2d44);
    CopyFlexContainer0b62a419d550e45.add(CopyImage06894411c85a14e, CopyFlexContainer01bd17a76811a49);
    var CopyFlexContainer03c41487314fe41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer03c41487314fe41",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer03c41487314fe41.setDefaultUnit(kony.flex.DP);
    var CopyImage0e1efea1cb2894f = new kony.ui.Image2({
        "height": "70dp",
        "id": "CopyImage0e1efea1cb2894f",
        "imageWhenFailed": "picture.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_rewardsalerts.png",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyFlexContainer0193624b1b8ab48 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexContainer0193624b1b8ab48",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "95dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0193624b1b8ab48.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0387b1400145946 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexContainer0387b1400145946",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0387b1400145946.setDefaultUnit(kony.flex.DP);
    var CopyLabel00d384884066648 = new kony.ui.Label({
        "id": "CopyLabel00d384884066648",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "EARLY ALERTS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel02a1c857fdab14d = new kony.ui.Label({
        "id": "CopyLabel02a1c857fdab14d",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxRed",
        "text": "FOR SALES",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0387b1400145946.add(CopyLabel00d384884066648, CopyLabel02a1c857fdab14d);
    var CopyLabel0fa081501d3f647 = new kony.ui.Label({
        "id": "CopyLabel0fa081501d3f647",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Be the first to know about big sales and amazing deals.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0193624b1b8ab48.add(CopyFlexContainer0387b1400145946, CopyLabel0fa081501d3f647);
    CopyFlexContainer03c41487314fe41.add(CopyImage0e1efea1cb2894f, CopyFlexContainer0193624b1b8ab48);
    var segRewardsInfo = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "Label0a61c395176a840": "EXCLUSIVE",
            "imgLogo": "ic_couponrewards.png",
            "lblBody": "We’ll deliver members-only offers straight to your inbox. Save on the décor and DIYs you love most!",
            "lblHeader": "OFFERS",
            "rchTxtBody": "RichText"
        }, {
            "Label0a61c395176a840": "EXCLUSIVE",
            "imgLogo": "ic_couponrewards.png",
            "lblBody": "We’ll deliver members-only offers straight to your inbox. Save on the décor and DIYs you love most!",
            "lblHeader": "OFFERS",
            "rchTxtBody": "RichText"
        }, {
            "Label0a61c395176a840": "EXCLUSIVE",
            "imgLogo": "ic_couponrewards.png",
            "lblBody": "We’ll deliver members-only offers straight to your inbox. Save on the décor and DIYs you love most!",
            "lblHeader": "OFFERS",
            "rchTxtBody": "RichText"
        }],
        "groupCells": false,
        "id": "segRewardsInfo",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxMainRewardInfo,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlexContainer06bdc793499af41": "FlexContainer06bdc793499af41",
            "FlexContainer0d609b1d81f1345": "FlexContainer0d609b1d81f1345",
            "FlexContainer0ebaf1a4b1fea49": "FlexContainer0ebaf1a4b1fea49",
            "Label0a61c395176a840": "Label0a61c395176a840",
            "flxMainRewardInfo": "flxMainRewardInfo",
            "imgLogo": "imgLogo",
            "lblBody": "lblBody",
            "lblHeader": "lblHeader",
            "rchTxtBody": "rchTxtBody"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnJoinRewards = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnJoinRewards",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRewardsProgramInfo.joinMichaelRewards"),
        "top": "25dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnFAQ = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnFAQ",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRewardsProgramInfo.faq"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnTermsandConditions = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnTermsandConditions",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRewardsProgramInfo.termsConditions"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyflxShare0586afa16084f40 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyflxShare0586afa16084f40",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "15dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    CopyflxShare0586afa16084f40.setDefaultUnit(kony.flex.DP);
    var lblShare = new kony.ui.Label({
        "height": "100%",
        "id": "lblShare",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon42pxBlue",
        "text": "s",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [20, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnShare = new kony.ui.Button({
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "100%",
        "id": "btnShare",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnShare"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 20, 0],
        "paddingInPixel": false
    }, {});
    CopyflxShare0586afa16084f40.add(lblShare, btnShare);
    var FlexContainer0aa3dc1a8c1f441 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20dp",
        "id": "FlexContainer0aa3dc1a8c1f441",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0aa3dc1a8c1f441.setDefaultUnit(kony.flex.DP);
    FlexContainer0aa3dc1a8c1f441.add();
    flxMainContainer.add(flxTopSpacerDND, flxRewardsBanner, FlexContainer06bdc793499af41, CopyFlexContainer039f8ce96f8e040, CopyFlexContainer0b62a419d550e45, CopyFlexContainer03c41487314fe41, segRewardsInfo, btnJoinRewards, btnFAQ, btnTermsandConditions, CopyflxShare0586afa16084f40, FlexContainer0aa3dc1a8c1f441);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRewardsProgramInfo.capitalProgramInfo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "15%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0ee00528ffdfe4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0ee00528ffdfe4c",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, btnHeaderLeft, Label0ee00528ffdfe4c);
    var km64c65071b2c4a6586f203517faba879 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km64c65071b2c4a6586f203517faba879.setDefaultUnit(kony.flex.DP);
    var km8b1b75dca904b11b0940901600ab3bf = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8b1b75dca904b11b0940901600ab3bf.setDefaultUnit(kony.flex.DP);
    var kmb47021cc0fe4389bb764024e565f557 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km8e906cf90d743da8f7c0c16d885ad3b = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km8b1b75dca904b11b0940901600ab3bf.add(kmb47021cc0fe4389bb764024e565f557, km8e906cf90d743da8f7c0c16d885ad3b);
    var kmf9884c02b314da085943167ba589779 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf9884c02b314da085943167ba589779.setDefaultUnit(kony.flex.DP);
    var km8fe639fe8734c4eb7bcdf526fa14bab = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km8d1493d114b4a6687da031c9854933d = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var km698e3cc03d24df183621e65e015893c = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf9884c02b314da085943167ba589779.add(km8fe639fe8734c4eb7bcdf526fa14bab, km8d1493d114b4a6687da031c9854933d, km698e3cc03d24df183621e65e015893c);
    var km00f7d501a1249b8aade8453536d5af3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km00f7d501a1249b8aade8453536d5af3.setDefaultUnit(kony.flex.DP);
    var km4bbc1dd6d1f4449ad29adc727759b6e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km1ef505dd4be41c1a03ec5a5703775b2 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km00f7d501a1249b8aade8453536d5af3.add(km4bbc1dd6d1f4449ad29adc727759b6e, km1ef505dd4be41c1a03ec5a5703775b2);
    var km0ee9238969c4d29bb3d7090c34de77f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0ee9238969c4d29bb3d7090c34de77f.setDefaultUnit(kony.flex.DP);
    var km6e5d500201b4d6d8a51ab6c0c4d812d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5fba044e24a4e1c8a4f6eed340aeeaa = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km0ee9238969c4d29bb3d7090c34de77f.add(km6e5d500201b4d6d8a51ab6c0c4d812d, km5fba044e24a4e1c8a4f6eed340aeeaa);
    var kmf8ba5d074f64630b902022cb360578c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf8ba5d074f64630b902022cb360578c.setDefaultUnit(kony.flex.DP);
    var kmc0612fe56c14f5e94c9a9561054079d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kma52a8191e1b422a881185375b198cdd = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmf8ba5d074f64630b902022cb360578c.add(kmc0612fe56c14f5e94c9a9561054079d, kma52a8191e1b422a881185375b198cdd);
    var km8150f9064dc4c5eb2f1cbe3a6f3da60 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8150f9064dc4c5eb2f1cbe3a6f3da60.setDefaultUnit(kony.flex.DP);
    var km0f7446b8e6449f2a430f8d41a5631b9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km11deb167dd54cc386907f05ac4eeaa5 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km8150f9064dc4c5eb2f1cbe3a6f3da60.add(km0f7446b8e6449f2a430f8d41a5631b9, km11deb167dd54cc386907f05ac4eeaa5);
    var kme40bcb7bb6b4df7968b454bca483d8c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme40bcb7bb6b4df7968b454bca483d8c.setDefaultUnit(kony.flex.DP);
    var kmb6568203ceb4853bf5c9ff02f68f7a9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km5d510b6a22f47858c0e178faf8d86ef = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kme40bcb7bb6b4df7968b454bca483d8c.add(kmb6568203ceb4853bf5c9ff02f68f7a9, km5d510b6a22f47858c0e178faf8d86ef);
    var km15c8c2960cd49b6b251cdec1cb7dfff = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km15c8c2960cd49b6b251cdec1cb7dfff.setDefaultUnit(kony.flex.DP);
    var km4f9f6671a764c9c95e5dc073e4af822 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km93c301f93dd4489be7622c3c60f2951 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km15c8c2960cd49b6b251cdec1cb7dfff.add(km4f9f6671a764c9c95e5dc073e4af822, km93c301f93dd4489be7622c3c60f2951);
    km64c65071b2c4a6586f203517faba879.add(km8b1b75dca904b11b0940901600ab3bf, kmf9884c02b314da085943167ba589779, km00f7d501a1249b8aade8453536d5af3, km0ee9238969c4d29bb3d7090c34de77f, kmf8ba5d074f64630b902022cb360578c, km8150f9064dc4c5eb2f1cbe3a6f3da60, kme40bcb7bb6b4df7968b454bca483d8c, km15c8c2960cd49b6b251cdec1cb7dfff);
    frmRewardsProgramInfo.add(flxMainContainer, flxHeaderTitleContainer, km64c65071b2c4a6586f203517faba879);
};

function frmRewardsProgramInfoGlobals() {
    frmRewardsProgramInfo = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRewardsProgramInfo,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmRewardsProgramInfo",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};