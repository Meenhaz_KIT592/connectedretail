function addWidgetsfrmWeeklyAdHome() {
    frmWeeklyAdHome.setDefaultUnit(kony.flex.DP);
    var flxAds = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxAds",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "92dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAds.setDefaultUnit(kony.flex.DP);
    var segAds = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": "54dp",
        "data": [{}, {}, {}],
        "groupCells": false,
        "id": "segAds",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxWeeklyAdHome,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxWeeklyAdHome": "flxWeeklyAdHome",
            "flxWhiteLine": "flxWhiteLine",
            "lblAisle": "lblAisle",
            "lblCategoryCount": "lblCategoryCount",
            "lblCategoryName": "lblCategoryName",
            "lblNumber": "lblNumber"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAds.add(segAds);
    var km87ed0bc4a9a40b19991c86b00d596ba = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "isMaster": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxSearchResultsContainer",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": 2,
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6,
        "isVisible": false,
        "skin": "sknFormWhite"
    }, {}, {});
    km87ed0bc4a9a40b19991c86b00d596ba.setDefaultUnit(kony.flex.DP);
    var kmc5b4d647d64427baf0753fe396473a9 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDNDSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc5b4d647d64427baf0753fe396473a9.setDefaultUnit(kony.flex.DP);
    kmc5b4d647d64427baf0753fe396473a9.add();
    var kme32fac4d06a42128ca5cb15657a4835 = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "id": "segSearchOffers",
        "left": "0dp",
        "scrollingEvents": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "groupCells": false,
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        }
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km33d971faf584cd1a34f7879b72c6f2a = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "id": "segSearchResults",
        "left": "0dp",
        "scrollingEvents": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "groupCells": false,
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        }
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km87ed0bc4a9a40b19991c86b00d596ba.add(kmc5b4d647d64427baf0753fe396473a9, kme32fac4d06a42128ca5cb15657a4835, km33d971faf584cd1a34f7879b72c6f2a);
    var km212755cb2b4430eb45fd37727194228 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    km212755cb2b4430eb45fd37727194228.setDefaultUnit(kony.flex.DP);
    var km2d479d0a7484102ac54f20dcc053659 = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km2d479d0a7484102ac54f20dcc053659.setDefaultUnit(kony.flex.DP);
    km2d479d0a7484102ac54f20dcc053659.add();
    var km57890ed36114979984138cfd62952a8 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km57890ed36114979984138cfd62952a8.setDefaultUnit(kony.flex.DP);
    var kmc46c822f3d94be28d3b8152ab689c06 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc7dd43e29ac493683d2c0197977e561 = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    var km7fae9d87b724509b3dee7fcd11f216b = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmf2d867c8d7545b5a31aa55c82838c9d = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    kmf2d867c8d7545b5a31aa55c82838c9d.setDefaultUnit(kony.flex.DP);
    kmf2d867c8d7545b5a31aa55c82838c9d.add();
    var km36600125b1641d3bf4a20c76c8d0607 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km57890ed36114979984138cfd62952a8.add(kmc46c822f3d94be28d3b8152ab689c06, kmc7dd43e29ac493683d2c0197977e561, km7fae9d87b724509b3dee7fcd11f216b, kmf2d867c8d7545b5a31aa55c82838c9d, km36600125b1641d3bf4a20c76c8d0607);
    var kma57aa8dae034d3395f8ff0eb8069494 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kma57aa8dae034d3395f8ff0eb8069494.setDefaultUnit(kony.flex.DP);
    var kmf53381dc86d49fe8ff2679836466bf3 = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmf53381dc86d49fe8ff2679836466bf3.setDefaultUnit(kony.flex.DP);
    var km567e86b46bd444caa9fe51a04a97050 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km567e86b46bd444caa9fe51a04a97050.setDefaultUnit(kony.flex.DP);
    var km5e66a7fd9bb40d681db6cc344ba9ff4 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km567e86b46bd444caa9fe51a04a97050.add(km5e66a7fd9bb40d681db6cc344ba9ff4);
    var kmb88807a9a28426691ded4dfa5c9e183 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    kmb88807a9a28426691ded4dfa5c9e183.setDefaultUnit(kony.flex.DP);
    var km1c5e44d38154fd89c1211754387ca48 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb88807a9a28426691ded4dfa5c9e183.add(km1c5e44d38154fd89c1211754387ca48);
    var kmb953ab90e384b8e89f9fe79a2ec8946 = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmb953ab90e384b8e89f9fe79a2ec8946.setDefaultUnit(kony.flex.DP);
    var kmdca25e776f94706a7a53b8d5082a724 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmfc78ded2eb547f09b435c339364918d = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb953ab90e384b8e89f9fe79a2ec8946.add(kmdca25e776f94706a7a53b8d5082a724, kmfc78ded2eb547f09b435c339364918d);
    var kmb00ec9c887f4b649a91a8a63a02016a = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmf53381dc86d49fe8ff2679836466bf3.add(km567e86b46bd444caa9fe51a04a97050, kmb88807a9a28426691ded4dfa5c9e183, kmb953ab90e384b8e89f9fe79a2ec8946, kmb00ec9c887f4b649a91a8a63a02016a);
    var km1bc54b18ed74f78ace64cecab71883d = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km1bc54b18ed74f78ace64cecab71883d.setDefaultUnit(kony.flex.DP);
    km1bc54b18ed74f78ace64cecab71883d.add();
    kma57aa8dae034d3395f8ff0eb8069494.add(kmf53381dc86d49fe8ff2679836466bf3, km1bc54b18ed74f78ace64cecab71883d);
    var km340b531e92143a3b8d25e9bb664bd02 = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km340b531e92143a3b8d25e9bb664bd02.setDefaultUnit(kony.flex.DP);
    var km39b5abe836649e4ae1057125f82cb62 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km39b5abe836649e4ae1057125f82cb62.setDefaultUnit(kony.flex.DP);
    var km12fc13d605d42a8a32e15ae68cb052c = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km12fc13d605d42a8a32e15ae68cb052c.setDefaultUnit(kony.flex.DP);
    var km18076a7545c4dadbb41e58cdf1b1340 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmc50acccf8034f49a904f16f3fd46f70 = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km12fc13d605d42a8a32e15ae68cb052c.add(km18076a7545c4dadbb41e58cdf1b1340, kmc50acccf8034f49a904f16f3fd46f70);
    var km62d493f20a848d1bdeef932a3d84bfc = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km62d493f20a848d1bdeef932a3d84bfc.setDefaultUnit(kony.flex.DP);
    var kmcba2a8a5c5646d1a28f3b92676853bd = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmcba2a8a5c5646d1a28f3b92676853bd.setDefaultUnit(kony.flex.DP);
    var kme3866d63fbe472dabc07c1dd4703a98 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km9e8a76844ab42539447fe7c4a7774c1 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km4e5c99d11204b7a8499b5a63258b697 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmcf2adcfad724e038e786b5626cfeda0 = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3db68efd16a4ed88c318fc106553b16 = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd47f92412f445e29e94a3683e167c6c = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmcba2a8a5c5646d1a28f3b92676853bd.add(kme3866d63fbe472dabc07c1dd4703a98, km9e8a76844ab42539447fe7c4a7774c1, km4e5c99d11204b7a8499b5a63258b697, kmcf2adcfad724e038e786b5626cfeda0, km3db68efd16a4ed88c318fc106553b16, kmd47f92412f445e29e94a3683e167c6c);
    var kmbf95b9a5e8744ac85878aef9650c7fc = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmbf95b9a5e8744ac85878aef9650c7fc.setDefaultUnit(kony.flex.DP);
    var km7beeaf6e8ca4965989579b04f7b938f = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km072b77cb5e24d33a27f8bc7b0571fac = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km18ea2bbff164a76bc9369d8bde3a5d7 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmbf95b9a5e8744ac85878aef9650c7fc.add(km7beeaf6e8ca4965989579b04f7b938f, km072b77cb5e24d33a27f8bc7b0571fac, km18ea2bbff164a76bc9369d8bde3a5d7);
    km62d493f20a848d1bdeef932a3d84bfc.add(kmcba2a8a5c5646d1a28f3b92676853bd, kmbf95b9a5e8744ac85878aef9650c7fc);
    var km1c17cddc6244b588057bd5afd011ead = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km4bf532f333d44d5977f2b5bf6952186 = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km39b5abe836649e4ae1057125f82cb62.add(km12fc13d605d42a8a32e15ae68cb052c, km62d493f20a848d1bdeef932a3d84bfc, km1c17cddc6244b588057bd5afd011ead, km4bf532f333d44d5977f2b5bf6952186);
    km340b531e92143a3b8d25e9bb664bd02.add(km39b5abe836649e4ae1057125f82cb62);
    km212755cb2b4430eb45fd37727194228.add(km2d479d0a7484102ac54f20dcc053659, km57890ed36114979984138cfd62952a8, kma57aa8dae034d3395f8ff0eb8069494, km340b531e92143a3b8d25e9bb664bd02);
    var flxWeeklyAds = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxWeeklyAds",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "50dp",
        "width": "100%"
    }, {}, {});
    flxWeeklyAds.setDefaultUnit(kony.flex.DP);
    var btnRefineSearch = new kony.ui.Button({
        "focusSkin": "sknBtnBgTrans28pxBoldWhiteFoc",
        "height": "100%",
        "id": "btnRefineSearch",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTrans28pxBoldWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxWeeklyAds.add(btnRefineSearch);
    var km672c07c629340f8b8cc4c1549407cce = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km672c07c629340f8b8cc4c1549407cce.setDefaultUnit(kony.flex.DP);
    var kme26389ae8b24aee89a2d77d23be835c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    kme26389ae8b24aee89a2d77d23be835c.setDefaultUnit(kony.flex.DP);
    var km24da91a5cbb48b582cef2857f6c9d57 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km24da91a5cbb48b582cef2857f6c9d57.setDefaultUnit(kony.flex.DP);
    var km0e9fa14c54c4f91ba5e78ad0b788f78 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kma1b0e61ec1e4ea2b436689f44946045 = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    kma1b0e61ec1e4ea2b436689f44946045.setDefaultUnit(kony.flex.DP);
    var km901c7c2afe746da961140cc4ed79359 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2feb7876efa407d96ed615a30ec5bb2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmff34b326cce4b0d85960e0d46d1d7e9 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kma1b0e61ec1e4ea2b436689f44946045.add(km901c7c2afe746da961140cc4ed79359, km2feb7876efa407d96ed615a30ec5bb2, kmff34b326cce4b0d85960e0d46d1d7e9);
    var km790f0ab3ce146b5a7ff8e9e778cf371 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km8c55057928b47e4971b421c3fa10a64 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km24da91a5cbb48b582cef2857f6c9d57.add(km0e9fa14c54c4f91ba5e78ad0b788f78, kma1b0e61ec1e4ea2b436689f44946045, km790f0ab3ce146b5a7ff8e9e778cf371, km8c55057928b47e4971b421c3fa10a64);
    var km9ccf84895314b289864cce46345ec6f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    km9ccf84895314b289864cce46345ec6f.setDefaultUnit(kony.flex.DP);
    var kmbeb1e8c35fd434ab333ba3cece545de = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbeb1e8c35fd434ab333ba3cece545de.setDefaultUnit(kony.flex.DP);
    var kme3ea845a06449ddb917e58bed8dddfd = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmbeb1e8c35fd434ab333ba3cece545de.add(kme3ea845a06449ddb917e58bed8dddfd);
    km9ccf84895314b289864cce46345ec6f.add(kmbeb1e8c35fd434ab333ba3cece545de);
    var kmcf7844b7a9e4ca0b25cf33881904ed9 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kme26389ae8b24aee89a2d77d23be835c.add(km24da91a5cbb48b582cef2857f6c9d57, km9ccf84895314b289864cce46345ec6f, kmcf7844b7a9e4ca0b25cf33881904ed9);
    km672c07c629340f8b8cc4c1549407cce.add(kme26389ae8b24aee89a2d77d23be835c);
    var kmf80d8d21bc94425923d3e3086ff8af7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf80d8d21bc94425923d3e3086ff8af7.setDefaultUnit(kony.flex.DP);
    var km2941639ecb14c7c9790a431904e3966 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    km2941639ecb14c7c9790a431904e3966.setDefaultUnit(kony.flex.DP);
    var kmd98a2c5d0734a768ea059b4d8fe6418 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kmd98a2c5d0734a768ea059b4d8fe6418.setDefaultUnit(kony.flex.DP);
    var kmc386f4b7b1f4540a601c32433d79654 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    kmc386f4b7b1f4540a601c32433d79654.setDefaultUnit(kony.flex.DP);
    kmc386f4b7b1f4540a601c32433d79654.add();
    var km28dc6ac9f504053a175de4aa94e3115 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmee196ea45184d22bd574fcee4013433 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km963269a975743779b74f925389e1c3a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km963269a975743779b74f925389e1c3a.setDefaultUnit(kony.flex.DP);
    var kma69f1a4b7c94e339c19a9c9e0cf6e85 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km49b290ddc5548c4b9ed494023d0581b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km963269a975743779b74f925389e1c3a.add(kma69f1a4b7c94e339c19a9c9e0cf6e85, km49b290ddc5548c4b9ed494023d0581b);
    var kmc978cf8e8904db686332d583acc1ca2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc978cf8e8904db686332d583acc1ca2.setDefaultUnit(kony.flex.DP);
    var km192deb8739f474a9478a6b2c5ffd975 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km83dd43431904086bf10bab3a5fffdcf = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    kmc978cf8e8904db686332d583acc1ca2.add(km192deb8739f474a9478a6b2c5ffd975, km83dd43431904086bf10bab3a5fffdcf);
    var kmdc2aae614864bf99b77bdaffdebdde5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmdc2aae614864bf99b77bdaffdebdde5.setDefaultUnit(kony.flex.DP);
    var kmd0cd669de1d4969959e251f1e532d20 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmff7748c29ae434886845f5ebed7ca27 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmdc2aae614864bf99b77bdaffdebdde5.add(kmd0cd669de1d4969959e251f1e532d20, kmff7748c29ae434886845f5ebed7ca27);
    var km0272ee610de4f5ea6a8d51fb84b5514 = new kony.ui.Button({
        "centerY": "15dp",
        "height": "13.93%",
        "id": "btnClearVoiceSearch",
        "right": "9dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmd98a2c5d0734a768ea059b4d8fe6418.add(kmc386f4b7b1f4540a601c32433d79654, km28dc6ac9f504053a175de4aa94e3115, kmee196ea45184d22bd574fcee4013433, km963269a975743779b74f925389e1c3a, kmc978cf8e8904db686332d583acc1ca2, kmdc2aae614864bf99b77bdaffdebdde5, km0272ee610de4f5ea6a8d51fb84b5514);
    km2941639ecb14c7c9790a431904e3966.add(kmd98a2c5d0734a768ea059b4d8fe6418);
    kmf80d8d21bc94425923d3e3086ff8af7.add(km2941639ecb14c7c9790a431904e3966);
    var kme34c887faaf45f9b25a815b547bdf13 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    kme34c887faaf45f9b25a815b547bdf13.setDefaultUnit(kony.flex.DP);
    var kmc03cad312294eac9069aeb2202caa0d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc03cad312294eac9069aeb2202caa0d.setDefaultUnit(kony.flex.DP);
    var km7025748a8034abb9a42de98a8c8987f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km6069afe63db42a1bbc8ce0b13fa3b37 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmc03cad312294eac9069aeb2202caa0d.add(km7025748a8034abb9a42de98a8c8987f, km6069afe63db42a1bbc8ce0b13fa3b37);
    var km7ed2e397d8b49afa28e729e0544b892 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7ed2e397d8b49afa28e729e0544b892.setDefaultUnit(kony.flex.DP);
    var km1595483f9e348cf9d7296ef9ab768b6 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmb58abe9eb354439a9a43b4e35101b23 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var kma63e8c30c0b441fa6d4a924109cfc8f = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km7ed2e397d8b49afa28e729e0544b892.add(km1595483f9e348cf9d7296ef9ab768b6, kmb58abe9eb354439a9a43b4e35101b23, kma63e8c30c0b441fa6d4a924109cfc8f);
    var km9ccf50e25b14022869b3db7ced34ae1 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9ccf50e25b14022869b3db7ced34ae1.setDefaultUnit(kony.flex.DP);
    var km7575079069443b99255d736751d7645 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km85d6d2fe3464ea7baff46886fabd1a6 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km9ccf50e25b14022869b3db7ced34ae1.add(km7575079069443b99255d736751d7645, km85d6d2fe3464ea7baff46886fabd1a6);
    var kmf6f325ffff6467197e35bb7f0ade492 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf6f325ffff6467197e35bb7f0ade492.setDefaultUnit(kony.flex.DP);
    var km552edbee81e4e46a6761883f9f82e51 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km0190972438e45b4a42868b810d4a2d8 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmf6f325ffff6467197e35bb7f0ade492.add(km552edbee81e4e46a6761883f9f82e51, km0190972438e45b4a42868b810d4a2d8);
    var kmd1118e9b7224faaad1ba2122646abf7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd1118e9b7224faaad1ba2122646abf7.setDefaultUnit(kony.flex.DP);
    var kmf5ab22c9ad745479ac7fbda9388f1a5 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmcc405aa24c04877875e0d414183285e = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmd1118e9b7224faaad1ba2122646abf7.add(kmf5ab22c9ad745479ac7fbda9388f1a5, kmcc405aa24c04877875e0d414183285e);
    var kma054835df4f475e866d4324aa216f1f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma054835df4f475e866d4324aa216f1f.setDefaultUnit(kony.flex.DP);
    var km71d1a0671f34b80b5fbb7310f0820e0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km6c5a985fd11487ead4dfddf2afc022d = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kma054835df4f475e866d4324aa216f1f.add(km71d1a0671f34b80b5fbb7310f0820e0, km6c5a985fd11487ead4dfddf2afc022d);
    var km17aa687a80641ea83ec066a0f3a275a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km17aa687a80641ea83ec066a0f3a275a.setDefaultUnit(kony.flex.DP);
    var km82c15a6fde94ca891b2a3a5c29a9a1d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbb0d816559143889a2aec1144b5af63 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km17aa687a80641ea83ec066a0f3a275a.add(km82c15a6fde94ca891b2a3a5c29a9a1d, kmbb0d816559143889a2aec1144b5af63);
    var km7ccb1ae743145a6bf229d321889a7d8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7ccb1ae743145a6bf229d321889a7d8.setDefaultUnit(kony.flex.DP);
    var km5814460a89f4074a6e6f5762d379c62 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km8c957f83d784c6cb64153f771f1fe44 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km7ccb1ae743145a6bf229d321889a7d8.add(km5814460a89f4074a6e6f5762d379c62, km8c957f83d784c6cb64153f771f1fe44);
    kme34c887faaf45f9b25a815b547bdf13.add(kmc03cad312294eac9069aeb2202caa0d, km7ed2e397d8b49afa28e729e0544b892, km9ccf50e25b14022869b3db7ced34ae1, kmf6f325ffff6467197e35bb7f0ade492, kmd1118e9b7224faaad1ba2122646abf7, kma054835df4f475e866d4324aa216f1f, km17aa687a80641ea83ec066a0f3a275a, km7ccb1ae743145a6bf229d321889a7d8);
    var km2abb9ed8bbf4e7b99020638016e1d34 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    km2abb9ed8bbf4e7b99020638016e1d34.setDefaultUnit(kony.flex.DP);
    var kmbb8e1bad33b4beba854c88572b7ffd2 = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km6066f29ef3249d5a8fecc1cd709f119 = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    km6066f29ef3249d5a8fecc1cd709f119.setDefaultUnit(kony.flex.DP);
    var kmadbc9faddd7422fa85db847fa7038aa = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmadbc9faddd7422fa85db847fa7038aa.setDefaultUnit(kony.flex.DP);
    var kmd51b3ffdc0d46a6a3fb8383abec11b4 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmadbc9faddd7422fa85db847fa7038aa.add(kmd51b3ffdc0d46a6a3fb8383abec11b4);
    var kma1117c7f142440882c51775f8a35c2c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kma1117c7f142440882c51775f8a35c2c.setDefaultUnit(kony.flex.DP);
    var km2f8c0d7636240b9bffea81a007c1915 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km8c5125182c54f8d8905da208dbb3631 = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClearSearch",
        "right": "2%",
        "text": "X",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kma1117c7f142440882c51775f8a35c2c.add(km2f8c0d7636240b9bffea81a007c1915, km8c5125182c54f8d8905da208dbb3631);
    var kmb04b612fe074dabab1aa697635e0fda = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmb04b612fe074dabab1aa697635e0fda.setDefaultUnit(kony.flex.DP);
    var km555e675e94843fcb1087ac1d5517e5b = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    kmb04b612fe074dabab1aa697635e0fda.add(km555e675e94843fcb1087ac1d5517e5b);
    var kmfc0ad0bf96b4ca4a0ccabc0329b48c2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmfc0ad0bf96b4ca4a0ccabc0329b48c2.setDefaultUnit(kony.flex.DP);
    var kmf3605117e21410c9b8ff68e897068ce = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmfc0ad0bf96b4ca4a0ccabc0329b48c2.add(kmf3605117e21410c9b8ff68e897068ce);
    var km9eb0a3ac82347edb326c7c96bfa4eb8 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCameraImageSearch",
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "isVisible": false,
        "skin": "sknBtnPopupCloseTransparentBg"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmcefae6f139946c0b6b260a899aa918a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmcefae6f139946c0b6b260a899aa918a.setDefaultUnit(kony.flex.DP);
    var km89fc0348169494d8bc844f7cbd7b6ef = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmcefae6f139946c0b6b260a899aa918a.add(km89fc0348169494d8bc844f7cbd7b6ef);
    km6066f29ef3249d5a8fecc1cd709f119.add(kmadbc9faddd7422fa85db847fa7038aa, kma1117c7f142440882c51775f8a35c2c, kmb04b612fe074dabab1aa697635e0fda, kmfc0ad0bf96b4ca4a0ccabc0329b48c2, km9eb0a3ac82347edb326c7c96bfa4eb8, kmcefae6f139946c0b6b260a899aa918a);
    var km06a1f6f41ca4a5d80ed945e627379ee = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmacd0f5d620140079ddc6b7c686be22c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmacd0f5d620140079ddc6b7c686be22c.setDefaultUnit(kony.flex.DP);
    var km4dbb2fd34584056ba6f19e2b687760e = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kma350a21ebbd4b1da119e633e8d9cbcd = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmacd0f5d620140079ddc6b7c686be22c.add(km4dbb2fd34584056ba6f19e2b687760e, kma350a21ebbd4b1da119e633e8d9cbcd);
    km2abb9ed8bbf4e7b99020638016e1d34.add(kmbb8e1bad33b4beba854c88572b7ffd2, km6066f29ef3249d5a8fecc1cd709f119, km06a1f6f41ca4a5d80ed945e627379ee, kmacd0f5d620140079ddc6b7c686be22c);
    frmWeeklyAdHome.add(flxAds, km87ed0bc4a9a40b19991c86b00d596ba, km212755cb2b4430eb45fd37727194228, flxWeeklyAds, km672c07c629340f8b8cc4c1549407cce, kmf80d8d21bc94425923d3e3086ff8af7, kme34c887faaf45f9b25a815b547bdf13, km2abb9ed8bbf4e7b99020638016e1d34);
};

function frmWeeklyAdHomeGlobals() {
    frmWeeklyAdHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmWeeklyAdHome,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmWeeklyAdHome",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};