function addWidgetsfrmGuide() {
    frmGuide.setDefaultUnit(kony.flex.DP);
    var flexGuide1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide1.setDefaultUnit(kony.flex.DP);
    var imgGuide1 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide1",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide1.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "49.99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent1.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle11 = new kony.ui.Label({
        "id": "lblGuideTitle11",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmHome.anEasyWayToMake"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideTitle12 = new kony.ui.Label({
        "id": "lblGuideTitle12",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmHome.atFingerTips"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc1 = new kony.ui.Label({
        "height": "40dp",
        "id": "lblGuideDesc1",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "34%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "3%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl1.setDefaultUnit(kony.flex.DP);
    var lblGuideControl1 = new kony.ui.Label({
        "centerX": "49%",
        "centerY": "50%",
        "id": "lblGuideControl1",
        "isVisible": true,
        "left": "23dp",
        "skin": "sknLblGothamBoldRedGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideControl1"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron1 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron1",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIconRedGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl1.add(lblGuideControl1, lblGuideChevron1);
    flexGuideContent1.add(lblGuideTitle11, lblGuideTitle12, lblGuideDesc1, flexGuideControl1);
    flexGuide1.add(imgGuide1, flexGuideContent1);
    var flexGuide2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "500dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide2.setDefaultUnit(kony.flex.DP);
    var imgGuide2 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide2",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide2.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "50%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent2.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle21 = new kony.ui.Label({
        "id": "lblGuideTitle21",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideTitle21"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideTitle22 = new kony.ui.Label({
        "id": "lblGuideTitle22",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideTitle22"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc2 = new kony.ui.Label({
        "id": "lblGuideDesc2",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideDesc2"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl21 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl21",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "3%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl21.setDefaultUnit(kony.flex.DP);
    var lblGuideControl21 = new kony.ui.Label({
        "centerX": "49.50%",
        "centerY": "49%",
        "id": "lblGuideControl21",
        "isVisible": true,
        "left": "23dp",
        "skin": "sknLblGothamBoldRedGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.allowLocation"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron21 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron21",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconRedGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl21.add(lblGuideControl21, lblGuideChevron21);
    var flexGuideControl22 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl22",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBorderWhiteGuideControl",
        "top": "3%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl22.setDefaultUnit(kony.flex.DP);
    var lblGuideControl22 = new kony.ui.Label({
        "centerX": "49.50%",
        "centerY": "49%",
        "id": "lblGuideControl22",
        "isVisible": true,
        "skin": "sknLblGothamBoldWhiteGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.notRightNow"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron22 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron22",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl22.add(lblGuideControl22, lblGuideChevron22);
    flexGuideContent2.add(lblGuideTitle21, lblGuideTitle22, lblGuideDesc2, flexGuideControl21, flexGuideControl22);
    flexGuide2.add(imgGuide2, flexGuideContent2);
    var flexGuide3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "500dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide3.setDefaultUnit(kony.flex.DP);
    var imgGuide3 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide3",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide3.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "50%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent3.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle3 = new kony.ui.Label({
        "id": "lblGuideTitle3",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.beTheFirst"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc3 = new kony.ui.Label({
        "id": "lblGuideDesc3",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideDesc3"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl31 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "40%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl31",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl31.setDefaultUnit(kony.flex.DP);
    var lblGuideControl31 = new kony.ui.Label({
        "centerX": "48%",
        "centerY": "50.00%",
        "id": "lblGuideControl31",
        "isVisible": true,
        "left": "23dp",
        "skin": "sknLblGothamBoldRedGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSettings.enableNotifications"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron31 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron31",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl31.add(lblGuideControl31, lblGuideChevron31);
    var flexGuideControl32 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "17%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl32",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBorderWhiteGuideControl",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl32.setDefaultUnit(kony.flex.DP);
    var lblGuideControl32 = new kony.ui.Label({
        "centerX": "49%",
        "centerY": "48%",
        "id": "lblGuideControl32",
        "isVisible": true,
        "skin": "sknLblGothamBoldWhiteGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.notRightNow"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron32 = new kony.ui.Label({
        "centerY": "47%",
        "id": "lblGuideChevron32",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl32.add(lblGuideControl32, lblGuideChevron32);
    flexGuideContent3.add(lblGuideTitle3, lblGuideDesc3, flexGuideControl31, flexGuideControl32);
    flexGuide3.add(imgGuide3, flexGuideContent3);
    var flexGuide4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "500dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide4.setDefaultUnit(kony.flex.DP);
    var imgGuide4 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide4",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide4.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "50%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent4.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle4 = new kony.ui.Label({
        "id": "lblGuideTitle4",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideTitle4"),
        "textStyle": {
            "lineSpacing": 4,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc4 = new kony.ui.Label({
        "id": "lblGuideDesc4",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideDesc4"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "25%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl41",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBorderWhiteGuideControl",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl41.setDefaultUnit(kony.flex.DP);
    var lblGuideControl41 = new kony.ui.Label({
        "centerX": "49%",
        "centerY": "48%",
        "id": "lblGuideControl41",
        "isVisible": true,
        "skin": "sknLblGothamBoldWhiteGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnNext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron41 = new kony.ui.Label({
        "centerY": "47%",
        "id": "lblGuideChevron41",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl41.add(lblGuideControl41, lblGuideChevron41);
    flexGuideContent4.add(lblGuideTitle4, lblGuideDesc4, flexGuideControl41);
    flexGuide4.add(imgGuide4, flexGuideContent4);
    var flexGuide5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "500dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide5.setDefaultUnit(kony.flex.DP);
    var imgGuide5 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide5",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide5.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "50%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent5.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle5 = new kony.ui.Label({
        "id": "lblGuideTitle5",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideTitle5"),
        "textStyle": {
            "lineSpacing": 4,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc5 = new kony.ui.Label({
        "id": "lblGuideDesc5",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideDesc5"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl51 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "25%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl51",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBorderWhiteGuideControl",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl51.setDefaultUnit(kony.flex.DP);
    var lblGuideControl51 = new kony.ui.Label({
        "centerX": "49%",
        "centerY": "48%",
        "id": "lblGuideControl51",
        "isVisible": true,
        "skin": "sknLblGothamBoldWhiteGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnNext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron51 = new kony.ui.Label({
        "centerY": "47%",
        "id": "lblGuideChevron51",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl51.add(lblGuideControl51, lblGuideChevron51);
    flexGuideContent5.add(lblGuideTitle5, lblGuideDesc5, flexGuideControl51);
    flexGuide5.add(imgGuide5, flexGuideContent5);
    var flexGuide7 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide7",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "500dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide7.setDefaultUnit(kony.flex.DP);
    var imgGuide7 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide7",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide7.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent7 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent7",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "50%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent7.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle7 = new kony.ui.Label({
        "id": "lblGuideTitle7",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideTitle5"),
        "textStyle": {
            "lineSpacing": 4,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc7 = new kony.ui.Label({
        "id": "lblGuideDesc7",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideDesc5"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl71 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "25%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl71",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBorderWhiteGuideControl",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl71.setDefaultUnit(kony.flex.DP);
    var lblGuideControl71 = new kony.ui.Label({
        "centerX": "49%",
        "centerY": "48%",
        "id": "lblGuideControl71",
        "isVisible": true,
        "skin": "sknLblGothamBoldWhiteGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnNext"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron71 = new kony.ui.Label({
        "centerY": "47%",
        "id": "lblGuideChevron71",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl71.add(lblGuideControl71, lblGuideChevron71);
    flexGuideContent7.add(lblGuideTitle7, lblGuideDesc7, flexGuideControl71);
    flexGuide7.add(imgGuide7, flexGuideContent7);
    var flexGuide6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexGuide6",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "500dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuide6.setDefaultUnit(kony.flex.DP);
    var imgGuide6 = new kony.ui.Image2({
        "height": "50%",
        "id": "imgGuide6",
        "isVisible": true,
        "left": "10dp",
        "right": "10dp",
        "skin": "slImage",
        "src": "guide6.png",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexGuideContent6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flexGuideContent6",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "50%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexGuideContent6.setDefaultUnit(kony.flex.DP);
    var lblGuideTitle6 = new kony.ui.Label({
        "id": "lblGuideTitle6",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMediumGuideTitle",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.helloThere"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideDesc6 = new kony.ui.Label({
        "id": "lblGuideDesc6",
        "isVisible": true,
        "left": 10,
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideDesc4"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexGuideControl61 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl61",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "3%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl61.setDefaultUnit(kony.flex.DP);
    var lblGuideControl61 = new kony.ui.Label({
        "centerX": "50.00%",
        "centerY": "50.00%",
        "id": "lblGuideControl61",
        "isVisible": true,
        "left": "23dp",
        "skin": "sknLblGothamBoldRedGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnSignIn"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron61 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron61",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIconRedGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl61.add(lblGuideControl61, lblGuideChevron61);
    var flexGuideControl62 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20%",
        "id": "flexGuideControl62",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "isModalContainer": false,
        "skin": "sknFlexBorderWhiteGuideControl",
        "top": "3%",
        "width": "85%",
        "zIndex": 1
    }, {}, {});
    flexGuideControl62.setDefaultUnit(kony.flex.DP);
    var lblGuideControl62 = new kony.ui.Label({
        "centerX": "49%",
        "centerY": "49%",
        "id": "lblGuideControl62",
        "isVisible": true,
        "skin": "sknLblGothamBoldWhiteGuideControl",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSignIn.btnCreateAccount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron62 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron62",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl62.add(lblGuideControl62, lblGuideChevron62);
    var flexGuideControl63 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flexGuideControl63",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "4%",
        "skin": "sknFlexBgRedNoBdr",
        "top": "6%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flexGuideControl63.setDefaultUnit(kony.flex.DP);
    var lblGuideControl63 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblGuideControl63",
        "isVisible": true,
        "right": "15dp",
        "skin": "sknLblGothamRegularGuideDesc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmGuide.lblGuideControl63"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblGuideChevron63 = new kony.ui.Label({
        "centerY": "48%",
        "id": "lblGuideChevron63",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIconWhiteGuideChevron",
        "text": "K",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexGuideControl63.add(lblGuideControl63, lblGuideChevron63);
    flexGuideContent6.add(lblGuideTitle6, lblGuideDesc6, flexGuideControl61, flexGuideControl62, flexGuideControl63);
    flexGuide6.add(imgGuide6, flexGuideContent6);
    frmGuide.add(flexGuide1, flexGuide2, flexGuide3, flexGuide4, flexGuide5, flexGuide7, flexGuide6);
};

function frmGuideGlobals() {
    frmGuide = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmGuide,
        "allowVerticalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmGuide",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};