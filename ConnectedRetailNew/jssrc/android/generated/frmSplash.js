function addWidgetsfrmSplash() {
    frmSplash.setDefaultUnit(kony.flex.DP);
    frmSplash.add();
};

function frmSplashGlobals() {
    frmSplash = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSplash,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmSplash",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknSplash"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};