function addWidgetsfrmRewardsProfileInterests() {
    frmRewardsProfileInterests.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var rtxtQuestion = new kony.ui.RichText({
        "id": "rtxtQuestion",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGothamBold28pxDark",
        "text": "How often do you do creative projects<br>like arts & crafts, DIY, etc.",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopyFlexContainer0792d6ad92f2e4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer0792d6ad92f2e4b",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0792d6ad92f2e4b.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0b0f6bc1c5f384c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0b0f6bc1c5f384c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0b0f6bc1c5f384c.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0b0f6bc1c5f384c.add();
    var CopyLabel0824bb7e3e95b4d = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0824bb7e3e95b4d",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "u",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyButton018ad112d795a46 = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "CopyButton018ad112d795a46",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": "NEVER",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0792d6ad92f2e4b.add(CopyFlexContainer0b0f6bc1c5f384c, CopyLabel0824bb7e3e95b4d, CopyButton018ad112d795a46);
    var CopyFlexContainer0d1c034d1c6c748 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer0d1c034d1c6c748",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0d1c034d1c6c748.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer029d9b618981143 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer029d9b618981143",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer029d9b618981143.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer029d9b618981143.add();
    var CopyLabel029cf0ae772af48 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel029cf0ae772af48",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "u",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyButton08e0d797e1d0546 = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "CopyButton08e0d797e1d0546",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": "RARELY",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0d1c034d1c6c748.add(CopyFlexContainer029d9b618981143, CopyLabel029cf0ae772af48, CopyButton08e0d797e1d0546);
    var CopyFlexContainer05d8e19b94d5144 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer05d8e19b94d5144",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer05d8e19b94d5144.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer072cd66febaf942 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer072cd66febaf942",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer072cd66febaf942.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer072cd66febaf942.add();
    var CopyLabel0df1a3ea7015149 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0df1a3ea7015149",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "u",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyButton08ce2a499fe4447 = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "CopyButton08ce2a499fe4447",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": "SOMETIMES",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer05d8e19b94d5144.add(CopyFlexContainer072cd66febaf942, CopyLabel0df1a3ea7015149, CopyButton08ce2a499fe4447);
    var CopyFlexContainer0c0e46e29453e49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer0c0e46e29453e49",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0c0e46e29453e49.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0e4125c2e56a647 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer0e4125c2e56a647",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0e4125c2e56a647.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0e4125c2e56a647.add();
    var CopyLabel05ff736419dbc46 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel05ff736419dbc46",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "u",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyButton03c3e74df278445 = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "CopyButton03c3e74df278445",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": "REGULARLY",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer0c0e46e29453e49.add(CopyFlexContainer0e4125c2e56a647, CopyLabel05ff736419dbc46, CopyButton03c3e74df278445);
    var CopyFlexContainer00b268f47db8248 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer00b268f47db8248",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer00b268f47db8248.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer08a69cf9501094d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyFlexContainer08a69cf9501094d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer08a69cf9501094d.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer08a69cf9501094d.add();
    var CopyLabel052e548a250b045 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel052e548a250b045",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px999999",
        "text": "u",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyButton0885a134b67ae43 = new kony.ui.Button({
        "focusSkin": "sknBtnTransReg24pxDarkFoc",
        "height": "100%",
        "id": "CopyButton0885a134b67ae43",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransReg24pxDark",
        "text": "VERY FREQUENTLY",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer00b268f47db8248.add(CopyFlexContainer08a69cf9501094d, CopyLabel052e548a250b045, CopyButton0885a134b67ae43);
    var FlexContainer0e885e995a5cc4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "FlexContainer0e885e995a5cc4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgE0E0E0",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0e885e995a5cc4e.setDefaultUnit(kony.flex.DP);
    FlexContainer0e885e995a5cc4e.add();
    var segInterests = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "imgSelect": "imagedrag.png",
            "lblInterest": "BABY/TODDLER/PRE-SCHOOL",
            "lblSelect": "u"
        }, {
            "imgSelect": "imagedrag.png",
            "lblInterest": "BABY/TODDLER/PRE-SCHOOL",
            "lblSelect": "u"
        }, {
            "imgSelect": "imagedrag.png",
            "lblInterest": "BABY/TODDLER/PRE-SCHOOL",
            "lblSelect": "u"
        }],
        "groupCells": false,
        "id": "segInterests",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxRewardsProfileInterest,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "FlexContainer0e885e995a5cc4e": "FlexContainer0e885e995a5cc4e",
            "flxRewardsInterest": "flxRewardsInterest",
            "flxRewardsProfileInterest": "flxRewardsProfileInterest",
            "imgSelect": "imgSelect",
            "lblInterest": "lblInterest",
            "lblSelect": "lblSelect"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnReset = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnReset",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": "RESET",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnNextToRewardsSkills = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnNextToRewardsSkills",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": "SKIP",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxMainContainer.add(flxTopSpacerDND, rtxtQuestion, CopyFlexContainer0792d6ad92f2e4b, CopyFlexContainer0d1c034d1c6c748, CopyFlexContainer05d8e19b94d5144, CopyFlexContainer0c0e46e29453e49, CopyFlexContainer00b268f47db8248, FlexContainer0e885e995a5cc4e, segInterests, btnReset, btnNextToRewardsSkills);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": "REWARDS PROFILE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderTitleContainer.add(lblFormTitle, lblChevron, btnHeaderLeft);
    var kmc2ef5c9802e475eb5589e7c1c7d7a80 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    kmc2ef5c9802e475eb5589e7c1c7d7a80.setDefaultUnit(kony.flex.DP);
    var km9cf9916d061470abee139a022b7f648 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9cf9916d061470abee139a022b7f648.setDefaultUnit(kony.flex.DP);
    var kmfa317f48ce0400abac53d18add144a0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km9bba68f25dc49bcaef8ec84a1ad75c1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km9cf9916d061470abee139a022b7f648.add(kmfa317f48ce0400abac53d18add144a0, km9bba68f25dc49bcaef8ec84a1ad75c1);
    var km293cce1830e45c1b2201cafff397805 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km293cce1830e45c1b2201cafff397805.setDefaultUnit(kony.flex.DP);
    var kmfb39e90a3744ee59eea97b1710fd26a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km1ab09f8fe514b38aa8ea291d09fee5c = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var kmd3c1eff544b4c82b21cf95f70992fab = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km293cce1830e45c1b2201cafff397805.add(kmfb39e90a3744ee59eea97b1710fd26a, km1ab09f8fe514b38aa8ea291d09fee5c, kmd3c1eff544b4c82b21cf95f70992fab);
    var km9a5d6d7f7df4ec2b7654c0e039851f0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9a5d6d7f7df4ec2b7654c0e039851f0.setDefaultUnit(kony.flex.DP);
    var km056df9b516f496a9e9a4abfb829578f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmbedbb15869048b594e08127a1542f24 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km9a5d6d7f7df4ec2b7654c0e039851f0.add(km056df9b516f496a9e9a4abfb829578f, kmbedbb15869048b594e08127a1542f24);
    var km505f4e9917c4518bcbf7e5414aafaf6 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km505f4e9917c4518bcbf7e5414aafaf6.setDefaultUnit(kony.flex.DP);
    var km5b692ad6e5c466d92ad145bc5d1c1ed = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km86e9abcd98849fcb6609ff6395ee57a = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km505f4e9917c4518bcbf7e5414aafaf6.add(km5b692ad6e5c466d92ad145bc5d1c1ed, km86e9abcd98849fcb6609ff6395ee57a);
    var km089549bf5344c8aa8eb433d686e0f5b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km089549bf5344c8aa8eb433d686e0f5b.setDefaultUnit(kony.flex.DP);
    var km3c40936e2cd43ac9daf0f9d61bf65e5 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km20a1dfa6d164807910ac0c00e98755b = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km089549bf5344c8aa8eb433d686e0f5b.add(km3c40936e2cd43ac9daf0f9d61bf65e5, km20a1dfa6d164807910ac0c00e98755b);
    var km3fda298e80d4c80ba06db89d2224e60 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3fda298e80d4c80ba06db89d2224e60.setDefaultUnit(kony.flex.DP);
    var km2832257e4184c09b8a8da427db42b8d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km60de284a5ac4fc1a948c17ca4d520aa = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km3fda298e80d4c80ba06db89d2224e60.add(km2832257e4184c09b8a8da427db42b8d, km60de284a5ac4fc1a948c17ca4d520aa);
    var kmcb127778b3d4fa0b7c908e6bca9338f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmcb127778b3d4fa0b7c908e6bca9338f.setDefaultUnit(kony.flex.DP);
    var km9e434b46c324ba8890ef7dbf9f51732 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km366ac41c9504f428d3298dffe9957bf = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmcb127778b3d4fa0b7c908e6bca9338f.add(km9e434b46c324ba8890ef7dbf9f51732, km366ac41c9504f428d3298dffe9957bf);
    var kmcdde5d0938a4416a435ddb3b1fa6051 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmcdde5d0938a4416a435ddb3b1fa6051.setDefaultUnit(kony.flex.DP);
    var km4b8b1eed663492a87cd3af957676972 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmc77f6f4e8a24dc5a03828660d88d155 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmcdde5d0938a4416a435ddb3b1fa6051.add(km4b8b1eed663492a87cd3af957676972, kmc77f6f4e8a24dc5a03828660d88d155);
    kmc2ef5c9802e475eb5589e7c1c7d7a80.add(km9cf9916d061470abee139a022b7f648, km293cce1830e45c1b2201cafff397805, km9a5d6d7f7df4ec2b7654c0e039851f0, km505f4e9917c4518bcbf7e5414aafaf6, km089549bf5344c8aa8eb433d686e0f5b, km3fda298e80d4c80ba06db89d2224e60, kmcb127778b3d4fa0b7c908e6bca9338f, kmcdde5d0938a4416a435ddb3b1fa6051);
    frmRewardsProfileInterests.add(flxMainContainer, flxHeaderTitleContainer, kmc2ef5c9802e475eb5589e7c1c7d7a80);
};

function frmRewardsProfileInterestsGlobals() {
    frmRewardsProfileInterests = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRewardsProfileInterests,
        "enabledForIdleTimeout": false,
        "id": "frmRewardsProfileInterests",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};