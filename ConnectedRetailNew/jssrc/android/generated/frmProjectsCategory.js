function addWidgetsfrmProjectsCategory() {
    frmProjectsCategory.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCategory.setDefaultUnit(kony.flex.DP);
    var imgCategoryImg = new kony.ui.Image2({
        "id": "imgCategoryImg",
        "imageWhenFailed": "notavailable_1080x142.png",
        "imageWhileDownloading": "white_1080x142.png",
        "isVisible": false,
        "left": "0dp",
        "skin": "slImage",
        "src": "white_1080x142.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCategoryName = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransMed32px818181",
        "height": "100%",
        "id": "btnCategoryName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTransMed32px818181",
        "text": "Holiday & Celebration",
        "top": "0dp",
        "width": "100%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {});
    var lblCategoryBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblCategoryBack",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon34px000000",
        "text": "L",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblGothamMedium32pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnBack"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCategory.add(imgCategoryImg, btnCategoryName, lblCategoryBack, lblBack);
    var lbltextNoResultsFound = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbltextNoResultsFound",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxDarkGray",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmProjectsCategory.lbltextNoResultsFound"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexSeg = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "76.50%",
        "horizontalScrollIndicator": true,
        "id": "flexSeg",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexSeg.setDefaultUnit(kony.flex.DP);
    var segCateogories = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "data": [{
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Teacher Appreciation",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Patriotic",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Summer",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "School Spirit",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Day of the Dead",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Valentine's Day",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Halloween",
            "lblNumber": ""
        }, {
            "lblAisle": "",
            "lblCategoryCount": "",
            "lblCategoryName": "Christmas",
            "lblNumber": ""
        }],
        "groupCells": false,
        "id": "segCateogories",
        "isVisible": true,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowE0E0E0",
        "rowSkin": "sknSegRowF7F7F7",
        "rowTemplate": flxCategoryList,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "ffffff00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "10dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxCategoryList": "flxCategoryList",
            "flxWhiteLine": "flxWhiteLine",
            "lblAisle": "lblAisle",
            "lblCategoryCount": "lblCategoryCount",
            "lblCategoryName": "lblCategoryName",
            "lblNumber": "lblNumber"
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "12dp",
        "id": "flxBottomSpacerDND",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flexSeg.add(segCateogories, flxBottomSpacerDND);
    flxMainContainer.add(flxTopSpacerDND, flxCategory, lbltextNoResultsFound, flexSeg);
    var km2d226418e4d497f95a20d8e8aeeeba5 = new kony.ui.FlexScrollContainer({
        "isMaster": true,
        "id": "flxSearchResultsContainer",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    km2d226418e4d497f95a20d8e8aeeeba5.setDefaultUnit(kony.flex.DP);
    var km2521e635c4b44aeb256c22ec9ce00c9 = new kony.ui.FlexContainer({
        "id": "flxTopSpacerDNDSearch",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km2521e635c4b44aeb256c22ec9ce00c9.setDefaultUnit(kony.flex.DP);
    km2521e635c4b44aeb256c22ec9ce00c9.add();
    var km4296ca0fb0a4bbab4d683028b62c4e9 = new kony.ui.SegmentedUI2({
        "id": "segSearchOffers",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km12863c6f7de44599720165d5a454333 = new kony.ui.SegmentedUI2({
        "id": "segSearchResults",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km2d226418e4d497f95a20d8e8aeeeba5.add(km2521e635c4b44aeb256c22ec9ce00c9, km4296ca0fb0a4bbab4d683028b62c4e9, km12863c6f7de44599720165d5a454333);
    var km0b9532d4ba84094b6088b80c7cd5cae = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    km0b9532d4ba84094b6088b80c7cd5cae.setDefaultUnit(kony.flex.DP);
    var kmaea38377a29430186221f9b23b6da8e = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmaea38377a29430186221f9b23b6da8e.setDefaultUnit(kony.flex.DP);
    kmaea38377a29430186221f9b23b6da8e.add();
    var km0befc28099141daa7f6b029279f63c1 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km0befc28099141daa7f6b029279f63c1.setDefaultUnit(kony.flex.DP);
    var km6cd169dcbc3409aac1483ee761c3330 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmaf0c2d9497041988b7c3f965224f9e5 = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {});
    var kmbe26ebbbb5e459bb3faba3a44b9ab50 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kme6491020d484627a436db9517f7d294 = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    kme6491020d484627a436db9517f7d294.setDefaultUnit(kony.flex.DP);
    kme6491020d484627a436db9517f7d294.add();
    var km834657daf0a4f21b3110fc7e5484dc0 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km0befc28099141daa7f6b029279f63c1.add(km6cd169dcbc3409aac1483ee761c3330, kmaf0c2d9497041988b7c3f965224f9e5, kmbe26ebbbb5e459bb3faba3a44b9ab50, kme6491020d484627a436db9517f7d294, km834657daf0a4f21b3110fc7e5484dc0);
    var km0ac019c4fd147b089b1eb3b60ceacb7 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km0ac019c4fd147b089b1eb3b60ceacb7.setDefaultUnit(kony.flex.DP);
    var km620aefa2be1482b905d52c60988be63 = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km620aefa2be1482b905d52c60988be63.setDefaultUnit(kony.flex.DP);
    var km0b035d415fc4c0094b4c4f3e4609ea0 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km0b035d415fc4c0094b4c4f3e4609ea0.setDefaultUnit(kony.flex.DP);
    var kma1b9aeb2cfa44a187b17e5820a9d57d = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km0b035d415fc4c0094b4c4f3e4609ea0.add(kma1b9aeb2cfa44a187b17e5820a9d57d);
    var km5831d3885f34619904bcc2ba229ab91 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    km5831d3885f34619904bcc2ba229ab91.setDefaultUnit(kony.flex.DP);
    var km6183a58f1d44a53b3a8b1f46fa0cf6c = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km5831d3885f34619904bcc2ba229ab91.add(km6183a58f1d44a53b3a8b1f46fa0cf6c);
    var km955af7421814e9bae9babb990d5eb1f = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km955af7421814e9bae9babb990d5eb1f.setDefaultUnit(kony.flex.DP);
    var km3b6e190059e4c118e9eed81f02706b1 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmbbbc6248a114088ab88e7f19599930a = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km955af7421814e9bae9babb990d5eb1f.add(km3b6e190059e4c118e9eed81f02706b1, kmbbbc6248a114088ab88e7f19599930a);
    var km2455759d39d46bba34768c38dafdd56 = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km620aefa2be1482b905d52c60988be63.add(km0b035d415fc4c0094b4c4f3e4609ea0, km5831d3885f34619904bcc2ba229ab91, km955af7421814e9bae9babb990d5eb1f, km2455759d39d46bba34768c38dafdd56);
    var km9d25d3fe0364712b02163ea48f7d40d = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km9d25d3fe0364712b02163ea48f7d40d.setDefaultUnit(kony.flex.DP);
    km9d25d3fe0364712b02163ea48f7d40d.add();
    km0ac019c4fd147b089b1eb3b60ceacb7.add(km620aefa2be1482b905d52c60988be63, km9d25d3fe0364712b02163ea48f7d40d);
    var km121487097104350a5e34e2260eceb01 = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km121487097104350a5e34e2260eceb01.setDefaultUnit(kony.flex.DP);
    var kmbc561f57ab14fd5b265036aa1b3dab7 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    kmbc561f57ab14fd5b265036aa1b3dab7.setDefaultUnit(kony.flex.DP);
    var kma62274424cd4286a8e1f91848e67ddd = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    kma62274424cd4286a8e1f91848e67ddd.setDefaultUnit(kony.flex.DP);
    var km686e35d67c74a3d8ffd9336dd939471 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmdc46fea88444c41a2022e7ab91d55cb = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kma62274424cd4286a8e1f91848e67ddd.add(km686e35d67c74a3d8ffd9336dd939471, kmdc46fea88444c41a2022e7ab91d55cb);
    var kma780ef6ad024e6d9d246614d6b6a5d8 = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kma780ef6ad024e6d9d246614d6b6a5d8.setDefaultUnit(kony.flex.DP);
    var km7ddc9f89f0c4342b93b9af21f2aa57e = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km7ddc9f89f0c4342b93b9af21f2aa57e.setDefaultUnit(kony.flex.DP);
    var kmb43c1ed8cee4912a6daadf432109828 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd48e37c475447648f118e663baeeb59 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km627ff52f62d406295e9579de76e3b73 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km7d3d21ec5c64bd9afdfc538a7eef288 = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km7bc0b29b6b843b78549304c48bbb8fb = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km32225ac23884d14a14315263cb2da17 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km7ddc9f89f0c4342b93b9af21f2aa57e.add(kmb43c1ed8cee4912a6daadf432109828, kmd48e37c475447648f118e663baeeb59, km627ff52f62d406295e9579de76e3b73, km7d3d21ec5c64bd9afdfc538a7eef288, km7bc0b29b6b843b78549304c48bbb8fb, km32225ac23884d14a14315263cb2da17);
    var km90bcdf133ce42a8a22e1d813eefb922 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km90bcdf133ce42a8a22e1d813eefb922.setDefaultUnit(kony.flex.DP);
    var km08a9104f86f4fa7a0c0f2941cd5547a = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3ed88042f344f548111f02c94772e70 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km8f4f822c02740cd8772af986c1ce443 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km90bcdf133ce42a8a22e1d813eefb922.add(km08a9104f86f4fa7a0c0f2941cd5547a, km3ed88042f344f548111f02c94772e70, km8f4f822c02740cd8772af986c1ce443);
    kma780ef6ad024e6d9d246614d6b6a5d8.add(km7ddc9f89f0c4342b93b9af21f2aa57e, km90bcdf133ce42a8a22e1d813eefb922);
    var km4d6cfe0e4cc44348cc47fb698828eb2 = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km219123f1c9e4a62ad3e6889ae9ed967 = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmbc561f57ab14fd5b265036aa1b3dab7.add(kma62274424cd4286a8e1f91848e67ddd, kma780ef6ad024e6d9d246614d6b6a5d8, km4d6cfe0e4cc44348cc47fb698828eb2, km219123f1c9e4a62ad3e6889ae9ed967);
    km121487097104350a5e34e2260eceb01.add(kmbc561f57ab14fd5b265036aa1b3dab7);
    km0b9532d4ba84094b6088b80c7cd5cae.add(kmaea38377a29430186221f9b23b6da8e, km0befc28099141daa7f6b029279f63c1, km0ac019c4fd147b089b1eb3b60ceacb7, km121487097104350a5e34e2260eceb01);
    var flxTransLayout = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxTransLayout",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 11
    }, {}, {});
    flxTransLayout.setDefaultUnit(kony.flex.DP);
    flxTransLayout.add();
    var kmea15d79c23e42bbaf331071f2b0e505 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 11,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kmea15d79c23e42bbaf331071f2b0e505.setDefaultUnit(kony.flex.DP);
    var km0a2dd92a9e94e44bf835d213a934e24 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    km0a2dd92a9e94e44bf835d213a934e24.setDefaultUnit(kony.flex.DP);
    var kmaabcf50d61e4dd7836e4f5f8e9db56d = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmaabcf50d61e4dd7836e4f5f8e9db56d.setDefaultUnit(kony.flex.DP);
    var km392ded7c777400981226fac0a08b386 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2bff18170c0404f96d7142125ad5b76 = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km2bff18170c0404f96d7142125ad5b76.setDefaultUnit(kony.flex.DP);
    var km3e6de56d1b1492baafdcad485d29939 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km0195d943445416ab921d4441864fed7 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd40e71601ad41189dca23a91546f9be = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km2bff18170c0404f96d7142125ad5b76.add(km3e6de56d1b1492baafdcad485d29939, km0195d943445416ab921d4441864fed7, kmd40e71601ad41189dca23a91546f9be);
    var km1543adafeec4281a6862a06cb8e1422 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km25e1591bf154bbd8cfc9998d1be0390 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmaabcf50d61e4dd7836e4f5f8e9db56d.add(km392ded7c777400981226fac0a08b386, km2bff18170c0404f96d7142125ad5b76, km1543adafeec4281a6862a06cb8e1422, km25e1591bf154bbd8cfc9998d1be0390);
    var km08e2bc5e8154f5aa8b2bed71d0809aa = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    km08e2bc5e8154f5aa8b2bed71d0809aa.setDefaultUnit(kony.flex.DP);
    var kmbc24915e845465badc1a0a152877cf4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbc24915e845465badc1a0a152877cf4.setDefaultUnit(kony.flex.DP);
    var km594675a61764e44aa1f59d37437cc8a = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmbc24915e845465badc1a0a152877cf4.add(km594675a61764e44aa1f59d37437cc8a);
    km08e2bc5e8154f5aa8b2bed71d0809aa.add(kmbc24915e845465badc1a0a152877cf4);
    var kmcc2294bf74a4ab4834f7c85d349e6df = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km0a2dd92a9e94e44bf835d213a934e24.add(kmaabcf50d61e4dd7836e4f5f8e9db56d, km08e2bc5e8154f5aa8b2bed71d0809aa, kmcc2294bf74a4ab4834f7c85d349e6df);
    kmea15d79c23e42bbaf331071f2b0e505.add(km0a2dd92a9e94e44bf835d213a934e24);
    var km23b065825b249d88f4068cb10186eb9 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km23b065825b249d88f4068cb10186eb9.setDefaultUnit(kony.flex.DP);
    var km116943d9b7e4a57ab29f8c7a2f1d8f2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    km116943d9b7e4a57ab29f8c7a2f1d8f2.setDefaultUnit(kony.flex.DP);
    var km9322bcfc8314ceb98d23e23f83643f4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km9322bcfc8314ceb98d23e23f83643f4.setDefaultUnit(kony.flex.DP);
    var km2d8c374ba6641de881459c1db1224e2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    km2d8c374ba6641de881459c1db1224e2.setDefaultUnit(kony.flex.DP);
    km2d8c374ba6641de881459c1db1224e2.add();
    var km8b1eeb68d344e8b97f21bdb76ff2c03 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km107e0248ef747ba8fbc21880815f7a0 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var kmd6fdfeb55154eb5ac164bb83f62db8b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd6fdfeb55154eb5ac164bb83f62db8b.setDefaultUnit(kony.flex.DP);
    var km5331c0486584ea499bd3e39a97f8bc5 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km780b13e20864905ba9e1425ef121426 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    kmd6fdfeb55154eb5ac164bb83f62db8b.add(km5331c0486584ea499bd3e39a97f8bc5, km780b13e20864905ba9e1425ef121426);
    var kma55470da67f414a8778461de693d936 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma55470da67f414a8778461de693d936.setDefaultUnit(kony.flex.DP);
    var km9e28b1c80e94f76b194607f581f5ce5 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km958cc60681c43fe88a9e6ce07652c1e = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    kma55470da67f414a8778461de693d936.add(km9e28b1c80e94f76b194607f581f5ce5, km958cc60681c43fe88a9e6ce07652c1e);
    var km3ebf036519141edac3afe6e311b6b15 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3ebf036519141edac3afe6e311b6b15.setDefaultUnit(kony.flex.DP);
    var km85263d2c9db453b9ba37421a056e395 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km1f784cc07774779882a9e85bae6ed6a = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    km3ebf036519141edac3afe6e311b6b15.add(km85263d2c9db453b9ba37421a056e395, km1f784cc07774779882a9e85bae6ed6a);
    var km319961aa2ae41d9bb83c978a6fd8fd9 = new kony.ui.Button({
        "centerY": "15dp",
        "height": "13.93%",
        "id": "btnClearVoiceSearch",
        "right": "9dp",
        "text": "X",
        "top": "0dp",
        "width": "8%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km9322bcfc8314ceb98d23e23f83643f4.add(km2d8c374ba6641de881459c1db1224e2, km8b1eeb68d344e8b97f21bdb76ff2c03, km107e0248ef747ba8fbc21880815f7a0, kmd6fdfeb55154eb5ac164bb83f62db8b, kma55470da67f414a8778461de693d936, km3ebf036519141edac3afe6e311b6b15, km319961aa2ae41d9bb83c978a6fd8fd9);
    km116943d9b7e4a57ab29f8c7a2f1d8f2.add(km9322bcfc8314ceb98d23e23f83643f4);
    km23b065825b249d88f4068cb10186eb9.add(km116943d9b7e4a57ab29f8c7a2f1d8f2);
    var km11a9977e05a41a695249866aafafd76 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km11a9977e05a41a695249866aafafd76.setDefaultUnit(kony.flex.DP);
    var kmb039490026a4e2682bd81d57e5d316b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb039490026a4e2682bd81d57e5d316b.setDefaultUnit(kony.flex.DP);
    var km80e7cd3fbde4964bed4cdc201cc19be = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km0ed99e22a934f63a516078aff5a5325 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmb039490026a4e2682bd81d57e5d316b.add(km80e7cd3fbde4964bed4cdc201cc19be, km0ed99e22a934f63a516078aff5a5325);
    var km02a5309e0c0406faab0632b15613851 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km02a5309e0c0406faab0632b15613851.setDefaultUnit(kony.flex.DP);
    var km8cf3171217c46b5bed16fd4f16a1282 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmf61eec6ba0c4ee2ad384e568c18a780 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    var kmf2b7d162f2d492290a6a2fcb6d28d11 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km02a5309e0c0406faab0632b15613851.add(km8cf3171217c46b5bed16fd4f16a1282, kmf61eec6ba0c4ee2ad384e568c18a780, kmf2b7d162f2d492290a6a2fcb6d28d11);
    var km3ab5c93c4d24d95a88315d2e4e84607 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3ab5c93c4d24d95a88315d2e4e84607.setDefaultUnit(kony.flex.DP);
    var kmb3417a2790c4e34927147c30bfb2de0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km006f8a68e284958985230149de889bf = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km3ab5c93c4d24d95a88315d2e4e84607.add(kmb3417a2790c4e34927147c30bfb2de0, km006f8a68e284958985230149de889bf);
    var km0ca3c8db11d4e7ba221bb61641b7801 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0ca3c8db11d4e7ba221bb61641b7801.setDefaultUnit(kony.flex.DP);
    var kmbc405bdb29441459f10291b7c458603 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km0281a3edc624f38bcc9fd7dc7981ee5 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km0ca3c8db11d4e7ba221bb61641b7801.add(kmbc405bdb29441459f10291b7c458603, km0281a3edc624f38bcc9fd7dc7981ee5);
    var km7f37fd75eb84f7cab9cce1529f304ec = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7f37fd75eb84f7cab9cce1529f304ec.setDefaultUnit(kony.flex.DP);
    var kmbf6e026825741e29a097e19e5c69d34 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km2aa39673a7a4026a4e510cfe3d5d221 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km7f37fd75eb84f7cab9cce1529f304ec.add(kmbf6e026825741e29a097e19e5c69d34, km2aa39673a7a4026a4e510cfe3d5d221);
    var km5114f1267cb415eab9e93c731f2017a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km5114f1267cb415eab9e93c731f2017a.setDefaultUnit(kony.flex.DP);
    var kmeebf236d7e145e2ba318b40a4f84013 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var kmd10a1cdc43143d1a5d9b90f67a309d9 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km5114f1267cb415eab9e93c731f2017a.add(kmeebf236d7e145e2ba318b40a4f84013, kmd10a1cdc43143d1a5d9b90f67a309d9);
    var kmd00757ed37c47ab9621a621f7cc0ac7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd00757ed37c47ab9621a621f7cc0ac7.setDefaultUnit(kony.flex.DP);
    var km42a14ce5a614f77a4e85fab433fdd5a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km4a0ee62b9dc47c1a49485311d21df8d = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    kmd00757ed37c47ab9621a621f7cc0ac7.add(km42a14ce5a614f77a4e85fab433fdd5a, km4a0ee62b9dc47c1a49485311d21df8d);
    var km7d6541f89e64795bec7a924edd54f3d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7d6541f89e64795bec7a924edd54f3d.setDefaultUnit(kony.flex.DP);
    var kma17d742a2d3461191b72ef18efcee6c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km3cc8b25ed674ea8b8c1c601fdeeba12 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {});
    km7d6541f89e64795bec7a924edd54f3d.add(kma17d742a2d3461191b72ef18efcee6c, km3cc8b25ed674ea8b8c1c601fdeeba12);
    km11a9977e05a41a695249866aafafd76.add(kmb039490026a4e2682bd81d57e5d316b, km02a5309e0c0406faab0632b15613851, km3ab5c93c4d24d95a88315d2e4e84607, km0ca3c8db11d4e7ba221bb61641b7801, km7f37fd75eb84f7cab9cce1529f304ec, km5114f1267cb415eab9e93c731f2017a, kmd00757ed37c47ab9621a621f7cc0ac7, km7d6541f89e64795bec7a924edd54f3d);
    var km8ee9b810e3d43dbb5714062f6325a83 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    km8ee9b810e3d43dbb5714062f6325a83.setDefaultUnit(kony.flex.DP);
    var km100adb3283c4dfcbcabd4ba5db3e5b4 = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmce4c7b0a11f4fee8cba3ab2e7aa5b7d = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    kmce4c7b0a11f4fee8cba3ab2e7aa5b7d.setDefaultUnit(kony.flex.DP);
    var km0688b56b28b4f7eb87ebd9ea034218b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km0688b56b28b4f7eb87ebd9ea034218b.setDefaultUnit(kony.flex.DP);
    var kmeb30b07231f4831970d7ac5d777025a = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km0688b56b28b4f7eb87ebd9ea034218b.add(kmeb30b07231f4831970d7ac5d777025a);
    var km12f084351a04df285d4c6485a800fcb = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km12f084351a04df285d4c6485a800fcb.setDefaultUnit(kony.flex.DP);
    var kmf85aa272c194d6784768a526e03183b = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km09e6635bb48401dac2e8940ee8a6280 = new kony.ui.Button({
        "centerY": "50%",
        "height": "100%",
        "id": "btnClearSearch",
        "right": "2%",
        "text": "X",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1,
        "focusSkin": "sknBtnMenuFoc",
        "isVisible": false,
        "skin": "sknBtnIconGraySmall"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km12f084351a04df285d4c6485a800fcb.add(kmf85aa272c194d6784768a526e03183b, km09e6635bb48401dac2e8940ee8a6280);
    var km4a4e424c0204dc0b59a467184f2be18 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km4a4e424c0204dc0b59a467184f2be18.setDefaultUnit(kony.flex.DP);
    var kmf14f695de914584811aad6d3a5c178c = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "enableOverlay": true,
        "enablePhotoCropFeature": false,
        "overlayConfig": {
            "captureButtonText": "",
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay,
            "tapAnywhere": false
        }
    });
    km4a4e424c0204dc0b59a467184f2be18.add(kmf14f695de914584811aad6d3a5c178c);
    var kmd7ffef8e4934fdeb37844e35ca29e68 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmd7ffef8e4934fdeb37844e35ca29e68.setDefaultUnit(kony.flex.DP);
    var km1d4bcc59f7c4302b56f3c828d1a0cc0 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmd7ffef8e4934fdeb37844e35ca29e68.add(km1d4bcc59f7c4302b56f3c828d1a0cc0);
    var kmf7a99cf68a6429d98d5eca54739cb04 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCameraImageSearch",
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "focusSkin": "sknBtnPopupCloseTransparentBg",
        "isVisible": false,
        "skin": "sknBtnPopupCloseTransparentBg"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km175210a4356423b8a68bba18a045a73 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km175210a4356423b8a68bba18a045a73.setDefaultUnit(kony.flex.DP);
    var kmcd484a1286248bdbbce4b1d0f6a8ba2 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km175210a4356423b8a68bba18a045a73.add(kmcd484a1286248bdbbce4b1d0f6a8ba2);
    kmce4c7b0a11f4fee8cba3ab2e7aa5b7d.add(km0688b56b28b4f7eb87ebd9ea034218b, km12f084351a04df285d4c6485a800fcb, km4a4e424c0204dc0b59a467184f2be18, kmd7ffef8e4934fdeb37844e35ca29e68, kmf7a99cf68a6429d98d5eca54739cb04, km175210a4356423b8a68bba18a045a73);
    var km0acd192c66d42e0abbfa8f5934e4941 = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km0c1544485234915a21094dae799ca6d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0c1544485234915a21094dae799ca6d.setDefaultUnit(kony.flex.DP);
    var kma7112e462914a76ae4fa7a9b5a6be12 = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var km7c2cbe5f83a489e8e86bd19702ee927 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km0c1544485234915a21094dae799ca6d.add(kma7112e462914a76ae4fa7a9b5a6be12, km7c2cbe5f83a489e8e86bd19702ee927);
    km8ee9b810e3d43dbb5714062f6325a83.add(km100adb3283c4dfcbcabd4ba5db3e5b4, kmce4c7b0a11f4fee8cba3ab2e7aa5b7d, km0acd192c66d42e0abbfa8f5934e4941, km0c1544485234915a21094dae799ca6d);
    frmProjectsCategory.add(flxMainContainer, km2d226418e4d497f95a20d8e8aeeeba5, km0b9532d4ba84094b6088b80c7cd5cae, flxTransLayout, kmea15d79c23e42bbaf331071f2b0e505, km23b065825b249d88f4068cb10186eb9, km11a9977e05a41a695249866aafafd76, km8ee9b810e3d43dbb5714062f6325a83);
};

function frmProjectsCategoryGlobals() {
    frmProjectsCategory = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmProjectsCategory,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmProjectsCategory",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};