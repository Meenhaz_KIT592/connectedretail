function initializetempStoreDetails() {
    flxStoreDetailsWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetailsWrap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreDetailsWrap.setDefaultUnit(kony.flex.DP);
    var flxStoreDetailsCollapse = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetailsCollapse",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxStoreDetailsCollapse.setDefaultUnit(kony.flex.DP);
    var btnFavorite = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxRed",
        "height": "75dp",
        "id": "btnFavorite",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxGray",
        "text": "F",
        "top": "0dp",
        "width": "60dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblTapToSelect = new kony.ui.Label({
        "centerX": "30dp",
        "height": "40dp",
        "id": "lblTapToSelect",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLbl8px66666",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxStoreDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "60dp",
        "isModalContainer": false,
        "right": "3%",
        "skin": "slFbox",
        "top": "10dp",
        "zIndex": 1
    }, {}, {});
    flxStoreDetails.setDefaultUnit(kony.flex.DP);
    var flexComingSoon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "5dp",
        "clipBounds": true,
        "focusSkin": "sknFlexBgWhiteFoc",
        "height": "20dp",
        "id": "flexComingSoon",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "160dp",
        "zIndex": 1
    }, {}, {});
    flexComingSoon.setDefaultUnit(kony.flex.DP);
    var flexComingSoonText = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknFlexRedNoBorderFoc",
        "height": "20dp",
        "id": "flexComingSoonText",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexRedNoBorder",
        "top": "0dp",
        "width": "150dp",
        "zIndex": 1
    }, {}, {});
    flexComingSoonText.setDefaultUnit(kony.flex.DP);
    var lblComingSoon = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblComingSoon",
        "isVisible": true,
        "skin": "sknLblGothamRegular24pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.comingSoon"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20dp",
        "id": "flexFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-10dp",
        "skin": "sknFlexBgWhite",
        "top": "-10dp",
        "width": "20dp",
        "zIndex": 1
    }, {}, {});
    flexFlag.setDefaultUnit(kony.flex.DP);
    flexFlag.add();
    flexComingSoonText.add(lblComingSoon, flexFlag);
    flexComingSoon.add(flexComingSoonText);
    var lblNameStore = new kony.ui.Label({
        "id": "lblNameStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 1,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddressLine1 = new kony.ui.Label({
        "id": "lblAddressLine1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 1,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddressLine2 = new kony.ui.Label({
        "id": "lblAddressLine2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPhone = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxRed",
        "id": "lblPhone",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTransReg24pxBlue",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxDirections = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "32dp",
        "id": "flxDirections",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDirections.setDefaultUnit(kony.flex.DP);
    var lblDistance = new kony.ui.Label({
        "height": "28dp",
        "id": "lblDistance",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnDirections = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "28dp",
        "id": "btnDirections",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknBtn13px489ca7GothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.btnDirections"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDirections.add(lblDistance, btnDirections);
    flxStoreDetails.add(flexComingSoon, lblNameStore, lblAddressLine1, lblAddressLine2, lblPhone, flxDirections);
    var flxStockInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "flxStockInfo",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "sknFlexBgLtGray",
        "top": "5dp",
        "width": "70dp",
        "zIndex": 1
    }, {}, {});
    flxStockInfo.setDefaultUnit(kony.flex.DP);
    var lblStockTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "35%",
        "id": "lblStockTitle",
        "isVisible": true,
        "skin": "sknLblGothamMedium24px818181",
        "text": "In Stock",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblStock = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblStock",
        "isVisible": true,
        "skin": "sknLblGothamMedium28px818181",
        "text": "5",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStockInfo.add(lblStockTitle, lblStock);
    var flxExpandDetailsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "45dp",
        "id": "flxExpandDetailsNew",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "10dp",
        "skin": "slFbox",
        "width": "80dp",
        "zIndex": 2
    }, {}, {});
    flxExpandDetailsNew.setDefaultUnit(kony.flex.DP);
    var lblStoreHoursNew = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblStoreHoursNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLbl9pxBlackGothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.lblStoreHoursSmall"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "65dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnExpand = new kony.ui.Label({
        "centerY": "50%",
        "id": "btnExpand",
        "isVisible": true,
        "right": 0,
        "skin": "sknLbl9pxBlack",
        "text": "D",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxExpandDetailsNew.add(lblStoreHoursNew, btnExpand);
    flxStoreDetailsCollapse.add(btnFavorite, lblTapToSelect, flxStoreDetails, flxStockInfo, flxExpandDetailsNew);
    var flxStoreDetailsExpand = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetailsExpand",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxStoreDetailsExpand.setDefaultUnit(kony.flex.DP);
    var flxStoreHorizontal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreHorizontal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxStoreHorizontal.setDefaultUnit(kony.flex.DP);
    var flxStoreTimings = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreTimings",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "60dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "60%"
    }, {}, {});
    flxStoreTimings.setDefaultUnit(kony.flex.DP);
    var lblStoreHours = new kony.ui.Label({
        "id": "lblStoreHours",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblStoreHours"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDay1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay1.setDefaultUnit(kony.flex.DP);
    var lblDay1 = new kony.ui.Label({
        "id": "lblDay1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay1"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay1Time = new kony.ui.Label({
        "id": "lblDay1Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay1.add(lblDay1, lblDay1Time);
    var flxDay2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay2.setDefaultUnit(kony.flex.DP);
    var lblDay2 = new kony.ui.Label({
        "id": "lblDay2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay2"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay2Time = new kony.ui.Label({
        "id": "lblDay2Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay2.add(lblDay2, lblDay2Time);
    var flxDay3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay3",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay3.setDefaultUnit(kony.flex.DP);
    var lblDay3 = new kony.ui.Label({
        "id": "lblDay3",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay3"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay3Time = new kony.ui.Label({
        "id": "lblDay3Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay3.add(lblDay3, lblDay3Time);
    var flxDay4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay4",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay4.setDefaultUnit(kony.flex.DP);
    var lblDay4 = new kony.ui.Label({
        "id": "lblDay4",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay4"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay4Time = new kony.ui.Label({
        "id": "lblDay4Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay4.add(lblDay4, lblDay4Time);
    var flxDay5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay5",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay5.setDefaultUnit(kony.flex.DP);
    var lblDay5 = new kony.ui.Label({
        "id": "lblDay5",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay5"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay5Time = new kony.ui.Label({
        "id": "lblDay5Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay5.add(lblDay5, lblDay5Time);
    var flxDay6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay6",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay6.setDefaultUnit(kony.flex.DP);
    var lblDay6 = new kony.ui.Label({
        "id": "lblDay6",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay6"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay6Time = new kony.ui.Label({
        "id": "lblDay6Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay6.add(lblDay6, lblDay6Time);
    var flxDay7 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay7",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay7.setDefaultUnit(kony.flex.DP);
    var lblDay7 = new kony.ui.Label({
        "id": "lblDay7",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay7"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDay7Time = new kony.ui.Label({
        "id": "lblDay7Time",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDay7.add(lblDay7, lblDay7Time);
    var lblGap = new kony.ui.Label({
        "height": "10dp",
        "id": "lblGap",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStoreTimings.add(lblStoreHours, flxDay1, flxDay2, flxDay3, flxDay4, flxDay5, flxDay6, flxDay7, lblGap);
    var btnCollapse = new kony.ui.Button({
        "bottom": "0dp",
        "focusSkin": "sknBtnIcon36pxDark",
        "height": "60dp",
        "id": "btnCollapse",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIcon36pxDark",
        "text": "U",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStoreHorizontal.add(flxStoreTimings, btnCollapse);
    var flxItemLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxItemLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemLocation.setDefaultUnit(kony.flex.DP);
    var lblMapIcon = new kony.ui.Label({
        "height": "100%",
        "id": "lblMapIcon",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon34pxBlue",
        "text": "e",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnStoreMap = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnStoreMap",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 12, 0],
        "paddingInPixel": false
    }, {});
    var flxSeparatorTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxSeparatorTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSeparatorTop.setDefaultUnit(kony.flex.DP);
    flxSeparatorTop.add();
    flxItemLocation.add(lblMapIcon, btnStoreMap, flxSeparatorTop);
    flxStoreDetailsExpand.add(flxStoreHorizontal, flxItemLocation);
    var flxSeparator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxSeparator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxMedGrayBG",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSeparator.setDefaultUnit(kony.flex.DP);
    flxSeparator.add();
    flxStoreDetailsWrap.add(flxStoreDetailsCollapse, flxStoreDetailsExpand, flxSeparator);
}