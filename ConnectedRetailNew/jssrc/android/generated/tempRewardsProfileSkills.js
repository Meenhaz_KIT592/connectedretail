function initializetempRewardsProfileSkills() {
    flexSkillsOuter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "focusSkin": "slFbox",
        "id": "flexSkillsOuter",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flexSkillsOuter.setDefaultUnit(kony.flex.DP);
    var skillName = new kony.ui.Label({
        "centerX": "50%",
        "id": "skillName",
        "isVisible": true,
        "skin": "sknLblGothamMedium28px818181",
        "text": "JEWELRY",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flexSliderOuter = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "30dp",
        "id": "flexSliderOuter",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 10
    }, {}, {});
    flexSliderOuter.setDefaultUnit(kony.flex.DP);
    var flexSliderLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "2dp",
        "id": "flexSliderLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBg979797",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexSliderLine.setDefaultUnit(kony.flex.DP);
    flexSliderLine.add();
    var flexSlider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "6dp",
        "id": "flexSlider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBg979797",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    flexSlider1.setDefaultUnit(kony.flex.DP);
    flexSlider1.add();
    var btn1 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "22dp",
        "id": "btn1",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBtnTransNoText",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexSlider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "6dp",
        "id": "flexSlider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "20%",
        "isModalContainer": false,
        "skin": "sknFlexBg979797",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    flexSlider2.setDefaultUnit(kony.flex.DP);
    flexSlider2.add();
    var btn2 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "22dp",
        "id": "btn2",
        "isVisible": true,
        "left": "18%",
        "skin": "sknBtnTransNoText",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexSlider3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "6dp",
        "id": "flexSlider3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "40%",
        "isModalContainer": false,
        "skin": "sknFlexBg979797",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    flexSlider3.setDefaultUnit(kony.flex.DP);
    flexSlider3.add();
    var btn3 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "22dp",
        "id": "btn3",
        "isVisible": true,
        "left": "38%",
        "skin": "sknBtnTransNoText",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexSlider4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "6dp",
        "id": "flexSlider4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "60%",
        "isModalContainer": false,
        "skin": "sknFlexBg979797",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    flexSlider4.setDefaultUnit(kony.flex.DP);
    flexSlider4.add();
    var btn4 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "22dp",
        "id": "btn4",
        "isVisible": true,
        "left": "58%",
        "skin": "sknBtnTransNoText",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexSlider5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "6dp",
        "id": "flexSlider5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "80%",
        "isModalContainer": false,
        "skin": "sknFlexBg979797",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    flexSlider5.setDefaultUnit(kony.flex.DP);
    flexSlider5.add();
    var btn5 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "22dp",
        "id": "btn5",
        "isVisible": true,
        "left": "78%",
        "skin": "sknBtnTransNoText",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexSlider6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "6dp",
        "id": "flexSlider6",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlexBg979797",
        "width": "6dp",
        "zIndex": 1
    }, {}, {});
    flexSlider6.setDefaultUnit(kony.flex.DP);
    flexSlider6.add();
    var btn6 = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "25dp",
        "id": "btn6",
        "isVisible": true,
        "left": "92%",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexSliderSelector = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "flexSliderSelector",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    flexSliderSelector.setDefaultUnit(kony.flex.DP);
    var sliderSelectorInner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "11dp",
        "id": "sliderSelectorInner",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "CopyslFbox07c2c8d46c4924d",
        "width": "11dp",
        "zIndex": 1
    }, {}, {});
    sliderSelectorInner.setDefaultUnit(kony.flex.DP);
    sliderSelectorInner.add();
    flexSliderSelector.add(sliderSelectorInner);
    var flexSliderSelector2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "30dp",
        "id": "flexSliderSelector2",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "slFbox",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    flexSliderSelector2.setDefaultUnit(kony.flex.DP);
    var sliderSelectorInner2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "11dp",
        "id": "sliderSelectorInner2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "CopyslFbox07c2c8d46c4924d",
        "width": "11dp",
        "zIndex": 1
    }, {}, {});
    sliderSelectorInner2.setDefaultUnit(kony.flex.DP);
    sliderSelectorInner2.add();
    flexSliderSelector2.add(sliderSelectorInner2);
    flexSliderOuter.add(flexSliderLine, flexSlider1, btn1, flexSlider2, btn2, flexSlider3, btn3, flexSlider4, btn4, flexSlider5, btn5, flexSlider6, btn6, flexSliderSelector, flexSliderSelector2);
    var skillMapValue = new kony.ui.Label({
        "centerX": "50%",
        "id": "skillMapValue",
        "isVisible": true,
        "skin": "sknLblGothamRegular24px515151",
        "text": "INTERMEDIATE",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexSkillsOuter.add(skillName, flexSliderOuter, skillMapValue);
}