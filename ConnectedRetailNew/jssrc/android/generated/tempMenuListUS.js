function initializetempMenuListUS() {
    flexMenuListContainerUS = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "flexMenuListContainerUS",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    flexMenuListContainerUS.setDefaultUnit(kony.flex.DP);
    var lblIconMenuList = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "lblIconMenuList",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIconMoreMenuBlack",
        "text": "A",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMenuListItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMenuListItem",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamMedium32pxGray",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexMenuListContainerUS.add(lblIconMenuList, lblMenuListItem);
}