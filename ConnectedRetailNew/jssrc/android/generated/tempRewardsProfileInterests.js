function initializetempRewardsProfileInterests() {
    flxRewardsProfileInterest = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxRewardsProfileInterest",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxRewardsProfileInterest.setDefaultUnit(kony.flex.DP);
    var flxRewardsInterest = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "93%",
        "id": "flxRewardsInterest",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRewardsInterest.setDefaultUnit(kony.flex.DP);
    flxRewardsInterest.add();
    var lblInterest = new kony.ui.Label({
        "height": "99%",
        "id": "lblInterest",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "BABY/TODDLER/PRE-SCHOOL",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSelect = new kony.ui.Image2({
        "height": "100%",
        "id": "imgSelect",
        "isVisible": false,
        "right": "0dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSelect = new kony.ui.Label({
        "height": "99%",
        "id": "lblSelect",
        "isVisible": false,
        "right": "10dp",
        "skin": "sknLblIcon34px999999",
        "text": "u",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0e885e995a5cc4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "FlexContainer0e885e995a5cc4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgE0E0E0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0e885e995a5cc4e.setDefaultUnit(kony.flex.DP);
    FlexContainer0e885e995a5cc4e.add();
    flxRewardsProfileInterest.add(flxRewardsInterest, lblInterest, imgSelect, lblSelect, FlexContainer0e885e995a5cc4e);
}