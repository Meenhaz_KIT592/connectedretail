function initializetempHazardousPDP() {
    FlexContainer077c1e8f5d52649 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer077c1e8f5d52649",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    FlexContainer077c1e8f5d52649.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer08bef204c42384d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20dp",
        "id": "CopyFlexContainer08bef204c42384d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%"
    }, {}, {});
    CopyFlexContainer08bef204c42384d.setDefaultUnit(kony.flex.DP);
    var warningImg = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "warningImg",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "20dp",
        "skin": "slImage",
        "src": "warning.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var warningText = new kony.ui.Label({
        "centerY": "50%",
        "height": "16dp",
        "id": "warningText",
        "isVisible": true,
        "left": "50dp",
        "skin": "skn28pxGothamBoldGrey",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.warning"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer08bef204c42384d.add(warningImg, warningText);
    var lblWarningDesc = new kony.ui.Label({
        "id": "lblWarningDesc",
        "isVisible": true,
        "left": "50dp",
        "right": "51dp",
        "skin": "GothamBookgreyBrown24",
        "text": "sjkfs jsdfhskjfhsfhsfhsdkddfjdsfdsfkjsfjdfdfhsjkfdhdkfsjfhsjfhsfjkshfsjfhskfjshfkjdshfjksfskfdshfkjsdfhskdjdfsk",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer077c1e8f5d52649.add(CopyFlexContainer08bef204c42384d, lblWarningDesc);
}