/**
 * PUBLIC
 * This is the view for the Pinch and Zoom form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.PinchandZoomView = (function() {
    var totalImagesGroup = [];
    var fromPDPFlag = true;
    /**
     * PUBLIC
     * Open the Scan form
     */
    function show() {
        MVCApp.Toolbox.common.setTransition(frmPinchandZoom, 3);
        frmPinchandZoom.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmPinchandZoom.btnClose.onClick = function() {
            if (fromPDPFlag) {
                MVCApp.Toolbox.common.setTransition(frmPDP, 2);
                frmPDP.show();
            } else {
                MVCApp.Toolbox.common.setTransition(frmPJDP, 2);
                frmPJDP.show();
            }
        };
        frmPinchandZoom.postShow = addGestureHandler;
    }

    function addGestureHandler() //This function registers the Gesture events for the widgets.
    {
        try {
            frmPinchandZoom.flxAndroid.addGestureRecognizer(6, {
                fingers: 2,
                continuousEvents: true
            }, onGestureFunction);
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Product and Project Image Zoom";
            lTealiumTagObj.page_name = "Product and Project Image Zoom";
            lTealiumTagObj.page_type = "Image Zoom";
            lTealiumTagObj.page_category_name = "Image Zoom";
            lTealiumTagObj.page_category = "Image Zoom";
            TealiumManager.trackView("Product and Project Image Zoom Screen", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            if (fromPDPFlag) {
                cust360Obj.productid = MVCApp.getPDPController().getProductIDForTracking();
                MVCApp.Customer360.sendInteractionEvent("ZoomProduct", cust360Obj);
            } else {
                cust360Obj.projectid = MVCApp.getPJDPController().getProjectIDForTracking();
                MVCApp.Customer360.sendInteractionEvent("ZoomProject", cust360Obj);
            }
        } catch (e) {
            kony.print("error while regestering the gestures" + e);
            if (e) {
                TealiumManager.trackEvent("Gesture Handler Exception in Pinch & Zoom Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function onGestureFunction(commonWidget, gestureInfo) // The callback function when the gesture event is triggered.
    {
        try {
            var GesType = "" + gestureInfo.gestureType;
            var tapParams = gestureInfo.gesturesetUpParams.taps;
            if (GesType == "6") // pinch gesture
            {
                var transformScale = kony.ui.makeAffineTransform();
                transformScale.scale(gestureInfo["scale"], gestureInfo["scale"]);
                frmPinchandZoom.imageAndroid.transform = transformScale;
                frmPinchandZoom.forceLayout();
            }
        } catch (e) {
            alert("error in gesture call back" + e);
            if (e) {
                TealiumManager.trackEvent("Gesture Exception in Pinch & Zoom Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function updateScreen(imagesGroup, fromPDP, selImgIndex) {
        fromPDPFlag = fromPDP;
        totalImagesGroup = imagesGroup;
        var defaultImage = imagesGroup.zoomImages || [];
        var selectImage = "";
        if (defaultImage.length > 0) {
            defaultImage = defaultImage[0] || [];
            if (defaultImage.length > 0) {
                var selImage = defaultImage[selImgIndex] || "";
                selectImage = selImage.link || "";
            }
        }
        if (kony.os.deviceInfo().name === "android") {
            if (defaultImage.length == 0) {
                frmPinchandZoom.zoomImageAndroid.imageURL = "notavailable_1012x628.png";
            } else {
                frmPinchandZoom.zoomImageAndroid.imageURL = selectImage;
            }
        }
        if (kony.os.deviceInfo().name === "iPhone") {
            frmPinchandZoom.zoomImageIOS.imageUrl = selectImage;
        }
        //show Images
        frmPinchandZoom.flxThumbs.removeAll();
        var smallImagesGroup = imagesGroup.smallImages || [];
        var smallImages = [];
        if (smallImagesGroup.length > 0) {
            smallImages = smallImagesGroup[0];
        }
        if (smallImages.length > 7) {
            frmPinchandZoom.flxThumbs.centerX = null;
            frmPinchandZoom.flxThumbs.left = "5dp";
        } else {
            frmPinchandZoom.flxThumbs.centerX = "50%";
        }
        frmPinchandZoom.flxThumbs.width = smallImages.length * 48;
        if (smallImages.length > 1) {
            loadSmallImages(smallImages, selImgIndex);
        }
        show();
    }

    function loadSmallImages(smallImages, selImgIndex) {
        var selectedImage = "";
        if (selImgIndex !== "" & selImgIndex !== "") {
            selectedImage = parseInt(selImgIndex);
        }
        for (var i = 0; i < smallImages.length; i++) {
            var defaultSelectedSkin = "sknFlexBgBorderGray";
            if (i === selectedImage) {
                defaultSelectedSkin = "sknFlxBlackBorder2px"; //"sknFlxBlackBorder2px";
            } else {
                defaultSelectedSkin = "sknFlexBgBorderGray"; //"sknFlexBgTransBorderLtGray"; 
            }
            var FlexContainer067018dc9d32f45 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "34dp",
                "id": "FlexContainer067018dc9d32f45" + i,
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "skin": defaultSelectedSkin,
                "width": "34dp",
                "onClick": onChangeProductImage,
                "zIndex": 1
            }, {}, {});
            FlexContainer067018dc9d32f45.setDefaultUnit(kony.flex.DP);
            var Image0fe184711e4e043 = new kony.ui.Image2({
                "height": "100%",
                "id": "Image0fe184711e4e043" + i,
                "imageWhenFailed": "notavailable_1012x628",
                "imageWhileDownloading": "white_1012x628",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": smallImages[i].link,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer067018dc9d32f45.add(Image0fe184711e4e043);
            frmPinchandZoom.flxThumbs.add(FlexContainer067018dc9d32f45);
        }
    }

    function onChangeProductImage() {
        if (this.skin != "sknFlxBlackBorder2px") {
            var imagesGroup = totalImagesGroup;
            var smallImages = imagesGroup.smallImages;
            if (smallImages.length > 0) {
                smallImages = smallImages[0];
            }
            for (var i = 0; i < smallImages.length; i++) {
                frmPinchandZoom["FlexContainer067018dc9d32f45" + i].skin = "sknFlexBgBorderGray"; // "sknFlexBgTransBorderLtGray";
            }
            this.skin = "sknFlxBlackBorder2px";
            var selectedIndex = this.id.split("FlexContainer067018dc9d32f45")[1];
            var defaultZoomImageGroup = imagesGroup.zoomImages || [];
            var defaultImage = [];
            if (defaultZoomImageGroup.length > 0) {
                defaultImage = defaultZoomImageGroup[0];
            }
            var selZoomImage = "";
            if (defaultImage.length > 0) {
                var selZoomImageTemp = defaultImage[selectedIndex] || "";
                selZoomImage = selZoomImageTemp.link || "";
            }
            if (kony.os.deviceInfo().name === "android") {
                if (selZoomImage === "") {
                    frmPinchandZoom.zoomImageAndroid.imageURL = "notavailable_1012x628.png";
                } else {
                    frmPinchandZoom.zoomImageAndroid.imageURL = selZoomImage;
                }
            } else {
                frmPinchandZoom.zoomImageIOS.imageUrl = selZoomImage;
            }
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});