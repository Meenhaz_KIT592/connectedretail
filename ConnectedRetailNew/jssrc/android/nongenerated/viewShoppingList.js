var MVCApp = MVCApp || {};
var gswipeFlag = false;
MVCApp.ShoppingListView = (function() {
    var widgetData = {
        flxShoppingListContainer: "flxShoppingListContainer",
        flxShoppingListInner: "flxShoppingListInner",
        flxShoppingListContents: "flxShoppingListContents",
        imgProduct: "imgProduct",
        flxStockAisleInfo: "flxStockAisleInfo",
        lblProductName: "lblProductName",
        lblStockInfo: "lblStockInfo",
        flxAisleInfo: "flxAisleInfo",
        btnDelete: "btnDelete",
        flxSeparator: "flxSeparator",
        lblAisle: "lblAisle",
        lblAisleNo: "lblAisleNo",
        flxPpoducts: "flxPpoducts",
        lblShoppingListHdr: "lblShoppingListHdr",
        btnExpand: "btnExpand",
        flexMyListHeader: "flexMyListHeader"
    };
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    //  frmShoppingList.flxHeaderWrap.btnCouponMain.isVisible = false;
    //  frmShoppingList.flxHeaderWrap.imgCouponIcon.isVisible = false; 
    function defaultShoppingList() {
        frmShoppingList.flxHeaderWrap.textSearch.text = "";
        frmShoppingList.flxSearchResultsContainer.setVisibility(false);
        frmShoppingList.flxHeaderWrap.btnClearSearch.setVisibility(false);
    }

    function bindEvents() {
        frmShoppingList.preShow = function() {
            frmShoppingList.flxSearchOverlay.setVisibility(false);
            frmShoppingList.flxVoiceSearch.setVisibility(false);
            swipeInit();
        };
        frmShoppingList.btnGotIt.onClick = onClickBtnGotIt;
        frmShoppingList.btnDeleteAll.onClick = deleteAllFromProductList;
        MVCApp.searchBar.bindEvents(frmShoppingList, "frmShoppingList");
        MVCApp.tabbar.bindEvents(frmShoppingList);
        MVCApp.tabbar.bindIcons(frmShoppingList, "frmShoppingList");
        frmShoppingList.segShoppingList.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
        frmShoppingList.segShoppingList.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
        frmShoppingList.postShow = function() {
            var cartValue = MVCApp.Toolbox.common.getCartItemValue();
            frmShoppingList.flxHeaderWrap.lblItemsCount.text = cartValue;
            frmShoppingList.flxSearchOverlay.setVisibility(false);
            MVCApp.Toolbox.common.setBrightness("frmShoppingList");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "My List: Michaels Mobile App";
            lTealiumTagObj.page_name = "My List: Michaels Mobile App";
            lTealiumTagObj.page_type = "Navigation";
            lTealiumTagObj.page_category_name = "My List";
            lTealiumTagObj.page_category = "My List";
            TealiumManager.trackView("My List Tab", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            MVCApp.Customer360.sendInteractionEvent("MyList", cust360Obj);
        };
        frmShoppingList.onDeviceBack = function() {
            if (voiceSearchFlag) {
                onXButtonClick(voiceSearchForm);
                if (voiceSearchForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")) {
                    voiceSearchFlag = false;
                    voiceSearchForm.flxVoiceSearch.setVisibility(false);
                } else {
                    voiceSearchFlag = true;
                    tapToCancelFlag = true;
                    deviceBackFlag = true;
                }
                MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
            }
            if (kony.application.getPreviousForm().id == "frmHome") {
                MVCApp.getHomeController().load();
            }
        };
    }

    function onClickBtnGotIt() {
        kony.store.setItem("initshoppinglistpage", true);
        frmShoppingList.flxOverlay.isVisible = false;
    }

    function swipeInit() {
        var swipgesture = frmShoppingList.segShoppingList.rowTemplate.flxShoppingListInner.setGestureRecognizer(2, {
            fingers: 1,
            swipedistance: 50,
            swipevelocity: 50
        }, onGesture);
    }

    function onGesture(widgetRef, gestureInfo, context) {
        if (gestureInfo.swipeDirection == 1) {
            frmShoppingList.segShoppingList.onTouchStart = tempAction;
            frmShoppingList.segShoppingList.onTouchEnd = tempAction;
        } else {
            frmShoppingList.segShoppingList.onTouchStart = function(eventObj, x, y) {
                checkonTouchStart(eventObj, x, y);
            };
            frmShoppingList.segShoppingList.onTouchEnd = function(eventObj, x, y) {
                checkonTouchEnd(eventObj, x, y);
            };
        }
        kony.print(" widgetRef - " + JSON.stringify(widgetRef));
        kony.print(" gestureInfo - " + JSON.stringify(gestureInfo));
        kony.print(" context - " + JSON.stringify(context));
        if (gestureInfo.swipeDirection == 1) {
            widgetRef.animate(kony.ui.createAnimation({
                "100": {
                    "left": "-30%",
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    }
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            });
        } else if (gestureInfo.swipeDirection == 2) {
            widgetRef.animate(kony.ui.createAnimation({
                "100": {
                    "left": "0%",
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    }
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            });
        }
    }

    function show() {
        frmShoppingList.flxHeaderWrap.textSearch.text = "";
        frmShoppingList.flxSearchResultsContainer.setVisibility(false);
        frmShoppingList.flxHeaderWrap.btnClearSearch.setVisibility(false);
        frmShoppingList.forceLayout();
        MVCApp.Toolbox.common.setTransition(frmShoppingList, 0);
        var fromBack = MVCApp.getShoppingListController().checkFromBack();
        if (fromBack) {
            frmShoppingList.show();
        }
        frmShoppingList.defaultAnimationEnabled = false;
        //frmShoppingList.show();
    }

    function createProductList(results) {
        results = results || [];
        if (results.length > 0) {
            var type = results.type || "";
            var id = results.id || "";
            if (id !== "" && type == MVCApp.Service.Constants.ShoppingList.wishList) {
                kony.print("product list is created");
            } else {
                kony.print("product list is not created");
            }
        } else {
            kony.print("product list is not created");
        }
    }

    function getProductListItems(shopList) {
        shopList = shopList || [];
        //frmShoppingList.segShoppingList.removeAll();
        var shopListData = shopList.getDataForShoppingList();
        kony.print(" view data " + JSON.stringify(shopListData));
        if (shopListData.length > 0) {
            if (!kony.store.getItem("initshoppinglistpage")) {
                frmShoppingList.flxOverlay.isVisible = true;
                frmShoppingList.lblPopUpMessage.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.MylistDeletePrompt");
                frmShoppingList.btnGotIt.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageOverlay.popUpButtonText");
                frmShoppingList.flxOverlay.forceLayout();
            }
            frmShoppingList.segShoppingList.widgetDataMap = widgetData;
            var tempData = [];
            var delectedFromSectionIndex = 0;
            for (var k = 0; k < shopListData.length; k++) {
                var temp = [];
                if (k === 0) {
                    var e = shopListData[k][0];
                    e.btnExpand = {
                        text: "U",
                        onClick: shopList.expandSegment,
                        info: {
                            index: k,
                            status: false
                        }
                    }; // D,true
                    temp.push(e);
                    temp.push(shopListData[k][1]);
                    tempData.push(temp);
                } else {
                    var d = shopListData[k][0];
                    var dProjectData = shopListData[k][1] || "";
                    var objProjectData = dProjectData[0] || "";
                    var dProjectId = objProjectData.projectId || "";
                    if (projectIdDeleted && dProjectId === projectIdDeleted) {
                        d.btnExpand = {
                            text: "U",
                            onClick: shopList.expandSegment,
                            info: {
                                index: k,
                                status: true
                            }
                        }; // D,true
                        temp.push(d);
                        temp.push(dProjectData);
                        tempData.push(temp);
                        delectedFromSectionIndex = k;
                    } else {
                        d.btnExpand = {
                            text: "D",
                            onClick: shopList.expandSegment,
                            info: {
                                index: k,
                                status: false
                            }
                        }; // D,true
                        temp.push(d);
                        temp.push([]);
                        tempData.push(temp);
                    }
                }
            }
            frmShoppingList.segShoppingList.setData(tempData);
            if (projectIdDeleted && projectIdDeleted != "") {
                kony.print("list is highlighted at " + delectedFromSectionIndex);
                if (delectedFromSectionIndex === 0) {
                    frmShoppingList.segShoppingList.selectedRowIndex = null;
                } else {
                    frmShoppingList.segShoppingList.selectedRowIndex = [delectedFromSectionIndex, 0];
                }
            }
            frmShoppingList.segShoppingList.setVisibility(true);
            frmShoppingList.flxSubHeader.setVisibility(true);
            frmShoppingList.lblNoList.setVisibility(false);
            projectIdDeleted = null;
        } else {
            frmShoppingList.segShoppingList.setVisibility(false);
            frmShoppingList.flxSubHeader.setVisibility(false);
            frmShoppingList.lblNoList.setVisibility(true);
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        show();
    }

    function deleteAllFromProductList() {
        MVCApp.getShoppingListController().confirmationAlert();
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
            frmShoppingList.segShoppingList.bottom = "0dp";
        } else {
            MVCApp.Toolbox.common.dispFooter();
            frmShoppingList.segShoppingList.bottom = "54dp";
        }
        if (checkScroll == 0) {
            MVCApp.Toolbox.common.hideFooter();
            frmShoppingList.segShoppingList.bottom = "0dp";
        }
    }

    function tempAction(eventObj) {
        kony.print("dummy action");
    }
    return {
        bindEvents: bindEvents,
        createProductList: createProductList,
        getProductListItems: getProductListItems,
        show: show,
        deleteAllFromProductList: deleteAllFromProductList,
        defaultShoppingList: defaultShoppingList
    };
});