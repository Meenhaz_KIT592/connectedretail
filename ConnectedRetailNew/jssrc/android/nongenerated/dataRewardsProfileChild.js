var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.RewardsProfileChild = (function(json) {
    var typeOf = "RewardsProfileChild";
    var _data = json || {};

    function onChildTextChange() {
        var data = frmRewardsProfileAddChild.segChildAgeLevel.data;
        for (var i = 0; i < data.length; i++) {
            var originalData = [];
            if (data[i].txtChild != undefined && data[i].txtChild != "") {
                frmRewardsProfileAddChild.btnNextToRewardsInterests.text = "NEXT";
            } else frmRewardsProfileAddChild.btnNextToRewardsInterests.text = "SKIP";
        }
    }

    function getRewardsProfileChildData() {
        try {
            var rewardsProfile = _data.results;
            if (rewardsProfile != "" && rewardsProfile != null && rewardsProfile != undefined) {
                rewardsProfile = JSON.parse(rewardsProfile);
                kony.print("Rewards Profile Child:" + rewardsProfile);
                rewardsProfile = rewardsProfile.c_RewardsProfile;
                var childNames = [];
                if (rewardsProfile != "" && rewardsProfile != null) {
                    var rewardsChild = rewardsProfile.children;
                    if (rewardsChild != "" && rewardsChild != null) {
                        var questions = [];
                        questions = rewardsChild.questions;
                        for (var i = 0; i < questions.length; i++) {
                            var temp1 = [];
                            temp1.value = questions[i].value;
                            temp1.lblChild = questions[i].displayValue;
                            temp1.txtChild = {
                                onTextChange: onChildTextChange
                            };
                            childNames.push(temp1);
                        }
                    }
                }
                return childNames;
            }
        } catch (e) {
            kony.print("Exception in getRewardsProfileChildData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Rewards Profile Child Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    return {
        typeOf: typeOf,
        getRewardsProfileChildData: getRewardsProfileChildData
    };
});