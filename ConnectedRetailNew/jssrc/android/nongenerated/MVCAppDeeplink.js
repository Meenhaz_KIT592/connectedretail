var weeklyAdId = "";
var weeklyAdFormID = "";
var gblLaunchMode = 0;
var firstLaunchFromPrevKonyVersionFlag = "";
var gblIsFromImageSearch = false;
var gblFromHomeTile = false;
var formFromDeeplink = false;
var deeplinkEnabled = false;
var frombrowserdeeplink = false;
var dealId = "";

function appservicereq(params) {
    kony.print("App service request triggred");
    kony.print(JSON.stringify(params));
    var test = params;
    var lparams = test.launchparams;
    gblLaunchMode = params.launchmode;
    if (params.launchmode == 3) {
        deeplinkEnabled = true;
        var currentForm = kony.application.getCurrentForm() || "";
        var currentFormID = currentForm.id || "";
        var lMyLocationFlag = false;
        if (params.launchparams.formToOpen == "frmRewardsProgramInfo") {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.rewardTermsandConditions);
        }
        if (params.launchparams.formToOpen == "frmPrivacyPolicy") {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
        }
        if (params.launchparams.formToOpen == "frmHelp") {
            MVCApp.getHelpDetailsController().load();
        }
        if (params.launchparams.formToOpen == "frmTermsConditions") {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.createTermsandConditions);
        }
        if (params.launchparams.formToOpen == "frmSignIn") {
            rewardsInfoFlag = false;
            isFromShoppingList = false;
            MVCApp.getSignInController().load();
        }
        if (params.launchparams.formToOpen == "frmCreateAccount") {
            fromCoupons = false;
            MVCApp.getCreateAccountController().load();
        }
        if (params.launchparams.formToOpen == "frmWeeklyAds") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            frombrowserdeeplink = true;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            if (fromBrowserViews) {
                MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(true);
            }
            if (lMyLocationFlag === "true") {
                loadWeeklyAds = true;
                frmWeeklyAdHome.segAds.removeAll();
                frmWeeklyAdHome.defaultAnimationEnabled = false;
                weeklyAdEntryFromVoiceSearch = false;
                MVCApp.getWeeklyAdHomeController().load();
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "weeklyAds"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmPJDP") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            frombrowserdeeplink = true;
            var formName = kony.application.getCurrentForm().id;
            MVCApp.Toolbox.common.clearPdpGlobalVariables();
            MVCApp.getPJDPController().setQueryString("");
            MVCApp.getPJDPController().setEntryType("deeplink");
            MVCApp.getPJDPController().load(params.launchparams.productId, false, formName);
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmCoupons") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            couponsEntryFromVoiceSearch = false;
            if (params.launchparams.fromHomeTile) {
                gblFromHomeTile = true;
            } else {
                gblFromHomeTile = false;
            }
            frmObject = frmHome;
            MVCApp.getHomeController().loadCoupons();
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmPDP") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            frombrowserdeeplink = true;
            var headerText = [];
            headerText[0] = "";
            headerText[1] = "";
            MVCApp.getPDPController().setQueryString("");
            var productType = params.launchparams.product_type || "";
            var productId = params.launchparams.productId || "";
            var defaultProductId = params.launchparams.c_default_product_id || "";
            var sessionId = params.launchparams.sessionId || "";
            var invokeForm = "frmHome";
            var currForm = kony.application.getCurrentForm();
            if (currForm != null && currForm != undefined) {
                var currFormId = currForm.id || "";
                if (currFormId == "frmChatBox") {
                    MVCApp.getChatBoxController().setSession(sessionId);
                    invokeForm = "frmChatBox";
                    MVCApp.getPDPController().setFromBarcode(true);
                }
            }
            if (productType == "item") {
                if (productId !== "" || productId != "null" || productId !== null) {
                    MVCApp.getPDPController().load(productId, true, false, headerText, invokeForm);
                }
            } else if (productType == "master") {
                if ((productId !== "" || productId != "null" || productId !== null) && (defaultProductId !== "" || defaultProductId != "null" || defaultProductId !== null)) {
                    MVCApp.getPDPController().load(productId, false, false, headerText, invokeForm, defaultProductId);
                }
            } else {
                if (productId !== "" || productId != "null" || productId !== null) {
                    MVCApp.getPDPController().load(productId, true, false, headerText, invokeForm);
                }
            }
            MVCApp.getPDPController().setEntryType("deeplink");
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProductsList") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            frombrowserdeeplink = true;
            var query = params.launchparams.q || "";
            var cgid = params.launchparams.cgid || "";
            var source = params.launchparams.source || ""
            var queryObj = {};
            gblIsFromImageSearch = false;
            imageSearchProjectBasedFlag = false;
            frmProductsList.segProductsList.removeAll();
            if (cgid !== "") {
                queryObj = {
                    "id": cgid
                };
                MVCApp.getProductsListController().setFromDeepLink(true);
                MVCApp.getProductsListController().load(queryObj, false);
            } else if (query !== "") {
                MVCApp.getProductsListController().load(decodeURI(decodeURI(query)), true);
            }
            if (source == "QR") {
                MVCApp.getProductsListController().setEntryType("QRCodeSearch");
            } else {
                MVCApp.getProductsListController().setEntryType("deeplink");
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmEvents") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            if (fromBrowserViews) {
                MVCApp.getEventsController().setFormEntryFromCartOrWebView(true);
            }
            MVCApp.getEventsController().load(false);
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmEventDetail") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var reqParams = {};
            if (fromBrowserViews) {
                MVCApp.getEventDetailsController().setFormEntryFromCartOrWebView(true);
            }
            reqParams.eventid = params.launchparams.Id || "";
            MVCApp.getEventsController().load(reqParams);
            frombrowserdeeplink = true;
        } else if (params.launchparams.formToOpen == "frmWeeklyAdDetail") {
            frombrowserdeeplink = true;
            dealId = params.launchparams.Id || "";
            gblIsWeeklyAdDetailDeepLink = true;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            if (lMyLocationFlag === "true") {
                MVCApp.Toolbox.common.showLoadingIndicator("");
                MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId, "", weeklyAdFormID, dealId);
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "weeklyAdDetail"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
        } else if (params.launchparams.formToOpen == "frmChatBox") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.getChatBoxController().load();
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProducts") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.getProductsController().updateScreen();
            MVCApp.getProductsListController().setFromDeepLink(false);
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProductCategory") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            if (fromBrowserViews) {
                MVCApp.getProductCategoryController().setFormEntryFromCartOrWebView(true);
            }
            var reqParams = {};
            reqParams.productId = params.launchparams.productId || "";
            MVCApp.getProductsListController().setFromDeepLink(false);
            MVCApp.getProductCategoryController().load(reqParams);
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProductCategoryMore") {
            if (fromBrowserViews) {
                MVCApp.getProductCategoryMoreController().setFormEntryFromCartOrWebView(true);
            }
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var reqParams1 = {};
            MVCApp.getProductsListController().setFromDeepLink(false);
            reqParams1.id = params.launchparams.productId || "";
            reqParams1.name = "";
            reqParams1.headerImage = "";
            MVCApp.getProductCategoryMoreController().load(reqParams1);
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProjects") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.getProjectsController().updateScreen();
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProjectsCategory") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var reqParams2 = {};
            reqParams2.id = params.launchparams.projectId || "";
            reqParams2.image = "";
            reqParams2.name = "";
            MVCApp.getProjectsCategoryController().load(reqParams2);
            //  frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmWeeklyAdsList") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            if (fromBrowserViews) {
                MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(true);
            }
            var currentForm = kony.application.getCurrentForm();
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            if (currentForm != null && currentForm != undefined) {
                weeklyAdFormID = currentForm.id;
            }
            if (lMyLocationFlag === "true") {
                weeklyAdId = params.launchparams.Id || "";
                MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId, "", weeklyAdFormID);
                frombrowserdeeplink = true;
                if (currentFormID !== "frmWeeklyAd") {
                    formFromDeeplink = true;
                }
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "weeklyAdsList"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmProjectsList") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var queryProjectList = {};
            queryProjectList.id = params.launchparams.Id || "";
            gblIsFromImageSearch = false;
            imageSearchProjectBasedFlag = false;
            MVCApp.getProjectsListController().setEntryType("deeplink");
            MVCApp.getProjectsListController().load(queryProjectList, false, false);
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmSearch") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var searchQuery = params.launchparams.searchText || "";
            kony.store.setItem("searchedProdId", searchQuery);
            frmProductsList.segProductsList.removeAll();
            MVCApp.getProductsListController().load(searchQuery, true);
            gblIsFromImageSearch = false;
            imageSearchProjectBasedFlag = false;
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmRewards") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.getRewardsProgramInfoController().load(false);
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmHome") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.searchBar.homePageLoadingHandler();
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmSettings") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.getSettingsController().load();
            MVCApp.tabbar.bindEvents(frmSettings);
            MVCApp.tabbar.bindEvents(frmMore);
            MVCApp.tabbar.bindIcons(frmSettings, "frmMore");
            frombrowserdeeplink = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } else if (params.launchparams.formToOpen == "frmFindStoreMapView") {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var currentformId = "";
            try {
                currentformId = kony.application.getCurrentForm().id;
            } catch (e) {
                currentformId = "frmHome";
            }
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.entryPoint = "Deeplink";
            MVCApp.Customer360.sendInteractionEvent("ChooseStore", cust360Obj);
            MVCApp.getLocatorMapController().load(currentformId);
            frombrowserdeeplink = true;
        } else {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
        //End of storeHours deeplinking Code.
        var currentForm = kony.application.getCurrentForm();
        if (currentForm === null) {
            currentForm = "";
        }
        return currentForm;
    } else if (params.launchmode == 2) {
        deeplinkEnabled = true;
        var currentForm = kony.application.getCurrentForm();
        if (!currentForm) {
            return frmHome;
        }
        appLaunchCust360Sent = true;
        kony.print("do not go to home Form 1231" + deeplinkEnabled);
    } else {
        deeplinkEnabled = false;
        try {
            frmHome.onDeviceBack = onDeviceBackOfHomeForm;
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On the click of back in VoiceSearch from frmHome", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        if (MVCApp.Toolbox.Service.getFlagForInstallation() == "true") {
            initLaunchFlag = true;
            kony.store.setItem("firstLaunchIntallation", "true");
            kony.store.setItem("firstIntallation", "false");
            kony.store.setItem("showWhatsNew", "true");
            kony.store.setItem("firstLaunchFromPrevKonyVersionFlag", "false");
            MVCApp.getGuideController().init();
            return frmGuide;
        } else {
            kony.store.setItem("firstLaunchIntallation", "false");
            kony.store.setItem("showWhatsNew", "false");
            firstLaunchFromPrevKonyVersionFlag = kony.store.getItem("firstLaunchFromPrevKonyVersionFlag");
            if (!firstLaunchFromPrevKonyVersionFlag || (firstLaunchFromPrevKonyVersionFlag == "false" && (!isPushRegisterInvokedInPrevVersion && initLaunchFlagForVersion72))) {
                kony.store.setItem("firstLaunchFromPrevKonyVersionFlag", "false");
                MVCApp.getGuideController().init();
                return frmGuide;
            } else if (firstLaunchFromPrevKonyVersionFlag == "false") {
                if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
                    MVCApp.Toolbox.common.showLoadingIndicator("");
                    frmHome.flxSearchOverlay.flxVisual.cmrImageSearch.cameraOptions = {
                        hideControlBar: true,
                        focusMode: constants.CAMERA_FOCUS_MODE_CONTINUOUS
                    };
                    frmHome.flxHeaderWrap.cmrImageSearch.cameraOptions = {
                        hideControlBar: true,
                        focusMode: constants.CAMERA_FOCUS_MODE_CONTINUOUS
                    };
                    frmHome.flxSearchOverlay.flxVisual.cmrImageSearch.setEnabled(false);
                    frmHome.flxHeaderWrap.cmrImageSearch.setEnabled(false);
                } else {
                    if (appInForeground) {
                        homePreShowHandler();
                        MVCApp.getHomeController().load();
                    }
                }
                //return frmHome;
                //       //#ifdef iphone
                //       return frmHome;
                //       //#endif
            }
        }
    }
}