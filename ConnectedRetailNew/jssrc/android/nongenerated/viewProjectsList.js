/**
 * PUBLIC
 * This is the view for the ProjectsList form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProjectsListView = (function() {
    /**
     * PUBLIC
     * Open the ProjectsList form
     */
    function determineContentOfHeader(fromSearch) {
        if (fromSearch) {
            frmProjectsList.flxCategory.setVisibility(false);
            frmProjectsList.btnProducts.skin = "sknBtnTabsDefault";
            frmProjectsList.btnProducts.focusSkin = "sknBtnTabsDefault";
            frmProjectsList.btnProjects.skin = "sknBtnTabsActive";
            frmProjectsList.btnProjects.focusSkin = "sknBtnTabsActive";
            frmProjectsList.flxProdProjTabs.isVisible = true;
        } else {
            frmProjectsList.flxCategory.setVisibility(true);
            frmProjectsList.flxProdProjTabs.isVisible = false;
        }
    }

    function showUI() {
        frmProjectsList.flxSearchResultsContainer.setVisibility(false);
        MVCApp.Toolbox.common.setTransition(frmProjectsList, 2);
        if (!gblIsFromImageSearch) {
            frmProjectsList.flxRefineSearchWrap.isVisible = true;
            frmProjectsList.flxCategory.isVisible = true;
        }
        frmProjectsList.show();
    }

    function show(fromSearch) {
        determineContentOfHeader(fromSearch);
        frmProjectsList.segProjectList.removeAll();
        MVCApp.Toolbox.common.setTransition(frmProjectsList, 3);
    }
    var prevFormName = "";

    function savePreviousScreenForm() {
        var prevForm = kony.application.getPreviousForm();
        if (prevForm != null && prevForm != undefined) {
            prevForm = prevForm.id;
        }
        if (prevForm == "frmHome") {
            prevFormName = prevForm;
        } else if (prevForm == "frmProjectsCategory") {
            prevFormName = prevForm;
        }
    }

    function updateProductCount(totalProducts) {
        var totProducts = 0;
        if (totalProducts) {
            totProducts = totalProducts;
        }
        var fromRefine = MVCApp.getProjectsListController().getFromRefineFlag();
        if (!fromRefine) {
            if (totProducts === "0") {
                frmProjectsList.flxProdProjTabs.isVisible = false;
            } else {
                frmProjectsList.btnProducts.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnProduts") + " (" + totProducts + ")";
                frmProjectsList.btnProducts.skin = "sknBtnTabsDefault";
                frmProjectsList.btnProducts.focusSkin = "sknBtnTabsDefault";
                frmProjectsList.btnProjects.skin = "sknBtnTabsActive";
                frmProjectsList.btnProjects.focusSkin = "sknBtnTabsActive";
                frmProjectsList.flxProdProjTabs.isVisible = true;
            }
        }
        frmProjectsList.flxMainContainer.forceLayout();
    }

    function blankPageShow() {
        frmProjectsList.flxRefineSearchWrap.isVisible = false;
        frmProjectsList.flxCategory.isVisible = false;
        frmProjectsList.btnCategoryName.text = "";
        frmProjectsList.segProjectList.removeAll();
        frmProjectsList.flxHeaderWrap.textSearch.text = MVCApp.getProjectsListController().getQueryStringForSearch();
        frmProjectsList.flxProdProjTabs.isVisible = false;
        frmProjectsList.flxSearchResultsContainer.setVisibility(false);
        var text = frmProjectsList.flxHeaderWrap.textSearch.text || "";
        if (text === "") {
            frmProjectsList.flxHeaderWrap.btnClearSearch.setVisibility(false);
        } else {
            frmProjectsList.flxHeaderWrap.btnClearSearch.setVisibility(false);
        }
        MVCApp.Toolbox.common.setTransition(frmProjectsList, 3);
    }

    function updateScreen(dataProjects, fromSearch, count) {
        frmProjectsList.segProjectList.widgetDataMap = {
            lblName: "product_name",
            imgProject: "picProject",
            imgExpertise: "skillLevel",
            lblExpertiseDesc: "c_skillLevel",
            imgDuration: "creftTime",
            lblDuration: "c_ecomCraftTime",
            flxItemLocation: "flxDurationAndSkill"
        };
        show(fromSearch);
        var totalResultsForSearch = 0;
        frmProjectsList.flxVoiceSearch.setVisibility(false);
        var dataForSeg = dataProjects.getDataForProjects() || [];
        if (dataForSeg.length === 0) {
            var queryString = MVCApp.getProjectsListController().getQueryText();
            if (voiceSearchFlag) {
                sorryPageFlag = true;
                frmProjectsList.flxVoiceSearch.lblUtterenace2.text = queryString;
                frmProjectsList.flxVoiceSearch.lblUtterence1.setVisibility(false);
                frmProjectsList.flxVoiceSearch.lblUtterance3.setVisibility(false);
                frmProjectsList.flxVoiceSearch.lblVoiceSearchHeading.text = "Sorry";
                frmProjectsList.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.NoResultsFoundForVoiceSearch");
                frmProjectsList.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
                frmProjectsList.flxVoiceSearch.setVisibility(true);
                MVCApp.Customer360.callVoiceSearchResult("failure", "No Projects Found", "");
            } else {
                sorryPageFlag = false;
                setTopForNoResultsFromCategory(fromSearch);
                frmProjectsList.btnCategoryName.text = queryString.name;
                if (fromSearch) {
                    frmProjectsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noResultsFound") + "<b> " + queryString + " </b>";
                } else {
                    frmProjectsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noResultsFound") + "<b> " + queryString.name + " </b>";
                }
                frmProjectsList.lbltextNoResultsFound.setVisibility(false);
                frmProjectsList.flxNoResults.setVisibility(true);
                frmProjectsList.flxProdProjTabs.isVisible = false;
            }
        } else {
            currentForm.flxVoiceSearch.setVisibility(false);
            currentForm.flxSearchOverlay.setVisibility(false);
            var totalResults = dataProjects.getTotalResults();
            var queryString = MVCApp.getProjectsListController().getQueryText();
            if (voiceSearchFlag) {
                var totalProductsResults = MVCApp.getProjectsListController().getProductCount() || 0;
                totalResultsForSearch = parseInt(totalProductsResults) + parseInt(totalResults);
                MVCApp.Customer360.callVoiceSearchResult("success", "", "PJLP", totalResultsForSearch);
                frmProjectsList.flxHeaderWrap.textSearch.text = queryString;
            } else if (!fromSearch) {
                frmProjectsList.flxHeaderWrap.textSearch.text = queryString.name;
            } else {
                frmProjectsList.flxHeaderWrap.textSearch.text = queryString;
            }
            var start = dataProjects.getStart();
            MVCApp.getProjectsListController().setStart(parseInt(start));
            if (!gblIsFromImageSearch) {
                MVCApp.getProjectsListController().setTotal(parseInt(totalResults));
                var previousForm = kony.application.getPreviousForm();
                if (!previousForm || previousForm.id !== "frmRefineProjects") {
                    var totalProducts = MVCApp.getProjectsListController().getProductCount();
                    updateProductCount(totalProducts);
                }
                frmProjectsList.btnProjects.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnProjects") + " (" + totalResults + ")";
            }
            frmProjectsList.segProjectList.setData(dataForSeg);
            if (!fromSearch) {
                var queryString = MVCApp.getProjectsListController().getQueryText();
                frmProjectsList.btnCategoryName.text = queryString.name;
                frmProjectsList.imgCategoryImg.src = "";
                frmProjectsList.imgCategoryImg.src = queryString.bgimage;
            }
        }
        if (!gblIsFromImageSearch) {
            var refineCount = MVCApp.getProjectsListController().getRefineCount();
            if (refineCount) {
                frmProjectsList.btnRefineSearch.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnRefineSearch") + " (" + refineCount + ")";
            } else {
                frmProjectsList.btnRefineSearch.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnRefineSearch");
            }
            frmProjectsList.show();
        }
        frmProjectsList.flxSearchResultsContainer.setVisibility(false);
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function setCustomer360Tags() {
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.entryType = MVCApp.getProjectsListController().getEntryType();
        var projectIds = MVCApp.getProjectsListController().getProjectIds();
        for (var m = 0; m < projectIds.length; m++) {
            cust360Obj["projectid" + m] = projectIds[m];
        }
        if (!formFromDeeplink) MVCApp.Customer360.sendInteractionEvent("PJLP", cust360Obj);
        else formFromDeeplink = false;
    }

    function getSelectedDataForPDP() {
        var selectedData = frmProjectsList.segProjectList.selectedRowItems[0];
        var productId = "";
        if (gblIsFromImageSearch) {
            productId = selectedData.id;
        } else {
            productId = selectedData.product_id;
        }
        MVCApp.Toolbox.common.clearPdpGlobalVariables();
        var queryString = MVCApp.getProjectsListController().getQueryStringForSearch();
        MVCApp.getPJDPController().setQueryString(queryString);
        MVCApp.getPJDPController().setEntryType("pjlp");
        MVCApp.getPJDPController().load(productId, true, "frmProjectsList");
        var entryType = MVCApp.getProjectsListController().getEntryType();
        MVCApp.Toolbox.common.sendTealiumTagsForSearchAnalytics(entryType, productId);
        if (frmProjectsList.flxProdProjTabs.isVisible) {
            kony.print("searchResultClick:ProjectsList");
            MVCApp.sendMetricReport(frmProjectsList, [{
                "searchResultClick": "ProjectsList"
            }]);
        }
    }

    function showMore(results) {
        var dataProjects = results.getDataForProjects();
        var start = results.getStart();
        MVCApp.getProjectsListController().setStart(parseInt(start));
        frmProjectsList.segProjectList.addAll(dataProjects);
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        setCustomer360Tags();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function backtoProjectCategory() {
        if (prevFormName == "frmHome" || frombrowserdeeplink) {
            MVCApp.getHomeController().load();
        } else if (prevFormName == "frmProjectsCategory") {
            MVCApp.getProjectsCategoryController().displayPage();
        }
    }

    function bindEvents() {
        var whenProjBtnClicked = false;
        frmProjectsList.onHide = function() {
            frmProjectsList.btnRefineSearch.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnRefineSearch");
        };
        MVCApp.searchBar.bindEvents(frmProjectsList, "frmProjects");
        MVCApp.tabbar.bindEvents(frmProjectsList);
        MVCApp.tabbar.bindIcons(frmProjectsList, "frmProjects");
        frmProjectsList.segProjectList.onRowClick = getSelectedDataForPDP;
        frmProjectsList.btnCategoryName.onClick = backtoProjectCategory;
        frmProjectsList.lblCategoryBack.onTouchEnd = backtoProjectCategory;
        frmProjectsList.btnProjects.onClick = function() {
            if (whenProjBtnClicked) {
                if (gblIsFromImageSearch && productsFromImageSearch.length === 0) {
                    frmProjectsList.btnProducts.skin = "sknBtnTabsDefault";
                    frmProjectsList.btnProducts.focusSkin = "sknBtnTabsDefault";
                    frmProjectsList.btnProjects.skin = "sknBtnTabsActive";
                    frmProjectsList.btnProjects.focusSkin = "sknBtnTabsActive";
                    frmProjectsList.flxNoResults.isVisible = false;
                    frmProjectsList.flxMainContainer.forceLayout();
                } else {
                    frmProductsList.btnProducts.skin = "sknBtnTabsDefault";
                    frmProductsList.btnProducts.focusSkin = "sknBtnTabsDefault";
                    frmProductsList.btnProjects.skin = "sknBtnTabsActive";
                    frmProductsList.btnProjects.focusSkin = "sknBtnTabsActive";
                    frmProductsList.flxNoResults.isVisible = false;
                    frmProductsList.flxMainContainer.forceLayout();
                }
            }
        };
        frmProjectsList.btnProducts.onClick = function() {
            var totalProducts = null;
            var isFromVoiceSearch = false;
            var entryType = MVCApp.getProjectsListController().getEntryType();
            var queryString;
            if (entryType == "VoiceSearch") {
                isFromVoiceSearch = true;
                queryString = MVCApp.getProjectsListController().getQueryStringForSearch();
            } else {
                queryString = frmProjectsList.flxHeaderWrap.textSearch.text;
            }
            if (gblIsFromImageSearch) {
                totalProducts = productsFromImageSearch.length === 0 ? false : productsFromImageSearch.length;
            } else {
                totalProducts = MVCApp.getProjectsListController().getProductCount();
            }
            kony.print("@@@@LOG:bindEvents-totalProducts:" + totalProducts + "type:" + typeof totalProducts);
            if (totalProducts) {
                MVCApp.Toolbox.common.showLoadingIndicator("");
                if (gblIsFromSortAndFilter || isFromVoiceSearch) {
                    MVCApp.getProductsListController().load(queryString, true);
                } else {
                    if (imageSearchProjectBasedFlag) {
                        imageSearchProjectBasedFlag = false;
                        imageSearchProgressFlag = true;
                        MVCApp.getImageSearchController().returnToFormFlag = true;
                        MVCApp.getImageSearchController().showProductsListFromImageSearch();
                    } else {
                        MVCApp.getProductsListController().backToScreen();
                    }
                }
            } else {
                whenProjBtnClicked = true;
                frmProjectsList.btnProducts.skin = "sknBtnTabsActive";
                frmProjectsList.btnProducts.focusSkin = "sknBtnTabsActive";
                frmProjectsList.btnProjects.skin = "sknBtnTabsDefault";
                frmProjectsList.btnProjects.focusSkin = "sknBtnTabsDefault";
                if (gblIsFromImageSearch) {
                    imageSearchProjectBasedFlag = false;
                    frmProjectsList.flxNoResults.top = "100dp";
                    frmProjectsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.noProductsFound");
                    frmProjectsList.lbltextNoResultsFound.setVisibility(false);
                } else {
                    frmProjectsList.flxNoResults.top = "142dp";
                    frmProjectsList.rtxtSearchNotFound.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noProductsFound") + "&nbsp;<b>" + queryString + "</b>";
                }
                frmProjectsList.flxMainContainer.forceLayout();
                frmProjectsList.flxNoResults.isVisible = true;
            }
        };
        frmProjectsList.btnRefineSearch.onClick = MVCApp.getSortProjectController().load;
        frmProjectsList.segProjectList.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
        frmProjectsList.segProjectList.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
        frmProjectsList.preShow = function() {
            if (gblIsFromImageSearch) {
                frmProjectsList.flxRefineSearchWrap.isVisible = false;
                frmProjectsList.flxProdProjTabs.top = "50dp";
                frmProjectsList.segProjectList.top = "92dp";
            } else {
                frmProjectsList.flxCategory.top = "88dp";
                frmProjectsList.flxRefineSearchWrap.setVisibility(true);
                frmProjectsList.flxNoResults.setVisibility(false);
                frmProjectsList.flxProdProjTabs.top = "90dp";
            }
        }
        frmProjectsList.postShow = function() {
            var cartValue = MVCApp.Toolbox.common.getCartItemValue();
            frmProjectsList.flxHeaderWrap.lblItemsCount.text = cartValue;
            MVCApp.Toolbox.common.setBrightness("frmProjectsList");
            MVCApp.Toolbox.common.destroyStoreLoc();
            savePreviousScreenForm();
            var lProductsCount = 0;
            var lProjectsCount = 0;
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Category:" + frmProjectsList.btnCategoryName.text;
            lTealiumTagObj.page_name = "Category:" + frmProjectsList.btnCategoryName.text;
            lTealiumTagObj.page_type = "project category";
            lTealiumTagObj.page_category_name = "Projects:" + frmProjectsCategory.btnCategoryName.text + ":" + frmProjectsList.btnCategoryName.text;
            lTealiumTagObj.page_category = "Projects:" + frmProjectsCategory.btnCategoryName.text + ":" + frmProjectsList.btnCategoryName.text;
            lTealiumTagObj.page_subcategory_name = frmProjectsList.btnCategoryName.text;
            if (MVCApp.getProjectsListController().getFromImageSearch()) {
                if (MVCApp.getProjectsListController().getProductsCountFromImageSearch()) {
                    lProductsCount = parseInt(MVCApp.getProjectsListController().getProductsCountFromImageSearch());
                }
                if (MVCApp.getProjectsListController().getProjectsCountFromImageSearch()) {
                    lProjectsCount = parseInt(MVCApp.getProjectsListController().getProjectsCountFromImageSearch());
                }
                lTealiumTagObj.page_id = "Category:";
                lTealiumTagObj.page_name = "Category:";
                lTealiumTagObj.page_type = "project category";
                lTealiumTagObj.page_category_name = "Projects:";
                lTealiumTagObj.page_category = "Projects:";
                lTealiumTagObj.page_subcategory_name = "";
                lTealiumTagObj.search_term = "";
                lTealiumTagObj.product_image_search_results = lProductsCount;
                lTealiumTagObj.project_image_search_results = lProjectsCount;
                lTealiumTagObj.search_results = lProductsCount + lProjectsCount;
                lTealiumTagObj.search_status = "successful";
                MVCApp.getProjectsListController().setFromImageSearch(false);
            } else if (frmProjectsList.flxHeaderWrap.textSearch.text && frmProjectsList.flxProdProjTabs.isVisible) {
                if (MVCApp.getProjectsListController().getProductCount()) {
                    lProductsCount = parseInt(MVCApp.getProjectsListController().getProductCount());
                }
                if (MVCApp.getProjectsListController().getTotal()) {
                    lProjectsCount = MVCApp.getProjectsListController().getTotal();
                }
                lTealiumTagObj.search_term = frmProjectsList.flxHeaderWrap.textSearch.text;
                lTealiumTagObj.product_search_results = lProductsCount;
                lTealiumTagObj.project_search_results = lProjectsCount;
                lTealiumTagObj.search_results = lProductsCount + lProjectsCount;
                lTealiumTagObj.search_status = "successful";
            } else {
                lTealiumTagObj.search_term = frmProjectsList.flxHeaderWrap.textSearch.text;
                lTealiumTagObj.search_results = 0;
                lTealiumTagObj.product_search_results = 0;
                lTealiumTagObj.project_search_results = 0;
                lTealiumTagObj.search_status = "unsuccessful";
            }
            TealiumManager.trackView("Project Tier 2 Category Page", lTealiumTagObj);
            if (!formFromDeeplink) {
                setCustomer360Tags();
            } else {
                formFromDeeplink = false;
            }
        };
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
            frmProjectsList.segProjectList.bottom = "0dp";
        } else {
            MVCApp.Toolbox.common.dispFooter();
            frmProjectsList.segProjectList.bottom = "54dp";
        }
    }

    function setTopForNoResultsFromCategory(fromSearch) {
        if (fromSearch) {
            frmProjectsList.flxCategory.top = "88dp";
            frmProjectsList.flxRefineSearchWrap.setVisibility(true);
            frmProjectsList.flxMainContainer.forceLayout();
        } else {
            frmProjectsList.flxRefineSearchWrap.setVisibility(false);
            frmProjectsList.flxCategory.top = "50dp";
            frmProjectsList.flxNoResults.top = "92dp";
            frmProjectsList.flxMainContainer.forceLayout();
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        showUI: showUI,
        showMore: showMore,
        blankPageShow: blankPageShow,
        updateProductCount: updateProductCount
    };
});