/**
 * PUBLIC
 * This is the view for the chatbox form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ChatBoxView = (function() {
    var prevFormName = "";
    /**
     * PUBLIC
     * Open the More form
     */
    function show() {
        var language = kony.store.getItem("languageSelected");
        if (language == "en" || language == "en_US" || language == "en_CA") {
            frmChatBox.imgChatBot.left = "12%";
        } else {
            frmChatBox.imgChatBot.left = "9%";
        }
        frmChatBox.show();
    }

    function bindEvents() {
        try {
            frmChatBox.btnHeaderLeft.onClick = showPreviousScreen;
            frmChatBox.postShow = function() {
                savePreviousScreenForm();
                frmChatBox.browserWidget.handleRequest = handleRequestCallback;
                var lTealiumTagObj = gblTealiumTagObj;
                lTealiumTagObj.page_id = "Chatbot Michaels Mobile App";
                lTealiumTagObj.page_name = "Chatbot Michaels Mobile App";
                lTealiumTagObj.page_type = "Chatbot";
                lTealiumTagObj.page_category_name = "Chatbot";
                lTealiumTagObj.page_category = "Chatbot";
                TealiumManager.trackView("Chatbot Screen", lTealiumTagObj);
            };
            frmChatBox.browserWidget.onSuccess = function() {};
        } catch (e) {
            kony.print(e);
            if (e) {
                TealiumManager.trackEvent("Bind Events Exception in ChatBot Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function savePreviousScreenForm() {
        var prevForm = kony.application.getPreviousForm();
        if (prevForm != null && prevForm != undefined) {
            prevForm = prevForm.id;
        }
        if (prevForm == "frmHome") {
            prevFormName = prevForm;
        } else if (prevForm == "frmProductCategory") {
            prevFormName = prevForm;
        } else if (prevForm == "frmProductCategoryMore") {
            prevFormName = prevForm;
        }
    }

    function handleRequestCallback(browserWidget, params) {
        var URL = params.originalURL || "";
        if (URL !== "") {
            kony.print("PDP URL from chat boat" + URL);
            MVCApp.Toolbox.common.openApplicationURL(URL);
        }
    }

    function updateScreen() {
        //base URL
        var baseURL = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.chatbotBaseURL");
        //StoreNo
        var storeNo = "";
        var storeString = kony.store.getItem("myLocationDetails") || "";
        if (storeString !== "") {
            try {
                var storeObj = JSON.parse(storeString);
                storeNo = storeObj.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewChatBox.js on updateScreen for storeString:" + JSON.stringify(e)
                }]);
                storeNo = "";
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception in ChatBot Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeNo = "";
        }
        //customerid
        var customerId = "";
        var userObject = kony.store.getItem("userObj");
        if (userObject === "" || userObject === null) {
            customerId = "";
        } else {
            customerId = userObject.customer_id || "";
        }
        //DeviceId
        var deviceID = "";
        deviceID = kony.os.deviceInfo().deviceid;
        var sessionID = MVCApp.getChatBoxController().getSession();
        MVCApp.getChatBoxController().setSession("");
        var openChatURL = baseURL + "sessionId=" + sessionID + "&storeId=" + storeNo;
        var urlConf = {
            URL: openChatURL,
            requestMethod: constants.BROWSER_REQUEST_METHOD_GET
        };
        frmChatBox.browserWidget.requestURLConfig = urlConf;
        frmChatBox.browserWidget.setVisibility(false);
        show();
        frmChatBox.browserWidget.onSuccess = function() {
            frmChatBox.browserWidget.setVisibility(true);
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        };
        frmChatBox.browserWidget.onFailure = function() {
            frmChatBox.browserWidget.setVisibility(false);
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.chatbot.errorMessage"), "");
        };
    }

    function showPreviousScreen() {
        MVCApp.getChatBoxController().setSession("");
        if (prevFormName == "frmHome" || frombrowserdeeplink) {
            MVCApp.getHomeController().load();
        } else if (prevFormName == "frmProductCategory") {
            frmProductCategory.show();
        } else if (prevFormName == "frmProductCategoryMore") {
            frmProductCategoryMore.show();
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});