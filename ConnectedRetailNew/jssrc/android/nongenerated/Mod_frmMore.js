//Type your code here
function tapMore() {
    frmMore.flxFooterWrap.flxProducts.skin = 'slFbox';
    frmMore.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmMore.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmMore.flxFooterWrap.flxEvents.skin = 'slFbox';
    frmMore.flxFooterWrap.flxMore.skin = 'sknFlexBgBlackOpaque10';
}

function NAV_frmMore() {
    frmMore.show();
    tapMore();
}
/* Toggle My Store */
var toggleRewards = true;

function rewardsToggle() {
    var height = "";
    if (frmMore.btnRewardsSignUp.isVisible) {
        if (!rewardsCardToggleFlag) {
            height = "135dp";
        } else {
            height = "215dp";
        }
    } else {
        if (!rewardsCardToggleFlag) {
            height = "180dp";
        } else {
            height = "321dp";
        }
    }
    if (toggleRewards === true) {
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            if (frmMore.btnRewardsSignUp.isVisible === false && rewardsCardToggleFlag === true) {
                MVCApp.Toolbox.common.setBrightness("frmMore");
                gblBrightNessMoreValue = gblBrightNessValue;
                MVCApp.Toolbox.common.setBrightnessForBarcode();
                gblBrightnessfrmSignInPage = true;
            }
        }
        frmMore.flxRewardMainContainer.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "height": height
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(180);
        frmMore.lblIconDropdownRewards.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
    } else {
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            var createInstanceObject = new bright.createInstance();
            var currentBrightnessValue = createInstanceObject.getBrightness();
            if (frmMore.btnRewardsSignUp.isVisible && currentBrightnessValue != gblBrightNessMoreValue) {
                gblBrightNessMoreValue = currentBrightnessValue;
            }
            var returnedValueStr = gblBrightNessMoreValue + "";
            MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValueStr);
            createInstanceObject.setBrightness(returnedValueStr);
        }
        frmMore.flxRewardMainContainer.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "height": "44dp"
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
        var trans101 = kony.ui.makeAffineTransform();
        trans101.rotate(0);
        frmMore.lblIconDropdownRewards.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
    }
    toggleRewards = !toggleRewards;
}
var rewardsCardToggleFlag = true;

function rewardsCardToggle() {
    var height = "";
    if (frmMore.btnRewardsSignUp.isVisible) {
        if (!toggleRewards) {
            if (rewardsCardToggleFlag) {
                height = "135dp";
            } else {
                height = "190dp";
            }
        }
    } else {
        if (!toggleRewards) {
            if (rewardsCardToggleFlag) {
                height = "180dp";
            } else {
                height = "321dp";
            }
        }
    }
    if (rewardsCardToggleFlag === true) {
        //alert("if");
        var createInstanceObject = new bright.createInstance();
        var returnedValueStr = gblBrightNessMoreValue + "";
        MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValueStr);
        createInstanceObject.setBrightness(returnedValueStr);
        frmMore.lblRewardCardHolderName.setVisibility(false);
        frmMore.flxRewardInfo.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "height": "13dp"
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
        var trans100 = kony.ui.makeAffineTransform();
        trans100.rotate(180);
        frmMore.CopylblIconDropdown0cb91a4bf71574e.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
    } else {
        //alert("else");
        if (frmMore.btnRewardsSignUp.isVisible === false) {
            MVCApp.Toolbox.common.setBrightnessForBarcode();
        }
        frmMore.lblRewardCardHolderName.setVisibility(true);
        frmMore.flxRewardInfo.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "height": "68dp"
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
        var trans101 = kony.ui.makeAffineTransform();
        trans101.rotate(0);
        frmMore.CopylblIconDropdown0cb91a4bf71574e.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
    }
    rewardsCardToggleFlag = !rewardsCardToggleFlag;
    frmMore.flxRewardMainContainer.height = height;
}