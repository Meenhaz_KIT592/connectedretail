var imageSearchInvokedForm = null;
var imageSearchGalleryFlag = false;
var imageSearchOverlayFlag = false;
var imageSearchProgressFlag = false;

function imageOverlayInitCallback() {
    frmImageOverlay.preShow = imgOverlayPreShowCallback;
    frmImageOverlay.postShow = imgOverlayPostShowCallback;
    //the init would be called at application start up for android. Hence moved this code to postshow.
}

function onclickFlxOverlay() {
    kony.print("LOG:onclickFlxOverlay-START");
}

function onClickImageSearchGallery() {
    imagerotateforandroid.deleteTmpImage();
    TealiumManager.trackEvent("Gallery Icon Click From Camera", {
        conversion_category: "Camera",
        conversion_id: "Gallery Icon",
        conversion_action: 2
    });
    if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
        imageSearchGalleryFlag = true;
        var lResult = kony.application.checkPermission(kony.os.RESOURCE_EXTERNAL_STORAGE, {});
        if (lResult.status == kony.application.PERMISSION_GRANTED) {
            var queryContext = {
                mimeType: "image/*"
            };
            kony.phone.openMediaGallery(onGalleryImageSelectionCallback, queryContext);
        } else requestPermissionAtRuntime(kony.os.RESOURCE_EXTERNAL_STORAGE, externalStorageStatusCall);
        directorySettings.requestPermission("android.write_external_storage.permission");
    } else if (!networkAlertPopupFlag) {
        networkAlertPopupFlag = true;
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, function() {}, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
    }
}

function externalStorageStatusCall(response) {
    if (response.status == kony.application.PERMISSION_GRANTED) {
        var queryContext = {
            mimeType: "image/*"
        };
        kony.phone.openMediaGallery(onGalleryImageSelectionCallback, queryContext);
    } else if (response.status == kony.application.PERMISSION_GRANTED) {
        var basicConfig = {
            alertType: constants.ALERT_TYPE_CONFIRMATION,
            message: "Please enable the permission in Device Settings to proceed. Do you want to open settings?",
            alertHandler: permissionAlertHandler
        }
        kony.ui.Alert(basicConfig);
    }
}

function onGalleryImageSelectionCallback(selectedImgRawbytes, permStatus, mimeType) {
    if (permStatus == kony.application.PERMISSION_DENIED) {
        if (!kony.store.getItem("isCustomPhotoLibUsagePopupShownAlready")) {
            kony.store.setItem("isCustomPhotoLibUsagePopupShownAlready", true);
        } else {
            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.CustomPhotoLibraryUsageDescription"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.CustomPhotoLibraryUsageTitle"), constants.ALERT_TYPE_CONFIRMATION, getCameraPopup, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.DontAllow"));
        }
    } else if (selectedImgRawbytes) {
        var selectedImg = {
            "rawBytes": selectedImgRawbytes
        };
        MVCApp.getImageSearchController().didCaptureImage(selectedImg);
    }
}

function getCameraPopup(pResponse) {
    if (pResponse) {
        redirectToSettings.redirectUserToCameraSettings();
    }
}

function onClickBtnGotIt() {
    frmImageOverlay.flxOverlay.isVisible = false;
}

function onClickFlexInfo() {
    frmImageOverlay.flxOverlay.isVisible = true;
    /*var locale=kony.store.getItem("languageSelected");
    if(locale== "fr_CA"){
      frmImageOverlay.flxDialog.width = "78%";
      frmImageOverlay.flxDialog.height = "45%";
    }else{
      frmImageOverlay.flxDialog.width = "74%";
      frmImageOverlay.flxDialog.height = "40%";
    }*/
    frmImageOverlay.lblPopUpMessage.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageOverlay.popUpMessage");
    frmImageOverlay.btnGotIt.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageOverlay.popUpButtonText");
    frmImageOverlay.flxOverlay.forceLayout();
}

function imgOverlayPreShowCallback() {
    frmImageOverlay.flexCrossHairs.setVisibility(true);
    frmImageOverlay.btnSnap.onClick = onClickImageSearchSnap;
    frmImageOverlay.flexBack.onClick = onClickImageOverlayBack;
    frmImageOverlay.flexGallery.onClick = onClickImageSearchGallery;
    frmImageOverlay.flexInfo.onClick = onClickFlexInfo;
    frmImageOverlay.btnGotIt.onClick = onClickBtnGotIt;
    frmImageOverlay.flxOverlay.onClick = onclickFlxOverlay;
    frmImageOverlay.flexImageSearch.setVisibility(false);
    if (frmImageOverlay.flexErrorMessage.isVisible) {
        frmImageOverlay.flexErrorMessage.setVisibility(false);
    }
    imageSearchOverlayFlag = true;
}

function imgOverlayPostShowCallback() {
    if (!kony.store.getItem("isInfoPopupShownAlready")) {
        kony.store.setItem("isInfoPopupShownAlready", true);
        frmImageOverlay.flxOverlay.isVisible = true;
    }
    var lTealiumTagObj = gblTealiumTagObj;
    lTealiumTagObj.page_id = "ImageSearch: Michaels Mobile App";
    lTealiumTagObj.page_name = "ImageSearch: Michaels Mobile App";
    lTealiumTagObj.page_type = "ImageSearch";
    lTealiumTagObj.page_category_name = "ImageSearch";
    lTealiumTagObj.page_category = "ImageSearch";
    TealiumManager.trackView("ImageSearch Screen", lTealiumTagObj);
    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
    MVCApp.Customer360.sendInteractionEvent("ImageSearchLaunch", cust360Obj);
    TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchInitiation, {
        conversion_category: "App Search",
        conversion_id: "ImageSearch",
        conversion_action: 1
    });
    var currentForm = kony.application.getCurrentForm();
    var previousForm = kony.application.getPreviousForm();
    if (previousForm) {
        if (previousForm.id != "frmImageSearch") {
            imageSearchInvokedForm = previousForm;
        }
        if (!frmImageOverlay.flexSnap.isVisible) {
            frmImageOverlay.flexSnap.setVisibility(true);
        }
        if (!frmImageOverlay.flexFooter.isVisible) {
            frmImageOverlay.flexFooter.setVisibility(true);
        }
    } else if (currentForm.id == "frmHome") {
        imageSearchInvokedForm = currentForm;
    }
}

function onClickImageSearchSnap() {
    if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
        imageSearchGalleryFlag = false;
        imageSearchProjectBasedFlag = false;
        if (frmImageOverlay.flexErrorMessage.isVisible) {
            frmImageOverlay.flexErrorMessage.setVisibility(false);
        }
        if (frmImageOverlay.flexStatusBar.isVisible) {
            frmImageOverlay.flexStatusBar.setVisibility(false);
        }
        if (frmImageOverlay.flexBody.isVisible) {
            frmImageOverlay.flexBody.setVisibility(false);
        }
        frmImageOverlay.flexCrossHairs.setVisibility(false);
        frmImageOverlay.imgImageSearch.setVisibility(false);
        frmImageOverlay.lblAndSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.capturing") + "...";
        frmImageOverlay.lblAndSearchStatus.setVisibility(true);
        if (frmImageOverlay.flexSnap.isVisible) {
            frmImageOverlay.flexSnap.setVisibility(false);
        }
        if (frmImageOverlay.flexFooter.isVisible) {
            frmImageOverlay.flexFooter.setVisibility(false);
        }
        // frmImageOverlay.lblSearchStatus.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.imageSearch.capturing") + "...";
        // frmImageOverlay.lblSearchStatus.setVisibility(true);
        frmImageOverlay.flexImageSearch.setVisibility(true);
        if (imageSearchTryAgainFlag) {
            frmImageSearch.cmrTryAgain.takePicture();
        } else {
            if (!cameraWidgetRef) {
                cameraWidgetRef = frmHome.flxHeaderWrap.cmrImageSearch;
            }
            cameraWidgetRef.takePicture();
        }
    } else if (!networkAlertPopupFlag) {
        networkAlertPopupFlag = true;
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, function() {}, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
    }
}

function onClickImageSearchFlash() {}

function onClickImageOverlayBack() {
    imageSearchProgressFlag = false;
    imageSearchOverlayFlag = false;
    currentForm.flxSearchOverlay.setVisibility(false);
    if (!imageSearchStoreModeRefreshFlag && gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
        imageSearchStoreModeRefreshFlag = true;
        refreshImageSearchInvokedFormWithStoreSpecificData(true);
    } else {
        MVCApp.getImageSearchController().onClickImageSearchBack();
    }
    // imageSearchInvokedForm.flxSearchOverlay.flxVisual.cmrImageSearch.closeCamera();
    cameraWidgetRef.closeCamera();
    if (imageSearchPNG && imageSearchPNG.exists()) {
        imageSearchPNG.remove();
    }
}

function refreshImageSearchInvokedFormWithStoreSpecificData(pIsInStoreModeEnabled) {
    var lServiceRequestObj = {};
    if (imageSearchInvokedForm.id === "frmHome") {
        initLaunchFlag = false;
        getProductsCategory();
    } else if (imageSearchInvokedForm.id === "frmPDP") {
        lServiceRequestObj = MVCApp.getPDPController().lServiceRequestObj;
        MVCApp.getPDPController().load(lServiceRequestObj.productId, lServiceRequestObj.varientFlag, lServiceRequestObj.varientChangeFlag, lServiceRequestObj.headerTxt, lServiceRequestObj.formName, lServiceRequestObj.defaultVariantID, lServiceRequestObj.firstEnry, lServiceRequestObj.fromBack);
    } else if (imageSearchInvokedForm.id === "frmProductsList") {
        if (gblIsFromImageSearch) {
            lServiceRequestObj = MVCApp.getImageSearchController().lPLPServiceRequestObj;
            MVCApp.getImageSearchController().getPLPDataForImageSearch(lServiceRequestObj);
        } else {
            lServiceRequestObj = MVCApp.getProductsListController().lServiceRequestObj;
            MVCApp.getProductsListController().load(lServiceRequestObj.query, lServiceRequestObj.fromSearch, lServiceRequestObj.sortValue, lServiceRequestObj.refStringCgid, lServiceRequestObj.fromSort, lServiceRequestObj.fromBack, lServiceRequestObj.fromProdCat1);
        }
    } else if (imageSearchInvokedForm.id === "frmProductCategoryMore") {
        lServiceRequestObj = MVCApp.getProductCategoryMoreController().lServiceRequestObj;
        MVCApp.getProductCategoryMoreController().load(lServiceRequestObj.selectedSubCategory);
    } else if (imageSearchInvokedForm.id === "frmProductCategory") {
        lServiceRequestObj = MVCApp.getProductCategoryController().lServiceRequestObj;
        MVCApp.getProductCategoryController().load(lServiceRequestObj.selectedCategory);
    } else if (imageSearchInvokedForm.id === "frmPJDP") {
        lServiceRequestObj = MVCApp.getPJDPController().lServiceRequestObj;
        MVCApp.getPJDPController().load(lServiceRequestObj.productId, lServiceRequestObj.fromPJDPFlag, lServiceRequestObj.formName);
    } else if (imageSearchInvokedForm.id === "frmProjectsList") {
        if (gblIsFromImageSearch) {
            lServiceRequestObj = MVCApp.getImageSearchController().lPJLPServiceRequestObj;
            MVCApp.getImageSearchController().getPJLPDataForImageSearch(lServiceRequestObj);
        } else {
            lServiceRequestObj = MVCApp.getProjectsListController().lServiceRequestObj;
            MVCApp.getProjectsListController().load(lServiceRequestObj.query, lServiceRequestObj.fromSearch, lServiceRequestObj.fromRefine, lServiceRequestObj.refineString);
        }
    } else if (imageSearchInvokedForm.id === "frmShoppingList") {
        lServiceRequestObj = MVCApp.getShoppingListController().lServiceRequestObj;
        MVCApp.getShoppingListController().getProductListItems(lServiceRequestObj.fromBack);
    } else if (imageSearchInvokedForm.id === "frmMore") {
        MVCApp.getMoreController().load();
    } else if (imageSearchInvokedForm.id === "frmWeeklyAdHome") {
        MVCApp.getWeeklyAdHomeController().load();
    } else if (imageSearchInvokedForm.id === "frmWeeklyAd" || imageSearchInvokedForm.id === "frmWeeklyAdDetail") {
        alertUserAboutPageRefresh(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.entryExitMessageForWeeklyAds"));
    } else if (imageSearchInvokedForm.id === "frmEvents") {
        MVCApp.getEventsController().load(true);
    } else if (imageSearchInvokedForm.id === "frmEventDetail") {
        MVCApp.getEventDetailsController().loadInStoreModeEventDetails();
    }
}