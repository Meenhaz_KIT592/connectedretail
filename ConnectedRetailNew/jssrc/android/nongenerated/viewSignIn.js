/**
 * PUBLIC
 * This is the view for the Sign In form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var gblPassword = "";
var MVCApp = MVCApp || {};
var isSigninForm = false;
isMyListflag = false;
var isShowExecuted = false;
var _entryPointForSignin = "";
var _requestMethod = "";
var fromWebView = false;
MVCApp.SignInView = (function() {
    /**
     * PUBLIC
     * Open the More form
     */
    function show() {
        clearFields();
        MVCApp.Toolbox.common.setTransition(frmSignIn, 3);
        if (kony.store.getItem("switch") == null || kony.store.getItem("switch") == true) {
            frmSettings.switchTouch.src = "on.png";
            kony.store.setItem("switch", true);
        }
        frmSignIn.show();
        //touchID code
        isTouchSupported();
        var touchIDAlertTobeHidden = kony.store.getItem("hideTouchIDAlertForLoginPage") || false;
        if (kony.store.getItem("storeFirstUserForTouchId") != null && !touchIDAlertTobeHidden) { //kony.store.getItem("touchIdSaved") || 
            if (kony.store.getItem("switch")) {
                authenticate();
            }
        }
        MVCApp.tabbar.bindEvents(frmSignIn);
        MVCApp.tabbar.bindIcons(frmSignIn, "frmMore");
    }

    function clearFields() {
        frmSignIn.txtEmail.text = "";
        frmSignIn.txtPassword.text = "";
        frmSignIn.btnClearEmail.setVisibility(false);
        frmSignIn.btnClearPassword.setVisibility(false);
        frmSignIn.flxOverlay.setVisibility(false);
    }

    function setEntryPointForSignin(value) {
        _entryPointForSignin = value;
    }

    function getEntryPointForSignin() {
        return _entryPointForSignin;
    }

    function setRequestMethod(value) {
        _requestMethod = value;
    }

    function getRequestMethod() {
        return _requestMethod;
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmSignIn.onDeviceBack = function() {
            if (frmSignIn.flxOverlay.isVisible) {
                noRewardsProgram();
            } else {
                var previousForm = kony.application.getPreviousForm();
                if ((previousForm && previousForm !== null && previousForm !== undefined) && (previousForm.id == "frmMore" || previousForm.id == "frmSettings" || previousForm.id == "frmCreateAccount" || previousForm.id == "frmForgotPwd" || previousForm.id == "frmRegions")) {
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblIsFromHomeScreen) {
                        gblIsFromHomeScreen = false;
                        MVCApp.getHomeController().load();
                    } else {
                        MVCApp.getMoreController().loadMore();
                    }
                } else if (previousForm.id == "frmCartView") {
                    MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"), true);
                } else {
                    previousForm.show();
                    if (previousForm.id == "frmCoupons") {
                        MVCApp.Toolbox.common.setBrightness("frmCoupons");
                    }
                }
            }
        };
        frmSignIn.flxOverlay.onTouchEnd = function() {};
        frmSignIn.preShow = function() {
            var signInFromPage = "";
            setRequestMethod("");
            kony.print("the gblURL is======" + gblUrl);
            if (fromCartSignIn && gblUrl !== "") {
                fromCartSignIn = false;
                if (MVCApp.getSignInController().getButtonTypeForCheckOutButton()) {
                    signInFromPage = "Favorites-Cart";
                    frmSignIn.btnCreateAccount.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.btnCreateAccount");
                } else {
                    signInFromPage = "CheckOut";
                    frmSignIn.btnCreateAccount.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.guestCheckout");
                }
            } else {
                if (gblUrl && fromWebView) {
                    if (gblUrl.indexOf("pid=B") !== -1) {
                        signInFromPage = "Favorites-Project";
                    } else if (gblUrl.indexOf("pid") !== -1 && gblUrl.indexOf("personalized=true") !== -1) {
                        signInFromPage = "SaveForLater-Product";
                    } else {
                        signInFromPage = "Favorites-Product";
                    }
                    fromWebView = false;
                }
                frmSignIn.btnCreateAccount.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.btnCreateAccount");
            }
            kony.print("entry point is " + signInFromPage);
            setEntryPointForSignin(signInFromPage);
        }
        frmSignIn.onHide = function() {
            MVCApp.getSignInController().setButtonTypeForCheckOutButton(false);
        }
        frmSignIn.postShow = function() {
            MVCApp.Toolbox.common.setBrightness("frmSignIn");
            frmSignIn.txtPassword.autoComplete = false;
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Sign In: Michaels Mobile App";
            lTealiumTagObj.page_name = "Sign In: Michaels Mobile App";
            lTealiumTagObj.page_type = "Sign In";
            lTealiumTagObj.page_category_name = "Sign In";
            lTealiumTagObj.page_category = "Sign In";
            TealiumManager.trackView("Sign In screen", lTealiumTagObj);
        };
        frmSignIn.btnSignIn.onClick = onClickSignIn;
        frmSignIn.btnCancel.onClick = function() {
            var previousForm = kony.application.getPreviousForm();
            isFromRewards = false;
            if (gblIsPasswordExpired) {
                if (currentFormForPasswordExpiryCase) {
                    previousForm = currentFormForPasswordExpiryCase;
                }
                gblIsPasswordExpired = false;
                currentFormForPasswordExpiryCase = null;
            }
            if (previousForm.id == "frmMore" || previousForm.id == "frmSettings" || previousForm.id == "frmCreateAccount" || previousForm.id == "frmForgotPwd" || previousForm.id == "frmRegions") {
                if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblIsFromHomeScreen) {
                    gblIsFromHomeScreen = false;
                    MVCApp.getHomeController().load();
                } else {
                    if (isFromShoppingList) {
                        MVCApp.getHomeController().load();
                    } else {
                        MVCApp.getMoreController().loadMore();
                    }
                }
            } else if (previousForm.id == "frmCartView") {
                MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"), true);
            } else if (previousForm.id == "frmWebView") {
                MVCApp.getProductsListWebController().loadWebView(pdpUrl);
                pdpUrl = "";
            } else {
                previousForm.show();
                if (previousForm.id == "frmCoupons") {
                    MVCApp.Toolbox.common.setBrightness("frmCoupons");
                }
            }
        };
        frmSignIn.btnCreateAccount.onClick = onClickCreateAccount;
        frmSignIn.btnForgotPassword.onClick = onClickForgotPassword;
        frmSignIn.btnYes.onClick = function() {
            var createAccountResObj = kony.store.getItem("userObj");
            MVCApp.getJoinRewardsController().load(createAccountResObj);
        };
        frmSignIn.btnRewardProgramInfo.onClick = function() {
            entryBrowserFormName = "";
            MVCApp.getRewardsProgramInfoController().load(false);
        };
        /**
         * @function
         *
         */
        frmSignIn.btnNotRightNow.onClick = noRewardsProgram;
        frmSignIn.txtPassword.onDone = onClickSignIn;
        frmSignIn.txtEmail.onTextChange = function() {
            var text = frmSignIn.txtEmail.text;
            if (text.length > 0) {
                frmSignIn.btnClearEmail.setVisibility(true);
            } else {
                frmSignIn.btnClearEmail.setVisibility(false);
            }
        };
        frmSignIn.txtPassword.onTextChange = function() {
            var text = frmSignIn.txtPassword.text;
            if (text.length > 0) {
                frmSignIn.btnClearPassword.setVisibility(true);
            } else {
                frmSignIn.btnClearPassword.setVisibility(false);
            }
        };
        frmSignIn.btnClearEmail.onClick = function() {
            frmSignIn.btnClearEmail.setVisibility(false);
            frmSignIn.txtEmail.text = "";
        };
        frmSignIn.btnClearPassword.onClick = function() {
            frmSignIn.btnClearPassword.setVisibility(false);
            frmSignIn.txtPassword.text = "";
        };
    }

    function updateScreen(results) {
        isShowExecuted = false;
        var currentForm = "";
        if (gblIsPasswordExpired) {
            kony.store.setItem("userObj", "");
        }
        if (null !== kony.application.getCurrentForm() && undefined !== kony.application.getCurrentForm()) {
            currentForm = kony.application.getCurrentForm().id;
        }
        if (isFromShoppingList) {
            var message = "";
            var previousForm = kony.application.getPreviousForm();
            if (previousForm) {
                previousForm = previousForm.id;
            }
            if (currentForm === "frmSignIn") {
                isSigninForm = true;
            } else {
                isSigninForm = false;
            }
            if (isMyList) {
                message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.logInToViewSList");
            } else {
                if (currentForm == "frmPDP" || isPJDPAddProduct) {
                    message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.logInToAddProduct");
                } else {
                    message = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.logInToAddProject");
                }
            }
            if (!gblIsPasswordExpired) {
                MVCApp.Toolbox.common.customAlert(message, "", constants.ALERT_TYPE_CONFIRMATION, navigateToPreviousScreen, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSignIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            } else {
                navigateToPreviousScreen(true);
            }
        }
        var locale = kony.store.getItem("languageSelected");
        if (locale == "en_US" || locale == "en") {
            // frmSignIn.lblMsg.setVisibility(true);
            frmSignIn.lblMsg.setVisibility(false);
        } else {
            frmSignIn.lblMsg.setVisibility(false);
        }
        if (!isMyList && currentForm !== "frmPDP" && currentForm !== "frmPJDP") {
            if (!isShowExecuted) {
                show();
            }
        }
        if (isFromRewards && currentForm == "frmMore") {
            if (!isShowExecuted) {
                show();
            }
        }
    }

    function navigateToPreviousScreen(response) {
        var currentForm = kony.application.getCurrentForm().id;
        if (!response && !isSigninForm) {
            if (isMyList) {
                isMyList = false;
                kony.print("Cancel button is clicked - shopping list");
            } else {
                if (currentForm === "frmPDP" || currentForm === "frmPJDP") {
                    kony.print("Cancel button is clicked - PDP/PJDP");
                } else {
                    var previousForm = kony.application.getPreviousForm();
                    previousForm.show();
                }
            }
            isFromShoppingList = false;
        }
        if (response) {
            if (isFromRewards) {
                kony.print("from More Page");
            } else if (isMyList) {
                isMyList = false;
                isMyListflag = true;
            } else {
                kony.print("From PDP/PJDP page");
            }
            isShowExecuted = true;
            show();
        }
    }

    function noRewardsProgram() {
        var prevForm = kony.application.getPreviousForm();
        //DeepLink change
        var deepLinkFlag = MVCApp.getSignInController().getDeepLinkFlag();
        var deepLinkPrevName = MVCApp.getSignInController().getDeepLinkPrevName();
        //End
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.isRewardsProfileCreated = "false";
        MVCApp.Customer360.sendInteractionEvent("RewardsProfileCreationWithAccount", cust360Obj);
        if (prevForm == frmCoupons) {
            var userObject = kony.store.getItem("userObj");
            if (userObject === "" || userObject === null) {
                prevForm.lblRewardText.text = "SIGN IN TO VIEW MY REWARDS";
                prevForm.flxBarCodeWrap.isVisible = false;
                prevForm.flxNotUser.isVisible = true;
                prevForm.btnSignIn.isVisible = true;
                prevForm.btnSignUp.right = "51%";
            } else {
                var c_loyaltyPhoneNo = userObject.c_loyaltyPhoneNo || "";
                if (c_loyaltyPhoneNo !== "") {
                    prevForm.lblRewardText.text = userObject.first_name + " " + userObject.last_name + " REWARDS CARD";
                    prevForm.flxBarCodeWrap.isVisible = true;
                    prevForm.flxNotUser.isVisible = false;
                    prevForm.imgBarcode.src = MVCApp.serviceConstants.defaultLoyaltyCardBarcode + userObject.c_loyaltyMemberID || "";
                } else {
                    prevForm.lblRewardText.text = "SIGN IN TO VIEW MY REWARDS";
                    prevForm.flxNotUser.isVisible = true;
                    prevForm.btnSignIn.isVisible = false;
                    prevForm.btnSignUp.right = "35%";
                    prevForm.flxBarCodeWrap.isVisible = false;
                }
            }
            MVCApp.Toolbox.common.showLoadingIndicator("");
            //kony.application.getPreviousForm().show();
            MVCApp.getHomeController().loadCoupons();
        } else if (prevForm == frmWebView || prevForm == frmCartView) {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var count = MVCApp.Toolbox.common.getGuestBasketCount() || 0;
            if (count > 0) {
                if (gblUrl !== "" && gblUrl.indexOf("favorites") !== -1) {
                    var urlArr1 = gblUrl.split(/&/g);
                    var p_id = "";
                    for (var i = 0; i < urlArr1.length; i++) {
                        if (urlArr1[i].indexOf("pid") !== -1) {
                            p_id = urlArr1[i].split("=")[1];
                        }
                    }
                    MVCApp.Toolbox.common.setMerge(true);
                } else MVCApp.Toolbox.common.setMerge(true);
            }
            MVCApp.Toolbox.common.createBasketforUser(function() {
                MVCApp.Toolbox.common.setGuestBasketCount(0);
                if (gblUrl !== "") {
                    if (gblUrl.indexOf("pid") !== -1) {
                        var urlArr = gblUrl.split(/&/g);
                        var pid = "";
                        kony.print("fasfdsaf" + gblUrl.split(/&/g));
                        for (var i = 0; i < urlArr.length; i++) {
                            if (urlArr[i].indexOf("pid") !== -1) {
                                pid = urlArr[i].split("=")[1];
                            }
                        }
                        if (pid) {
                            var storeId = MVCApp.Toolbox.common.getStoreID();
                            if (prevForm == frmCartView) {
                                MVCApp.getCartController().loadCart(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Cart-MobileAppAddToWishlist?pid=" + pid + "", true);
                            } else MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Add?pid=" + pid + "");
                        }
                    } else {
                        //MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "COSinglePageCheckout/Start", true);
                        MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/COSinglePageCheckout-Start", true);
                    }
                    gblUrl = "";
                }
            });
        } else {
            if (isFromShoppingList) {
                if (frmObject.id === "frmPDP" || frmObject.id === "frmPJDP") {
                    var prodOrProjID = kony.store.getItem("shoppingListProdOrProjID");
                    frmObject.show();
                    kony.print("noRewardsProgram - prodOrProjID:" + prodOrProjID);
                    if (frmObject.id === "frmPDP") {
                        MVCApp.getShoppingListController().addToProductList(prodOrProjID, MVCApp.Service.Constants.ShoppingList.pdpType);
                    } else {
                        MVCApp.getShoppingListController().addProjectListLooping(prodOrProjID);
                    }
                } else {
                    MVCApp.getShoppingListController().getProductListItems();
                }
                isFromShoppingList = false;
            } else if (deepLinkFlag) {
                if (deepLinkPrevName == "frmHome") {
                    MVCApp.getHomeController().load();
                } else if (deepLinkPrevName == "frmProductCategory") {
                    frmProductCategory.show();
                } else if (deepLinkPrevName == "frmProductCategoryMore") {
                    frmProductCategoryMore.show();
                }
            } else {
                if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                    if (prevForm && (prevForm.id === "frmHome" || prevForm.id === "frmRegions")) {
                        MVCApp.getHomeController().load();
                    } else {
                        MVCApp.getMoreController().load();
                    }
                } else {
                    if (prevForm.id == "frmRegions") {
                        MVCApp.getHomeController().load();
                    } else {
                        MVCApp.getMoreController().load();
                    }
                }
            }
        }
    }

    function onClickSignIn() {
        fromTouchId = false;
        TealiumManager.trackEvent("Sign In click", {
            conversion_category: "Login Page",
            conversion_id: "Sign in",
            conversion_action: 2
        });
        if (validateFields()) {
            var reqParams = {};
            isMyListflag = false;
            reqParams.user = frmSignIn.txtEmail.text;
            reqParams.password = frmSignIn.txtPassword.text;
            MVCApp.getSignInController().validateLogin(reqParams);
        }
        setRequestMethod("UsernamePassword");
    }

    function onClickCreateAccount() {
        if (kony.application.getPreviousForm().id === "frmCartView" && MVCApp.getSignInController().getButtonTypeForCheckOutButton() !== true) {
            sendTealiumTagsForSignIn("Guest");
            MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/COSinglePageCheckout-Start", true);
            //MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "COSinglePageCheckout/Start", true);
        } else {
            MVCApp.getCreateAccountController().load();
        }
    }

    function onClickForgotPassword() {
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblIsFromHomeScreen) {
            gblIsFromHomeScreen = false;
        }
        MVCApp.getResetPasswordController().load();
    }

    function validateFields() {
        var email = frmSignIn.txtEmail.text;
        var password = frmSignIn.txtPassword.text;
        var regularEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var errorMsg = [];
        var errorFlag = false;
        if (email === null || email.trim() === "") {
            errorMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAddress"));
            errorFlag = true;
        } else {
            if (!regularEx.test(email.trim())) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
                return false;
            }
        }
        if (password === null || password.trim() === "") {
            errorMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.password"));
            errorFlag = true;
        }
        if (errorFlag) {
            var result = "";
            for (var i = 0; i < errorMsg.length; i++) {
                if (i == 0) {
                    result = result + errorMsg[i];
                } else {
                    result = result + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.and") + " " + errorMsg[i];
                }
            }
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.weNeed") + " " + result + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.toContinue"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
            return false;
        } else {
            return true;
        }
    }
    //touchID code
    function showTouchIDAlert(result) {
        //alert(kony.store.getItem("touchIdSaved"));
        if (kony.store.getItem("storeFirstRegionForTouchId") == kony.store.getItem("languageSelected")) {
            kony.store.setItem("touchIdSaved", true);
        }
        var showActAlert = kony.store.getItem("switch");
        if (result.hasOwnProperty("c_loyaltyMemberID")) {
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            var loyalty_id = result.c_loyaltyMemberID;
            cust360Obj.signinStatus = "success";
            cust360Obj.customerId = result.customer_id;
            cust360Obj.customerNumber = result.customer_no;
            cust360Obj.customerEmail = result.email;
            MVCApp.Customer360.sendInteractionEvent("SignIn", cust360Obj);
            sfmcCustomObject.Indiv_Id = result.customer_no;
            sfmcCustomObject.FirstName = result.first_name ? result.first_name : "";
            sfmcCustomObject.LastName = result.last_name ? result.last_name : "";
            kony.store.removeItem("loyaltyMemberId");
            kony.store.setItem("loyaltyMemberId", loyalty_id);
            if (OneManager) {
                OneManager.sendAttributes(JSON.stringify(sfmcCustomObject));
            }
            if (kony.store.getItem("touchIdSupport")) {
                if (kony.store.getItem("touchIdSaved")) {
                    loginResponse(result);
                } else {
                    if (showActAlert) {
                        var yesLabelText = "";
                        var alertMessage = "";
                        var devName = kony.os.deviceInfo().model;
                        yesLabelText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.turnOnTouchId");
                        alertMessage = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.anyOneWithFingerPrintAbleToSignIn");
                        var myalert = kony.ui.Alert({
                            message: alertMessage,
                            alertType: constants.ALERT_TYPE_CONFIRMATION,
                            alertTitle: "",
                            yesLabel: yesLabelText,
                            noLabel: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.mayBeLater"),
                            alertIcon: "transparent.png",
                            alertHandler: function(response1) {
                                kony.ui.dismissAlert(myalert);
                                if (response1) {
                                    var alertMessage = "";
                                    var devName = kony.os.deviceInfo().model;
                                    alertMessage = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.useTouchId");
                                    kony.print("alertMessage-------->" + alertMessage);
                                    kony.ui.Alert({
                                        message: alertMessage,
                                        alertType: constants.ALERT_TYPE_INFO,
                                        alertTitle: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.allSet"),
                                        yesLabel: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnContinue"),
                                        alertIcon: "transparent.png",
                                        alertHandler: function(response2) {
                                            if (response2) {
                                                kony.store.setItem("touchIdSaved", true);
                                                kony.store.setItem("switch", true);
                                                loginResponse(result);
                                                frmSettings.switchTouch.src = "on.png";
                                                var region = MVCApp.Toolbox.common.getRegion();
                                                //KNYMetricsService.sendEvent("Custom", null,"frmSignIn", "", null, {"region":region,"BiometricLogin":true});
                                                var reportDataObj = {
                                                    "region": region,
                                                    "BiometricLogin": true
                                                };
                                                var reportDataStr = JSON.stringify(reportDataObj);
                                                MVCApp.sendMetricReport(frmSignIn, [{
                                                    "SuccessfulSignIn": reportDataStr
                                                }]);
                                            } else {
                                                kony.store.setItem("touchIdSaved", false);
                                                kony.store.setItem("switch", false);
                                                frmSettings.switchTouch.src = "off.png";
                                                loginResponse(result);
                                            }
                                        }
                                    }, {});
                                } else {
                                    kony.store.setItem("touchIdSaved", false);
                                    loginResponse(result);
                                    kony.store.setItem("switch", false);
                                    frmSettings.switchTouch.src = "off.png";
                                }
                            }
                        }, {});
                    } else {
                        loginResponse(result);
                    }
                }
            } else {
                loginResponse(result);
            }
        } else {
            loginResponse(result);
        }
    }

    function loginResponse(response) {
        if (conf_alert != null && conf_alert != undefined) {
            kony.ui.dismissAlert(conf_alert);
            conf_alert = null;
        }
        var successState = "Successful";
        var prevFormObj = kony.application.getPreviousForm();
        var prevForm = "";
        if (prevFormObj !== null && prevFormObj !== undefined) {
            prevForm = prevFormObj.id;
        }
        var shopListFlag = false;
        //DeepLink change
        var deepLinkFlag = MVCApp.getSignInController().getDeepLinkFlag();
        //End
        if (response.hasOwnProperty("c_loyaltyMemberID")) {
            if (isFromShoppingList) {
                shopListFlag = true;
            }
            kony.store.setItem("userObj", response);
            setUserDetailsForTealiumTagging(response);
            if (kony.store.getItem("pushSwitch")) {
                var country = MVCApp.Toolbox.common.getRegion();
                var locale = kony.store.getItem("languageSelected");
                var deviceID = kony.os.deviceInfo().deviceid;
                MVCApp.PushNotification.common.modifyUser(deviceID + "@michaels.com", deviceID, locale, country);
            }
            var pswd;
            if (frmSignIn.txtPassword.text.length > 0) {
                pswd = frmSignIn.txtPassword.text;
            } else {
                pswd = kony.store.getItem("storeFirstPwdForTouchId");
            }
            kony.store.setItem("userCredential", pswd);
            var encryptedEmail = MVCApp.Toolbox.common.MVCApp_encryptPin(response.email);
            encryptedEmail = kony.convertToBase64(encryptedEmail);
            kony.store.setItem("username", encryptedEmail);
            var encryptedPass;
            encryptedPass = MVCApp.Toolbox.common.MVCApp_encryptPin(pswd);
            encryptedPass = kony.convertToBase64(encryptedPass);
            kony.store.setItem("userCredential", encryptedPass);
            if (!(prevForm == "frmWebView" || prevForm == "frmCartView")) {
                var count = MVCApp.Toolbox.common.getGuestBasketCount() || 0;
                if (count > 0) {
                    MVCApp.Toolbox.common.setMerge(true);
                } else {
                    MVCApp.Toolbox.common.setMerge(false);
                }
                MVCApp.Toolbox.common.createBasketforUser(function() {
                    MVCApp.Toolbox.common.setGuestBasketCount(0);
                });
            }
            gblDecryptedPassword = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            gblDecryptedUser = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
            //touchID code
            if (kony.store.getItem("touchIdSaved") && ((kony.store.getItem("storeFirstUserForTouchId") === null) || (kony.store.getItem("storeFirstUserForTouchId") === gblDecryptedUser))) {
                kony.store.setItem("storeFirstUserForTouchId", gblDecryptedUser);
                kony.store.setItem("storeFirstPwdForTouchId", gblDecryptedPassword);
                kony.store.setItem("storeFirstRegionForTouchId", kony.store.getItem("languageSelected"));
                kony.store.setItem("hideTouchIDAlertForLoginPage", false);
            }
            gblPassword = kony.store.getItem("userCredential");
            var loyaltyMemberId = response.c_loyaltyMemberID || "";
            var customer_no = response.customer_no || "";
            if (customer_no !== "") {
                kony.setUserID(customer_no);
            }
            var c_loyaltyEmail = response.c_loyaltyEmail || "";
            var c_loyaltyPhoneNo = response.c_loyaltyPhoneNo || "";
            //if(MVCApp.Toolbox.common.getRegion()=="US"){
            if ((prevForm != "frmGuide") && (loyaltyMemberId === "" && c_loyaltyEmail === "" && c_loyaltyPhoneNo === "")) {
                frmSignIn.flxOverlay.setVisibility(true);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            } else {
                var userObject = kony.store.getItem("userObj");
                if (prevForm == "frmRegions") {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.heyThere"));
                    //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"));
                    MVCApp.getHomeController().load();
                } else if (prevForm == "frmWebView" || prevForm == "frmCartView") {
                    var count = MVCApp.Toolbox.common.getGuestBasketCount() || 0;
                    if (count > 0) {
                        if (gblUrl !== "" && gblUrl.indexOf("favorites") !== -1) {
                            var urlArr1 = gblUrl.split(/&/g);
                            var p_id = "";
                            for (var i = 0; i < urlArr1.length; i++) {
                                if (urlArr1[i].indexOf("pid") !== -1) {
                                    p_id = urlArr1[i].split("=")[1];
                                }
                            }
                            MVCApp.Toolbox.common.setMerge(true);
                        } else MVCApp.Toolbox.common.setMerge(true);
                    }
                    MVCApp.Toolbox.common.createBasketforUser(function() {
                        MVCApp.Toolbox.common.setGuestBasketCount(0);
                        if (gblUrl !== "") {
                            if (gblUrl.indexOf("pid") !== -1) {
                                var urlArr = gblUrl.split(/&/g);
                                var pid = "";
                                kony.print("fasfdsaf" + gblUrl.split(/&/g));
                                for (var i = 0; i < urlArr.length; i++) {
                                    if (urlArr[i].indexOf("pid") !== -1) {
                                        pid = urlArr[i].split("=")[1];
                                    }
                                }
                                if (pid) {
                                    var storeId = MVCApp.Toolbox.common.getStoreID();
                                    if (prevForm == "frmCartView") {
                                        MVCApp.getCartController().loadCart(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Cart-MobileAppAddToWishlist?pid=" + pid + "", true);
                                    } else MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Add?pid=" + pid + "");
                                }
                            } else {
                                //MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "COSinglePageCheckout/Start", true);
                                MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/COSinglePageCheckout-Start", true);
                            }
                            gblUrl = "";
                        }
                    });
                } else {
                    if (prevForm === "frmPJDP" || prevForm === "frmPDP") {
                        MVCApp.getMyAccountController().load(userObject);
                    } else if (prevForm == "frmGuide") {
                        //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"));
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.heyThere"));
                        getProductsCategory();
                    } else if (deepLinkFlag) {
                        var userObj = kony.store.getItem("userObj");
                        MVCApp.getRewardsProfileController().load(userObj);
                    } else if (isFromRewards) {
                        var createAccountResObj = kony.store.getItem("userObj");
                        MVCApp.getRewardsProfileController().load(createAccountResObj);
                    } else {
                        //           MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"),"", constants.ALERT_TYPE_INFO,function handler(){},
                        //                                             MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),"");
                        if (conf_alert != null && conf_alert != undefined) {
                            kony.ui.dismissAlert(conf_alert);
                            conf_alert = null;
                        }
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.heyThere"));
                        MVCApp.getMyAccountController().load(userObject);
                    }
                }
            }
            /* }else{
                     noRewardsProgram();
                     MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.heyThere"));

                     //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"));
                     } */
        } else {
            var status = response.Status || [];
            successState = "Unsuccessful";
            shopListFlag = false;
            var errDesc = "";
            var errCode = "";
            var email = frmSignIn.txtEmail.text || "";
            if (status.length > 0) {
                errDesc = status[0].errDescription || "";
                errCode = status[0].errCode || "";
                //clearing text fields
                frmSignIn.txtEmail.text = "";
                frmSignIn.txtPassword.text = "";
                frmSignIn.btnClearEmail.setVisibility(false);
                frmSignIn.btnClearPassword.setVisibility(false);
                //END
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.emailPswdNotMatched"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
                if (kony.store.getItem("touchIdSupport")) {
                    if (fromTouchId) {
                        if (kony.store.getItem("storeFirstRegionForTouchId") != kony.store.getItem("languageSelected")) {
                            frmSignIn.txtEmail.text = kony.store.getItem("storeFirstUserForTouchId");
                            kony.store.setItem("touchIdSaved", false);
                        } else {
                            if (errCode === "err.AuthUser.PasswordChanged") {
                                kony.store.removeItem("storeFirstUserForTouchId");
                                kony.store.setItem("touchIdSaved", false);
                                kony.store.removeItem("storeFirstRegionForTouchId");
                            }
                        }
                    }
                }
            }
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.signinStatus = "failure";
            cust360Obj.failureCode = errDesc;
            cust360Obj.email = email;
            MVCApp.Customer360.sendInteractionEvent("SignIn", cust360Obj);
        }
        if (!shopListFlag) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
        kony.print("signin result for app comm   " + successState);
        sendTealiumTagsForSignIn(successState);
        gblIsPasswordExpired = false;
        currentFormForPasswordExpiryCase = null;
    }

    function sendTealiumTagsForSignIn(resultStatus) {
        var requestMethod = getRequestMethod() || "";
        if (requestMethod == "") {
            if (isTouchID) {
                requestMethod = "Touch ID";
            } else {
                requestMethod = "Face ID";
            }
        }
        if (resultStatus == "Guest") {
            requestMethod = "";
        }
        var entryPoint = getEntryPointForSignin() || "Default";
        TealiumManager.trackEvent("Sign in Page-App Commerce", {
            sign_in_trigger: entryPoint,
            sign_in_method: requestMethod,
            sign_in_result: resultStatus
        });
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        loginResponse: loginResponse,
        showTouchIDAlert: showTouchIDAlert
    };
});