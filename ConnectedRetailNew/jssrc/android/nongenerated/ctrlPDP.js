/**
 * PUBLIC
 * This is the controller for the PDP form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.PDPController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    var latLon = {};
    var fromBarcode = false;
    var lServiceRequestObj = {};
    var frmName = "";
    var entryType = "";
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.PDPModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.PDPView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    var lPageCategoryNameForPDP = "";

    function setPageCategoryNameForPDP(pPageCategoryNameForPDP) {
        lPageCategoryNameForPDP = pPageCategoryNameForPDP;
    }

    function getPageCategoryNameForPDP() {
        return lPageCategoryNameForPDP;
    }
    var lIsClearancePricePresent = false;

    function setIsClearancePricePresentFlag(pIsClearancePricePresent) {
        lIsClearancePricePresent = pIsClearancePricePresent;
    }

    function getIsClearancePricePresentFlag() {
        return lIsClearancePricePresent;
    }
    var lIsPromoPricePresent = false;

    function setIsPromoPricePresentFlag(pIsPromoPricePresent) {
        lIsPromoPricePresent = pIsPromoPricePresent;
    }

    function getIsPromoPricePresentFlag() {
        return lIsPromoPricePresent;
    }
    var lIsDropshipItemOnlineOnly = false;

    function setIsDropshipItemOnlineOnlyFlag(pIsDropshipItemOnlineOnly) {
        lIsDropshipItemOnlineOnly = pIsDropshipItemOnlineOnly;
    }

    function getIsDropshipItemOnlineOnlyFlag() {
        return lIsDropshipItemOnlineOnly;
    }
    var lIsDropshipItemSellable = false;

    function setIsDropshipItemSellableFlag(pIsDropshipItemSellable) {
        lIsDropshipItemSellable = pIsDropshipItemSellable;
    }

    function getIsDropshipItemSellableFlag() {
        return lIsDropshipItemSellable;
    }
    var paramsForStoreChange = {};

    function setParamsForStoreChange(params) {
        paramsForStoreChange = params;
    }

    function getParamsForStoreChange() {
        return paramsForStoreChange;
    }
    var productID = "";

    function setProductID(value) {
        productID = value;
    }

    function getProductID() {
        return productID;
    }

    function getPrimaryCategoryId() {
        return primary_category_id ? _data.primary_category_id : "Categories";
    }

    function setFromBarcode(value) {
        fromBarcode = value;
    }

    function getFromBarcode() {
        return fromBarcode;
    }

    function geoErrorcallback(positionerror) {
        var currentLatLon = {
            lat: "",
            lon: ""
        };
        view.setGeoLocation(false);
        var locationString = "";
        var locationDetails = {};
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            locationString = kony.store.getItem("myLocationDetails");
            locationDetails = JSON.parse(locationString);
            var lat = locationDetails.latitude;
            var lon = locationDetails.longitude;
            currentLatLon = {
                lat: lat,
                lon: lon
            };
            MVCApp.getPDPController().setLatLon(currentLatLon);
            MVCApp.getLocatorMapController().setStoreIDForTracking(locationDetails.clientkey || "");
        }
        MVCApp.getLocatorMapController().loadCheckNearby("frmPDP");
    }

    function setLatLon(latLonObj) {
        latLon = latLonObj;
    }

    function getLatLon() {
        return latLon;
    }

    function geoSuccesscallback(position) {
        view.setGeoLocation(true);
        var currentLatLon = {
            lat: "",
            lon: ""
        };
        if (position.coords.latitude !== "" && position.coords.longitude !== "") {
            var currentLat = position.coords.latitude;
            var currentLon = position.coords.longitude;
            currentLatLon = {
                lat: currentLat,
                lon: currentLon
            };
            setLatLon(currentLatLon);
        }
        MVCApp.getLocatorMapController().setStoreIDForTracking("");
        setLatLon(currentLatLon);
        MVCApp.getLocatorMapController().loadCheckNearby("frmPDP");
    }

    function getCurrentLocation() {
        kony.print("@@@ In getCurrentGeoLocation() @@@");
        var positionoptions = {
            timeout: 5000,
            enableHighAccuracy: true,
            useBestProvider: true
        };
        kony.location.getCurrentPosition(geoSuccesscallback, geoErrorcallback, positionoptions);
    }

    function setFormName(formName) {
        frmName = formName;
    }

    function getFormName() {
        return frmName;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    var _storeID = "";

    function setStoreID(storeID) {
        _storeID = storeID
    }

    function getStoreID() {
        return _storeID;
    }
    var variantFlag = false;

    function setHeaderVariantFlag(varFlag) {
        variantFlag = varFlag;
    }

    function getHeaderVariantFlag() {
        return variantFlag;
    }

    function load(productId, varientFlag, varientChangeFlag, headerTxt, formName, defaultVariantID, firstEnry, fromBack) {
        lServiceRequestObj.productId = productId;
        lServiceRequestObj.varientFlag = varientFlag;
        lServiceRequestObj.varientChangeFlag = varientChangeFlag;
        lServiceRequestObj.headerTxt = headerTxt;
        lServiceRequestObj.formName = formName;
        lServiceRequestObj.defaultVariantID = defaultVariantID;
        lServiceRequestObj.firstEnry = firstEnry;
        lServiceRequestObj.fromBack = fromBack;
        //         MVCApp.Toolbox.common.showLoadingIndicator("");
        kony.print("PDP Controller.load");
        init();
        setFormName(formName);
        //getCurrentLocation();
        setHeader(headerTxt);
        var expandParam = "";
        if (!varientFlag) {
            getAllImages(productId);
            expandParam = "images,prices,availability,variations,promotions,set_products";
        } else {
            expandParam = "images,prices,availability,promotions,set_products";
        }
        if (varientChangeFlag) {
            expandParam = "images,prices,availability,promotions,set_products";
        }
        var defVariantID = defaultVariantID || "";
        var requestParams = {};
        requestParams.locale = MVCApp.Toolbox.Service.getLocale();
        requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParams.expand = expandParam;
        requestParams.productID = productId;
        requestParams.all_images = "false";
        requestParams.defaultvariantProductId = defVariantID;
        var storeNo = "";
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeNo = gblInStoreModeDetails.storeID;
        } else {
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (storeString !== "") {
                try {
                    var storeObj = JSON.parse(storeString);
                    storeNo = storeObj.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlPDP.js on load for storeString:" + JSON.stringify(e)
                    }]);
                    storeNo = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception While Loading The PDP Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                storeNo = "";
            }
        }
        if (firstEnry) {
            setParamsForStoreChange(requestParams);
            setHeaderVariantFlag(varientFlag);
        }
        setStoreID(storeNo);
        gblTealiumTagObj.storeid = storeNo;
        requestParams.store_id = storeNo;
        setProductID(productId);
        var barCode = getFromBarcode();
        if (barCode) {
            MVCApp.Toolbox.common.clearPdpGlobalVariables();
        }
        if (!varientChangeFlag) {
            if (fromBack) {} else {
                view.defaultPDP();
            }
        }
        if (defVariantID == "") {
            setProductIDForTracking(productId);
        } else {
            setProductIDForTracking(defVariantID);
        }
        MVCApp.Toolbox.common.showLoadingIndicator("");
        kony.print("@@@LOG:load-ctrlPDP.js-varientChangeFlag:" + varientChangeFlag);
        model.loadPDP(view.updateScreen, requestParams, varientFlag, varientChangeFlag, getFromBarcode(), getFormName());
        var requestParams1 = {};
        var recommendedProductsRequired = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.PDP.recommendations");
        var recoFromCoremetrics = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.PDP.coremetrics");
        var serviceType = MVCApp.serviceType.Coremetrics;
        if (recommendedProductsRequired === "true") {
            if (recoFromCoremetrics === "true") {
                requestParams1.cm_targetid = productId;
            } else {
                serviceType = MVCApp.serviceType.Einstein;
                requestParams1.productId = productId;
            }
            if (!varientChangeFlag) {
                model.getRecommendedProducts(serviceType, view.updateRecommendedProducts, requestParams1, varientChangeFlag);
            } else {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    MVCApp.Toolbox.common.recommendedProductPush("", varientChangeFlag);
                }
            }
        } else {
            view.updateRecommendedProducts([]);
        }
    }

    function getAllImages(productId) {
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.productID = productId;
        requestParams.all_images = "true";
        model.loadAllImages(view.loadAllImages, requestParams, productId);
    }
    var headerTextNew;

    function setHeader(headerTxt) {
        headerTextNew = headerTxt;
        view.setHeaderText(headerTxt);
    }

    function getHeaderText() {
        return headerTextNew;
    }

    function showUI() {
        view.showPDP();
    }

    function backHistory() {
        if (!_isInit) {
            init();
        }
        view.backHistory();
    }

    function addToProductListCallBack(results) {
        view.addToProductListCallBack(results);
    }
    var prodIdForCheckNearBy = "";

    function getProductIDForCheck() {
        return prodIdForCheckNearBy;
    }

    function setProductIDForCheck(value) {
        prodIdForCheckNearBy = value;
    }
    var queryString = "";

    function getQueryString() {
        return queryString;
    }

    function setQueryString(value) {
        queryString = value;
    }
    var productIDForTracking = "";

    function getProductIDForTracking() {
        return productIDForTracking;
    }

    function setProductIDForTracking(value) {
        productIDForTracking = value;
    }

    function getEntryType() {
        return entryType;
    }
    var entrySearchType = "";

    function setSearchEntryType(value) {
        entrySearchType = value;
    }

    function setEntryType(value) {
        entryType = value;
        if (value == "plp") {
            entrySearchType = MVCApp.getProductsListController().getEntryType();
            setSearchEntryType(entrySearchType);
        } else {
            setSearchEntryType(value);
        }
    }

    function getSearchEntryType() {
        return entrySearchType;
    }

    function searchCheckNearby() {
        getCurrentLocation();
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        getAllImages: getAllImages,
        showUI: showUI,
        getLatLon: getLatLon,
        setFromBarcode: setFromBarcode,
        getFromBarcode: getFromBarcode,
        backHistory: backHistory,
        addToProductListCallBack: addToProductListCallBack,
        getFormName: getFormName,
        getProductID: getProductID,
        setLatLon: setLatLon,
        setStoreID: setStoreID,
        getStoreID: getStoreID,
        setParamsForStoreChange: setParamsForStoreChange,
        getParamsForStoreChange: getParamsForStoreChange,
        getHeaderText: getHeaderText,
        setHeaderVariantFlag: setHeaderVariantFlag,
        getHeaderVariantFlag: getHeaderVariantFlag,
        getProductIDForCheck: getProductIDForCheck,
        setProductIDForCheck: setProductIDForCheck,
        setQueryString: setQueryString,
        getQueryString: getQueryString,
        lServiceRequestObj: lServiceRequestObj,
        setFormName: setFormName,
        setIsClearancePricePresentFlag: setIsClearancePricePresentFlag,
        getIsClearancePricePresentFlag: getIsClearancePricePresentFlag,
        setIsPromoPricePresentFlag: setIsPromoPricePresentFlag,
        getIsPromoPricePresentFlag: getIsPromoPricePresentFlag,
        setIsDropshipItemOnlineOnlyFlag: setIsDropshipItemOnlineOnlyFlag,
        getIsDropshipItemOnlineOnlyFlag: getIsDropshipItemOnlineOnlyFlag,
        setIsDropshipItemSellableFlag: setIsDropshipItemSellableFlag,
        getIsDropshipItemSellableFlag: getIsDropshipItemSellableFlag,
        setPageCategoryNameForPDP: setPageCategoryNameForPDP,
        getPageCategoryNameForPDP: getPageCategoryNameForPDP,
        setProductIDForTracking: setProductIDForTracking,
        getProductIDForTracking: getProductIDForTracking,
        setEntryType: setEntryType,
        getEntryType: getEntryType,
        getSearchEntryType: getSearchEntryType,
        searchCheckNearby: searchCheckNearby
    };
});