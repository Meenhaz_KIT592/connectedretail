/**
 * PUBLIC
 * This is the controller for the Scan form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ScanController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.ScanModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.ScanView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load() {
        kony.print("Scan Controller.load");
        if (kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false) {
            rewardsToggle();
        }
        init();
        var lVersion = parseFloat(kony.os.deviceInfo().version);
        if (lVersion < 6.0) {
            kony.print("inside if condition");
            view.show();
        } else {
            kony.print("inside else condition");
            checkCamPermissionForAndroid();
        }
    }

    function getCameraPopup(pResponse) {
        if (pResponse) {
            redirectToSettings.redirectUserToCameraSettings();
        }
    }

    function checkCamPermissionForAndroid() {
        var lResult = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, {});
        // change made to make Allow from user to take user to Scanner view
        if (lResult.status == kony.application.PERMISSION_DENIED) {
            requestPermissionAtRuntime(kony.os.RESOURCE_CAMERA, function(pResponse) {
                if (pResponse.status == kony.application.PERMISSION_GRANTED) {
                    view.show();
                }
            });
        } else {
            view.show();
        }
        try {
            if (barcodeSettings == null || barcodeSettings == undefined) {
                barcodeSettings = new runtimepermissions.permissions();
                barcodeSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
                barcodeSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.barcodesettingpermission"));
                barcodeSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
                barcodeSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.cameraservices"));
            }
        } catch (e) {
            if (e) {
                kony.print("Exception while setting run time permissions" + e);
            }
        }
        barcodeSettings.requestPermission("android.camera.permission");
        /*if(lResult.status == kony.application.PERMISSION_DENIED){
          if(lResult.canRequestPermission){
            kony.print("LOG:checkCamPermissionForAndroid-Requesting for the permission");
            kony.application.requestPermission(kony.os.RESOURCE_CAMERA, permissionStatusCallback,{});
          }
        }else{
          kony.print("LOG:checkCamPermissionForAndroid-Permission Granted");
          view.show();
        }*/
    }

    function permissionStatusCallback(pResponse) {
        kony.print("LOG:permissionStatusCallback-pResponse::" + JSON.stringify(pResponse));
        if (pResponse.status == kony.application.PERMISSION_GRANTED) {
            kony.print("LOG:permissionStatusCallback-Permission Granted");
            view.show();
        } else {
            kony.print("LOG:permissionStatusCallback-Access Denied");
        }
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load
    };
});