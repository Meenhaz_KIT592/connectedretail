var MVCApp = MVCApp || {};
var gshoppingList = [];
var isFromShoppingList = false;
var isPJDPAddProduct = false;
var isMyList = false;
var gblIsPasswordExpired = false;
var currentFormForPasswordExpiryCase = null;
var projectIdDeleted = null;
var deletedFromProjects = false;
MVCApp.ShoppingListController = (function() {
    var lServiceRequestObj = {};
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.ShoppingListModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.ShoppingListView();
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    var storeNo = "";
    var isStoreModeAlreadyEnabled = false;

    function setStoreModeStatus(pIsStoreModeAlreadyEnabled) {
        isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
    }

    function getStoreModeStatus() {
        return isStoreModeAlreadyEnabled;
    }

    function getStoreID() {
        return storeNo;
    }

    function setStoreID(storeID) {
        storeNo = storeID;
    }

    function addToProductList(productId, type) {
        var userObject = kony.store.getItem("userObj");
        if (userObject === "" || userObject === null) {
            kony.store.setItem("shoppingListProdOrProjID", productId);
            kony.print("addToProductList - productId:" + kony.store.getItem("shoppingListProdOrProjID"));
            if (type == MVCApp.Service.Constants.ShoppingList.pjdpType) {
                isPJDPAddProduct = true;
            } else {
                isPJDPAddProduct = false;
            }
            isMyList = false;
            navigateToLoginScreen();
        } else {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            kony.print("shopping list addToProductList");
            init();
            var password = gblDecryptedPassword;
            var requestParams = {};
            requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
            requestParams.customer_id = userObject.customer_id;
            //     requestParams.list_id = "false";
            requestParams.type = MVCApp.Service.Constants.ShoppingList.pdpType;
            requestParams.product_id = productId;
            requestParams.public = MVCApp.Service.Constants.ShoppingList.public;
            requestParams.user = gblDecryptedUser; //userObject.login;
            requestParams.password = password;
            var storeNo = "";
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                storeNo = gblInStoreModeDetails.storeID;
            } else {
                var storeString = kony.store.getItem("myLocationDetails") || "";
                if (storeString !== "") {
                    var storeObj = JSON.parse(storeString);
                    storeNo = storeObj.clientkey;
                } else {
                    storeNo = "";
                }
            }
            requestParams.store_id = storeNo;
            if (type == MVCApp.Service.Constants.ShoppingList.pjdpType) {
                model.addToProductList(MVCApp.getPJDPController().addToProductListCallBack, requestParams, type, productId);
            } else {
                model.addToProductList(MVCApp.getPDPController().addToProductListCallBack, requestParams, type, productId);
            }
        }
    }
    var isFromBack = false;

    function setFromBack(value) {
        isFromBack = value;
    }

    function checkFromBack() {
        return isFromBack;
    }

    function createProductList() {
        init();
        var userObject = kony.store.getItem("userObj");
        var password = gblDecryptedPassword; //MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
        var requestParams = {};
        requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParams.customer_id = userObject.customer_id;
        requestParams.user = gblDecryptedUser; //userObject.login;
        requestParams.password = password;
        model.createProductList(requestParams);
    }

    function getProductListItems(fromBack) {
        lServiceRequestObj.fromBack = fromBack;
        var userObject = kony.store.getItem("userObj");
        var storeModeEnabled = false;
        currentFormForPasswordExpiryCase = kony.application.getCurrentForm();
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeModeEnabled = true;
        }
        MVCApp.sendMetricReport(frmShoppingList, [{
            "shoppingList": "appmenu" + " storeMode" + storeModeEnabled
        }]);
        init();
        if (userObject === "" || userObject === null) {
            isMyList = true;
            navigateToLoginScreen();
        } else {
            if (fromBack) {
                setFromBack(fromBack);
            } else {
                frmShoppingList.segShoppingList.removeAll();
                view.defaultShoppingList();
                frmShoppingList.show();
            }
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var password = gblDecryptedPassword;
            var requestParams = {};
            requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
            requestParams.customer_id = userObject.customer_id;
            requestParams.start = MVCApp.Service.Constants.ShoppingList.start;
            requestParams.expand = MVCApp.Service.Constants.ShoppingList.listItemExpand;
            requestParams.count = MVCApp.Service.Constants.ShoppingList.count;
            requestParams.user = gblDecryptedUser; //userObject.login;
            requestParams.password = password;
            var storeNo = "";
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                storeNo = gblInStoreModeDetails.storeID;
                setStoreModeStatus(true);
            } else {
                var storeString = kony.store.getItem("myLocationDetails") || "";
                if (storeString !== "") {
                    try {
                        var storeObj = JSON.parse(storeString);
                        storeNo = storeObj.clientkey;
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "ctrlShoppingList.js on getProductListItems for storeString:" + JSON.stringify(e)
                        }]);
                        storeNo = "";
                        if (e) {
                            TealiumManager.trackEvent("POI Parse Exception While Getting List Items For My List Screen", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                } else {
                    storeNo = "";
                }
            }
            setStoreID(storeNo);
            gblTealiumTagObj.storeid = storeNo;
            requestParams.store_id = storeNo;
            // model.getProductListItems(view.getProductListItems,requestParams);
            if (MVCApp.Toolbox.common.getRegion() == "US") {
                var url = "on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Show/";
                MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + url);
            } else {
                model.getProductListItems(view.getProductListItems, requestParams);
            }
        }
    }

    function removeFromProductList(itemId, projectId) {
        MVCApp.Toolbox.common.showLoadingIndicator("");
        init();
        var userObject = kony.store.getItem("userObj");
        var password = gblDecryptedPassword; //MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
        var requestParams = {};
        requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParams.customer_id = userObject.customer_id;
        requestParams.item_id = itemId;
        requestParams.user = gblDecryptedUser; //userObject.login;
        requestParams.password = password;
        if (projectId !== "") {
            requestParams.c_relatedProjects = [projectId];
        }
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.productid = itemId;
        cust360Obj.projectid = projectId;
        MVCApp.Customer360.sendInteractionEvent("DeleteFromMyList", cust360Obj);
        model.removeFromProductList(requestParams);
    }

    function confirmationAlert() {
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.youWantToDeleteProducts"), "", constants.ALERT_TYPE_CONFIRMATION, removeProductListLooping, "YES", "NO");
    }

    function removeProductListLooping(response) {
        if (response) {
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            MVCApp.Customer360.sendInteractionEvent("DeleteAll", cust360Obj);
            MVCApp.Toolbox.common.showLoadingIndicator("");
            var loopCount = gshoppingList.length;
            var userObject = kony.store.getItem("userObj");
            var password = gblDecryptedPassword; //MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            var itemId = "";
            for (var i = 0; i < loopCount; i++) {
                var itemData = gshoppingList[i];
                if (i < (loopCount - 1)) {
                    itemId = itemId + itemData.id + ",";
                } else {
                    itemId = itemId + itemData.id;
                }
            }
            var requestParams = {};
            requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
            requestParams.customer_id = userObject.customer_id;
            requestParams.item_id = itemId;
            requestParams.user = gblDecryptedUser; //userObject.login;
            requestParams.password = password;
            requestParams.loop_count = 0;
            //         requestParams. loop_count=loopCount;
            requestParams.loop_separator = ",";
            //type :product,product_id,customer_id,listid,username,password,client_id = "0426981f-5f77-4d24-8a46-4bb3077b3030";
            TealiumManager.trackEvent("DeleteAll Button Click From My List Page", {
                conversion_category: "My List",
                conversion_id: "Delete All",
                conversion_action: 2
            });
            model.removeProductListLooping(requestParams);
        }
    }

    function addProjectListLooping(projectId) {
        var userObject = kony.store.getItem("userObj");
        if (userObject === "" || userObject === null) {
            isPJDPAddProduct = false;
            kony.store.setItem("shoppingListProdOrProjID", projectId);
            isMyList = false;
            navigateToLoginScreen();
        } else {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            kony.print("shopping list addProjectListLooping");
            init();
            var loopCount = gProductList.length;
            var password = gblDecryptedPassword;
            var productId = "";
            for (var i = 0; i < loopCount; i++) {
                var itemData = gProductList[i];
                if (i < (loopCount - 1)) {
                    productId = productId + itemData.id + ",";
                } else {
                    productId = productId + itemData.id;
                }
            }
            var requestParams = {};
            requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
            requestParams.customer_id = userObject.customer_id;
            requestParams.type = MVCApp.Service.Constants.ShoppingList.pdpType;
            requestParams.product_id = productId;
            requestParams.public = MVCApp.Service.Constants.ShoppingList.public;
            requestParams.c_relatedProjects = [projectId];
            requestParams.user = gblDecryptedUser;
            requestParams.password = password;
            requestParams.loop_count = 0;
            requestParams.loop_separator = ",";
            var storeNo = "";
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (storeString !== "") {
                try {
                    var storeObj = JSON.parse(storeString);
                    storeNo = storeObj.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlShoppinList.js on addProjectListLooping for storeString:" + JSON.stringify(e)
                    }]);
                    storeNo = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception While Looping The Project List For My List Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                storeNo = "";
            }
            requestParams.store_id = storeNo;
            model.addProjectListLooping(MVCApp.getPJDPController().addToProjectListCallBack, requestParams, projectId);
        }
    }

    function showAfterLogin() {
        frmObject = kony.application.getCurrentForm();
        isFromShoppingList = true;
    }

    function askForLogin(message) {
        rewardsInfoFlag = false;
        MVCApp.Toolbox.common.customAlert(message, "", constants.ALERT_TYPE_CONFIRMATION, navigateToLoginScreen, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSignIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
    }

    function navigateToLoginScreen() {
        rewardsInfoFlag = false;
        showAfterLogin();
        MVCApp.getSignInController().load();
    }

    function showUI() {
        init();
        view.defaultShoppingList();
        frmShoppingList.show();
    }
    return {
        init: init,
        addToProductList: addToProductList,
        removeFromProductList: removeFromProductList,
        getProductListItems: getProductListItems,
        createProductList: createProductList,
        removeProductListLooping: removeProductListLooping,
        addProjectListLooping: addProjectListLooping,
        confirmationAlert: confirmationAlert,
        setStoreID: setStoreID,
        showUI: showUI,
        getStoreID: getStoreID,
        checkFromBack: checkFromBack,
        setFromBack: setFromBack,
        lServiceRequestObj: lServiceRequestObj,
        getStoreModeStatus: getStoreModeStatus,
        navigateToLoginScreen: navigateToLoginScreen
    };
});