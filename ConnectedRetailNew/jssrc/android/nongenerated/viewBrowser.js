/**
 * PUBLIC
 * This is the view for the browser form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.BrowserView = (function() {
    function show() {
        frmRewardsProgram.show();
    }

    function rewardsProgramPostShowCallback() {
        MVCApp.Toolbox.common.destroyStoreLoc();
        var lTealiumTagObj = gblTealiumTagObj;
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        if (MVCApp.getBrowserController().getContentID() === MVCApp.serviceConstants.createTermsandConditions) {
            lTealiumTagObj.page_id = "Terms and Conditions: Michaels Mobile App";
            lTealiumTagObj.page_name = "Terms and Conditions: Michaels Mobile App";
            lTealiumTagObj.page_type = "Policy";
            lTealiumTagObj.page_category_name = "Policy";
            lTealiumTagObj.page_category = "Policy";
            MVCApp.Customer360.sendInteractionEvent("TermsAndConditions", cust360Obj);
            TealiumManager.trackView("Terms and Conditions Screen", lTealiumTagObj);
        } else if (MVCApp.getBrowserController().getContentID() === MVCApp.serviceConstants.mobileRewardsFAQ) {
            MVCApp.Customer360.sendInteractionEvent("RewardsFAQ", cust360Obj);
        } else if (MVCApp.getBrowserController().getContentID() === MVCApp.serviceConstants.rewardTermsandConditions) {
            MVCApp.Customer360.sendInteractionEvent("RewardsTermsAndConditions", cust360Obj);
        } else {
            lTealiumTagObj.page_id = "Privacy Policy: Michaels Mobile App";
            lTealiumTagObj.page_name = "Privacy Policy: Michaels Mobile App";
            lTealiumTagObj.page_type = "Policy";
            lTealiumTagObj.page_category_name = "Policy";
            lTealiumTagObj.page_category = "Policy";
            TealiumManager.trackView("Privacy Policy Screen", lTealiumTagObj);
            MVCApp.Customer360.sendInteractionEvent("PrivacyPolicy", cust360Obj);
        }
    }

    function handleRequestCallback(browserWidget, params) {
        var URL = params.originalURL;
        if (URL.indexOf("mobile-privacypolicy.html") !== -1) {
            gblIsFromTermsAndConditions = true;
            //Code to navigate to Privacy Policy page
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
        }
        if (params.originalURL.indexOf(":") > 0) {
            var mailCheck = params.originalURL.split(":");
            if (mailCheck[0] == "mailto") {
                var regularEx = MVCApp.regEx.emailValidation;
                if (mailCheck[1] != undefined && mailCheck[1] != null) {
                    if (regularEx.test(mailCheck[1])) {
                        kony.phone.openEmail([mailCheck[1]]);
                        return true;
                    }
                }
            } else if (mailCheck[0] == "tel") {
                try {
                    kony.phone.dial(mailCheck[1]);
                } catch (err) {
                    kony.print("error in dial:: " + err);
                    if (err) {
                        TealiumManager.trackEvent("Dial Exception in Browser View Flow", {
                            exception_name: err.name,
                            exception_reason: err.message,
                            exception_trace: err.stack,
                            exception_type: err
                        });
                    }
                }
                return true;
            }
        }
        return false;
    }

    function handleRequestCallbackForAndroid(browserWidget, params) {
        var URL = params.originalURL;
        if (URL.indexOf("mobile-privacypolicy.html") !== -1 || URL.indexOf("https://mobile-privacypolicy") !== -1) {
            gblIsFromTermsAndConditions = true;
            //Code to navigate to Privacy Policy page
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
            return true;
        }
        return false;
    }

    function bindEvents() {
        try {
            frmRewardsProgram.btnHeaderLeft.onClick = showPreviousScreen;
            frmRewardsProgram.postShow = rewardsProgramPostShowCallback;
            frmRewardsProgram.browserWidget.handleRequest = handleRequestCallbackForAndroid;
        } catch (e) {
            kony.print(e);
            if (e) {
                TealiumManager.trackEvent("Handle Request Exception in Browser View Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function updateScreen(result) {
        frmRewardsProgram.browserWidget.enableParentScrollingWhenReachToBoundaries = false;
        frmRewardsProgram.lblFormTitle.text = result.name || "";
        var browserContent = result.c_body || "";
        browserContent = browserContent.replace("\r", "").replace("\n", "").replace("\t", "");
        browserContent = browserContent.replace("mobile-privacypolicy.html", "https://mobile-privacypolicy");
        frmRewardsProgram.browserWidget.htmlString = browserContent;
        show();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function showPreviousScreen() {
        var prevScreen = kony.application.getPreviousForm().id;
        if (prevScreen == "frmCreateAccount") {
            frmCreateAccount.show();
            MVCApp.tabbar.bindEvents(frmCreateAccount);
            MVCApp.tabbar.bindIcons(frmCreateAccount, "frmMore");
        } else if (prevScreen == "frmCreateAccountStep2") {
            frmCreateAccountStep2.show();
        } else if (prevScreen == "frmRewardsJoin") {
            frmRewardsJoin.show();
        } else if (prevScreen == "frmReviewProfile") {
            frmReviewProfile.show();
        } else if (prevScreen == "frmRewardsProgramInfo") {
            frmRewardsProgramInfo.show();
            MVCApp.tabbar.bindEvents(frmRewardsProgramInfo);
            MVCApp.tabbar.bindIcons(frmRewardsProgramInfo, "frmMore");
        } else if (prevScreen == "frmSettings") {
            MVCApp.Toolbox.common.setTransition(frmSettings, 2);
            frmSettings.show();
            MVCApp.tabbar.bindEvents(frmSettings);
            MVCApp.tabbar.bindIcons(frmSettings, "frmMore");
        } else if (prevScreen == "frmReviews") {
            retainReviewFields = true;
            MVCApp.ReviewsView().show();
        } else if (prevScreen == "frmCartView") {
            if (!isCartPageFromMyAccount) {
                MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
            } else {
                MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "Account");
            }
        } else if (entryBrowserFormName === "frmWebView") {
            var url = MVCApp.Toolbox.common.findWebViewUrlToGoBack();
            MVCApp.getProductsListWebController().loadWebView(url);
        } else {
            MVCApp.getHomeController().load();
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});