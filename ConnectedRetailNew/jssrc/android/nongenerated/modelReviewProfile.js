var MVCApp = MVCApp || {};
MVCApp.ReviewProfileModel = (function() {
    var serviceType = MVCApp.serviceType.SigninRewardsProfile;
    // var operationId = "updateRewardsProfile"; // MVCApp.serviceType.fetchCustomerInfo;
    var operationId = "loyaltyUpdate";

    function updateRewardProfile(callback, requestParams) {
        //  var loyalty_id=kony.store.getItem("loyaltyMemberId");
        var userObject = kony.store.getItem("userObj");
        var loyalty_id = userObject.c_loyaltyMemberID || "";
        requestParams.isLoyalty = "true";
        requestParams.c_loyaltyMemberID = loyalty_id;
        var langReg = kony.store.getItem("languageSelected");
        if (langReg == "en_CA") {
            requestParams.division = "MIKC";
        } else if (langReg == "fr_CA") {
            requestParams.division = "MIKQ";
        }
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            //alert(" results : "+JSON.stringify(results));
            if (undefined !== results.results && null !== results.results && "" !== results.results) {
                var updatedData = results.results || "";
                if (updatedData !== "") {
                    updatedData = JSON.parse(updatedData);
                    var fName = updatedData.first_name || "";
                    var lName = updatedData.last_name || "";
                    var phone = updatedData.c_loyaltyPhoneNo || "";
                    if (fName !== "" && lName !== "" && phone !== "") {
                        var userObj = kony.store.getItem("userObj");
                        userObj.first_name = fName;
                        userObj.last_name = lName;
                        userObj.c_loyaltyPhoneNo = phone;
                        kony.store.setItem("userObj", userObj);
                    }
                    // var timeStamp=new Date();
                    // var userObject=kony.store.getItem("userObj")
                    //  var loyaltyID=userObject.c_loyaltyMemberID;
                    // var reportObj={"timeStamp":timeStamp,"region":"US","loyaltyPhone":phone,"loyaltyID":loyaltyID};
                    //reportObj=JSON.stringify(reportObj);
                    // MVCApp.sendMetricReport(frmReviewProfile,[{"UpdateRewardsProfileSuccess":reportObj}])
                }
                callback();
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                var timeStamp = new Date();
                var reportObj = {
                    "timeStamp": timeStamp,
                    "reason": "Incorrect response"
                };
                reportObj = JSON.stringify(reportObj);
                MVCApp.sendMetricReport(frmReviewProfile, [{
                    "UpdateRewardsProfileFailure": reportObj
                }])
                alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
            }
        }, function(error) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            var timeStamp = new Date();
            var reportObj = {
                "timeStamp": timeStamp,
                "reason": "Service Failure"
            };
            reportObj = JSON.stringify(reportObj);
            MVCApp.sendMetricReport(frmReviewProfile, [{
                "UpdateRewardsProfileFailure": reportObj
            }])
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
        });
    }
    return {
        updateRewardProfile: updateRewardProfile
    };
});