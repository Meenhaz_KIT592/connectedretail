/**
 * PUBLIC
 * This is the controller for the StoreMap form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.SortProductController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    var categorySelectedArr = [];
    var selectedCategory = "";
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.SortProductView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    var attributes = [];

    function setAttributes(arrValue) {
        attributes = arrValue;
    }

    function getAttributes() {
        return attributes;
    }
    var fromSearchVal = false;

    function setSearchValue(value) {
        fromSearchVal = value;
    }

    function getSearchValue() {
        return fromSearchVal;
    }

    function getSelectedCategory() {
        return selectedCategory;
    }
    var productList = {};

    function setDataForProductList(value) {
        productList = value;
    }
    var attriRefSelected = [];

    function setSelectedRefinesAndAttributes(value) {
        attriRefSelected = value;
    }

    function getSelectedRefinesAndAttributes() {
        return attriRefSelected;
    }

    function setRefineandAttributes(selectedRef) {
        try {
            var selRefObj = JSON.parse(selectedRef);
            var attref = [];
            for (var key in selRefObj) {
                var temp = {};
                if (key !== "cgid") {
                    var refString = selRefObj[key] || "";
                    var refArr = refString.split("|");
                    temp = {
                        "attribute": key,
                        "refineValues": refArr
                    };
                    attref.push(temp);
                }
            }
            kony.print("attref" + JSON.stringify(attref));
            setSelectedRefinesAndAttributes(attref);
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "ctrlSortProducts.js on setRefineandAttributs for selectedRef:" + JSON.stringify(e)
            }]);
            if (e) {
                TealiumManager.trackEvent("Selected Attribute Refinement Parse Exception in Sort Products Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    var defaultParams = {};

    function setDefaultParamsFromProdList(value) {
        defaultParams = value;
    }

    function getDefaultParamsFromProdList() {
        return defaultParams;
    }

    function load(firstTimeEntry, dataProductsList) {
        kony.print("Sort Products  Controller.load");
        init();
        var firstTime = firstTimeEntry || false;
        var objForView = getRefinementObj();
        var selSort = getSelectedSort();
        var selectedRef = getSelectedRefinement();
        var selectedCategory = getSelectedCategory();
        setRefineandAttributes(selectedRef);
        if (!firstTimeEntry) {
            setDataForProductList(dataProductsList);
        } else {
            var requestParams = MVCApp.getProductsListController()._getRequestParams();
            setDefaultParamsFromProdList(requestParams);
        }
        view.showSortPage(objForView, selSort, selectedRef, selectedCategory, firstTime);
    }

    function setSelectedCategory(value) {
        selectedCategory = value;
    }

    function removeSelectedCategory() {
        setSelectedCategory("Categories");
    }
    var queryString = "";
    var refString = "";

    function setEntryQueryParam(query) {
        queryString = query;
    }

    function getEntryQueryParam() {
        return queryString;
    }
    var refinementObj = {};

    function setRefinementObj(referenceOBJ) {
        refinementObj = referenceOBJ;
        var sortArr = referenceOBJ.sortArr || [];
        var catArr = referenceOBJ.categoriesArr || [];
        var otherCount = referenceOBJ.projectCount || "";
    }

    function getRefinementObj() {
        return refinementObj;
    }
    var selectedRefinement = "";
    var selectedSort = "";

    function setSelectedRefinement(value) {
        selectedRefinement = value;
        if (selectedRefinement != "") {
            try {
                var selRef = JSON.parse(selectedRefinement);
                var category = selRef.cgid || "";
                setSelectedCategory(category);
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "ctrlSortProducts.js on setSelectedRefinement for selectedRefinement:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("Selected Refinement Parse Exception in Sort Products Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }

    function getSelectedRefinement() {
        return selectedRefinement;
    }

    function setSelectedSort(value) {
        selectedSort = value;
    }

    function getSelectedSort() {
        return selectedSort;
    }

    function getCorrespondingRefines(attr) {}

    function prepareInputParams() {
        var sort = getSelectedSort();
        var query = getEntryQueryParam();
        var category = getSelectedCategory();
        var attributes = getAttributes();
        var refStringCgid = "";
        var allRefineComponenets = getSelectedRefinesAndAttributes();
        if (allRefineComponenets.length !== 0) {
            refStringCgid = "refine_1=cgid=" + category;
            for (var i = 0; i < allRefineComponenets.length; i++) {
                var temp = allRefineComponenets[i];
                var attribute = temp.attribute || "";
                var refineSelected = temp.refineValues;
                var refineStringForAttr = "";
                for (var j = 0; j < refineSelected.length; j++) {
                    var refValue = refineSelected[j];
                    if (attribute == "c_size") {
                        refValue = refValue.replace(/"/g, "%22");
                    }
                    if (j + 1 == refineSelected.length) {
                        refineStringForAttr += refValue;
                    } else {
                        refineStringForAttr += refValue + "%7C";
                    }
                }
                refStringCgid += "&refine_" + (i + 2) + "=" + attribute + "=" + refineStringForAttr;
            }
        } else {
            refStringCgid = "refine=cgid=" + category;
        }
        return {
            query: query,
            sort: sort,
            category: category,
            refineString: refStringCgid
        };
    }

    function prepareForJustServiceCall() {
        var changedParams = prepareInputParams();
        var query = changedParams.query;
        var sort = changedParams.sort;
        var category = changedParams.category;
        var refString = changedParams.refineString;
        refString = refString.replace(/ /g, "%20");
        kony.print("the params are  " + JSON.stringify(changedParams));
        MVCApp.getProductsListController().loadFromSortRefine(query, refString, sort, true);
    }

    function goToProductList() {
        var changedParams = prepareInputParams();
        var query = changedParams.query;
        var sort = changedParams.sort;
        var category = changedParams.category;
        var refString = changedParams.refineString;
        refString = refString.replace(/ /g, "%20");
        var fromSearch = getSearchValue();
        var fromProdSubcat = MVCApp.getProductsListController().getFromProdSubCat();
        kony.print("the params are  " + JSON.stringify(changedParams));
        MVCApp.getProductsListController().load(query, fromSearch, sort, refString, true, false, fromProdSubcat);
    }

    function goToDefaultProdList() {
        var reqParams = getDefaultParamsFromProdList();
        var query = reqParams.q;
        var sort = reqParams.sort;
        var refString = reqParams.refineString;
        refString = refString.replace(/ /g, "%20");
        var fromSearch = getSearchValue();
        var fromProdSubcat = MVCApp.getProductsListController().getFromProdSubCat();
        kony.print("the params are  " + JSON.stringify(reqParams));
        MVCApp.getProductsListController().load(query, fromSearch, sort, refString, true, false, fromProdSubcat);
    }
    var _inputParamsForSearch = {};

    function setEntryInputParams(value) {
        _inputParamsForSearch = value;
        setEntryQueryParam(value.q);
    }

    function getEntryInputParams() {
        return _inputParamsForSearch;
    }

    function setEntryRefineString(refStr) {
        refString = refStr;
    }

    function setRefineString(value) {}

    function getRefineStringSortValue() {
        var inputParams = getEntryInputParams();
        var query = inputParams.q;
        var refString = inputParams.refineString;
        setEntryQueryParam(query);
        setEntryRefineString(refString);
    }

    function goToProjects() {
        var query = getEntryQueryParam();
        var fromSearch = getSearchValue();
        frmProjectsList.segProjectList.removeAll();
        frmProjectsList.flxRefineSearchWrap.isVisible = false;
        frmProjectsList.flxCategory.isVisible = false;
        frmProjectsList.flxSearchResultsContainer.isVisible = false;
        gblIsFromImageSearch = false;
        imageSearchProjectBasedFlag = false;
        frmProjectsList.show();
        MVCApp.getProjectsListController().load(query, true);
    }

    function loadListWithoutService() {
        var dataProducts = getDataForProductList();
        MVCApp.getProductsListController().loadListWOService(dataProducts);
    }
    var defaultCategory = "";
    var defaultSort = "";

    function setDefaultCategory(value) {
        defaultCategory = value;
    }

    function getDefaultCategory() {
        return defaultCategory;
    }

    function setDefaultSort(value) {
        defaultSort = value;
    }

    function getDefaultSort() {
        return defaultSort;
    }

    function removeAllFilters() {
        var params = MVCApp.getProductsListController().getDefaultParams();
        var query = getEntryQueryParam();
        var refineString = params.refineString;
        var sort = MVCApp.Service.Constants.Search.sort;
        view.clearAllGbls();
        MVCApp.getProductsListController().loadFromSortRefine(query, refineString, sort);
    }

    function getDataForProductList() {
        return productList;
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        setRefinementObj: setRefinementObj,
        setSelectedSort: setSelectedSort,
        setSelectedRefinement: setSelectedRefinement,
        getSelectedSort: getSelectedSort,
        getSelectedRefinement: getSelectedRefinement,
        goToProjects: goToProjects,
        prepareInputParams: prepareInputParams,
        setEntryInputParams: setEntryInputParams,
        setSearchValue: setSearchValue,
        getSearchValue: getSearchValue,
        setSelectedCategory: setSelectedCategory,
        getSelectedCategory: getSelectedCategory,
        removeSelectedCategory: removeSelectedCategory,
        prepareForJustServiceCall: prepareForJustServiceCall,
        loadListWithoutService: loadListWithoutService,
        setAttributes: setAttributes,
        getSelectedRefinesAndAttributes: getSelectedRefinesAndAttributes,
        setSelectedRefinesAndAttributes: setSelectedRefinesAndAttributes,
        goToProductList: goToProductList,
        setDefaultSort: setDefaultSort,
        setDefaultCategory: setDefaultCategory,
        removeAllFilters: removeAllFilters,
        setDataForProductList: setDataForProductList,
        getDataForProductList: getDataForProductList,
        getDefaultParamsFromProdList: getDefaultParamsFromProdList,
        goToDefaultProdList: goToDefaultProdList
    };
});