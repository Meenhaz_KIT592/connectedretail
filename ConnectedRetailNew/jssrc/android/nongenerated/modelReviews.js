/**
 * PUBLIC
 * This is the model for formProducts form
 */
var MVCApp = MVCApp || {};
MVCApp.ReviewsModel = (function() {
    var serviceType = MVCApp.serviceType.BazaarVoiceService;
    var operationId = MVCApp.serviceEndPoints.Reviews;

    function getReviews(callback, requestParams, name, flag) {
        kony.print("modelReviews.js");
        MakeServiceCall("BazaarVoiceServiceOrch", operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.HasErrors == "false") {
                var reviewsDataObj = new MVCApp.data.Reviews(results, requestParams, name, flag);
                callback(reviewsDataObj);
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.errorRetreivingReviews"));
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.errorRetreivingReviews"));
        });
    }
    /**
     * @function
     *
     */
    function submitReview(callback, requestParams) {
        var serviceType = MVCApp.serviceType.BazaarVoiceService;
        var operationId = "submitReview"; //MVCApp.serviceEndPoints.Reviews;
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            if (results.HasErrors == "false") {
                var nickKeyObj = {
                    nick: requestParams.nickname,
                    userKey: results.AuthorSubmissionToken
                }
                gblNickKeyPair.push(nickKeyObj);
                //gblUserKey=results.AuthorSubmissionToken;
                callback();
            } else {
                var errorsArr = results.Errors || [];
                var errMessage = "";
                var dupSubmission = false;
                for (var j = 0; j < errorsArr.length; j++) {
                    if (errorsArr[j].Code == "ERROR_DUPLICATE_SUBMISSION") {
                        errMessage = errorsArr[j].Message;
                        dupSubmission = true;
                    }
                }
                if (!dupSubmission) {
                    var formErrors = results.FormErrors || "";
                    if (formErrors !== "") {
                        try {
                            formErrors = JSON.parse(formErrors);
                            var fieldError = formErrors.FieldErrors || "";
                            var header = "";
                            if (fieldError !== "") {
                                for (var key in fieldError) {
                                    var header = fieldError[key] || "";
                                    if (header !== "") {
                                        errMessage += header.Message || "";
                                    }
                                }
                            }
                        } catch (e) {
                            MVCApp.sendMetricReport(frmHome, [{
                                "Parse_Error_DW": "modelReview.js on submitReview for formErrors:" + JSON.stringify(e)
                            }]);
                            formErrors = "";
                            if (e) {
                                TealiumManager.trackEvent("Form Errors Parse Exception in Reviews Model Flow", {
                                    exception_name: e.name,
                                    exception_reason: e.message,
                                    exception_trace: e.stack,
                                    exception_type: e
                                });
                            }
                        }
                    }
                }
                alert(errMessage);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            }
        }, function(error) {
            //alert("Error is "+JSON.stringify(error));
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        });
    }
    return {
        getReviews: getReviews,
        submitReview: submitReview
    };
});