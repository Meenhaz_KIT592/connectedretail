var gblDecryptedPassword = "";
var gblDecryptedUser = "";
var gblproducts = null;
var gblBrightNessValue = "";
var gblBrightNessMoreValue = "";
var gblbrightnessrewards = false;
var gblBrightNessrewardsValue = "";
var networkAlertPopupFlag = false;
var gblCouponFormName = "";
var gblHomeLoadDate = new Date();
var gblBeaconObjArr = []
var OneManager;
var gblTid = "";
var networkAlertInStore = false;
gblBrightnessfrmSignInPage = false;
var BeaconManagerObject;
var gblBluetoothEnabled = false;
var gblBeaconRangeFlag = false;
var gblNewBrightnessValueStoreMode = "";
var gblIsNewBrightnessValuePresent = false;
var appInForeground = true;
var isHomeFormLoaded = false;
var isPushRegisterInvokedInPrevVersion = true;
var initLaunchFlagForVersion72 = false;
var SlyceManager = null;
var OneManager = null;
var ifAppinForeGroundFirst = true;
var InstantiateBeaconManagerObject;
var gblBeaconMgmtArr = [];
var gblUniqueID = "";
var isAppInBGAnalytics = "stateless"; // this would be the case if the app hasn't gone to backGround or foreground even once
var sfmcCustomObject = {};
var locationSettings;
var cameraSettings;
var barcodeSettings;
var directorySettings;
var writesettings;
var recordAudioSettings;
var gblislocationon = false;
var gblFavStoreRelocated = false;
var isBackGround = false;

function onCallPostAppInit() {
    try {
        var appKey = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.OneManager.apiKey");
        var sharedSecret = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.OneManager.sharedSecret");
        var siteKey = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.OneManager.siteKey");
        var touchPointURL = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.OneManager.touchpointURI");
        var hostName = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.OneManager.hostName");
        var userId = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.OneManager.userId");
        var senderID = MVCApp.Toolbox.common.getSenderId();
        var sfmcAppid = MVCApp.sfmc.android.appid;
        var sfmcAccessToken = MVCApp.sfmc.android.accessToken;
        var isCustomerRegistered = kony.store.getItem("firstLaunchFor9") || "true";
        var pushSwitch = kony.store.getItem("pushSwitch") || false;
        if (pushSwitch && isCustomerRegistered) {
            var userObject = kony.store.getItem("userObj") || "";
            var firstname = userObject.first_name || "";
            var lastname = userObject.last_name || "";
            sfmcCustomObject.FirstName = firstname;
            sfmcCustomObject.LastName = lastname;
            kony.store.setItem("firstLaunchFor9", "false");
        }
        var appMode = {
            onbackground: fnCallOnBackground,
            onforeground: fnCallOnForeground,
            onlowmemory: fnCallOnLowMemory
        };
        kony.application.setApplicationCallbacks(appMode);
        KNYMetricsService.setEventConfig("Buffer", 20, 200);
        KNYMetricsService.setEventTracking(["FormEntry", "Touch", "ServiceRequest", "ServiceResponse", "Gesture", "Error", "Crash"]);
        // SlyceManager = new Slyce.RequestManager();
        OneManager = new Customer360.OneManagerAndroid(siteKey, touchPointURL, appKey, sharedSecret, userId, hostName, sfmcAppid, sfmcAccessToken, senderID);
        if (OneManager) {
            OneManager.sendAttributes(JSON.stringify(sfmcCustomObject));
        }
        /*if(SlyceManager){
		SlyceManager.initialize();
    }*/
    } catch (e) {
        kony.print("in onCallPostAppInit exception: " + e);
        try {
            // SlyceManager = new Slyce.RequestManager();
            //SlyceManager.initialize();
            OneManager = new Customer360.OneManagerAndroid(siteKey, touchPointURL, appKey, sharedSecret, userId, hostName, sfmcAppid, sfmcAccessToken, senderID);
        } catch (ex) {
            kony.print("Slyce initialization exception: " + ex);
            try {
                OneManager = new Customer360.OneManagerAndroid(siteKey, touchPointURL, appKey, sharedSecret, userId, hostName, sfmcAppid, sfmcAccessToken, senderID);
            } catch (oneex) {
                kony.print("OneManager initialization exception: " + oneex);
            }
        }
    }
}

function requestPermissionAtRuntime(requestedResourceId, statusCallback) {
    kony.application.requestPermission(requestedResourceId, statusCallback, {});
}

function permissionAlertHandler(resp) {
    if (resp) kony.application.openApplicationSettings();
}

function onCallPostAppInitAndroid() {
    try {
        gblCurrentLocationPermission = kony.application.checkPermission(kony.os.RESOURCE_LOCATION, {});
        if (gblCurrentLocationPermission.status == kony.application.PERMISSION_GRANTED) {
            gblIsLocationPermissionGranted = true;
        } else if (gblCurrentLocationPermission.status == kony.application.PERMISSION_DENIED) {
            gblIsLocationPermissionDenied = true;
        }
        if (MVCApp.Toolbox.Service.getFlagForInstallation() != "true") {
            firstLaunchFromPrevKonyVersionFlag = kony.store.getItem("firstLaunchFromPrevKonyVersionFlag");
            if (firstLaunchFromPrevKonyVersionFlag == "false") {
                if (resourceManager.isBluetoothAvailable() && gblIsLocationPermissionGranted && resourceManager.isGPSAvailable()) {
                    initializeEstimoteLibraries();
                }
            }
        }
    } catch (e) {
        kony.print("in onCallPostAppInit exception" + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("PostAppInit Lifecycle Event Exception", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
    checkIffavStoreRelocated();
}

function fnCallOnLowMemory() {
    try {
        TealiumManager.trackEvent("Memory Warning from iOS", {
            memory_warning: true
        });
    } catch (pError) {
        kony.print("LOG:fnCallOnLowMemory-pError:" + JSON.stringify(pError));
        if (pError) {
            TealiumManager.trackEvent("onLowMemory Lifecycle Event Exception", {
                exception_name: pError.name,
                exception_reason: pError.message,
                exception_trace: pError.stack,
                exception_type: pError
            });
        }
    }
}

function checkIffavStoreRelocated() {
    var myLocationString = kony.store.getItem("myLocationDetails")
    if (myLocationString) {
        var inputParams = {};
        inputParams.storeID = MVCApp.Toolbox.common.getFavouriteStore();
        inputParams.country = MVCApp.Service.Constants.Common.country;
        var serviceType = MVCApp.serviceType.LocatorService;
        var operationId = MVCApp.serviceType.fetchINStoreDetailsByID;
        MakeServiceCall(serviceType, operationId, inputParams, function(Results) {
            kony.print("LOG:fetchInStoreModeStoreIfInvalidStore-Results:" + JSON.stringify(Results));
            var grantedLocPermission = locationSettings.verifyDenySetting("android.access_fine_location.permission");
            if (Results.opstatus === 0 || Results.opstatus === "0") {
                if (!Results.collection || !Results.collection.length > 0) {
                    kony.store.setItem("myLocationDetails", "");
                    MVCApp.Toolbox.Service.setMyLocationFlag(false);
                    if (grantedLocPermission) {
                        gblFavStoreRelocated = true;
                        var positionoptions = {
                            timeout: 5000,
                            enableHighAccuracy: true,
                            useBestProvider: true
                        };
                        kony.location.getCurrentPosition(geoSuccesscallback, geoErrorcallback, positionoptions);
                    } else {
                        gblFavStoreRelocated = true;
                        geoErrorcallback();
                    }
                } else {
                    var collection = Results.collection || [];
                    var poiColl = collection[0] || "";
                    var poi = poiColl.poi || "";
                    var poiString = "";
                    if (poi !== "") {
                        poiString = JSON.stringify(poi);
                        kony.store.setItem("myLocationDetails", poiString);
                    }
                }
            }
        }, function(Error) {
            kony.print("LOG:fetchInStoreModeStoreDetailsIfInvalidStore-Error:" + JSON.stringify(Error));
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        });
    }
}

function initRuntimePermissionObjects() {
    locationSettings = new runtimepermissions.permissions();
    locationSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
    locationSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.locationsettingpermission"));
    locationSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
    locationSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.locationservices"));
    cameraSettings = new runtimepermissions.permissions();
    cameraSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
    cameraSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.camerasettingpermission"));
    cameraSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
    cameraSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.cameraservices"));
    barcodeSettings = new runtimepermissions.permissions();
    barcodeSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
    barcodeSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.barcodesettingpermission"));
    barcodeSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
    barcodeSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.cameraservices"));
    directorySettings = new runtimepermissions.permissions();
    directorySettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
    directorySettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.directorysettingpermission"));
    directorySettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
    directorySettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.mediaservices"));
    writesettings = new runtimepermissions.permissions();
    writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow"));
    writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
    writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
    writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
    writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
    recordAudioSettings = new runtimepermissions.permissions();
    recordAudioSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
    recordAudioSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.recordaudiopermission"));
    recordAudioSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
    recordAudioSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.audioservices"));
}

function fnCallOnBackground() {
    try {
        isBackGround = true;
        kony.print("in background");
        MVCApp.Toolbox.common.setRegisterforTimeOut();
        appInForeground = false;
        if (!gblInStoreModeDetails || !gblInStoreModeDetails.isInStoreModeEnabled) {
            stopForegroundStoreModeTimer();
            startBackgroundStoreModeTimer();
        }
        var currentForm = kony.application.getCurrentForm();
        if (currentForm && currentForm.id === "frmHome") {
            kony.timer.cancel("HomeCarouselTimer");
        }
    } catch (e) {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        kony.print("in background exception " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("onBackground Lifecycle Event Exception", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function fnCallOnForeground() {
    try {
        kony.print("fnCallOnForeground");
        MVCApp.Toolbox.common.setRegisterforTimeOut();
        if (isBackGround == true) {
            isBackGround = false;
            if (!MVCApp.Toolbox.common.checkIfGuestUser() && MVCApp.Toolbox.common.verifyTokenExpiredOrNot()) {
                MVCApp.getHomeController().load();
            }
            MVCApp.Toolbox.common.verifyExpiryFlowOfAuthUser();
        } else MVCApp.Toolbox.common.appLaunchFlowforAuth();
        var currentForm = kony.application.getCurrentForm();
        var lPreviousForm = kony.application.getPreviousForm();
        kony.print("in foreground");
        var locPermission = false;
        if (currentForm && currentForm.id == "frmFindStoreMapView") {
            var fromCheckNearby = MVCApp.getLocatorMapController().getCheckNearby() || false;
            if (!fromCheckNearby) {
                gblCurrentLocationPermission = kony.application.checkPermission(kony.os.RESOURCE_LOCATION, {});
                if (gblCurrentLocationPermission.status == kony.application.PERMISSION_GRANTED) {
                    MVCApp.getLocatorMapController().load("frmHome");
                }
            }
        }
        onBckToForegroundAtImageSeach();
        if (ifAppinForeGroundFirst) {
            configureTealiumTagObj();
            if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
                loadi18nValues();
                versionCheck();
                MVCAppAnalytics.executeBufferList();
                MVCApp.Toolbox.common.sendBeaconDataToAnalytics();
                if (MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.reviewSwitch") === "true") {
                    initialiseTwoStageRating();
                }
            }
            onCallPostAppInitAndroid();
            if (MVCApp.Toolbox.Service.getFlagForInstallation() != "true") {
                firstLaunchFromPrevKonyVersionFlag = kony.store.getItem("firstLaunchFromPrevKonyVersionFlag");
                if ((firstLaunchFromPrevKonyVersionFlag == "false") && (isPushRegisterInvokedInPrevVersion || !initLaunchFlagForVersion72)) {
                    gblInStoreModeDetails.isAppLaunch = true;
                    getProductsCategory();
                }
            }
            var appLaunch = kony.store.getItem("firstLaunchIntallation") || "true";
            if (appLaunch == "true") {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.isFirstLaunch = "true";
                cust360Obj.launchType = "appIconClick";
                MVCApp.Customer360.sendBaseTouchPoint(cust360Obj);
            } else {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.isFirstLaunch = "false";
                MVCApp.PushNotification.common.registerPush();
                if (!appLaunchCust360Sent) {
                    cust360Obj.launchType = "appIconClick";
                    MVCApp.Customer360.sendBaseTouchPoint(cust360Obj);
                }
            }
            ifAppinForeGroundFirst = false;
            registerPushForVersion85Plus();
            MVCApp.Toolbox.common.setRegisterforTimeOut();
            // MVCApp.Toolbox.common.verifyExpiryFlowOfAuthUser(); 
        } else {
            if (currentForm && currentForm.id === "frmHome") {
                MVCApp.searchBar.homePageLoadingHandler(true);
            }
        }
        kony.print("in foreground---------------------->");
        appInForeground = true;
        if (!gblInStoreModeDetails || !gblInStoreModeDetails.isInStoreModeEnabled) {
            stopBackgroundStoreModeTimer();
            checkWiFiAndGeoFenceForStoreMode();
            startForegroundStoreModeTimer();
        } else {
            var storeModeExitThresholdTime = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
            var d = new Date();
            var currTime = Math.round(d.getTime() / 1000);
            if ((currTime - gblInStoreModeDetails.entryTimestamp) > (storeModeExitThresholdTime * gblInStoreModeDetails.storeModeCounter)) {
                checkWiFiAndGeoFenceForStoreMode();
            }
        }
        if (currentForm && currentForm.id === "frmScan") {
            kony.print("Inside barcode scanner flow");
            frmScan.cusBarCodeWid.startCamera();
        }
        if (imageSearchGalleryFlag && currentForm && lPreviousForm && lPreviousForm.id === "frmImageOverlay") {
            currentForm.flxHeaderWrap.cmrImageSearch.openCamera();
        }
    } catch (e) {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        kony.print("in foreground exception " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("onForeground Lifecycle Event Exception", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function registerPushForVersion85Plus() {
    var customerRegistered = kony.store.getItem("firstLaunchFor86") || "true";
    var pushSwitch = kony.store.getItem("pushSwitch") || false;
    if (customerRegistered !== "false") {
        if (pushSwitch) {
            var regId = kony.store.getItem("pushnativeid") || "";
            if (regId !== "") {
                MVCApp.PushNotification.common.subscribeSFMCPushMessaging(regId);
                kony.store.setItem("firstLaunchFor86", "false");
            }
        }
    }
}

function showEmailWithFeedback(response) {
    var emailId = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.customerEmailApp");
    var sub = "";
    var msgbody = response;
    var cc = [""];
    var bcc = [""];
    var deviceInfo = kony.os.deviceInfo();
    var osVersion;
    var handSet = deviceInfo.model;
    var appVersion = appConfig.appVersion;
    osVersion = deviceInfo.version;
    sub = "Android " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.help.feedback") + " - " + osVersion + ", " + handSet + ", " + appVersion;
    kony.phone.openEmail([emailId], cc, bcc, sub, msgbody, false, []);
}

function initialiseTwoStageRating() {
    try {
        var rateTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.rateTitle");
        var rateLater = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.rateLater");
        var rateNever = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.rateNever");
        var feedbackTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.feedbackTitle");
        var feedbackDescription = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.feedbackDescription");
        var feedbackNegative = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.feedbackNegative");
        var feedBackPositive = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.feedbackPositive");
        var eventCount = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.androidRateApp.numberOfEvents");
        var PromptRatingRequestObject = new TwoStageRate.PromptRatingRequest(eventCount, rateTitle, rateLater, rateNever, feedbackTitle, feedbackDescription, feedbackNegative, feedBackPositive, function(response) {
            showEmailWithFeedback(response);
        });
    } catch (e) {
        kony.print("Error in initialising the twoStageRateApp");
    }
}

function initializeStartUpForm(params) {
    isTouchSupported();
    if (null != kony.store.getItem("userCredential") && undefined != kony.store.getItem("userCredential") && "" != kony.store.getItem("userCredential")) {
        gblDecryptedPassword = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
    }
    kony.print("user obj----" + JSON.stringify(kony.store.getItem("username")));
    if (null != kony.store.getItem("username") && undefined != kony.store.getItem("username") && "" != kony.store.getItem("username")) {
        gblDecryptedUser = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
    }
    kony.print("gblDecryptedUser---" + gblDecryptedUser + "--gblDecryptedPassword--" + gblDecryptedPassword);
    setTheme();
    localNotCallBacks();
    MVCApp.PushNotification.common.setupPushCallbacks();
}

function initializeEstimoteLibraries() {
    BeaconManagerObject = new Beacon.BeaconManager();
    BeaconManagerObject.initializeEstimote(MVCApp.beacon.android.appid, MVCApp.beacon.android.accessToken, 30, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.beacon.uuid"));
    BeaconManagerObject.startMonitorListener(callerEnterCallBack, callerExitCallBack, callMgmtCallback);
}

function callMgmtCallback(result) {
    if (result) {
        //alert("Beacon results----->"+result);
        var deviceID = MVCApp.Toolbox.common.getDeviceID();
        var deviceName = kony.os.deviceInfo().name;
        var loyaltyID = "";
        var userObject = kony.store.getItem("userObj");
        if (userObject && userObject.c_loyaltyMemberID !== null && userObject.c_loyaltyMemberID !== undefined) {
            loyaltyID = userObject.c_loyaltyMemberID;
        }
        var beaconMgmtArray = result.split("~");
        if (beaconMgmtArray.length == 6) {
            var beaconMgmtObj = {};
            beaconMgmtObj.batteryPercentage = beaconMgmtArray[0];
            beaconMgmtObj.batteryLifetime = beaconMgmtArray[1];
            beaconMgmtObj.batteryVoltage = beaconMgmtArray[2];
            beaconMgmtObj.major = beaconMgmtArray[3];
            beaconMgmtObj.minor = beaconMgmtArray[4];
            beaconMgmtObj.proximityUUID = beaconMgmtArray[5];
            beaconMgmtObj.deviceID = deviceID;
            beaconMgmtObj.loyaltyID = loyaltyID;
            beaconMgmtObj.deviceName = deviceName;
            beaconMgmtObj.deviceTimeStamp = new Date();
            MVCApp.sendMetricReport(frmHome, [{
                "beacon": JSON.stringify(beaconMgmtObj)
            }]);
            gblBeaconMgmtArr.push(beaconMgmtObj);
            sendMetricForBeacons();
            TealiumManager.trackEvent("Store Visit Beacon", {
                event_type: "registration",
                conversion_category: "Store Visit",
                conversion_id: "Beacon",
                conversion_action: 2,
                storeid: beaconMgmtObj.major,
                device_time_stamp: JSON.stringify(beaconMgmtObj.deviceTimeStamp),
                device_id: beaconMgmtObj.deviceID,
                loyalty_id: beaconMgmtObj.loyaltyID,
                device_type: beaconMgmtObj.deviceName
            });
        }
    }
}

function avail(arr, obj) {
    var k = -1;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == obj) {
            k = i;
        }
    }
    return k;
}

function callerEnterCallBack(result) {
    kony.print("result-------------%%%" + JSON.stringify(result));
    gblBeaconRangeFlag = true;
    storeChangeInStoreMode = false;
    gblInStoreModeDetails = kony.store.getItem("inStoreModeDetails") || {
        isInStoreModeEnabled: false,
        storeModeCounter: 1
    };
    if (result && gblInStoreModeDetails.isInStoreModeEnabled) {
        if ((result.split("~")[1]) != gblInStoreModeDetails.storeID) {
            gblInStoreModeDetails.differentStoreFlag = true;
        }
    }
    if (!gblInStoreModeDetails.isInStoreModeEnabled || (gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.differentStoreFlag)) {
        if (gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.differentStoreFlag) {
            gblInStoreModeDetails.storeModeExtensionFlag = false;
            try {
                kony.timer.cancel("StoreModeTimer");
            } catch (e) {
                if (e.errorCode == 102) {
                    kony.print("Timer Cancel: StoreModeTimer is already cancelled -----> " + JSON.stringify(e));
                }
                if (e) {
                    TealiumManager.trackEvent("Store Mode Timer Exception In Caller Enter Callback", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        var d = new Date();
        var currTime = Math.round(d.getTime() / 1000);
        var uuid_array = [];
        uuid_array = kony.store.getItem("beacon_array");
        var time_interval = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
        if (null != uuid_array && uuid_array.length > 0) {
            kony.print("uuid_array----" + uuid_array);
            var isAvailable;
            var set_of_uuids = [];
            for (var k = 0; k < uuid_array.length; k++) {
                set_of_uuids.push(uuid_array[k]["uuid"]);
            }
            kony.print("set_of_uuids ----" + JSON.stringify(set_of_uuids));
            isAvailable = avail(set_of_uuids, result);
            kony.print("isAvailable---" + isAvailable);
            if (isAvailable == -1) {
                var b = {
                    "uuid": result,
                    "time": currTime
                };
                uuid_array.push(b);
                kony.store.setItem("beacon_array", uuid_array);
                MVCApp.getHomeController().beaconsDetails(result.split("~")[2], result.split("~")[1], result.split("~")[0]);
            } else {
                if ((currTime - uuid_array[isAvailable]["time"]) > time_interval) {
                    kony.print(currTime - kony.store.getItem("interval"));
                    var b = {
                        "uuid": result,
                        "time": currTime
                    };
                    uuid_array[isAvailable] = b;
                    var index = uuid_array.indexOf(result);
                    kony.store.setItem("beacon_array", uuid_array);
                    MVCApp.getHomeController().beaconsDetails(result.split("~")[2], result.split("~")[1], result.split("~")[0]);
                }
            }
        } else {
            uuid_array = [];
            var b = {
                "uuid": result,
                "time": currTime
            };
            uuid_array.push(b);
            kony.store.setItem("beacon_array", uuid_array);
            MVCApp.getHomeController().beaconsDetails(result.split("~")[2], result.split("~")[1], result.split("~")[0]);
        }
    }
    kony.print("beacon arayyyyyy---" + kony.store.getItem("beacon_array"));
}

function callerExitCallBack(result) {
    kony.print("callerExitCallBack---------" + JSON.stringify(result));
    gblBeaconRangeFlag = false;
}

function preappInit_IOS() {}

function preapppInit_android() {
    preAppInitCallback();
}

function preAppInitCallback() {
    gblInStoreModeDetails = kony.store.getItem("inStoreModeDetails") || {
        isInStoreModeEnabled: false,
        storeModeCounter: 1
    };
    initAppData();
    storeChangeInStoreMode = false;
    customAppppInitCallback();
}

function customAppppInitCallback() {
    //add existing pre app init code here.. 
    gblInStoreModeDetails = kony.store.getItem("inStoreModeDetails") || {
        isInStoreModeEnabled: false,
        storeModeCounter: 1
    };
    storeChangeInStoreMode = false;
    //initAppData();
    var userObject = kony.store.getItem("userObj");
    if (userObject != null && userObject != undefined && userObject != "") {
        var customer_no = userObject.customer_no || "";
        if (customer_no !== "") {
            kony.setUserID(customer_no);
        }
    } else {
        sfmcCustomObject.Indiv_Id = "";
    }
    var selectedLang = kony.store.getItem("languageSelected") || "";
    var selectedRegion = "";
    var country = "";
    var devLang = kony.i18n.getCurrentDeviceLocale();
    if (selectedLang == "") {
        var languageAndSupport = MVCApp.Toolbox.common.isLocaleSupportedInApp(devLang);
        //var languageAndSupport={};
        var support = false;
        var IOSSupport = false;
        var language = languageAndSupport.language || "";
        support = languageAndSupport.support || false;
        var region = languageAndSupport.region || "";
        var iosLanguage = languageAndSupport.iosLanguage || "";
        if (iosLanguage === "en" && region === "CA") {
            kony.store.setItem("languageSelected", "en_CA");
        } else if (iosLanguage === "fr" && region === "CA") {
            kony.store.setItem("languageSelected", "fr_CA");
        } else if (region === "US") {
            kony.store.setItem("languageSelected", "en");
        } else if (!IOSSupport && region === "CA") {
            kony.store.setItem("languageSelected", "en_CA");
        } else if (support) {
            kony.store.setItem("languageSelected", language);
        } else {
            kony.store.setItem("languageSelected", "en");
        }
    }
    var langSetNew = kony.store.getItem("languageSelected");
    if (langSetNew == "en" || langSetNew == "en_US") {
        selectedRegion = "US";
    } else {
        selectedRegion = "CA";
    }
    sfmcCustomObject.Region = (langSetNew == "fr_CA") ? "fr" : langSetNew;
    kony.store.setItem("Region", selectedRegion);
    MVCApp.Toolbox.Service.setServiceType();
    if (!kony.store.getItem("pushAlert")) {
        isPushRegisterInvokedInPrevVersion = false;
    }
    if (!kony.store.getItem("firstIntallationFor72")) {
        kony.store.setItem("firstIntallationFor72", true);
        initLaunchFlagForVersion72 = true;
    }
    kony.i18n.setCurrentLocaleAsync(kony.store.getItem("languageSelected"), function() {
        kony.print("The default locale has been set on app init to " + selectedLang);
    }, function() {
        kony.print("Some error occured in default app language setting");
    });
}

function loadi18nValues() {
    var reqParams = {};
    var maxTimeStamp = kony.store.getItem("maxTimeStamp");
    if (maxTimeStamp == null || maxTimeStamp == undefined || maxTimeStamp == "") {
        reqParams.inputTimeStamp = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.maxTimeStamp");
    } else {
        reqParams.inputTimeStamp = maxTimeStamp;
    }
    MakeServiceCall(MVCApp.serviceType.internalizationorchservice, MVCApp.serviceEndPoints.i18orcheservice, reqParams, function(results) {
        if (results.opstatus === 0 || results.opstatus === "0") {
            var values = results.internalization || [];
            var tempEn = {};
            var tempEnCA = {};
            var tempFRCA = {};
            for (var i = 0; i < values.length; i++) {
                var keyVal = values[i].key;
                tempEn[keyVal] = "" + values[i].en;
                tempEnCA[keyVal] = "" + values[i].en_CA;
                tempFRCA[keyVal] = "" + values[i].fr_CA;
            }
            setLocaleData(tempEn, "en");
            setLocaleData(tempEnCA, "en_CA");
            setLocaleData(tempFRCA, "fr_CA");
            initRuntimePermissionObjects();
            try {
                if (results.maxTimeStamp != null && results.maxTimeStamp != undefined && results.maxTimeStamp != "") {
                    kony.store.setItem("maxTimeStamp", results.maxTimeStamp);
                }
            } catch (e) {
                kony.print("Exception is  " + JSON.stringify(e));
                if (e) {
                    TealiumManager.trackEvent("Exception On Internationalization Service Call", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }, function(error) {
        initRuntimePermissionObjects();
        kony.print("Response is " + JSON.stringify(error));
    });
}

function setLocaleData(data, locale) {
    try {
        kony.i18n.updateResourceBundle(data, locale);
    } catch (i18nError) {
        kony.print("Exception While updating ResourceBundle  : " + i18nError);
        if (i18nError) {
            TealiumManager.trackEvent("Updation Of Resource Bundle Exception", {
                exception_name: i18nError.name,
                exception_reason: i18nError.message,
                exception_trace: i18nError.stack,
                exception_type: i18nError
            });
        }
    }
}

function setCurrentLocaleAsync(data, loc, maxTimeStamp) {
    try {
        setLocaleData(data, loc);
        kony.i18n.setCurrentLocaleAsync(loc, function() {
            onsuccessLocale(maxTimeStamp);
        }, onfailurecallback);
    } catch (err) {
        kony.print("the value of err.message is:" + err);
        if (err) {
            TealiumManager.trackEvent("Exception While Setting Current Locale Async", {
                exception_name: err.name,
                exception_reason: err.message,
                exception_trace: err.stack,
                exception_type: err
            });
        }
    }
}

function onsuccessLocale(maxTimeStamp) {
    try {
        kony.print("locale changed to ");
        kony.store.setItem("maxTimeStamp", maxTimeStamp);
    } catch (err) {
        kony.print("the value of err.message is:" + err);
        if (err) {
            TealiumManager.trackEvent("Exception On Successful Locale Change", {
                exception_name: err.name,
                exception_reason: err.message,
                exception_trace: err.stack,
                exception_type: err
            });
        }
    }
}

function onfailurecallback() {
    try {
        kony.print("locale changed to en/ar");
    } catch (err) {
        kony.print("the value of err.message is:" + err);
        if (err) {
            TealiumManager.trackEvent("Exception On Unsuccessful Locale Change", {
                exception_name: err.name,
                exception_reason: err.message,
                exception_trace: err.stack,
                exception_type: err
            });
        }
    }
}

function versionCheck() {
    var requestParams = MVCApp.versionCheck.getRequestParams();
    MakeServiceCall("versionverifyorcservice", "versionverifyorcheservice", requestParams, function(results) {
        if (results.opstatus === 0 || results.opstatus === "0") {
            var upgradeStatus = MVCApp.versionCheck.succesCallBack(results);
        }
    }, function(error) {
        kony.print("Response is " + JSON.stringify(error));
    });
}

function homePreShowHandler() {
    MVCApp.Toolbox.common.updateWeeklyAdd();
    //frmHome.flxHeaderWrap.imgCouponIcon.isVisible = false;
    // frmHome.flxHeaderWrap.btnCoupon.isVisible = false;
    MVCApp.Toolbox.common.updateCart();
    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
        MVCApp.getHomeController().setStoreModeDetails();
    } else {
        try {
            if (null != kony.store.getItem("userCredential") && undefined != kony.store.getItem("userCredential") && "" != kony.store.getItem("userCredential")) {
                gblDecryptedPassword = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            }
            frmHome.flxMyStoreContainer.isVisible = true;
            frmHome.flxStoreModeContainer.isVisible = false;
            if (kony.store.getItem("myLocationDetails")) {
                var locDetails = JSON.parse(kony.store.getItem("myLocationDetails"));
                if (locDetails.address2 !== "") {
                    frmHome.lblStoreName.text = locDetails.address2;
                } else {
                    frmHome.lblStoreName.text = locDetails.city;
                }
                frmHome.flxChooseAStore.isVisible = false;
                frmHome.flxStoreName.isVisible = true;
            } else {
                frmHome.flxChooseAStore.isVisible = true;
                frmHome.flxStoreName.isVisible = false;
            }
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "MVCStartupForm.js on homePreShowHandler for locDetails:" + JSON.stringify(e)
            }]);
            if (e) {
                TealiumManager.trackEvent("POI Parse Exception On PreShow Of Home Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
}

function initAppData() {
    var whenStoreNotSet = {
        "doesLoadCouponsWhenStoreNotSet": false,
        "fromWhichScreen": ""
    };
    whenStoreNotSet = JSON.stringify(whenStoreNotSet);
    kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
    kony.store.setItem("shoppingListProdOrProjID", "");
    kony.store.setItem("currentProdCategory", {});
    kony.store.setItem("searchedProdId", "");
    gblIsNotFromOnChangeVarient = true;
    gblCouponPageValue = false;
    gblProductsBackValue = false;
    gblIsFromSortAndFilter = false;
    gblIsFromHomeScreen = false;
    gblCurrentLocationPermission = 0;
    gblIsLocationPermissionGranted = false;
    gblIsLocationPermissionDenied = false;
    gblIsFromStoreLocatorFlow = false;
    gblWhenTheProductScannedIsNotPresent = false;
    gblTealiumTagObj = new initTealiumTagObject();
    gblIsRewardsBeingShown = false;
    gblAndroidPushSettingControlFlag = false;
    gblIsWeeklyAdDetailDeepLink = false;
}

function loadSearchContent(callback) {
    var reqParams = MVCApp.Service.getCommonInputParamaters();
    var storeNo = "";
    var storeString = kony.store.getItem("myLocationDetails") || "";
    if (storeString !== "") {
        try {
            var storeObj = JSON.parse(storeString);
            storeNo = storeObj.clientkey;
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "MVCStartupForm.js on loadSearchContent for storeString:" + JSON.stringify(e)
            }]);
            if (e) {
                TealiumManager.trackEvent("Exception While Loading The Search Content", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    } else {
        storeNo = "";
    }
    if (glbStoreNoForSearch === "") {
        glbStoreNoForSearch = storeNo;
    }
    reqParams.store_id = storeNo;
    reqParams.refineString = MVCApp.serviceConstants.searchContentRefineString;
    MakeServiceCall(MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchContent, reqParams, function(results) {
        if (results.opstatus === 0 || results.opstatus === "0") {
            var searchContentValue = results.hits || [];
            kony.store.setItem("searchContent", searchContentValue);
            if (callback !== undefined && callback !== null) callback();
        }
    }, function(error) {
        kony.print("Response is " + JSON.stringify(error));
        if (callback !== undefined && callback !== null) callback();
    });
}

function hideScrollIndicator() {
    kony.application.setApplicationBehaviors({
        hidedefaultloadingindicator: true
    });
}

function startForegroundStoreModeTimer() {
    try {
        kony.timer.schedule("StoreModeTimerFG", checkWiFiAndGeoFenceForStoreMode, parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.foregroundStoreModeTimerInSeconds")), true);
    } catch (e) {
        kony.print("exception in invoking StoreModeTimerFG " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Timer Exception While Starting The Store Mode In Foreground", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function stopForegroundStoreModeTimer() {
    try {
        kony.timer.cancel("StoreModeTimerFG");
    } catch (e) {
        if (e.errorCode == 102) {
            kony.print("Timer Cancel: StoreModeTimerFG is already cancelled -----> " + JSON.stringify(e));
        }
        if (e) {
            TealiumManager.trackEvent("Timer Exception While Stopping The Store Mode In Foreground", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function startBackgroundStoreModeTimer() {
    try {
        kony.timer.schedule("StoreModeTimerBG", checkWiFiAndGeoFenceForStoreMode, parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.backgroundStoreModeTimerInSeconds")), true);
    } catch (e) {
        kony.print("exception in invoking StoreModeTimerBG " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Timer Exception While Starting The Store Mode In Background", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function stopBackgroundStoreModeTimer() {
    try {
        kony.timer.cancel("StoreModeTimerBG");
    } catch (e) {
        if (e.errorCode == 102) {
            kony.print("Timer Cancel: StoreModeTimerBG is already cancelled -----> " + JSON.stringify(e));
        }
        if (e) {
            TealiumManager.trackEvent("Timer Exception While Stopping The Store Mode In Background", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function fetchStoreDetails(UUID, major, minor) {
    //db service call
    beaconsDetails(UUID, major, minor);
}

function checkWiFiAndGeoFenceForStoreMode() {
    if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
        try {
            if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_WIFI)) {
                var macIP = "";
                macIP = getWifiDetailsFromFFI();
                if (macIP && verifyMacIP(macIP)) {
                    getWelcomeMessageForStore(macIP);
                }
            } else {
                var currentForm = kony.application.getCurrentForm();
                if (currentForm && currentForm.id !== "frmGuide") {
                    getStorePositionFromGeoFence();
                }
            }
        } catch (e) {
            kony.print("Exception occurred in getCurrentPosition" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Location Services Exception While Checking The WIFI & GeoFence For The Store Mode", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        //}
    } else {
        kony.print("checkWiFiAndGeoFenceForStoreMode - No network available.");
    }
}

function verifyMacIP(macIP) {
    var returnFlag = true;
    var existingMacIP = kony.store.getItem("macIP");
    if (existingMacIP && existingMacIP == macIP) {
        returnFlag = false;
    } else {
        kony.store.removeItem("macIP");
        kony.store.setItem("macIP", macIP);
    }
    return returnFlag;
}

function getStorePositionFromGeoFence() {
    if (appInForeground && !gblIsLocationPermissionDenied) {
        try {
            var positionOptions = {
                timeout: parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.timeoutValForGeoFence")),
                useBestProvider: true,
                enableHighAccuracy: true
            };
            kony.location.getCurrentPosition(locationSuccessCallback, locationErrorCallback, positionOptions);
        } catch (e) {
            kony.print("Exception occurred in getStoreFromGeo" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Location Services Exception While Getting The Store Position From GeoFence", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
}

function locationSuccessCallback(position) {
    if (appInForeground) {
        var latitude = position.coords.latitude || "";
        var longitude = position.coords.longitude || "";
        //need to uncomment above two lines and comment below 2 lines to send the real time data to service
        //        var latitude = "32.9112500";
        //        var longitude = "-96.958600";
        var timestamp = position.timestamp || "";
        if (latitude !== "" && longitude !== "" && timestamp !== "") {
            getNearByStores(latitude, longitude, timestamp);
        } else {
            kony.print("Error in getting latitude/longitude/timestamp from watPosition");
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                exitStoreMode();
            }
        }
    }
}

function locationErrorCallback(positionerror) {
    kony.print("LOG:locationErrorCallback-gblIsFromStoreLocatorFlow:" + gblIsFromStoreLocatorFlow);
    if (gblIsFromStoreLocatorFlow) {
        gblIsFromStoreLocatorFlow = false;
        MVCApp.getLocatorMapController().geoErrorcallback(positionerror);
    } else {
        if (appInForeground) {
            var errorMesg = "Error code: " + positionerror.code;
            errorMesg = errorMesg + " message: " + positionerror.message;
            kony.print("Error occurred in getWatchPosition - " + errorMesg);
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                exitStoreMode();
            }
        }
    }
}

function getNearByStores(latitude, longitude, timestamp) {
    var serviceType = MVCApp.serviceType.fetchWelcomeMessage;
    var operationId = MVCApp.serviceEndPoints.fetchWelcomeMessageByStoreId;
    var inputParams = {};
    inputParams.latitude = latitude;
    inputParams.longitude = longitude;
    inputParams.country = MVCApp.Service.Constants.Common.country;
    inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
    inputParams.currTime = new Date().getTime() / 1000;
    try {
        MakeServiceCall(serviceType, operationId, inputParams, function(results) {
            if (results !== null && results !== undefined && (results.httpStatusCode === 200 || results.httpStatusCode === "200") && (results.opstatus === 0 || results.opstatus === "0") && (results.hasNearbyStore === "true" || results.hasNearbyStore === true)) {
                if (results["STOREWIFIMACDETAILS"] !== null && results["STOREWIFIMACDETAILS"] !== undefined && results["STOREWIFIMACDETAILS"].length > 0) {
                    var storeName = results["STOREWIFIMACDETAILS"][0]["shoppingcenter"] || "";
                    var store_name = results["STOREWIFIMACDETAILS"][0]["store_name"] || "";
                    var store_id = results["STOREWIFIMACDETAILS"][0]["store_id"] || "";
                    var welcomeMessage = [];
                    welcomeMessage = results["WELCOME_MESSAGE"] || [];
                    kony.store.setItem("storeIDWhenStoreWIFIIsEnabled", store_id);
                    var deviceID = MVCApp.Toolbox.common.getDeviceID();
                    var deviceName = kony.os.deviceInfo().name;
                    var loyaltyID = "";
                    var userObject = kony.store.getItem("userObj");
                    if (userObject && userObject.c_loyaltyMemberID !== null && userObject.c_loyaltyMemberID !== undefined) {
                        loyaltyID = userObject.c_loyaltyMemberID;
                    }
                    var wifiReportingObj = {};
                    wifiReportingObj.deviceID = deviceID;
                    wifiReportingObj.deviceName = deviceName;
                    wifiReportingObj.loyaltyID = loyaltyID;
                    wifiReportingObj.store_id = store_id;
                    wifiReportingObj.deviceTimeStamp = new Date();
                    MVCAppAnalytics.sendCustomMetrics("storeVisit", "", "GeoFence", "", store_id);
                    MVCApp.sendMetricReport(frmHome, [{
                        "GeoFenceDetails": JSON.stringify(wifiReportingObj)
                    }]);
                    TealiumManager.trackEvent("Store Visit GeoFence", {
                        event_type: "registration",
                        conversion_category: "Store Visit",
                        conversion_id: "GPS",
                        conversion_action: 2,
                        storeid: wifiReportingObj.store_id,
                        device_time_stamp: JSON.stringify(wifiReportingObj.deviceTimeStamp),
                        device_id: wifiReportingObj.deviceID,
                        loyalty_id: wifiReportingObj.loyaltyID,
                        device_type: wifiReportingObj.deviceName
                    });
                    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                    cust360Obj.entryType = "gps";
                    cust360Obj.storeid = MVCApp.Toolbox.common.getStoreID();
                    cust360Obj.entryState = MVCApp.Toolbox.common.getEntryState();
                    MVCApp.Customer360.sendInteractionEvent("StoreMode", cust360Obj);
                    if (store_id && gblInStoreModeDetails.storeID && gblInStoreModeDetails.storeID != store_id) {
                        gblInStoreModeDetails.differentStoreFlag = true;
                    } else {
                        gblInStoreModeDetails.differentStoreFlag = false;
                    }
                    if (!gblInStoreModeDetails || !gblInStoreModeDetails.isInStoreModeEnabled || (gblInStoreModeDetails.storeModeExtensionFlag && gblInStoreModeDetails.differentStoreFlag) || gblInStoreModeDetails.differentStoreFlag) {
                        if (gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.differentStoreFlag) {
                            gblInStoreModeDetails.storeModeExtensionFlag = false;
                            try {
                                kony.timer.cancel("StoreModeTimer");
                            } catch (e) {
                                if (e.errorCode == 102) {
                                    kony.print("Timer Cancel: StoreModeTimer is already cancelled -----> " + JSON.stringify(e));
                                }
                                if (e) {
                                    TealiumManager.trackEvent("Exception While Cancelling The Store Mode Timer", {
                                        exception_name: e.name,
                                        exception_reason: e.message,
                                        exception_trace: e.stack,
                                        exception_type: e
                                    });
                                }
                            }
                        }
                        showStoreSpecificMessage(storeName, store_name, store_id, welcomeMessage);
                    } else if (gblInStoreModeDetails.storeModeExtensionFlag) {
                        gblInStoreModeDetails.storeModeExtensionFlag = false;
                        gblInStoreModeDetails.storeModeCounter++;
                        try {
                            var inStoreModeExitThresholdSeconds = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
                            kony.timer.schedule("StoreModeTimer", verifyStoreModeExtension, inStoreModeExitThresholdSeconds, false);
                        } catch (e) {
                            kony.print("exception in invoking timer " + JSON.stringify(e));
                            if (e) {
                                TealiumManager.trackEvent("Exception While Invoking The Store Mode Timer", {
                                    exception_name: e.name,
                                    exception_reason: e.message,
                                    exception_trace: e.stack,
                                    exception_type: e
                                });
                            }
                        }
                    }
                }
            } else {
                //As Geo-Fence is the last fallback to identify if user is in a store or away,
                //and as even this service failed, exiting from Store Mode.
                kony.print("getNearByStores: No nearByStores available.");
                if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                    exitStoreMode();
                }
            }
        }, function(error) {
            //As Geo-Fence is the last fallback to identify if user is in a store or away,
            //and as even this service failed, exiting from Store Mode.
            kony.print("getNearByStores: No nearByStores available -->" + JSON.stringify(error));
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                exitStoreMode();
            }
        });
    } catch (e) {
        kony.print("Exception occurred in getNearByStores " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception While Getting The Near By Stores", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function getWelcomeMessageForStore(macIP) {
    var serviceType = MVCApp.serviceType.KonyMFDBService;
    var operationId = MVCApp.serviceEndPoints.fetchStoreWifiDetails;
    var inputParams = {};
    inputParams.filter = macIP;
    inputParams.currTime = new Date().getTime() / 1000;
    try {
        MakeServiceCall(serviceType, operationId, inputParams, function(results) {
            var storeWifiFlag = false;
            if (results !== null && results !== undefined && (results.opstatus === 0 || results.opstatus === "0")) {
                if (results["STOREWIFIMACDETAILS"] !== null && results["STOREWIFIMACDETAILS"] !== undefined && results["STOREWIFIMACDETAILS"].length > 0) {
                    var storeName = results["STOREWIFIMACDETAILS"][0]["shoppingcenter"] || "";
                    var store_name = results["STOREWIFIMACDETAILS"][0]["store_name"] || "";
                    var store_id = results["STOREWIFIMACDETAILS"][0]["store_id"] || "";
                    var welcomeMessage = [];
                    welcomeMessage = results["WELCOME_MESSAGE"] || [];
                    storeWifiFlag = true;
                    var deviceID = MVCApp.Toolbox.common.getDeviceID();
                    var deviceName = kony.os.deviceInfo().name;
                    var loyaltyID = "";
                    var userObject = kony.store.getItem("userObj");
                    if (userObject && userObject.c_loyaltyMemberID !== null && userObject.c_loyaltyMemberID !== undefined) {
                        loyaltyID = userObject.c_loyaltyMemberID;
                    }
                    var wifiReportingObj = {};
                    wifiReportingObj.deviceID = deviceID;
                    wifiReportingObj.deviceName = deviceName;
                    wifiReportingObj.loyaltyID = loyaltyID;
                    wifiReportingObj.store_id = store_id;
                    wifiReportingObj.deviceTimeStamp = new Date();
                    MVCAppAnalytics.sendCustomMetrics("storeVisit", "", "Wifi", "", store_id);
                    MVCApp.sendMetricReport(frmHome, [{
                        "WifiDetails": JSON.stringify(wifiReportingObj)
                    }]);
                    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                    cust360Obj.entryType = "wifi";
                    cust360Obj.entryState = MVCApp.Toolbox.common.getEntryState();
                    cust360Obj.storeid = store_id;
                    MVCApp.Customer360.sendInteractionEvent("StoreMode", cust360Obj);
                    TealiumManager.trackEvent("Store Visit WIFI", {
                        event_type: "registration",
                        conversion_category: "Store Visit",
                        conversion_id: "Wi-Fi",
                        conversion_action: 2,
                        storeid: wifiReportingObj.store_id,
                        device_time_stamp: JSON.stringify(wifiReportingObj.deviceTimeStamp),
                        device_id: wifiReportingObj.deviceID,
                        loyalty_id: wifiReportingObj.loyaltyID,
                        device_type: wifiReportingObj.deviceName
                    });
                    if (store_id && gblInStoreModeDetails.storeID && gblInStoreModeDetails.storeID != store_id) {
                        gblInStoreModeDetails.differentStoreFlag = true;
                    } else {
                        gblInStoreModeDetails.differentStoreFlag = false;
                    }
                    if (!gblInStoreModeDetails || !gblInStoreModeDetails.isInStoreModeEnabled || (gblInStoreModeDetails.storeModeExtensionFlag && gblInStoreModeDetails.differentStoreFlag) || gblInStoreModeDetails.differentStoreFlag) {
                        if (gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.differentStoreFlag) {
                            gblInStoreModeDetails.storeModeExtensionFlag = false;
                            try {
                                kony.timer.cancel("StoreModeTimer");
                            } catch (e) {
                                if (e.errorCode == 102) {
                                    kony.print("Timer Cancel: StoreModeTimer is already cancelled -----> " + JSON.stringify(e));
                                }
                                if (e) {
                                    TealiumManager.trackEvent("Exception While Cancelling The Store Mode Timer When In A Different Store", {
                                        exception_name: e.name,
                                        exception_reason: e.message,
                                        exception_trace: e.stack,
                                        exception_type: e
                                    });
                                }
                            }
                        }
                        showStoreSpecificMessage(storeName, store_name, store_id, welcomeMessage);
                    } else if (gblInStoreModeDetails.storeModeExtensionFlag) {
                        gblInStoreModeDetails.storeModeExtensionFlag = false;
                        gblInStoreModeDetails.storeModeCounter++;
                        try {
                            var inStoreModeExitThresholdSeconds = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
                            kony.timer.schedule("StoreModeTimer", verifyStoreModeExtension, inStoreModeExitThresholdSeconds, false);
                        } catch (e) {
                            kony.print("exception in invoking timer " + JSON.stringify(e));
                            if (e) {
                                TealiumManager.trackEvent("Exception While Invoking The Store Mode Timer When In A Different Store", {
                                    exception_name: e.name,
                                    exception_reason: e.message,
                                    exception_trace: e.stack,
                                    exception_type: e
                                });
                            }
                        }
                    }
                    invokeTimerForWifiSwitch();
                } else {
                    kony.print("Store details not available.");
                    storeWifiFlag = false;
                }
            } else {
                kony.print("error in getWelcomeMessageForStore response ");
                storeWifiFlag = false;
            }
            if (!storeWifiFlag) {
                var currentForm = kony.application.getCurrentForm();
                if (currentForm && currentForm.id !== "frmGuide") {
                    getStorePositionFromGeoFence();
                }
            }
        }, function(error) {
            kony.print("fetchStoreWifiDetails error callback " + JSON.stringify(error));
            var currentForm = kony.application.getCurrentForm();
            if (currentForm && currentForm.id !== "frmGuide") {
                getStorePositionFromGeoFence();
            }
        });
    } catch (e) {
        kony.print("exception in getWelcomeMessageForStore " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception While Getting The Welcome Messgae For The Store", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function setLastStoreVisit() {
    var currentTimeStamp = new Date().getTime();
    try {
        kony.store.setItem("lastStoreVisit", currentTimeStamp);
    } catch (e) {
        kony.print("excpetion in setting lastStoreVisit data to device store");
        if (e) {
            TealiumManager.trackEvent("Exception While Setting the Last Store Visit Data To Device Store", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function showStoreSpecificMessage(storeName, store_name, store_id, welcomeMessage) {
    var userObject = kony.store.getItem("userObj");
    var currentTimeStamp = new Date().getTime();
    try {
        kony.store.setItem("lastStoreVisit", currentTimeStamp);
    } catch (e) {
        kony.print("excpetion in setting data to device store");
        if (e) {
            TealiumManager.trackEvent("Exception While Showing The Store Specific Message", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
    var loyaltyID = "";
    if (userObject && userObject.c_loyaltyMemberID !== null && userObject.c_loyaltyMemberID !== undefined) {
        loyaltyID = userObject.c_loyaltyMemberID;
    }
    var deviceID = "";
    var deviceInfo = JSON.stringify(kony.os.deviceInfo());
    var deviceName = kony.os.deviceInfo().name;
    deviceID = kony.os.deviceInfo().deviceid;
    kony.print("deviceID::::" + deviceID);
    if (storeName !== "" && storeName !== undefined && storeName !== null) {
        var reportData = {};
        reportData.deviceID = deviceID;
        reportData.loyaltyID = loyaltyID;
        reportData.deviceName = deviceName;
        reportData.deviceTimeStamp = new Date();
        reportData.store_id = store_id;
        kony.print("reportData::" + JSON.stringify([{
            "POS_Purchase_Tracking": JSON.stringify(reportData)
        }]));
        MVCApp.sendMetricReport(frmHome, [{
            "POS_Purchase_Tracking": JSON.stringify(reportData)
        }]);
        createLocalnotification(null, store_id, storeName, welcomeMessage);
    }
}
//for android
function getWifiDetailsFromFFI() {
    try {
        var PrintWifiDetailsObject = new wifiDetails.PrintWifiDetails();
        var returnedValue = PrintWifiDetailsObject.getWifiDetails();
        return returnedValue.length > 0 ? returnedValue[0] : "";
    } catch (e) {
        kony.print("exception in getWifiDetailsFromFFI " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception While Getting the WIFI Details From Android FFI", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}
//for iPhone
function getIPhoneWiFiDetails() {
    try {
        var returnedValue = wifiDetailsIphone.getWifiDetails();
        kony.print("returnedValue----" + JSON.stringify(returnedValue));
        return returnedValue.length > 0 ? returnedValue[1]["BSSID"] : "";
    } catch (e) {
        kony.print("exception in getIPhoneWiFiDetails " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception While Getting the WIFI Details From iOS FFI", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function testService() {
    var serviceType = "getAdInfoList";
    var operation = "getFeaturedListingAds";
    var requestParams = {
        "storeref": "1056",
        "languageid": "1",
        "sort": "1",
        "limit": "100",
        "campaignID": "495dca5c0cde6302"
    };
    MVCApp.Toolbox.common.showLoadingIndicator();
    try {
        var integrationObj = KNYMobileFabric.getIntegrationService(serviceType);
        integrationObj.invokeOperation(operation, null, requestParams, function(results) {
            alert("success " + JSON.stringify(results));
        }, function(error) {
            alert("error JSON" + JSON.stringify(error));
        });
    } catch (e) {
        alert("exception in test " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception While Making The getFeaturedListingAds Service Call", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function getProductsCategory(fromRegionChange, callback) {
    var serviceType = MVCApp.serviceType.getAisleNumbers; //MVCApp.serviceType.DemandwareService;
    var operationId = MVCApp.serviceEndPoints.getProductProjectCategory; //MVCApp.serviceEndPoints.getProductCategories;
    var requestParams = MVCApp.Service.getCommonInputParamaters();
    requestParams.id = MVCApp.serviceConstants.defaultProductId;
    requestParams["$select"] = "*";
    var storeNo = "";
    var storeId = "";
    var storeString = kony.store.getItem("myLocationDetails") || "";
    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
        if (gblInStoreModeDetails.isAppLaunch) {
            var storeModeExitThresholdTime = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.inStoreMode.exitThreshold.seconds"));
            var d = new Date();
            var currTime = Math.round(d.getTime() / 1000);
            if ((currTime - gblInStoreModeDetails.entryTimestamp) < (storeModeExitThresholdTime * gblInStoreModeDetails.storeModeCounter)) {
                storeId = gblInStoreModeDetails.storeID;
                storeNo = "and store_id eq " + gblInStoreModeDetails.storeID;
            } else {
                gblInStoreModeDetails = {
                    isInStoreModeEnabled: false,
                    storeModeCounter: 1
                };
                kony.store.removeItem("inStoreModeDetails");
                if (storeString !== "") {
                    try {
                        var storeObj = JSON.parse(storeString);
                        storeId = storeObj.clientkey;
                        storeNo = "and store_id eq " + storeObj.clientkey;
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "MVCStartupForm.js on getProductCategory for storeString:" + JSON.stringify(e)
                        }]);
                        if (e) {
                            TealiumManager.trackEvent("POI Parse Exception While Getting The Product Categories", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                } else {
                    storeNo = "";
                    storeId = "";
                }
            }
        } else {
            storeId = gblInStoreModeDetails.storeID;
            storeNo = "and store_id eq " + gblInStoreModeDetails.storeID;
        }
    } else {
        if (storeString !== "") {
            try {
                var storeObj = JSON.parse(storeString);
                storeId = storeObj.clientkey;
                storeNo = "and store_id eq " + storeObj.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "MVCStartupForm.js on getProductCategory for storeString:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Getting The Product Category", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeNo = "";
            storeId = "";
        }
    }
    gblStoreKeyForProducts = storeId;
    gblTealiumTagObj.storeid = storeId;
    requestParams["$filter"] = "status eq '" + "A'" + storeNo;
    requestParams.projectCategoriesId = MVCApp.serviceConstants.defaultProjectId;
    MakeServiceCall(serviceType, operationId, requestParams, function(results) {
        if (results.opstatus === 0 || results.opstatus === "0") {
            try {
                kony.timer.cancel("HomeCarouselTimer");
            } catch (e) {
                if (e) {
                    TealiumManager.trackEvent("Exception While Cancelling The Carousel Timer When Getting The Product Categories", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
            gblHomeLoadDate = new Date();
            var productsDataObj = new MVCApp.data.Products(results);
            var projectsDataObj = new MVCApp.data.Projects(results);
            gblproducts = productsDataObj;
            gblProjects = projectsDataObj;
            // loadSearchContent();
            //invokeTimer();
            kony.print("@@@LOG:getProductsCategory-fromRegionChange:" + fromRegionChange + "initLaunchFlag:" + initLaunchFlag);
            if (fromRegionChange !== true) {
                // getStorePosition();
                if (initLaunchFlag) {
                    getCurrentLocation();
                } else {
                    MVCApp.getHomeController().showNearest("");
                }
            } else {
                loadSearchContent(callback);
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
    }, function(error) {
        kony.print("Response is " + JSON.stringify(error));
        //if(error.errcode == 1011){
        // MVCApp.getHomeController().load();
        if (callback !== null && callback !== undefined) callback();
        else MVCApp.getHomeController().load();
        //}
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    });
}

function getHomeContent() {
    var reqParams = {};
    reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
    reqParams.refineString = MVCApp.serviceConstants.homeContentRefineString;
    MakeServiceCall(MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchContent, reqParams, function(results) {
        if (results.opstatus === 0 || results.opstatus === "0") {
            var homeContentValue = results.hits || [];
            kony.store.setItem("homeContent", homeContentValue);
            //MVCApp.getSplashController().updateScreen();
        }
    }, function(error) {
        kony.print("Response is " + JSON.stringify(error));
    });
}

function getCurrentLocation() {
    try {
        if (locationSettings == null || locationSettings == undefined) {
            locationSettings = new runtimepermissions.permissions();
        }
    } catch (e) {
        kony.print("Exception while getting location" + e);
    }
    var grantedLocPerm = locationSettings.verifyDenySetting("android.access_fine_location.permission");
    kony.print("@@@ In getCurrentGeoLocation() @@@" + "FALG VALUE::" + grantedLocPerm);
    if (grantedLocPerm) {
        var positionoptions = {
            timeout: 5000,
            enableHighAccuracy: true,
            useBestProvider: true
        };
        kony.location.getCurrentPosition(geoSuccesscallback, geoErrorcallback, positionoptions);
    } else {
        geoErrorcallback();
    }
}

function geoErrorcallback(positionerror) {
    kony.print("@@@ In geoErrorcallback() @@@");
    MVCApp.getHomeController().showNearest("");
    if (gblFavStoreRelocated) {
        gblFavStoreRelocated = false;
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.RelocatedFavStoreLocationDisabled"), "", constants.ALERT_TYPE_INFO, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), "");
    }
}

function geoSuccesscallback(position) {
    var currentLat = "";
    var currentLon = "";
    if (position.coords.latitude !== "" && position.coords.longitude !== "") {
        currentLat = position.coords.latitude;
        currentLon = position.coords.longitude;
        loadStoresWithLatLon(currentLat, currentLon);
    }
}

function loadStoresWithLatLon(lat, lon) {
    var inputParams = {};
    inputParams.latitude = lat;
    inputParams.longitude = lon;
    inputParams.country = MVCApp.Service.Constants.Common.country;
    inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
    MVCApp.Toolbox.common.showLoadingIndicator("");
    MVCApp.getLocatorMapController().loadForHome(inputParams);
}

function setTheme() {
    kony.print("in setTheme----------------->");
    var deviceModel = kony.os.deviceInfo().model;
    kony.print("the width is pre ------------------> " + screenWidth);
    var screenWidth = kony.store.getItem("screenWidth");
    if (screenWidth && screenWidth != "" && screenWidth != null) {} else {
        screenWidth = kony.os.deviceInfo().screenWidth;
        kony.store.setItem("screenWidth", screenWidth);
    }
    kony.print("the width post is -------------------->" + screenWidth);
    var theme = "defaultTheme";
    if (screenWidth == 320) {
        theme = "defaultTheme";
    } else if (screenWidth >= 360 && screenWidth <= 400) {
        theme = "iPhone67Theme";
    } else if (screenWidth > 400) {
        theme = "iPhone67PlusTheme";
    }
    try {
        if (theme !== "") {
            //alert(theme);
            kony.theme.setCurrentTheme(theme, success, error);
        }
    } catch (e) {
        kony.print("Exception in setting theme " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception While Setting The Theme", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }

    function success() {
        kony.print("theme - " + theme);
    }

    function error(err) {
        kony.print("error in setting theme " + JSON.stringify(err));
    }
}

function sendMetricForBeacons() {
    if (gblBeaconMgmtArr.length > 0) {
        if (gblBeaconMajor != "") {
            for (var k = 0; k < gblBeaconMgmtArr.length; k++) {
                var beaconMgmtObj = gblBeaconMgmtArr[k];
                var beaconInfoStr = "";
                if (beaconMgmtObj.major == gblBeaconMajor) {
                    for (var key in beaconMgmtObj) {
                        if (key !== "deviceID" && key !== "loyaltyID" && key !== "deviceTimeStamp" && key !== "deviceName") beaconInfoStr += beaconMgmtObj[key] + ",";
                    }
                    //remove the last separator
                    kony.print("pre beacon Info String is" + beaconInfoStr);
                    beaconInfoStr = beaconInfoStr.substring(0, beaconInfoStr.length - 2);
                    kony.print("beacon Info String is" + beaconInfoStr);
                    MVCAppAnalytics.sendCustomMetrics("storeVisit", "", "beacon", beaconInfoStr, beaconMgmtObj.major)
                    gblBeaconMgmtArr = [];
                    gblBeaconMajor = "";
                }
            }
        }
    }
}