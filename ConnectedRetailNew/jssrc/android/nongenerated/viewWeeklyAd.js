/**
 * PUBLIC
 * This is the view for the WeeklyAd form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.WeeklyAdView = (function() {
    var prevFormName = "";
    /**
     * PUBLIC
     * Open the More form
     */
    function show(headerText, previousFormName) {
        prevFormName = previousFormName;
        frmWeeklyAd.btnDeleteAll.text = headerText;
        frmWeeklyAd.flxHeaderWrap.textSearch.text = "";
        frmWeeklyAd.flxSearchResultsContainer.setVisibility(false);
        frmWeeklyAd.flxHeaderWrap.btnClearSearch.setVisibility(false);
        MVCApp.Toolbox.common.setTransition(frmWeeklyAd, 2);
        flxWeeklyAdRowContainer.lblNewExclusive.text = "";
        frmWeeklyAd.segShoppingList.removeAll();
        if (!gblIsWeeklyAdDetailDeepLink) {
            frmWeeklyAd.show();
        }
    }

    function showFormFromVoiceSearch() {
        frmWeeklyAd.flxSearchResultsContainer.setVisibility(false);
        frmWeeklyAd.flxSearchOverlay.setVisibility(false);
        frmWeeklyAd.flxVoiceSearch.setVisibility(false);
        frmWeeklyAd.flxHeaderWrap.btnClearSearch.setVisibility(false);
        MVCApp.Toolbox.common.setTransition(frmWeeklyAd, 3);
        frmWeeklyAd.show();
    }

    function showDealsDetails() {
        var segData = frmWeeklyAd.segShoppingList.data;
        var selectedIndex = frmWeeklyAd.segShoppingList.selectedRowIndex[1];
        MVCApp.getWeeklyAdDetailController().load(segData[selectedIndex], frmWeeklyAd.btnDeleteAll.text);
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function navigateToPreviousScreen() {
        kony.print("LOG:navigateToPreviousScreen-START");
        if (voiceSearchFlag) {
            onXButtonClick(voiceSearchForm);
            if (voiceSearchForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")) {
                voiceSearchFlag = false;
                voiceSearchForm.flxVoiceSearch.setVisibility(false);
            } else {
                voiceSearchFlag = true;
                tapToCancelFlag = true;
                deviceBackFlag = true;
            }
            MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
        }
        MVCApp.Toolbox.common.setTransition(frmWeeklyAdHome, 2);
        loadWeeklyAds = false;
        showPreviousScreen();
    }

    function bindEvents() {
        frmWeeklyAd.onDeviceBack = navigateToPreviousScreen;
        frmWeeklyAd.lblIconChevron.onTouchEnd = navigateToPreviousScreen;
        frmWeeklyAd.segShoppingList.onRowClick = showDealsDetails;
        MVCApp.searchBar.bindEvents(frmWeeklyAd, "frmWeeklyAd");
        MVCApp.tabbar.bindEvents(frmWeeklyAd);
        MVCApp.tabbar.bindIcons(frmWeeklyAd, "frmWeeklyAd");
        frmWeeklyAd.segShoppingList.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
        frmWeeklyAd.segShoppingList.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
        frmWeeklyAd.preShow = function() {
            frmWeeklyAd.flxSearchOverlay.setVisibility(false);
            frmWeeklyAd.flxVoiceSearch.setVisibility(false);
        };
        frmWeeklyAd.postShow = function() {
            var cartValue = MVCApp.Toolbox.common.getCartItemValue();
            frmWeeklyAd.flxHeaderWrap.lblItemsCount.text = cartValue;
            frmWeeklyAd.flxSearchOverlay.setVisibility(false);
            MVCApp.Toolbox.common.setBrightness("frmWeeklyAd");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Weekly Ad: Category:" + frmWeeklyAd.btnDeleteAll.text;
            lTealiumTagObj.page_name = "Weekly Ad: Category:" + frmWeeklyAd.btnDeleteAll.text;
            lTealiumTagObj.page_type = "weekly ad";
            lTealiumTagObj.page_category_name = "Weekly Ad: Category:" + frmWeeklyAd.btnDeleteAll.text;
            lTealiumTagObj.page_category = "Weekly Ad: Category:" + frmWeeklyAd.btnDeleteAll.text;
            lTealiumTagObj.page_subcategory_name = "Weekly Ad: Category:" + frmWeeklyAd.btnDeleteAll.text;
            lTealiumTagObj.category_ID = "Weekly Ad";
            TealiumManager.trackView("Weekly Ad Category Page", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            var storeid = MVCApp.Toolbox.common.getStoreID();
            cust360Obj.storeid = storeid;
            var departmentId = MVCApp.getWeeklyAdController().getDepartmentID();
            if (departmentId == "") {
                departmentId = "Featured Deals";
            }
            cust360Obj.categoryid = departmentId;
            cust360Obj.storeid = MVCApp.Toolbox.common.getStoreID();
            if (!formFromDeeplink) {
                MVCApp.Customer360.sendInteractionEvent("WeeklyAdCategoryList", cust360Obj);
            } else {
                formFromDeeplink = false;
            }
        };
    }

    function savePreviousScreenForm() {
        var prevForm = kony.application.getPreviousForm();
        if (prevForm != null && prevForm != undefined) {
            prevForm = prevForm.id;
        }
        if (prevForm == "frmHome") {
            prevFormName = prevForm;
        } else if (prevForm == "frmWeeklyAdHome") {
            prevFormName = prevForm;
        }
    }

    function showPreviousScreen() {
        var ifEntryFromBrowser = MVCApp.getWeeklyAdController().getFormEntryFromCartOrWebView();
        if (prevFormName == "frmHome") {
            MVCApp.getHomeController().load();
        } else if (prevFormName == "frmWeeklyAdHome") {
            frmWeeklyAdHome.show();
        } else if (ifEntryFromBrowser) {
            if (entryBrowserFormName == "frmCartView") {
                MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
            } else if (entryBrowserFormName === "frmWebView") {
                var urlBack = MVCApp.Toolbox.common.findWebViewUrlToGoBack();
                MVCApp.getProductsListWebController().loadWebView(urlBack);
            } else {
                MVCApp.getHomeController().load();
            }
        } else { // This is to ensure any unhandled previous forms gets redirected to Home Screen. 
            MVCApp.getHomeController().load();
        }
    }

    function updateScreen(results, dealId) {
        var headerText = frmWeeklyAd.btnDeleteAll.text || "";
        var prevForm = kony.application.getPreviousForm();
        var prevFormName = "";
        if (prevForm != null && prevForm != undefined) {
            prevFormName = prevForm.id;
        }
        if (prevFormName !== "frmWeeklyAdHome") {
            headerText = results.getHeaderFromResponse();
            frmWeeklyAd.btnDeleteAll.text = headerText;
        }
        var adsList = results.getWeeklyAdList(headerText);
        if (adsList.length > 0) {
            frmWeeklyAd.segShoppingList.widgetDataMap = {
                lblAdditionalDeal: "lblAdditionalDeal",
                imgProduct: "image1",
                lblProductName: "title",
                lblRegPrice: "originalDeal",
                lblPrice: "deal",
                lblStoreInfo: "storeStatus",
                lblDateRange: "dateRange",
                flxWeeklyAdHeader: "headerFlag",
                lblNewExclusive: "offerTag",
                txtDisclaimer1: "txtDisclaimer1",
                txtDisclaimer: "txtDisclaimer2"
            };
            frmWeeklyAd.segShoppingList.data = adsList;
            flxWeeklyAdDisclaimer.flxWeeklyAdContents.onClick = dummy;
            frmWeeklyAd.flxMainContainer.forceLayout();
            if (dealId) {
                for (var i = 0; i < adsList.length; i++) {
                    if (adsList[i].id == dealId) {
                        MVCApp.getWeeklyAdDetailController().load(adsList[i], adsList[i].departments[0].name);
                        gblIsWeeklyAdDetailDeepLink = false;
                        break;
                    }
                }
            }
        } else {
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAd.noAds"));
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function dummy() {
        kony.print("test");
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
            frmWeeklyAd.segShoppingList.bottom = "0dp";
        } else {
            MVCApp.Toolbox.common.dispFooter();
            frmWeeklyAd.segShoppingList.bottom = "54dp";
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        showFormFromVoiceSearch: showFormFromVoiceSearch,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});