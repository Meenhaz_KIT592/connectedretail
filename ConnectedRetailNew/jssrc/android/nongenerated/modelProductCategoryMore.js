/**
 * PUBLIC
 * This is the model for formProductCategoryMore form
 */
var MVCApp = MVCApp || {};
MVCApp.ProductCategoryMoreModel = (function() {
    var serviceType = MVCApp.serviceType.getAisleNumbers;
    var operationId = MVCApp.serviceEndPoints.getAisleNumbersForSubcategory;

    function loadSubSubCategories(callback, requestParams) {
        kony.print("modelProductCategoryMore.js");
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.opstatus === 0 || results.opstatus === "0") {
                var productsDataObj = new MVCApp.data.Products(results);
                callback(productsDataObj);
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
            var productsDataObj = new MVCApp.data.Products(error);
            callback(productsDataObj);
        });
    }
    return {
        loadSubSubCategories: loadSubSubCategories
    };
});