var rewardsFavouriteStore = "";
var rewardsDefaultFavouriteStore = "";
var MVCApp = MVCApp || {};
MVCApp.RewardsProfileModel = (function() {
    //  var serviceType =  "rewards";//MVCApp.serviceType.SigninRewardsProfile;
    //var operationId = "signinRewards";//MVCApp.serviceType.fetchCustomerInfo;
    var serviceType = "signupforloyaltyservice"; //MVCApp.serviceType.SigninRewardsProfile;
    var operationId = "loyaltylookup"; //MVCApp.serviceType.fetchCustomerInfo;
    var operationId1 = "getAddress";

    function load(callback, requestParams) {
        rewardsFavouriteStore = "";
        rewardsDefaultFavouriteStore = "";
        // var loyalty_id=kony.store.getItem("loyaltyMemberId");
        var userObject = kony.store.getItem("userObj");
        var loyalty_id = userObject.c_loyaltyMemberID || "";
        requestParams.isLoyalty = "true";
        requestParams.c_loyaltyMemberID = loyalty_id;
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Rewards Response is " + JSON.stringify(results));
            //alert(" results : "+JSON.stringify(results));
            if (undefined !== results.results && null !== results.results && "" !== results.results) {
                rewardsProfileData = {};
                var favStore = "";
                var poi = "";
                var collectionArray = results.collection || [];
                if (collectionArray.length > 0) {
                    poi = collectionArray[0].poi || "";
                }
                if (poi != "") {
                    favStore = JSON.stringify(poi);
                }
                rewardsFavouriteStore = favStore;
                rewardsDefaultFavouriteStore = favStore;
                isFromRewards = false;
                var rewardsProfileData1 = new MVCApp.data.RewardsProfile(results);
                MVCApp.getRewardsProfileController().setRewardProfileData(rewardsProfileData1);
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.isRewardsProfileCreated = "true";
                MVCApp.Customer360.sendInteractionEvent("RewardsProfileCreationWithAccount", cust360Obj);
                callback();
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
            }
        }, function(error) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            var status = error.Status || [];
            var showAlert = true;
            if (status) {
                if (status.length > 0) {
                    var errStatus = status[0];
                    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                    cust360Obj.isRewardsProfileCreated = "false";
                    cust360Obj.failureCode = errStatus.errCode || "NA";
                    MVCApp.Customer360.sendInteractionEvent("RewardsProfileCreationWithAccount", cust360Obj);
                    if (errStatus.errCode == "error.call.fetchJWTTokenForUser") {
                        kony.store.setItem("userObj", "");
                        MVCApp.Toolbox.common.checkIfUserIsForTouchID(requestParams.user)
                        MVCApp.getSignInController().invalidPassWordFromRewards();
                        showAlert = false;
                    }
                }
            }
            if (showAlert) {
                alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
            }
        });
    }

    function getData(requestParams) {
        MakeServiceCall(serviceType, operationId1, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            //alert(" results : "+JSON.stringify(results));
            if (undefined !== results.results && null !== results.results) {
                alert("getaddress" + JSON.stringify(results))
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
            }
        }, function(error) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.noResults"));
        });
    }
    return {
        load: load,
        getData: getData
    };
});