//Type your code here
//var pspConfig = {"iconPosition" : constants.ALERT_ICON_POSITION_LEFT, "contentAlignment":constants.ALERT_CONTENT_ALIGN_CENTER } ;
var searchTermForAnalytics = "";
var intentForAnalytics = "";
var cartItemsVal = "";
var sessionURI = "";
var MVCApp = MVCApp || {};
var jwtToken = "";
var numberOfItems = 0;
var basketId;
var customerId;
var calback;
var isMerge = false;
var couponCodes = [];
MVCApp.Toolbox = {
    //CHANGES THE SKIN OF THE REQUIRED WiDGET
    widget: {
        changeSkin: function(skin, widget) {
            kony.print("Applying skin " + skin + " to widget with ID " + widget.id);
            widget.skin = skin;
        }
    },
    common: {
        destoryFrmWeb: function(id) {
            kony.print("I am in destroy form--->start")
            try {
                if (id == "frmWebView") {
                    frmWebView.brwrView.evaluateJavaScript("window.stop();document.open();document.close();");
                    frmWebView.brwrView.removeGestureRecognizer(4);
                    kony.print("frmWebView I am in destroy form--->middle")
                        // frmWebView.destroy()
                } else if (id == "frmCartView") {
                    frmCartView.brwrView.evaluateJavaScript("window.stop();document.open();document.close();");
                    frmCartView.brwrView.removeGestureRecognizer(4);
                    kony.print("frmWebView I am in destroy form--->middle")
                        // frmCartView.destroy()
                }
                kony.print("frmWebView I am in destroy form--->end")
            } catch (e) {
                kony.print("I am in destroy form" + e)
            }
        },
        setCartCallback: function(callback) {
            calback = callback;
        },
        findWebViewUrlToGoBack: function() {
            var url = "";
            var entry = MVCApp.getProductsListController().getEntryType();
            if (entry === "taxonomy") {
                if (pdpUrl.indexOf(".html") !== -1) {
                    url = pdpUrl.split("?")[0];
                } else {
                    url = taxanomyurl;
                }
            } else {
                if (pdpUrl.indexOf("search?q=") !== -1) {
                    url = pdpUrl.split("&")[0];
                } else {
                    url = pdpUrl.split("?")[0];
                }
            }
            return url;
        },
        findIfWebviewUrlisDeeplink: function(url) {
            for (var i = 0; i < deeplinkArr.length; i++) {
                var res = url.indexOf(deeplinkArr[i]) > -1 ? true : false;
                if (res) {
                    return true;
                }
            }
            return false;
        },
        getCartCallback: function() {
            return calback;
        },
        isMerge: function() {
            return isMerge;
        },
        setMerge: function(flag) {
            isMerge = flag;
        },
        setGuestBasketCount: function(count) {
            kony.store.setItem("guestBasketCount", count);
        },
        getGuestBasketCount: function() {
            return kony.store.getItem("guestBasketCount");
        },
        setSearchTermForAnalytics: function(value) {
            searchTermForAnalytics = value;
        },
        setIntentForAnalytics: function(value) {
            intentForAnalytics = value;
        },
        getIntentForAnalytics: function() {
            return intentForAnalytics;
        },
        setSessionURI: function(uri) {
            kony.store.setItem("sessionuri", uri);
        },
        getSessionURI: function() {
            return kony.store.getItem("sessionuri");
        },
        lookForCoupon: function(promoCode) {
            var i = couponCodes ? couponCodes.length : 0;
            while (i--) {
                if (couponCodes[i] + "" === promoCode) {
                    return true;
                }
            }
            return false;
        },
        setCoupons: function(promo) {
            couponCodes = promo;
        },
        getCoupons: function() {
            return couponCodes;
        },
        setProducts: function(products) {
            kony.store.setItem("products", products);
        },
        getProducts: function() {
            return kony.store.getItem("products") || [];
        },
        setRegisterforTimeOut() {
            if (MVCApp.Toolbox.common.getRegion() == "US") {
                kony.application.registerForIdleTimeout(10, function() {
                    MVCApp.Toolbox.common.refreshToken();
                    MVCApp.Toolbox.common.setRegisterforTimeOut();
                });
            }
        },
        setJWTToken: function(jwt) {
            if (kony.store.getItem("JWTToken") === null) {
                kony.store.setItem("JWTToken", jwt);
            } else if (jwt != kony.store.getItem("JWTToken")) {
                kony.store.removeItem("JWTToken");
                kony.store.setItem("JWTToken", jwt);
            }
        },
        getJWTToken: function() {
            return kony.store.getItem("JWTToken") || "";
        },
        setBasket: function(basket_Id) {
            basketId = basket_Id;
            kony.store.setItem("basketId", basketId);
        },
        getBasket: function() {
            return kony.store.getItem("basketId") || "";
        },
        setCustomerId: function(customer_Id) {
            customerId = customer_Id;
            kony.store.setItem("customerId", customerId);
        },
        getCustomerId: function() {
            return kony.store.getItem("customerId") || "";
        },
        removeCustomerId: function() {
            return kony.store.removeItem("customerId") || "";
        },
        removeBasket: function() {
            return kony.store.removeItem("basketId") || "";
        },
        removeJWTToken: function() {
            return kony.store.removeItem("JWTToken") || "";
        },
        startTimeToRefreshToken: function() {
            // kony.timer.schedule("refreshToken", MVCApp.Toolbox.common.refreshToken, 5*60, true)
        },
        cancelTimer: function() {
            //kony.timer.cancel("refreshToken");
        },
        refreshToken: function() {
            MVCApp.getHomeController().load();
            /*MVCApp.Toolbox.common.verifyExpiryFlowOfAuthUser(function(){
        	
           });*/
            //              var requestParams = {};
            //             requestParams.client_id = MVCApp.Toolbox.Service.getClientId();
            // 			requestParams.jwtToken=MVCApp.Toolbox.common.getJWTToken();
            //             var service = MVCApp.serviceType.Appcommerce;
            //             var operation = MVCApp.serviceEndPoints.refresh;
            //        if(! MVCApp.Toolbox.common.checkIfGuestUser()){
            //         requestParams.user =MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
            //         requestParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            //         }
            //        MakeServiceCall(service,operation,requestParams,function(results){
            //           var flag = results.isJWTNew? results.isJWTNew:""
            //           MVCApp.Toolbox.common.setJWTToken(results.jwtToken);
            //          /* if(!MVCApp.Toolbox.common.checkIfGuestUser()){
            //           		 MVCApp.Toolbox.common.setProducts([]); 
            //                  MVCApp.Toolbox.common.createOrMergeBasket(results);
            //           }else{*/
            //                    // if (flag=="true") {
            // 						 // MVCApp.Toolbox.common.createOrMergeBasket(results);
            //                        var request = new kony.net.HttpRequest();
            // 							request.open(constants.HTTP_METHOD_POST, results.uriRequest, false);
            // 							request.setRequestHeader("Content-Type", "application/json");
            // 							request.setRequestHeader("Authorization", jwtToken);
            // 							request.onReadyStateChange = function() {
            // 							  if(request.readyState == 4) {
            //                                 if (flag=="true"){
            //                                     if(!MVCApp.Toolbox.common.checkIfGuestUser()){
            //           		 						   MVCApp.Toolbox.common.setProducts([]); 
            //                                 }
            //                                  MVCApp.Toolbox.common.createOrMergeBasket(results);
            //                               }
            // 							};
            //   					//}
            //           }
            //           request.send();
            //        },function(error){
            //           kony.print("error in authorzing");
            //         });
            /*   var serviceName=MVCApp.serviceType.Appcommerce;
                var operation=MVCApp.serviceEndPoints.refresh;
               var inputParams ={};
                inputParams.jwtToken=MVCApp.Toolbox.common.getJWTToken();
                inputParams.client_id = MVCApp.Service.Constants.Common.clientId;
                MakeServiceCall(serviceName,operation,inputParams,MVCApp.Toolbox.common.setRefreshToken,function(error){
                   kony.print("error in authorzing");
                 });*/
        },
        setRefreshToken: function(results) {
            MVCApp.Toolbox.common.createBasketforUser();
            /* MVCApp.Toolbox.common.setJWTToken(results.jwtToken);
         var uriRequest =MVCApp.Toolbox.common.getSessionURI(); 
         var jwtToken = MVCApp.Toolbox.common.getJWTToken();
         var request = new kony.net.HttpRequest();
       kony.print("i am here------>setRefreshToken"+uriRequest)
         request.open(constants.HTTP_METHOD_POST, uriRequest, false);
         request.setRequestHeader("Content-Type","application/json");
         request.setRequestHeader("Authorization", jwtToken);
		 request.onReadyStateChange=function(){
           kony.print("i am here------>");
          };*/
        },
        verifyTokenExpiredOrNot: function() {
            var token = MVCApp.Toolbox.common.getJWTToken() || "";
            kony.print("token" + token);
            if (token == "" || token == undefined || token == null) return true;
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            var jsonPayload = MVCApp.Toolbox.common.decode(base64);
            jsonPayload = jsonPayload.replace(/\0/g, '');
            var obj = JSON.parse(jsonPayload);
            var tokenExpiryPeriod = obj["exp"] * 1000;
            var currentPeriod = new Date().getTime();
            var diffPeriod = (tokenExpiryPeriod - currentPeriod) / 60000;
            kony.print("token expiry Period" + tokenExpiryPeriod + "currentPeriod  " + currentPeriod);
            kony.print("diffPeriod--" + diffPeriod);
            if (diffPeriod - 3 <= 0) return true;
            else return false;
        },
        findIfWebviewUrlisExternal: function(urlWebView) {
            for (var i = 0; i < allWebViewUrls.length; i++) {
                var regexp = new RegExp(allWebViewUrls[i]);
                var res = regexp.test(urlWebView.split("?")[0] || "");
                if (res) {
                    return true;
                }
            }
            return false;
        },
        formatURLBeforeLoading: function(originalURL, external) {
            var newURL = originalURL.replace('http://', 'https://');
            if (external) { //if external is true then remove the Appsource paramter.
                newURL = newURL.replace('&appsource=mobileapp', "");
                newURL = newURL.replace('?appsource=mobileapp', "?");
                return newURL;
            }
            return newURL;
        },
        decode: function(stringToConvert) {
            kony.print("I am here " + stringToConvert);
            try {
                var Base64 = {
                    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
                    decode: function(e) {
                        var base64String = "";
                        var characterCount, r, i;
                        var s, o, u, a;
                        var f = 0;
                        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                        while (f < e.length) {
                            s = this._keyStr.indexOf(e.charAt(f++));
                            o = this._keyStr.indexOf(e.charAt(f++));
                            u = this._keyStr.indexOf(e.charAt(f++));
                            a = this._keyStr.indexOf(e.charAt(f++));
                            characterCount = s << 2 | o >> 4;
                            r = (o & 15) << 4 | u >> 2;
                            i = (u & 3) << 6 | a;
                            base64String = base64String + String.fromCharCode(characterCount);
                            if (u != 64) {
                                base64String = base64String + String.fromCharCode(r);
                            }
                            if (a != 64) {
                                base64String = base64String + String.fromCharCode(i);
                            }
                        }
                        base64String = Base64._utf8_decode(base64String);
                        return base64String;
                    },
                    _utf8_decode: function(e) {
                        var base64String = "";
                        var characterCount = 0;
                        var r = 0,
                            c1 = 0,
                            c2 = 0,
                            c3 = 0;
                        while (characterCount < e.length) {
                            r = e.charCodeAt(characterCount);
                            if (r < 128) {
                                base64String += String.fromCharCode(r);
                                characterCount++;
                            } else if (r > 191 && r < 224) {
                                c2 = e.charCodeAt(characterCount + 1);
                                base64String += String.fromCharCode((r & 31) << 6 | c2 & 63);
                                characterCount += 2;
                            } else {
                                c2 = e.charCodeAt(characterCount + 1);
                                c3 = e.charCodeAt(characterCount + 2);
                                base64String += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                                characterCount += 3;
                            }
                        }
                        return base64String;
                    }
                };
                if (stringToConvert !== null && stringToConvert !== undefined) {
                    var returnBase64 = Base64.decode(stringToConvert);
                    kony.print("blob data " + returnBase64);
                    return returnBase64;
                } else {
                    return null;
                }
            } catch (err) {
                kony.print(err);
            }
        },
        guestSessionOnSignOut: function() {
            var requestParams = {};
            requestParams.clientId = MVCApp.Toolbox.Service.getClientId();
            var service = MVCApp.serviceType.Appcommerce;
            var operation = MVCApp.serviceEndPoints.getSessionForWebView;
            MakeServiceCall(service, operation, requestParams, function(results) {}, function(error) {
                kony.print("error in authorzing");
            });
        },
        callisSessionIsDone: function() {
            kony.print("callisSessionIsDone" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Account-TaxExemptMobileApp?appsource=mobileapp&sessionbridge=true");
            var httpClient = new kony.net.HttpRequest();
            httpClient.timeout = 5000;
            httpClient.open(constants.HTTP_METHOD_GET, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Account-TaxExemptMobileApp?appsource=mobileapp&sessionbridge=true");
            httpClient.setRequestHeader("User-Agent", "Michaels Appcommerce Android Mobile");
            httpClient.send();
        },
        appLaunchFlowforAuth: function() {
            kony.print("appLaunchFlowforAuth---->" + MVCApp.Toolbox.common.getSessionURI());
            var requestParams = {};
            // if( ! MVCApp.Toolbox.common.checkIfGuestUser()){
            if (!MVCApp.Toolbox.common.checkIfGuestUser() && MVCApp.Toolbox.common.verifyTokenExpiredOrNot()) {
                requestParams.clientId = MVCApp.Toolbox.Service.getClientId();
                var service = MVCApp.serviceType.Appcommerce;
                var operation = MVCApp.serviceEndPoints.getSessionForWebView;
                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                    requestParams.user = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username")) || "";
                    requestParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential")) || "";
                    if (requestParams.user === "" || requestParams.password === "") return;
                }
                MakeServiceCall(service, operation, requestParams, function(results) {
                    MVCApp.Toolbox.common.setJWTToken(results.jwtToken);
                    MVCApp.Toolbox.common.setSessionURI(results.uriRequest);
                    var request = new kony.net.HttpRequest();
                    kony.print("i am in createBasketforUser--------------------------->" + results.uriRequest);
                    request.open(constants.HTTP_METHOD_POST, results.uriRequest, false);
                    request.setRequestHeader("Content-Type", "application/json");
                    request.setRequestHeader("Authorization", results.jwtToken);
                    request.onReadyStateChange = function() {
                        MVCApp.Toolbox.common.createOrMergeBasket(results);
                        MVCApp.Toolbox.common.callisSessionIsDone();
                    };
                    request.send();
                }, function(error) {
                    var status = error.Status[0] || "";
                    var errCode = status.errCode || "";
                    if (errCode == "AuthenticationFailedException") {
                        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.common.Signin.PasswordExpried"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"), constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.naviagteToSignIn, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
                    }
                });
            } else {
                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                    var request = new kony.net.HttpRequest();
                    kony.print("i am in createBasketforUser--------------------------->" + MVCApp.Toolbox.common.getSessionURI());
                    request.open(constants.HTTP_METHOD_POST, MVCApp.Toolbox.common.getSessionURI(), false);
                    request.setRequestHeader("Content-Type", "application/json");
                    request.setRequestHeader("Authorization", MVCApp.Toolbox.common.getJWTToken());
                    request.onReadyStateChange = function() {
                        MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                        MVCApp.Toolbox.common.callisSessionIsDone();
                    };
                    request.send();
                } else {
                    MVCApp.Toolbox.common.getGuestBasketDetails();
                }
            }
        },
        setLMRID: function() {
            var requestParams = {};
            var serviceType = "signupforloyaltyservice"; //MVCApp.serviceType.Registration;
            var operationId = "loyaltySignin"; //MVCApp.serviceEndPoints.signInUser;
            // if( ! MVCApp.Toolbox.common.checkIfGuestUser()){
            if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                requestParams.clientId = MVCApp.Toolbox.Service.getClientId();
                requestParams.isLoyalty = "true";
                requestParams.user = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username")) || "";
                requestParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential")) || "";
                MakeServiceCall(serviceType, operationId, requestParams, function(results) {
                    kony.print("Response is " + JSON.stringify(results));
                    if (results.opstatus === 0 || results.opstatus === "0") {
                        var loyalty_id = results.c_loyaltyMemberID;
                        kony.store.setItem("userObj", results);
                        kony.store.removeItem("loyaltyMemberId");
                        kony.store.setItem("loyaltyMemberId", loyalty_id);
                    }
                }, function(error) {
                    var errStatus = error.Status || [];
                    var errCode = "";
                    if (errStatus.length > 0) {
                        var errCodeObj = errStatus[0] || "";
                        errCode = errCodeObj.errCode || "";
                    }
                });
            }
        },
        naviagteToSignIn: function(response) {
            if (response) {
                MVCApp.Toolbox.common.signOutUSer();
                MVCApp.getSignInController().load();
            } else {
                MVCApp.Toolbox.common.signOutUSer();
            }
        },
        verifyExpiryFlowOfAuthUser: function(callBack) {
            var requestParams = {};
            // if( ! MVCApp.Toolbox.common.checkIfGuestUser()){
            if (!MVCApp.Toolbox.common.checkIfGuestUser() && MVCApp.Toolbox.common.verifyTokenExpiredOrNot()) {
                requestParams.clientId = MVCApp.Toolbox.Service.getClientId();
                var service = MVCApp.serviceType.Appcommerce;
                var operation = MVCApp.serviceEndPoints.getSessionForWebView;
                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                    requestParams.user = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username")) || "";
                    requestParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential")) || "";
                    if (requestParams.user === "" || requestParams.password === "") return;
                    //var callback= MVCApp.Toolbox.common.getCartCallback();
                }
                MakeServiceCall(service, operation, requestParams, function(results) {
                    MVCApp.Toolbox.common.setJWTToken(results.jwtToken);
                    MVCApp.Toolbox.common.setSessionURI(results.uriRequest);
                    var request = new kony.net.HttpRequest();
                    kony.print("i am in createBasketforUser--------------------------->" + results.uriRequest);
                    request.open(constants.HTTP_METHOD_POST, results.uriRequest, false);
                    request.setRequestHeader("Content-Type", "application/json");
                    request.setRequestHeader("Authorization", results.jwtToken);
                    request.onReadyStateChange = function() {
                        if (callBack) MVCApp.Toolbox.common.setCartCallback(callBack);
                        MVCApp.Toolbox.common.createOrMergeBasket(results);
                        MVCApp.Toolbox.common.callisSessionIsDone();
                    };
                    request.send();
                }, function(error) {
                    var status = error.Status[0] || "";
                    var errCode = status.errCode || "";
                    if (errCode == "AuthenticationFailedException") {
                        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.common.Signin.PasswordExpried"), null, constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.naviagteToSignIn, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
                    } else {
                        if (callBack) callBack();
                    }
                    kony.print("error in authorzing");
                });
            } else {
                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                    MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken(), callBack);
                } else {
                    MVCApp.Toolbox.common.getGuestBasketDetails();
                }
            }
            //}
        },
        removeItemFromBasket: function(itemid, basketid, jwttoken, callback) {
            var inputParams = {};
            inputParams.Authorization = jwttoken;
            inputParams.basket_id = basketid;
            inputParams.item_id = itemid;
            var service = MVCApp.serviceType.AppcommerceCart;
            var operation = MVCApp.serviceEndPoints.deleteItemFromBasket;
            MakeServiceCall(service, operation, inputParams, function(results) {
                callback();
            }, function(error) {
                callback();
            });
        },
        getCartDetails: function(jwtToken) {
            var jwtToken = kony.store.getItem("jwtToken") || "";
            var basketId = kony.store.getItem("basketId") || "";
            var serviceName = MVCApp.serviceType.Appcommerce;
            var operation = MVCApp.serviceEndPoints.getSessionForWebView;
            var inputParams = {};
            var userObj = kony.store.getItem("userObj") || "";
            if (userObj && userObj != "") {
                inputParams.user = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
                inputParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            }
            MakeServiceCall(serviceName, operation, inputParams, MVCApp.Toolbox.common.getBasketDetails, MVCApp.Toolbox.common.invalidToken);
        },
        checkIfGuestUser: function() {
            var userObj = kony.store.getItem("userObj") || "";
            if (userObj && userObj !== "") return false;
            else return true;
        },
        createBasketforUser: function(callback) {
            var requestParams = {};
            if (callback) MVCApp.Toolbox.common.setCartCallback(callback);
            requestParams.clientId = MVCApp.Toolbox.Service.getClientId();
            var service = MVCApp.serviceType.Appcommerce;
            var operation = MVCApp.serviceEndPoints.getSessionForWebView;
            if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                requestParams.user = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
                requestParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            }
            MakeServiceCall(service, operation, requestParams, function(results) {
                var request = new kony.net.HttpRequest();
                MVCApp.Toolbox.common.setSessionURI(results.uriRequest);
                kony.print("i am in createBasketforUser--------------------------->" + results.uriRequest);
                request.open(constants.HTTP_METHOD_POST, results.uriRequest, false);
                request.setRequestHeader("Content-Type", "application/json");
                request.setRequestHeader("Authorization", results.jwtToken);
                request.onReadyStateChange = function() {
                    cookies = kony.net.getCookies(results.uriRequest);
                    kony.print("i am in createBasketforUser	--------------------------->" + JSON.stringify(cookies));
                    if (callback) MVCApp.Toolbox.common.setCartCallback(callback);
                    MVCApp.Toolbox.common.createOrMergeBasket(results);
                    MVCApp.Toolbox.common.callisSessionIsDone();
                };
                request.send();
            }, function(error) {
                kony.print("error in authorzing");
            });
        },
        createOrMergeBasket: function(results) {
            var jwt = results.jwtToken;
            var customer_id = results.customer_id;
            var session_uri = results.uriRequest;
            var guestCustomer_id;
            var guestBasket;
            if (MVCApp.Toolbox.common.isMerge()) {
                guestCustomer_id = JSON.parse(JSON.stringify(MVCApp.Toolbox.common.getCustomerId()));
                guestBasket = JSON.parse(JSON.stringify(MVCApp.Toolbox.common.getBasket()));
            }
            /* if(!MVCApp.Toolbox.common.checkIfGuestUser()){
                   MVCApp.Toolbox.common.setProducts([]);
             }else{
                   MVCApp.Toolbox.common.setJWTToken(results.jwtToken);
                   MVCApp.Toolbox.common.setSessionURI(results.uriRequest);
                   MVCApp.Toolbox.common.setCustomerId(results.customer_id);
                  	MVCApp.Toolbox.common.createBasketWithAuth();
             }*/
            MVCApp.Toolbox.common.setJWTToken(results.jwtToken);
            MVCApp.Toolbox.common.setSessionURI(results.uriRequest);
            MVCApp.Toolbox.common.setCustomerId(results.customer_id);
            MVCApp.Toolbox.common.createBasketWithAuth(guestCustomer_id, guestBasket);
        },
        createBasketForSignedInUser: function() {
            if (MVCApp.Toolbox.common.verifyTokenExpiredOrNot()) MVCApp.Toolbox.common.createBasketforUser()
            else MVCApp.Toolbox.common.createBasketWithAuth();
        },
        createBasketWithAuth: function(guestCustomer_id, guestBasket) {
            var inputParams = {};
            var service;
            var operation
            inputParams.Authorization = MVCApp.Toolbox.common.getJWTToken();
            // inputParams.products=MVCApp.Toolbox.common.getProducts();
            if (!MVCApp.Toolbox.common.isMerge()) {
                service = MVCApp.serviceType.AppcommerceCart;
                operation = MVCApp.serviceEndPoints.CreateBasketForUser;
            } else {
                inputParams.customer_id = MVCApp.Toolbox.common.getCustomerId();
                inputParams.guest_customer_id = guestCustomer_id;
                inputParams.guest_basket_id = guestBasket;
                service = MVCApp.serviceType.mergeBasketTosignedUser;
                operation = MVCApp.serviceEndPoints.merge;
            }
            MakeServiceCall(service, operation, inputParams, function(results) {
                var basketId = results.basket_id || "";
                var basketArry = [];
                if (basketId === "" && operation == MVCApp.serviceEndPoints.merge) {
                    basketArry = results["baskets"];
                    if (basketArry !== undefined && basketArry !== null && basketArry.length > 0) {
                        var basketObj = basketArry[0];
                        basketId = basketObj["basket_id"];
                    }
                }
                MVCApp.Toolbox.common.setBasket(basketId);
                if (MVCApp.Toolbox.common.getProducts().length > 0 && !MVCApp.Toolbox.common.isMerge()) MVCApp.Toolbox.common.addItemsTOBasket();
                else MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                var callBackfun = MVCApp.Toolbox.common.getCartCallback();
                if (callBackfun) {
                    callBackfun();
                }
            }, function(error) {
                var type = error.type;
                var basketId = "";
                if (type == "CustomerBasketsQuotaExceededException") {
                    basketId = error.basketId || "";
                    var extBasket = MVCApp.Toolbox.common.getBasket();
                    var basketArray = [];
                    var flag = false;
                    if (basketId.indexOf(",") !== -1) {
                        basketArray = basketId.split(",");
                        for (var i = 0; i < basketArray.length; i++) {
                            if (basketArray[i] == extBasket) {
                                flag = true;
                            }
                        }
                        if (!flag) {
                            MVCApp.Toolbox.common.setBasket(basketArray[0]);
                        }
                    } else {
                        MVCApp.Toolbox.common.setBasket(basketId);
                    }
                    if (MVCApp.Toolbox.common.getProducts().length > 0 && !MVCApp.Toolbox.common.isMerge()) MVCApp.Toolbox.common.addItemsTOBasket();
                    else MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    var callBackfun = MVCApp.Toolbox.common.getCartCallback();
                    if (callBackfun) {
                        callBackfun();
                    }
                }
            });
        },
        addItemsTOBasket: function() {
            var inputParams = {};
            inputParams.Authorization = MVCApp.Toolbox.common.getJWTToken();
            inputParams.products = MVCApp.Toolbox.common.getProducts();
            inputParams.basketId = MVCApp.Toolbox.common.getBasket();
            var service = MVCApp.serviceType.AppcommerceCart;
            var operation = MVCApp.serviceEndPoints.AddItemToBasket;
            MakeServiceCall(service, operation, inputParams, function(results) {
                var products = results.products || [];
                var noOfItemsInCart = 0;
                for (var i = 0; i < products.length; i++) {
                    noOfItemsInCart += parseInt(products[i].quantity || "0");
                }
                MVCApp.Toolbox.common.setNumberOfItemsInCart(noOfItemsInCart);
                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                    // MVCApp.Toolbox.common.setProducts([]);
                    //MVCApp.Toolbox.common.removeBasket();
                    //MVCApp.Toolbox.common.removeCustomerId();
                } else
                // MVCApp.Toolbox.common.setProducts(products);
                    MVCApp.Toolbox.common.updateCartIcon(frmWebView);
            }, function(error) {
                var message = error.fault;
                if (message.indexOf("ProductItemNotAvailableException") !== -1) {
                    //MVCApp.Toolbox.common.customAlertNoTitle("The products are in basket is not available for quantity","");
                    MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                } else {
                    MVCApp.Toolbox.common.setNumberOfItemsInCart(0);
                    MVCApp.Toolbox.common.updateCartIcon(frmWebView);
                }
            });
        },
        /*mergeCart:function(customerId){
         
          var inputParams= {};
          inputParams.Authorization= MVCApp.Toolbox.common.getJWTToken();
          inputParams.basket_id=MVCApp.Toolbox.common.getBasket();
          inputParams.customer_id=MVCApp.Toolbox.common.getCustomerId()||"";
         
          var service=MVCApp.serviceType.AppcommerceCart;
          var operation=MVCApp.serviceEndPoints.mergeBasket;
          MakeServiceCall(service,operation,inputParams,function(results){
            var basketId= results.basket_id;
             MVCApp.Toolbox.common.setBasket(basketId);
             MVCApp.Toolbox.common.setCustomerId(customerId);
            
            MVCApp.Toolbox.common.getBasketDetails( MVCApp.Toolbox.common.getJWTToken());
          },function(error){
            var type= error.type;
            var basketId="";
             MVCApp.Toolbox.common.setCustomerId(customerId);
            if(type=="CustomerBasketsQuotaExceededException"){
              basketId=error.basketId||"";
              MVCApp.Toolbox.common.setBasket(basketId);
              MVCApp.Toolbox.common.getBasketDetails( MVCApp.Toolbox.common.getJWTToken());
            }
          });
        },*/
        getBasketDetails: function(jwtToken, callback) {
            MVCApp.Toolbox.common.setCoupons([]);
            MVCApp.Toolbox.common.setMerge(false);
            var authorization = jwtToken;
            var basketId = MVCApp.Toolbox.common.getBasket();
            var serviceName = MVCApp.serviceType.AppcommerceCart;
            var operation = MVCApp.serviceEndPoints.getCartDetails;
            var inputParams = {};
            if (basketId && basketId !== "") {
                inputParams.Authorization = authorization;
                inputParams.basketId = basketId;
                MakeServiceCall(serviceName, operation, inputParams, function(results) {
                    var products = results.products || [];
                    var noOfItemsInCart = 0;
                    for (var i = 0; i < products.length; i++) {
                        noOfItemsInCart += parseInt(products[i].quantity || "0");
                        if (products[i].price_adjustments && products[i].price_adjustments.length > 0) {
                            for (var j = 0; j < products[i].price_adjustments.length; j++) {
                                couponCodes.push(products[i].price_adjustments[j].coupon_code);
                            }
                        }
                    }
                    if (callback) {
                        callback();
                    }
                    /* if(!MVCApp.Toolbox.common.checkIfGuestUser()){
                       MVCApp.Toolbox.common.setProducts([]);
                     }else
                         MVCApp.Toolbox.common.setProducts(products);*/
                    MVCApp.Toolbox.common.setNumberOfItemsInCart(noOfItemsInCart);
                    MVCApp.Toolbox.common.updateCartIcon(frmWebView);
                }, function(error) {
                    kony.print("getBasketDetails-->" + JSON.stringify(error));
                    if (error["httpStatusCode"] == 401) {
                        // kony.print("getBasketDetails"+error["opstatus"]);
                        MVCApp.Toolbox.common.verifyExpiryFlowOfAuthUser();
                    }
                    if (error["httpStatusCode"] == 404) {
                        // kony.print("getBasketDetails"+error["opstatus"]);
                        MVCApp.Toolbox.common.removeJWTToken();
                        MVCApp.Toolbox.common.appLaunchFlowforAuth();
                    }
                    if (callback) {
                        callback();
                    }
                    //MVCApp.Toolbox.common.setNumberOfItemsInCart(0);
                    //MVCApp.Toolbox.common.setCoupons([]);
                    //MVCApp.Toolbox.common.updateCartIcon(frmWebView);
                })
            } else {
                if (callback) {
                    callback();
                }
                MVCApp.Toolbox.common.setNumberOfItemsInCart("0");
                MVCApp.Toolbox.common.updateCartIcon(frmWebView);
            }
        },
        invalidToken: function(error) {
            MVCApp.Toolbox.common.setNumberOfItemsInCart(0);
            MVCApp.Toolbox.common.updateCartIcon(frmWebView);
        },
        setNumberOfItemsInCart: function(value) {
            numberOfItems = value;
        },
        getNumberOfItemsInCart: function() {
            return numberOfItems + "";
        },
        updateCartIcon: function(myForm) {
            try {
                var numItems = MVCApp.Toolbox.common.getNumberOfItemsInCart() || "0";
                kony.print("number ---" + numItems + "---cart");
                numItems = numItems.trim();
                numItems = parseInt(numItems) ? parseInt(numItems) : numItems;
                if (numItems == 0) {
                    //frmCartView.lblItemsCount.isVisible=false;
                    //myForm.flxHeaderWrap.lblItemsCount.isVisible= false;
                    frmHome.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmMore.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmPDP.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmPJDP.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmProductCategory.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmProducts.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmProductsList.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmProjectsList.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmShoppingList.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmWebView.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.isVisible = false;
                    frmCartView.lblItemsCount.isVisible = false;
                } else {
                    //myForm.flxHeaderWrap.lblItemsCount.isVisible= true;
                    frmHome.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmMore.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmPDP.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmPJDP.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmProducts.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmProductsList.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmWebView.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.text = numItems + "";
                    frmCartView.lblItemsCount.text = numItems + "";
                    frmHome.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmMore.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmPDP.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmPJDP.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmProductCategory.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmProducts.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmProductsList.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmProjectsList.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmShoppingList.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmWebView.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.isVisible = true;
                    frmCartView.lblItemsCount.isVisible = true;
                }
                if (numItems > 99) {
                    frmHome.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmHome.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmHome.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmHome.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmMore.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmMore.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmMore.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmMore.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmPDP.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmPDP.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmPDP.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmPDP.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmProducts.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmProducts.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmProducts.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmProducts.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmWebView.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmWebView.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmWebView.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmWebView.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.width = "40%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.height = "55%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.top = "4%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.right = "4%";
                    frmCartView.lblItemsCount.width = "40%";
                    frmCartView.lblItemsCount.height = "55%";
                    frmCartView.lblItemsCount.top = "4%";
                    frmCartView.lblItemsCount.right = "4%";
                } else {
                    frmHome.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmHome.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmHome.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmHome.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmMore.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmMore.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmMore.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmMore.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmPDP.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmPDP.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmPDP.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmPDP.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmPJDP.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmProductCategory.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmProductCategoryMore.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmProducts.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmProducts.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmProducts.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmProducts.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmProductsList.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmProjectsCategory.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmProjectsList.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmShoppingList.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmWebView.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmWebView.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmWebView.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmWebView.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmWeeklyAd.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.width = "30%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.height = "42%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.top = "7%";
                    frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.right = "3%";
                    frmCartView.lblItemsCount.width = "30%";
                    frmCartView.lblItemsCount.height = "42%";
                    frmCartView.lblItemsCount.top = "7%";
                    frmCartView.lblItemsCount.right = "3%";
                }
            } catch (e) {
                kony.print("Error in updateCart : " + JSON.stringify(e));
            }
            // frmShoppingList.flxHeaderWrap.lblItemsCount.text= numItems;// patch fix to update Shopping List on Back from Cart
        },
        signOutUSer: function() {
            var operation = MVCApp.serviceEndPoints.logout;
            var serviceName = MVCApp.serviceType.Appcommerce;
            var inputParams = {};
            kony.store.removeItem("userObj");
            MakeServiceCall(serviceName, operation, inputParams, function(results) {
                //MVCApp.Toolbox.common.guestSessionOnSignOut();
                try {
                    frmWebView.destroy();
                    frmCartView.destroy();
                    kony.net.clearCookies(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels"));
                    cartsession.clearCookies(
                        /**Function*/
                        function() {
                            kony.print("Cookies cleared")
                        });
                } catch (err) {}
                //MVCApp.Toolbox.common.guestSessionOnSignOut();
                MVCApp.Toolbox.common.setProducts([]);
                MVCApp.Toolbox.common.removeBasket();
                MVCApp.Toolbox.common.removeCustomerId();
                MVCApp.Toolbox.common.removeJWTToken();
                MVCApp.Toolbox.common.setNumberOfItemsInCart(0);
                MVCApp.Toolbox.common.updateCartIcon(frmWebView);
                //MVCApp.Toolbox.common.createBasketforUser();
            }, function(error) {});
        },
        getCartItemValue: function() {
            return MVCApp.Toolbox.common.getNumberOfItemsInCart();
        },
        setCartItemValue: function(value) {
            MVCApp.Toolbox.common.setNumberOfItemsInCart(value);
        },
        getGuestBasketDetails: function() {
            kony.print("getGuestBasketDetails")
            var noOfItemsInCart = MVCApp.Toolbox.common.getGuestBasketCount() || 0;
            MVCApp.Toolbox.common.setNumberOfItemsInCart(noOfItemsInCart);
            MVCApp.Toolbox.common.updateCartIcon(frmWebView);
        },
        getSearchTermForAnalytics: function() {
            return searchTermForAnalytics;
        },
        sendTealiumTagsForSearchAnalytics: function(typeOfSearch, productId) {
            var conversionCategory = "ProductSearch";
            if (typeOfSearch == "imageSearch") {
                conversionCategory = "ProductSearchImageSearch";
            } else if (typeOfSearch == "VoiceSearch") {
                conversionCategory = "ProductSearchVoiceSearch";
            } else if (typeOfSearch == "textSearch") {
                conversionCategory = "ProductSearchText";
            } else if (typeOfSearch == "barcodeScan") {
                conversionCategory = "ProductSearchBarcode";
            } else if (typeOfSearch == "QRCodeSearch") {
                conversionCategory = "ProductSearchQRCodeSearch";
            } else {
                return;
            }
            TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumProductSearchClicks, {
                conversion_category: conversionCategory,
                conversion_id: productId,
                conversion_action: 2
            });
        },
        checkForWhichSearch: function(typeOfSearch) {
            var searchType = "";
            if (typeOfSearch == "imageSearch") {
                searchType = "ImageSearch";
            } else if (typeOfSearch == "VoiceSearch") {
                searchType = "VoiceSearch";
            } else if (typeOfSearch == "textSearch") {
                searchType = "TextSearch";
            } else if (typeOfSearch == "barcodeScan") {
                searchType = "BarcodeSearch";
            } else if (typeOfSearch == "QRCodeSearch") {
                searchType = "QRCodeSearch";
            }
            return searchType;
        },
        updateCart: function() {
            if (MVCApp.Toolbox.common.getRegion() !== "US") {
                frmCartView.flxHeaderTitleContainer.btnCart.isVisible = false;
                frmCartView.flxHeaderTitleContainer.flxCart.isVisible = false;
                frmHome.flxHeaderWrap.btnCart.isVisible = false;
                frmMore.flxHeaderWrap.btnCart.isVisible = false;
                frmPDP.flxHeaderWrap.btnCart.isVisible = false;
                frmPJDP.flxHeaderWrap.btnCart.isVisible = false;
                frmProductCategory.flxHeaderWrap.btnCart.isVisible = false;
                frmProductCategoryMore.flxHeaderWrap.btnCart.isVisible = false;
                frmProducts.flxHeaderWrap.btnCart.isVisible = false;
                frmProductsList.flxHeaderWrap.btnCart.isVisible = false;
                frmProjectsCategory.flxHeaderWrap.btnCart.isVisible = false;
                frmProjectsList.flxHeaderWrap.btnCart.isVisible = false;
                frmShoppingList.flxHeaderWrap.btnCart.isVisible = false;
                frmWebView.flxHeaderWrap.btnCart.isVisible = false;
                frmWeeklyAd.flxHeaderWrap.btnCart.isVisible = false;
                frmWeeklyAdDetail.flxHeaderWrap.btnCart.isVisible = false;
                frmHome.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmMore.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmPDP.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmPJDP.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProductCategory.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProductCategoryMore.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProducts.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProductsList.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProjectsCategory.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProjectsList.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmShoppingList.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmWebView.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmWeeklyAd.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProjects.flxHeaderWrap.btnCart.isVisible = false;
                frmProjects.flxHeaderWrap.lblItemsCount.isVisible = false;
                frmProjects.flxHeaderWrap.flxCart.isVisible = false;
                frmHome.flxHeaderWrap.flxCart.isVisible = false;
                frmMore.flxHeaderWrap.flxCart.isVisible = false;
                frmPDP.flxHeaderWrap.flxCart.isVisible = false;
                frmPJDP.flxHeaderWrap.flxCart.isVisible = false;
                frmProductCategory.flxHeaderWrap.flxCart.isVisible = false;
                frmProductCategoryMore.flxHeaderWrap.flxCart.isVisible = false;
                frmProducts.flxHeaderWrap.flxCart.isVisible = false;
                frmProductsList.flxHeaderWrap.flxCart.isVisible = false;
                frmProjectsCategory.flxHeaderWrap.flxCart.isVisible = false;
                frmProjectsList.flxHeaderWrap.flxCart.isVisible = false;
                frmShoppingList.flxHeaderWrap.flxCart.isVisible = false;
                frmWebView.flxHeaderWrap.flxCart.isVisible = false;
                frmWeeklyAd.flxHeaderWrap.flxCart.isVisible = false;
                frmWeeklyAdDetail.flxHeaderWrap.flxCart.isVisible = false;
                frmWeeklyAdHome.flxHeaderWrap.flxCart.isVisible = false;
            } else {
                /* frmHome.flxHeaderWrap.btnCart.isVisible = true;
         frmMore.flxHeaderWrap.btnCart.isVisible = true;
         frmPDP.flxHeaderWrap.btnCart.isVisible = true;
         frmPJDP.flxHeaderWrap.btnCart.isVisible = true;
         frmProductCategory.flxHeaderWrap.btnCart.isVisible = true;
         frmProductCategoryMore.flxHeaderWrap.btnCart.isVisible = true;
         frmProducts.flxHeaderWrap.btnCart.isVisible = true;
         frmProductsList.flxHeaderWrap.btnCart.isVisible = true;
         frmProjectsCategory.flxHeaderWrap.btnCart.isVisible = true;
         frmProjectsList.flxHeaderWrap.btnCart.isVisible = true;
         frmShoppingList.flxHeaderWrap.btnCart.isVisible = true;
         frmWeeklyAd.flxHeaderWrap.btnCart.isVisible = true;
         frmWeeklyAdDetail.flxHeaderWrap.btnCart.isVisible = true;
         frmHome.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmMore.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmPDP.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmPJDP.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProductCategory.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProductCategoryMore.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProducts.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProductsList.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProjectsCategory.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProjectsList.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmShoppingList.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmWebView.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmWeeklyAd.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProjects.flxHeaderWrap.btnCart.isVisible = true;
		 frmProjects.flxHeaderWrap.lblItemsCount.isVisible = true;
         frmProjects.flxHeaderWrap.flxCart.isVisible = true;
         frmHome.flxHeaderWrap.flxCart.isVisible = true;
         frmMore.flxHeaderWrap.flxCart.isVisible = true;
         frmPDP.flxHeaderWrap.flxCart.isVisible = true;
         frmPJDP.flxHeaderWrap.flxCart.isVisible = true;
         frmProductCategory.flxHeaderWrap.flxCart.isVisible = true;
         frmProductCategoryMore.flxHeaderWrap.flxCart.isVisible = true;
         frmProducts.flxHeaderWrap.flxCart.isVisible = true;
         frmProductsList.flxHeaderWrap.flxCart.isVisible = true;
         frmProjectsCategory.flxHeaderWrap.flxCart.isVisible = true;
         frmProjectsList.flxHeaderWrap.flxCart.isVisible = true;
         frmShoppingList.flxHeaderWrap.flxCart.isVisible = true;
         
         frmWeeklyAd.flxHeaderWrap.flxCart.isVisible = true;
         frmWeeklyAdDetail.flxHeaderWrap.flxCart.isVisible = true;
         //MVCApp.Toolbox.common.updateCartIcon(frmWebView);
		 frmWeeklyAdHome.flxHeaderWrap.flxCart.isVisible = true;*/
                frmWebView.flxHeaderWrap.flxCart.isVisible = true;
                frmWebView.flxHeaderWrap.btnCart.isVisible = true;
                frmWebView.flxHeaderWrap.lblItemsCount.isVisible = true;
                frmCartView.flxHeaderTitleContainer.btnCart.isVisible = true;
                frmCartView.flxHeaderTitleContainer.flxCart.isVisible = true;
                frmCartView.flxHeaderTitleContainer.flxCart.lblItemsCount.isVisible = true;
            }
            // frmShoppingList.flxHeaderWrap.lblItemsCount.text= numItems;// patch fix to update Shopping List on Back from Cart
        },
        updateWeeklyAdd: function() {
            frmHome.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmCartView.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmHelp.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmCreateAccount.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmEventDetail.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmEvents.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmFindStoreMapView.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmHelp.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmHome.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmMore.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmMyAccount.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmPDP.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmPJDP.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProductCategory.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProductCategoryMore.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProducts.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProductsList.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProjects.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProjectsCategory.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmProjectsList.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmReviewProfile.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmReviews.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProfileAddChild.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProfileInterests.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProfileSkills.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProfileStep1.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProfileStep2.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProfileStep4.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmRewardsProgramInfo.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmSettings.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmShoppingList.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmSignIn.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmSortAndRefine.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmWebView.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmWeeklyAd.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmWeeklyAdDetail.flxFooterWrap.flxWeeklyAd.width = "22%";
            frmWeeklyAdHome.flxFooterWrap.flxWeeklyAd.width = "22%";
        },
        sendTealiumDataForAppSearchCancellation: function() {
            TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCancellation, {
                conversion_category: "App SearchCancelled",
                conversion_id: "VoiceSearch",
                conversion_action: 2
            });
        },
        validateVoiceSearchPermissions: function(myForm) {
            var SFSpeechRecognizer = objc.import('SFSpeechRecognizer');
            var reuestAuth = SFSpeechRecognizer.authorizationStatus();
            var requestMicroPhoneAuth = redirectToSettings.checkMicroPhoneSettings();
            var permissionsGrantedForSpeech = false;
            var permissionsGrantedForMicrophone = false;
            var askedForFirstTime = false;
            if (reuestAuth == SFSpeechRecognizerAuthorizationStatusNotDetermined) {
                askedForFirstTime = true;
                SFSpeechRecognizer.requestAuthorization(function(authStatus) {
                    if (authStatus == SFSpeechRecognizerAuthorizationStatusAuthorized) {
                        permissionsGrantedForSpeech = true;
                    } else if (authStatus == SFSpeechRecognizerAuthorizationStatusDenied) {
                        permissionsGrantedForSpeech = false;
                    }
                    permissionsGrantedForMicrophone = MVCApp.Toolbox.common.validateSpeechAudioPermission(requestMicroPhoneAuth, myForm, permissionsGrantedForSpeech);
                });
            } else if (reuestAuth == SFSpeechRecognizerAuthorizationStatusAuthorized) {
                permissionsGrantedForSpeech = true;
                permissionsGrantedForMicrophone = MVCApp.Toolbox.common.validateSpeechAudioPermission(requestMicroPhoneAuth, myForm, permissionsGrantedForSpeech);
            } else if (reuestAuth == SFSpeechRecognizerAuthorizationStatusDenied) {
                permissionsGrantedForSpeech = false;
                permissionsGrantedForMicrophone = MVCApp.Toolbox.common.validateSpeechAudioPermission(requestMicroPhoneAuth, myForm, permissionsGrantedForSpeech);
            }
            if (askedForFirstTime) {
                kony.print("do nothing");
            } else {
                if (permissionsGrantedForMicrophone && permissionsGrantedForSpeech) {
                    if (!createVoiceAndSpeechObjectsFlag) {
                        createSingletons();
                    }
                    voiceSearchFlag = true;
                    myForm.flxSearchOverlay.setVisibility(false);
                    previousForm = myForm;
                    showMicPanel(myForm);
                } else {
                    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.CustomVoiceDescription"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.CustomVoiceSearchUsageTitle"), constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.voiceSearchSettingsRedirect, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.voiceSearch.okayButton"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.voiceSearch.DontAllow"));
                }
            }
        },
        validateSpeechAudioPermission: function(requestMicroPhoneAuth, myForm, requestSpeechRecognition) {
            var voiceSearchMicPermission = false;
            if (requestMicroPhoneAuth == 2) {
                redirectToSettings.requestMicroPhonePermission(function(result) {
                    if (result == "TRUE") {
                        if (requestSpeechRecognition) {
                            if (!createVoiceAndSpeechObjectsFlag) {
                                createSingletons();
                            }
                            voiceSearchFlag = true;
                            myForm.flxSearchOverlay.setVisibility(false);
                            previousForm = myForm;
                            showMicPanel(myForm);
                        }
                        voiceSearchMicPermission = true;
                    } else {
                        voiceSearchMicPermission = false;
                    }
                });
            } else if (requestMicroPhoneAuth == 1) {
                voiceSearchMicPermission = false;
            } else if (requestMicroPhoneAuth == 0) {
                voiceSearchMicPermission = true;
            }
            return voiceSearchMicPermission;
        },
        voiceSearchSettingsRedirect: function(response) {
            if (response) {
                redirectToSettings.redirectUserToCameraSettings();
            }
        },
        getSenderId: function() {
            var url = appConfig.isturlbase || "";
            url = url.toLowerCase();
            var idFromi18n = "";
            var senderId = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.pushNoti.senderId");
            if (url.indexOf("michaels-qa") > 0 || url.indexOf("michaels-uat") > 0 || url.indexOf("michaels-dev") > 0 || url.indexOf("michaels-stg") > 0) {
                idFromi18n = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.pushNoti.senderIdDev") || "";
            } else {
                idFromi18n = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.pushNoti.senderIdProd") || "";
            }
            if (idFromi18n != "") {
                senderId = idFromi18n;
            }
            return senderId;
        },
        determineIfExternal: function(url) {
            var urlLowerCase = url.toLowerCase();
            if ((urlLowerCase.indexOf("appservices") < 0)) {
                return true;
            }
            return false;
        },
        openApplicationURL: function(url) {
            var tid = OneManager ? OneManager.getTID() : "";
            var urlLowerCase = url.toLowerCase();
            if ((urlLowerCase.indexOf("appservices") < 0)) {
                if ((urlLowerCase.indexOf("?") < 0)) {
                    url += "?one-tid=" + tid;
                } else {
                    url += "&one-tid=" + tid;
                }
            }
            kony.application.openURL(url);
        },
        getEntryState: function() {
            if (appInForeground) {
                return "foreground";
            } else {
                return "background";
            }
        },
        trackAppLaunchPushNotification: function() {
            var lDeviceTimeStamp = JSON.stringify(new Date());
            var lDeviceId = MVCApp.Toolbox.common.getDeviceID();
            TealiumManager.trackEvent("App Launch Push Notification", {
                conversion_category: "Push Notification",
                conversion_id: "App Launched",
                conversion_action: 2,
                applaunch_push_notification: true,
                device_time_stamp: lDeviceTimeStamp,
                device_id: lDeviceId,
                campaign_id: gblCampaignId
            });
            gblCampaignId = "";
        },
        sendBeaconDataToAnalytics: function() {
            var beaconString = kony.store.getItem("beaconData") || "";
            var objArr = "";
            var obj = {};
            var reportsToBeRecorded = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.beaconAnalyticsTobeRecorded") || "";
            if ("" === reportsToBeRecorded || "TRUE" === reportsToBeRecorded) {
                if (null !== beaconString && "" !== beaconString) {
                    objArr = JSON.parse(beaconString);
                    for (var i = 0; i < objArr.length; i++) {
                        obj = objArr[i];
                        TealiumManager.trackEvent("Time delay in Beacon Notification", {
                            beacon_status: obj.status,
                            notification_time_taken: obj.timeTaken,
                            beacon_entry_identifier: obj.uniqueID,
                            beacon_major: obj.major,
                            beacon_UUID: obj.UUID
                        });
                        MVCApp.sendMetricReport(frmHome, [{
                            "status": obj.status,
                            "time_taken": obj.timeTaken,
                            "entry_identifier": obj.uniqueID,
                            "major": obj.major,
                            "UUID": obj.UUID
                        }]);
                    }
                }
            }
            kony.store.setItem("beaconData", "");
        },
        checkIfUserIsForTouchID: function(userName) {
            var touchIDUser = kony.store.getItem("storeFirstUserForTouchId") || "";
            if (touchIDUser) {
                if (userName == touchIDUser) {
                    kony.print("the user credentials of Touch ID has been changed");
                    kony.store.setItem("hideTouchIDAlertForLoginPage", true);
                }
            }
        },
        //CHECKS FOR THE AVAILABILITY OF NETWORK
        checkIfNetworkExists: function() {
            return (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY));
        },
        setDuration: function(craftTime) {
            var duration = craftTime || "";
            var indicator = "";
            if (duration === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.AboutAnHour") || duration === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.OverAnHour")) {
                indicator = "time_4.png";
            } else if (duration === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.About45Minutes")) {
                indicator = "time_3.png";
            } else if (duration === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.About30Minutes")) {
                indicator = "time_2.png";
            } else if (duration === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.About15Minutes")) {
                indicator = "time_1.png";
            } else {
                indicator = "time_1.png";
            }
            return indicator;
        },
        setSkillLevel: function(skillLevel) {
            var skill = skillLevel || "";
            var skillIndicator = "";
            if (skill === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.Beginner")) {
                skillIndicator = "skill_new_1.png";
            } else if (skill === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.Intermediate")) {
                skillIndicator = "skill_new_2.png";
            } else if (skill === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.Expert") || skill === MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProjectsList.Advanced")) {
                skillIndicator = "skill_new_3.png";
            } else {
                skillIndicator = "skill_new_3.png";
            }
            return skillIndicator;
        },
        exit: function() {
            try {
                kony.application.exit();
            } catch (Error) {
                alert("Exception While exiting the application  : " + Error);
                if (Error) {
                    TealiumManager.trackEvent("App Exit Exception", {
                        exception_name: Error.name,
                        exception_reason: Error.message,
                        exception_trace: Error.stack,
                        exception_type: Error
                    });
                }
            }
        },
        showInMenu: function(hideFlag, showFlag, showInAppFlag) {
            var hideOnMobile = hideFlag || false;
            var showMenu = showFlag || false;
            var showInAppFlag = showInAppFlag || false;
            var menuInDisplay = true;
            if (showInAppFlag == false || showInAppFlag == "false") {
                menuInDisplay = false;
            }
            return menuInDisplay;
        },
        isLocaleSupportedInApp: function(devlang) {
            var devLangauage = devlang.language;
            var devRegion = devlang.country;
            var locale = devLangauage + "_" + devRegion;
            return {
                support: MVCApp.Toolbox.common.isFoundInLangArray(locale),
                language: locale
            };
        },
        getImages: function(imageGroups) {
            var smallImagesArray = [];
            var mediumImagesArray = [];
            var largeImagesArray = [];
            var zoomImagesArray = [];
            for (var i = 0; i < imageGroups.length; i++) {
                if ((imageGroups[i].view_type || "") == "small") {
                    smallImagesArray.push(imageGroups[i].images);
                }
                if ((imageGroups[i].view_type || "") == "medium") {
                    mediumImagesArray.push(imageGroups[i].images);
                }
                if ((imageGroups[i].view_type || "") == "large") {
                    largeImagesArray.push(imageGroups[i].images);
                }
                if ((imageGroups[i].view_type || "") == "zoom") {
                    zoomImagesArray.push(imageGroups[i].images);
                }
            }
            var imagesArray = {};
            imagesArray.smallImages = smallImagesArray;
            imagesArray.mediumImages = mediumImagesArray;
            imagesArray.largeImages = largeImagesArray;
            imagesArray.zoomImages = zoomImagesArray;
            return imagesArray;
        },
        indexArrayPushResults: function(pdpDataObj, varientFlag, varientChangeFlag, name, barCodeFlag, formName) {
            var indLen = pdpIndexArray.length;
            var prevData = "";
            if (varientChangeFlag) {
                for (j = indLen; j > 0; j--) {
                    var tempData = kony.store.getItem((j).toString());
                    tempData = tempData + "";
                    tempData = JSON.parse(tempData);
                    if (!tempData.change) {
                        prevData = tempData.data;
                        break;
                    }
                }
            }
            indLen = indLen + 1;
            pdpIndexArray.push(indLen);
            var temp = {
                "data": JSON.stringify(pdpDataObj),
                "varFlag": varientFlag,
                "change": varientChangeFlag,
                "id": name,
                "barCodeFlag": barCodeFlag,
                "prevData": prevData,
                "formName": formName
            };
            kony.store.setItem(indLen.toString(), JSON.stringify(temp));
        },
        indexArrayPopResults: function() {
            var indLen1 = pdpIndexArray.length;
            var formContent = kony.store.getItem((indLen1 - 1).toString());
            kony.store.removeItem((indLen1).toString());
            pdpIndexArray.pop();
            return formContent;
        },
        recommendedProductPush: function(res, variantFlag) {
            var len = pdpRecomProducts.length;
            len = len + 1;
            pdpRecomProducts.push(len);
            if (variantFlag) {
                kony.store.setItem("RP" + len.toString(), kony.store.getItem("RP" + (len - 1).toString()));
            } else {
                kony.store.setItem("RP" + len.toString(), JSON.stringify(res));
            }
        },
        recommendedProductPop: function() {
            var len1 = pdpRecomProducts.length;
            var formContent1 = kony.store.getItem("RP" + (len1 - 1).toString());
            kony.store.removeItem("RP" + (len1).toString());
            pdpRecomProducts.pop();
            return formContent1;
        },
        imagesPush: function(res, productId) {
            var temp = {};
            temp.res = res;
            temp.productId = productId;
            var len2 = pdpImages.length;
            len2 = len2 + 1;
            pdpImages.push(len2);
            kony.store.setItem("I" + len2.toString(), JSON.stringify(temp));
        },
        imagesPop: function(productId) {
            var len3 = pdpImages.length;
            var formContent2 = kony.store.getItem("I" + (len3 - 1).toString()) || "";
            var temp = formContent2 + "";
            if (temp !== "") {
                temp = JSON.parse(temp);
                kony.store.removeItem("I" + (len3).toString());
                pdpImages.pop();
                return temp.res;
            }
        },
        clearPdpGlobalVariables: function() {
            if (pdpIndexArray.length > 0) {
                for (k = pdpIndexArray.length; k > 0; k--) {
                    kony.store.removeItem((k).toString());
                }
            }
            if (pdpRecomProducts.length > 0) {
                for (k = pdpRecomProducts.length; k > 0; k--) {
                    kony.store.removeItem("RP" + (k).toString());
                }
            }
            if (pdpImages.length > 0) {
                for (k = pdpImages.length; k > 0; k--) {
                    kony.store.removeItem("I" + (k).toString());
                }
            }
            pdpIndexArray = [];
            pdpRecomProducts = [];
            pdpImages = [];
        },
        //Sets the text of the widgets as per the current Locale
        geti18nValueVA: function(key) {
            key = key || "";
            if (key === "") {
                return "";
            }
            var resultString = "";
            resultString = kony.i18n.getLocalizedString(key);
            return resultString;
        },
        replacePipesAndColons: function(imageString) {
            var n = imageString.indexOf("fit=inside");
            if (n > 0) {
                var strOld = imageString.substring(0, n);
                var strNew = imageString.substring(n, imageString.length);
                strNew = strNew.replace("|", "%7C");
                strNew = strNew.replace(":", "%3A");
                imageString = strOld + strNew;
            }
            return imageString;
        },
        // http to https 
        replaceWithHttps: function(url) {
            url = url || "";
            if (url !== "") {
                url = url.replace("http:", "https:");
            }
            return url;
        },
        //Shows the loading indicator
        showLoadingIndicator: function(text) {
            text = text || "";
            kony.application.showLoadingScreen(null, text, constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        },
        //shows loading indicator for voice search
        showLoadingIndicatorForVoiceSearch: function(text) {
            text = text || "";
            kony.application.showLoadingScreen(null, text, constants.LOADING_SCREEN_POSITION_ONLY_CENTER, false, true, null);
        },
        showLoadingIndicatorForWebViews: function(text) {
            text = text || "";
            kony.application.showLoadingScreen(null, text, constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
        },
        //dismisses the loading indicator
        dismissLoadingIndicator: function() {
            kony.application.dismissLoadingScreen();
        },
        destroyStoreLoc: function() {},
        //Transition
        setTransition: function(frm, Transtype) {
            frm.inTransitionConfig = {
                "formAnimation": Transtype,
                transitionDuration: "0.1"
            };
        },
        getAisleNumber: function(value) {
            if (value !== "") {
                if (!isNaN(value)) {
                    return "S" + value;
                } else {
                    return value;
                }
            } else {
                return "";
            }
        },
        getStockInfo: function(inStoreOnlineFlag, stockLevel, ats, cStoreInventory) {
            var lMyLocationFlag = false;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            var stockInfo = {};
            stockInfo.storeInventoryFlag = false;
            stockInfo.storeInventoryStatus = "";
            stockInfo.aisleNoStatus = false;
            stockInfo.buyOnlineStatus = false;
            stockInfo.buyOnlineDisabilityFlag = false;
            if (lMyLocationFlag === "true") {
                var stockQty = cStoreInventory.ats;
                if (stockQty != null && stockQty != undefined) {
                    stockQty = parseInt(stockQty);
                } else {
                    stockQty = "";
                }
                if (inStoreOnlineFlag == "Sellable") {
                    var stockStatus = MVCApp.Toolbox.common.getStockStatusAllCaps(stockQty, cStoreInventory);
                    if (stockQty !== "" && ats !== "") {
                        stockInfo.storeInventoryFlag = true;
                        stockInfo.storeInventoryStatus = stockStatus;
                        stockInfo.aisleNoStatus = true;
                        stockInfo.buyOnlineStatus = true;
                        stockInfo.buyOnlineDisabilityFlag = false;
                    } else if (stockQty !== "" && ats === "") {
                        stockInfo.storeInventoryFlag = true;
                        stockInfo.storeInventoryStatus = stockStatus;
                        stockInfo.aisleNoStatus = true;
                        stockInfo.buyOnlineStatus = true;
                        stockInfo.buyOnlineDisabilityFlag = true;
                    } else if (stockQty === "" && ats !== "") {
                        stockInfo.storeInventoryFlag = true;
                        stockInfo.storeInventoryStatus = stockStatus;
                        stockInfo.aisleNoStatus = false;
                        stockInfo.buyOnlineStatus = true;
                        stockInfo.buyOnlineDisabilityFlag = false;
                    }
                } else if (inStoreOnlineFlag == "Online" || inStoreOnlineFlag == "Dropship") {
                    stockInfo.storeInventoryFlag = true;
                    stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                    stockInfo.aisleNoStatus = false;
                    stockInfo.buyOnlineStatus = true;
                    if (ats !== "") {
                        stockInfo.buyOnlineDisabilityFlag = false;
                    } else {
                        stockInfo.buyOnlineDisabilityFlag = true;
                    }
                } else if (inStoreOnlineFlag == "InStore") {
                    var stockStatus1 = MVCApp.Toolbox.common.getStockStatusAllCaps(stockQty);
                    stockInfo.buyOnlineStatus = false;
                    stockInfo.buyOnlineDisabilityFlag = false;
                    stockInfo.storeInventoryFlag = true;
                    if (stockQty !== "") {
                        stockInfo.storeInventoryStatus = stockStatus1;
                        stockInfo.aisleNoStatus = true;
                    } else {
                        stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly");
                        stockInfo.aisleNoStatus = false;
                    }
                }
            } else {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    if (inStoreOnlineFlag == "Sellable") {
                        if (ats !== "") {
                            stockInfo.storeInventoryFlag = false;
                            stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.getStockStatusAllCaps(ats);
                            stockInfo.aisleNoStatus = false;
                            stockInfo.buyOnlineStatus = true;
                            stockInfo.buyOnlineDisabilityFlag = false;
                        } else {
                            stockInfo.storeInventoryFlag = true;
                            stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.getStockStatusAllCaps(ats);
                            stockInfo.aisleNoStatus = false;
                            stockInfo.buyOnlineStatus = false;
                            stockInfo.buyOnlineDisabilityFlag = false;
                        }
                    } else if (inStoreOnlineFlag == "Online" || inStoreOnlineFlag == "Dropship") {
                        if (ats !== "") {
                            stockInfo.storeInventoryFlag = true;
                            stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                            stockInfo.aisleNoStatus = false;
                            stockInfo.buyOnlineStatus = true;
                            stockInfo.buyOnlineDisabilityFlag = false;
                        } else {
                            stockInfo.storeInventoryFlag = true;
                            stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                            stockInfo.aisleNoStatus = false;
                            stockInfo.buyOnlineStatus = true;
                            stockInfo.buyOnlineDisabilityFlag = true;
                        }
                    } else if (inStoreOnlineFlag == "InStore") {
                        stockInfo.storeInventoryFlag = true;
                        stockInfo.storeInventoryStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly");
                        stockInfo.aisleNoStatus = false;
                        stockInfo.buyOnlineStatus = false;
                        stockInfo.buyOnlineDisabilityFlag = false;
                    }
                } else {
                    stockInfo.storeInventoryFlag = false;
                    stockInfo.storeInventoryStatus = "";
                    stockInfo.aisleNoStatus = false;
                    stockInfo.buyOnlineStatus = false;
                    stockInfo.buyOnlineDisabilityFlag = false;
                }
            }
            return stockInfo;
        },
        getStockStatusOfNearbyStores: function(stockLevel) {
            if (stockLevel >= MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.maximum")) {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.smallInStock");
            } else if (stockLevel < MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.maximum") && stockLevel > MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.minimum")) {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.limitedQuantities");
            } else if (stockLevel == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.minimum")) {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.outOfStock");
            } else if (stockLevel === "") {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.notAvailable");
            }
        },
        getStockStatusAllCaps: function(stockLevel, cstoreInvenory) {
            if (stockLevel >= MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.maximum")) {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.inStock");
            } else if (stockLevel < MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.maximum") && stockLevel > MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.minimum")) {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.limitedQuantities");
            } else if (stockLevel == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.stockLevel.minimum")) {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.outOfStock");
            } else if (stockLevel === "" && cstoreInvenory !== null && cstoreInvenory !== undefined && cstoreInvenory !== "") {
                return MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmPDP.notAvailable");
            } else if (stockLevel === "") {
                return "";
            }
        },
        getStockStatus: function(stockLevel) {
            // changed as per discussion 19-05-17
            if (stockLevel >= 3) {
                return "IN STOCK";
            } else if (stockLevel < 3 && stockLevel > 0) {
                return "Limited Quantities";
            } else if (stockLevel === 0) {
                return "Out of Stock";
            } else if (stockLevel === "") {
                return "Not Available at this location";
            }
        },
        getStoreMapElementName: function(aisleNo) {
            var aisleInfo = {};
            var item = "";
            var str = "";
            var myArray = "";
            aisleInfo.name = "";
            aisleInfo.number = "";
            if (aisleNo) {
                aisleNo = aisleNo.toUpperCase();
            }
            if (aisleNo.indexOf("EC") === 0) {
                item = /[EC]{2}[0-9]{1}/;
                str = aisleNo.replace(" ", "");
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.endCap");
                    aisleInfo.number = aisleNo.replace("EC", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("CEC") === 0) {
                item = /[CEC]{3}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.endCap");
                    aisleInfo.number = aisleNo.replace("CEC", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("SEC") === 0) {
                item = /[SEC]{3}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.endCap");
                    aisleInfo.number = aisleNo.replace("SEC", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("DA") === 0) {
                item = /[DA]{2}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.mainAisle");
                    aisleInfo.number = ""; //aisleNo.replace("DA-", "");
                    aisleInfo.number = ""; //aisleInfo.number.replace("DA", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("MDAG") === 0) {
                item = /[MDAG]{4}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.mainAisle");
                    aisleInfo.number = ""; //aisleNo.replace("MDAG", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("SC") === 0) {
                item = /[SC]{2}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = ""; //MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.ailse");
                    aisleInfo.number = ""; //aisleNo.replace("SC", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("SSC") === 0) {
                item = /[SSC]{3}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.ailse");
                    aisleInfo.number = aisleNo.replace("SSC", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("SPP") === 0) {
                item = /[SPP]{3}/;
                str = aisleNo;
                myArray = str.match(item);
                if (myArray !== null) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.endCap");
                    aisleInfo.number = aisleNo.replace("SPP", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("S") === 0) {
                var match_1 = aisleNo.match(/(\D+)?\d/);
                var index_1 = match_1 ? match_1[0].length - 1 : -1;
                if (index_1 == 1) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.ailse");
                    aisleInfo.number = aisleNo.replace("S", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("C") === 0) {
                var match_1 = aisleNo.match(/(\D+)?\d/);
                var index_1 = match_1 ? match_1[0].length - 1 : -1;
                if (index_1 == 1) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.ailse");
                    aisleInfo.number = aisleNo.replace("C", "");
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("W") === 0) {
                var match_1 = aisleNo.match(/(\D+)?\d/);
                var index_1 = match_1 ? match_1[0].length - 1 : -1;
                if (index_1 == 1) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.wall");
                    aisleInfo.number = "";
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else if (aisleNo.indexOf("M") === 0) {
                var match_1 = aisleNo.match(/(\D+)?\d/);
                var index_1 = match_1 ? match_1[0].length - 1 : -1;
                if (index_1 == 1) {
                    aisleInfo.name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.wall");
                    aisleInfo.number = "";
                } else {
                    aisleInfo.name = "";
                    aisleInfo.number = "";
                }
                return aisleInfo;
            } else {
                return aisleInfo;
            }
        },
        encodeBASE64: function(input) {
            var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" + "ghijklmnopqrstuv" + "wxyz0123456789+/" + "=";
            input = escape(input);
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
            return output;
        },
        isArray: function(value) {
            return Object.prototype.toString.call(value) === '[object Array]';
        },
        alertWeeklyHandlerCallBack: function(response) {
            MVCApp.getLocatorMapController().load("");
        },
        //To be used to show alerts useful to the application. For eg: Error Alerts. 
        //ONLY yes and No labels are non-configurable. 
        customAlert: function(alertMessage, title, type, alertHandlerCallBack, yesLabel, noLabel) {
            var titleMsg = title || "";
            if (titleMsg === "") {
                titleMsg = "";
            }
            var pspConfig = {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT,
                "contentAlignment": constants.ALERT_CONTENT_ALIGN_CENTER
            };
            kony.ui.Alert({
                "message": alertMessage,
                "alertType": type,
                "alertTitle": titleMsg,
                "yesLabel": yesLabel,
                "noLabel": noLabel,
                "alertIcon": "transparent.png",
                "alertHandler": alertHandlerCallBack
            }, pspConfig);
        },
        customAlertNoTitle: function(alertMessage, title) {
            var titleMsg = title || "";
            if (titleMsg === "") {
                titleMsg = "";
            }
            var pspConfig = {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT,
                "contentAlignment": constants.ALERT_CONTENT_ALIGN_CENTER
            };
            var alert = kony.ui.Alert({
                "message": alertMessage,
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": title,
                "yesLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"),
                "noLabel": "",
                "alertIcon": "transparent.png",
                "alertHandler": null
            }, pspConfig);
        },
        redirectToStoreLocation: function(checkFlag) {
            if (checkFlag) {
                MVCApp.getLocatorMapController().load("");
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                if (voiceSearchFlag) {
                    MVCApp.Customer360.callVoiceSearchResult("failure", "User has no store selected", "");
                    onXButtonClick(frmObject);
                    showMicPanel(frmObject);
                }
                MVCApp.getStoreMapController().setSearchTerm("");
            }
        },
        showPriceRange: function(pricemin, priceMax) {
            return (MVCApp.Toolbox.common.addDollarsToNumber(pricemin) + " - " + MVCApp.Toolbox.common.addDollarsToNumber(priceMax));
        },
        showPriceDecimals: function(price) {
            if (price != "") {
                price = parseFloat(price);
                return price.toFixed(2);
            } else {
                return price;
            }
        },
        addDollarsToNumber: function(number) {
            var price = number || "";
            var langReg = kony.store.getItem("languageSelected");
            if (price != "") {
                price = MVCApp.Toolbox.common.showPriceDecimals(price);
                if (langReg === "fr_CA") {
                    // price=price+"$";
                    if (price !== null && price !== undefined) {
                        price = parseFloat(price);
                        price = Number(price).toLocaleString('fr') + "$";
                    }
                } else {
                    price = "$" + price;
                }
            }
            return price;
        },
        isArray: function(object) {
            return true;
        },
        getListOfPins: function(data) {
            var listOfPins = [];
            for (var i = 0; i < data.length; i++) {
                var temp = {};
                temp.lat = data[i].lat;
                temp.lon = data[i].lon;
                listOfPins.push(temp);
            }
            return listOfPins;
        },
        getPolyline: function(data) {
            var topIndex = 0;
            var bottomIndex = 0;
            var rightIndex = 0;
            var leftIndex = 0;
            var topLat = 0;
            var bottomLat = 0;
            var rightLat = 0;
            var leftLat = 0;
            var topLon = 0;
            var bottomLon = 0;
            var rightLon = 0;
            var leftLon = 0;
            var listOfPins = MVCApp.Toolbox.common.getListOfPins(data);
            var pin = listOfPins[0];
            for (var i = 0; i < listOfPins.length; i++) {
                var pin = listOfPins[i];
                var lat = pin.lat;
                var lon = pin.lon;
                lat = parseFloat(lat);
                lon = parseFloat(lon);
                if (topLat == 0) {
                    topLat = lat;
                    topLon = lon;
                    topIndex = i;
                } else {
                    if (lat > topLat) {
                        topLat = lat;
                        topLon = lon;
                        topIndex = i;
                    }
                }
                if (bottomLat == 0) {
                    bottomLat = lat;
                    bottomLon = lon;
                    bottomIndex = i;
                } else {
                    if (lat < bottomLat) {
                        bottomLat = lat;
                        bottomLon = lon;
                        bottomIndex = i;
                    }
                }
                if (rightLon == 0) {
                    rightLon = lon;
                    rightLat = lat;
                    rightIndex = i;
                } else {
                    if (lon < rightLon) {
                        rightLon = lon;
                        rightLat = lat;
                        rightIndex = i;
                    }
                }
                if (leftLon == 0) {
                    leftLon = lon;
                    leftLat = lat;
                    leftIndex = i;
                } else {
                    if (lon > leftLon) {
                        leftLon = lon;
                        leftLat = lat;
                        leftIndex = i;
                    }
                }
                kony.print("After\n Top=>(" + topLat + "," + topLon + ")\n Bottom=>(" + bottomLat + "," + bottomLon + ")\n Right=>(" + rightLat + "," + rightLon + ")\n Left=>(" + leftLat + "," + leftLon + ")");
                kony.print(" Top Index=" + topIndex + "\n Bottom Index=" + bottomIndex + "\n Left Index=" + leftIndex + "\n Right Index=" + rightIndex);
            }
            var polyLineData = [{
                lat: topLat - 0.2,
                lon: topLon
            }, {
                lat: leftLat - 0.2,
                lon: leftLon
            }, {
                lat: bottomLat + 0.2,
                lon: bottomLon
            }, {
                lat: rightLat + 0.2,
                lon: rightLon
            }];
            return polyLineData;
        },
        isFoundInLangArray: function(language) {
            for (var i = 0; i < MVCApp.serviceConstants.languageArr.length; i++) {
                if (MVCApp.serviceConstants.languageArr[i] == language) {
                    return true
                }
            }
            return false;
        },
        getRegion: function() {
            var region = kony.store.getItem("Region");
            return region;
        },
        hideFooter: function() {
            try {
                var form = kony.application.getCurrentForm();
                if (form.flxFooterWrap) {
                    form.flxFooterWrap.animate(kony.ui.createAnimation({
                        "100": {
                            "stepConfig": {
                                "timingFunction": kony.anim.EASE
                            },
                            "bottom": "-70dp"
                        }
                    }), {
                        "delay": 0,
                        "iterationCount": 1,
                        "fillMode": kony.anim.FILL_MODE_FORWARDS,
                        "duration": 0.5
                    });
                }
            } catch (e) {
                kony.print("exception in hideFooter" + JSON.stringify(e));
            }
        },
        dispFooter: function() {
            try {
                var form = kony.application.getCurrentForm();
                if (form.flxFooterWrap) {
                    form.flxFooterWrap.animate(kony.ui.createAnimation({
                        "100": {
                            "stepConfig": {
                                "timingFunction": kony.anim.EASE
                            },
                            "bottom": "0dp"
                        }
                    }), {
                        "delay": 0,
                        "iterationCount": 1,
                        "fillMode": kony.anim.FILL_MODE_FORWARDS,
                        "duration": 0.5
                    });
                }
            } catch (e) {
                kony.print("exception in displayFooter" + JSON.stringify(e));
            }
        },
        setDefaultBrightnessValue: function(value) {
            gblBrightNessValue = value;
        },
        setBrightnesstoDefault: function() {
            try {
                var form = kony.application.getPreviousForm() || "";
                if (form) {
                    var formId = form.id || "";
                    var createInstanceObject = new bright.createInstance();
                    if (formId == "frmCoupons") {
                        createInstanceObject.setBrightness(gblBrightNessValue);
                    } else {
                        var returnedValue = createInstanceObject.getBrightness();
                        if (returnedValue) {
                            MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValue);
                            createInstanceObject.setBrightness(gblBrightNessValue);
                        }
                    }
                }
            } catch (e) {
                kony.print("Error with the previous Form " + JSON.stringify(e));
            }
        },
        setBrightnessForBarcode: function() {
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                var brightnessMaxLimit = "";
                var createInstanceObject = new bright.createInstance();
                var currentBrightness = createInstanceObject.getBrightness();
                brightnessMaxLimit = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.androidBrightnessMaxLimit");
                if (currentBrightness < brightnessMaxLimit) {
                    createInstanceObject.setBrightness(brightnessMaxLimit);
                }
            }
        },
        setBrightness: function(frmId) {
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                if (frmId == "frmCoupons") {
                    MVCApp.Toolbox.common.setBrightnessForBarcode();
                } else {
                    MVCApp.Toolbox.common.setBrightnesstoDefault();
                }
            }
        },
        MVCApp_encryptPin: function(inputstring) {
            try {
                var algo = MVCApp.encryptionConstants.subalgo;
                var encryptDecryptKey = "";
                encryptDecryptKey = kony.crypto.newKey(MVCApp.encryptionConstants.keyalgo, 128, {
                    passphrasetext: ["inputstring"],
                    subalgo: MVCApp.encryptionConstants.subalgo,
                    passphrasehashalgo: MVCApp.encryptionConstants.passphrasehashalgo
                });
                var prptobj = {
                    padding: MVCApp.encryptionConstants.padding,
                    mode: MVCApp.encryptionConstants.mode,
                    initializationvector: MVCApp.encryptionConstants.initializationvector
                };
                var encryptedText = kony.crypto.encrypt(algo, encryptDecryptKey, inputstring, prptobj);
                //alert("encrypted text---"+encryptedText);
                return encryptedText;
            } catch (err) {
                kony.print("Error  in callbackEncryptAes : " + err);
                return null;
            }
        },
        MVCApp_decryptPin: function(encryptedString) {
            try {
                var algo = MVCApp.encryptionConstants.subalgo;
                var encryptDecryptKey = "";
                encryptDecryptKey = kony.crypto.newKey(MVCApp.encryptionConstants.keyalgo, 128, {
                    passphrasetext: ["inputstring"],
                    subalgo: MVCApp.encryptionConstants.subalgo,
                    passphrasehashalgo: MVCApp.encryptionConstants.passphrasehashalgo
                });
                var prptobj = {
                    padding: MVCApp.encryptionConstants.padding,
                    mode: MVCApp.encryptionConstants.mode,
                    initializationvector: MVCApp.encryptionConstants.initializationvector
                };
                encryptedString = kony.convertToRawBytes(encryptedString);
                var decryptedString = kony.crypto.decrypt(algo, encryptDecryptKey, encryptedString, prptobj);
                //alert("decrypted text---"+decryptedString);
                return decryptedString;
            } catch (err) {
                alert("Error in callbackDecryptAes : " + err);
                return null;
            }
        },
        getDeviceID: function() {
            var deviceID = "";
            var deviceInfo = JSON.stringify(kony.os.deviceInfo());
            deviceID = kony.os.deviceInfo().deviceid;
            return deviceID;
        },
        getStoreID: function() {
            var storeID = "";
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                storeID = gblInStoreModeDetails.storeID;
            } else {
                var storeString = kony.store.getItem("myLocationDetails") || "";
                if (storeString != "") {
                    var storeObj = JSON.parse(storeString);
                    storeID = storeObj.clientkey || "";
                }
            }
            return storeID;
        },
        getFavouriteStore: function() {
            var storeID = "";
            var storeString = "";
            try {
                storeString = kony.store.getItem("myLocationDetails") || "";
            } catch (e) {
                kony.print("no key myLocationDetails exist");
            }
            if (storeString != "") {
                var storeObj = JSON.parse(storeString);
                storeID = storeObj.clientkey || "";
            }
            return storeID;
        },
        getLoyaltyID: function() {
            var loyaltyID = "";
            var userObject = kony.store.getItem("userObj");
            if (userObject && userObject.c_loyaltyMemberID !== null && userObject.c_loyaltyMemberID !== undefined) {
                loyaltyID = userObject.c_loyaltyMemberID;
            }
            return loyaltyID;
        },
        getLoginStatus: function() {
            var userObject = kony.store.getItem("userObj");
            if (userObject && userObject.c_loyaltyMemberID !== null && userObject.c_loyaltyMemberID !== undefined) {
                return "registeredUser";
            } else {
                return "guest";
            }
        },
        getKuid: function() {
            var kuid = "";
            var userObject = kony.store.getItem("userObj");
            if (userObject && userObject.customer_no !== null && userObject.customer_no !== undefined) {
                kuid = userObject.customer_no;
            }
            return kuid;
        },
        requestReview: function() {
            var ostype = "android" == kony.os.deviceInfo().name ? "android" : "iPhone";
            var allowReview = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.reviewRequest.formSpecific");
            var loginRequired = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.appreviews.loginRequired");
            var userStatusFlag = true;
            if (loginRequired == "true") {
                userStatusFlag = sfmcCustomObject.Indiv_Id ? true : false;
            }
            try {
                if (ostype == "iPhone" && allowReview == "true" && userStatusFlag) {
                    var UIDevice = objc.import("UIDevice");
                    var sysVersion = UIDevice.currentDevice().systemVersion || "0.0";
                    var version = sysVersion.split(".");
                    sysVersion = version[0] + "." + version[1];
                    kony.print("App Version..." + sysVersion);
                    if (Number(sysVersion) < MVCApp.iPhone.reviewRequestVersion) {
                        return;
                    }
                    var applicableFormsForAppReviews = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.appreviews.applicableForms");
                    var applicableFormsArray = applicableFormsForAppReviews ? applicableFormsForAppReviews.split(",") : [];
                    if (applicableFormsArray && applicableFormsArray.length > 0) {
                        var currentForm = kony.application.getCurrentForm();
                        if (currentForm && applicableFormsArray.indexOf(currentForm.id) != -1) {
                            var reviewRequestCounter = kony.store.getItem("reviewRequestCounter") || 1;
                            var moduloDivisor = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.appreviews.moduloDivisor"));
                            if ((reviewRequestCounter !== 1) && (reviewRequestCounter % moduloDivisor === 0)) {
                                kony.print("Invoking the App Review");
                                var sKStoreReviewController = objc.import("SKStoreReviewController");
                                kony.print("App Review started.");
                                sKStoreReviewController.requestReview();
                                kony.print("App Review done.");
                            }
                            kony.store.setItem("reviewRequestCounter", ++reviewRequestCounter);
                        }
                    }
                }
            } catch (e) {
                kony.print("Error with reviewRequest. " + JSON.stringify(e));
            }
        }
    },
    Service: {
        getClientId: function() {
            return MVCApp.Service.Constants.Common.clientId;
        },
        getLocale: function() {
            var locale = kony.store.getItem("languageSelected");
            if (locale == null || locale == "" || locale == undefined) {
                locale = "default";
            } else {
                if (locale == "en" || locale == "en_US") {
                    locale = "default";
                } else {
                    locale = locale.replace("_", "-");
                }
            }
            MVCApp.Service.Constants.Common.locale = locale;
            return MVCApp.Service.Constants.Common.locale;
        },
        setServiceType: function() {
            var selectedRegion = kony.store.getItem("Region");
            if (selectedRegion == "CA") {
                MVCApp.serviceType.DemandwareService = "DemandwareServiceCanada";
                MVCApp.serviceType.getAisleNumbers = "getAisleNumbersCanada";
                MVCApp.serviceType.getProductDetailsService = "variantsCanada";
                MVCApp.Service.Constants.Common.country = "CA";
                MVCApp.serviceConstants.defaultProjectId = "projects";
                MVCApp.Service.Constants.SearchProjects.refineString = "refine=cgid=projects";
                MVCApp.serviceType.imagesearch = "imagesearchCA";
                var locale = kony.store.getItem("languageSelected");
                if (locale == "fr_CA") {
                    MVCApp.serviceConstants.buyOnlineLink = MVCApp.serviceConstants.buyOnlineLink_fr_CA;
                } else {
                    MVCApp.serviceConstants.buyOnlineLink = MVCApp.serviceConstants.buyOnlineLink_en_CA;
                }
            } else {
                MVCApp.serviceType.DemandwareService = "DemandwareService";
                MVCApp.serviceType.getAisleNumbers = "getAisleNumbers";
                MVCApp.serviceType.getProductDetailsService = "variants";
                MVCApp.Service.Constants.Common.country = "US";
                MVCApp.serviceConstants.defaultProjectId = "Projects";
                MVCApp.Service.Constants.SearchProjects.refineString = "refine=cgid=Projects";
                MVCApp.serviceType.imagesearch = "imagesearch";
                MVCApp.serviceConstants.buyOnlineLink = MVCApp.serviceConstants.buyOnlineLink_US;
            }
        },
        setLocale: function(locale) {
            MVCApp.Service.Constants.Common.locale = locale;
        },
        getApiversion: function() {
            return MVCApp.Service.Constants.Reviews.apiversion;
        },
        gestPasskey: function() {
            return MVCApp.Service.Constants.Reviews.passkey;
        },
        getOffset: function() {
            return MVCApp.Service.Constants.Reviews.offset;
        },
        getCountry: function() {
            MVCApp.Service.Constants.Common.country = kony.store.getItem("Region");
            return MVCApp.Service.Constants.Common.country;
        },
        setCountry: function(country) {
            MVCApp.Service.Constants.Common.country = country;
        },
        setFlagForInstallation: function(value) {
            kony.store.setItem("firstIntallation", value); //value;
        },
        getFlagForInstallation: function() {
            var getFlag = kony.store.getItem("firstIntallation") || "";
            if (getFlag == "") {
                kony.store.setItem("firstIntallation", "true");
            }
            return kony.store.getItem("firstIntallation");
        },
        setMyLocationFlag: function(value) {
            MVCApp.Service.Constants.Common.myLocationSet = value ? "true" : "false";
            kony.store.setItem("setMyLocationFlag", MVCApp.Service.Constants.Common.myLocationSet);
        },
        getMyLocationFlag: function() {
            return kony.store.getItem("setMyLocationFlag");
        }
    }
};