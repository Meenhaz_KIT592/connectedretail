/**
 * PUBLIC
 * This is the view for the ForgotPassword form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ForgotPasswordView = (function() {
    /**
     * PUBLIC
     * Open the More form
     */
    function show() {
        frmForgotPwd.txtEmail.text = "";
        frmForgotPwd.btnClearEmail.setVisibility(false);
        MVCApp.Toolbox.common.setTransition(frmForgotPwd, 3);
        frmForgotPwd.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmForgotPwd.btnHeaderLeft.onClick = function() {
            MVCApp.Toolbox.common.setTransition(frmSignIn, 2);
            frmSignIn.show();
            MVCApp.tabbar.bindEvents(frmSignIn);
            MVCApp.tabbar.bindIcons(frmSignIn, "frmMore");
        };
        frmForgotPwd.btnCreateAccount.onClick = MVCApp.getCreateAccountController().load;
        frmForgotPwd.btnSubmit.onClick = onClickSubmit;
        frmForgotPwd.txtEmail.onTextChange = function() {
            var text = frmForgotPwd.txtEmail.text;
            if (text.length > 0) {
                frmForgotPwd.btnClearEmail.setVisibility(true);
            } else {
                frmForgotPwd.btnClearEmail.setVisibility(false);
            }
        };
        frmForgotPwd.btnClearEmail.onClick = function() {
            frmForgotPwd.btnClearEmail.setVisibility(false);
            frmForgotPwd.txtEmail.text = "";
        };
        frmForgotPwd.postShow = function() {
            MVCApp.Toolbox.common.setBrightness("frmForgotPwd");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Forgot Password: Michaels Mobile App";
            lTealiumTagObj.page_name = "Forgot Password: Michaels Mobile App";
            lTealiumTagObj.page_type = "Password";
            lTealiumTagObj.page_category_name = "Password Reset";
            lTealiumTagObj.page_category = "Password Reset";
            TealiumManager.trackView("Forgot Password screen", lTealiumTagObj);
        };
    }

    function onClickSubmit() {
        if (validateFields()) {
            var email = frmForgotPwd.txtEmail.text;
            MVCApp.getForgotPasswordController().submitEmail(email);
        }
    }

    function validateFields() {
        var email = frmForgotPwd.txtEmail.text;
        var regularEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (email === null || email.trim() === "") {
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.enterEmail"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
            return false;
        }
        if (!regularEx.test(email.trim())) {
            MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.enterValidEmail"), "");
            return false;
        }
        return true;
    }

    function updateScreen(results) {
        show();
    }

    function submitEmailResponse(results) {
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.forgotPassword.emailReceive"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.gotItTitle"));
        isFromShoppingList = false;
        MVCApp.getSignInController().load();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        submitEmailResponse: submitEmailResponse
    };
});