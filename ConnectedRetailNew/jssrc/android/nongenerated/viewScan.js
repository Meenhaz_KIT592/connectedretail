/**
 * PUBLIC
 * This is the view for the Scan form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ScanView = (function() {
    /**
     * PUBLIC
     * Open the Scan form
     */
    function show() {
        //Creates an object of class 'barcodescanner'
        MVCApp.Toolbox.common.setTransition(frmScan, 3);
        frmScan.show();
        //frmScan.cusBarCodeWid.startCamera();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function onSuccessfulScan(results) {
        MVCApp.sendMetricReport(frmScan, [{
            "scan": "submit"
        }]);
        var captureType = "BarcodeCapture";
        MVCApp.getProductsListController().setEntryType("barcodeScan");
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        if (results.startsWith("appservices")) {
            cust360Obj.linkType = "deepLink";
            cust360Obj.link = results;
            captureType = "QRcodeCapture";
        } else if (results.startsWith("http")) {
            cust360Obj.linkType = "webLink";
            cust360Obj.link = results;
            captureType = "QRcodeCapture";
            MVCApp.getProductsListController().setEntryType("QRCodeSearch");
        }
        MVCApp.Customer360.sendInteractionEvent(captureType, cust360Obj);
        if (checkAndHandleLink(results)) {
            return;
        }
        if (!(results.startsWith("appservices") || results.startsWith("http"))) {
            /*if(results.charAt(0)!="0"){
             results = "0"+results; 
            }*/
        }
        kony.store.setItem("searchedProdId", results);
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            MVCApp.getProductsListWebController().load("search?q=" + results);
        } else {
            if (!(results.startsWith("appservices") || results.startsWith("http"))) {
                if (results.charAt(0) != "0") {
                    results = "0" + results;
                }
            }
            MVCApp.getProductsListController().loadForPDP(results);
        }
    }

    function barcodeCapCallback(barcodeData) {
        kony.print("****************** Inside barcodeCapCallback -> barcode Data: " + barcodeData);
        if (barcodeData == "Back" || barcodeData == "Permission") {
            kony.print("Clicked back");
            var frm = kony.application.getCurrentForm();
            frm.show();
        } else onSuccessfulScan(barcodeData);
    }

    function launchBarcodeCapture() {
        barcode.captureBarcode(barcodeCapCallback);
    }

    function startCameraForWidget() {
        frmScan.cusBarCodeWid.startCamera();
        MVCApp.sendMetricReport(frmScan, [{
            "scan": "init"
        }]);
        MVCApp.Toolbox.common.destroyStoreLoc();
        var lTealiumTagObj = gblTealiumTagObj;
        lTealiumTagObj.page_id = "Scan: Michaels Mobile App";
        lTealiumTagObj.page_name = "Scan: Michaels Mobile App";
        lTealiumTagObj.page_type = "Scan";
        lTealiumTagObj.page_category_name = "Scan";
        lTealiumTagObj.page_category = "Scan";
        TealiumManager.trackView("Scan Screen", lTealiumTagObj);
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        MVCApp.Customer360.sendInteractionEvent("BarcodeLaunch", cust360Obj);
        TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchInitiation, {
            conversion_category: "App Search",
            conversion_id: "BarcodeSearch",
            conversion_action: 1
        });
    }

    function goToPreviousForm() {
        var frmObj = kony.application.getPreviousForm();
        MVCApp.Toolbox.common.setTransition(frmObj, 2);
        frmObj.show();
        frmScan.cusBarCodeWid.endCamera();
    }

    function bindEvents() {
        frmScan.postShow = startCameraForWidget;
        frmScan.cusBarCodeWid.scanResultCallback = onSuccessfulScan;
        frmScan.btnBack.onClick = goToPreviousForm;
    }

    function updateScreen(results) {
        show();
        // MVCAppToolBox_dismissLoadingIndicator();
    }

    function checkAndHandleLink(barcodeContent) {
        if (barcodeContent.startsWith("appservices") || checkMobileWebLink(barcodeContent)) {
            if (barcodeContent.startsWith("http")) {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.link = barcodeContent;
                cust360Obj.category = "";
                cust360Obj.searchResult = "success";
                MVCApp.Customer360.sendInteractionEvent("QRcodeResult", cust360Obj);
            }
            MVCApp.getProductsListController().setQRlink(barcodeContent);
            kony.application.openURL(barcodeContent.replace("MIK_MobileCamera", "MIK_ecMobileApp"));
            return true;
        }
        return false;
    }

    function checkMobileWebLink(barcodeContent) {
        var validUrl = false;
        if (barcodeContent.startsWith("http")) {
            var mobileWebUrl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrl.applicable");
            var mobileWebUrlArray = mobileWebUrl ? mobileWebUrl.split(",") : [];
            var mobileWebUrlArrayLen = mobileWebUrlArray.length;
            if (mobileWebUrlArray && mobileWebUrlArrayLen > 0) {
                for (var count = 0; count < mobileWebUrlArrayLen; count++) {
                    if (barcodeContent.indexOf(mobileWebUrlArray[count]) != -1) {
                        validUrl = true;
                        break;
                    }
                }
            }
        }
        return validUrl;
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        startCameraForWidget: startCameraForWidget
    };
});