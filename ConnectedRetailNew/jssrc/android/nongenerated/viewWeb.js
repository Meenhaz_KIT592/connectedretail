/**
 * PUBLIC
 * This is the view for the ProductsList form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var gblUrl = "";
var backFlag = false;
var pdpUrl = "";
var fromForm = "";
var infinish = false;
MVCApp.webView = (function() {
    /**
     * PUBLIC
     * Open the ProductsList form
     */
    function bindEvents() {
        MVCApp.tabbar.bindIcons(frmWebView);
        MVCApp.tabbar.bindEvents(frmWebView);
        MVCApp.searchBar.bindEvents(frmWebView, "frmMore");
        frmWebView.postShow = postShowCallback;
        frmWebView.preShow = preShowCallback;
        frmWebView.btnBack.onClick = goPrevious;
        frmWebView.onDeviceBack = goPrevious;
        frmWebView.onHide = function() {
            //MVCApp.Toolbox.common.cancelTimer();
            //  var htmlStringWelcome= "<html></html>";
            // frmWebView.brwrView.htmlString=htmlStringWelcome;
        };
        frmWebView.brwrView.onSuccess = function() {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            frmWebView.brwrView.isVisible = true;
            var availabilitybox = frmWebView.brwrView.evaluateJavaScript("$('#availability-box').html();");
            var locname = frmWebView.brwrView.evaluateJavaScript("document.location.pathname;");
            if (locname && locname.indexOf(".html") !== -1) {
                if (availabilitybox == null) {
                    var reload = frmWebView.brwrView.evaluateJavaScript("app.product.reloadAvailabilityBoxPdp();");
                    kony.print("::reload::" + reload);
                }
            }
        };
        frmWebView.brwrView.onFailure = function(eventObj, error) {
            error = error || {};
            kony.print(" in error --> frmWebView.brwrView.onFailure" + JSON.stringify(error) + " WidgetID:" + eventObj.id);
            kony.print("#availability-box::" + frmWebView.brwrView.evaluateJavaScript("$('#availability-box').html();"));
            frmWebView.brwrView.isVisible = true;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            /* if(error!==undefined  && error !== null ){
                if(error.errorCode!==undefined && error.errorCode !== null ){
                  var errorResponseCode= error.errorCode || "";
                  var availabilitybox = frmWebView.brwrView.evaluateJavaScript("$('#availability-box').html();");
                  var locname= frmWebView.brwrView.evaluateJavaScript("document.location.pathname;");
                  if (locname.indexOf(".html") !== -1){            
                      if(availabilitybox == null){ 
                       var reload = frmWebView.brwrView.evaluateJavaScript("app.product.reloadAvailabilityBoxPdp();");    
                        kony.print("errorResponseCode"+errorResponseCode +"::reload::"+reload);
                  	  }
                  }
                }
              }*/
            var availabilitybox = frmWebView.brwrView.evaluateJavaScript("$('#availability-box').html();");
            var locname = frmWebView.brwrView.evaluateJavaScript("document.location.pathname;");
            if (locname && locname.indexOf(".html") !== -1) {
                if (availabilitybox == null) {
                    var reload = frmWebView.brwrView.evaluateJavaScript("app.product.reloadAvailabilityBoxPdp();");
                    kony.print("::reload::" + reload);
                }
            }
        };
        frmWebView.brwrView.onPageStarted = function(eventobject, params) {
            //  kony.print("The onPageStarted eventobject is: " + eventobject + "@@@@ params are: " + params["queryParams"]);
            MVCApp.Toolbox.common.showLoadingIndicator("");
            frmWebView.brwrView.enableJsInterface = true;
            frmWebView.brwrView.enableParentScrollingWhenReachToBoundaries = false;
            frmWebView.brwrView.clearCanvasBeforeLoading = true;
            // frmWebView.brwrView.isVisible=true;
            var URL = params.originalURL || "";
            kony.print("In handle Request---onPageStarted-------->" + URL);
            if (URL !== "") {
                if (URL.indexOf("search?q") !== -1) {
                    pdpUrl = URL;
                    frmWebView.flxBack.isVisible = false;
                } else {
                    if (URL.indexOf(".html") !== -1) {
                        pdpUrl = URL;
                        frmWebView.flxBack.isVisible = true;
                    }
                }
            }
            if (URL !== "") {
                if (URL.indexOf("cart?dwcont") !== -1) {
                    // MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                        MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    } else {
                        /*    var result =  frmWebView.brwrView.evaluateJavaScriptAsync("getBasketCountForApp()",function (result, error){
                      try{
                        var count= result||"";
                        count = count.replace(/"/g, "");
                        count = count.replace(/\\/g, "");
                        count=count.replace("/","");
                        count=count.trim();
                        if(count!=="null" && count!==""){
                           MVCApp.Toolbox.common.setGuestBasketCount(parseInt(count));
	                       MVCApp.Toolbox.common.getGuestBasketDetails();
                        }
                      
                      }catch(err){
                        kony.print(err)
                      }
                      
                    }); 
                      */
                    }
                } else {
                    if (URL.indexOf("COSummary-Submit") !== -1) {
                        kony.print("Are we even coming here? COSummary-Submit");
                        /* if( ! MVCApp.Toolbox.common.checkIfGuestUser()){ 
                                      MVCApp.Toolbox.common.createBasketforUser(function() {

                                 });
                                   }
                         */
                    }
                }
            }
            //MVCApp.Toolbox.common.showLoadingIndicator("");
            if (firstTime === true) {
                firstTime = false;
                kony.print("firstTime------>>>>");
                //  MVCApp.Toolbox.common.showLoadingIndicator("");
            }
        };
        frmWebView.brwrView.onPageFinished = function(eventobject, params) {
            var URL = params.originalURL || "";
            kony.print("In handle Request---onPageFinished-------->" + URL);
            if (URL == "about:blank" && backFlag) {
                backFlag = false;
                goPrevious();
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            /*  if (MVCApp.Toolbox.common.checkIfGuestUser()) {  
          	var res2 = frmWebView.brwrView.evaluateJavaScriptAsync("getBasketCountForApp()", function(result, error) {
                                try {
                                    kony.print("getBasketCountForApp" + result);
                                    var count = result || "";
                                    count = count.replace(/"/g, "");
                                    count = count.replace(/\\/g, "");
                                    count = count.replace("/", "");
                                    count = count.trim();
                                    //if(result){
                                    if (count!=="null" && count !== "") {
                                        MVCApp.Toolbox.common.setGuestBasketCount(parseInt(count));
                                        MVCApp.Toolbox.common.getGuestBasketDetails();
                                    }
                                    //}
                                } catch (err) {
                                    kony.print(err)
                                }
                            });}
      
      */
        };
        frmWebView.brwrView.onProgressChanged = function(eventobject, params) {
            if (frmWebView.brwrView && frmWebView.brwrView.canGoBack()) MVCApp.Toolbox.common.dismissLoadingIndicator("");
        };
        frmWebView.brwrView.handleRequest = function(browserWidget, params) {
            var URL = params.originalURL || "";
            kony.print("In handle Request----------->" + URL);
            if (URL !== "") {
                kony.print("In handle Request external or not----------->" + URL);
                URL = MVCApp.Toolbox.common.formatURLBeforeLoading(URL, MVCApp.Toolbox.common.findIfWebviewUrlisExternal(URL));
                kony.print("formatURLBeforeLoading---------->" + URL);
                if (MVCApp.Toolbox.common.findIfWebviewUrlisExternal(URL)) {
                    kony.print("In handle Request external ----------->" + URL);
                    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18.common.webview.redirectMessage"), "", constants.ALERT_TYPE_CONFIRMATION, function(response) {
                        if (response) {
                            kony.application.openURL(URL);
                        } else {
                            kony.print("do nothing");
                        }
                    }, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel"));
                    return true;
                } else if (MVCApp.Toolbox.common.findIfWebviewUrlisDeeplink(URL)) {
                    fromBrowserViews = true;
                    entryBrowserFormName = "frmCartView";
                    kony.application.openURL(URL);
                    return true;
                }
                if (URL.indexOf(".pdf") !== -1) {
                    kony.print("in Open Media" + URL);
                    kony.application.openURL("http://docs.google.com/gview?embedded=true&url=" + URL);
                    return false;
                }
                if (URL.indexOf("frmAddtoCart") !== -1) {
                    if (MVCApp.Toolbox.common.checkIfGuestUser()) {
                        var res4 = frmWebView.brwrView.evaluateJavaScriptAsync("getSessionBasketDeprecated()", function(result, error) {
                            try {
                                kony.print("getSessionBasket" + result);
                                // var obj = JSON.parse(result||{});
                                var obj = {};
                                try {
                                    obj = JSON.parse(result || {});
                                } catch (e) {}
                                kony.print("getSessionBasket" + obj.basket_id);
                                kony.print("getSessionBasket" + obj.customer_id);
                                kony.print("getSessionBasket" + obj.product_count);
                                if (obj.product_count == undefined || obj.product_count == "") {
                                    // MVCApp.Toolbox.common.setGuestBasketCount(0);
                                    //MVCApp.Toolbox.common.getGuestBasketDetails();
                                } else {
                                    MVCApp.Toolbox.common.setBasket(obj.basket_id);
                                    MVCApp.Toolbox.common.setCustomerId(obj.customer_id);
                                    MVCApp.Toolbox.common.setJWTToken(obj.access_token);
                                    MVCApp.Toolbox.common.setGuestBasketCount(obj.product_count);
                                    //MVCApp.Toolbox.common.setGuestBasketCount(parseInt(obj.product_count));
                                    MVCApp.Toolbox.common.getGuestBasketDetails();
                                }
                                fromCoupons = true;
                                isFromShoppingList = false;
                                gblUrl = URL;
                                fromForm = "frmWebView";
                            } catch (err) {
                                kony.print(err)
                            }
                        });
                    }
                    if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                        MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    } else {
                        var result1 = frmWebView.brwrView.evaluateJavaScriptAsync("getBasketCountForApp()", function(result, error) {
                            try {
                                kony.print("getBasketCountForApp" + result);
                                var count = result || "";
                                count = count.replace(/"/g, "");
                                count = count.replace(/\\/g, "");
                                count = count.replace("/", "");
                                count = count.trim();
                                //if(result){
                                if (count !== "null" && count !== "") {
                                    MVCApp.Toolbox.common.setGuestBasketCount(parseInt(count));
                                    MVCApp.Toolbox.common.getGuestBasketDetails();
                                }
                            } catch (err) {
                                kony.print(err)
                            }
                        });
                    }
                    return true;
                }
                if (URL.indexOf("com.michaels.ConnectedRetail?pid") !== -1) {
                    return true;
                }
                if (URL.indexOf("frmShoppingList&pid") !== -1) {
                    return true;
                }
                if (URL.indexOf("frmSignIn") !== -1) {
                    if (MVCApp.Toolbox.common.checkIfGuestUser()) {
                        var res4 = frmWebView.brwrView.evaluateJavaScriptAsync("getSessionBasketDeprecated()", function(result, error) {
                            try {
                                kony.print("getSessionBasket" + result);
                                //var obj = JSON.parse(result||{});
                                var obj = {};
                                try {
                                    obj = JSON.parse(result || {});
                                } catch (e) {}
                                kony.print("getSessionBasket" + obj.basket_id);
                                kony.print("getSessionBasket" + obj.customer_id);
                                kony.print("getSessionBasket" + obj.product_count);
                                if (obj.product_count == undefined || obj.product_count == "") {
                                    //MVCApp.Toolbox.common.setGuestBasketCount(0);
                                } else {
                                    MVCApp.Toolbox.common.setBasket(obj.basket_id);
                                    MVCApp.Toolbox.common.setCustomerId(obj.customer_id);
                                    MVCApp.Toolbox.common.setJWTToken(obj.access_token);
                                    MVCApp.Toolbox.common.setGuestBasketCount(obj.product_count);
                                }
                                fromCoupons = false;
                                isFromShoppingList = false;
                                gblUrl = URL;
                                MVCApp.getSignInController().load();
                                fromForm = "frmWebView";
                                fromWebView = true;
                            } catch (err) {
                                kony.print(err)
                            }
                        });
                        favoritesFlag = true;
                        return true;
                    } else {
                        if (URL.indexOf("pid") !== -1) {
                            var urlArr = URL.split(/&/);
                            var pid = urlArr[1] ? urlArr[1].split("=") : null;
                            if (pid) {
                                params.originalURL = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + pid[1] + ".html?appsource=mobileapp&action=favorite";
                                //MVCApp.getProductsListWebController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels")+pid[1]+".html?appsource=mobileapp&action=favorite");
                            }
                        }
                        favoritesFlag = true;
                        fromForm = "frmWebView";
                        return true;
                    }
                }
                if (URL.indexOf("cart?appsource=mobileapp") !== -1) {
                    //gblUrl = URL;
                    MVCApp.getCartController().loadWebView(URL, true);
                    return true;
                }
                if (URL.indexOf("frmHome") !== -1) {
                    // MVCApp.getHomeController().load();
                    //MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                        MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                        MVCApp.getHomeController().load();
                    } else {
                        var res = frmWebView.brwrView.evaluateJavaScriptAsync("getBasketCountForApp()", function(result, error) {
                            try {
                                MVCApp.getHomeController().load();
                                kony.print("getBasketCountForApp" + result);
                                var count = result || "";
                                count = count.replace(/"/g, "");
                                count = count.replace(/\\/g, "");
                                count = count.replace("/", "");
                                count = count.trim();
                                //if(result){
                                if (count !== "null" && count !== "") {
                                    MVCApp.Toolbox.common.setGuestBasketCount(parseInt(count));
                                    MVCApp.Toolbox.common.getGuestBasketDetails();
                                }
                                //}
                            } catch (err) {
                                kony.print(err)
                            }
                        });
                    }
                    return true;
                }
                if (URL.indexOf("appservices://com.michaels.ConnectedRetail?formToOpen=frmCoupons") !== -1) {
                    voiceSearchFlag = false;
                    couponsEntryFromVoiceSearch = false;
                    var storeModeEnabled = false;
                    try {
                        if (writesettings == null || writesettings == undefined) {
                            writesettings = new runtimepermissions.permissions();
                            writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow"));
                            writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
                            writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
                            writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
                            writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                        }
                    } catch (e) {
                        if (e) {
                            kony.print("Exception while setting run time permissions" + e);
                        }
                    }
                    writesettings.requestPermission("android.write_settings.permission");
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                        storeModeEnabled = true;
                    }
                    var region = MVCApp.Toolbox.common.getRegion();
                    var reportData = {
                        "storeModeEnabled": storeModeEnabled,
                        "region": region
                    };
                    MVCApp.sendMetricReport(frmHome, [{
                        "CouponsCLick": JSON.stringify(reportData)
                    }]);
                    MVCApp.getHomeController().loadCoupons();
                    return true;
                }
                if (URL.indexOf("formToOpen=frmPDP&action=storechange") !== -1) {
                    var result = frmWebView.brwrView.evaluateJavaScriptAsync("getStoreIdForApp()", function(result, error) {
                        kony.print(">>Result:" + JSON.stringify(result) + "<<");
                        var store = result || "";
                        store = store.replace(/"/g, "");
                        store = store.replace(/\\/g, "");
                        store = store.replace("/", "");
                        store = store.trim();
                        //             var resArray = res1?res1.split(","):[];
                        //             var store="";
                        //             for(var i=0;i<resArray.length;i++){
                        //               var tmp = resArray[i];
                        //               if(tmp.indexOf("clientkey")!==-1){
                        //                 store=tmp.split(":")[1];
                        //                 store = store.replace(/"/g, "");
                        //                 store = store.replace(/\\/g, "");
                        //                 store=store.replace("/","");
                        //                 store=store.trim();
                        //               }
                        //             }
                        kony.print(">>Result:" + store + "<<");
                        if (store.length > 0) {
                            var reqParams = {};
                            if (gblInStoreModeDetails.storeID !== store) {
                                storeChangeInStoreMode = true;
                            }
                            reqParams.storeID = store;
                            // reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
                            reqParams.country = MVCApp.Service.Constants.Common.country;
                            MakeServiceCall(MVCApp.serviceType.LocatorService, MVCApp.serviceType.fetchINStoreDetailsByID, reqParams, function(results) {
                                if (results.opstatus === 0 || results.opstatus === "0") {
                                    var collection = results.collection || [];
                                    var poiColl = collection[0] || "";
                                    var poi = poiColl.poi || "";
                                    var poiString = "";
                                    if (poi !== "") {
                                        poiString = JSON.stringify(poi);
                                        kony.store.setItem("myLocationDetails", poiString);
                                    }
                                    //kony.print("Poi data" + JSON.parse(poi));
                                }
                            }, function(error) {
                                kony.print("Response is " + JSON.stringify(error));
                            });
                        }
                    });
                    return true;
                }
            }
            if (URL.indexOf("appservices://com.michaels.ConnectedRetail?formToOpen=frmCreateAccount") !== -1) {
                if (MVCApp.Toolbox.common.checkIfGuestUser()) {
                    MVCApp.getCreateAccountController().load();
                }
                return true;
            }
            return false;
        };
        frmWebView.brwrView.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
        frmWebView.brwrView.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
    }

    function preShowCallback() {
        var searchString = kony.store.getItem("searchedProdId");
        frmWebView.flxHeaderWrap.textSearch.text = searchString;
        frmWebView.flxSearchOverlay.setVisibility(false);
        var entry = MVCApp.getProductsListController().getEntryType();
        if (entry === "taxonomy") {
            frmWebView.flxBack.isVisible = true;
        } else {
            frmWebView.flxBack.isVisible = false;
        }
        if (voiceSearchFlag) {
            //MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("VOICE SEARCH FORM::" + voiceSearchForm.id);
            voiceSearchForm.flxVoiceSearch.setVisibility(false);
            voiceSearchFlag = false;
            //return;
        }
        kony.print("preShowCallback------->>");
    }

    function postShowCallback() {
        kony.print("postShowCallback------->>");
        frmWebView.flxHeaderWrap.flxSearchContents.isVisible = true;
        MVCApp.Toolbox.common.startTimeToRefreshToken();
        //     MVCApp.Toolbox.common.setBrightness("frmWebView");
        //     MVCApp.Toolbox.common.destroyStoreLoc();
        //var cartValue=  MVCApp.Toolbox.common.getCartItemValue();
        //frmWebView.flxHeaderWrap.lblItemsCount.text=cartValue;
        var lPreviousForm = kony.application.getPreviousForm();
        var lPrevFormName = "";
        if (lPreviousForm) {
            lPrevFormName = lPreviousForm.id;
        }
    }
    var headerconfig = {
        "Authorization": "Basic c3RvcmVmcm9udDptaWsxMjM="
    };

    function show() {
        MVCApp.Toolbox.common.showLoadingIndicator("");
        frmWebView.show();
    }

    function loadPdpView(data) {
        if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
            MVCApp.Toolbox.common.showLoadingIndicator();
            MVCApp.Toolbox.common.verifyExpiryFlowOfAuthUser(function() {
                showPdpView(data);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            })
        } else showPdpView(data);
    }

    function showPdpView(data) {
        //MVCApp.Toolbox.common.showLoadingIndicator("");
        MVCApp.Toolbox.common.showLoadingIndicator("");
        var uriRequest = MVCApp.Toolbox.common.getSessionURI();
        var jwtToken = MVCApp.Toolbox.common.getJWTToken();
        MVCApp.Toolbox.common.getGuestBasketDetails();
        frmWebView.flxMainContainer.remove(frmWebView.brwrView);
        var brwrView = new kony.ui.Browser({
            "detectTelNumber": true,
            "enableZoom": false,
            "id": "brwrView",
            "isVisible": true,
            "left": "0%",
            "top": "0dp",
            "width": "100%",
            "height": "87%",
            "htmlString": "&nbsp;",
            "zIndex": 1,
            "info": "sdfds",
        }, {}, {
            "iphone": {
                "psp": {
                    "bounces": true,
                    "browserType": constants.BROWSER_TYPE_WKWEBVIEW
                }
            }
        });
        //     //#ifdef iphone
        //     //brwrView.bounces = false;
        //     //brwrView.browserType = 1;
        //     //#endif
        brwrView.enableJsInterface = true;
        frmWebView.flxMainContainer.add(brwrView);
        frmWebView.brwrView.evaluateJavaScript("document.open();document.close();");
        bindEvents();
        var storeId = MVCApp.Toolbox.common.getStoreID();
        var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
        var strCookie = "";
        gblUrl = "";
        favoritesFlag = false;
        strCookie = "appsource=mobileapp;";
        var strId = "";
        if (favStoreId !== "") {
            // appRequest= "?stid="+favStoreId+"&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
            //headerconfig["set-cookie"]="storeId="+favStoreId+"";
            strCookie = strCookie + "storeId=" + favStoreId + ";";
            // headerconfig["set-cookie"]="storeId="+favStoreId+"";
        } else {
            // headerconfig["set-cookie"]="storeId="+storeId+"";
            strCookie = strCookie + "storeId=" + storeId + ";";
            // strCookie="storeId="+favStoreId+";";
        }
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            // headerconfig["set-cookie"]="storeVisit="+storeId+"";
            strCookie = strCookie + "storeVisit=" + storeId + ";";
        }
        headerconfig["set-cookie"] = strCookie;
        if (favStoreId !== "" || (storeId !== null && storeId !== "")) {
            headerconfig["Cookie"] = strCookie;
        }
        if (kony.application.getPreviousForm() && kony.application.getPreviousForm().id === "frmSignIn") {
            if (gblUrl.indexOf("pid") !== -1) {
                // var urlArr = gblUrl.split(/&/);
                //data = data+"&"+urlArr[1]+"&action=favorite";
            } else {
                data = data + "&action=checkout";
            }
        }
        kony.print(" URL before loading web view " + data);
        var browserContent = {
            "URL": data,
            "requestMethod": constants.BROWSER_REQUEST_METHOD_GET,
            "headers": headerconfig
        };
        frmWebView.brwrView.requestURLConfig = browserContent;
        show();
        //frmWebView.brwrView.isVisible = true;
        /* var request = new kony.net.HttpRequest();
         request.open(constants.HTTP_METHOD_POST, uriRequest, false);
         request.setRequestHeader("Content-Type","application/json");
         request.setRequestHeader("Authorization", jwtToken);
         request.onReadyStateChange=function(){


           if(kony.application.getPreviousForm() && kony.application.getPreviousForm().id === "frmSignIn"){
             if(gblUrl.indexOf("pid") !== -1){
               // var urlArr = gblUrl.split(/&/);
               //data = data+"&"+urlArr[1]+"&action=favorite";
             } else {
               data = data+"&action=checkout" ; 
             }

           }
           kony.print(" URL before loading web view "+ data);

           var browserContent = {
             "URL": data,
             "requestMethod": constants.BROWSER_REQUEST_METHOD_GET,
             "headers" : headerconfig
           };


           frmWebView.brwrView.requestURLConfig = browserContent;
           show();
         };
         request.send();*/
    }
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
        } else {
            MVCApp.Toolbox.common.dispFooter();
        }
        if (checkScroll == 0) {
            MVCApp.Toolbox.common.hideFooter();
        }
    }

    function goPrevious() {
        //var entry = MVCApp.getProductsListController().getEntryType();
        frmWebView.brwrView.evaluateJavaScript("window.stop();");
        var prevForm = kony.application.getPreviousForm() || "";
        var prevFormId = prevForm.id || "";
        if (frmWebView.brwrView.canGoBack()) {
            backFlag = true;
            if (prevFormId == "frmCartView") {
                if (MVCApp.getProductsListController().getEntryType() === "taxonomy") {
                    taxanomyForm.show();
                    taxanomyForm = {};
                }
            } else {
                frmWebView.brwrView.goBack();
            }
        } else {
            //MVCApp.Toolbox.common.createBasketforUser();
            if (fromBrowserViews) {
                if (entryFormObjectToBrowser) {
                    prevFormId = entryFormObjectToBrowser.id || (prevForm.id || "");
                } else {
                    prevFormId = prevForm.id || "";
                }
            }
            kony.print("The previousForm is " + prevFormId);
            if (prevFormId == "frmShoppingList") {
                MVCApp.getShoppingListController().getProductListItems();
            } else if (prevFormId == "frmCartView") {
                if (!cartFlag) {
                    var url = "";
                    var searchStr = kony.store.getItem("searchedProdId") || "";
                    if (MVCApp.getProductsListController().getEntryType() !== "taxonomy") {
                        url = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "search?q=" + searchStr;
                        frmWebView.flxBack.isVisible = false;
                    } else {
                        url = taxanomyurl;
                        frmWebView.flxBack.isVisible = true;
                    }
                    var storeId = MVCApp.Toolbox.common.getStoreID();
                    var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
                    var appRequest = "?stid=" + storeId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                        if (storeChangeInStoreMode) {
                            appRequest = "?stid=" + favStoreId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                        }
                        appRequest = appRequest + "&storevisit=" + storeId;
                    }
                    if (url.indexOf("?") !== -1) {
                        appRequest = appRequest.replace(/\?/g, "&");
                    }
                    url = url + appRequest;
                    var browserContent = {
                        "URL": url,
                        "requestMethod": constants.BROWSER_REQUEST_METHOD_GET,
                        "headers": headerconfig
                    };
                    frmWebView.brwrView.requestURLConfig = browserContent;
                    show();
                    // frmWebView.btnBack.onClick = function(){};
                    //frmWebView.onDeviceBack = function(){};
                } else {
                    MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
                    cartFlag = false;
                }
            } else if (prevFormId == "frmSignIn") {
                MVCApp.getHomeController().load();
                MVCApp.Toolbox.common.destoryFrmWeb("frmWebView");
            } else if (prevFormId == "frmScan") {
                MVCApp.getHomeController().load();
                MVCApp.Toolbox.common.destoryFrmWeb("frmWebView");
            } else if (fromBrowserViews) {
                if (entryFormObjectToBrowser) {
                    entryFormObjectToBrowser.show();
                } else {
                    kony.print("prevFormId  " + prevFormId);
                    kony.application.getPreviousForm().show();
                    MVCApp.Toolbox.common.destoryFrmWeb("frmWebView");
                }
                fromBrowserViews = false;
            } else {
                kony.print("prevFormId  " + prevFormId);
                kony.application.getPreviousForm().show();
                MVCApp.Toolbox.common.destoryFrmWeb("frmWebView");
            }
        }
    }
    return {
        bindEvents: bindEvents,
        loadPdpView: loadPdpView
    };
});