var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.RewardsProfileInterests = (function(json) {
    var typeOf = "RewardsProfileInterests";
    var _data = json || {};

    function getRewardsProfileInterestsData() {
        try {
            var rewardsProfile = _data.results;
            if (rewardsProfile != "" && rewardsProfile != null && rewardsProfile != undefined) {
                rewardsProfile = JSON.parse(rewardsProfile);
                kony.print("Rewards Profile Interests:" + rewardsProfile);
                rewardsProfile = rewardsProfile.c_RewardsProfile;
                var interests = [];
                if (rewardsProfile != "" && rewardsProfile != null) {
                    var rewardsInterests = rewardsProfile.skills;
                    if (rewardsInterests != "" && rewardsInterests != null) {
                        var questions = [];
                        var validvalues = [];
                        questions = rewardsInterests.questions;
                        if (questions != "" && questions != null) {
                            interests.questions = questions;
                            validvalues = rewardsInterests.validvalues;
                            for (var i = 0; i < validvalues.length; i++) {
                                var temp1 = [];
                                temp1.value = validvalues[i];
                                temp1.select = {
                                    text: "u",
                                    isVisible: false
                                };
                                interests.push(temp1);
                            }
                        }
                    }
                }
                return interests;
            }
        } catch (e) {
            kony.print("Exception in getRewardsProfileInterestsData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Rewards Profile Parse Exception in Rewards Profile Interests Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    return {
        typeOf: typeOf,
        getRewardsProfileInterestsData: getRewardsProfileInterestsData
    };
});