			/**
			 * PUBLIC
			 * This is the view for the Guide form.
			 * All actions that impact the UI are implemented here.
			 * _variable or _functions indicate elements which are not publicly exposed
			 */
			var MVCApp = MVCApp || {};
			var guideCounter = 0;
			var clickedDeniedLocation = false;
			var dummyAndroidSubscription = false;
			var dummyTokenRegistration = false;
			MVCApp.GuideView = (function() {
			    var onClickEnablePushNotificationCounter = 0;

			    function show() {
			        kony.store.setItem("pushuserId", "");
			        frmGuide.show();
			    }

			    function bindEvents() {
			        frmGuide.postShow = function() {
			            var lTealiumTagObj = gblTealiumTagObj;
			            lTealiumTagObj.page_id = "Guide: Michaels Mobile App";
			            lTealiumTagObj.page_name = "Guide: Michaels Mobile App";
			            lTealiumTagObj.page_type = "Guide";
			            lTealiumTagObj.page_category_name = "Guide";
			            lTealiumTagObj.page_category = "Guide";
			            TealiumManager.trackView("Guide Screen", lTealiumTagObj);
			            if (guideCounter == 0) {
			                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
			                MVCApp.Customer360.sendInteractionEvent("Guide-FirstScreen-WhatsNew", cust360Obj);
			                guideCounter++;
			            }
			        };
			        frmGuide.onDeviceBack = function() {};
			        frmGuide.flexGuideControl1.onClick = function() {
			            frmGuide.flexGuide1.setEnabled(false);
			            if (gblIsLocationPermissionGranted || (firstLaunchFromPrevKonyVersionFlag == "false")) {
			                frmGuide.flexGuide2.setEnabled(false);
			                guideFormAnimation(frmGuide.flexGuide3);
			                frmGuide.flexGuide3.forceLayout();
			            } else {
			                guideFormAnimation(frmGuide.flexGuide2);
			                frmGuide.flexGuide2.forceLayout();
			            }
			        };
			        frmGuide.flexGuideControl21.onClick = function() {
			            if (initLaunchFlag || !firstLaunchFromPrevKonyVersionFlag) {
			                if (gblIsLocationPermissionDenied) {
			                    redirectToSettings.locationSettings();
			                    frmGuide.flexGuide2.setEnabled(false);
			                    guideFormAnimation(frmGuide.flexGuide3);
			                    frmGuide.flexGuide3.forceLayout();
			                } else {
			                    getCurrentLocationFromGuide();
			                }
			            }
			        };
			        frmGuide.flexGuideControl22.onClick = function() {
			            frmGuide.flexGuide2.setEnabled(false);
			            frmGuide.flexGuideControl22.onClick = function() {
			                kony.print("test");
			            };
			            guideFormAnimation(frmGuide.flexGuide3);
			            frmGuide.flexGuide3.forceLayout();
			            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
			            cust360Obj.permission = "denied";
			            clickedDeniedLocation = true;
			            MVCApp.Customer360.sendInteractionEvent("WhileUsingAppLocationGuide2", cust360Obj);
			        };
			        //ToDo - To Be Used in Release-3 on inclusion of PushNotifications.
			        frmGuide.flexGuideControl31.onClick = function() {
			            gblAndroidPushSettingControlFlag = true;
			            frmGuide.lblGuideControl31.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
			            frmGuide.lblGuideChevron31.isVisible = true;
			            frmGuide.lblGuideChevron31.skin = "sknLblIconRedGuideChevron";
			            var screenWidth = kony.os.deviceInfo().screenWidth;
			            kony.store.setItem("screenWidth", screenWidth);
			            kony.print("in GUIDE INITLAUNCHFLAG---------->" + initLaunchFlag);
			            frmSettings.pushSwitch.src = "off.png";
			            kony.store.setItem("pushSwitch", false);
			            MVCApp.PushNotification.common.registerPush();
			            androidPushNavigateToNextFormCallback();
			        }
			        frmGuide.flexGuideControl32.onClick = function() {
			            dummyAndroidSubscription = true;
			            var regid = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.AndroidGCM.dummySubscriptionToken");
			            MVCApp.PushNotification.common.subscribeMFPushMessaging(regid);
			            frmSettings.pushSwitch.src = "off.png";
			            kony.store.setItem("pushSwitch", false);
			            kony.store.setItem("gblNotificationNotNowFlag", true);
			            var screenWidth = kony.os.deviceInfo().screenWidth;
			            kony.store.setItem("screenWidth", screenWidth);
			            frmGuide.flexGuide3.setEnabled(false);
			            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
			            cust360Obj.permission = "denied";
			            MVCApp.Customer360.sendInteractionEvent("NotificationGuide3", cust360Obj);
			            androidPushNavigateToNextFormCallback();
			        };
			        frmGuide.flexGuideControl41.onClick = function() {
			            initializeEstimoteLibraries();
			            frmGuide.flexGuide4.setEnabled(false);
			            skipblueToothScreenForAndroid();
			        };
			        skipblueToothScreenForIPhone = function() {}
			        skipblueToothScreenForAndroid = function() {
			            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
			            var bluetoothEnabled = resourceManager.isBluetoothAvailable();
			            if (bluetoothEnabled) cust360Obj.permission = "granted";
			            else {
			                cust360Obj.permission = "denied";
			            }
			            sendCustomerInteractionForLocationPermission();
			            MVCApp.Customer360.sendInteractionEvent("BluetoothGuide5", cust360Obj);
			            gblCurrentLocationPermission = kony.application.checkPermission(kony.os.RESOURCE_LOCATION, {});
			            if (gblCurrentLocationPermission.status == kony.application.PERMISSION_GRANTED) {
			                gblIsLocationPermissionGranted = true;
			                gblIsLocationPermissionDenied = false;
			            } else if (gblCurrentLocationPermission.status == kony.application.PERMISSION_DENIED) {
			                gblIsLocationPermissionDenied = true;
			            }
			            var userObject = kony.store.getItem("userObj");
			            if (userObject) {
			                MVCApp.getHomeController().showDefaultView();
			                MVCApp.Toolbox.common.showLoadingIndicator("");
			                getProductsCategory();
			            } else {
			                frmGuide.flexGuide5.setEnabled(false);
			                var lVersion = parseFloat(kony.os.deviceInfo().version);
			                if (lVersion < 6.0) {
			                    guideFormAnimation(frmGuide.flexGuide6);
			                    frmGuide.flexGuide6.forceLayout();
			                } else {
			                    guideFormAnimation(frmGuide.flexGuide7);
			                    frmGuide.flexGuide7.forceLayout();
			                }
			            }
			        }
			        frmGuide.flexGuideControl51.onClick = function() {
			            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
			            var bluetoothEnabled = resourceManager.isBluetoothAvailable();
			            if (bluetoothEnabled) cust360Obj.permission = "granted";
			            else {
			                cust360Obj.permission = "denied";
			            }
			            sendCustomerInteractionForLocationPermission();
			            MVCApp.Customer360.sendInteractionEvent("BluetoothGuide5", cust360Obj);
			            gblCurrentLocationPermission = kony.application.checkPermission(kony.os.RESOURCE_LOCATION, {});
			            if (gblCurrentLocationPermission.status == kony.application.PERMISSION_GRANTED) {
			                gblIsLocationPermissionGranted = true;
			                gblIsLocationPermissionDenied = false;
			            } else if (gblCurrentLocationPermission.status == kony.application.PERMISSION_DENIED) {
			                gblIsLocationPermissionDenied = true;
			            }
			            var userObject = kony.store.getItem("userObj");
			            if (userObject) {
			                MVCApp.getHomeController().showDefaultView();
			                MVCApp.Toolbox.common.showLoadingIndicator("");
			                getProductsCategory();
			            } else {
			                frmGuide.flexGuide5.setEnabled(false);
			                var lVersion = parseFloat(kony.os.deviceInfo().version);
			                if (lVersion < 6.0) {
			                    guideFormAnimation(frmGuide.flexGuide6);
			                } else {
			                    guideFormAnimation(frmGuide.flexGuide7);
			                    frmGuide.flexGuide7.forceLayout();
			                }
			            }
			        };
			        frmGuide.flexGuideControl71.onClick = function() {
			            try {
			                if (writesettings == null || writesettings == undefined) {
			                    writesettings = new runtimepermissions.permissions();
			                    writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow"));
			                    writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
			                    writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
			                    writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
			                    writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
			                }
			            } catch (e) {
			                if (e) {
			                    kony.print("Exception while setting run time permissions" + e);
			                }
			            }
			            writesettings.requestPermission("android.write_settings.permission");
			            frmGuide.flexGuide7.setEnabled(false);
			            guideFormAnimation(frmGuide.flexGuide6);
			            frmGuide.flexGuide6.forceLayout();
			        };
			        frmGuide.flexGuideControl61.onClick = function() {
			            MVCApp.getSignInController().load();
			        };
			        frmGuide.flexGuideControl62.onClick = function() {
			            MVCApp.getCreateAccountController().load();
			        };
			        frmGuide.flexGuideControl63.onClick = function() {
			            MVCApp.getHomeController().showDefaultView();
			            MVCApp.Toolbox.common.showLoadingIndicator("");
			            getProductsCategory();
			        };
			    }

			    function androidPushNavigateToNextFormCallback() {
			        if (initLaunchFlagForVersion72 && !isPushRegisterInvokedInPrevVersion && !initLaunchFlag && firstLaunchFromPrevKonyVersionFlag) {
			            var userObject = kony.store.getItem("userObj");
			            if (userObject) {
			                MVCApp.getHomeController().showDefaultView();
			                MVCApp.Toolbox.common.showLoadingIndicator("");
			                getProductsCategory();
			            } else {
			                frmGuide.flexGuide5.setEnabled(false);
			                MVCApp.getGuideController().guideFormAnimation(frmGuide.flexGuide6);
			                frmGuide.flexGuide6.forceLayout();
			            }
			        } else {
			            MVCApp.getGuideController().guideFourDisplay();
			        }
			    }

			    function getCurrentLocationFromGuide() {
			        kony.print("@@@ In getCurrentGeoLocation() @@@");
			        var positionoptions = {
			            timeout: 5000,
			            enableHighAccuracy: true,
			            useBestProvider: true
			        };
			        kony.location.getCurrentPosition(locationSuccesscallback, locationErrorcallback, positionoptions);
			    }

			    function locationErrorcallback(positionerror) {
			        kony.print("@@@ In geoErrorcallback() @@@");
			        //MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.enableLocationService") ,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.confirmationAlert"),constants.ALERT_TYPE_CONFIRMATION,
			        //locationSettingsHandler,MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.notRightNow"));
			        frmGuide.flexGuide2.setEnabled(false);
			        guideFormAnimation(frmGuide.flexGuide3);
			        frmGuide.flexGuide3.forceLayout();
			    }

			    function locationSuccesscallback(position) {
			        var currentLat = "";
			        var currentLon = "";
			        if (position.coords.latitude !== "" && position.coords.longitude !== "") {
			            currentLat = position.coords.latitude;
			            currentLon = position.coords.longitude
			            frmGuide.flexGuide2.setEnabled(false);
			            guideFormAnimation(frmGuide.flexGuide3);
			            frmGuide.flexGuide3.forceLayout();
			        }
			    }

			    function guideFormAnimation(animateObj) {
			        if (animateObj.id == "flexGuide2") {
			            frmGuide.lblGuideTitle21.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideTitle21");
			            frmGuide.lblGuideTitle22.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideTitle22");
			            frmGuide.lblGuideDesc2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideDesc2");
			            frmGuide.lblGuideControl21.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.allowLocation");
			            frmGuide.lblGuideControl22.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.notRightNow");
			            frmGuide.flexGuide2.forceLayout();
			        } else if (animateObj.id == "flexGuide3") {
			            frmGuide.lblGuideTitle3.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSettings.beTheFirst");
			            frmGuide.lblGuideDesc3.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideDesc3");
			            frmGuide.lblGuideControl31.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.allowNotification");
			            frmGuide.lblGuideControl32.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.notRightNow");
			            frmGuide.flexGuide3.forceLayout();
			        } else if (animateObj.id == "flexGuide4") {
			            frmGuide.lblGuideTitle4.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideTitle4");
			            frmGuide.lblGuideDesc4.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideDesc4");
			            frmGuide.lblGuideControl41.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
			            frmGuide.flexGuide4.forceLayout();
			        } else if (animateObj.id == "flexGuide5") {
			            frmGuide.lblGuideTitle5.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideTitle5");
			            frmGuide.lblGuideDesc5.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideDesc5");
			            frmGuide.lblGuideControl51.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
			            frmGuide.flexGuide5.forceLayout();
			        } else if (animateObj.id == "flexGuide6") {
			            frmGuide.lblGuideTitle6.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.helloThere");
			            frmGuide.lblGuideDesc6.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideDesc6");
			            frmGuide.lblGuideControl61.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSignIn");
			            frmGuide.lblGuideControl62.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.btnCreateAccount");
			            frmGuide.lblGuideControl63.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideControl63");
			            frmGuide.flexGuide6.forceLayout();
			        } else if (animateObj.id == "flexGuide7") {
			            if (MVCApp.Toolbox.Service.getLocale().indexOf("fr") > -1) {
			                frmGuide.lblGuideTitle7.top = "5%";
			                frmGuide.lblGuideDesc7.top = "30%";
			            } else {
			                frmGuide.lblGuideTitle7.top = "10%";
			                frmGuide.lblGuideDesc7.top = "25%";
			            }
			            frmGuide.lblGuideTitle7.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideTitle7");
			            frmGuide.lblGuideDesc7.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmGuide.lblGuideDesc7");
			            frmGuide.lblGuideControl71.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
			            frmGuide.flexGuide7.forceLayout();
			        }
			        kony.print("in view animation of Guide  " + animateObj.id);
			        animateObj.animate(kony.ui.createAnimation({
			            0: {
			                left: "500dp",
			                "stepConfig": {}
			            },
			            100: {
			                left: "0dp",
			                "stepConfig": {}
			            }
			        }), {
			            delay: 0,
			            fillMode: kony.anim.FILL_MODE_FORWARDS,
			            duration: 0.25
			        }, {
			            animationEnd: function() {}
			        });
			    }

			    function guideFourDisplay() {
			        frmGuide.flexGuide3.setEnabled(false);
			        guideFormAnimation(frmGuide.flexGuide4);
			        frmGuide.flexGuide4.forceLayout();
			    }

			    function sendCustomerInteractionForLocationPermission() {
			        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
			        var grantPermission = "denied";
			        var permission = kony.application.checkPermission(kony.os.RESOURCE_LOCATION, {});
			        if (permission.status == kony.application.PERMISSION_GRANTED) {
			            grantPermission = "granted";
			        }
			        cust360Obj.permission = grantPermission;
			        MVCApp.Customer360.sendInteractionEvent("WhileUsingAppLocationGuide2", cust360Obj)
			    }
			    return {
			        show: show,
			        bindEvents: bindEvents,
			        guideFourDisplay: guideFourDisplay,
			        guideFormAnimation: guideFormAnimation
			    };
			});