//Type your code here
/*
function tapNone() {
	frmHome.flxFooterWrap.flxProducts.skin = 'slFbox';
	frmHome.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmHome.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmHome.flxFooterWrap.flxEvents.skin = 'slFbox';
	frmHome.flxFooterWrap.flxMore.skin = 'slFbox';
}

function NAV_frmHome() { frmHome.show(); tapNone(); }

function preHome() {
	frmHome.segHomeCarousel.pageSkin = "seg2Normal";
	frmHome.flxHeaderWrap.btnHome.isVisible = false;
	frmHome.flxHeaderWrap.textSearch.text = '';
  	exitInStoreInfo();
}


function exitInStoreInfo() {
	function hideInStoreInfo() {
    	frmHome.flxStoreInfo.isVisible = false;
      	animateInStoreMode();
    }
  
  	frmHome.flxMyStoreContainer.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":"45dp"}}),
      {"delay":3,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
      {"animationEnd": hideInStoreInfo }
    );
  
}


// In Store Mode Animation Script
function animateInStoreMode() {
  	frmHome.flxInStoreNew.isVisible = true;
	function test_CALLBACK() { }
	//var form = kony.application.getCurrentForm();
	function anim_FlxInStoreBtn1() {
        frmHome.lblFindit1.animate(
          kony.ui.createAnimation({"100":{"top":"0.1dp", "stepConfig":{"timingFunction":kony.anim.EASE}}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
          {"animationEnd": test_CALLBACK }
        );
      	frmHome.flxLineVertical.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":"80%"}}),
          {"delay":1.1,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {"animationEnd": test_CALLBACK }
        );
      	frmHome.flxLineHorizontal.animate(
          kony.ui.createAnimation({"100":{"top":"0dp", "stepConfig":{"timingFunction":kony.anim.EASE}, "width":"80%"}}),
          {"delay":1.2,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {"animationEnd": test_CALLBACK }
        );
      
		var trans101 = kony.ui.makeAffineTransform();
    	trans101.scale(1, 1);
      	frmHome.flxInStoreBtn1.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans101}}),
          {"delay":0.7,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {"animationEnd": test_CALLBACK }
        );
      	frmHome.flxInStoreBtn2.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans101}}),
          {"delay":0.8,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {"animationEnd": test_CALLBACK }
        );
      	frmHome.flxInStoreBtn3.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans101}}),
          {"delay":1,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {"animationEnd": test_CALLBACK }
        );
      	frmHome.flxInStoreBtn4.animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans101}}),
          {"delay":0.9,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {"animationEnd": test_CALLBACK }
        );
    }
    
  	frmHome.flxMyStoreContainer.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":"355dp"}}),
      {"delay":0.8,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
      {"animationEnd": anim_FlxInStoreBtn1 }
    );
  
    frmHome.flxLineVertical.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "height":"1dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": test_CALLBACK }
    );
    frmHome.flxLineHorizontal.animate(
      kony.ui.createAnimation({"100":{"top":"0dp", "stepConfig":{"timingFunction":kony.anim.EASE}, "width":"1dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": test_CALLBACK }
    );
  
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(0.01, 0.01);
    frmHome.flxInStoreBtn1.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": test_CALLBACK }
    );
    frmHome.flxInStoreBtn2.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": test_CALLBACK }
    );
    frmHome.flxInStoreBtn3.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": test_CALLBACK }
    );
    frmHome.flxInStoreBtn4.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans100}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
      {"animationEnd": test_CALLBACK }
    );

}

function hideFooter() {  
  	function test_CALLBACK() { }
  	frmHome.flxFooterWrap.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "bottom":"-60dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
      {"animationEnd": test_CALLBACK }
    );
}


function onReachingEndFunCallBack(scrollBox, scrollDirection)
{
	alert("onReachingEnd event triggered");
}

function dispFooter() {    
  	function test_CALLBACK() { }
  	frmHome.flxFooterWrap.animate(
      kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "bottom":"0dp"}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.5},
      {"animationEnd": test_CALLBACK }
    );
}
*/