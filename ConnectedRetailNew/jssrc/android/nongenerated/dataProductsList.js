//Type your code here
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.ProductsList = (function(json) {
    var typeOf = "ProductsList";
    var _data = json || {};
    var gblValueFound = false;
    var gblInnerValues = {};

    function getData() {
        return _data;
    }

    function setData() {
        _data = json;
    }

    function getTotalResults() {
        return _data.total || "";
    }
    var projectCount = 0;

    function setProjectCount(count) {
        projectCount = count;
        MVCApp.getProductsListController().setProjectCount(projectCount);
    }

    function getProjectCount() {
        return projectCount;
    }

    function getAisleStoreMap(eventObj) {
        var params = {};
        params.aisleNo = eventObj.info.aisleNum;
        params.productName = eventObj.info.product_name;
        params.calloutFlag = true;
        MVCApp.sendMetricReport(frmProductsList, [{
            "storemap": "click"
        }]);
        MVCApp.getStoreMapController().load(params);
    }

    function getDataForProductList() {
        var lMyLocationFlag = false;
        var productIds = [];
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            lMyLocationFlag = "true";
        } else {
            lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
        }
        var productResults = completeData || {};
        var total = _data.total || 0;
        lastRecordIndex = lastRecordIndex >= total ? total : lastRecordIndex;
        var productResultsArr = [];
        var dataForProdsArr = [];
        if (MVCApp.Toolbox.common.isArray(productResults)) {
            productResultsArr = productResults;
        } else {
            productResultsArr.push(productResults);
        }
        if (productResultsArr.length > 0) {
            for (var m = 0; m < productResultsArr.length; m++) {
                var productId = productResultsArr[m].c_default_product_id || "";
                if (productId == "") {
                    productId = productResultsArr[m].product_id || "";
                }
                productIds.push(productId);
            }
        } else {
            productIds = [];
        }
        setProductIds(productIds);
        for (i = firstRecordIndex; i < lastRecordIndex; i++) {
            var temp = productResultsArr[i] || "";
            var cStoreInventory;
            if (temp.hasOwnProperty("c_store_inventory") && null != temp.c_store_inventory && undefined != temp.c_store_inventory) {
                cStoreInventory = temp.c_store_inventory || "";
            } else {
                cStoreInventory = "";
            }
            var priceMax = temp.price_max || "";
            var listPrice = temp.list_price || "";
            var listPriceMax = temp.list_price_max || "";
            var priceTobeDisplayed = "";
            var markedPriceToBeDisplayed = "";
            var skinOfPrice = "";
            var isMaster = "false";
            var productType = temp.product_type || {};
            var prices = temp.prices || "";
            var master = productType.master || "false";
            var imageSrc = temp.c_default_image || "";
            var starReview = temp.c_bvAverageRating || 0;
            var reviewNumber = temp.c_bvReviewCount || 0;
            temp.reviewNumber = "(" + reviewNumber + ")";
            if (imageSrc === "") {
                var image = temp.image || "";
                if (image != "") {
                    try {
                        imageSrc = JSON.parse(temp.image).link + "&fit=inside%7C220%3A220";
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "dataProductsList.js on getDataForProductList for temp.image:" + JSON.stringify(e)
                        }]);
                        if (e) {
                            TealiumManager.trackEvent("Image Parse Exception in PLP Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                }
            }
            starReview = parseFloat(starReview);
            starReview = starReview.toFixed(1);
            var starFloor = Math.floor(starReview);
            var starDecimal = starReview.toString().split(".")[1];
            starDecimal = parseInt(starDecimal);
            for (var j = 1; j <= 5; j++) {
                if (j <= starFloor) {
                    temp["imgStar" + j] = {
                        src: "star.png"
                    };
                } else {
                    temp["imgStar" + j] = {
                        src: "starempty.png"
                    };
                }
            }
            var starFlag = starFloor + 1;
            if (starDecimal > 0) {
                temp["imgStar" + starFlag] = {
                    src: "star" + starDecimal + ".png"
                };
            }
            var prodReviews = reviewNumber;
            var rvwNumberStr = prodReviews + "";
            var reviewLen = rvwNumberStr.length;
            var widthAlign = 90;
            if (reviewLen === 0) widthAlign = 90;
            if (reviewLen > 0) {
                var screenWidth = kony.os.deviceInfo().screenWidth;
                if (screenWidth == "414") {
                    widthAlign = 100 + (reviewLen * 6);
                } else {
                    widthAlign = 90 + (reviewLen * 5);
                }
            }
            widthAlign = widthAlign.toString();
            widthAlign = widthAlign + "dp";
            temp.reviewNumber = "(" + prodReviews + ")";
            temp.flxStarRating = {
                "width": widthAlign
            };
            kony.print("prices in PLP :: " + prices);
            temp.flxClearanceText = {
                isVisible: false
            };
            if (prices !== "") {
                prices = prices + "";
                try {
                    prices = JSON.parse(prices);
                    for (var k in prices) {
                        var value1 = prices[k] || "";
                        if ((k == "michaels-usd-Clearance-price" || k == "michaels-usd-Clearance-prices") && value1 !== "") {
                            // alert(" michaelsClearPrice : "+value1);
                            temp.flxClearanceText = {
                                isVisible: true
                            };
                        }
                    }
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "dataProductsList.js on getDataForProductsList for prices:" + JSON.stringify(e)
                    }]);
                    prices = "";
                    if (e) {
                        TealiumManager.trackEvent("Prices Parse Exception in PLP Flow", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            }
            //end
            temp.picProduct = imageSrc;
            if (i === 0) {
                setProjectCount(temp.c_other_count);
            }
            // added for aisle info 
            if (lMyLocationFlag === "true") {
                var aisleNumber;
                var value;
                kony.print(" cStoreInventory : " + cStoreInventory);
                kony.print(" cStoreInventory length : " + cStoreInventory.length);
                if (isEmpty(cStoreInventory) && cStoreInventory !== "" && cStoreInventory.length !== 0) {
                    cStoreInventory = cStoreInventory + "";
                    try {
                        cStoreInventory = JSON.parse(cStoreInventory);
                        for (var key in cStoreInventory) {
                            var value = "";
                            if (cStoreInventory.hasOwnProperty(key)) {
                                value = cStoreInventory[key];
                            }
                            var aisleInfo;
                            if (!cStoreInventory.hasOwnProperty("Location") && !cStoreInventory.hasOwnProperty("ats") && !cStoreInventory.hasOwnProperty("aisle")) {
                                temp.flxItemLocation = {
                                    isVisible: false
                                };
                            } else {
                                if (key == "Location" && cStoreInventory.hasOwnProperty("Location") && value !== "") {
                                    aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(value);
                                    temp.btnStoreMap = {
                                        text: aisleInfo.name.toUpperCase() + " " + aisleInfo.number,
                                        isVisible: true,
                                        onClick: getAisleStoreMap,
                                        info: {
                                            product_name: temp.product_name,
                                            aisleNum: MVCApp.Toolbox.common.getAisleNumber(value),
                                            wrap_clip_id: temp.wrap_clip_id
                                        }
                                    };
                                    temp.lblMapIcon = "c";
                                    //Aisle info made visiblity off(default done)
                                    temp.flxItemLocation = {
                                        isVisible: true
                                    };
                                } else if (key == "ats" && cStoreInventory.hasOwnProperty("ats") && value !== "") {
                                    var stockLevel = value;
                                    //Aisle info made visiblity off(default done)
                                    temp.flxItemLocation = {
                                        isVisible: true
                                    };
                                    //kony.print("parseInt(stockLevel)-----"+parseInt(stockLevel));
                                    temp.lblAisleNo = MVCApp.Toolbox.common.getStockStatus(parseInt(stockLevel));
                                } else if (key == "aisle" && cStoreInventory.hasOwnProperty("aisle") && value !== "") {
                                    aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(value);
                                    temp.btnStoreMap = {
                                        text: aisleInfo.name + " " + aisleInfo.number,
                                        isVisible: true,
                                        onClick: getAisleStoreMap,
                                        info: {
                                            product_name: temp.product_name,
                                            aisleNum: MVCApp.Toolbox.common.getAisleNumber(value)
                                        }
                                    };
                                    temp.lblMapIcon = "c";
                                    //Aisle info made visiblity off(default done)
                                    temp.flxItemLocation = {
                                        isVisible: true
                                    };
                                } else if (key == "wrap_clip_id" && cStoreInventory.hasOwnProperty("wrap_clip_id") && value !== "") {
                                    gblWrap_Clip_ID = value || "";
                                    kony.print("gblWrap_Clip_ID2:" + gblWrap_Clip_ID);
                                }
                            }
                        }
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "dataProductsList.js on getDataForProductsList for cStoreInventory:" + JSON.stringify(e)
                        }]);
                        temp.flxItemLocation = {
                            isVisible: false
                        };
                        if (e) {
                            TealiumManager.trackEvent("Store Inventory Parse Exception in PLP Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                } else {
                    temp.flxItemLocation = {
                        isVisible: false
                    };
                }
            } else {
                temp.flxItemLocation = {
                    isVisible: false
                };
            }
            temp.flxItemLocation = {
                isVisible: false
            };
            kony.print("temp.btnStoreMap : " + temp.btnStoreMap);
            // end
            var isShowOnlinePrice = false;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                var lSalePrice = "";
                var lRegularPrice = "";
                var lRegPrice = "";
                var lSalePriceSkin = "";
                var lRegularPriceSkin = "";
                var lIsMasterProduct = false;
                if (productType) {
                    try {
                        productType = JSON.parse(productType);
                        if (productType.master) {
                            lIsMasterProduct = true;
                        }
                    } catch (e) {
                        if (e) {
                            TealiumManager.trackEvent("JSON Parse Exception in PLP Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                }
                if (cStoreInventory) {
                    if (lIsMasterProduct) {
                        if (cStoreInventory.MinimumRetailPrice && cStoreInventory.MaximumRetailPrice) {
                            lRegularPriceSkin = "sknLblGothamRegular32pxDarkGray";
                            if (parseFloat(cStoreInventory.MinimumRetailPrice) != parseFloat(cStoreInventory.MaximumRetailPrice)) {
                                lRegularPrice = MVCApp.Toolbox.common.showPriceRange(cStoreInventory.MinimumRetailPrice, cStoreInventory.MaximumRetailPrice);
                            } else {
                                if (cStoreInventory.retail_price) {
                                    lRegularPrice = MVCApp.Toolbox.common.addDollarsToNumber(cStoreInventory.retail_price);
                                }
                            }
                        } else {
                            isShowOnlinePrice = true;
                        }
                    } else {
                        var lPricesArr = [];
                        var isRetailPrice = false;
                        if (cStoreInventory.retail_price) {
                            isRetailPrice = true;
                            lPricesArr.push(parseFloat(cStoreInventory.retail_price));
                        }
                        if (cStoreInventory.clearance_price) {
                            lPricesArr.push(parseFloat(cStoreInventory.clearance_price));
                        }
                        if (cStoreInventory.promo_price) {
                            if (cStoreInventory.PromotionalStart && cStoreInventory.PromotionalEnd) {
                                var lIsCurDateWithInPromoDatesRange = checkIfIsCurDateWithInPromoDatesRange(cStoreInventory.PromotionalStart, cStoreInventory.PromotionalEnd);
                                if (lIsCurDateWithInPromoDatesRange) {
                                    lPricesArr.push(parseFloat(cStoreInventory.promo_price));
                                }
                            }
                        }
                        if (lPricesArr.length == 1) {
                            lRegularPriceSkin = "sknLblGothamRegular32pxDarkGray";
                            lRegularPrice = MVCApp.Toolbox.common.addDollarsToNumber(lPricesArr[0]);
                        } else if (lPricesArr.length > 1) {
                            lPricesArr.sort(function(a, b) {
                                return a - b;
                            });
                            lSalePrice = MVCApp.Toolbox.common.addDollarsToNumber(lPricesArr[0]);
                            if (isRetailPrice) {
                                lRegPrice = MVCApp.Toolbox.common.addDollarsToNumber(cStoreInventory.retail_price);
                                if (lSalePrice === lRegPrice) {
                                    lSalePriceSkin = "sknLblGothamRegular32pxDarkGray";
                                } else {
                                    lSalePriceSkin = "sknLblGothamMedium32pxRed";
                                    lRegularPrice = "Reg. " + lRegPrice;
                                    lRegularPriceSkin = "sknLblGothamMedium24pxLtGray";
                                }
                            }
                        } else if (lPricesArr.length === 0) {
                            isShowOnlinePrice = true;
                        }
                    }
                } else {
                    isShowOnlinePrice = true;
                }
                if (!isShowOnlinePrice) {
                    temp.priceProduct = {
                        text: lSalePrice,
                        skin: lSalePriceSkin
                    };
                    temp.markedPriceProduct = {
                        text: lRegularPrice,
                        skin: lRegularPriceSkin
                    };
                }
            } else {
                isShowOnlinePrice = true;
            }
            if (isShowOnlinePrice) {
                if (listPrice !== "") {
                    skinOfPrice = "sknLblGothamMedium32pxRed";
                    if (listPriceMax !== "") {
                        markedPriceToBeDisplayed = MVCApp.Toolbox.common.showPriceRange(listPrice, listPriceMax);
                    } else {
                        markedPriceToBeDisplayed = MVCApp.Toolbox.common.addDollarsToNumber(listPrice);
                    }
                    temp.markedPriceProduct = "Reg. " + markedPriceToBeDisplayed;
                } else {
                    skinOfPrice = "sknLblGothamRegular32pxDarkGray";
                }
                if (priceMax !== "") {
                    priceTobeDisplayed = MVCApp.Toolbox.common.showPriceRange(temp.price, temp.price_max);
                } else {
                    priceTobeDisplayed = MVCApp.Toolbox.common.addDollarsToNumber(temp.price);
                }
                temp.priceProduct = {
                    text: priceTobeDisplayed,
                    skin: skinOfPrice
                };
            }
            dataForProdsArr.push(temp);
        }
        getRefinementsforProductList();
        return dataForProdsArr;
    }

    function getStart() {
        return _data.start;
    }

    function getSelectedSort() {
        return _data.selected_sorting_option;
    }

    function getSelectedRefinement() {
        return _data.selected_refinements;
    }

    function findIfCgidExistsAndReturnInnerValues(valuesOuter, selCategoryID) {
        //horizontal category finding
        var innerValues = [];
        var valueFound = false;
        for (var i = 0; i < valuesOuter.length; i++) {
            if (valuesOuter[i].value === selCategoryID) {
                valueFound = true;
                innerValues = valuesOuter[i] || {};
                gblValueFound = true;
                gblInnerValues = innerValues;
                var labelValue = valuesOuter[i].label;
                MVCApp.getProductsListController().setHeaderNameDeeplink(labelValue);
                break;
            }
        }
        if (!valueFound) {
            for (var j = 0; j < valuesOuter.length; j++) {
                if (!gblValueFound) {
                    var valuesInner = valuesOuter[j].values || [];
                    if (valuesInner.length !== 0) {
                        findIfCgidExistsAndReturnInnerValues(valuesInner, selCategoryID);
                    }
                }
            }
        } else {
            return innerValues;
        }
    }

    function getCategoryOptions() {
        var refinement = _data.refinements || [];
        var refArr = [];
        var tempCategories = "";
        var CategoryArr = [];
        var prodCategoryArr = [];
        var selectedCategory = getSelectedRefinement();
        var selCategoryID = "";
        try {
            if (selectedCategory) {
                selectedCategory = JSON.parse(selectedCategory);
                selCategoryID = selectedCategory.cgid || "";
            }
            refArr = MVCApp.Toolbox.common.isArray(refinement) ? refinement : refArr.push(refinement);
            for (var i = 0; i < refArr.length; i++) {
                var refRow = refArr[i] || {};
                var refLabel = refRow.label || "";
                if (refRow.label == "Category" || refRow.label == "Catégorie") {
                    var refRowValues = refRow.values || [];
                    var refRowValTemp = refRowValues[0] || {};
                    var labelName = refRowValTemp.value;
                    gblInnerValues = {};
                    if (selCategoryID == "Categories" || selCategoryID == "Categories") {
                        var hitCount = refRowValTemp.hit_count;
                        tempCategories = {
                            "header": labelName,
                            "data": refRowValTemp
                        };
                        prodCategoryArr.push(tempCategories);
                    } else {
                        gblValueFound = false;
                        var categoryValues = findIfCgidExistsAndReturnInnerValues(refRow.values || [], selCategoryID);
                        tempCategories = {
                            "header": labelName,
                            "data": gblInnerValues
                        };
                        prodCategoryArr.push(tempCategories);
                    }
                } else {
                    var refRowValues = refRow.values || [];
                    kony.print("values are " + JSON.stringify(refRow));
                    if (refRowValues.length !== 0) {
                        tempCategories = {
                            "header": refLabel,
                            "data": refRow
                        };
                        CategoryArr.push(tempCategories);
                    }
                }
            }
            kony.print("the category array is " + JSON.stringify(CategoryArr));
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "dataProductsList.js on getCategoryOptions for selectedCategory:" + JSON.stringify(e)
            }]);
            if (e) {
                TealiumManager.trackEvent("Selected Category Parse Exception in PLP Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        return {
            CategoryArr: CategoryArr,
            prodCategoryArr: prodCategoryArr
        };
    }

    function getSortingOptions() {
        var sortingData = _data.sorting_options || [];
        var sortArr = [];
        for (var i = 0; i < sortingData.length; i++) {
            var temp = {};
            temp.id = sortingData[i].id;
            temp.label = sortingData[i].label;
            sortArr.push(temp);
        }
        return sortArr;
    }

    function getRefinementsforProductList() {
        var sortArr = getSortingOptions();
        var categoriesObj = getCategoryOptions();
        var prodCategoryArr = categoriesObj.prodCategoryArr;
        var categoriesArr = categoriesObj.CategoryArr;
        var projectCount = getProjectCount();
        var refObj = {
            "sortArr": sortArr,
            "categoriesArr": categoriesArr,
            "projectCount": projectCount,
            "prodCategoryArr": prodCategoryArr
        };
        MVCApp.getSortProductController().setRefinementObj(refObj);
        var selectedRefinement = getSelectedRefinement() || "";
        var selObj = {};
        if (selectedRefinement !== "") {
            try {
                selObj = JSON.parse(selectedRefinement);
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "dataProductsList.js on getRefinementsforProductList for selectedRefinement:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("Selected Refinement Parse Exception in PLP Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        var countOfRef = 0;
        for (var key in selObj) {
            if (key !== "cgid" && key !== "pmid") {
                var refString = selObj[key];
                var refArr = refString.split("|") || [];
                countOfRef = countOfRef + refArr.length;
                //countOfRef++;
            }
        }
        MVCApp.getProductsListController().setRefineCount(countOfRef);
        MVCApp.getSortProductController().setSelectedRefinement(selectedRefinement);
        var selectedSort = getSelectedSort();
        MVCApp.getSortProductController().setSelectedSort(selectedSort);
    }

    function getProductID() {
        var productResults = _data.hits || {};
        var productResultsArr = [];
        var dataForProdsArr = [];
        var prodId = "";
        var defVarId = "";
        var upc = "";
        var variant = false;
        if (MVCApp.Toolbox.common.isArray(productResults)) {
            productResultsArr = productResults;
        } else {
            productResultsArr.push(productResults);
        }
        if (productResultsArr.length !== 0 && productResultsArr.length != undefined) {
            defVarId = productResultsArr[0].c_default_product_id || "";
            prodId = productResultsArr[0].product_id || "";
            upc = productResultsArr[0].c_upc || "";
            variant = true;
            var prodType = productResultsArr[0].product_type + "" || {};
            try {
                var prodTypeObj = JSON.parse(prodType);
                var master = prodTypeObj.master || "";
                if (master === true) {
                    variant = false;
                } else {
                    variant = true;
                }
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "dataProductsList.js on getProductID for prodType:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("Product Type Parse Exception in PLP Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        return {
            productID: prodId,
            defaultVariantID: defVarId,
            isVariant: variant,
            upc: upc
        };
    }

    function isEmpty(record) {
        if (record !== undefined && record !== null) {
            return true;
        }
        return false;
    }
    var productIds = [];

    function getProductIds() {
        return productIds;
    }

    function setProductIds(values) {
        MVCApp.getProductsListController().setProductIds(values);
        productIds = values;
    }
    return {
        typeOf: typeOf,
        getData: getData,
        setData: setData,
        getDataForProductList: getDataForProductList,
        getTotalResults: getTotalResults,
        getStart: getStart,
        getSortingOptions: getSortingOptions,
        getRefinementsforProductList: getRefinementsforProductList,
        getProductID: getProductID,
        isEmpty: isEmpty,
        getProductIds: getProductIds
    };
});