/**
 * PUBLIC
 * This is the view for the MyAccount form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.MyAccountView = (function() {
    /**
     * PUBLIC
     * Open the MyAccount form
     */
    /**
     * @function
     *
     */
    function show() {
        if (MVCApp.Toolbox.common.getRegion() == "CA") {
            frmMyAccount.btnViewRewards.setVisibility(false);
        } else {
            frmMyAccount.btnViewRewards.setVisibility(true);
        }
        frmMyAccount.flxFooterWrap.isVisible = false;
        frmMyAccount.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function postShowAcc() {
        MVCApp.Toolbox.common.setBrightness("frmAccount");
        MVCApp.Toolbox.common.destroyStoreLoc();
    }

    function bindEvents() {
        MVCApp.tabbar.bindEvents(frmMyAccount);
        MVCApp.tabbar.bindIcons(frmMyAccount, "frmMore");
        frmMyAccount.postShow = postShowAcc;
        frmMyAccount.btnViewRewards.onClick = function() {
            // alert("Coupons net yet implemented,Showing Home Page");
            frmObject = frmMyAccount;
            MVCApp.getHomeController().loadCoupons();
        };
        /**
         * @function
         *
         */
        frmMyAccount.btnHeaderLeft.onClick = function() {
            var formID = kony.application.getPreviousForm();
            if (formID == frmCoupons) {
                //frmObject=frmCoupons;
                MVCApp.getHomeController().loadCoupons();
            } else {
                MVCApp.getMoreController().load();
            }
        };
        frmMyAccount.btnSignOut.onClick = function() {
            MVCApp.getMyAccountController().logout();
        };
        try {
            frmMyAccount.flexCouponContainer.btnSignIn.onClick = function() {
                fromCoupons = true;
                isFromShoppingList = false;
                MVCApp.getSignInController().load();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking Sign In Button in My Account Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            frmMyAccount.flexCouponContainer.btnSignUp.onClick = function() {
                fromCoupons = true;
                //MVCApp.CouponsView().hideCoupons();
                var userObject = kony.store.getItem("userObj");
                if (userObject === "" || userObject === null) {
                    isFromShoppingList = false;
                    MVCApp.getSignInController().load();
                } else {
                    MVCApp.getJoinRewardsController().load(userObject);
                }
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking Sign Up Button in My Account Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        /**
         * @function
         *
         */
        try {
            frmMyAccount.flexCouponContainer.btnDone.onClick = function() {
                MVCApp.CouponsView().hideCoupons();
                frmMyAccount.flxFooterWrap.isVisible = false;
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking Done Button in My Account Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function updateScreen(createUserResponse) {
        var firstName = createUserResponse.first_name || "";
        var lastName = createUserResponse.last_name || "";
        var email = createUserResponse.login || "";
        frmMyAccount.lblWelcomeName.text = "Welcome " + firstName + " " + lastName;
        frmMyAccount.lblLoggedmMailId.text = "You’re signed-in as " + email;
        var shopList = false;
        if (kony.application.getPreviousForm() == frmCoupons) {
            //frmObject=frmCoupons;
            MVCApp.getHomeController().loadCoupons();
        } else if (isFromShoppingList) {
            isFromShoppingList = false;
            shopList = true;
            if (frmObject.id === "frmPDP" || frmObject.id === "frmPJDP") {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.SignedIn"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.heyThere"), constants.ALERT_TYPE_INFO, addProdOrProjToShoppingList, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"), "");
            } else {
                MVCApp.getShoppingListController().getProductListItems();
            }
        } else {
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && kony.application.getPreviousForm() && kony.application.getPreviousForm().id === "frmHome") {
                gblNewBrightnessValueStoreMode = gblBrightNessValue;
                gblIsNewBrightnessValuePresent = true;
                gblIsFromHomeScreen = false;
                MVCApp.getHomeController().onClickRewards();
                frmObject = frmHome;
            } else {
                gblBrightNessrewardsValue = gblBrightNessValue;
                MVCApp.getMoreController().load();
                rewardsToggle();
                gblBrightnessfrmSignInPage = true;
            }
        }
        if (!shopList) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
    }

    function executeRewardsToggleCallback() {
        try {
            kony.timer.cancel("timerRewardsToggle");
            rewardsToggle();
        } catch (e) {
            kony.print("@@@LOG:executeRewardsToggleCallback-e:" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Timer Exception During Rewards Option Toggle", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function addProdOrProjToShoppingList() {
        var prodOrProjID = kony.store.getItem("shoppingListProdOrProjID");
        if (prodOrProjID) {
            frmObject.show();
            if (frmObject.id === "frmPDP") {
                MVCApp.getShoppingListController().addToProductList(prodOrProjID, MVCApp.Service.Constants.ShoppingList.pdpType);
            } else {
                MVCApp.getShoppingListController().addProjectListLooping(prodOrProjID);
            }
        } else {
            frmObject.show();
        }
    }

    function logout(results) {
        kony.store.setItem("userObj", "");
        MVCApp.getMoreController().load();
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        logout: logout
    };
});