/**
 * PUBLIC
 * This is the controller for the Help form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ReviewProfileController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.ReviewProfileModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.ReviewProfileView();
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load() {
        init();
        view.updateScreen();
        //model.getRewardsProfile(view.updateScreen);
    }

    function updateRewardProfile(pFromWhichStep) {
        init();
        MVCApp.Toolbox.common.showLoadingIndicator("");
        var requestParams = {};
        var step1Flag = false;
        var step2Flag = false;
        var step3Flag = false;
        var step4Flag = false;
        var step5Flag = false;
        requestParams.client_id = MVCApp.Service.Constants.Common.clientId;
        requestParams.customer_id = createAccountResObj.customer_id;
        if (kony.store.getItem("touchIdSaved") && fromTouchId) {
            requestParams.user = kony.store.getItem("storeFirstUserForTouchId");
            requestParams.password = kony.store.getItem("storeFirstPwdForTouchId");
        } else {
            requestParams.user = gblDecryptedUser;
            requestParams.password = gblDecryptedPassword;
        }
        if (pFromWhichStep === "step2") {
            step1Flag = true;
            step2Flag = true;
        } else if (pFromWhichStep === "step3") {
            step1Flag = true;
            step2Flag = true;
            step3Flag = true;
        } else if (pFromWhichStep === "step4") {
            step1Flag = true;
            step2Flag = true;
            step3Flag = true;
            step4Flag = true;
        } else if (pFromWhichStep === "step5" || pFromWhichStep === undefined) {
            step1Flag = true;
            step2Flag = true;
            step3Flag = true;
            step4Flag = true;
            step5Flag = true;
        }
        //Step 1 request data
        if (step1Flag) {
            requestParams.first_name = rewardsProfileData.firstName;
            requestParams.last_name = rewardsProfileData.lastName;
            requestParams.c_loyaltyEmail = rewardsProfileData.email;
            requestParams.phone = rewardsProfileData.phoneNo;
            requestParams.c_loyaltyBirthday = rewardsProfileData.birthday;
        }
        //Step 2 request data
        if (step2Flag) {
            requestParams.address1 = rewardsProfileData.address1;
            requestParams.address2 = rewardsProfileData.address2;
            requestParams.city = rewardsProfileData.city;
            requestParams.postal_code = rewardsProfileData.zip;
            requestParams.state_code = rewardsProfileData.state;
            requestParams.isAddressExists = rewardsProfileData.isAddressExists;
            requestParams.c_loyaltyAddressUUID = rewardsProfileData.c_loyaltyAddressUUID || "";
        }
        //Step 3 request data
        if (step3Flag) {
            requestParams.c_childrendata = rewardsProfileData.c_childrendata || "";
        }
        //Step 4 request data
        if (step4Flag) {
            requestParams.c_interests = rewardsProfileData.c_interests || "";
            requestParams.c_surveyQA = rewardsProfileData.c_surveyQA || "";
        }
        //Step 5 request data
        if (step5Flag) {
            var favStore = rewardsFavouriteStore;
            if (favStore != "" && favStore != null && favStore != undefined) {
                try {
                    var favStoreObj = JSON.parse(favStore);
                    if (favStoreObj != "") {
                        requestParams.c_preferredStore = favStoreObj.clientkey || "";
                    } else {
                        requestParams.c_preferredStore = "";
                    }
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlReviewProfile.js on updateRewardsProfile for favStore:" + JSON.stringify(e)
                    }]);
                    requestParams.c_preferredStore = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception in Review Profile Flow", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                requestParams.c_preferredStore = "";
            }
        }
        model.updateRewardProfile(view.confirmationAlertMessage, requestParams);
    }

    function sumary() {
        init();
        view.sumary();
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        updateRewardProfile: updateRewardProfile,
        sumary: sumary
    };
});