/**
 * PUBLIC
 * This is the controller for the ProductsList form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.CouponListController = (function() {
    var _isInit = false;
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.CouponListWebModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.webView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }

    function load() {
        init();
        model.load(view.loadPdpView);
        _isInit = false;
    }
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load
    };
});