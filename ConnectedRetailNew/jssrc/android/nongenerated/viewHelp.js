/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.HelpView = (function() {
    /**
     * PUBLIC
     * Open the Events form
     */
    function show() {
        MVCApp.Toolbox.common.setTransition(frmHelp, 3);
        frmHelp.show();
        MVCApp.tabbar.bindEvents(frmHelp);
        MVCApp.tabbar.bindIcons(frmHelp, "frmMore");
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function showPreviousForm() {
        var prevForm = kony.application.getPreviousForm();
        var prevFormId = prevForm.id || "";
        if (entryBrowserFormName == "frmCartView") {
            MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
        } else if (entryBrowserFormName === "frmWebView") {
            var url = MVCApp.Toolbox.common.findWebViewUrlToGoBack();
            MVCApp.getProductsListWebController().loadWebView(url);
        } else if (prevFormId == "frmHome") {
            MVCApp.getHomeController().load();
        } else {
            goBackToMore();
        }
    }

    function bindEvents() {
        frmHelp.btnCall.onClick = callCustomerCare;
        frmHelp.btnEmail.onClick = emailCustomerCare;
        frmHelp.btnReview.onClick = reviewApp;
        frmHelp.btnHeaderLeft.onClick = showPreviousForm;
        frmHelp.postShow = function() {
            MVCApp.Toolbox.common.setBrightness("frmHelp");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Michaels Mobile App: Help Screen";
            lTealiumTagObj.page_name = "Michaels Mobile App: Help Screen";
            lTealiumTagObj.page_type = "Help";
            lTealiumTagObj.page_category_name = "Help";
            lTealiumTagObj.page_category = "Help";
            TealiumManager.trackView("Help Screen", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            MVCApp.Customer360.sendInteractionEvent("Help", cust360Obj);
        };
    }

    function updateScreen() {
        show();
    }

    function callCustomerCare() {
        MVCApp.getHelpDetailsController().callCustomerCare();
    }

    function emailCustomerCare() {
        MVCApp.getHelpDetailsController().emailCustomerCare();
    }

    function reviewApp() {
        MVCApp.getHelpDetailsController().reviewApp();
    }

    function openCCPA() {
        MVCApp.getHelpDetailsController().openCCPA();
    }

    function goBackToMore() {
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.lowercase.YouAreShoppingAt");
            if (gblInStoreModeDetails.POI) {
                if (gblInStoreModeDetails.POI.address2) {
                    frmMore.lblMyStoreName.text = gblInStoreModeDetails.POI.address2;
                } else if (gblInStoreModeDetails.POI.city) {
                    frmMore.lblMyStoreName.text = gblInStoreModeDetails.POI.city;
                } else {
                    frmMore.lblMyStoreName.text = gblInStoreModeDetails.storeName;
                }
            } else {
                frmMore.lblMyStoreName.text = gblInStoreModeDetails.storeName;
            }
            if (MVCApp.Toolbox.common.getRegion() !== "US") {
                frmMore.flxStoreMap.setVisibility(true);
            } else {
                frmMore.flxStoreMap.isVisible = false;
            }
            frmMore.flxStoreInfo.labelSeparator.isVisible = false;
        } else {
            if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
                frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.lblMyStore");
                try {
                    var location = JSON.parse(kony.store.getItem("myLocationDetails"));
                    if (location.address2 !== "") {
                        frmMore.lblMyStoreName.text = location.address2;
                    } else {
                        frmMore.lblMyStoreName.text = location.city;
                    }
                    if (MVCApp.Toolbox.common.getRegion() !== "US") {
                        frmMore.flxStoreMap.setVisibility(true);
                    } else {
                        frmMore.flxStoreMap.isVisible = false;
                    }
                    frmMore.flxStoreInfo.labelSeparator.setVisibility(false);
                    frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.lblMyStore");
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "viewMore.js on lInStoreModeDetails for myLocationDetails:" + JSON.stringify(e)
                    }]);
                    frmMore.flxStoreMap.setVisibility(false);
                    frmMore.flxStoreInfo.labelSeparator.setVisibility(true);
                    frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.setMyStore");
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception in Help View Flow", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                frmMore.flxStoreMap.setVisibility(false);
                frmMore.flxStoreInfo.labelSeparator.setVisibility(true);
                frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.setMyStore");
            }
        }
        MVCApp.Toolbox.common.setTransition(frmMore, 2);
        frmMore.show();
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});