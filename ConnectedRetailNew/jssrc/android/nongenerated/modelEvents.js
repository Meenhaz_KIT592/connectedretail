/**
 * PUBLIC
 * This is the model for Events form
 */
var MVCApp = MVCApp || {};
MVCApp.EventsModel = (function() {
    var serviceType = MVCApp.serviceType.events;
    var operationId = MVCApp.serviceEndPoints.storeEvents;
    kony.print("serviceType" + serviceType);
    kony.print("operationId" + operationId);

    function getEvents(callback, requestParams, eventid) {
        try {
            MakeServiceCall(serviceType, operationId, requestParams, function(results) { //successCallBack
                if (results.opstatus === 0 || results.opstatus === "0") {
                    var processedEventsObject = new MVCApp.data.Events(results);
                    callback(processedEventsObject, eventid);
                }
            }, function(error) { //errorCallBack
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                kony.print("Error response while getting events " + JSON.stringify(error));
            });
        } catch (e) {
            kony.print("excpetion occurred while getting events - " + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Events Exception in Events Service Call Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    return {
        getEvents: getEvents
    };
});