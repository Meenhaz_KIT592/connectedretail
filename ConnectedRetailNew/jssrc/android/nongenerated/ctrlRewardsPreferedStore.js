/**
 * PUBLIC
 * This is the controller for the Help form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.RewardsProfilePreferedStoreController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        //model = new MVCApp.RewardsProfileChildModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.RewardsProfilePreferedStoreView();
        model = new MVCApp.RewardsProfilePreferedStoreModel();
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load(editFlag) {
        init();
        setEditFlag(editFlag);
        view.updateScreen(editFlag);
    }

    function initiateSearch(text) {
        text = text.trim();
        if (text !== "") {
            var inputParams = {};
            inputParams.country = MVCApp.Service.Constants.Common.country;
            inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
            inputParams.addressline = text;
            model.loadDataFromSearch(inputParams, view.updateRewardsStoreScreen);
        } else {
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.enterText"));
        }
    }
    var prevLoc = null;

    function setPreviousLocation(value) {
        prevLoc = value;
    }

    function getPreviousLocation() {
        return prevLoc;
    }
    var rewardPrefferedStore = "";

    function setRewardPreferredStore(value) {
        rewardPrefferedStore = value;
    }

    function getRewardPreferredStore() {
        return rewardPrefferedStore;
    }
    var changeFlag = false;

    function setEditFlag(value) {
        changeFlag = value;
    }

    function getEditFlag() {
        return changeFlag;
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        initiateSearch: initiateSearch,
        setPreviousLocation: setPreviousLocation,
        getPreviousLocation: getPreviousLocation,
        setRewardPreferredStore: setRewardPreferredStore,
        getRewardPreferredStore: getRewardPreferredStore,
        setEditFlag: setEditFlag,
        getEditFlag: getEditFlag
    };
});