/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var ifEventDetailDeeplink = false;
var eventsfromdeeplink;
var MVCApp = MVCApp || {};
MVCApp.EventsView = (function() {
    /**
     * PUBLIC
     * Open the Events form
     */
    function show() {
        MVCApp.Toolbox.common.setTransition(frmEvents, 3);
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmEvents.segEvents.onRowClick = navigateToEventDetails;
        frmEvents.btnHeaderLeft.onClick = goBackToMore;
        frmEvents.onDeviceBack = goBackToMore;
        frmEvents.postShow = function() {
            MVCApp.Toolbox.common.setBrightness("frmEvents");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Events: Michaels Mobile App";
            lTealiumTagObj.page_name = "Events: Michaels Mobile App";
            lTealiumTagObj.page_type = "Navigation";
            lTealiumTagObj.page_category_name = "Events";
            lTealiumTagObj.page_category = "Events";
            TealiumManager.trackView("Events", lTealiumTagObj);
        };
        frmEvents.segEvents.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
        frmEvents.segEvents.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
        MVCApp.tabbar.bindEvents(frmEvents);
        MVCApp.tabbar.bindIcons(frmEvents, "frmMore");
    }

    function updateScreen(results, eventid) {
        var eventsList = results.getEventsListData() || [];
        if (eventid) {
            for (var i = 0; i < eventsList.length; i++) {
                if (eventsList[i].eventIDTrack === eventid.eventid) {
                    ifEventDetailDeeplink = true;
                    eventsfromdeeplink = eventsList[i];
                    MVCApp.getEventDetailsController().setStoreModeEventDetails(eventsfromdeeplink);
                    MVCApp.getEventDetailsController().setEventDetails(eventsfromdeeplink);
                    MVCApp.getEventDetailsController().load();
                    break;
                } else {
                    MVCApp.getHomeController().load();
                }
            }
        } else if (eventsList.length > 0) {
            frmEvents.segEvents.setVisibility(true);
            frmEvents.segEvents.setData(eventsList);
        } else {
            frmEvents.lblNoEvents.setVisibility(true);
            frmEvents.lblNoEvents.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.events.noEvents");
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        show();
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.storeid = MVCApp.Toolbox.common.getStoreID();
        var resultSet = MVCApp.getEventsController().getResultEvents();
        if (resultSet && resultSet.length > 0) {
            for (var m = 0; m < resultSet.length; m++) {
                cust360Obj["eventid" + m] = resultSet[m];
            }
        }
        MVCApp.Customer360.sendInteractionEvent("EventsList", cust360Obj);
    }

    function clearForm() {
        frmEvents.segEvents.removeAll();
        frmEvents.segEvents.setVisibility(false);
        frmEvents.lblNoEvents.setVisibility(false);
    }

    function navigateToEventDetails() {
        var eventDetails = frmEvents.segEvents.selectedRowItems || {};
        MVCApp.getEventDetailsController().setStoreModeEventDetails(eventDetails);
        MVCApp.getEventDetailsController().setEventDetails(eventDetails);
        MVCApp.getEventDetailsController().setFormEntryFromCartOrWebView(false);
        MVCApp.getEventDetailsController().load();
    }

    function goBackToMore() {
        var prevousFormFlag = MVCApp.getEventsController().getFromMoreFlag();
        if (prevousFormFlag) {
            MVCApp.Toolbox.common.setTransition(frmMore, 2);
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.lowercase.YouAreShoppingAt");
                if (gblInStoreModeDetails.POI) {
                    if (gblInStoreModeDetails.POI.address2) {
                        frmMore.lblMyStoreName.text = gblInStoreModeDetails.POI.address2;
                    } else if (gblInStoreModeDetails.POI.city) {
                        frmMore.lblMyStoreName.text = gblInStoreModeDetails.POI.city;
                    } else {
                        frmMore.lblMyStoreName.text = gblInStoreModeDetails.storeName;
                    }
                } else {
                    frmMore.lblMyStoreName.text = gblInStoreModeDetails.storeName;
                }
                if (MVCApp.Toolbox.common.getRegion() !== "US") {
                    frmMore.flxStoreMap.isVisible = true;
                } else {
                    frmMore.flxStoreMap.isVisible = false;
                }
                frmMore.flxStoreInfo.labelSeparator.isVisible = false;
                if (frmMore.btnRewardsSignUp.isVisible === false && !toggleRewards) {
                    MVCApp.Toolbox.common.setBrightnessForBarcode();
                }
            } else {
                if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
                    var location = JSON.parse(kony.store.getItem("myLocationDetails"));
                    if (location.address2 !== "") {
                        frmMore.lblMyStoreName.text = location.address2;
                    } else {
                        frmMore.lblMyStoreName.text = location.city;
                    }
                    if (MVCApp.Toolbox.common.getRegion() !== "US") {
                        frmMore.flxStoreMap.setVisibility(true);
                    } else {
                        frmMore.flxStoreMap.isVisible = false;
                    }
                    frmMore.flxStoreInfo.labelSeparator.setVisibility(false);
                    frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.lblMyStore");
                } else {
                    frmMore.flxStoreMap.setVisibility(false);
                    frmMore.flxStoreInfo.labelSeparator.setVisibility(true);
                    frmMore.flxStoreInfo.lblMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.setMyStore");
                }
            }
            frmMore.show();
        } else {
            MVCApp.getHomeController().load();
        }
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
            frmEvents.segEvents.bottom = "0dp";
        } else {
            MVCApp.Toolbox.common.dispFooter();
            frmEvents.segEvents.bottom = "54dp";
        }
        if (checkScroll == 0) {
            MVCApp.Toolbox.common.hideFooter();
            frmEvents.segEvents.bottom = "0dp";
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        clearForm: clearForm
    };
});