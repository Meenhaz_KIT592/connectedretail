//Type your code here
var voiceSearchFlag = false;
var previousForm = {};
var MVCApp = MVCApp || {};
var frmObject = {};
var tapToCancelFlag = false;
var sorryPageFlag = false;
var deviceBackFlag = false;
var cameraWidgetRef = null;
var createVoiceAndSpeechObjectsFlag = false;
var couponsEntryFromVoiceSearch = false
var voiceSearchForm = {};
var entryFormObjectToBrowser = {};
MVCApp.searchBar = {
    bindEvents: function(myForm, formName) {
        frmObject = myForm;
        try {
            if (myForm.flxHeaderWrap.flxSearchIcon) {
                myForm.flxHeaderWrap.flxSearchIcon.onClick = function() {
                    voiceSearchFlag = false;
                    if (myForm.id == "frmHome") {
                        myForm.flxSearchOverlay.flxContainerSearchInner.top = myForm.flxHeaderWrap.flxSearchContainer.top;
                    } else {
                        if (myForm.flxSearchOverlay) {
                            myForm.flxSearchOverlay.flxContainerSearchInner.top = "0dp";
                        }
                    }
                    myForm.flxSearchOverlay.txtSearchField.setFocus(true);
                    myForm.flxSearchOverlay.flxContainerSearchExpand.forceLayout();
                    myForm.flxSearchOverlay.lblVoice.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchMessage");
                    myForm.flxSearchOverlay.lblVisual.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.visualSearchMessage");
                    myForm.flxSearchOverlay.lblBarCode.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.barCodeMessage");
                    myForm.flxSearchOverlay.txtSearchField.text = myForm.flxHeaderWrap.textSearch.text;
                    var locale = kony.store.getItem("languageSelected");
                    if (locale == "fr_CA") {
                        myForm.flxSearchOverlay.flxContainerSearchInner.height = "140dp";
                    } else {
                        myForm.flxSearchOverlay.flxContainerSearchInner.height = "130dp";
                    }
                    myForm.flxSearchOverlay.txtSearchField.right = "35dp";
                    myForm.flxSearchOverlay.setVisibility(true);
                }
            }
        } catch (e) {
            if (e) {
                kony.print("IN EXCEPTION::" + e);
                //TealiumManager.trackEvent("UI Exception On Search Textfield", {exception_name:e.name,exception_reason:e.message,exception_trace:e.stack,exception_type:e});
            }
        }
        try {
            if (myForm.flxHeaderWrap.textSearch) {
                myForm.flxHeaderWrap.textSearch.setEnabled(false);
                myForm.flxHeaderWrap.flxTextboxContents.onClick = function() {
                    voiceSearchFlag = false;
                    if (myForm.id == "frmHome") {
                        myForm.flxSearchOverlay.flxContainerSearchInner.top = myForm.flxHeaderWrap.flxSearchContainer.top;
                    } else {
                        if (myForm.flxSearchOverlay) {
                            myForm.flxSearchOverlay.flxContainerSearchInner.top = "0dp";
                        }
                    }
                    myForm.flxSearchOverlay.txtSearchField.setFocus(true);
                    myForm.flxSearchOverlay.flxContainerSearchExpand.forceLayout();
                    myForm.flxSearchOverlay.lblVoice.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchMessage");
                    myForm.flxSearchOverlay.lblVisual.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.visualSearchMessage");
                    myForm.flxSearchOverlay.lblBarCode.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.barCodeMessage");
                    myForm.flxSearchOverlay.txtSearchField.text = myForm.flxHeaderWrap.textSearch.text;
                    var locale = kony.store.getItem("languageSelected");
                    if (locale == "fr_CA") {
                        myForm.flxSearchOverlay.flxContainerSearchInner.height = "140dp";
                    } else {
                        myForm.flxSearchOverlay.flxContainerSearchInner.height = "130dp";
                    }
                    myForm.flxSearchOverlay.txtSearchField.right = "35dp";
                    myForm.flxSearchOverlay.setVisibility(true);
                };
            }
        } catch (e) {
            if (e) {
                kony.print("IN EXCEPTION::" + e);
            }
        }
        try {
            if (myForm.flxHeaderWrap.cmrImageSearch) {
                currentForm = myForm;
                if (myForm.flxHeaderWrap.btnCameraImageSearch) {
                    myForm.flxHeaderWrap.btnCameraImageSearch.onClick = function() {
                        voiceSearchFlag = false;
                        var lResult = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, {});
                        if (lResult.status == kony.application.PERMISSION_DENIED) {
                            requestPermissionAtRuntime(kony.os.RESOURCE_CAMERA, function(pResponse) {
                                // kony.print("LOG:permissionStatusCallback-pResponse::"+ JSON.stringify(pResponse));
                                if (pResponse.status == kony.application.PERMISSION_GRANTED) {
                                    kony.print("LOG:permissionStatusCallback-Permission Granted");
                                    cameraWidgetRef = myForm.flxHeaderWrap.cmrImageSearch;
                                    myForm.flxHeaderWrap.cmrImageSearch.openCamera();
                                } else {
                                    // kony.print("LOG:permissionStatusCallback-Access Denied");
                                }
                            });
                        } else {
                            myForm.flxHeaderWrap.cmrImageSearch.openCamera();
                        }
                        try {
                            if (cameraSettings == null || cameraSettings == undefined) {
                                cameraSettings = new runtimepermissions.permissions();
                                cameraSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
                                cameraSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.camerasettingpermission"));
                                cameraSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
                                cameraSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.cameraservices"));
                            }
                        } catch (e) {
                            if (e) {
                                kony.print("Exception while setting run time permissions" + e);
                            }
                        }
                        cameraSettings.requestPermission("android.camera.permission");
                    };
                }
                myForm.flxHeaderWrap.cmrImageSearch.onCapture = MVCApp.getImageSearchController().didCaptureImage;
                myForm.flxHeaderWrap.cmrImageSearch.onTouchEnd = function() {
                    voiceSearchFlag = false;
                    cameraWidgetRef = myForm.flxHeaderWrap.cmrImageSearch;
                    MVCApp.getImageSearchController().onTouchEndImageSearchCamera();
                };
                myForm.flxHeaderWrap.cmrImageSearch.cameraOptions = {
                    hideControlBar: true,
                    focusMode: constants.CAMERA_FOCUS_MODE_CONTINUOUS
                };
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Camera widget not available.", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxHeaderWrap.btnBarCode) {
                currentForm = myForm;
                myForm.flxHeaderWrap.btnBarCode.onClick = function() {
                    voiceSearchFlag = false;
                    MVCApp.getScanController().load();
                };
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception on clicking the barcode button", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchOverlay.flxVisual.cmrImageSearch) {
                currentForm = myForm;
                if (myForm.flxSearchOverlay.flxVisual) {
                    myForm.flxSearchOverlay.flxVisual.onClick = function() {
                        voiceSearchFlag = false;
                        requestPermissionAtRuntime(kony.os.RESOURCE_CAMERA, function(pResponse) {
                            kony.print("LOG:permissionStatusCallback-pResponse::" + JSON.stringify(pResponse));
                            if (pResponse.status == kony.application.PERMISSION_GRANTED) {
                                kony.print("LOG:permissionStatusCallback-Permission Granted");
                                cameraWidgetRef = myForm.flxSearchOverlay.flxVisual.cmrImageSearch;
                                myForm.flxSearchOverlay.flxVisual.cmrImageSearch.openCamera();
                            } else {
                                //   kony.print("LOG:permissionStatusCallback-Access Denied");
                            }
                        });
                        try {
                            if (cameraSettings == null || cameraSettings == undefined) {
                                cameraSettings = new runtimepermissions.permissions();
                                cameraSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
                                cameraSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.camerasettingpermission"));
                                cameraSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
                                cameraSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.cameraservices"));
                            }
                        } catch (e) {
                            if (e) {
                                kony.print("Exception while setting run time permissions" + e);
                            }
                        }
                        cameraSettings.requestPermission("android.camera.permission");
                    };
                    if (myForm.flxHeaderWrap.btnCameraImageSearch) {
                        myForm.flxHeaderWrap.btnCameraImageSearch.onClick = function() {
                            voiceSearchFlag = false;
                            requestPermissionAtRuntime(kony.os.RESOURCE_CAMERA, function(pResponse) {
                                // kony.print("LOG:permissionStatusCallback-pResponse::"+ JSON.stringify(pResponse));
                                if (pResponse.status == kony.application.PERMISSION_GRANTED) {
                                    kony.print("LOG:permissionStatusCallback-Permission Granted");
                                    cameraWidgetRef = myForm.flxSearchOverlay.flxVisual.cmrImageSearch;
                                    myForm.flxSearchOverlay.flxVisual.cmrImageSearch.openCamera();
                                } else {
                                    // kony.print("LOG:permissionStatusCallback-Access Denied");
                                }
                            });
                            try {
                                if (cameraSettings == null || cameraSettings == undefined) {
                                    cameraSettings = new runtimepermissions.permissions();
                                    cameraSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
                                    cameraSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.camerasettingpermission"));
                                    cameraSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
                                    cameraSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.cameraservices"));
                                }
                            } catch (e) {
                                if (e) {
                                    kony.print("Exception while setting run time permissions" + e);
                                }
                            }
                            cameraSettings.requestPermission("android.camera.permission");
                        };
                    }
                }
                myForm.flxSearchOverlay.flxVisual.cmrImageSearch.onCapture = MVCApp.getImageSearchController().didCaptureImage;
                myForm.flxSearchOverlay.flxVisual.cmrImageSearch.onTouchEnd = function() {
                    voiceSearchFlag = false;
                    cameraWidgetRef = myForm.flxSearchOverlay.flxVisual.cmrImageSearch;
                    MVCApp.getImageSearchController().onTouchEndImageSearchCamera();
                };
                myForm.flxSearchOverlay.flxVisual.cmrImageSearch.cameraOptions = {
                    hideControlBar: true,
                    focusMode: constants.CAMERA_FOCUS_MODE_CONTINUOUS
                };
                myForm.flxSearchOverlay.setVisibility(false);
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Camera widget not available.", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchOverlay.flxBarCode.imgBarCode) {
                currentForm = myForm;
                myForm.flxSearchOverlay.flxBarCode.onClick = function() {
                    voiceSearchFlag = false;
                    MVCApp.getScanController().load();
                };
                myForm.flxSearchOverlay.setVisibility(false);
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception on clicking the barcode flex", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchOverlay.flxBarCode.imgBarCode) {
                currentForm = myForm;
                myForm.flxSearchOverlay.flxBarCode.imgBarCode.onTouchEnd = function() {
                    voiceSearchFlag = false;
                    MVCApp.getScanController().load();
                };
                myForm.flxSearchOverlay.setVisibility(false);
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception on clicking the barcode", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.onBeginEditing = function() {
                frmObject = myForm;
                voiceSearchFlag = false;
                MVCApp.searchBar.checkStoreChangeForSearchContent();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Entering The Text in Search Textfield", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxVoiceSearch.flxMicPanel) {
                myForm.flxVoiceSearch.flexBack.onClick = function() {
                    voiceSearchForm = myForm;
                    onXButtonClick(myForm);
                    if (myForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")) {
                        voiceSearchFlag = false;
                        myForm.flxVoiceSearch.setVisibility(false);
                        MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
                        MVCApp.searchBar.showPreviousForm();
                    } else if (frmProductsList.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain")) {
                        kony.print(" No results ...");
                        voiceSearchFlag = false;
                        tapToCancelFlag = false;
                        myForm.flxVoiceSearch.setVisibility(false);
                        MVCApp.searchBar.showPreviousForm();
                        frmProductsList.flxVoiceSearch.setVisibility(false);
                    } else {
                        voiceSearchFlag = true;
                        tapToCancelFlag = true;
                        MVCApp.searchBar.showPreviousForm();
                        deviceBackFlag = true;
                    }
                    entryFormObjectToBrowser = myForm;
                }
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On the click of back in VoiceSearch", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flxSearchOverlay.flxContainerSearchExpand.imgBack.onTouchEnd = function() {
                frmObject = myForm;
                voiceSearchFlag = false;
                frmObject.flxSearchOverlay.setVisibility(false);
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On the click of back in VoiceSearch", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxHeaderWrap.flxImageVoiceSearch) {
                myForm.flxHeaderWrap.flxImageVoiceSearch.onClick = function() {
                    voiceSearchForm = myForm;
                    voiceSearchFlag = true;
                    myForm.flxSearchOverlay.setVisibility(false);
                    previousForm = myForm;
                    runtimepermissions.invokePermission(function() {
                        if (!createVoiceAndSpeechObjectsFlag) {
                            createSingletonsAndroid();
                        }
                        showMicPanel(myForm);
                    });
                    try {
                        if (recordAudioSettings == null || recordAudioSettings == undefined) {
                            recordAudioSettings = new runtimepermissions.permissions();
                            recordAudioSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
                            recordAudioSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.recordaudiopermission"));
                            recordAudioSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
                            recordAudioSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.audioservices"));
                        }
                    } catch (e) {
                        if (e) {
                            kony.print("Exception while setting run time permissions" + e);
                        }
                    }
                    recordAudioSettings.requestPermission("Manifest.permission.RECORD_AUDIO");
                }
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On entering The Voice Search", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchOverlay.flxContainerSearchExpand.flxVoice) {
                myForm.flxSearchOverlay.flxContainerSearchExpand.flxVoice.onClick = function() {
                    voiceSearchForm = myForm;
                    voiceSearchFlag = true;
                    myForm.flxSearchOverlay.setVisibility(false);
                    previousForm = myForm;
                    runtimepermissions.invokePermission(function() {
                        if (!createVoiceAndSpeechObjectsFlag) {
                            createSingletonsAndroid();
                        }
                        showMicPanel(myForm);
                    });
                    try {
                        if (recordAudioSettings == null || recordAudioSettings == undefined) {
                            recordAudioSettings = new runtimepermissions.permissions();
                            recordAudioSettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.gotosettings"));
                            recordAudioSettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.recordaudiopermission"));
                            recordAudioSettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notnow"));
                            recordAudioSettings.setServiceText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.audioservices"));
                        }
                    } catch (e) {
                        if (e) {
                            kony.print("Exception while setting run time permissions" + e);
                        }
                    }
                    recordAudioSettings.requestPermission("Manifest.permission.RECORD_AUDIO");
                }
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On entering The Voice Search from Search Overlay", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchOverlay.flxContainerSearchExpand) {
                myForm.flxSearchOverlay.flxContainerSearchExpand.onClick = function() {
                    voiceSearchFlag = false;
                    myForm.flxSearchOverlay.setVisibility(false);
                    if (myForm.flxSearchResultsContainer) {
                        myForm.flxSearchResultsContainer.setVisibility(false);
                    }
                }
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On exiting The Search Overlay", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxVoiceSearch.flxMicPanel) {
                myForm.flxVoiceSearch.flxMicPanel.onClick = function() {
                    kony.print("in flxMicPanel");
                }
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On on click voice search screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxVoiceSearch.imgMicStatus) {
                myForm.flxVoiceSearch.imgMicStatus.onTouchEnd = function() {
                    if (myForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToCancel")) {
                        tapToCancelFlag = true;
                        voiceSearchForm = myForm;
                        onXButtonClick(voiceSearchForm);
                        MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
                        if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
                            myForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.pleaseWaitMessage");
                            MVCApp.Toolbox.common.dismissLoadingIndicator();
                            MVCApp.Toolbox.common.showLoadingIndicator("");
                        } else {
                            MVCApp.Toolbox.common.dismissLoadingIndicator();
                            myForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
                            myForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable");
                            myForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
                            myForm.flxVoiceSearch.forceLayout();
                        }
                    } else if (myForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain")) {
                        onXButtonClick(myForm);
                        showMicPanel(myForm);
                    }
                }
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On clicking the mic in the voice search screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchResultsContainer) {
                myForm.flxSearchResultsContainer.segSearchOffers.onRowClick = function() {
                    frmObject = myForm;
                    var segData = myForm.flxSearchResultsContainer.segSearchOffers.data;
                    var selectedIndex = myForm.flxSearchResultsContainer.segSearchOffers.selectedRowIndex[1];
                    var contentURL = segData[selectedIndex].c_contentUrl || "";
                    if (contentURL !== "") {
                        MVCApp.Toolbox.common.openApplicationURL(contentURL);
                    }
                };
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Offer in The Search Page", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.onTextChange = function() {
                frmObject = myForm;
                MVCApp.searchBar.determineVisibilityOfClearSearch();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception While Entering The Text in Search Textfield", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.onTouchStart = function() {
                frmObject = myForm;
                voiceSearchFlag = false;
                if (frmObject == frmHome) {
                    frmHome.flexCardAnimate.isVisible = false;
                    if (frmHome.flxHeaderWrap.flxSearchContainer.top != "50dp") {
                        frmHome.flxHeaderWrap.flxSearchContainer.top = "7dp";
                        frmHome.flxHeaderWrap.flxSearchContainer.left = "48dp";
                        frmHome.flxHeaderWrap.flxSearchContainer.right = "53dp";
                        frmHome.flxHeaderWrap.flxSearchContents.left = "0dp";
                        frmHome.flxHeaderWrap.flxSearchContents.right = "0dp";
                        frmHome.flxHeaderWrap.flxSearchContents.skin = "sknFlexBgLtGrayBdr2";
                    }
                    if (frmHome.flxHeaderWrap.flxSearchContents.left == "10dp") {
                        frmHome.flxHeaderWrap.flxSearchContainer.skin = "sknFlexBgD30e2cNoBdr";
                    } else {
                        frmHome.flxHeaderWrap.flxSearchContainer.skin = "slFbox";
                    }
                    kony.print("header top value---" + frmHome.flxHeaderWrap.flxSearchContainer.top);
                }
                if (kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false) {
                    rewardsToggle();
                }
                MVCApp.searchBar.setDatatoSegment();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Starting To Enter Text In To Search Textfield", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxSearchOverlay.flxContainerSearchExpand.btnClearVoiceSearch) {
                myForm.flxSearchOverlay.flxContainerSearchExpand.btnClearVoiceSearch.onClick = function() {
                    frmObject = myForm;
                    MVCApp.searchBar.clearText();
                };
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Clear Button in Search Textfield", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flxHeaderWrap.btnHome.onClick = function() {
                var prevForm = kony.application.getPreviousForm() || "";
                var prevFormId = prevForm.id || "";
                var currentForm = kony.application.getCurrentForm() || "";
                var currentFormId = currentForm.id || "";
                kony.print("prevFormId::currentForm" + prevFormId + " :: " + currentFormId);
                if (prevFormId == "frmWebView" || currentForm == "frmWebView") {
                    frmWebView.brwrView.evaluateJavaScript("window.stop();document.open();document.close();");
                } else if (prevFormId == "frmCartView" || currentFormId == "frmCartView") {
                    frmCartView.brwrView.evaluateJavaScript("window.stop();document.open();document.close();");
                }
                if (voiceSearchFlag) {
                    onXButtonClick(myForm);
                }
                voiceSearchFlag = false;
                if (myForm.flxSearchOverlay.flxContainerSearchExpand) {
                    myForm.flxSearchOverlay.setVisibility(false);
                }
                myForm.flxVoiceSearch.setVisibility(false);
                MVCApp.MoreView().brightnesshandlingfrmMore();
                if (kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false) {
                    rewardsToggle();
                }
                imageSearchOverlayFlag = false;
                frmHome.flxSearchOverlay.setVisibility(false);
                frmHome.flxVoiceSearch.setVisibility(false);
                MVCApp.searchBar.homePageLoadingHandler();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Home Icon Present On The Header", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.onDone = function(eventobject) {
                currentForm = myForm;
                frmObject = myForm;
                voiceSearchFlag = false;
                TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchInitiation, {
                    conversion_category: "App Search",
                    conversion_id: "TextSearch",
                    conversion_action: 1
                });
                MVCApp.searchBar.onQueryEntered(eventobject);
                entryFormObjectToBrowser = myForm;
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Done Button Present On The Search Keyboard", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            //artf5478
            if (myForm.flxFooterWrap.btnCoupon) {
                myForm.flxFooterWrap.btnCoupon.onClick = function() {
                    voiceSearchFlag = false;
                    couponsEntryFromVoiceSearch = false;
                    var storeModeEnabled = false;
                    try {
                        if (writesettings == null || writesettings == undefined) {
                            writesettings = new runtimepermissions.permissions();
                            writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow"));
                            writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
                            writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
                            writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
                            writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                        }
                    } catch (e) {
                        if (e) {
                            kony.print("Exception while setting run time permissions" + e);
                        }
                    }
                    writesettings.requestPermission("android.write_settings.permission");
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                        storeModeEnabled = true;
                    }
                    var region = MVCApp.Toolbox.common.getRegion();
                    var reportData = {
                        "storeModeEnabled": storeModeEnabled,
                        "region": region
                    };
                    MVCApp.sendMetricReport(frmHome, [{
                        "CouponsCLick": JSON.stringify(reportData)
                    }]);
                    MVCApp.getHomeController().loadCoupons();
                    //MVCApp.getCouponListController().load();
                    frmObject = myForm;
                };
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Coupons Icon Present On The Header", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            //artf5478
            if (myForm.flxFooterWrap.btnCouponMain) {
                myForm.flxFooterWrap.btnCouponMain.onClick = function() {
                    voiceSearchFlag = false;
                    couponsEntryFromVoiceSearch = false;
                    if (kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false) {
                        rewardsToggle();
                    }
                    try {
                        if (writesettings == null || writesettings == undefined) {
                            writesettings = new runtimepermissions.permissions();
                            writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow"));
                            writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
                            writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
                            writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
                            writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                        }
                    } catch (e) {
                        if (e) {
                            kony.print("Exception while setting run time permissions" + e);
                        }
                    }
                    writesettings.requestPermission("android.write_settings.permission");
                    MVCApp.getHomeController().loadCoupons();
                    //MVCApp.getCouponListController().load();
                    frmObject = myForm;
                };
            }
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Coupons Button Present On The Header", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            if (myForm.flxHeaderWrap && myForm.flxHeaderWrap.btnCart) {
                myForm.flxHeaderWrap.btnCart.onClick = function() {
                    var cartQuantity = MVCApp.Toolbox.common.getNumberOfItemsInCart() || "0";
                    TealiumManager.trackEvent("Cart Icon Click", {
                        adobe_app_event: "Cart Icon",
                        app_cart_quantity: cartQuantity
                    });
                    MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
                    frmObject = myForm;
                    entryFormObjectToBrowser = myForm;
                };
            }
        } catch (e) {
            kony.print("Exception on Clicking btnCart");
        }
        try {
            myForm.flexCouponContainer.btnDone.onClick = MVCApp.CouponsView().hideCoupons;
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Done Button Present On Coupons Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flexCouponContainer.btnSignIn.onClick = function() {
                fromCoupons = true;
                MVCApp.getSignInController().load();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Sign In Button Present On Coupons Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flexCouponContainer.btnSignUp.onClick = function() {
                fromCoupons = true;
                var userObject = kony.store.getItem("userObj");
                if (userObject === "" || userObject === null) {
                    MVCApp.getSignInController().load();
                } else {
                    MVCApp.getJoinRewardsController().load(userObject);
                }
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Sign Up Button Present On Coupons Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flexCouponContainer.flxCloseBtnWrap.onClick = myForm.flexCouponContainer.flxCouponDetailsPopup.isVisible = false;
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Close Button Present On Coupons Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            myForm.flexCouponContainer.btnPopupClose.onClick = myForm.flexCouponContainer.flxCouponDetailsPopup.isVisible = false;
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking The Close Button Present On Coupons Popup Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
};
MVCApp.searchBar.homePageLoadingHandler = function(backToForeFlag) {
    if (!imageSearchOverlayFlag) { // Added as part of fix for [artf3264]
        var serviceInvocationIntervalInMinutes = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHome.homeIcon.serviceInvocationIntervalInMinutes");
        if (!frmHome.fxlContainer.widgets() || (frmHome.fxlContainer.widgets().length === 0)) {
            getProductsCategory();
        } else if (serviceInvocationIntervalInMinutes && kony.os.isInteger(serviceInvocationIntervalInMinutes)) {
            serviceInvocationIntervalInMinutes = Math.floor(serviceInvocationIntervalInMinutes);
            var newDate = new Date();
            var timeDiff = Math.abs(newDate - gblHomeLoadDate);
            var diffInMinutes = Math.ceil(timeDiff / (1000 * 60));
            if (diffInMinutes > serviceInvocationIntervalInMinutes) {
                gblHomeLoadDate = newDate;
                getProductsCategory();
            } else {
                try {
                    kony.timer.cancel("HomeCarouselTimer");
                } catch (e) {
                    if (e) {
                        TealiumManager.trackEvent("Carousel Timer Exception On Home Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
                var storeId = "";
                if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                    storeId = gblInStoreModeDetails.storeID;
                } else {
                    var storeString = kony.store.getItem("myLocationDetails") || "";
                    if (storeString !== "") {
                        var storeObj = JSON.parse(storeString);
                        storeId = storeObj.clientkey;
                    } else {
                        storeId = "";
                    }
                }
                gblTealiumTagObj.storeid = storeId;
                if (!backToForeFlag) {
                    if (storeId === gblStoreNoForHome) {
                        MVCApp.getHomeController().showDefaultView();
                    } else {
                        MVCApp.getHomeController().load();
                    }
                }
            }
        } else {
            kony.print("i18n.phone.frmHome.homeIcon.serviceInvocationIntervalInMinutes is missing or is not an integer");
            getProductsCategory();
        }
    }
};
MVCApp.searchBar.onQueryEntered = function(eventobject, fromVoiceSearch) {
    var searchQuery = "";
    if (kony.application.getCurrentForm().id !== "frmWebView") {
        kony.print("currentformprint" + kony.application.getCurrentForm().id);
        MVCApp.Toolbox.common.showLoadingIndicator("");
    }
    kony.print("currentformprint" + kony.application.getCurrentForm().id);
    MVCApp.getProductsListController().setFromDeepLink(false);
    if (eventobject) {
        searchQuery = eventobject.text;
        if (searchQuery) searchQuery = searchQuery.trim();
    }
    if (searchQuery !== "") {
        kony.store.setItem("searchedProdId", searchQuery);
        var storeModeEnabled = false;
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeModeEnabled = true;
        }
        var region = MVCApp.Toolbox.common.getRegion();
        var reportData = {
            "storeModeEnabled": storeModeEnabled,
            "searchQuery": searchQuery,
            "region": region
        };
        MVCApp.sendMetricReport(frmHome, [{
            "searchText": JSON.stringify(reportData)
        }]);
        TealiumManager.trackEvent("Text Search", {
            conversion_category: "Text Search",
            conversion_id: "Text Search",
            conversion_action: 2
        });
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.searchString = searchQuery;
        MVCApp.Customer360.sendInteractionEvent("TextSearch", cust360Obj);
        gblIsFromImageSearch = false;
        imageSearchProjectBasedFlag = false;
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            MVCApp.getProductsListWebController().load("search?q=" + searchQuery);
        } else {
            MVCApp.getProductsListController().load(searchQuery, true);
        }
        if (fromVoiceSearch) {
            MVCApp.getProductsListController().setEntryType("VoiceSearch");
        } else {
            MVCApp.getProductsListController().setEntryType("textSearch");
        }
        MVCAppAnalytics.sendCustomMetrics("searchReport", searchQuery, "", "", "");
    } else {
        frmObject.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.text = ""
        MVCApp.Toolbox.common.dismissLoadingIndicator("");
    }
};
MVCApp.searchBar.clearText = function(eventobject) {
    frmObject.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.text = "";
    if (frmObject.flxSearchResultsContainer) {
        frmObject.flxSearchResultsContainer.setVisibility(false);
    }
    frmObject.flxSearchOverlay.flxContainerSearchExpand.btnClearVoiceSearch.setVisibility(false);
};
MVCApp.searchBar.determineVisibilityOfClearSearch = function(eventobject) {
    var text = frmObject.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.text;
    if (text.length > 0) {
        frmObject.flxSearchOverlay.flxContainerSearchExpand.btnClearVoiceSearch.setVisibility(true);
        MVCApp.searchBar.setDatatoSegment();
    } else {
        frmObject.flxSearchOverlay.flxContainerSearchExpand.btnClearVoiceSearch.setVisibility(false);
        if (frmObject.flxSearchResultsContainer) {
            frmObject.flxSearchResultsContainer.setVisibility(false);
        }
    }
    if (frmObject == frmHome) {
        frmHome.flexCardAnimate.isVisible = false;
        if (frmHome.flxSearchResultsContainer && frmHome.flxSearchResultsContainer.isVisible) {
            frmHome.flxHeaderWrap.flxSearchContainer.skin = "sknFlexBgD30e2cNoBdr";
        } else {
            frmHome.flxHeaderWrap.flxSearchContainer.skin = "slFbox";
        }
    }
};
MVCApp.searchBar.setDatatoSegment = function() {
    var searchContentData = kony.store.getItem("searchContent");
    if (searchContentData === null || searchContentData === "") {} else {
        if (frmObject.flxSearchResultsContainer) {
            frmObject.flxSearchResultsContainer.segSearchOffers.widgetDataMap = {
                lblSearchOffer: "name"
            };
            frmObject.flxSearchResultsContainer.segSearchOffers.data = searchContentData;
            frmObject.flxSearchResultsContainer.segSearchResults.data = [];
        }
        if (frmObject.id == "frmHome") {
            if (frmObject.flxHeaderWrap.flxSearchContainer.top != "50dp") {
                //frmObject.flxSearchResultsContainer.flxTopSpacerDNDSearch.height = "50dp";
            }
            frmHome.flexCardAnimate.isVisible = false;
            if (frmHome.flxSearchOverlay.flxContainerSearchExpand) {
                frmHome.flxSearchOverlay.flxContainerSearchExpand.txtSearchField.setFocus(true);
            }
            if (frmHome.flxSearchResultsContainer) {
                frmHome.flxSearchResultsContainer.forceLayout();
            }
        }
    }
};
MVCApp.searchBar.checkStoreChangeForSearchContent = function() {
    var storeId = "";
    var storeString = kony.store.getItem("myLocationDetails") || "";
    if (storeString !== "") {
        var storeObj = JSON.parse(storeString);
        storeId = storeObj.clientkey;
    } else {
        storeId = "";
    }
    if (storeId === glbStoreNoForSearch) {} else {
        glbStoreNoForSearch = storeId;
        var reqParams = {};
        reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
        reqParams.refineString = MVCApp.serviceConstants.searchContentRefineString;
        reqParams.store_id = storeId;
        reqParams.locale = MVCApp.Toolbox.Service.getLocale();
        MakeServiceCall(MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchContent, reqParams, function(results) {
            if (results.opstatus === 0 || results.opstatus === "0") {
                var searchContentValue = results.hits || [];
                kony.store.setItem("searchContent", searchContentValue);
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
        });
    }
};
MVCApp.searchBar.showPreviousForm = function() {
    var searchMessage = currentForm.flxVoiceSearch.lblVoiceSearchHeading.text;
    if (!sorryPageFlag) {
        onXButtonClick(currentForm);
        currentForm.flxVoiceSearch.setVisibility(false);
        currentForm.flxSearchOverlay.setVisibility(false);
    } else {
        var prevForm = "";
        var prevFormObj = kony.application.getPreviousForm();
        prevForm = kony.application.getPreviousForm().id;
        var formController = MVCApp._getController(prevForm.substring(3));
        kony.print("PREV FROM:::" + prevForm);
        if (prevForm == "frmHome") {
            frmHome.flxVoiceSearch.setVisibility(false);
            formController.showDefaultView();
        } else if (prevForm == "frmProductsList") {
            frmProductsList.flxVoiceSearch.setVisibility(false);
            formController.backToScreen();
        } else if (prevForm == "frmProductCategory") {
            frmProductCategory.flxVoiceSearch.setVisibility(false);
            MVCApp.getProductCategoryController().displayPage();
        } else if (prevForm == "frmProjectsCategory") {
            frmProjectsCategory.flxVoiceSearch.setVisibility(false);
            MVCApp.getProjectsCategoryController().displayPage();
        } else if (prevForm == "frmProducts") {
            frmProducts.flxVoiceSearch.setVisibility(false);
            MVCApp.getProductsController().displayPage();
        } else if (prevForm == "frmProjects") {
            frmProjects.flxVoiceSearch.setVisibility(false);
            MVCApp.getProjectsController().displayProjects();
        } else if (prevForm == "frmProjectsList") {
            frmProjectsList.flxVoiceSearch.setVisibility(false);
            MVCApp.getProjectsListController().showUI();
        } else if (prevForm == "frmPDP") {
            frmPDP.flxVoiceSearch.setVisibility(false);
            MVCApp.getPDPController().showUI();
        } else if ((prevForm == "frmPJDP")) {
            frmPJDP.flxVoiceSearch.setVisibility(false);
            frmPJDP.show();
        } else if (prevForm == "frmWeeklyAdHome") {
            frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
            MVCApp.getWeeklyAdHomeController().showUI();
        } else if (prevForm == "frmWeeklyAd") {
            frmWeeklyAd.flxVoiceSearch.setVisibility(false);
            MVCApp.getWeeklyAdController().showUI();
        } else if (prevForm == "frmWeeklyAdDetail") {
            frmWeeklyAdDetail.flxVoiceSearch.setVisibility(false);
            MVCApp.getWeeklyAdDetailController().showUI();
        } else if (prevForm == "frmMore") {
            frmMore.flxVoiceSearch.setVisibility(false);
            MVCApp.getMoreController().load();
        } else if (prevForm == "frmShoppingList") {
            frmShoppingList.flxVoiceSearch.setVisibility(false);
            MVCApp.getShoppingListController().showUI();
        } else if (prevForm == "frmProductCategoryMore") {
            MVCApp.getProductCategoryMoreController().displaySamePage();
        } else if (prevForm == "frmGuide") {
            currentForm.flxVoiceSearch.setVisibility(false);
            MVCApp.getHomeController().showDefaultView();
        } else {
            prevFormObj.show();
        }
    }
    sorryPageFlag = false;
    deviceBackFlag = false;
};
MVCApp.searchBar.showWeeklyAdd = function() {
    if (kony.application.getCurrentForm().id == "frmMore" && toggleRewards === false) {
        rewardsToggle();
    }
    var form = kony.application.getCurrentForm().id;
    var storeModeEnabled = false;
    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
        storeModeEnabled = true;
    }
    var region = MVCApp.Toolbox.common.getRegion();
    var reportData = {
        "storeModeEnabled": storeModeEnabled,
        "formName": form,
        "region": region
    };
    MVCApp.sendMetricReport(frmHome, [{
        "WeeklyAdFromAppMenu": JSON.stringify(reportData)
    }]);
    MVCApp.Toolbox.common.dispFooter();
    frmWeeklyAdHome.flxFooterWrap.bottom = "0dp";
    if (MVCApp.Toolbox.Service.getMyLocationFlag() === "true" || (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled)) {
        MVCApp.Toolbox.common.setTransition(frmWeeklyAdHome, 0);
        loadWeeklyAds = true;
        MVCApp.getWeeklyAdHomeController().load();
    } else {
        var whenStoreNotSet = {
            "doesLoadCouponsWhenStoreNotSet": true,
            "fromWhichScreen": "weeklyAds"
        };
        whenStoreNotSet = JSON.stringify(whenStoreNotSet);
        kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
    }
};