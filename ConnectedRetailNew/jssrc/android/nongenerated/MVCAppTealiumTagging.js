//Type your code here
function initTealiumTagObject() {
    var lTealiumTagObj = {};
    lTealiumTagObj.customer_id = "";
    lTealiumTagObj.page_id = "";
    lTealiumTagObj.customer_email = "";
    lTealiumTagObj.user_anonymous = false;
    lTealiumTagObj.user_authenticated = false;
    lTealiumTagObj.user_registered = false;
    lTealiumTagObj.page_category = "";
    lTealiumTagObj.category_ID = "";
    lTealiumTagObj.firstName = "";
    lTealiumTagObj.lastName = "";
    lTealiumTagObj.app_id = "";
    lTealiumTagObj.platform_version = "";
    lTealiumTagObj.view_height = 0;
    lTealiumTagObj.view_size = "";
    lTealiumTagObj.view_width = 0;
    lTealiumTagObj.storeid = "";
    lTealiumTagObj.loyalty_id = "";
    lTealiumTagObj.instore_mode = false;
    return lTealiumTagObj;
}

function configureTealiumTagObj() {
    setUserDetailsForTealiumTagging(kony.store.getItem("userObj"));
    gblTealiumTagObj.view_width = kony.os.deviceInfo().screenWidth;
    gblTealiumTagObj.view_height = kony.os.deviceInfo().screenHeight;
    gblTealiumTagObj.view_size = kony.os.deviceInfo().screenWidth + " X " + kony.os.deviceInfo().screenHeight;
    gblTealiumTagObj.platform_version = kony.os.deviceInfo().version;
    gblTealiumTagObj.app_id = appConfig.appName + " " + appConfig.appVersion;
    if (kony.store.getItem("ksid")) {
        gblTealiumTagObj.push_subscription_id_kony = kony.store.getItem("ksid");
    }
    var lDeviceId = MVCApp.Toolbox.common.getDeviceID();
    if (lDeviceId) {
        gblTealiumTagObj.device_id = lDeviceId;
    }
    var pushNativeId = kony.store.getItem("pushnativeid");
    if (pushNativeId) {
        gblTealiumTagObj.push_subscription_id_android = pushNativeId;
    }
    TealiumManager.initialize();
}

function setUserDetailsForTealiumTagging(pResponse) {
    if (pResponse) {
        gblTealiumTagObj.user_anonymous = false;
        gblTealiumTagObj.user_authenticated = true;
        gblTealiumTagObj.user_registered = true;
        gblTealiumTagObj.loyalty_id = pResponse.c_loyaltyMemberID;
        gblTealiumTagObj.customer_email = pResponse.email;
        gblTealiumTagObj.firstName = pResponse.first_name;
        gblTealiumTagObj.lastName = pResponse.last_name;
        gblTealiumTagObj.customer_id = pResponse.customer_id;
    } else {
        gblTealiumTagObj.user_anonymous = true;
        gblTealiumTagObj.user_authenticated = false;
        gblTealiumTagObj.user_registered = false;
        gblTealiumTagObj.loyalty_id = "";
        gblTealiumTagObj.customer_email = "";
        gblTealiumTagObj.firstName = "";
        gblTealiumTagObj.lastName = "";
        gblTealiumTagObj.customer_id = kony.os.deviceInfo().deviceid;
    }
}