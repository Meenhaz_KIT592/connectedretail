/**
 * PUBLIC
 * This is the model for formMore form
 */
var MVCApp = MVCApp || {};
MVCApp.MoreModel = (function() {
    var serviceType = "";
    var operationId = "";

    function loadData(callback, requestParams) {
        kony.print("modelMore.js");
        /* MVCApp.makeServiceCall( serviceType,operationId,requestParams,
                                function(results)
                                {
           kony.print("Response is "+JSON.stringify(results)); 
           callback(results,requestParams);
           
         },function(error){
         kony.print("Response is "+JSON.stringify(error));
         }
                               );*/
        callback("");
    }

    function androidAddLoyalityToWallet() {
        var requestParams = {};
        var userObject = kony.store.getItem("userObj");
        if (userObject !== "" || userObject !== null) {
            requestParams.loyaltyBarcode = userObject.c_loyaltyMemberID || "";
            requestParams.loyaltyBarcodeNumber = userObject.c_loyaltyMemberID || "";
            requestParams.titles = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.memberName") + "~" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.memberId");
            requestParams.descriptions = userObject.first_name + " " + userObject.last_name + "~" + userObject.c_loyaltyMemberID;
            requestParams.localDate = new Date().toString();
        }
        MakeServiceCall("Wallet", "addLoyaltyCardToAndroidWallet", requestParams, function(results) {
            if (results.opstatus === 0 || results.opstatus === "0") {
                var androidDeeplinkURL = results.jwtToken || "";
                if (androidDeeplinkURL !== "") {
                    kony.application.openURL(androidDeeplinkURL);
                    var loyaltyID = userObject.c_loyaltyMemberID || "";
                    var objRewardsForAnalytics = {
                        "loyaltyID": loyaltyID,
                        "platform": "android"
                    };
                    objRewardsForAnalytics = JSON.stringify(objRewardsForAnalytics);
                    //KNYMetricsService.sendEvent("Custom",null, "frmMore", "", null, objRewardsForAnalytics);
                    MVCApp.sendMetricReport(frmMore, [{
                        "loyaltyAddedToWallet": objRewardsForAnalytics
                    }]);
                }
            }
        }, function(error) {});
    }

    function iPhoneAddLoyalityToWallet(callback) {
        var requestParams = {};
        var userObject = kony.store.getItem("userObj");
        if (userObject !== "" || userObject !== null) {
            requestParams.loyaltyBarcode = userObject.c_loyaltyMemberID || "";
            requestParams.loyaltyBarcodeNumber = userObject.c_loyaltyMemberID || "";
            requestParams.titles = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.memberName") + "~" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.memberId");
            requestParams.descriptions = userObject.first_name + " " + userObject.last_name;
            requestParams.memberNameLbl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.memberName");
            requestParams.memberIdLbl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.memberId");
            requestParams.termsandconditionsText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSettings.btnTermsandConditions");
            var termsAndConditionsLink = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.walletIphone.termsandconditionsLink");
            requestParams.termsandconditionsValue = encodeURI(termsAndConditionsLink);
            requestParams.localDate = new Date().toString();
            requestParams.user = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
            requestParams.password = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            var loyaltyID = userObject.c_loyaltyMemberID || "";
        }
        MakeServiceCall("Wallet", "addLoyaltyCardToiOSWallet", requestParams, function(results) {
            if (results.opstatus === 0 || results.opstatus === "0") {
                var base64String = results.base64String || "";
                if (base64String !== "") {
                    callback(base64String);
                    var objRewardsForAnalytics = {
                        "loyaltyID": loyaltyID,
                        "platform": "iOS"
                    };
                    objRewardsForAnalytics = JSON.stringify(objRewardsForAnalytics);
                    //KNYMetricsService.sendEvent("Custom",null, "frmMore", "", null, objRewardsForAnalytics);
                    MVCApp.sendMetricReport(frmMore, [{
                        "loyaltyAddedToWallet": objRewardsForAnalytics
                    }]);
                }
            }
        }, function(error) {});
    }
    return {
        loadData: loadData,
        androidAddLoyalityToWallet: androidAddLoyalityToWallet,
        iPhoneAddLoyalityToWallet: iPhoneAddLoyalityToWallet
    };
});