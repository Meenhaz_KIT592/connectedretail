//Type your code here
currentForm = "";
var storeMapFromVoiceSearch = false;
var showStoreLocatorFromVoiceSearch = false;
var weeklyAdEntryFromVoiceSearch = false;

function playSound(name, callback) {
    kony.print("**** in play sound");
    var soundFilePath = NSBundle.mainBundle().pathForResourceOfType(name, 'mp4');
    if (name === "ding") {
        if (typeof(player1) === "undefined") {
            var fileURL = NSURL.fileURLWithPath(soundFilePath);
            player1 = AVAudioPlayer.alloc().initWithContentsOfURLError(fileURL, undefined);
            player1.delegate = playerDelegate;
            kony.print("**** player1 initialized");
        }
        player1.callback = callback;
        if (player1.playing) {
            player1.stop();
        }
        player1.play();
    } else {
        if (typeof(player2) === "undefined") {
            fileURL = NSURL.fileURLWithPath(soundFilePath);
            player2 = AVAudioPlayer.alloc().initWithContentsOfURLError(fileURL, undefined);
            player2.delegate = playerDelegate;
            kony.print("**** player2 initialized");
        }
        player2.callback = callback;
        if (player2.playing) {
            player2.stop();
        }
        player2.play();
    }
}

function firstEntity(entities, entity) {
    if (entities !== null && entities !== "undefined") {
        var val = entities && entities[entity] && Array.isArray(entities[entity]) && entities[entity].length > 0 && entities[entity][0];
        if (!val) {
            return null;
        }
        return val;
    } else {
        return null;
    }
}

function predict(text, cb) {
    kony.print("********** pedicting text : " + text);
    var witRequest = new kony.net.HttpRequest();
    var entity, i;
    witRequest.onReadyStateChange = function() {
        if (this.readyState === constants.HTTP_READY_STATE_DONE) {
            if (this.response) {
                kony.print("********** onReadyStateChange done : ");
                var intent = firstEntity(this.response.entities, 'intent');
                var searchTerm = firstEntity(this.response.entities, 'searchTerms');
                kony.print("********** intent  : " + JSON.stringify(intent));
                kony.print("********** entities  : " + JSON.stringify(this.response.entities));
                kony.print("********** searchTerm  : " + JSON.stringify(searchTerm));
                if (intent) {
                    if (searchTerm) {
                        kony.print("********** search terms  : " + JSON.stringify(this.response.entities.searchTerms));
                        kony.print("In if part::" + intent.value);
                        kony.print("In if part2::" + this.response.entities.searchTerms[0].value);
                        cb(intent.value, this.response.entities.searchTerms[0].value);
                    } else {
                        cb(intent.value, text);
                    }
                } else if (searchTerm) {
                    kony.print("in search terms and no intent");
                    cb("", this.response.entities.searchTerms[0].value);
                } else {
                    kony.print("in no search and no intent");
                    cb("", text);
                }
            } else {
                kony.print("in no search and no intent");
                cb("", text);
            }
        }
    };
    var inputText = encodeURIComponent(text);
    var version = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.wit.version");
    var token = getWitAiToken();
    var url = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.wit.url");
    url = url + version + '&q=' + inputText;
    witRequest.open(constants.HTTP_METHOD_GET, url);
    witRequest.setRequestHeader('Authorization', 'Bearer ' + token);
    witRequest.send();
}

function getWitAiToken() {
    var locale = MVCApp.Toolbox.Service.getLocale();
    if (locale === "fr-CA") {
        return MVCApp.wit.ai.fr.token;
    } else {
        return MVCApp.wit.ai.en.token;
    }
}
var recoText = "";

function partialResultCallback(text) {
    kony.print("************ partialResultCallback *********");
    kony.runOnMainThread(function(text) {
        if (text) {
            recoText = text.trim();
        } else {
            recoText = "";
        }
        kony.print("************ runOnMainThread *********");
        kony.print("partial text : " + text);
        //currentForm.flxVoiceSearch.lblMicStatusText.text = text;
    }, [text]);
}
var michaelsVA = {
    showProjects: false
};

function showMsgWaitAndTakeToStoreSelection(msg) {
    //Write code similar to Coupons
}

function showStoreMapWithAisleFor(searchTerms) {
    var storeId = "";
    var storeString = kony.store.getItem("myLocationDetails") || "";
    if (storeString !== "") {
        var storeObj = JSON.parse(storeString);
        storeId = storeObj.clientkey;
    } else {
        storeId = "";
    }
    var reqParams = {};
    reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
    reqParams.app_Locale = 'en';
    reqParams.store_id = storeId;
    reqParams.q = searchTerms;
    MakeServiceCall('voicesearch', 'getAisleForSearchProduct', reqParams, function(results) {
        if (results.opstatus === 0 || results.opstatus === "0") {
            kony.print("*** Results start ");
            kony.print(JSON.stringify(results));
            kony.print("*** Results end ");
            var inventory = JSON.parse(results.products[0].c_store_inventory);
            var aisleNo = inventory.aisle;
            var productName = results.products[0].name;
            kony.print(" *** Product Name =  " + productName);
            kony.print(" *** aisleNo  =  " + aisleNo);
            var params = {};
            params.aisleNo = aisleNo;
            params.productName = productName;
            params.calloutFlag = true;
            MVCApp.getStoreMapController().load(params);
        }
    }, function(results) {
        kony.print("Error :  Response is " + JSON.stringify(results));
        sorryPageFlag = true;
        currentForm.flxVoiceSearch.lblUtterenace2.text = recoText;
        currentForm.flxVoiceSearch.lblUtterence1.setVisibility(false);
        currentForm.flxVoiceSearch.lblUtterance3.setVisibility(false);
        currentForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
        currentForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.NoResultsFoundForVoiceSearch");
        currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
        currentForm.flxVoiceSearch.forceLayout();
        //currentForm.flxVoiceSearch.setVisibility(true);
    });
}

function finalCallback(text) {
    kony.runOnMainThread(function(text) {
        kony.print("finalCallback  text : " + recoText + "TEXT::" + text);
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.expression = recoText || "";
        MVCApp.Customer360.sendInteractionEvent("VoiceSearchUtterance", cust360Obj);
        //Tealium code for voice Search
        MVCApp.Toolbox.common.setIntentForAnalytics("");
        MVCApp.Toolbox.common.setSearchTermForAnalytics("");
        // currentForm.flxVoiceSearch.lblMicStatusText.text = recoText;
        currentForm.flxVoiceSearch.lblUtterenace2.text = recoText.length > 50 ? recoText.substr(0, 47) + "..." : recoText;
        currentForm.flxVoiceSearch.lblUtterence1.text = "";
        currentForm.flxVoiceSearch.lblUtterance3.text = "";
        playSoundAndroid("dong");
        if (bCancel) {
            return;
        }
        kony.print("onTextReco playSound callback got called 2");
        currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToCancel");
        currentForm.flxVoiceSearch.lblUtterence1.text = "";
        currentForm.flxVoiceSearch.lblUtterance3.text = "";
        currentForm.flxVoiceSearch.lblUtterenace2.text = recoText.length > 50 ? recoText.substr(0, 47) + "..." : recoText;
        predict(recoText, function(intent, searchTerms) {
            MVCApp.Toolbox.common.setIntentForAnalytics(intent);
            MVCApp.Toolbox.common.setSearchTermForAnalytics(searchTerms);
            switch (intent) {
                case 'showAisle':
                    //alert("IN SHOW AISLE");
                    kony.print("************ showAisle *********");
                    storeMapFromVoiceSearch = true;
                    frmObject = currentForm;
                    kony.print(" location flag  : " + MVCApp.Toolbox.Service.getMyLocationFlag());
                    kony.print(" location details  : " + kony.store.getItem("myLocationDetails") + "  *****end");
                    MVCApp.getStoreMapController().setSearchTerm(searchTerms);
                    animateVoiceSearch(currentForm.flxVoiceSearch, false, function() {
                        MVCApp.getStoreMapController().setEntryPoint("VoiceSearch");
                        MVCApp.getStoreMapController().showStoreMapWithAisleFor();
                    });
                    // tapToCancelFlag = false;
                    return;
                case 'showMyStore':
                    //alert("IN showMyStore");
                    storeMapFromVoiceSearch = true;
                    frmObject = currentForm;
                    kony.print("************ showMyStore *********");
                    animateVoiceSearch(currentForm.flxVoiceSearch, false, function() {
                        var params = {};
                        params.aisleNo = "";
                        params.productName = "";
                        params.calloutFlag = false;
                        MVCApp.sendMetricReport(frmHome, [{
                            "storemap": "click"
                        }]);
                        MVCApp.getStoreMapController().setEntryPoint("VoiceSearch");
                        MVCApp.getStoreMapController().load(params);
                    });
                    return;
            }
            kony.print("************ Search Terms : " + searchTerms);
            //alert("INTENT::"+intent + "Search Terms "+searchTerms);
            animateVoiceSearch(currentForm.flxVoiceSearch, false, function() {
                switch (intent) {
                    case 'showProducts':
                        //alert("in showProducts");
                        kony.print("************ showProducts *********");
                        //currentForm.flxHeaderWrap.textSearch.text = searchTerms;
                        MVCApp.searchBar.onQueryEntered({
                            text: searchTerms
                        }, true);
                        break;
                    case 'showProjects':
                        //alert("in showProjects");
                        kony.print("************ showProjects ********* " + searchTerms);
                        //currentForm.flxHeaderWrap.textSearch.text = searchTerms;
                        MVCApp.getProjectsListController().setQueryStringForSearch(searchTerms);
                        MVCApp.getProjectsListController().loadFromVoiceSearch(searchTerms, "");
                        break;
                    case 'showWeeklyad':
                        kony.print("************ Weekly Add ********* " + searchTerms);
                        currentForm.flxHeaderWrap.textSearch.text = searchTerms;
                        weeklyAdEntryFromVoiceSearch = true;
                        MVCApp.searchBar.showWeeklyAdd();
                        break;
                    case 'showCoupons':
                        kony.print("************ Coupons ********* " + searchTerms);
                        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                            storeModeEnabled = true;
                            try {
                                if (writesettings == null || writesettings == undefined) {
                                    writesettings = new runtimepermissions.permissions();
                                    writesettings.setOktext(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.allow"));
                                    writesettings.setMessage(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermission"));
                                    writesettings.setCancelText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.deny"));
                                    writesettings.setDenyText(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.writesettingpermissiondeny"));
                                    writesettings.setDenyButtonText(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                                }
                            } catch (e) {
                                if (e) {
                                    kony.print("Exception while setting run time permissions" + e);
                                }
                            }
                            writesettings.requestPermission("android.write_settings.permission");
                        }
                        frmObject = kony.application.getCurrentForm();
                        couponsEntryFromVoiceSearch = true;
                        MVCApp.getHomeController().loadCoupons();
                        break;
                    case 'showChannel':
                        kony.print("************ showChannel *********");
                        showYoutubeVideosInBrowser("", true);
                        break;
                    case 'showVideos':
                        //alert("showChannel");
                        kony.print("************ showVideos *********");
                        showYoutubeVideosInBrowser(searchTerms, false);
                        break;
                    case 'showStores':
                        //alert("showStores");
                        kony.print("************ showStores *********");
                        myLocationString = kony.store.getItem("myLocationDetails")
                        kony.print("Store details : " + myLocationString);
                        showStoreLocatorFromVoiceSearch = true;
                        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                        cust360Obj.entryPoint = "VoiceSearch";
                        MVCApp.Customer360.sendInteractionEvent("ChooseStore", cust360Obj);
                        MVCApp.Customer360.callVoiceSearchResult("success", "", "StoreLocator");
                        //MVCApp.getLocatorMapController().loadCheckNearby(frmHome);
                        MVCApp.getLocatorMapController().load(currentForm.id || "");
                        break;
                    default:
                        // alert("in DEFAULT:::");
                        /* if(recoText.includes("*")){
                           onXButtonClick(currentForm);
                           currentForm.flxVoiceSearch.imgMicStatus.src = "microphone_btn.png";
                           currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
                           currentForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingTwo");
                           currentForm.flxVoiceSearch.lblUtterence1.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceOne");
                           currentForm.flxVoiceSearch.lblUtterance3.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceThree");
                           currentForm.flxVoiceSearch.lblUtterenace2.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceTwo");
                           currentForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.someThing");
                           currentForm.flxVoiceSearch.lblUtterence1.setVisibility(true);
                           currentForm.flxVoiceSearch.lblUtterance3.setVisibility(true);
                         }
                         else{*/
                        kony.print("************ DEFAULT CASE *********");
                        //currentForm.flxHeaderWrap.textSearch.text = recoText;
                        MVCApp.searchBar.onQueryEntered({
                            text: recoText
                        }, true);
                        // }
                }
            });
        });
    }, [text]);
}

function showYoutubeVideos(text) {
    kony.print("********** Searching youtube videos for  : " + text);
    var youtubeRequest = new kony.net.HttpRequest();
    youtubeRequest.onReadyStateChange = function() {
        if (this.readyState === constants.HTTP_READY_STATE_DONE) {
            kony.print("********** Youtube search onReadyStateChange done : ");
            kony.print("********** youtube response  : " + JSON.stringify(this.response));
            parseYoutubeResponse(this.response);
        }
    };
    var inputText = encodeURIComponent(text);
    youtubeRequest.open(constants.HTTP_METHOD_GET, 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC6bJQnCG4sGeffyTY2oqQoQ&maxResults=5&pageToken=&key=AIzaSyDYcnVi5fP4lFF4IFKX34kX-m8LfMVMNjU&q=' + inputText);
    youtubeRequest.send();
}

function showYoutubeVideosInBrowser(text, showChannel) {
    var requestURL;
    var inputText = text.split(' ').join('+');
    if (!showChannel) {
        if (text != null && text.length > 0) {
            requestURL = {
                "URL": "https://www.youtube.com/user/MichaelsStores/search?query=" + inputText,
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            };
        } else {
            requestURL = {
                "URL": "https://www.youtube.com/user/MichaelsStores/videos",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            };
        }
    } else {
        requestURL = {
            "URL": "https://www.youtube.com/user/MichaelsStores/videos",
            "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
        };
    }
    //kony.print("******** requstURL : " + JSON.stringify(requestURL));
    kony.print("******** requstURL : " + requestURL.URL);
    //currentForm.flxVoiceSearch.setVisibility(false);
    MVCApp.getVoiceSearchVideosDetailsController().load(requestURL);
}

function onYoutubeVideosInBrowserClose() {
    animateFlex(frmHome.flxYoutubeResults, false, function() {
        frmHome.browserYoutubeResults.evaluateJavaScript("document.open();document.close();");
    });
}

function parseYoutubeResponse(response) {
    var dataSet = [];
    for (var i = 0; i < response.items.length; i++) {
        var data = {};
        data.lblTitle = response.items[i].snippet.title;
        data.lblDescription = response.items[i].snippet.description;
        data.imgThumbnail = response.items[i].snippet.thumbnails.default.url;
        data.videoId = (response.items[i].id.videoId) ? response.items[i].id.videoId : "";
        dataSet.push(data);
    }
    kony.print("************ Dataset for youtube videos segment");
    kony.print(JSON.stringify(dataSet));
    onShowYoutubeResultsPanel(dataSet);
}

function onsegYouTubeRowClick() {
    var selectedIndex = frmHome.segYoutube.selectedRowIndex[1];
    var segData = frmHome.segYoutube.data;
    var selectedData = segData[selectedIndex];
    kony.print("******* Selected Row Data : " + JSON.stringify(selectedData));
    var requestURL = {
        URL: "https://www.youtube.com/watch?v=" + selectedData.videoId
    };
    kony.print("******** requstURL : " + JSON.stringify(requestURL))
    frmHome.browserYoutube.requestURLConfig = requestURL;
    frmHome.flexYoutubeBrowerHolder.isVisible = true;
    kony.print("***** URL set");
}

function onYoutubeBrowserClose() {
    frmHome.flexYoutubeBrowerHolder.isVisible = false;
    var requestURL = {
        url: "https://www.youtube.com/",
        requestmethod: constants.BROWSER_REQUEST_METHOD_GET
    };
    frmHome.browserYoutube.requesturlconfig = requestURL;
}

function onShowYoutubeResultsPanel(dataSet) {
    animateFlex(frmHome.flxResultsPanel, true, function() {
        frmHome.segYoutube.setData(dataSet);
    });
}

function onHideYoutubeResultsPanel() {
    animateFlex(frmHome.flxResultsPanel, false);
}

function canStopCallback() {}

function resetMicPanelContent(currentForm) {
    currentForm.flxVoiceSearch.imgMicStatus.src = "microphone_btn.png";
    currentForm.flxVoiceSearch.lblMicStatusText.text = "";
    currentForm.flxVoiceSearch.lblUtterence1.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceOne");
    currentForm.flxVoiceSearch.lblUtterenace2.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceTwo");
    currentForm.flxVoiceSearch.lblUtterance3.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceThree");
    bCancel = false;
}

function showMicPanel(formObject) {
    currentForm = formObject;
    kony.print("CURRENT FROM::" + currentForm.id);
    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
    MVCApp.Customer360.sendInteractionEvent("VoiceSearchInitialise", cust360Obj);
    TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchInitiation, {
        conversion_category: "App Search",
        conversion_id: "VoiceSearch",
        conversion_action: 1
    });
    resetMicPanelContent(currentForm);
    //alert("FORM NAME::"+currentForm.id);
    kony.runOnMainThread(function() {
        kony.print("************ Starting Speech Reco *********");
        kony.print("before playing ding");
        currentForm.flxVoiceSearch.imgMicStatus.src = "microphone_btn.png";
        currentForm.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening");
        currentForm.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingOne");
        currentForm.flxVoiceSearch.lblUtterence1.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceOne");
        currentForm.flxVoiceSearch.lblUtterance3.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceThree");
        currentForm.flxVoiceSearch.lblUtterenace2.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.utterenceTwo");
        currentForm.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.someThing");
        var locale = kony.store.getItem("languageSelected");
        if (locale == "fr_CA") {
            currentForm.flxVoiceSearch.flxSampleUtterances.width = "320dp";
        } else {
            currentForm.flxVoiceSearch.flxSampleUtterances.width = "220dp";
        }
        currentForm.flxVoiceSearch.lblUtterence1.setVisibility(true);
        currentForm.flxVoiceSearch.lblUtterance3.setVisibility(true);
        currentForm.flxVoiceSearch.setVisibility(true);
        //currentForm.show();
        kony.runOnMainThread(function() {
            playSoundAndroid("ding");
        }, []);
    }, []);
    // animateVoiceSearch(frmHome.flxMicPanel,true,function(){
    // });
}

function changeContentToCouldntHere() {}

function changeContentToHelpVerbiages() {}
bCancel = false;

function onXButtonClick(formObject) {
    bCancel = true;
    var formToStopListen = formObject;
    animateVoiceSearch(formToStopListen.flxVoiceSearch, false, function() {
        kony.runOnMainThread(function() {
            stopRecognition();
        }, []);
    });
}
/*function animateVoiceSearch(widget,bShow,callback){
  if(bShow){
   /* widget.animate(
          kony.ui.createAnimation({
          "100":{"right":"0%", "stepConfig":{"timingFunction":kony.anim.EASE}}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
          {animationEnd: function() {
            if(callback){
          		callback();
            }
          }
        });
        currentForm.flxVoiceSearch.setVisibility(true);
  }else{
    /*widget.animate(
      kony.ui.createAnimation({
      "100":{"right":"-100%", "stepConfig":{"timingFunction":kony.anim.EASE}}}),
      {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
      {animationEnd: function() {
            if(callback){
          		callback();
            }
          }
        }
    );
    currentForm.flxVoiceSearch.setVisibility(false);
  }
}*/
function animateFlex(widget, bShow, callback) {
    var position0 = {};
    var position100 = {};
    if (bShow) {
        widget.isVisible = true;
        position0 = {
            //"top": "100%"
            opacity: 0
        };
        position100 = {
            // "top": "0%"
            opacity: 1
        };
    } else {
        position0 = {
            //"top": "0%"
            opacity: 1
        };
        position100 = {
            opacity: 0
                // "top": "100%"
        };
    }
    var animDefinition = {
        0: position0,
        100: position100
    };
    var animConfig = {
        "duration": 0.3,
        "iterationCount": 1,
        "delay": 0,
        "direction": kony.anim.DIRECTION_NONE,
        "fillMode": kony.anim.FILL_MODE_BOTH
    };
    var animationObject = kony.ui.createAnimation(animDefinition);
    //alert("starting anim");
    widget.animate(animationObject, animConfig, {
        animationStart: function() {
            if (bShow) {
                //alert("anim starting");
                widget.isVisible = true;
            }
        },
        animationEnd: function() {
            if (!bShow) {
                widget.isVisible = false;
            }
            if (callback) {
                callback();
            }
        }
    });
}

function animateVoiceSearch(widget, bShow, callback) {
    if (bShow) {
        widget.animate(kony.ui.createAnimation({
            "100": {
                "right": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            animationEnd: function() {
                if (callback) {
                    callback();
                }
            }
        });
        //currentForm.flxVoiceSearch.setVisibility(true);
    } else {
        widget.animate(kony.ui.createAnimation({
            "100": {
                "right": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            animationEnd: function() {
                if (callback) {
                    callback();
                }
            }
        });
        // currentForm.flxVoiceSearch.setVisibility(false);
    }
}