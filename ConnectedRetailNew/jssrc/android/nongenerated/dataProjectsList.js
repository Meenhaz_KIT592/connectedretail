//Type your code here
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.ProjectsList = (function(json) {
    var _data = json || {};

    function setData(json) {
        _data = json;
    }

    function getData() {
        return _data;
    }

    function getRefineMentsForProjects() {
        var refinements = _data.refinements || [];
        for (var i = 0; i < refinements.length; i++) {
            var temp = refinements[i];
            var tempAttr = temp.attribute_id;
            if (tempAttr === "c_craftTime" || tempAttr === "c_ecomCraftTime") {
                var refValuesCraft = temp.values || [];
                MVCApp.getSortProjectController().setCraftLabel(temp.label);
                MVCApp.getSortProjectController().setCraftValues(refValuesCraft);
            } else if (tempAttr === "c_skillLevel" || tempAttr === "c_projectSkillLevelAdult ") {
                var refValuesSkill = temp.values || [];
                MVCApp.getSortProjectController().setSkillLabel(temp.label);
                MVCApp.getSortProjectController().setSkillValues(refValuesSkill);
            }
        }
        var value = _data.selected_refinements || "";
        var selObj = {};
        if (value !== "") {
            try {
                selObj = JSON.parse(value);
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "dataProjectsList.js on getRefinementsForProjects for value:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("Value Parse Exception in PJLP Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        var countOfRef = 0;
        for (var key in selObj) {
            if (key !== "cgid") {
                var refString = selObj[key];
                var refArr = refString.split("|") || [];
                countOfRef = countOfRef + refArr.length;
                //countOfRef++;
            }
        }
        MVCApp.getProjectsListController().setRefineCount(countOfRef);
        MVCApp.getSortProjectController().setSelectedRefinements(value);
    }

    function getDataForProjects() {
        var projectResults = _data.hits || {};
        var projectResultsArr = [];
        var dataForProjArr = [];
        var projectIds = [];
        if (MVCApp.Toolbox.common.isArray(projectResults)) {
            projectResultsArr = projectResults;
        } else {
            projectResultsArr.push(projectResults);
        }
        for (var i = 0; i < projectResultsArr.length; i++) {
            var temp = projectResultsArr[i];
            if (gblIsFromImageSearch) {
                projectIds.push(temp.id || "");
            } else {
                projectIds.push(temp.product_id || "");
            }
            var skillLevel = temp.c_skillLevel || "";
            temp.flxDurationAndSkill = {
                isVisible: true
            };
            if (skillLevel.length < 4) {
                temp.c_skillLevel = "";
            }
            temp.skillLevel = MVCApp.Toolbox.common.setSkillLevel(skillLevel)
            var creftTime = temp.c_ecomCraftTime || "";
            temp.creftTime = MVCApp.Toolbox.common.setDuration(creftTime);
            if (skillLevel == "") {
                temp.skillLevel = "";
            }
            if (creftTime == "") {
                temp.creftTime = "";
            }
            if (skillLevel == "" && creftTime == "") {
                temp.flxDurationAndSkill = {
                    isVisible: false
                };
            } else {
                temp.flxDurationAndSkill = {
                    isVisible: true
                };
            }
            var isMaster = "false";
            var productType = temp.product_type || {};
            var master = productType.master || "false";
            var imageSrc = "";
            var tempImage = temp.image || "";
            if (master == "false") {
                if (tempImage !== "") {
                    try {
                        if (gblIsFromImageSearch) {
                            imageSrc = temp.image.link + "&fit=inside%7C220%3A220";
                        } else {
                            imageSrc = JSON.parse(temp.image).link + "&fit=inside%7C220%3A220";
                        }
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "dataProjectsList.js on getDataForProjects for temp.image:" + JSON.stringify(e)
                        }]);
                        imageSrc = temp.c_default_image || "";
                        if (e) {
                            TealiumManager.trackEvent("Image Parse Exception in PJLP Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                } else {
                    imageSrc = temp.c_default_image || "";
                }
            } else {
                imageSrc = temp.c_default_image;
            }
            temp.picProject = imageSrc;
            dataForProjArr.push(temp);
            if (i === 0) {
                var prodCount = temp.c_other_count || 0;
                MVCApp.getProjectsListController().setProductCount(prodCount);
                MVCApp.getSortProjectController().setProductCount(prodCount);
            }
        }
        getRefineMentsForProjects();
        MVCApp.getProjectsListController().setProjectIds(projectIds);
        return dataForProjArr;
    }

    function getTotalResults() {
        return _data.total || 0;
    }

    function getStart() {
        return _data.start || 0;
    }
    return {
        setData: setData,
        getData: getData,
        getDataForProjects: getDataForProjects,
        getTotalResults: getTotalResults,
        getStart: getStart
    };
});