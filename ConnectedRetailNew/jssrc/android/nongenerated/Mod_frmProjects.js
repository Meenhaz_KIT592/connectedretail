//Type your code here
function tapProjects() {
    frmProjects.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmProjects.flxFooterWrap.flxProjects.skin = 'sknFlexBgBlackOpaque10';
    frmProjects.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmProjects.flxFooterWrap.flxEvents.skin = 'slFbox';
    frmProjects.flxFooterWrap.flxMore.skin = 'slFbox';
}

function pre_frmProjects() {
    searchBlur();
    tapProjects();
    animateProjectsDepartments();
    frmProjects.flxHeaderWrap.btnHome.isVisible = true;
    frmProjects.flxHeaderWrap.textSearch.text = '';
}

function NAV_frmProjects() {
    frmProjects.show();
    pre_frmProjects();
}

function animateProjectsDepartments() {
    function test_CALLBACK() {}

    function anim_flxProjectDepartments() {
        //alert('hi');
        frmProjects.flxDepartment1.isVisible = true;
        frmProjects.flxDepartment2.isVisible = true;
        frmProjects.flxDepartment3.isVisible = true;
        frmProjects.flxDepartment4.isVisible = true;
        frmProjects.flxDepartment5.isVisible = true;
        frmProjects.flxDepartment6.isVisible = true;
        frmProjects.flxDepartment7.isVisible = true;
        var trans101 = kony.ui.makeAffineTransform();
        trans101.scale(1, 1);
        frmProjects.flxDepartment1.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.5,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
        frmProjects.flxDepartment2.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.6,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
        frmProjects.flxDepartment3.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.7,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
        frmProjects.flxDepartment4.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.8,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
        frmProjects.flxDepartment5.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.9,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
        frmProjects.flxDepartment6.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 1.0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
        frmProjects.flxDepartment7.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 1.1,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": test_CALLBACK
        });
    }
    frmProjects.flxDepartment1.isVisible = false;
    frmProjects.flxDepartment2.isVisible = false;
    frmProjects.flxDepartment3.isVisible = false;
    frmProjects.flxDepartment4.isVisible = false;
    frmProjects.flxDepartment5.isVisible = false;
    frmProjects.flxDepartment6.isVisible = false;
    frmProjects.flxDepartment7.isVisible = false;
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(1, 0.01);
    frmProjects.flxDepartment1.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": test_CALLBACK
    });
    frmProjects.flxDepartment2.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": test_CALLBACK
    });
    frmProjects.flxDepartment3.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": test_CALLBACK
    });
    frmProjects.flxDepartment4.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": test_CALLBACK
    });
    frmProjects.flxDepartment5.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": test_CALLBACK
    });
    frmProjects.flxDepartment6.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": test_CALLBACK
    });
    frmProjects.flxDepartment7.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": anim_flxProjectDepartments
    });
}