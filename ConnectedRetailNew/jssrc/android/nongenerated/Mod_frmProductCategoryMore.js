//Type your code here
function tapProductsCategoryMore() {
    frmProductCategoryMore.flxFooterWrap.flxProducts.skin = 'sknFlexBgBlackOpaque10';
    frmProductCategoryMore.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmProductCategoryMore.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmProductCategoryMore.flxFooterWrap.flxEvents.skin = 'slFbox';
    frmProductCategoryMore.flxFooterWrap.flxMore.skin = 'slFbox';
}

function pre_frmProductCategoryMore() {
    searchBlur();
    frmProductCategoryMore.flxHeaderWrap.btnHome.isVisible = true;
    frmProductCategoryMore.flxHeaderWrap.textSearch.text = '';
}

function NAV_frmProductCategoryMore() {
    frmProductCategoryMore.show();
    pre_frmProductCategoryMore();
    tapProductsCategoryMore();
}