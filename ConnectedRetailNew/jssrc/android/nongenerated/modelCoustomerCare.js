/**
 * PUBLIC
 * This is the model for CustomerCare form
 */
var MVCApp = MVCApp || {};
MVCApp.CustomerCareModel = (function() {
    function setCustomerCareDetails(callback) {
        var formFields = {};
        formFields.firstName = "";
        formFields.lastName = "";
        formFields.email = "";
        formFields.phone = "";
        formFields.store = "";
        var userObject = kony.store.getItem("userObj") || "";
        if (userObject !== "") {
            formFields.firstName = userObject.first_name || "";
            formFields.lastName = userObject.last_name || "";
            formFields.email = userObject.email || "";
            formFields.phone = userObject.phone || "";
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (storeString !== "") {
                //logic to be implemented if store is selected
                try {
                    var store = JSON.parse(kony.store.getItem("myLocationDetails")).address2 || "";
                    formFields.store = store;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "modelCoustomerCare.js on setCustomerCareDetails for myLocationDetails:" + JSON.stringify(e)
                    }]);
                    storeString = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception in Customer Care Service Call Flow", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            }
        }
        callback(formFields);
    }

    function onSubmit(formFields) {
        //need to validate the form fields and show appropriate error message 
        // if it clears all validations then make a service call 
    }
    return {
        setCustomerCareDetails: setCustomerCareDetails,
        onSubmit: onSubmit
    };
});