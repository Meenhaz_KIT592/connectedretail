/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.VoiceSearchVideosView = (function() {
    /**
     * PUBLIC
     * Open the Events form
     */
    function show() {}

    function bindEvents() {
        frmVoiceSearchVideos.btnCross.onClick = onCloseOfVideo;
        frmVoiceSearchVideos.preShow = function() {
            frmVoiceSearchVideos.browserYoutubeResults.clearHistory();
            frmVoiceSearchVideos.browserYoutubeResults.enableCache = true;
        }
        frmVoiceSearchVideos.flexBack.onClick = onClickOfBack;
        frmVoiceSearchVideos.flexForward.onClick = onClickOfForward;
    }

    function onClickOfBack() {
        var canGoBack = frmVoiceSearchVideos.browserYoutubeResults.canGoBack();
        if (canGoBack) {
            frmVoiceSearchVideos.browserYoutubeResults.goBack();
        }
    }

    function onClickOfForward() {
        var canGoForward = frmVoiceSearchVideos.browserYoutubeResults.canGoForward();
        if (canGoForward) {
            frmVoiceSearchVideos.browserYoutubeResults.goForward();
        }
    }

    function updateScreen(urlToBeopened) {
        var requestURL = urlToBeopened;
        frmVoiceSearchVideos.browserYoutubeResults.evaluateJavaScript("document.open();document.close();");
        frmVoiceSearchVideos.browserYoutubeResults.requestURLConfig = requestURL;
        frmVoiceSearchVideos.browserYoutubeResults.playVideoInFullScreen = true;
        currentForm.flxVoiceSearch.setVisibility(false);
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.entryType = "VoiceSearch";
        MVCApp.Customer360.sendInteractionEvent("YoutubeVideos", cust360Obj);
        MVCApp.Customer360.callVoiceSearchResult("success", "", "YoutubeVideos");
        frmVoiceSearchVideos.show();
    }

    function onCloseOfVideo() {
        showPreviousForm();
        frmVoiceSearchVideos.browserYoutubeResults.enableCache = false;
        frmVoiceSearchVideos.browserYoutubeResults.evaluateJavaScript("document.open();document.close();");
    }

    function showPreviousForm() {
        var frmObj = kony.application.getPreviousForm();
        var prevForm = "";
        if (frmObj != null) {
            prevForm = kony.application.getPreviousForm().id;
        } else {
            prevForm = kony.application.getCurrentForm().id;
        }
        var formController = MVCApp._getController(prevForm.substring(3));
        if (formController) {
            if (prevForm == "frmHome") {
                formController.showDefaultView();
            } else if (prevForm == "frmProductsList") {
                formController.backToScreen();
            } else if ((prevForm == "frmProductCategory") || (prevForm == "frmProjectsCategory") || (prevForm == "frmProducts")) {
                formController.displayPage();
            } else if (prevForm == "frmProjects") {
                formController.displayProjects();
            } else if ((prevForm == "frmProjectsList") || (prevForm == "frmPDP")) {
                formController.showUI();
            } else if ((prevForm == "frmPJDP")) {
                frmPJDP.show();
            } else if (prevForm == "frmWeeklyAdHome") {
                frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
                MVCApp.getWeeklyAdHomeController().showUI();
            } else if (prevForm == "frmWeeklyAd") {
                frmWeeklyAd.flxVoiceSearch.setVisibility(false);
                MVCApp.getWeeklyAdController().showUI();
            } else if (prevForm == "frmWeeklyAdDetail") {
                frmWeeklyAdDetail.flxVoiceSearch.setVisibility(false);
                MVCApp.getWeeklyAdDetailController().showUI();
            } else if (prevForm == "frmMore") {
                frmMore.flxVoiceSearch.setVisibility(false);
                MVCApp.getMoreController().load();
            } else if (prevForm == "frmShoppingList") {
                frmShoppingList.flxVoiceSearch.setVisibility(false);
                MVCApp.getShoppingListController().showUI();
            } else if (prevForm == "frmProductCategoryMore") {
                MVCApp.getProductCategoryMoreController().displaySamePage();
            } else {
                formController.show();
            }
        } else {
            frmObj.show();
        }
    }
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        showPreviousForm: showPreviousForm
    };
});