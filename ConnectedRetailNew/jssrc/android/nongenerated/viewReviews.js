/**
 * PUBLIC
 * This is the view for the Products form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
var gblUserKey = "";
var gblNickKeyPair = [];
var retainReviewFields = false;
MVCApp.ReviewsView = (function() {
    var toggleReviewsFlag = true;
    var rating = 0;
    var deviceFp = "";
    /**
     * PUBLIC
     * Open the Products form
     */
    /**
     * @function
     *
     */
    function setRatingsToGray() {
        for (var i = 1; i < 6; i++) {
            frmReviews["lblRating" + i].skin = "sknLblIconStarLrg";
        }
    }

    function clearAllFields() {
        clearTextEmail();
        clearTextLocation();
        clearTextNickName();
        clearTexttitle();
        clearReview(); //frmReviews.txtReview.text="";
        setRatingsToGray();
        setRatingText(0);
    }

    function show() {
        toggleReviewsFlag = true;
        var fromPDPFlag = MVCApp.getReviewsController().getFromPDP();
        if (fromPDPFlag) {
            frmReviews.lstbxReviewSort.selectedKey = "hghtolw";
        } else {}
        frmReviews.flxWriteReviewWrap.contentOffset = {
            x: 0,
            y: 0
        };
        kony.print("retainReviewFields---" + retainReviewFields);
        if (!retainReviewFields) {
            var transformObj = kony.ui.makeAffineTransform();
            transformObj.rotate(0);
            frmReviews.lblDropdown.transform = transformObj;
            frmReviews.imgCheckAgree.src = "checkbox_off.png"
            frmReviews.flxWriteReviewDetails.setVisibility(false);
            frmReviews.lblDropdown.text = "D";
            frmReviews.lblRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.clickToRate");
            frmReviews.imgRating.setVisibility(false);
            frmReviews.imgRating.src = "checkbox_off.png";
            clearAllFields();
            MVCApp.Toolbox.common.setTransition(frmMore, 3);
        }
        frmReviews.show();
        var reviewText = MVCApp.getReviewsController().getReviewText();
        if (reviewText == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.btnWriteAReview")) {
            frmReviews.flxCollapsible.setVisibility(false);
            showReviewsDetails();
        } else {
            frmReviews.flxCollapsible.setVisibility(true);
        }
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function clearTextEmail() {
        frmReviews.txtEmail.text = "";
        frmReviews.btnClearEmail.setVisibility(false);
    }

    function clearTextLocation() {
        frmReviews.txtLocation.text = "";
        frmReviews.btnClearLocation.setVisibility(false);
    }

    function clearTextNickName() {
        frmReviews.txtNickName.text = "";
        frmReviews.btnClearNickName.setVisibility(false);
    }

    function clearTexttitle() {
        frmReviews.txtReviewTitle.text = "";
        frmReviews.btnClearFirstName.setVisibility(false);
    }

    function txChangeFirstName() {
        var text = frmReviews.txtReviewTitle.text;
        if (text.length > 0) {
            frmReviews.btnClearFirstName.setVisibility(true);
        } else {
            frmReviews.btnClearFirstName.setVisibility(false);
        }
    }

    function txChangeEmail() {
        var text = frmReviews.txtEmail.text;
        if (text.length > 0) {
            frmReviews.btnClearEmail.setVisibility(true);
        } else {
            frmReviews.btnClearEmail.setVisibility(false);
        }
    }

    function txChangeLocation() {
        var text = frmReviews.txtLocation.text;
        if (text.length > 0) {
            frmReviews.btnClearLocation.setVisibility(true);
        } else {
            frmReviews.btnClearLocation.setVisibility(false);
        }
    }

    function txChangeNickName() {
        var text = frmReviews.txtNickName.text;
        if (text.length > 0) {
            frmReviews.btnClearNickName.setVisibility(true);
        } else {
            frmReviews.btnClearNickName.setVisibility(false);
        }
    }

    function taClearReview() {
        var text = frmReviews.txtReview.text;
    }

    function clearReview() {
        frmReviews.txtReview.text = "";
    }

    function bindEvents() {
        frmReviews.postShow = function() {
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Reviews: Michaels Mobile App";
            lTealiumTagObj.page_name = "Reviews: Michaels Mobile App";
            lTealiumTagObj.page_type = "Reviews";
            lTealiumTagObj.page_category_name = "Reviews";
            lTealiumTagObj.page_category = "Reviews";
            TealiumManager.trackView("Reviews Screen", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.productid = MVCApp.getReviewsController().getProductIDForReviewsTracking();
            MVCApp.Customer360.sendInteractionEvent("Reviews", cust360Obj);
        };
        frmReviews.flxMainContainer.onScrollEnd = onreachWidgetEnd;
        frmReviews.btnRating.onClick = function() {
            rating = 0;
            if (frmReviews.imgRating.src == "checkbox_on.png") {
                for (var i = 1; i < 6; i++) {
                    frmReviews["lblRating" + i].skin = "sknLblIconStarLrg";
                }
                frmReviews.lblRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.clickToRate");
                frmReviews.imgRating.isVisible = false;
            }
        };
        MVCApp.tabbar.bindIcons(frmReviews, "frmProducts");
        MVCApp.tabbar.bindEvents(frmReviews);
        frmReviews.btnHeaderLeft.onClick = function() {
            MVCApp.Toolbox.common.setTransition(frmPDP, 2);
            frmPDP.show();
        };
        frmReviews.lstbxReviewSort.onSelection = function() {
            MVCApp.getReviewsController().reviewSort();
        };
        frmReviews.txtEmail.onTextChange = txChangeEmail;
        frmReviews.txtNickName.onTextChange = txChangeNickName;
        frmReviews.txtLocation.onTextChange = txChangeLocation;
        frmReviews.txtReviewTitle.onTextChange = txChangeFirstName;
        frmReviews.btnClearEmail.onClick = clearTextEmail;
        frmReviews.btnClearFirstName.onClick = clearTexttitle;
        frmReviews.btnClearLocation.onClick = clearTextLocation;
        frmReviews.btnClearNickName.onClick = clearTextNickName;
        frmReviews.txtReview.onTextChange = taClearReview;
        frmReviews.lblDropdown.onTouchEnd = showReviewsDetails;
        frmReviews.btnWriteAReview.onClick = showReviewsDetails;
        frmReviews.btnSubmit.onClick = onClickSubmit;
        frmReviews.btnAgree.onClick = onClickAgree;
        for (var i = 1; i <= 5; i++) {
            frmReviews["lblRating" + i].onTouchEnd = setRating;
            frmReviews["lblRating" + i].info = {
                value: i
            }
        }
        frmReviews.rtTermsandConditions.onClick = function() {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.writeAReviewTermsandConditions);
        };
        frmReviews.showMoreReviews.onClick = MVCApp.getReviewsController().showMore;
        frmReviews.txtReview.onDone = MVCApp.getReviewsController().checkReviewLength;
        frmReviews.txtNickName.onEndEditing = MVCApp.getReviewsController().checkNickNameLength;
        frmReviews.txtReviewTitle.onEndEditing = MVCApp.getReviewsController().reviewTitleLength;
        frmReviews.txtEmail.onEndEditing = MVCApp.getReviewsController().reviewEmailLength;
    }

    function _ratingBarAvgNo(ratAvg) {
        return ratAvg.toPrecision();
    }

    function _ratingBarAvg(ratAvg, tot) {
        tot = parseInt(tot);
        var calRatAvg = 0;
        if (tot != 0 && tot != null) {
            calRatAvg = (ratAvg * 100) / tot;
        } else {
            calRatAvg = 0;
        }
        return calRatAvg.toString() + "%";
    }

    function updateScreen(reslt) {
        frmReviews.flxMainContainer.contentOffset = {
            x: 0,
            y: 0
        };
        var rvwDat = reslt.getReviewsData();
        if (rvwDat != []) {
            if (rvwDat.totReviewCount != undefined && rvwDat.totReviewCount != null && rvwDat.totReviewCount != "") {
                frmReviews.lblReviewCount.text = "(" + rvwDat.totReviewCount + ")";
                kony.store.setItem("totalReviews", rvwDat.totReviewCount);
            } else {
                frmReviews.lblReviewCount.text = "(0)";
                kony.store.setItem("totalReviews", 0);
            }
            var starRating = rvwDat.avgOverallRating || "";
            starRating = parseFloat(starRating);
            starRating = starRating.toFixed(1);
            var starFloor = Math.floor(starRating);
            var starDecimal = starRating.toString().split(".")[1];
            starDecimal = parseInt(starDecimal);
            for (var i = 1; i <= 5; i++) {
                if (i <= starRating) {
                    frmReviews["imgStar" + i].src = "star.png";
                } else {
                    frmReviews["imgStar" + i].src = "starempty.png";
                }
            }
            var starFlag = starFloor + 1;
            if (starDecimal > 0) {
                frmReviews["imgStar" + starFlag].src = "star" + starDecimal + ".png";
            }
            frmReviews.CopyLabel0488e83714d4d46.text = rvwDat.name;
            frmReviews.segReviews.widgetDataMap = {
                lblReviewComment: "ReviewText",
                lblStar1: "Star1",
                lblStar2: "Star2",
                lblStar3: "Star3",
                lblStar4: "Star4",
                lblStar5: "Star5",
                lblReviewDate: "SubmissionTime",
                lblReviewTitle: "Title",
                rchtxtNameLoc: "nameNLoc"
            };
            frmReviews.segReviews.data = rvwDat;
            ratingBars(reslt);
        } else {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
        var requestParams = MVCApp.getReviewsController().getRequestParams();
        var ofset = requestParams.offset + MVCApp.Service.Constants.Reviews.limit;
        var total = kony.store.getItem("totalReviews");
        if (total > ofset && rvwDat.length > 0) {
            frmReviews.showMoreReviews.setVisibility(true);
        } else {
            frmReviews.showMoreReviews.setVisibility(false);
        }
    }

    function showMore(results) {
        var dataRvws = results.getReviewsData();
        frmReviews.segReviews.addAll(dataRvws);
        var requestParams = MVCApp.getReviewsController().getRequestParams();
        var ofset = requestParams.offset + MVCApp.Service.Constants.Reviews.limit;
        var total = kony.store.getItem("totalReviews");
        if (total > ofset) {
            frmReviews.showMoreReviews.setVisibility(true);
        } else {
            frmReviews.showMoreReviews.setVisibility(false);
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function ratingBars(ratingResults) {
        var ratBarDat = ratingResults.ratingBars();
        frmReviews.lblRatingBar5.text = _ratingBarAvgNo(ratBarDat.flag5);
        frmReviews.flxRatingBar5.width = _ratingBarAvg(ratBarDat.flag5, ratBarDat.totRevwCount);
        frmReviews.lblRatingBar4.text = _ratingBarAvgNo(ratBarDat.flag4);
        frmReviews.flxRatingBar4.width = _ratingBarAvg(ratBarDat.flag4, ratBarDat.totRevwCount);
        frmReviews.lblRatingBar3.text = _ratingBarAvgNo(ratBarDat.flag3);
        frmReviews.flxRatingBar3.width = _ratingBarAvg(ratBarDat.flag3, ratBarDat.totRevwCount);
        frmReviews.lblRatingBar2.text = _ratingBarAvgNo(ratBarDat.flag2);
        frmReviews.flxRatingBar2.width = _ratingBarAvg(ratBarDat.flag2, ratBarDat.totRevwCount);
        frmReviews.lblRatingBar1.text = _ratingBarAvgNo(ratBarDat.flag1);
        frmReviews.flxRatingBar1.width = _ratingBarAvg(ratBarDat.flag1, ratBarDat.totRevwCount);
        show();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function showReviewsDetails() {
        frmReviews.lblRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.clickToRate");
        frmReviews.lblOverallRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.overallRating");
        frmReviews.lblOverallRating.skin = "sknLblGothamBold28pxDark";
        frmReviews.lblReviewTitle1.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTitle");
        frmReviews.lblReviewTitle1.skin = "sknLblGothamBold28pxDark";
        frmReviews.lblReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.review");
        frmReviews.lblReview.skin = "sknLblGothamBold28pxDark";
        frmReviews.lblNickName.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.nickName");
        frmReviews.lblNickName.skin = "sknLblGothamBold28pxDark";
        frmReviews.lblEmailReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.email");
        frmReviews.lblEmailReview.skin = "sknLblGothamBold28pxDark";
        frmReviews.lblBottomMessage.setVisibility(false);
        rating = 0;
        if (toggleReviewsFlag) {
            toggleChevronR("D", frmReviews.flxWriteReviewDetails, frmReviews.lblDropdown);
            frmReviews.flxWriteReviewDetails.setVisibility(true);
        } else {
            toggleChevronR("U", frmReviews.flxWriteReviewDetails, frmReviews.lblDropdown);
            frmReviews.flxWriteReviewDetails.setVisibility(false);
        }
        toggleReviewsFlag = !toggleReviewsFlag;
    }

    function toggleChevronR(state, widgetFlex, widgetLbl) {
        clearAllFields();
        var flexHeight = widgetFlex.height;
        if (state === "D") {
            widgetFlex.animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "height": flexHeight
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.5
            });
            var trans100 = kony.ui.makeAffineTransform();
            trans100.rotate(180);
            widgetLbl.animate(kony.ui.createAnimation({
                "100": {
                    "anchorPoint": {
                        "x": 0.5,
                        "y": 0.5
                    },
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            });
        } else {
            widgetFlex.animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "height": flexHeight
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.5
            });
            var trans101 = kony.ui.makeAffineTransform();
            trans101.rotate(0);
            widgetLbl.animate(kony.ui.createAnimation({
                "100": {
                    "anchorPoint": {
                        "x": 0.5,
                        "y": 0.5
                    },
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans101
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            });
        }
    }

    function onClickAgree() {
        if (frmReviews.imgCheckAgree.src == "checkbox_off.png") {
            frmReviews.imgCheckAgree.src = "checkbox_on.png";
            frmReviews.lblBottomMessage.isVisible = false;
        } else {
            frmReviews.imgCheckAgree.src = "checkbox_off.png";
            frmReviews.lblBottomMessage.isVisible = true;
        }
    }
    /**
     * @function
     *
     */
    function setRatingText(value) {
        var text = "";
        if (value == 1) {
            text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.poor");
        } else if (value === 2) {
            text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.fair");
        } else if (value === 3) {
            text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.average");
        } else if (value === 4) {
            text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.good");
        } else if (value === 5) {
            text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.excellent");
        }
        if (text === "") {
            frmReviews.imgRating.setVisibility(false);
            frmReviews.imgRating.src = "checkbox_off.png";
        }
        return text;
    }

    function setRating(eventObj) {
        var value = eventObj.info.value;
        rating = eventObj.info.value;
        for (var i = 1; i <= 5; i++) {
            if (i > value) {
                frmReviews["lblRating" + i].skin = "sknLblIconStarLrg";
            } else {
                frmReviews["lblRating" + i].skin = "sknLblIconStarLrgActive";
            }
        }
        var ratingText = setRatingText(value);
        frmReviews.lblRating.text = ratingText;
        frmReviews.imgRating.setVisibility(true);
        frmReviews.imgRating.src = "checkbox_on.png";
        frmReviews.lblOverallRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.overallRating");
        frmReviews.lblOverallRating.skin = "sknLblGothamBold28pxDark";
    }

    function validateFields() {
        var email = frmReviews.txtEmail.text;
        var review_title = frmReviews.txtReviewTitle.text;
        var review = frmReviews.txtReview.text;
        var nick_name = frmReviews.txtNickName.text;
        var regularEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var validationFlag = true;
        if (rating === 0) {
            frmReviews.lblOverallRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.ratingMissing");
            frmReviews.lblOverallRating.skin = "sknLblGothamBold28pxRed";
            validationFlag = false;
        } else {
            frmReviews.lblOverallRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.overallRating");
            frmReviews.lblOverallRating.skin = "sknLblGothamBold28pxDark";
        }
        if (review_title === null || review_title === "" || review_title === undefined) {
            frmReviews.lblReviewTitle1.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTitleMissing");
            frmReviews.lblReviewTitle1.skin = "sknLblGothamBold28pxRed";
            validationFlag = false;
        } else {
            frmReviews.lblReviewTitle1.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTitle");
            frmReviews.lblReviewTitle1.skin = "sknLblGothamBold28pxDark";
        }
        if (review === null || review === "" || review.length < 50) {
            frmReviews.lblReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewTooShort");
            frmReviews.lblReview.skin = "sknLblGothamBold28pxRed";
            validationFlag = false;
        } else {
            frmReviews.lblReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.review");
            frmReviews.lblReview.skin = "sknLblGothamBold28pxDark";
        }
        if (nick_name === null || nick_name === "" || nick_name.length < 4) {
            frmReviews.lblNickName.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.nickNameTooShort");
            frmReviews.lblNickName.skin = "sknLblGothamBold28pxRed";
            validationFlag = false;
        } else {
            frmReviews.lblNickName.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.nickName");
            frmReviews.lblNickName.skin = "sknLblGothamBold28pxDark";
        }
        if (email === null || email.trim() === "" || !regularEx.test(email.trim())) {
            frmReviews.lblEmailReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.emailMissing");
            frmReviews.lblEmailReview.skin = "sknLblGothamBold28pxRed";
            validationFlag = false;
        } else {
            frmReviews.lblEmailReview.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.email");
            frmReviews.lblEmailReview.skin = "sknLblGothamBold28pxDark";
        }
        if (frmReviews.imgCheckAgree.src != "checkbox_on.png") {
            frmReviews.lblBottomMessage.setVisibility(true);
            validationFlag = false;
        } else {
            frmReviews.lblBottomMessage.setVisibility(false);
        }
        if (!validationFlag) {
            return false;
        }
        return true;
    }

    function findIfNickExists(nickName) {
        nickName = nickName.trim();
        for (var i = 0; i < gblNickKeyPair.length; i++) {
            if (gblNickKeyPair[i].nick == nickName) {
                return {
                    available: true,
                    nickObject: gblNickKeyPair[i]
                };
            }
        }
        return false;
    }

    function onClickSubmit() {
        var locale = kony.store.getItem("languageSelected");
        if (locale == "en_US" || locale == "en" || locale == "default") {
            locale = "en_US";
        }
        var BlackBoxDevicePrintObject = new ivocation.BlackBoxDevicePrint();
        //Invokes method 'fetchDevicePrint' on the object
        BlackBoxDevicePrintObject.fetchDevicePrint(
            /**Function*/
            function(bb) {
                var submitReviewObj = {};
                var itemID = frmPDP.lblItemNumber.text;
                var product_id = itemID.split("#")[1];
                submitReviewObj.productId = product_id;
                submitReviewObj.rating = rating;
                submitReviewObj.reviewText = frmReviews.txtReview.text;
                submitReviewObj.nickname = frmReviews.txtNickName.text.trim();
                submitReviewObj.userLocation = frmReviews.txtLocation.text;
                submitReviewObj.userEmail = frmReviews.txtEmail.text;
                submitReviewObj.title = frmReviews.txtReviewTitle.text;
                submitReviewObj.isAgreed = true;
                submitReviewObj.isRecommended = true;
                submitReviewObj.locale = locale;
                submitReviewObj.deviceFingerprint = bb;
                var nickExists = findIfNickExists(submitReviewObj.nickname);
                if (nickExists.available) {
                    submitReviewObj.user = nickExists.nickObject.userKey;
                } else {
                    submitReviewObj.user = "";
                }
                //submitReviewObj.passkey = MVCApp.Service.Constants.Reviews.passkey;
                if (validateFields()) {
                    MVCApp.getReviewsController().loadSubmitReview(submitReviewObj);
                }
            });
    }
    /**
     * @function
     *
     */
    function updateAfterSubmit() {
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewSubmitted"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.thanksForSharing"));
        //alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.reviewSubmitted"));
        var reviewText = MVCApp.getReviewsController().getReviewText();
        if (reviewText == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.btnWriteAReview")) {
            frmPDP.show();
        } else {
            show();
        }
        frmReviews.flxMainContainer.contentOffset = {
            x: 0,
            y: 0
        };
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        //alert(checkScroll);
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
        } else {
            MVCApp.Toolbox.common.dispFooter();
        }
        if (checkScroll === 0) {
            MVCApp.Toolbox.common.hideFooter();
        }
    }

    function ratingThere() {
        if (rating === 0) {
            frmReviews.lblOverallRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.ratingMissing");
            frmReviews.lblOverallRating.skin = "sknLblGothamBold28pxRed";
        } else {
            frmReviews.lblOverallRating.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmReviews.overallRating");
            frmReviews.lblOverallRating.skin = "sknLblGothamBold28pxDark";
        }
    }
    //Here we expose the public variables and functions 
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        showMore: showMore,
        ratingBars: ratingBars,
        showReviewsDetails: showReviewsDetails,
        toggleChevronR: toggleChevronR,
        onClickSubmit: onClickSubmit,
        validateFields: validateFields,
        updateAfterSubmit: updateAfterSubmit,
        onClickAgree: onClickAgree,
        setRating: setRating
    };
});