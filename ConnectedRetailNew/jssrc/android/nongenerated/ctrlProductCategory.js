/**
 * PUBLIC
 * This is the controller for the ProductCategory form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProductCategoryController = (function() {
    var lServiceRequestObj = {};
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    var categoryId_tracking = "";
    var entryFromWebView = false;
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.ProductCategoryModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.ProductCategoryView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    var selectedCategory = {};

    function setSelectedSubCategory(category) {
        selectedCategory = category;
    }

    function getSelectedSubCategory() {
        return selectedCategory;
    }
    var storeNo = "";
    var isStoreModeAlreadyEnabled = false;

    function setStoreModeStatus(pIsStoreModeAlreadyEnabled) {
        isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
    }

    function getStoreModeStatus() {
        return isStoreModeAlreadyEnabled;
    }

    function setStoreId(storeID) {
        storeNo = storeID;
    }

    function getStoreId() {
        return storeNo;
    }

    function load(selectedCategory) {
        lServiceRequestObj.selectedCategory = selectedCategory;
        kony.print("ProductCategory Controller.load");
        init();
        frmProductCategory.flxTransLayout.setVisibility(true);
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.id = selectedCategory.productId;
        setSelectedSubCategory(selectedCategory);
        var storeNo = "";
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeNo = gblInStoreModeDetails.storeID;
            setStoreId(storeNo);
            setStoreModeStatus(true);
        } else {
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (storeString !== "") {
                try {
                    var storeObj = JSON.parse(storeString);
                    storeNo = storeObj.clientkey;
                    setStoreId(storeNo);
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlProductCategory.js on load for storeString:" + JSON.stringify(e)
                    }]);
                    setStoreId("");
                    storeNo = "112222";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception In Tier 1 Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                setStoreId("");
                storeNo = "112222";
            }
        }
        gblTealiumTagObj.storeid = storeNo;
        requestParams.storeNo = storeNo;
        requestParams.productId = selectedCategory.productId;
        setCategoryIDForTracking(selectedCategory.productId);
        view.show(selectedCategory);
        model.loadSubCategories(view.updateScreen, requestParams);
    }

    function displayPage() {
        init();
        view.displaySamePage();
    }

    function setCategoryIDForTracking(categoryID) {
        categoryId_tracking = categoryID;
    }

    function getCategoryIdForTracking() {
        return categoryId_tracking;
    }

    function setFormEntryFromCartOrWebView(value) {
        entryFromWebView = value;
    }

    function getFormEntryFromCartOrWebView() {
        return entryFromWebView;
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        displayPage: displayPage,
        getSelectedSubCategory: getSelectedSubCategory,
        setStoreId: setStoreId,
        getStoreId: getStoreId,
        lServiceRequestObj: lServiceRequestObj,
        getStoreModeStatus: getStoreModeStatus,
        getCategoryIdForTracking: getCategoryIdForTracking,
        setFormEntryFromCartOrWebView: setFormEntryFromCartOrWebView,
        getFormEntryFromCartOrWebView: getFormEntryFromCartOrWebView
    };
});