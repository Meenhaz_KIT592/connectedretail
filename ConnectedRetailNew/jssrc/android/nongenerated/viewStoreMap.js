/**
 * PUBLIC
 * This is the view for the StoreMap form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.StoreMapView = (function() {
    var gblMapData = {};
    var showDept = true,
        showAisle = false,
        isMultiLevel = false;
    var gblAisleType = "AISLE";
    var gblAisleNo = "";
    var gblPrdName = "Prisamcolor Premier Soft Core Colored Pencil Set...";
    var gblActiveFloor = "";
    var gblHtmlString = "";
    var isAisleFound = false;
    var gblAisleFloor = "";
    var calloutFlag = false;
    var prevFormToStoreLocator = "";
    //var gblWrap_Clip_ID ="";
    /**
     * PUBLIC
     * Open the StoreMap form
     */
    function updateStoreName(storeData) {
        if (storeData.address2 !== "") {
            frmStoreMap.lblStoreName.text = storeData.address2;
        } else {
            frmStoreMap.lblStoreName.text = storeData.city;
        }
    }

    function enableButtons(currentAisle) {
        var indexes = MVCApp.getStoreMapController().getFirstLastIndex() || {
            firstIndex: 0,
            lastIndex: 0
        };
        frmStoreMap.txtShoppingList.skin = "sknLblGothamMedium28pxDark";
        frmStoreMap.txtRight.skin = "sknLblGothamMedium28pxDark";
        frmStoreMap.lblLeft.setVisibility(true);
        frmStoreMap.lblRight.setVisibility(true);
        frmStoreMap.lblNext.skin = "sknLblIcon28pxBlack";
        frmStoreMap.lblPrev.skin = "sknLblIcon28pxBlack";
        if (indexes.firstIndex === indexes.lastIndex) {
            frmStoreMap.txtShoppingList.skin = "sknLblGothamMedium28pxDarkGreyOut";
            frmStoreMap.txtRight.skin = "sknLblGothamMedium28pxDarkGreyOut";
            frmStoreMap.lblLeft.setVisibility(false);
            frmStoreMap.lblRight.setVisibility(false);
            frmStoreMap.lblNext.skin = "sknLblIcon28pxBlackGreyOut";
            frmStoreMap.lblPrev.skin = "sknLblIcon28pxBlackGreyOut";
        } else if (currentAisle === indexes.firstIndex) {
            frmStoreMap.lblPrev.skin = "sknLblIcon28pxBlackGreyOut";
            frmStoreMap.lblLeft.setVisibility(false);
            frmStoreMap.txtShoppingList.skin = "sknLblGothamMedium28pxDarkGreyOut";
        } else if (currentAisle === indexes.lastIndex) {
            frmStoreMap.txtRight.skin = "sknLblGothamMedium28pxDarkGreyOut";
            frmStoreMap.lblRight.setVisibility(false);
            frmStoreMap.lblNext.skin = "sknLblIcon28pxBlackGreyOut";
        }
    }

    function show() {
        if (MVCApp.getStoreMapController().getFromMyList()) {
            frmStoreMap.flxSearchShadow1.setVisibility(true);
            frmStoreMap.flxStoreMap.top = "104dp";
        } else {
            frmStoreMap.flxSearchShadow1.setVisibility(false);
            frmStoreMap.flxStoreMap.top = "60dp";
        }
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            frmStoreMap.btnMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.YouAreShoppingAt");
            if (gblInStoreModeDetails.POI) {
                if (gblInStoreModeDetails.POI.address2) {
                    frmStoreMap.lblStoreName.text = gblInStoreModeDetails.POI.address2;
                } else if (gblInStoreModeDetails.POI.city) {
                    frmStoreMap.lblStoreName.text = gblInStoreModeDetails.POI.city;
                } else {
                    frmStoreMap.lblStoreName.text = gblInStoreModeDetails.storeName;
                }
            } else {
                frmStoreMap.lblStoreName.text = gblInStoreModeDetails.storeName;
            }
            frmStoreMap.lblChange.isVisible = false;
        } else {
            frmStoreMap.lblChange.isVisible = true;
            frmStoreMap.btnMyStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterHamMenu.lblMyStore");
            var storeString = kony.store.getItem("myLocationDetails") || "";
            var fromStoreLocator = MVCApp.getStoreMapController().getFromStroreLocator();
            if (fromStoreLocator) {
                var storeInfo = MVCApp.getStoreMapController().getStoreInfo();
                updateStoreName(storeInfo);
            } else {
                if (storeString !== "") {
                    try {
                        var storeObj = JSON.parse(storeString);
                        if (storeObj.address2 !== "") {
                            frmStoreMap.lblStoreName.text = storeObj.address2;
                        } else {
                            frmStoreMap.lblStoreName.text = storeObj.city;
                        }
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "viewStoreMap.js on show for storeString:" + JSON.stringify(e)
                        }]);
                        frmStoreMap.lblStoreName.text = "";
                        if (e) {
                            TealiumManager.trackEvent("POI Parse Exception in Store Map View", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                } else {
                    frmStoreMap.lblStoreName.text = "";
                }
            }
        }
        MVCApp.Toolbox.common.setTransition(frmStoreMap, 3);
        frmStoreMap.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function postShowFrmStoreMap() {
        var lTealiumTagObj = gblTealiumTagObj;
        lTealiumTagObj.page_id = "Michaels Mobile App: Store Map";
        lTealiumTagObj.page_name = "Michaels Mobile App: Store Map";
        lTealiumTagObj.page_type = "store info";
        lTealiumTagObj.page_category_name = "Store Map";
        lTealiumTagObj.page_category = "Store Map";
        TealiumManager.trackView("Store Map Screen", lTealiumTagObj);
        var formName = MVCApp.getLocatorMapController().getFormName();
        if (formName !== "frmStoreMap") {
            prevFormToStoreLocator = formName;
        }
        var entryPoint = MVCApp.getStoreMapController().getEntryPoint();
        if (entryPoint !== "") {
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.storeid = MVCApp.Toolbox.common.getStoreID();
            cust360Obj.entryPoint = entryPoint;
            MVCApp.Customer360.sendInteractionEvent("StoreMap", cust360Obj);
            if (entryPoint == "VoiceSearch") {
                MVCApp.Customer360.callVoiceSearchResult("success", "", "StoreMap");
            }
        }
    }

    function bindEvents() {
        frmStoreMap.postShow = postShowFrmStoreMap;
        frmStoreMap.btnDepartments.onClick = onDepartmentClick;
        frmStoreMap.btnAisles.onClick = onAisleClick;
        frmStoreMap.btnL1.onClick = function() {
            showCallout(calloutFlag, "0");
            setLevelUI("0");
            onDepartmentClick();
        };
        frmStoreMap.btnL2.onClick = function() {
            showCallout(calloutFlag, "1");
            setLevelUI("1");
            onDepartmentClick();
        };
        frmStoreMap.btnL3.onClick = function() {
            showCallout(calloutFlag, "2");
            setLevelUI("2");
            onDepartmentClick();
        };
        frmStoreMap.btnL4.onClick = function() {
            showCallout(calloutFlag, "3");
            setLevelUI("3");
            onDepartmentClick();
        };
        frmStoreMap.flxCancel.onClick = showPreviousPage;
        frmStoreMap.lblChange.onTouchStart = function() {
            storeMapFromVoiceSearch = false;
            MVCApp.getLocatorMapController().load("frmStoreMap");
        };
        frmStoreMap.lblLeft.onClick = function() {
            showOtherAisle("prev");
        };
        frmStoreMap.lblRight.onClick = function() {
            showOtherAisle("next");
        };
    }

    function showOtherAisle(page) {
        var categoryAisle = MVCApp.getStoreMapController().getCategoryData() || [];
        var categoryLength = categoryAisle.length;
        var currentAisle = MVCApp.getStoreMapController().getCurrentAisle() || "";
        var category;
        var hasAisle;
        var params;
        var flxAisleInfo;
        if (page === "next") {
            if (currentAisle < categoryLength - 1) {
                currentAisle++;
                for (var i = currentAisle; i < categoryLength; i++) {
                    flxAisleInfo = categoryAisle[i].flxAisleInfo || "";
                    category = flxAisleInfo.info || "";
                    hasAisle = category === "" ? false : true;
                    if (hasAisle) {
                        enableButtons(i);
                        params = {};
                        params.aisleNo = category.aisleNum;
                        params.productName = category.product_name;
                        params.calloutFlag = true;
                        params.currentAisle = i;
                        params.isFromMyList = MVCApp.getStoreMapController().getFromMyList();
                        MVCApp.getStoreMapController().load(params);
                        break;
                    }
                }
            }
        } else {
            if (currentAisle > 0) {
                currentAisle--;
                for (var j = currentAisle; j >= 0; j--) {
                    flxAisleInfo = categoryAisle[j].flxAisleInfo || "";
                    category = flxAisleInfo.info || "";
                    hasAisle = category === "" ? false : true;
                    if (hasAisle) {
                        enableButtons(j);
                        params = {};
                        params.aisleNo = category.aisleNum;
                        params.productName = category.product_name;
                        params.calloutFlag = true;
                        params.currentAisle = j;
                        params.isFromMyList = MVCApp.getStoreMapController().getFromMyList();
                        MVCApp.getStoreMapController().load(params);
                        break;
                    }
                }
            }
        }
    }

    function showPreviousPage() {
        var prevScreen = kony.application.getPreviousForm();
        if (prevScreen && prevScreen !== null && prevScreen !== undefined) {
            if (storeMapFromVoiceSearch) {
                prevScreen = frmObject;
            }
            if (prevScreen.id == "frmMore") {
                MVCApp.Toolbox.common.setTransition(frmMore, 2);
                frmMore.flxVoiceSearch.setVisibility(false);
                frmMore.show();
            } else if (prevScreen.id == "frmPDP") {
                MVCApp.Toolbox.common.setTransition(frmPDP, 2);
                frmPDP.flxVoiceSearch.setVisibility(false);
                frmPDP.show();
            } else if (prevScreen.id == "frmPJDP") {
                MVCApp.Toolbox.common.setTransition(frmPJDP, 2);
                frmPJDP.flxVoiceSearch.setVisibility(false);
                frmPJDP.show();
            } else if (prevScreen.id == "frmProductsList") {
                MVCApp.Toolbox.common.setTransition(frmProductsList, 2);
                frmProductsList.flxVoiceSearch.setVisibility(false);
                frmProductsList.show();
            } else if (prevScreen.id == "frmProductCategoryMore") {
                MVCApp.Toolbox.common.setTransition(frmProductCategoryMore, 2);
                frmProductCategoryMore.flxVoiceSearch.setVisibility(false);
                frmProductCategoryMore.show();
            } else if (prevScreen.id == "frmHome") {
                MVCApp.Toolbox.common.setTransition(frmHome, 2);
                frmHome.flxVoiceSearch.setVisibility(false);
                MVCApp.getHomeController().load();
                //frmHome.show();
            } else if (prevScreen.id == "frmFindStoreMapView") {
                MVCApp.Toolbox.common.setTransition(frmFindStoreMapView, 2);
                MVCApp.getLocatorMapController().setFormName(prevFormToStoreLocator);
                frmFindStoreMapView.show();
            } else if (prevScreen.id == "frmProductCategory") {
                frmProductCategory.flxVoiceSearch.setVisibility(false);
                frmProductCategory.show();
            } else if (prevScreen.id == "frmShoppingList") {
                frmShoppingList.flxVoiceSearch.setVisibility(false);
                frmShoppingList.show();
            } else if (prevScreen.id == "frmWeeklyAdHome") {
                frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
                MVCApp.getWeeklyAdHomeController().showUI();
            } else if (prevScreen.id == "frmWeeklyAd") {
                frmWeeklyAd.flxVoiceSearch.setVisibility(false);
                MVCApp.getWeeklyAdController().showUI();
            } else if (prevScreen.id == "frmWeeklyAdDetail") {
                frmWeeklyAdDetail.flxVoiceSearch.setVisibility(false);
                MVCApp.getWeeklyAdDetailController().showUI();
            } else if (prevScreen.id == "frmProductsList") {
                frmProductsList.flxVoiceSearch.setVisibility(false);
                MVCApp.getProductsListController().backToScreen();
            } else if (prevScreen.id == "frmProducts") {
                frmProducts.flxVoiceSearch.setVisibility(false);
                MVCApp.getProductsController().displayPage();
            } else if (prevScreen.id == "frmProjects") {
                frmProjects.flxVoiceSearch.setVisibility(false);
                MVCApp.getProjectsController().displayProjects();
            } else if (prevScreen.id == "frmProjectsCategory") {
                frmProjectsCategory.flxVoiceSearch.setVisibility(false);
                MVCApp.getProjectsCategoryController().displayPage();
            } else if (prevScreen.id == "frmProjectsList") {
                frmProjectsList.flxVoiceSearch.setVisibility(false);
                MVCApp.getProjectsListController().showUI();
            }
            storeMapFromVoiceSearch = false;
        }
    }

    function updateScreen(response, calloutParams) {
        var storeMapData = response.STORE_MAP || "";
        frmStoreMap.browserDepartments.isVisible = true;
        frmStoreMap.flxButtons.isVisible = true;
        frmStoreMap.lblNoStoreMap.isVisible = false;
        frmStoreMap.flxMainContainer.bottom = "53Dp";
        if (storeMapData.length === 0) {
            frmStoreMap.browserDepartments.isVisible = false;
            frmStoreMap.flxLeft.isVisible = false;
            frmStoreMap.flxButtons.isVisible = false;
            frmStoreMap.lblNoStoreMap.isVisible = true;
            frmStoreMap.flxMainContainer.bottom = "0Dp";
        } else {
            gblMapData = storeMapData;
            if (gblMapData.length > 1) {
                isMultiLevel = true;
            } else {
                isMultiLevel = false;
            }
            gblAisleNo = calloutParams.aisleNo || "";
            gblPrdName = calloutParams.productName.replace("\'", "\\\'") || "";
            gblPrdName = gblPrdName.replace('\"', '');
            gblPrdName = gblPrdName.replace('"', '');
            calloutFlag = calloutParams.calloutFlag || false;
            showCallout(calloutFlag);
            kony.print(" Global Active Floor : " + gblActiveFloor);
            setLevelUI(gblActiveFloor);
        }
        show();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    //show call out or not validation 
    function showCallout(showCalloutFlag, floor) {
        if (showCalloutFlag) {
            showDept = false;
            showAisle = true;
            renderMapData(floor);
        } else {
            showDept = true;
            showAisle = false;
            renderMapData(floor);
        }
    }

    function mapToFloor(aisleNo, gblMapData, floor) {
        // show map of level 0 by default
        var tempData = gblMapData[0] || {};
        gblHtmlString = tempData.map_data || "";
        gblActiveFloor = "0";
        gblAisleFloor = "0";
        isAisleFound = false;
        var tempAisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(aisleNo);
        //gblHtmlString=gblHtmlString.replace("Aisle<span>",tempAisleInfo.name+"<span>");
        if (aisleNo.indexOf("MDAG") === 0) {
            kony.print("aisleNo::::::4" + aisleNo);
            var itemnew1 = /[MDAG]{4}/;
            var strnew1 = aisleNo;
            var myArray1 = strnew1.match(itemnew1);
            if (myArray1 !== null) {
                var match = aisleNo.match(/(\D+)?\d/);
                var index = match ? match[0].length - 1 : -1;
                if (index > -1) {
                    aisleNo = "MDAG" + "0";
                }
            }
        }
        if (aisleNo.indexOf("DA") === 0) {
            kony.print("aisleNo::::::3" + aisleNo);
            var itemnew2 = /[DA]{2}/;
            var strnew2 = aisleNo;
            var myArra2 = strnew2.match(itemnew2);
            if (myArra2 !== null) {
                aisleNo = "MDAG" + "0";
            }
        }
        for (var i = 0; i < gblMapData.length; i++) {
            // if floor is passed, show map of that floor
            kony.print(" ********* MaptoFloor checking for floor " + floor);
            if (floor !== undefined && floor !== null) {
                kony.print(" gblMapData[i].level === floor   " + gblMapData[i].level + " == " + floor);
                if (gblMapData[i].level === floor) {
                    gblHtmlString = gblMapData[i].map_data;
                    if (tempAisleInfo.name === null || tempAisleInfo.name === "") {
                        tempAisleInfo.name = "&nbsp;";
                    }
                    if (tempAisleInfo.number === null || tempAisleInfo.number === "") {
                        tempAisleInfo.number = " &nbsp; ";
                    }
                    gblHtmlString = gblHtmlString.replace("Aisle<span>\"\ +\ aisleNumber\ +\ \"<\/span>", tempAisleInfo.name + "<span>" + tempAisleInfo.number + "<\/span>");
                    gblHtmlString = gblHtmlString.replace("Aisle<span>\"\ +\ aisleNumber\ +\ \"<\/span>", tempAisleInfo.name + "<span>" + tempAisleInfo.number + "<\/span>");
                    gblActiveFloor = floor;
                    if (gblMapData[i].map_data.indexOf("." + aisleNo) != -1) {
                        isAisleFound = true;
                    }
                    kony.print("aisleNo::::::1" + aisleNo);
                    break;
                }
            } else if (gblMapData[i].map_data.indexOf("." + aisleNo) != -1) {
                kony.print(" ********* retreiving  floor from mapdta floor :  " + gblMapData[i].level);
                kony.print("aisleNo::::::2" + aisleNo);
                gblAisleFloor = gblMapData[i].level;
                gblActiveFloor = gblMapData[i].level;
                gblHtmlString = gblMapData[i].map_data;
                if (tempAisleInfo.name === null || tempAisleInfo.name === "") {
                    tempAisleInfo.name = " &nbsp; ";
                }
                if (tempAisleInfo.number === null || tempAisleInfo.number === "") {
                    tempAisleInfo.number = " &nbsp; ";
                }
                gblHtmlString = gblHtmlString.replace("Aisle<span>\"\ +\ aisleNumber\ +\ \"<\/span>", tempAisleInfo.name + "<span>" + tempAisleInfo.number + "<\/span>");
                gblHtmlString = gblHtmlString.replace("Aisle<span>\"\ +\ aisleNumber\ +\ \"<\/span>", tempAisleInfo.name + "<span>" + tempAisleInfo.number + "<\/span>");
                isAisleFound = true;
                break;
            }
            kony.print("aisleNo::::::3" + aisleNo);
        }
    }
    //set Floor level 
    function setLevelUI(floor) {
        if (floor !== undefined && floor !== null) {
            gblActiveFloor = floor;
        }
        if (isMultiLevel) {
            frmStoreMap.flxLeft.isVisible = true;
            var level = parseInt(gblMapData[0].level);
            if (level == 0) {
                frmStoreMap.btnL1.setVisibility(true);
            } else {
                frmStoreMap.btnL1.setVisibility(false);
            }
            if (gblMapData.length + level == 3) {
                frmStoreMap.btnL3.setVisibility(true);
            } else if (gblMapData.length + level == 4) {
                frmStoreMap.btnL3.setVisibility(true);
                frmStoreMap.btnL4.setVisibility(true);
            } else if (gblMapData.length + level == 2) {
                frmStoreMap.btnL3.setVisibility(false);
                frmStoreMap.btnL4.setVisibility(false);
            }
            kony.print("****************** Multi Level ***********************");
            if (gblActiveFloor === "0") {
                kony.print("****************** Showing Level 0 ***********************");
                frmStoreMap.btnL1.skin = 'sknBtnSecondaryRed';
                frmStoreMap.btnL1.focusSkin = 'sknBtnSecondaryRedFoc';
                frmStoreMap.btnL2.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL2.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL3.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL3.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL4.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL4.focusSkin = 'sknBtnSecondaryGrayFoc';
            } else if (gblActiveFloor === "1") {
                kony.print("****************** Showing Level 1 ***********************");
                frmStoreMap.btnL2.skin = 'sknBtnSecondaryRed';
                frmStoreMap.btnL2.focusSkin = 'sknBtnSecondaryRedFoc';
                frmStoreMap.btnL1.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL1.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL3.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL3.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL4.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL4.focusSkin = 'sknBtnSecondaryGrayFoc';
            } else if (gblActiveFloor === "2") {
                kony.print("****************** Showing Level 3 ***********************");
                frmStoreMap.btnL3.skin = 'sknBtnSecondaryRed';
                frmStoreMap.btnL3.focusSkin = 'sknBtnSecondaryRedFoc';
                frmStoreMap.btnL1.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL1.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL2.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL2.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL4.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL4.focusSkin = 'sknBtnSecondaryGrayFoc';
            } else if (gblActiveFloor === "3") {
                kony.print("****************** Showing Level 4 ***********************");
                frmStoreMap.btnL4.skin = 'sknBtnSecondaryRed';
                frmStoreMap.btnL4.focusSkin = 'sknBtnSecondaryRedFoc';
                frmStoreMap.btnL1.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL1.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL2.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL2.focusSkin = 'sknBtnSecondaryGrayFoc';
                frmStoreMap.btnL3.skin = 'sknBtnSecondaryGray';
                frmStoreMap.btnL3.focusSkin = 'sknBtnSecondaryGrayFoc';
            }
        } else {
            frmStoreMap.flxLeft.isVisible = false;
        }
    }

    function changeSkins(bShowDepartment) {
        if (bShowDepartment) {
            frmStoreMap.btnDepartments.skin = 'sknBtnSecondaryRed';
            frmStoreMap.btnDepartments.focusSkin = 'sknBtnSecondaryRedFoc';
            frmStoreMap.btnAisles.setEnabled(true);
            frmStoreMap.btnAisles.skin = 'sknBtnSecondaryGray';
            frmStoreMap.btnAisles.focusSkin = 'sknBtnSecondaryGrayFoc';
        } else {
            frmStoreMap.btnDepartments.skin = 'sknBtnSecondaryGray';
            frmStoreMap.btnDepartments.focusSkin = 'sknBtnSecondaryGrayFoc';
            frmStoreMap.btnDepartments.setEnabled(true);
            frmStoreMap.btnAisles.skin = 'sknBtnSecondaryRed';
            frmStoreMap.btnAisles.focusSkin = 'sknBtnSecondaryRedFoc';
        }
    }
    // current htmlstring 
    var lastUsedDepartmentHtmlString = "";
    var lastUsedAisleViewHtmlString = "";
    var htmlStringFromServer = "";
    //setting html string to map widget
    function renderMapData(floor) {
        kony.print("In renderMapData floor : " + floor);
        //  kony.print("gblMapData:" + JSON.stringify(gblMapData));
        mapToFloor(gblAisleNo, gblMapData, floor);
        // gblHtmlString=gblHtmlString.replace("Aisle<span>","<span>");
        // gblHtmlString=gblHtmlString.replace(/Aisle<span>\"\ +\ aisleNumber\ +\ \"<\/span>/g,tempAisleInfo.name+"<span>"+tempAisleInfo.number+"<\/span>");
        var htmlString = gblHtmlString.replace("\\\"", "\"");
        frmStoreMap.browserDepartments.enableOverviewMode = true;
        frmStoreMap.browserDepartments.useWideViewport = true;
        changeSkins(showDept);
        htmlStringFromServer = htmlString;
        kony.print("isAisleFound::" + isAisleFound);
        kony.print("gblActiveFloor::" + gblActiveFloor);
        kony.print("gblAisleNo::" + gblAisleNo);
        if (isAisleFound && (gblActiveFloor === gblAisleFloor) && gblAisleNo !== "") {
            htmlString = setCallout(htmlString);
        } else {
            // show entrance
            htmlString = htmlString.replace("{WHATTODOWHATNOTTODO}", 'zoomOutAndGotoEntrance();');
        }
        kony.print("********************************************htmlString:" + htmlString);
        frmStoreMap.browserDepartments.htmlString = htmlString;
        globalHtmlString = htmlString;
        kony.print("END ********************************************htmlString:");
    }
    //setting callout to html string
    function setCallout(htmlString) {
        if (gblPrdName.length > 45) {
            gblPrdName = gblPrdName.substr(0, 42).concat("...");
        }
        if (gblWrap_Clip_ID === null || gblWrap_Clip_ID === undefined) {
            gblWrap_Clip_ID = "";
        }
        if (calloutFlag) {
            // show call out
            htmlString = htmlString.replace("{WHATTODOWHATNOTTODO}", "zoomOutAndShowCallout('" + gblAisleNo + "', '" + gblPrdName + "', true,'" + gblWrap_Clip_ID + "');showDepartments(false);");
        }
        //change DIV class to active
        //htmlString = htmlString.replace("aisle " + gblAisleNo, "aisle active " + gblAisleNo);
        return htmlString;
    }

    function onDepartmentClick() {
        showDept = true;
        showAisle = false;
        var tempHtmlString;
        changeSkins(showDept);
        if (gblWrap_Clip_ID === null || gblWrap_Clip_ID === undefined) {
            gblWrap_Clip_ID = "";
        }
        if (calloutFlag) {
            kony.print("gblWrap_Clip_ID:" + gblWrap_Clip_ID);
            // tempHtmlString = htmlStringFromServer.replace("{WHATTODOWHATNOTTODO}","zoomOutAndShowCallout('"+ gblAisleNo + "', '" + gblPrdName + "', true,'"+ gblWrap_Clip_ID  + "');");
            tempHtmlString = htmlStringFromServer.replace("{WHATTODOWHATNOTTODO}", "zoomOutAndShowCallout('" + gblAisleNo + "', '" + gblPrdName + "','" + gblWrap_Clip_ID + "');");
        } else {
            tempHtmlString = htmlStringFromServer.replace("{WHATTODOWHATNOTTODO}", 'zoomOutAndGotoEntrance();');
        }
        frmStoreMap.browserDepartments.htmlString = tempHtmlString;
        frmStoreMap.browserDepartments.evaluateJavaScript('showDepartments(true);');
    }

    function onAisleClick() {
        kony.print("gblAisleNo::" + gblAisleNo);
        showDept = false;
        showAisle = true;
        changeSkins(showDept);
        var tempHtmlString;
        if (gblWrap_Clip_ID === null || gblWrap_Clip_ID === undefined) {
            gblWrap_Clip_ID = "";
        }
        if (calloutFlag) {
            kony.print("gblWrap_Clip_ID:" + gblWrap_Clip_ID);
            tempHtmlString = htmlStringFromServer.replace("{WHATTODOWHATNOTTODO}", "gotoAisle('" + gblAisleNo + "', '" + gblPrdName + "', true,'" + gblWrap_Clip_ID + "');");
        } else {
            tempHtmlString = htmlStringFromServer.replace("{WHATTODOWHATNOTTODO}", 'zoomInAndGotoEntrance();');
        }
        frmStoreMap.browserDepartments.htmlString = tempHtmlString;
        frmStoreMap.browserDepartments.evaluateJavaScript('showDepartments(false);');
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        enableButtons: enableButtons
    };
});