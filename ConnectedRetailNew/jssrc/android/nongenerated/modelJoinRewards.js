/**
 * PUBLIC
 * This is the model for JoinRewards form
 */
var MVCApp = MVCApp || {};
MVCApp.JoinRewardsModel = (function() {
    var serviceType = "signupforloyaltyservice";
    var operationId = "loyaltySignin";

    function joinRewards(callback, requestParams) {
        kony.print("JoinRewardsModel.js");
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.opstatus === 0 || results.opstatus === "0") {
                callback(results);
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
        });
    }
    return {
        joinRewards: joinRewards
    };
});