var MVCApp = MVCApp || {};
var messagingClient;
var ksid;
var push_id;
var gblPushMsg;
var deeplink;
var gblCampaignId = "";
var appLaunchCust360Sent = false;
MVCApp.PushNotification = {
    common: {
        deRegisterPush: function() {
            var myhash = [];
            kony.push.deRegister(myhash);
        },
        registerPush: function() {
            kony.print("\n\n\n<--------in registerPush--------->\n\n\n");
            //var senderID ="238501342512"; // gcm key  AIzaSyDRWEfHrtwxPEg9zAsrLpr9pvYQBvkRcsA
            var senderID = MVCApp.Toolbox.common.getSenderId(); //var senderID ="626490506736"; // gcm key AIzaSyAoJvPbsOhvea9RCR3H0EUsJDuotK9wMf4
            var iOSconfig = [0, 1, 2];
            var androidConfig = {
                senderid: senderID
            };
            try {
                kony.print("in registerPush - processing for iOS");
                kony.push.register(iOSconfig);
            } catch (e) {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                if (e) {
                    TealiumManager.trackEvent("Exception While Registering For Push Notification", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        },
        // Function to setup all the callbacks for all push events
        // This function is called on the preshow of the form
        setupPushCallbacks: function() {
            kony.print("\n\n\n<--------in setupPushCallbacks--------->\n\n\n");
            // Create local object 'callbacks'
            var callbacks = {
                onsuccessfulregistration: MVCApp.PushNotification.common.regSuccess,
                onfailureregistration: MVCApp.PushNotification.common.regFail,
                onlinenotification: MVCApp.PushNotification.common.gotOnlinePushMessage,
                offlinenotification: MVCApp.PushNotification.common.gotOfflinePushMessage,
                onsuccessfulderegistration: MVCApp.PushNotification.common.deregSuccess,
                onfailurederegistration: MVCApp.PushNotification.common.deregFail
            };
            // Call the push API to register callbacks
            kony.push.setCallbacks(callbacks);
        },
        regSuccess: function(regId) {
            kony.print("\n\n\n<--------in regSuccess()--------->\n\n\n");
            kony.print("\nRegistration Id-->" + JSON.stringify(regId));
            var formObj = kony.application.getCurrentForm();
            var form = "";
            if (formObj !== null && formObj !== undefined) {
                form = formObj;
            }
            var checkNotification = redirectToSettings.checkNotificationSettingsStatus();
            kony.print("in Check Notifications -------> " + checkNotification);
            if (!checkNotification) {
                if (form.id == "frmGuide") {
                    redirectToSettings.registerForPushNotifications();
                } else {
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                }
            } else {
                if (form.id == "frmGuide") {
                    if (firstLaunchFromPrevKonyVersionFlag == "false") {
                        var userObject = kony.store.getItem("userObj");
                        if (userObject) {
                            MVCApp.getHomeController().showDefaultView();
                            MVCApp.Toolbox.common.showLoadingIndicator("");
                            getProductsCategory();
                        } else {
                            frmGuide.flexGuide5.setEnabled(false);
                            MVCApp.getGuideController().guideFormAnimation(frmGuide.flexGuide6);
                        }
                    } else {
                        frmGuide.flexGuide3.setEnabled(false);
                        MVCApp.getGuideController().guideFormAnimation(frmGuide.flexGuide4);
                    }
                    initializeEstimoteLibraries();
                }
            }
            var notificationNotNowFlag = kony.store.getItem("gblNotificationNotNowFlag") !== null ? kony.store.getItem("gblNotificationNotNowFlag") : false;
            if (regId && (regId !== kony.store.getItem("pushnativeid") || notificationNotNowFlag)) {
                TealiumManager.trackEvent("Push Subscription Id for iOS Platform", {
                    event_type: "registration",
                    push_subscription_id_ios: regId
                });
                MVCApp.PushNotification.common.subscribeMFPushMessaging(regId);
                MVCApp.PushNotification.common.subscribeSFMCPushMessaging(regId);
                kony.store.setItem("pushnativeid", regId);
            }
        },
        regFail: function(error) {
            //alert(" Device registration failed:"+JSON.stringify(error));
            kony.print("\n\n\n<--------in regFail--------->\n\n\n");
            kony.print("\n Error  --------->" + JSON.stringify(error));
            if (!MVCApp.Toolbox.common.checkIfNetworkExists()) {
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, function() {
                    var form = kony.application.getCurrentForm();
                    if (form.id == "frmGuide") {
                        initializeEstimoteLibraries();
                        frmGuide.flexGuide3.setEnabled(false);
                        MVCApp.getGuideController().guideFormAnimation(frmGuide.flexGuide4);
                    }
                }, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        },
        // Function to be invoked when the deregistration is successful. Once the deregistration is sucessful,
        // we need to unsubscribe to KMS.
        // So this function contains the code to invoke service 'unregisterSubscriberPush'
        deregSuccess: function() {
            //alert(" dde reg success ");
            kony.print("\n\n\n<--------in deregSuccess--------->\n\n\n");
            // Displaying the log on the screen along with previous log
            // Check if userKsID is not null
            //if (userKsID!=null){
            //}
            MVCApp.PushNotification.common.unsubscribeMFMessaging();
        },
        // Function to be invoked when the deregistration is unsuccessful
        deregFail: function(error) {
            if (!MVCApp.Toolbox.common.checkIfNetworkExists()) {
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, function() {}, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
            }
            kony.print("\n\n\n<--------in deregFail--------->\n\n\n");
            // Displaying the log on the screen along with previous log
        },
        // Function to be invoked when the device receives push notification message when the application is running
        gotOnlinePushMessage: function(pushMsg) {
            var campaignId = pushMsg.campaignId || "";
            if (campaignId !== "") {
                gblCampaignId = campaignId;
                MVCApp.sendMetricReport(frmHome, [{
                    "campaign": campaignId
                }]);
            }
            kony.print("<----- push message  ONLINE::: ----->" + JSON.stringify(pushMsg));
            var msg, title = "";
            deeplinkEnabled = true;
            if (pushMsg.hasOwnProperty("deeplink") || pushMsg.hasOwnProperty("isRichPush") || pushMsg._od) {
                var yesLabel = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertYes"),
                    noLabel = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertNo");
                if (pushMsg.hasOwnProperty("Yes")) {
                    yesLabel = pushMsg.Yes;
                }
                if (pushMsg.hasOwnProperty("No")) {
                    noLabel = pushMsg.No;
                }
                deeplink = pushMsg._od ? pushMsg._od : pushMsg.deeplink;
                title = pushMsg.alert.title;
                if (title && title !== "") {
                    msg = pushMsg.alert.body;
                } else {
                    msg = pushMsg.alert;
                }
                if (pushMsg.deeplink) {
                    deeplink = pushMsg.deeplink;
                } else if (pushMsg.isRichPush) {
                    push_id = pushMsg.mid;
                }
                if (gblLaunchMode === 2) {
                    MVCApp.PushNotification.common.getPushMsgDetailsOnline(true);
                    gblLaunchMode = 0;
                } else {
                    kony.ui.Alert({
                        message: msg,
                        alertTitle: title,
                        alertType: constants.ALERT_TYPE_CONFIRMATION,
                        alertIcon: "pushicon.png",
                        yesLabel: yesLabel,
                        noLabel: noLabel,
                        alertHandler: MVCApp.PushNotification.common.getPushMsgDetailsOnline
                    }, {});
                }
                //MVCApp.PushNotification.common.getPushMsgDetails(true);
            } else {
                title = pushMsg.alert.title;
                msg = pushMsg.alert.body;
                if (title && title !== "") {
                    msg = pushMsg.alert.body;
                } else {
                    msg = pushMsg.alert;
                }
                var pspConfig = {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT,
                    "contentAlignment": constants.ALERT_CONTENT_ALIGN_CENTER
                };
                kony.ui.Alert({
                    message: msg,
                    alertTitle: title,
                    alertType: constants.ALERT_TYPE_INFO,
                    alertIcon: "pushicon.png",
                    yesLabel: "OK"
                }, pspConfig);
            }
            MVCApp.Toolbox.common.trackAppLaunchPushNotification();
        },
        // Function to be invoked when the device receives push notification message when the application is not running
        gotOfflinePushMessage: function(pushMsg) {
            try {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.launchType = "pushNotification";
                cust360Obj.isFirstLaunch = "false";
                //MVCApp.Customer360.sendBaseTouchPoint(cust360Obj);
                MVCApp.Customer360.sendInteractionEvent("Launch", cust360Obj);
                deeplinkEnabled = true;
                appLaunchCust360Sent = true;
                var campaignId = pushMsg.campaignId || "";
                if (campaignId !== "") {
                    gblCampaignId = campaignId;
                    MVCApp.sendMetricReport(frmHome, [{
                        "campaign": campaignId
                    }]);
                }
                // alert("\n\n\n<--------in gotOfflinePushMessage--------->\n\n\n"+JSON.stringify(pushMsg));
                var form = kony.application.getCurrentForm();
                gblPushMsg = pushMsg;
                if (form) {
                    MVCApp.PushNotification.common.invokeAlertForOfflineHandler();
                } else {
                    kony.timer.schedule("OfflinePushTrigger", function() {
                        if (isHomeFormLoaded) {
                            MVCApp.PushNotification.common.invokeAlertForOfflineHandler();
                            kony.timer.cancel("OfflinePushTrigger");
                            gblPushMsg = {};
                        }
                    }, 2, true);
                }
                if (gblTealiumTagObj && !gblTealiumTagObj.platform_version) {
                    configureTealiumTagObj();
                }
                MVCApp.Toolbox.common.trackAppLaunchPushNotification();
            } catch (e) {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                kony.print("exception in offline handler---" + e);
                if (e) {
                    TealiumManager.trackEvent("Exception While getting Offline Push Message", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        },
        getPushMsgDetailsOffline: function(response) {
            if (response) {
                MVCApp.Toolbox.common.showLoadingIndicator("");
                if (deeplink) {
                    if (MVCApp.Toolbox.common.determineIfExternal(deeplink)) {
                        //FacebookAnalytics.reportEvents("", "", deeplink, "PushNotification");
                    }
                    openDeeplinkURL.openiOSUrl(deeplink + "", function(iosURl) {
                        MVCApp.Toolbox.common.openApplicationURL(iosURl);
                        deeplink = null;
                    });
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                } else if (push_id != null && push_id.length > 0 && push_id != undefined) {
                    var serviceName = "RichMessaagePushService";
                    var operationId = "GetDetails";
                    var inputParams = {
                        "pushID": push_id
                    };
                    MakeServiceCall(serviceName, operationId, inputParams, function(results) {
                        kony.print("RichMessaagePushService success :)" + JSON.stringify(results));
                        //MVCApp.Toolbox.common.openApplicationURL(results.message);//
                        deeplink = results.message;
                        kony.print("deeplink is    " + deeplink);
                        push_id = "";
                        MVCApp.Toolbox.common.dismissLoadingIndicator();
                    }, function(results) {
                        push_id = "";
                        MVCApp.Toolbox.common.dismissLoadingIndicator();
                        kony.print("RichMessaagePushService failed :(" + JSON.stringify(results));
                    });
                } else {
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                }
            }
        },
        getPushMsgDetailsOnline: function(response) {
            if (response) {
                MVCApp.Toolbox.common.showLoadingIndicator("");
                if (deeplink) {
                    if (MVCApp.Toolbox.common.determineIfExternal(deeplink)) {
                        //FacebookAnalytics.reportEvents("", "", deeplink, "PushNotification");
                    }
                    openDeeplinkURL.openiOSUrl(deeplink + "", function(iosURl) {
                        MVCApp.Toolbox.common.openApplicationURL(iosURl);
                        deeplink = null;
                    });
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                } else if (push_id != null && push_id.length > 0 && push_id != undefined) {
                    var serviceName = "RichMessaagePushService";
                    var operationId = "GetDetails";
                    var inputParams = {
                        "pushID": push_id
                    };
                    MakeServiceCall(serviceName, operationId, inputParams, function(results) {
                        push_id = "";
                        kony.print("RichMessaagePushService success :)" + JSON.stringify(results));
                        MVCApp.Toolbox.common.openApplicationURL(results.message); //
                        push_id = "";
                        MVCApp.Toolbox.common.dismissLoadingIndicator();
                    }, function(results) {
                        push_id = "";
                        MVCApp.Toolbox.common.dismissLoadingIndicator();
                        kony.print("RichMessaagePushService failed :(" + JSON.stringify(results));
                    });
                } else {
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                }
            }
        },
        invokeAlertForOfflineHandler: function() {
            var pushMsg = gblPushMsg;
            var msg, title = "";
            deeplinkEnabled = true;
            kony.print(" push message ::: " + JSON.stringify(pushMsg));
            if (pushMsg.hasOwnProperty("deeplink") || pushMsg.hasOwnProperty("isRichPush") || pushMsg._od) {
                var yesLabel = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertYes"),
                    noLabel = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertNo");
                if (pushMsg.hasOwnProperty("Yes")) {
                    yesLabel = pushMsg.Yes;
                }
                if (pushMsg.hasOwnProperty("No")) {
                    noLabel = pushMsg.No;
                }
                deeplink = pushMsg._od ? pushMsg._od : pushMsg.deeplink;
                title = pushMsg.alert.title;
                if (title && title !== "") {
                    msg = pushMsg.alert.body;
                } else {
                    msg = pushMsg.alert;
                }
                msg = pushMsg.alert.body;
                if (pushMsg.deeplink) {
                    deeplink = pushMsg.deeplink;
                } else if (pushMsg.isRichPush) {
                    push_id = pushMsg.mid;
                }
                MVCApp.PushNotification.common.getPushMsgDetailsOffline(true);
            } else {
                title = pushMsg.alert.title;
                if (title && title !== "") {
                    msg = pushMsg.alert.body;
                } else {
                    msg = pushMsg.alert;
                }
                var pspConfig = {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT,
                    "contentAlignment": constants.ALERT_CONTENT_ALIGN_CENTER
                };
                if (msg.indexOf("DING!") === 0) {
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                    return false;
                }
                kony.ui.Alert({
                    message: msg,
                    alertTitle: title,
                    alertType: constants.ALERT_TYPE_INFO,
                    alertIcon: "pushicon.png",
                    yesLabel: "OK",
                    alertHandler: MVCApp.Toolbox.common.dismissLoadingIndicator
                }, pspConfig);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            }
        },
        subscribeMFPushMessaging: function(regId) {
            regId = regId || kony.store.getItem("pushnativeid");
            var MFSubscriptionFlag = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.konyfabric.engagement.subscriptionFlag");
            if (MFSubscriptionFlag && MFSubscriptionFlag == "true") {
                kony.print("\n\n<----------in subscribeMFMessaging()-------->\n\n");
                var deviceID = kony.os.deviceInfo().identifierForVendor;
                var ostype = "android" == kony.os.deviceInfo().name ? "androidgcm" : "iphone";
                var ufID = "test@michaels.com";
                var country;
                var language = kony.store.getItem("languageSelected");
                ufID = deviceID + "@michaels.com";
                var country = MVCApp.Toolbox.common.getRegion();
                try {
                    messagingClient = KNYMobileFabric.getMessagingService();
                } catch (e) {
                    kony.print("\n<----------Exception-------->" + e.message);
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                    if (e) {
                        TealiumManager.trackEvent("Exception While Subscribing For MF Messaging", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
                kony.print("messagingClient: " + JSON.stringify(messagingClient));
                messagingClient.register(ostype, deviceID, regId, ufID, function(response) {
                    kony.print(" message :: " + response.message);
                    ksid = response.id;
                    if (ksid) {
                        TealiumManager.trackEvent("Push Subscription", {
                            event_type: "registration",
                            push_subscription_id_kony: ksid
                        });
                    }
                    kony.store.setItem("ksid", ksid);
                    var region = MVCApp.Toolbox.common.getRegion();
                    var pushUser = kony.store.getItem("pushuserId");
                    kony.print("pushUser----" + pushUser);
                    if (null != pushUser && undefined != pushUser && pushUser.length > 0) {
                        MVCApp.PushNotification.common.modifyUser(deviceID + "@michaels.com", deviceID, language, country);
                    } else {
                        MVCApp.PushNotification.common.createUser(ksid, deviceID + "@michaels.com", deviceID, language, country);
                    }
                    kony.print("\n<----------Subscription Success--------> " + JSON.stringify(response));
                    if (kony.store.getItem("gblNotificationNotNowFlag")) {
                        kony.store.setItem("gblNotificationNotNowFlag", false);
                    } else {
                        frmSettings.pushSwitch.src = "on.png";
                        kony.store.setItem("pushSwitch", true);
                    }
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                }, function(error) {
                    kony.print("\n<----------Subscription Failure--------> " + JSON.stringify(error));
                    frmSettings.pushSwitch.src = "off.png";
                    kony.store.setItem("pushSwitch", false);
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                    if (!MVCApp.Toolbox.common.checkIfNetworkExists()) {
                        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, function() {}, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                    }
                });
            }
        },
        subscribeSFMCPushMessaging: function(regId) {
            OneManager.sendPushToken(regId);
            TealiumManager.trackEvent("SFMC Push Subscription", {
                event_type: "SFMCPushSubscription",
                push_subscription_id: regId
            });
        },
        toggleSFMCPushMessaging: function(toggleFlag) {
            if (OneManager) {
                OneManager.sfmcTogglePushNotifications(toggleFlag);
            }
        },
        modifyUser: function(email, deviceID, language, country) {
            kony.print("modifyUser-------");
            var serviceName = MVCApp.serviceType.PushNotification;
            var operationId = MVCApp.serviceEndPoints.modifyUser;
            var userID = kony.store.getItem("pushuserId");
            country = MVCApp.PushNotification.common.getCountryName(country);
            var deviceType = "android";
            deviceType = "iphone";
            var userName = MVCApp.PushNotification.common.getUserName();
            var first_name = userName.first_name;
            var last_name = userName.last_name;
            var emailUser = userName.email;
            var ksidNew = kony.store.getItem("ksid");
            if (userID) {
                var inputParams = {
                    "lastName": last_name,
                    "firstName": first_name,
                    "email": email,
                    "country": country,
                    "deviceId": deviceID,
                    "language": language,
                    "id": userID,
                    "deviceType": deviceType,
                    "emailUser": emailUser,
                    "ksid": ksidNew
                };
                MakeServiceCall(serviceName, operationId, inputParams, function(results) {
                    kony.print("User modified--------" + JSON.stringify(results));
                }, function(results) {
                    kony.print("failed modifying user----------" + JSON.stringify(results));
                });
            } else {
                MVCApp.PushNotification.common.createUser(ksidNew, deviceID + "@michaels.com", deviceID, language, country);
            }
        },
        createUser: function(ksid, email, deviceID, language, country) {
            kony.print("createUser-------");
            var serviceName = MVCApp.serviceType.PushNotification;
            var operationId = MVCApp.serviceEndPoints.addUser;
            country = MVCApp.PushNotification.common.getCountryName(country);
            var deviceType = "android";
            deviceType = "iphone";
            var userName = MVCApp.PushNotification.common.getUserName();
            var first_name = userName.first_name;
            var last_name = userName.last_name;
            var emailUser = userName.email;
            var inputParams = {
                "lastName": last_name,
                "firstName": first_name,
                "ksid": ksid,
                "email": email,
                "country": country,
                "deviceId": deviceID,
                "language": language,
                "deviceType": deviceType,
                "emailUser": emailUser
            };
            MakeServiceCall(serviceName, operationId, inputParams, function(results) {
                kony.print("User created--------" + JSON.stringify(results));
                if (results.hasOwnProperty("id")) {
                    kony.store.setItem("pushuserId", results.id);
                } else {
                    kony.store.setItem("pushuserId", "");
                }
            }, function(results) {
                kony.print("failed creating user----------" + JSON.stringify(results));
                if (results.opstatus != "101") {
                    kony.store.setItem("pushuserId", "");
                } else {
                    if (results.hasOwnProperty("id")) {
                        kony.store.setItem("pushuserId", results.id);
                    }
                    MVCApp.PushNotification.common.modifyUser(deviceID + "@michaels.com", deviceID, language, country);
                }
            });
        },
        getCountryName: function(country) {
            if (country === "US" || country === "United States") {
                country = "United States";
            } else if (country === "CA" || country === "Canada") {
                country = "Canada";
            }
            return country;
        },
        getUserName: function() {
            var userObject = kony.store.getItem("userObj");
            var first_name = "";
            var last_name = "";
            var email = "";
            if (userObject && userObject != null && userObject != undefined) {
                first_name = userObject.first_name || "";
                last_name = userObject.last_name || "";
                email = userObject.email || "";
            }
            if (!first_name && first_name === "") {
                first_name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.pushNoti.firstName");
            }
            if (!last_name && last_name === "") {
                last_name = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.pushNoti.lastName");
            }
            return {
                first_name: first_name,
                last_name: last_name,
                email: email
            }
        },
        unsubscribeMFMessaging: function() {
            kony.print("\n\n<----------in unsubscribeKMS-------->\n\n");
            try {
                var MFSubscriptionFlag = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.konyfabric.engagement.subscriptionFlag");
                if (MFSubscriptionFlag && MFSubscriptionFlag == "true") {
                    messagingClient = KNYMobileFabric.getMessagingService();
                    messagingClient.unregister(function(response) {
                        kony.print("\n<----------Unsubscription Success--------> " + JSON.stringify(response));
                        kony.store.setItem("ksid", "");
                        frmSettings.pushSwitch.src = "off.png";
                        kony.store.setItem("pushSwitch", false);
                        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                        cust360Obj.appNotifications = "disabled";
                        TealiumManager.trackEvent("Opting Out for Push Notification", {
                            event_type: "registration",
                            optedout_push_notification: true
                        });
                    }, function(error) {
                        kony.print("\n<----------Unsubscription Failure--------> " + JSON.stringify(error));
                    });
                }
            } catch (e) {
                kony.print("\n<----------Exception-------->" + e.message);
                if (e.message.includes("Register and try again")) {
                    kony.store.setItem("ksid", "");
                    frmSettings.pushSwitch.src = "off.png";
                    kony.store.setItem("pushSwitch", false);
                    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                    cust360Obj.appNotifications = "disabled";
                    TealiumManager.trackEvent("Opting Out for Push Notification", {
                        event_type: "registration",
                        optedout_push_notification: true
                    });
                }
                if (e) {
                    TealiumManager.trackEvent("Exception While Unsubscribing For MF Messaging", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }
};