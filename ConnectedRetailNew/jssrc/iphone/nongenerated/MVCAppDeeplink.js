var weeklyAdId = "";
var weeklyAdFormID = "";
var gblLaunchMode = 0;
var firstLaunchFromPrevKonyVersionFlag = "";
var gblIsFromImageSearch = false;
var gblFromHomeTile = false;
var formFromDeeplink = false;
var deeplinkEnabled = false;
var frombrowserdeeplink = false;
var dealId = "";

function appservicereq(params) {
    kony.print("App service request triggred");
    kony.print(JSON.stringify(params));
    var test = params;
    var lparams = test.launchparams;
    gblLaunchMode = params.launchmode;
    if (params.launchmode == 3) {
        deeplinkEnabled = true;
        var currentForm = kony.application.getCurrentForm() || "";
        var currentFormID = currentForm.id || "";
        var lMyLocationFlag = false;
        if (params.launchparams.formToOpen == "frmRewardsProgramInfo") {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.rewardTermsandConditions);
            return true;
        }
        if (params.launchparams.formToOpen == "frmPrivacyPolicy") {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
            return true;
        }
        if (params.launchparams.formToOpen == "frmHelp") {
            MVCApp.getHelpDetailsController().load();
            return true;
        }
        if (params.launchparams.formToOpen == "frmTermsConditions") {
            MVCApp.getBrowserController().load(MVCApp.serviceConstants.createTermsandConditions);
            return true;
        }
        if (params.launchparams.formToOpen == "frmSignIn") {
            rewardsInfoFlag = false;
            isFromShoppingList = false;
            MVCApp.getSignInController().load();
            return true;
        }
        if (params.launchparams.formToOpen == "frmCreateAccount") {
            fromCoupons = false;
            MVCApp.getCreateAccountController().load();
            return true;
        }
        if (params.launchparams.formToOpen == "frmWeeklyAds") {
            frombrowserdeeplink = true;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            if (fromBrowserViews) {
                MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(true);
            }
            if (lMyLocationFlag === "true") {
                loadWeeklyAds = true;
                frmWeeklyAdHome.segAds.removeAll();
                frmWeeklyAdHome.defaultAnimationEnabled = false;
                weeklyAdEntryFromVoiceSearch = false;
                MVCApp.getWeeklyAdHomeController().load();
                if (currentFormID !== "frmWeeklyAds") formFromDeeplink = true;
                return frmWeeklyAdHome;
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "weeklyAds"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
        } else if (params.launchparams.formToOpen == "frmPJDP") {
            frombrowserdeeplink = true;
            var formName = kony.application.getCurrentForm().id;
            MVCApp.Toolbox.common.clearPdpGlobalVariables();
            MVCApp.getPJDPController().setQueryString("");
            MVCApp.getPJDPController().setEntryType("deeplink");
            MVCApp.getPJDPController().load(params.launchparams.productId, false, formName);
            if (currentFormID !== "frmPJDP") formFromDeeplink = true;
            return frmPJDP;
        } else if (params.launchparams.formToOpen == "frmCoupons") {
            couponsEntryFromVoiceSearch = false;
            if (params.launchparams.fromHomeTile) {
                gblFromHomeTile = true;
            } else {
                gblFromHomeTile = false;
            }
            frmObject = frmHome;
            MVCApp.getHomeController().loadCoupons();
            frombrowserdeeplink = true;
        } else if (params.launchparams.formToOpen == "frmPDP") {
            frombrowserdeeplink = true;
            var headerText = [];
            headerText[0] = "";
            headerText[1] = "";
            MVCApp.getPDPController().setQueryString("");
            var productType = params.launchparams.product_type || "";
            var productId = params.launchparams.productId || "";
            var defaultProductId = params.launchparams.c_default_product_id || "";
            var sessionId = params.launchparams.sessionId || "";
            var invokeForm = "frmHome";
            var currForm = kony.application.getCurrentForm();
            if (currForm != null && currForm != undefined) {
                var currFormId = currForm.id || "";
                if (currFormId == "frmChatBox") {
                    MVCApp.getChatBoxController().setSession(sessionId);
                    invokeForm = "frmChatBox";
                    MVCApp.getPDPController().setFromBarcode(true);
                }
            }
            if (productType == "item") {
                if (productId !== "" || productId != "null" || productId !== null) {
                    MVCApp.getPDPController().load(productId, true, false, headerText, invokeForm);
                }
            } else if (productType == "master") {
                if ((productId !== "" || productId != "null" || productId !== null) && (defaultProductId !== "" || defaultProductId != "null" || defaultProductId !== null)) {
                    MVCApp.getPDPController().load(productId, false, false, headerText, invokeForm, defaultProductId);
                }
            } else {
                if (productId !== "" || productId != "null" || productId !== null) {
                    MVCApp.getPDPController().load(productId, true, false, headerText, invokeForm);
                }
            }
            MVCApp.getPDPController().setEntryType("deeplink");
            formFromDeeplink = true;
            return frmPDP;
        } else if (params.launchparams.formToOpen == "frmProductsList") {
            frombrowserdeeplink = true;
            var query = params.launchparams.q || "";
            var cgid = params.launchparams.cgid || "";
            var source = params.launchparams.source || ""
            var queryObj = {};
            gblIsFromImageSearch = false;
            imageSearchProjectBasedFlag = false;
            frmProductsList.segProductsList.removeAll();
            if (cgid !== "") {
                queryObj = {
                    "id": cgid
                };
                MVCApp.getProductsListController().setFromDeepLink(true);
                MVCApp.getProductsListController().load(queryObj, false);
            } else if (query !== "") {
                MVCApp.getProductsListController().load(decodeURI(decodeURI(query)), true);
            }
            if (source == "QR") {
                MVCApp.getProductsListController().setEntryType("QRCodeSearch");
            } else {
                MVCApp.getProductsListController().setEntryType("deeplink");
            }
            if (currentFormID !== "frmPJDP") {
                formFromDeeplink = true;
            }
            return frmProductsList;
        } else if (params.launchparams.formToOpen == "frmEvents") {
            if (fromBrowserViews) {
                MVCApp.getEventsController().setFormEntryFromCartOrWebView(true);
            }
            MVCApp.getEventsController().load(false);
            frombrowserdeeplink = true;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                return frmEvents;
            } else {
                var storeString = kony.store.getItem("myLocationDetails") || "";
                if (storeString != "") {
                    return frmEvents;
                }
            }
        } else if (params.launchparams.formToOpen == "frmEventDetail") {
            var reqParams = {};
            if (fromBrowserViews) {
                MVCApp.getEventDetailsController().setFormEntryFromCartOrWebView(true);
            }
            reqParams.eventid = params.launchparams.Id || "";
            MVCApp.getEventsController().load(reqParams);
            frombrowserdeeplink = true;
        } else if (params.launchparams.formToOpen == "frmWeeklyAdDetail") {
            frombrowserdeeplink = true;
            dealId = params.launchparams.Id || "";
            gblIsWeeklyAdDetailDeepLink = true;
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            if (lMyLocationFlag === "true") {
                MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId, "", weeklyAdFormID, dealId);
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "weeklyAdDetail"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
        } else if (params.launchparams.formToOpen == "frmChatBox") {
            MVCApp.getChatBoxController().load();
            frombrowserdeeplink = true;
            return frmChatBox;
        } else if (params.launchparams.formToOpen == "frmProducts") {
            MVCApp.getProductsController().updateScreen();
            MVCApp.getProductsListController().setFromDeepLink(false);
            frombrowserdeeplink = true;
            if (currentFormID !== "frmProducts") {
                formFromDeeplink = true;
            }
            return frmProducts;
        } else if (params.launchparams.formToOpen == "frmProductCategory") {
            if (fromBrowserViews) {
                MVCApp.getProductCategoryController().setFormEntryFromCartOrWebView(true);
            }
            var reqParams = {};
            reqParams.productId = params.launchparams.productId || "";
            MVCApp.getProductsListController().setFromDeepLink(false);
            MVCApp.getProductCategoryController().load(reqParams);
            frombrowserdeeplink = true;
            if (currentFormID !== "frmProductCategory") {
                formFromDeeplink = true;
            }
            return true;
        } else if (params.launchparams.formToOpen == "frmProductCategoryMore") {
            if (fromBrowserViews) {
                MVCApp.getProductCategoryMoreController().setFormEntryFromCartOrWebView(true);
            }
            var reqParams1 = {};
            MVCApp.getProductsListController().setFromDeepLink(false);
            reqParams1.id = params.launchparams.productId || "";
            reqParams1.name = "";
            reqParams1.headerImage = "";
            MVCApp.getProductCategoryMoreController().load(reqParams1);
            frombrowserdeeplink = true;
            if (currentFormID !== "frmProductCategoryMore") {
                formFromDeeplink = true;
            }
            return true;
        } else if (params.launchparams.formToOpen == "frmProjects") {
            MVCApp.getProjectsController().updateScreen();
            frombrowserdeeplink = true;
            if (currentFormID !== "frmProjects") {
                formFromDeeplink = true;
            }
            return frmProjects;
        } else if (params.launchparams.formToOpen == "frmProjectsCategory") {
            var reqParams2 = {};
            reqParams2.id = params.launchparams.projectId || "";
            reqParams2.image = "";
            reqParams2.name = "";
            MVCApp.getProjectsCategoryController().load(reqParams2);
            //  frombrowserdeeplink = true;
            if (currentFormID !== "frmProjectsCategory") {
                formFromDeeplink = true;
            }
            return frmProjectsCategory;
        } else if (params.launchparams.formToOpen == "frmWeeklyAdsList") {
            if (fromBrowserViews) {
                MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(true);
            }
            var currentForm = kony.application.getCurrentForm();
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
            }
            if (currentForm != null && currentForm != undefined) {
                weeklyAdFormID = currentForm.id;
            }
            if (lMyLocationFlag === "true") {
                weeklyAdId = params.launchparams.Id || "";
                MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId, "", weeklyAdFormID);
                frombrowserdeeplink = true;
                if (currentFormID !== "frmWeeklyAd") {
                    formFromDeeplink = true;
                }
                return frmWeeklyAd;
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "weeklyAdsList"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreWeeklyAd"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
        } else if (params.launchparams.formToOpen == "frmProjectsList") {
            var queryProjectList = {};
            queryProjectList.id = params.launchparams.Id || "";
            gblIsFromImageSearch = false;
            imageSearchProjectBasedFlag = false;
            MVCApp.getProjectsListController().setEntryType("deeplink");
            MVCApp.getProjectsListController().load(queryProjectList, false, false);
            frombrowserdeeplink = true;
            if (currentFormID !== "frmProjectsList") {
                formFromDeeplink = true;
            }
            return frmProjectsList;
        } else if (params.launchparams.formToOpen == "frmSearch") {
            var searchQuery = params.launchparams.searchText || "";
            kony.store.setItem("searchedProdId", searchQuery);
            frmProductsList.segProductsList.removeAll();
            MVCApp.getProductsListController().load(searchQuery, true);
            gblIsFromImageSearch = false;
            imageSearchProjectBasedFlag = false;
            frombrowserdeeplink = true;
            if (currentFormID !== "frmProductsList") {
                formFromDeeplink = true;
            }
            return frmProductsList;
        } else if (params.launchparams.formToOpen == "frmRewards") {
            MVCApp.getRewardsProgramInfoController().load(false);
            frombrowserdeeplink = true;
            if (currentFormID !== "frmRewardsProgramInfo") {
                formFromDeeplink = true;
            }
            return frmRewardsProgramInfo;
        } else if (params.launchparams.formToOpen == "frmHome") {
            MVCApp.searchBar.homePageLoadingHandler();
            return frmHome;
        } else if (params.launchparams.formToOpen == "frmSettings") {
            MVCApp.getSettingsController().load();
            MVCApp.tabbar.bindEvents(frmSettings);
            MVCApp.tabbar.bindEvents(frmMore);
            MVCApp.tabbar.bindIcons(frmSettings, "frmMore");
            frombrowserdeeplink = true;
            return frmSettings;
        } else if (params.launchparams.formToOpen == "frmFindStoreMapView") {
            var currentformId = "";
            try {
                currentformId = kony.application.getCurrentForm().id;
            } catch (e) {
                currentformId = "frmHome";
            }
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.entryPoint = "Deeplink";
            MVCApp.Customer360.sendInteractionEvent("ChooseStore", cust360Obj);
            MVCApp.getLocatorMapController().load(currentformId);
            frombrowserdeeplink = true;
            if (currentformId !== "frmFindStoreMapView") {
                formFromDeeplink = true;
            }
            return frmFindStoreMapView;
        } else {}
        //End of storeHours deeplinking Code.
        var currentForm = kony.application.getCurrentForm();
        if (currentForm === null) {
            currentForm = "";
        }
        return currentForm;
    } else if (params.launchmode == 2) {
        deeplinkEnabled = true;
        var currentForm = kony.application.getCurrentForm();
        appLaunchCust360Sent = true;
        kony.print("do not go to home Form 1231" + deeplinkEnabled);
    } else {
        deeplinkEnabled = false;
        if (MVCApp.Toolbox.Service.getFlagForInstallation() == "true") {
            initLaunchFlag = true;
            kony.store.setItem("firstLaunchIntallation", "true");
            kony.store.setItem("firstIntallation", "false");
            kony.store.setItem("showWhatsNew", "true");
            kony.store.setItem("firstLaunchFromPrevKonyVersionFlag", "false");
            MVCApp.getGuideController().init();
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.launchType = "appIconClick";
            cust360Obj.isFirstLaunch = "true";
            MVCApp.Customer360.sendInteractionEvent("Launch", cust360Obj);
            return frmGuide;
        } else {
            kony.store.setItem("firstLaunchIntallation", "false");
            kony.store.setItem("showWhatsNew", "false");
            var returnedValue = ApplicationState.getApplicationState();
            gblApplicationState = returnedValue;
            if (returnedValue == "active") {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.launchType = "appIconClick";
                cust360Obj.isFirstLaunch = "false";
                MVCApp.Customer360.sendInteractionEvent("Launch", cust360Obj);
                registerPushForVersion85Plus();
            }
            firstLaunchFromPrevKonyVersionFlag = kony.store.getItem("firstLaunchFromPrevKonyVersionFlag");
            if (!firstLaunchFromPrevKonyVersionFlag || (firstLaunchFromPrevKonyVersionFlag == "false" && (!isPushRegisterInvokedInPrevVersion && initLaunchFlagForVersion72))) {
                kony.store.setItem("firstLaunchFromPrevKonyVersionFlag", "false");
                MVCApp.getGuideController().init();
                return frmGuide;
            } else if (firstLaunchFromPrevKonyVersionFlag == "false") {
                if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
                    MVCApp.Toolbox.common.showLoadingIndicator("");
                    frmHome.flxSearchOverlay.flxVisual.cmrImageSearch.cameraOptions = {
                        hideControlBar: true
                    };
                    frmHome.flxHeaderWrap.cmrImageSearch.cameraOptions = {
                        hideControlBar: true
                    };
                } else {
                    homePreShowHandler();
                    MVCApp.getHomeController().load();
                }
                //return frmHome;
                //       //#ifdef iphone
                //       return frmHome;
                //       //#endif
            }
        }
    }
}