/**
 * PUBLIC
 * This is the controller for the frmWeeklyAdHome form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.WeeklyAdHomeController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.WeeklyAdHomeModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.WeeklyAdHomeView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load() {
        MVCApp.Toolbox.common.showLoadingIndicator("");
        kony.print("Weekly Ads Controller.load");
        init();
        var campaignID = "";
        var requestParams = {};
        var storeNo = "";
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeNo = gblInStoreModeDetails.storeID;
        } else {
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (storeString !== "") {
                try {
                    var storeObj = JSON.parse(storeString);
                    storeNo = storeObj.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlWeeklyAdHome.js on load for storeString:" + JSON.stringify(e)
                    }]);
                    storeNo = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception in Weekly Ad Home Flow", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                storeNo = "";
            }
        }
        var languageID = MVCApp.Service.getLanguageID();
        var locale = kony.store.getItem("languageSelected");
        if (locale == "en" || locale == "en_US") {
            campaignID = MVCApp.serviceConstants.campaignID;
        } else {
            campaignID = MVCApp.serviceConstants.canadaCampaignID;
        }
        requestParams = {
            "storeref": storeNo,
            "languageid": languageID,
            "sort": "1",
            "limit": "100",
            "campaignID": campaignID
        };
        if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
            model.getAdDepartments(view.updateScreen, requestParams);
        } else {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            view.show();
        }
    }

    function showUI() {
        init();
        view.show();
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        showUI: showUI,
        load: load
    };
});