/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.WeeklyAds = (function(json) {
    var typeOf = "WeeklyAds";
    var _data = json || {};
    var allTags = gblTagValues || [];
    /**
     * PUBLIC - setter for the model
     * Set data from the JSON retrieved by the model
     */
    function isNullOrEmpty(value) {
        if (value == undefined || value == null || value == "" || value == "null") {
            return "";
        } else {
            return value;
        }
    }

    function findTagsId(idTag) {
        var imageURL = "";
        var results = allTags;
        if (results.length != 0) {
            for (var i = 0; i < results.length; i++) {
                if (results[i].id == idTag) {
                    imageURL = results[i].name || "";
                    return imageURL;
                }
            }
        }
        return imageURL;
    }

    function getAdDepartmentsData() {
        try {
            var allDepartmentData = [];
            var departments = _data.departments || [];
            var featuredDeals = _data.results || [];
            var tempData = {};
            var classAndEventsArray = [];
            for (var i = 0; i < departments.length; i++) {
                var displayOrder = departments[i].displayOrder || "";
                if (displayOrder == "1000") {
                    var temp = departments[i];
                    var desc = temp.description || "";
                    var listingSize = departments[i].listingCount || "";
                    if (desc == "Classes" || desc == "Events") {
                        if (listingSize > 0) {
                            temp.deals = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAds.deals");
                        } else {
                            temp.deals = "";
                            temp.listingCount = "";
                        }
                        temp.data = [];
                        classAndEventsArray.push(temp);
                    } else {
                        if (listingSize > 0) {
                            temp.deals = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAds.deals");
                        } else {
                            temp.deals = "";
                            temp.listingCount = "";
                        }
                        temp.data = [];
                        allDepartmentData.push(temp);
                    }
                }
            }
            allDepartmentData = allDepartmentData.concat(classAndEventsArray);
            return allDepartmentData;
        } catch (e) {
            kony.print("Exception in getProductsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Ad Departments Data Exception in Weekly Ads Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getWeeklyAdList(headerText) {
        try {
            var weeklyAdsListData = [];
            var resultsArray = _data || [];
            for (var i = 0; i < _data.length; i++) {
                var tempData = _data[i];
                var tags = tempData.tags || "";
                var tagObject = {};
                var tagid = "";
                if (tags != "") {
                    try {
                        tagObject = JSON.parse(tags);
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "dataWeeklyAds.js on getWeeklyAdList for tags:" + JSON.stringify(e)
                        }]);
                        tags = "";
                        if (e) {
                            TealiumManager.trackEvent("Tags Parse Exception in Weekly Ads Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                }
                var tagid = tagObject.id || "";
                var imageOffer = findTagsId(tagid);
                if (imageOffer !== "" && imageOffer !== "null") {
                    tempData.offerTag = {
                        isVisible: true,
                        text: imageOffer
                    };
                    tempData.headerFlag = {
                        isVisible: true
                    };
                } else {
                    tempData.offerTag = {
                        isVisible: false
                    };
                    tempData.headerFlag = {
                        isVisible: false
                    };
                }
                var tempImage = _data[i].images || [];
                if (tempImage.length > 0) {
                    tempData.image1 = tempImage[0].imageURL || "";
                }
                var onlineURLStatus = _data[i].buyOnlineLinkURL || "";
                if (onlineURLStatus === "" || onlineURLStatus === null || onlineURLStatus == "null") {
                    tempData.storeStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAd.inStore");
                    tempData.shopOnlineButton = false;
                } else {
                    tempData.shopOnlineButton = true;
                    if (headerText == "Online Deals") {
                        tempData.storeStatus = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                    } else {
                        tempData.storeStatus = "";
                    }
                }
                var saleStartDate = _data[i].saleStartDateString || "";
                var salesEndDate = _data[i].saleEndDateString || "";
                var salesStartDateArray = saleStartDate.split(",");
                var salesEndDateArray = salesEndDate.split(",");
                if (imageOffer && imageOffer.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.WeeklyAd.EverydayValue")) != -1) {
                    tempData.dateRange = "";
                } else {
                    tempData.dateRange = salesStartDateArray[0] + " - " + salesEndDateArray[0];
                }
                tempData.title = isNullOrEmpty(_data[i].title);
                tempData.originalDeal = isNullOrEmpty(_data[i].originalDeal);
                tempData.deal = isNullOrEmpty(_data[i].deal);
                tempData.description = isNullOrEmpty(_data[i].description);
                var additionalDeal = _data[i].additionalDealInformation || ""
                if (additionalDeal == "" || additionalDeal == null || additionalDeal == "null") {
                    tempData.lblAdditionalDeal = {
                        isVisible: false
                    };
                } else {
                    tempData.lblAdditionalDeal = {
                        isVisible: true,
                        text: additionalDeal
                    };
                }
                weeklyAdsListData.push(tempData);
            }
            var tempDis = {};
            var locale = kony.store.getItem("languageSelected");
            var curr = new Date(); // get current date
            var firstday = "";
            var lastday = "";
            var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
            var last = first + 6; // last day is the first day + 6
            var startDay = first - 2;
            var text1 = "";
            var month = new Array();
            month[0] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.January");
            month[1] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.February");
            month[2] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.March");
            month[3] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.April");
            month[4] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.May");
            month[5] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.June");
            month[6] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.July");
            month[7] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.August");
            month[8] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.September");
            month[9] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.October");
            month[10] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.November");
            month[11] = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.December");
            if (locale == "en" || locale == "en_US") {
                firstday = new Date(curr.setDate(first)).toDateString();
                lastday = new Date(curr.setDate(curr.getDate() + 6)).toDateString();
                text1 = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.weeklyad.disclaimer") + " ";
                firstday = firstday.substr(4);
                lastday = lastday.substr(4);
                tempDis.txtDisclaimer1 = text1 + firstday + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.weeklyad.through") + " " + lastday;
            } else {
                var caStartMonth = new Date(curr.setDate(startDay));
                var caEndMonth = new Date(curr.setDate(curr.getDate() + 6));
                firstday = new Date(curr.setDate(startDay)).toDateString();
                lastday = new Date(curr.setDate(curr.getDate() + 6)).toDateString();
                text1 = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.weeklyad.disclaimer") + " ";
                firstday = firstday.substring(8, 10);
                lastday = lastday.substring(8, 10);
                if (locale == "en_CA") {
                    var firstDate = firstday + " " + month[caStartMonth.getMonth()] + " " + caStartMonth.getFullYear();
                    var lastDate = lastday + " " + month[caEndMonth.getMonth()] + " " + caEndMonth.getFullYear();
                    tempDis.txtDisclaimer1 = text1 + firstDate + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.weeklyad.through") + " " + lastDate;
                } else {
                    var firstDate = firstday + " " + month[caStartMonth.getMonth()] + " " + caStartMonth.getFullYear();
                    var lastDate = lastday + " " + month[caEndMonth.getMonth()] + " " + caEndMonth.getFullYear();
                    tempDis.txtDisclaimer1 = text1 + firstDate + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.weeklyad.through") + " " + lastDate;
                }
            }
            tempDis.txtDisclaimer2 = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAd.disclaimer2");
            tempDis.template = flxWeeklyAdDisclaimer;
            weeklyAdsListData.push(tempDis);
            return weeklyAdsListData;
        } catch (e) {
            kony.print("Exception in getWeeklyAdList" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Weekly Ad List Exception in Weekly Ads Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getHeaderFromResponse() {
        var deptName = "";
        for (var i = 0; i < _data.length; i++) {
            var tempData = _data[i];
            var idInput = MVCApp.getWeeklyAdController().getDepartmentID();
            var departments = tempData.departments || [];
            if (departments.length > 0) {
                for (var j = 0; j < departments.length; j++) {
                    var deptRecord = departments[j] || "";
                    var deptID = deptRecord.id;
                    deptName = deptRecord.name;
                    if (deptID == idInput) {
                        return deptName;
                    }
                }
            }
        }
        return deptName;
    }
    //Here we expose the public variables and functions
    return {
        typeOf: typeOf,
        getAdDepartmentsData: getAdDepartmentsData,
        getWeeklyAdList: getWeeklyAdList,
        getHeaderFromResponse: getHeaderFromResponse
    };
});