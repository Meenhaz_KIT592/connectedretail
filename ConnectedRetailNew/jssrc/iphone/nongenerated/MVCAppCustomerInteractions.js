//Type your code here
var MVCApp = MVCApp || {};
MVCApp.Customer360 = {};
MVCApp.Customer360.setOfURLs = {
    "Launch": "",
    "Guide-FirstScreen-WhatsNew": "guide/whatsnew",
    "WhileUsingAppLocationGuide2": "guide/whileusinglocation",
    "NotificationGuide3": "guide/notifications",
    "AlwaysAllowLocationGuide4": "guide/alwaysallowlocation",
    "BluetoothGuide5": "guide/bluetooth",
    "SignIn": "signin",
    "CreateAccount": "createaccount",
    "RewardsProfileCreationWithAccount": "createaccount/rewardsprofile",
    "Home": "home",
    "HomeBanner": "home/banner",
    "ChooseStore": "home/choosestore",
    "Favourite Store": "home/choosestore/setfavourite",
    "StoreInfo": "home/storeinfo",
    "StoreMode": "home/storemode",
    "ImageSearchLaunch": "imagesearch/launch",
    "ImageSearchCapture": "imagesearch/capture",
    "ImageSearchResult": "imagesearch/result",
    "BarcodeLaunch": "barcode/launch",
    "BarcodeCapture": "barcode/capture",
    "QRcodeCapture": "qrsearch/capture",
    "QRcodeResult": "qrsearch/result",
    "BarcodeResult": "barcode/result",
    "TextSearch": "textsearch/submit",
    "TextSearchResult": "textsearch/result",
    "Coupons": "coupons/list",
    "CouponDetail": "coupons/detail",
    "Add To Wallet": "coupons/addtowallet",
    "ProductsCategories1": "product/category-level1",
    "ProductsCategories2": "product/category-level2",
    "ProductsCategories3": "product/category-level3",
    "PLP": "product/plp",
    "PDP": "product/pdp",
    "Reviews": "product/reviews",
    "ShareProduct": "product/share",
    "ZoomProduct": "product/zoom",
    "AddProductToMyList": "product/addtomylist",
    "ProductBuyOnline": "product/buyonline",
    "CheckNearByStores": "product/checknearby",
    "CheckNearByStoresResults": "product/checknearby/results",
    "ProductLocationInStore": "product/storemap",
    "ProjectsCategories1": "project/category-level1",
    "ProjectsCategories2": "project/category-level2",
    "PJLP": "project/pjlp",
    "PJDP": "project/pjdp",
    "ZoomProject": "project/zoom",
    "ShareProject": "project/share",
    "AddProjectToMyList": "project/addtomylist",
    "ProjectBuyOnline": "project/buyonline",
    "WeeklyAdCategories": "weeklyad/categories",
    "WeeklyAdCategoryList": "weeklyad/category-list",
    "WeeklyAdDetailsPage": "weeklyad/details",
    "ShareWeeklyad": "weeklyad/share",
    "MyList": "mylist",
    "DeleteFromMyList": "mylist/delete",
    "More": "more",
    "StoreMap": "more/storemap",
    "RewardsProfile": "more/rewards/profile",
    "RewardsProfileUpdate": "more/rewards/updateprofile",
    "RewardsProgramInfo": "more/rewards/programinfo",
    "RewardsFAQ": "more/rewards/faq",
    "RewardsTermsAndConditions": "more/rewards/tnc",
    "EventsList": "more/events/list",
    "EventDetails": "more/events/details",
    "Settings": "more/settings",
    "NotificationToggle": "more/settings/notifications",
    "TouchIdToggle": "more/settings/touchid",
    "RegionLanguageChange": "more/settings/region-language",
    "TermsAndConditions": "more/settings/tnc",
    "PrivacyPolicy": "more/settings/privacy",
    "SignOut": "more/settings/signout",
    "Help": "more/help",
    "EmailCustomerService": "more/help/email",
    "CallCustomerService": "more/help/call",
    "ReviewApp": "more/help/reviewapp",
    "SearchFromCheckNearby": "product/checknearby/search",
    "DeleteAll": "mylist/deleteall",
    "SearchFromCheckNearbyResults": "product/checknearby/search/results",
    "DeleteAllResult": "mylist/deleteall/result",
    "YoutubeVideos": "youtubeVideos",
    "VoiceSearchInitialise": "voiceSearch/init",
    "VoiceSearchUtterance": "voiceSearch/utterance",
    "VoiceSearchResult": "voiceSearch/result"
};
MVCApp.Customer360.getURL = function(touchPoint) {
    var cust360url = "";
    cust360url += "ios://michaelsapp/";
    cust360url += MVCApp.Customer360.setOfURLs[touchPoint];
    return cust360url;
};
MVCApp.Customer360.getGlobalAttributes = function() {
    var locale = MVCApp.Toolbox.Service.getLocale();
    if (locale == "default") {
        locale = "en-US";
    }
    var global360attributes = {};
    global360attributes.locale = locale;
    return global360attributes; //will update the code once we know if we have to send global properties
}
MVCApp.Customer360.sendInteractionEvent = function(touchPointKey, properties) {
    //   var propToBeSent;
    //   //#ifdef android
    //   if(properties!==null){
    //     propToBeSent=JSON.stringify(properties);
    //   }else{
    //     propToBeSent="";
    //   }
    //   //#endif
    //   //#ifdef iphone
    //   propToBeSent=properties;
    //   //#endif
    //   var touchPoint= MVCApp.Customer360.getURL(touchPointKey);
    //   kony.print("touchPoint is "+touchPoint);
    //   if(OneManager){
    //     OneManager.sendInteraction(
    //       /**String*/ touchPoint, 
    //       /**Object*/ propToBeSent, 
    //       /**Function*/ MVCApp.Customer360.successCB);
    //   }
};
MVCApp.Customer360.successCB = function(results) {
    kony.print("the event has been set");
};
MVCApp.Customer360.sendBaseTouchPoint = function(properties) {
    //   //#ifdef android
    //   if(OneManager){
    //     properties= JSON.stringify(properties);
    //     OneManager.sendBaseTouchPoints(properties,MVCApp.Customer360.successCB);
    //   }
    //   //#else
    //   if(OneManager){
    //   	OneManager.sendBaseTouchPoints(properties);
    //   }
    //   //#endif
};
MVCApp.Customer360.callVoiceSearchResult = function(result, failureCode, pageDisplayed, numberOfResults) {
    var conversionResult = "App SearchSuccessful";
    //   var cust360Obj=MVCApp.Customer360.getGlobalAttributes();
    var conversionResult = "App SearchSuccessful";
    var status = "successful";
    var searchTerm = MVCApp.Toolbox.common.getSearchTermForAnalytics();
    var intent = MVCApp.Toolbox.common.getIntentForAnalytics();
    var totalCount = 0;
    if (result == "success") {
        //     cust360Obj.pageDisplayed=pageDisplayed;
        if (intent == "showProducts" || intent === "") {
            if (pageDisplayed == "PDP") {
                totalCount = 1;
            } else {
                totalCount = numberOfResults || 0;
            }
        } else if (intent == "showProjects") {
            totalCount = numberOfResults || 0;
        } else {
            totalCount = 0;
        }
    } else if (result == "failure") {
        //     cust360Obj.failureCode=failureCode;
        conversionResult = "App SearchUnsuccessful";
        status = "unsuccessful";
        totalCount = 0;
    }
    //   cust360Obj.searchTerm=searchTerm;
    //   cust360Obj.intent=intent;
    //   cust360Obj.storeId=MVCApp.Toolbox.common.getStoreID();
    //   cust360Obj.result=result;
    //   MVCApp.Customer360.sendInteractionEvent("VoiceSearchResult", cust360Obj);
    TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {
        conversion_category: conversionResult,
        conversion_id: "VoiceSearch",
        conversion_action: 2,
        voice_search_utterance: recoText,
        voice_search_intent: intent,
        voice_search_term: searchTerm,
        voice_search_results: totalCount,
        voice_search_status: status
    });
};