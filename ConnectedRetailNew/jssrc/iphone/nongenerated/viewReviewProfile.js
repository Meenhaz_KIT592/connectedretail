/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
updateFlag = false;
MVCApp.ReviewProfileView = (function() {
    /**
     * PUBLIC
     * Open the Events form
     */
    function show() {
        frmReviewProfile.flxMainContainer.contentOffset = {
            x: 0,
            y: 0
        };
        frmReviewProfile.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmReviewProfile.postShow = function() {
            MVCApp.Toolbox.common.setBrightness("frmReviewProfile");
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Rewards Profile: Michaels Mobile App";
            lTealiumTagObj.page_name = "Rewards Profile: Michaels Mobile App";
            lTealiumTagObj.page_type = "Profile";
            lTealiumTagObj.page_category_name = "Profile";
            lTealiumTagObj.page_category = "Profile";
            TealiumManager.trackView("Rewards Profile screen", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            MVCApp.Customer360.sendInteractionEvent("RewardsProfile", cust360Obj);
        };
        frmReviewProfile.btnYes.onClick = navigateToMoreScreen;
        frmReviewProfile.flxOverlay.onTouchEnd = dummyOverlay;
        frmReviewProfile.rtxtAgreeConditions.onClick = showTermsandConditions;
        MVCApp.tabbar.bindEvents(frmReviewProfile);
        MVCApp.tabbar.bindIcons(frmReviewProfile, "frmMore");
        frmReviewProfile.btnHeaderLeft.onClick = showPreviousScreen;
        frmReviewProfile.btnPersonal.onClick = function() {
            MVCApp.getRewardsProfileController().updateData(true);
        };
        frmReviewProfile.btnAddress.onClick = function() {
            MVCApp.getRewardsProfileAdrsController().updateData(true);
        };
        frmReviewProfile.btnChildren.onClick = function() {
            MVCApp.getRewardsProfileChildController().updateData(true);
        };
        frmReviewProfile.btnSkills.onClick = function() {
            MVCApp.getRewardsProfileSkillsController().updateData(true);
        };
        frmReviewProfile.btnInterestsEdit.onClick = function() {
            MVCApp.getRewardsProfileInterestsController().updateData(true);
        };
        frmReviewProfile.btnAgree.onClick = onClickAgree;
        frmReviewProfile.btnCompleteProfile.onClick = onClickCompleteProfile;
        frmReviewProfile.btnClose.onClick = MVCApp.getMoreController().load;
        frmReviewProfile.btnStore.onClick = function() {
            MVCApp.getRewardsProfilePreferedStoreController().load(true);
        };
    }

    function disableForCanada() {
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            frmReviewProfile.flxChild.setVisibility(true);
            frmReviewProfile.CopyFlexContainer0093217c7c1ee45.setVisibility(true);
            // frmReviewProfile.flxInterests.setVisibility(true);
            frmReviewProfile.flxInterestsvalues.setVisibility(true);
            frmReviewProfile.flxInterestsSkills.setVisibility(true);
            frmReviewProfile.CopyFlexContainer087d3b7dd178c44.setVisibility(true);
            frmReviewProfile.CopyFlexContainer00066b8ed64c042.setVisibility(true);
            frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(true);
        } else {
            frmReviewProfile.flxChild.setVisibility(false);
            frmReviewProfile.CopyFlexContainer0093217c7c1ee45.setVisibility(false);
            frmReviewProfile.flxInterests.setVisibility(false);
            frmReviewProfile.flxInterestsvalues.setVisibility(false);
            frmReviewProfile.flxInterestsSkills.setVisibility(false);
            frmReviewProfile.CopyFlexContainer087d3b7dd178c44.setVisibility(false);
            frmReviewProfile.CopyFlexContainer00066b8ed64c042.setVisibility(false);
            frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
        }
    }

    function showTermsandConditions() {
        MVCApp.getBrowserController().load(MVCApp.serviceConstants.createTermsandConditions);
    }

    function dummyOverlay() {}

    function onClickAgree() {
        if (frmReviewProfile.imgCheckAgree.src == "checkbox_off.png") {
            frmReviewProfile.imgCheckAgree.src = "checkbox_on.png";
            frmReviewProfile.btnCompleteProfile.skin = "sknBtnRed";
            frmReviewProfile.btnCompleteProfile.focusSkin = "sknBtnRed";
        } else {
            frmReviewProfile.imgCheckAgree.src = "checkbox_off.png";
            frmReviewProfile.btnCompleteProfile.skin = "sknBtnDisable";
            frmReviewProfile.btnCompleteProfile.focusSkin = "sknBtnDisable";
        }
    }

    function updateScreen() {
        disableForCanada();
        frmReviewProfile.Label0c5601159c0bb4c.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.personal");
        frmReviewProfile.CopyLabel086c5f3f8403d48.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.address");
        frmReviewProfile.rtxtAgreeConditions.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.termsAndConditions");
        frmReviewProfile.btnCompleteProfile.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.completeProfile");
        frmReviewProfile.lblFormTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.reviewprofile");
        frmReviewProfile.btnPersonal.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnAddress.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnChildren.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.flxFooterWrap.lblProducts.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblProducts");
        frmReviewProfile.flxFooterWrap.lblProjects.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblProjects");
        frmReviewProfile.flxFooterWrap.lblWeeklyAd.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblWeeklyAd");
        frmReviewProfile.flxFooterWrap.lblMyLists.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblMyLists");
        frmReviewProfile.flxFooterWrap.lblEvents.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblEvents");
        frmReviewProfile.flxFooterWrap.lblMore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblMore");
        frmReviewProfile.btnClose.text = MVCApp.Toolbox.common.geti18nValueVA("i18.review.close");
        frmReviewProfile.btnCompleteProfile.skin = "sknBtnDisable";
        frmReviewProfile.btnCompleteProfile.focusSkin = "sknBtnDisable";
        //Personal data
        if (rewardsProfileData.firstName !== undefined) {
            frmReviewProfile.lblName.text = rewardsProfileData.firstName + " " + rewardsProfileData.lastName;
            frmReviewProfile.lblEmail.text = rewardsProfileData.email;
            frmReviewProfile.lblDOB.text = rewardsProfileData.birthday;
            frmReviewProfile.lblPhone.text = rewardsProfileData.phoneNo;
        }
        //Adress data
        if (rewardsProfileData.address1 !== undefined) {
            frmReviewProfile.lblAddress1.text = rewardsProfileData.address1;
            if (rewardsProfileData.address2 !== "" && rewardsProfileData.address2 !== null && rewardsProfileData.address2 !== "null") {
                frmReviewProfile.lblAddress2.setVisibility(true);
                frmReviewProfile.lblAddress2.text = rewardsProfileData.address2;
            } else {
                frmReviewProfile.lblAddress2.setVisibility(false);
            }
            frmReviewProfile.lblCity.text = rewardsProfileData.city + ", " + rewardsProfileData.state;
            frmReviewProfile.lblStateZip.text = rewardsProfileData.zip;
        }
        //Child Data
        if (rewardsProfileData.child !== undefined) {
            var data = rewardsProfileData;
            var childData = [];
            var childarr = data.child;
            var childFlag = false;
            var childStr;
            for (var i = 0; i < childarr.length; i++) {
                var temp = [];
                if (childarr[i].txtChild.text != undefined && childarr[i].txtChild.text != "") {
                    childFlag = true;
                    temp.lblReviewProfile = childarr[i].lblChild;
                    childStr = childarr[i].txtChild.text + "";
                    temp.lblReviewProfileNo = childStr;
                    childData.push(temp);
                }
            }
            frmReviewProfile.segData.data = childData;
        }
        //Interests Data
        if (rewardsProfileData.c_surveyQA !== undefined && rewardsProfileData.c_surveyQA[0] !== "defaults" && rewardsProfileData.c_surveyQA.length !== 0) {
            var intrst = rewardsProfileData.c_surveyQA;
            var intrstArray = intrst[0].split(":");
            frmReviewProfile.lblQuestion.text = intrstArray[0];
            frmReviewProfile.lblAnswer.text = intrstArray[1];
            if (intrstArray[1] === "" || intrstArray[1] === null) frmReviewProfile.flxQuestions.isVisible = false;
            else frmReviewProfile.flxQuestions.isVisible = true;
        } else {
            frmReviewProfile.flxQuestions.isVisible = false;
        }
        //Skills Data
        if (rewardsProfileData.displaySkillData !== undefined) {
            var skills = rewardsProfileData.displaySkillData;
            var skillFlag = false;
            var skillData = [];
            for (var j = 0; j < skills.length; j++) {
                var temp1 = [];
                if (skills[j].skillMapValue != undefined && skills[j].skillMapValue != "") {
                    skillFlag = true;
                    temp1.lblReviewProfile = skills[j].name;
                    temp1.lblReviewProfileNo = skills[j].skillMapValue;
                    skillData.push(temp1);
                }
            }
            frmReviewProfile.segSkills.data = skillData;
        }
        var data1 = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
        var datFalg = data1.getData();
        if (datFalg) {
            frmReviewProfile.flexTermsAndConditions.setVisibility(true);
            frmReviewProfile.btnHeaderLeft.setVisibility(false);
            frmReviewProfile.lblChevron.setVisibility(false);
            frmReviewProfile.btnCompleteProfile.setVisibility(true);
            frmReviewProfile.btnCompleteProfile.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.rewardsProfile.updateProfile");
            frmReviewProfile.imgCheckAgree.src = "checkbox_on.png";
            frmReviewProfile.flexLastLine.setVisibility(true);
            frmReviewProfile.btnClose.setVisibility(true);
            frmReviewProfile.btnCompleteProfile.skin = "sknBtnRed";
            frmReviewProfile.btnCompleteProfile.focusSkin = "sknBtnRed";
            frmReviewProfile.lblFormTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.reviewprofile");
        } else {
            frmReviewProfile.btnHeaderLeft.setVisibility(true);
            frmReviewProfile.lblChevron.setVisibility(true);
            frmReviewProfile.flexTermsAndConditions.setVisibility(true);
            frmReviewProfile.btnCompleteProfile.setVisibility(true);
            frmReviewProfile.btnCompleteProfile.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.completeProfile");
            frmReviewProfile.imgCheckAgree.src = "checkbox_off.png";
            frmReviewProfile.flexLastLine.setVisibility(true);
            frmReviewProfile.btnClose.setVisibility(false);
            frmReviewProfile.btnCompleteProfile.skin = "sknBtnDisable";
            frmReviewProfile.btnCompleteProfile.focusSkin = "sknBtnDisable";
            frmReviewProfile.lblFormTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.reviewprofile");
        }
        //Preffered Store
        var favStore = rewardsFavouriteStore;
        if (favStore != "" && favStore != null && favStore != undefined) {
            try {
                var favStoreObj = JSON.parse(favStore);
                if (favStoreObj != "") {
                    var storeData = new MVCApp.data.dataRewardsPreferedStore();
                    var favData = storeData.getFavouriteStoreData();
                    if (favData.length > 0) {
                        favData = favData[0];
                    }
                    frmReviewProfile.CopyLabel055a9a5cadcf54f.text = favData.lbladdress1 || "";
                    frmReviewProfile.CopyLabel015c44ef24d2d48.text = favData.lbladdress2 || "";
                    frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(true);
                } else {
                    frmReviewProfile.CopyLabel055a9a5cadcf54f.text = "";
                    frmReviewProfile.CopyLabel015c44ef24d2d48.text = "";
                    frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
                }
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewReviewProfile.js on updateScreen for favStore:" + JSON.stringify(e)
                }]);
                frmReviewProfile.CopyLabel055a9a5cadcf54f.text = "";
                frmReviewProfile.CopyLabel015c44ef24d2d48.text = "";
                frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
                if (e) {
                    TealiumManager.trackEvent("Exception While Updating The Review Profile Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            frmReviewProfile.CopyLabel055a9a5cadcf54f.text = "";
            frmReviewProfile.CopyLabel015c44ef24d2d48.text = "";
            frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
        }
        show();
    }

    function showPreviousScreen() {
        //frmRewardsProfileStep4.flxMainContainer.contentOffset = {x: 0,	y: 0 };
        var data = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
        var datFalg = data.getData();
        if (datFalg) {
            MVCApp.getMoreController().load();
        } else {
            if (MVCApp.Toolbox.common.getRegion() == "US") MVCApp.getRewardsProfilePreferedStoreController().load();
            else MVCApp.getRewardsProfileAdrsController().updateData();
        }
    }

    function onClickCompleteProfile() {
        if (frmReviewProfile.btnCompleteProfile.skin == "sknBtnRed" && updateFlag === true) {
            MVCApp.getReviewProfileController().updateRewardProfile();
        } else if (frmReviewProfile.btnCompleteProfile.skin == "sknBtnRed") {
            frmReviewProfile.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.rewardsProfile.noInfoEntered");
            frmReviewProfile.lbl1.setVisibility(false);
            frmReviewProfile.flxOverlay.isVisible = true;
        }
    }

    function updateRewardProfile() {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        frmReviewProfile.flexTermsAndConditions.setVisibility(false);
        frmReviewProfile.btnCompleteProfile.setVisibility(false);
        frmReviewProfile.flexLastLine.setVisibility(false);
        frmReviewProfile.btnClose.setVisibility(true);
        frmReviewProfile.btnHeaderLeft.setVisibility(false);
        frmReviewProfile.lblChevron.setVisibility(false);
        frmReviewProfile.lblFormTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.reviewprofile");
        var favStore = rewardsFavouriteStore;
        rewardsDefaultFavouriteStore = favStore;
    }

    function confirmationAlertMessage() {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        var data = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
        var datFalg = data.getData();
        if (datFalg) {
            frmReviewProfile.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.rewardsProfile.updatedMessage");
            frmReviewProfile.lbl1.isVisible = false;
            frmReviewProfile.flxOverlay.isVisible = true;
        } else {
            if (kony.application.getCurrentForm()) {
                var lCurrentForm = kony.application.getCurrentForm();
                if (lCurrentForm.id === "frmReviewProfile") {
                    lCurrentForm.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.rewardsProfile.confirmMessage");
                    var userData = kony.store.getItem("userObj");
                    TealiumManager.trackEvent("User Registered for Rewards Profile", {
                        event_type: "registration",
                        user_registered: true,
                        customer_email: userData.email,
                        loyalty_id: userData.c_loyaltyMemberID,
                        customer_id: userData.customer_id
                    });
                } else if (lCurrentForm.id === "frmRewardsProfileStep2" || lCurrentForm.id === "frmRewardsProfileStep4" || lCurrentForm.id === "frmRewardsProfileAddChild" || lCurrentForm.id === "frmRewardsProfileSkills") {
                    lCurrentForm.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.rewardsProfile.exitFinish");
                }
                lCurrentForm.lbl1.isVisible = false;
                lCurrentForm.flxOverlay.isVisible = true;
            }
        }
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        MVCApp.Customer360.sendInteractionEvent("RewardsProfileUpdate", cust360Obj);
    }

    function navigateToReviewProfile() {
        MVCApp.getReviewProfileController().sumary();
    }

    function navigateToMoreScreen() {
        frmReviewProfile.flxOverlay.isVisible = false;
        if (updateFlag === false) {
            MVCApp.getReviewProfileController().sumary();
        } else MVCApp.getMoreController().load();
    }

    function sumary() {
        var data = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
        data.sumary();
        updateFlag = false;
        frmReviewProfile.flexTermsAndConditions.setVisibility(true);
        frmReviewProfile.btnHeaderLeft.setVisibility(false);
        frmReviewProfile.lblChevron.setVisibility(false);
        frmReviewProfile.btnCompleteProfile.setVisibility(true);
        frmReviewProfile.btnCompleteProfile.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.rewardsProfile.updateProfile");
        frmReviewProfile.imgCheckAgree.src = "checkbox_on.png";
        frmReviewProfile.flexLastLine.setVisibility(true);
        frmReviewProfile.btnClose.setVisibility(true);
        frmReviewProfile.btnCompleteProfile.skin = "sknBtnRed";
        frmReviewProfile.btnCompleteProfile.focusSkin = "sknBtnRed";
        frmReviewProfile.lblFormTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.reviewprofile");
        frmReviewProfile.Label0c5601159c0bb4c.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.personal");
        frmReviewProfile.CopyLabel086c5f3f8403d48.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.address");
        frmReviewProfile.rtxtAgreeConditions.text = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.termsAndConditions");
        //frmReviewProfile.btnCompleteProfile.text=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.completeProfile"); 
        // frmReviewProfile.lblFormTitle.text=MVCApp.Toolbox.common.geti18nValueVA("i18.phone.rewards.reviewprofile"); 
        frmReviewProfile.btnPersonal.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnAddress.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnClose.text = MVCApp.Toolbox.common.geti18nValueVA("i18.review.close");
        frmReviewProfile.btnChildren.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnSkills.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.btnStore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.edit");
        frmReviewProfile.flxFooterWrap.lblProducts.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblProducts");
        frmReviewProfile.flxFooterWrap.lblProjects.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblProjects");
        frmReviewProfile.flxFooterWrap.lblWeeklyAd.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblWeeklyAd");
        frmReviewProfile.flxFooterWrap.lblMyLists.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblMyLists");
        frmReviewProfile.flxFooterWrap.lblEvents.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblEvents");
        frmReviewProfile.flxFooterWrap.lblMore.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.masterFooter.lblMore");
        disableForCanada();
        show();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        updateRewardProfile: updateRewardProfile,
        sumary: sumary,
        confirmationAlertMessage: confirmationAlertMessage
    };
});