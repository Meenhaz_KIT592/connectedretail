/**
 * PUBLIC
 * This is the view for the Products form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProductsView = (function() {
    var noWidgets = -1;
    /**
     * PUBLIC
     * Open the Products form
     */
    function show() {
        frmProducts.flxMainContainer.removeAll();
        noWidgets = -1;
        frmProducts.flxHeaderWrap.textSearch.text = "";
        frmProducts.flxSearchResultsContainer.setVisibility(false);
        frmProducts.defaultAnimationEnabled = false;
        frmProducts.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        MVCApp.tabbar.bindEvents(frmProducts);
        MVCApp.tabbar.bindIcons(frmProducts, "frmProducts");
        MVCApp.searchBar.bindEvents(frmProducts, "frmProducts");
        frmProducts.flxMainContainer.onScrollStart = onreachWidgetEnd;
        frmProducts.flxMainContainer.onScrollEnd = onreachScrollEnd;
        frmProducts.flxTransLayout.onClick = function() {
            kony.print("do nothing");
        };
        frmProducts.preShow = function() {
            frmProducts.flxSearchOverlay.setVisibility(false);
            frmProducts.flxVoiceSearch.setVisibility(false);
        };
        frmProducts.postShow = function() {
            var cartValue = MVCApp.Toolbox.common.getCartItemValue();
            frmProducts.flxHeaderWrap.lblItemsCount.text = cartValue;
            MVCApp.Toolbox.common.setBrightness("frmProducts");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Shop Navigation: Michaels Mobile App";
            lTealiumTagObj.page_name = "Shop Navigation: Michaels Mobile App";
            lTealiumTagObj.page_type = "Navigation";
            lTealiumTagObj.page_category_name = "Shop";
            lTealiumTagObj.page_category = "Shop";
            //artf5479
            lTealiumTagObj.category_ID = "Shop";
            lTealiumTagObj.page_category_name = "";
            TealiumManager.trackView("Shop Tab", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            if (!formFromDeeplink) MVCApp.Customer360.sendInteractionEvent("ProductsCategories1", cust360Obj);
            else formFromDeeplink = false;
        };
    }

    function displaySamePage() {
        frmProducts.defaultAnimationEnabled = true;
        MVCApp.Toolbox.common.setTransition(frmProducts, 3);
        frmProducts.show();
    }

    function displaySameScreen() {
        frmProducts.flxFooterWrap.bottom = "0dp";
        frmProducts.show();
    }

    function updateScreen(results) {
        var productsList = results.getProductsListData();
        MVCApp.getProductsController().setGlblProductsResult(results);
        kony.store.setItem("GetProductCategories", productsList); // this is kept for to getsubcategories 
        if (productsList.length > 0) {
            var flxTopSpacerDND = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTopSpacerDND",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
            frmProducts.flxMainContainer.add(flxTopSpacerDND);
            for (var i = 0; i < productsList.length; i++) {
                var bgImage = productsList[i].c_mobileThumbnail || "";
                var flxDepartment1 = new kony.ui.FlexContainer({
                    "autogrowMode": kony.flex.AUTOGROW_NONE,
                    "centerX": "50%",
                    "clipBounds": true,
                    "height": "42dp",
                    "id": "flxDepartment1" + i,
                    "isVisible": false,
                    "layoutType": kony.flex.FREE_FORM,
                    "skin": "sknFlexBgLtGray",
                    "top": "10dp",
                    "width": "100%",
                    "zIndex": 1
                }, {}, {});
                flxDepartment1.setDefaultUnit(kony.flex.DP);
                //     var Image04716670c6f474e = new kony.ui.Image2({
                //         "id": "Image04716670c6f474e"+i,
                //         "imageWhenFailed": "notavailable_1080x142",
                //         "imageWhileDownloading": "white_1080x142",
                //         "isVisible": true,
                //       	"height": "42dp",
                //         "left": "0dp",
                //         "skin": "slImage",
                //         "src": bgImage,
                //         "top": "0dp",
                //         "width": "100%",
                //         "zIndex": 1
                //     }, {
                //         "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                //         "padding": [0, 0, 0, 0],
                //         "paddingInPixel": false
                //     }, {});
                var lblBasic04716670c6f474e = {
                    "id": "lblBasic04716670c6f474e" + i,
                    "skin": "sknLblIcon34px999999",
                    "text": "K",
                    "isVisible": true,
                    "right": "10dp",
                    "zIndex": 1,
                    "top": "0dp",
                    "width": "20%",
                    "height": "100%"
                };
                var lblLayout = {
                    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                    "padding": [0, 0, 3, 0],
                    "paddingInPixel": false
                };
                var lblLayout1 = {
                    "renderAsAnchor": true
                };
                var lbl = new kony.ui.Label(lblBasic04716670c6f474e, lblLayout, lblLayout1);
                var btnCategoryName = new kony.ui.Button({
                    "height": "100%",
                    "focusSkin": "sknLblGothamRegular32pxDarkGray",
                    "id": "btnCategoryName" + i,
                    "isVisible": true,
                    "left": "10dp",
                    "onClick": onSelectProductCategory,
                    "skin": "sknLblGothamRegular32pxDarkGray",
                    "text": productsList[i].name,
                    "top": "0dp",
                    "width": "98%",
                    "centerY": "50%",
                    "zIndex": 4
                }, {
                    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                    "displayText": true,
                    "padding": [0, 0, 3, 0],
                    "paddingInPixel": false
                }, {
                    "showProgressIndicator": false
                }, {});
                flxDepartment1.add(btnCategoryName, lbl);
                frmProducts.flxMainContainer.add(flxDepartment1);
            }
            var flxBottomSpacerDND = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxBottomSpacerDND",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
            frmProducts.flxMainContainer.add(flxBottomSpacerDND);
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            for (var j = 0; j < productsList.length; j++) {
                var data = {};
                data.productId = productsList[j].id;
                data.name = productsList[j].name;
                data.fullName = productsList[j].fullName;
                data.c_CategoryUrl = productsList[j].c_CategoryUrl;
                data.image = productsList[j].c_mobileThumbnail || "";
                frmProducts["btnCategoryName" + j].info = {
                    "data": data
                };
            }
            //frmProducts.flxTransLayout.setVisibility(true);
            MVCApp.Toolbox.common.dispFooter();
            frmProducts.flxFooterWrap.bottom = "0dp";
            kony.timer.schedule("ProductsAnimation", AnimCallback, 0.1, false);
        } else {
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmproducts.noProdFound"));
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
    }

    function onSelectProductCategory(eventObj) {
        kony.store.setItem("currentProdCategory", eventObj.info.data);
        gblIsFromImageSearch = false;
        imageSearchProjectBasedFlag = false;
        frombrowserdeeplink = false;
        MVCApp.getProductCategoryController().setFormEntryFromCartOrWebView(false);
        MVCApp.getProductCategoryController().load(eventObj.info["data"]);
    }

    function AnimCallback() {
        noWidgets = noWidgets + 1;
        if (noWidgets <= (frmProducts.flxMainContainer.widgets().length - 3)) {
            var ref = "flxDepartment1" + noWidgets;
            animate(frmProducts[ref]);
        } else {
            noWidgets = -1;
        }
    }

    function animate(widgetRef) {
        if (widgetRef) {
            var trans100 = kony.ui.makeAffineTransform();
            trans100.rotate3D(0, 1, 0, 0);
            trans100.setPerspective(900.0);
            var trans000 = kony.ui.makeAffineTransform();
            trans000.rotate3D(90, 1, 0, 0);
            trans000.setPerspective(900.0);
            widgetRef.isVisible = true;
            var animDuration = 0.07;
            widgetRef.animate(kony.ui.createAnimation({
                "0": {
                    "anchorPoint": {
                        "x": 0.5,
                        "y": 0.1
                    },
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans000,
                    "opacity": 0
                },
                "100": {
                    "anchorPoint": {
                        "x": 0.5,
                        "y": 0.5
                    },
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100,
                    "opacity": 1
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": animDuration
            }, {
                "animationEnd": AnimCallback
            });
        }
    }
    /**
     * @function
     *
     */
    function showHideMenuBar() {
        try {
            var offsetY = frmProducts.flxMainContainer.contentOffsetMeasured.y;
            var contentHeight = frmProducts.flxMainContainer.contentSizeMeasured.height;
            var viewPortHeight = frmProducts.flxMainContainer.frame.height;
            if (contentHeight == offsetY + viewPortHeight) {
                var form = kony.application.getCurrentForm();
                form.flxFooterWrap.animate(kony.ui.createAnimation({
                    "100": {
                        "stepConfig": {
                            "timingFunction": kony.anim.EASE
                        },
                        "bottom": "0dp"
                    }
                }), {
                    "delay": 0,
                    "iterationCount": 1,
                    "fillMode": kony.anim.FILL_MODE_FORWARDS,
                    "duration": 0.5
                });
            } else if (Number(offsetY) == 0) {
                var form = kony.application.getCurrentForm();
                form.flxFooterWrap.animate(kony.ui.createAnimation({
                    "100": {
                        "stepConfig": {
                            "timingFunction": kony.anim.EASE
                        },
                        "bottom": "0dp"
                    }
                }), {
                    "delay": 0,
                    "iterationCount": 1,
                    "fillMode": kony.anim.FILL_MODE_FORWARDS,
                    "duration": 0.5
                });
            } else if (contentHeight != offsetY + viewPortHeight) {
                var form = kony.application.getCurrentForm();
                form.flxFooterWrap.animate(kony.ui.createAnimation({
                    "100": {
                        "stepConfig": {
                            "timingFunction": kony.anim.EASE
                        },
                        "bottom": "-60dp"
                    }
                }), {
                    "delay": 0,
                    "iterationCount": 1,
                    "fillMode": kony.anim.FILL_MODE_FORWARDS,
                    "duration": 0.5
                });
            }
        } catch (e) {
            alert("exception---" + e);
            if (e) {
                TealiumManager.trackEvent("Exception On Manipulating The Menu Bar To Hide Or Show In Shop Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        displaySamePage: displaySamePage,
        showHideMenuBar: showHideMenuBar,
        displaySameScreen: displaySameScreen
    };
});