/**
 * PUBLIC
 * This is the view for the StoreMap form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.StoreLocatorView = (function() {
    var storeID = "";
    var gblInfo = {};
    var gblOtherResults = false;
    var gblPolyLineArr = [];
    /**
     * PUBLIC
     * Open the StoreMap form
     */
    var widgetData = {
        lblAddressLine1: "lbladdress1",
        lblAddressLine2: "lbladdress2",
        lblPhone: "lblPhone",
        lblDistance: "distanceinMiles",
        btnDirections: "btnDirections",
        lblStockTitle: "lblStockText",
        lblStock: "lblStockNo",
        flxStockInfo: "flxStockInfo",
        flxDirections: "flxDirections",
        lblDay1Time: "mon_hrs_special",
        lblDay2Time: "tue_hrs_special",
        lblDay3Time: "wed_hrs_special",
        lblDay4Time: "thu_hrs_special",
        lblDay5Time: "fri_hrs_special",
        lblDay6Time: "sat_hrs_special",
        lblDay7Time: "sun_hrs_special",
        flxItemLocation: "flxItemLocation",
        flxStoreDetailsExpand: "flxStoreDetailsExpand",
        btnFavorite: "btnFavorite",
        btnExpand: "btnExpand",
        flxExpandDetailsNew: "flxExpand",
        lblStoreHoursNew: "lblStoreHoursNew",
        btnCollapse: "btnCollapse",
        lblAvailability: "lblAvailability",
        btnStoreMap: "btnStoreMap",
        lblStoreHours: "lblStoreHours",
        lblComingSoon: "lblComingSoon",
        flexComingSoon: "flexComingSoon",
        lblNameStore: "lblNameStore",
        flexFlag: "flexFlag",
        "lblTapToSelect": "lblTapToSelect"
    };

    function show() {
        clearText();
        var showList = MVCApp.getLocatorMapController().getCheckNearby();
        var country = MVCApp.Toolbox.common.getRegion();
        frmFindStoreMapView.lblCountry.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.common.address.country");
        if (country === "US") {
            frmFindStoreMapView.imageCountry.src = "flag_united_states.png";
        } else if (country === "CA") {
            frmFindStoreMapView.imageCountry.src = "flag_canada.png";
        }
        if (showList) {
            showListView();
            frmFindStoreMapView.lblAvailabilityMap.setVisibility(true);
            frmFindStoreMapView.flxItemLocationMap.setVisibility(true);
        } else {
            frmFindStoreMapView.flxTabs.setVisibility(true);
            frmFindStoreMapView.lblAvailabilityMap.setVisibility(false);
            frmFindStoreMapView.lblCountry.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.common.address.country");
            //frmFindStoreMapView.flxItemLocationMap.setVisibility(false);
            gbllocpermission = redirectToSettings.getLocationSettingsStatus();
            if (gbllocpermission == -1 && !gblislocationon && (!MVCApp.Toolbox.Service.getMyLocationFlag() || MVCApp.Toolbox.Service.getMyLocationFlag() === "false")) {
                try {
                    gblislocationon = true;
                    frmFindStoreMapView.flxLocationPrompt.isVisible = true;
                    kony.timer.schedule("LocationEnablePopUpTimer", Enablelocationservices, 5, false);
                } catch (exception) {
                    kony.print("exception = " + exception);
                }
            } else if (gbllocpermission != 2 && !gblislocationon && (!MVCApp.Toolbox.Service.getMyLocationFlag() || MVCApp.Toolbox.Service.getMyLocationFlag() === "false")) {
                gblislocationon = true;
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18.phone.locationpermission"), "", constants.ALERT_TYPE_CONFIRMATION, GotolocSettings, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.takeMetoLocationSettings"), MVCApp.Toolbox.common.geti18nValueVA("i18.phone.notrightnow"));
            }
            showMapView();
        }
        frmFindStoreMapView.show();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function deniedLocationPermissionCallback(result) {
        if (result) {
            var KonyMain = java.import('com.konylabs.android.KonyMain');
            var activityContext = KonyMain.getActivityContext();
            var Intent = java.import('android.content.Intent');
            var Settings = java.import('android.provider.Settings');
            var locationIntent = new Intent();
            locationIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            var uri = java.import('android.net.Uri')
            var appname = uri.fromParts("package", activityContext.getPackageName(), null);
            locationIntent.setData(appname);
            activityContext.startActivity(locationIntent);
        }
    }

    function Enablelocationservices() {
        try {
            kony.timer.cancel("LocationEnablePopUpTimer");
            frmFindStoreMapView.flxLocationPrompt.isVisible = false;
        } catch (exception) {
            kony.print("exception = " + exception);
        }
    }

    function back() {
        var formName = MVCApp.getLocatorMapController().getFormName();
        var storeIDPDP = MVCApp.getPDPController().getStoreID();
        var clientKey = "";
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            var myLocationString = kony.store.getItem("myLocationDetails") || "";
            try {
                var locationDetails = JSON.parse(myLocationString);
                clientKey = locationDetails.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewStoreLocator.js on show for myLocationString:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Going Back From Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        if (formName === "frmPDP") {
            frmPDP.flxVoiceSearch.setVisibility(false);
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                MVCApp.getPDPController().showUI();
            } else {
                if (storeIDPDP !== clientKey) {
                    var reqParamsForPDP = MVCApp.getPDPController().getParamsForStoreChange();
                    var prodID = reqParamsForPDP.productID;
                    var defaultVariantId = reqParamsForPDP.defaultvariantProductId;
                    var variantFlag = MVCApp.getPDPController().getHeaderVariantFlag();
                    var headerText = MVCApp.getPDPController().getHeaderText();
                    var formNamePDP = MVCApp.getPDPController().getFormName();
                    MVCApp.getPDPController().load(prodID, variantFlag, false, headerText, formNamePDP, defaultVariantId, true, true);
                } else {
                    MVCApp.getPDPController().showUI();
                }
            }
        } else if (formName === "frmMore") {
            frmMore.flxVoiceSearch.setVisibility(false);
            MVCApp.getMoreController().load();
        } else if (formName === "frmHome") {
            MVCApp.getHomeController().load();
        } else if (formName === "frmPJDP") {
            frmPJDP.flxVoiceSearch.setVisibility(false);
            frmPJDP.show();
        } else if (formName == "frmWeeklyAdHome") {
            frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
            MVCApp.WeeklyAdHomeController().showUI();
        } else if (formName == "frmWeeklyAd") {
            frmWeeklyAd.flxVoiceSearch.setVisibility(false);
            MVCApp.WeeklyAdController().showUI();
        } else if (formName == "frmWeeklyAdDetail") {
            frmWeeklyAdDetail.flxVoiceSearch.setVisibility(false);
            MVCApp.WeeklyAdDetailController().showUI();
        } else if (formName == "frmProductCategory") {
            frmProductCategory.flxVoiceSearch.setVisibility(false);
            MVCApp.getProductCategoryController().displayPage();
        } else if (formName == "frmProductCategoryMore") {
            frmProductCategoryMore.flxVoiceSearch.setVisibility(false);
            frmProductCategoryMore.show();
        } else if (formName == "frmShoppingList") {
            frmShoppingList.flxVoiceSearch.setVisibility(false);
            MVCApp.ShoppingListController().showUI();
        } else if (formName == "frmProducts") {
            frmProducts.flxVoiceSearch.setVisibility(false);
            MVCApp.getProductsController().displayPage();
        } else if (formName == "frmProductsList") {
            frmProductsList.flxVoiceSearch.setVisibility(false);
            MVCApp.getProductsListController().backToScreen();
        } else if (formName == "frmProjects") {
            frmProjects.flxVoiceSearch.setVisibility(false);
            MVCApp.getProjectsController().displayProjects();
        } else if (formName == "frmProjectsList") {
            frmProjectsList.flxVoiceSearch.setVisibility(false);
            MVCApp.ProjectsListController().showUI();
        } else if (formName == "frmProjectsCategory") {
            frmProjectsCategory.flxVoiceSearch.setVisibility(false);
            MVCApp.getProjectsCategoryController().displayPage();
        } else if (formName == "frmStoreMap") {
            var storeIdOriginal = MVCApp.getLocatorMapController().getOriginalStoreId();
            var storeIdNew = MVCApp.Toolbox.common.getFavouriteStore() || "";
            if (storeIdNew !== "") {
                if (storeIdNew !== storeIdOriginal) {
                    var storeObj = {};
                    storeObj.info = {};
                    storeObj.info.aisle = "";
                    storeObj.info.dataOfStore = kony.store.getItem("myLocationDetails") || "";
                    storeObj.info.storeId = storeIdNew;
                    goToStoreMap(storeObj);
                } else {
                    frmStoreMap.show();
                }
            } else {
                MVCApp.getHomeController().load();
            }
        } else if (formName == "frmCartView") {
            MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
        } else if (formName == "frmWebView") {
            var urlBack = MVCApp.Toolbox.common.findWebViewUrlToGoBack();
            MVCApp.getProductsListWebController().loadWebView(urlBack);
        } else {
            MVCApp.getHomeController().load();
        }
    }

    function drawCircle() {
        var centerLoc = MVCApp.getLocatorMapController().getCentralLocation();
        var lat = centerLoc.lat || "";
        var lon = centerLoc.lon || "";
        if (lon == "" && lat == "") {
            var inputLatLon = MVCApp.getLocatorMapController().getLatLon();
            var latitudeInput = inputLatLon.lat || "";
            var longitudeInput = inputLatLon.lon || "";
            if (latitudeInput == "" && longitudeInput == "") {
                var myStoreString = kony.store.getItem("myLocationDetails") || "";
                if (myStoreString != "") {
                    try {
                        var myStoreObj = JSON.parse(myStoreString);
                        lat = myStoreObj.latitude;
                        lon = myStoreObj.longitude;
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "viewStoreLocator.js on drawCircle for myStoreString:" + JSON.stringify(e)
                        }]);
                        if (e) {
                            TealiumManager.trackEvent("POI Parse Exception While Drawing Circle In Store Map Screen", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                } else {
                    var polyLineArr = getRegionalMap();
                    drawPolyLine(polyLineArr);
                    return;
                }
            } else {
                lat = latitudeInput;
                lon = longitudeInput;
            }
        }
        frmFindStoreMapView.mapStore.removeCircle("circleId");
        frmFindStoreMapView.mapStore.removePolyline("polyid1");
        var circleData = {
            id: "circleId",
            centerLocation: {
                lat: lat,
                lon: lon
            },
            navigatetoZoom: true,
            radius: 50000,
            circleConfig: {
                lineColor: "0x00000000",
                lineWidth: 10,
                showCenterPin: false
            }
        };
        frmFindStoreMapView.mapStore.addCircle(circleData);
    }

    function GotolocSettings(response) {
        if (response) {
            redirectToSettings.appSpecificLocationSettings();
        }
    }

    function drawPolyLine(polyLineArr) {
        frmFindStoreMapView.mapStore.removeCircle("circleId");
        frmFindStoreMapView.mapStore.removePolyline("polyid1");
        frmFindStoreMapView.mapStore.addPolyline({
            id: "polyid1",
            startLocation: {},
            endLocation: {},
            locations: polyLineArr,
            polylineConfig: {
                lineColor: "0x00000000",
                lineWidth: "1"
            }
        });
    }

    function getRegionalMap() {
        var polyLineobjArr = [];
        polyLineobjArr = [{
            lat: "50.00",
            lon: "-130.00"
        }, {
            lat: "27.00",
            lon: "-65.00"
        }];
        return polyLineobjArr;
    }

    function showMapWithPolyline(mapDataArr, otherResultsExist, addCircleNotRequired) {
        frmFindStoreMapView.mapStore.locationData = [];
        if (!addCircleNotRequired) {
            drawCircle();
        }
        frmFindStoreMapView.mapStore.locationData = mapDataArr;
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function determineVisibilityOfClear() {
        var text = frmFindStoreMapView.textSearch.text;
        text = text.trim();
        if (text.length > 0) {
            frmFindStoreMapView.btnClearSearch.setVisibility(true);
        } else {
            frmFindStoreMapView.btnClearSearch.setVisibility(false);
        }
    }

    function clearText() {
        frmFindStoreMapView.textSearch.text = "";
        frmFindStoreMapView.segOptionalStoreResults.removeAll();
        frmFindStoreMapView.segOptionalStoreResults.data = [];
        frmFindStoreMapView.flxOptionalResultsContainer.isVisible = false;
    }

    function btnGetDirections(eventobj) {
        var srcAddr = eventobj.info.srcAdd;
        var destAddr = eventobj.info.destAdd;
        var url = "http://maps.apple.com/?daddr=" + destAddr + "&saddr=" + srcAddr;
        MVCApp.Toolbox.common.openApplicationURL(url);
    }

    function onClickOfFavourite(eventobj) {
        if (eventobj.skin == "sknBtnIcon36pxGray") {
            frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxRed";
            frmFindStoreMapView.lblTapToSelectMap.setVisibility(false);
            var locationDetails;
            try {
                locationDetails = JSON.parse(eventobj.info.data);
                MVCApp.sendMetricReport(frmFindStoreMapView, [{
                    "STORESELECTED": locationDetails.clientkey + ""
                }]);
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.storeid = locationDetails.clientkey;
                MVCApp.Customer360.sendInteractionEvent("Favourite Store", cust360Obj);
            } catch (e) {
                kony.print("Exception is  " + JSON.stringify(e));
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception In Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
            kony.store.setItem("myLocationDetails", eventobj.info.data);
            MVCApp.Toolbox.Service.setMyLocationFlag(true);
            updateMapData();
            updateSegmentData(false);
            //Invoking DW /pixel service which would set selected fav. store to storeId cookie.
            var cookies = kony.net.getCookies(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "pixel?appsource=mobileapp") || [];
            var httpClient = new kony.net.HttpRequest();
            httpClient.timeout = 5000;
            httpClient.open(constants.HTTP_METHOD_GET, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "pixel?appsource=mobileapp&stid=" + locationDetails.clientkey);
            var cookieString = "";
            for (index = 0; index < cookies.length; index++) {
                kony.print("Cookie is: " + cookies[index]);
                cookieString += cookies[index] + ";";
                //httpClient.setRequestHeader("cookie"  cookies[index]);     
            }
            httpClient.setRequestHeader("Cookie", cookieString);
            httpClient.send();
            //This is where display of Coupons/Events/WeeklyAds logic comes when store is not set
            //This is where display of Coupons/Events/WeeklyAds logic comes when store is not set
            var whenStoreNotSet = kony.store.getItem("whenStoreNotSet");
            try {
                whenStoreNotSet = JSON.parse(whenStoreNotSet);
                if (whenStoreNotSet.doesLoadCouponsWhenStoreNotSet) {
                    if (whenStoreNotSet.fromWhichScreen === "coupons") {
                        MVCApp.getHomeController().loadCoupons();
                    } else if (whenStoreNotSet.fromWhichScreen === "weeklyAds") {
                        loadWeeklyAds = true;
                        MVCApp.getWeeklyAdHomeController().load();
                    } else if (whenStoreNotSet.fromWhichScreen === "events") {
                        var previousForm = kony.application.getPreviousForm().id;
                        if (previousForm == "frmHome") {
                            MVCApp.getEventsController().load(false);
                        } else {
                            MVCApp.getEventsController().load(true);
                        }
                    } else if (whenStoreNotSet.fromWhichScreen === "rewards") {
                        frmObject = frmMore;
                        MVCApp.getMoreController().rewards();
                    } else if (whenStoreNotSet.fromWhichScreen === "home") {
                        if (whenStoreNotSet.contentURL) {
                            MVCApp.Toolbox.common.openApplicationURL(whenStoreNotSet.contentURL);
                        }
                    } else if (whenStoreNotSet.fromWhichScreen === "storeMapForAisle") {
                        MVCApp.getStoreMapController().showStoreMapWithAisleFor();
                    } else if (whenStoreNotSet.fromWhichScreen === "storeMapFullStore") {
                        var params = {};
                        params.aisleNo = "";
                        params.productName = "";
                        params.calloutFlag = false;
                        MVCApp.getStoreMapController().load(params);
                    }
                    whenStoreNotSet = {
                        "doesLoadCouponsWhenStoreNotSet": false,
                        "fromWhichScreen": ""
                    };
                    whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                    kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                }
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewStoreLocator.js on onClickOfFavourite for whenStoreNotSet:" + JSON.stringify(e)
                }]);
                updateSegmentData(true);
                frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxGray";
                kony.store.setItem("myLocationDetails", "");
                MVCApp.Toolbox.Service.setMyLocationFlag(false);
                updateMapData();
                if (e) {
                    TealiumManager.trackEvent("Exception While Favouriting The Store In Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            updateSegmentData(true);
            frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxGray";
            frmFindStoreMapView.lblTapToSelectMap.setVisibility(true);
            kony.store.setItem("myLocationDetails", "");
            MVCApp.Toolbox.Service.setMyLocationFlag(false);
            updateMapData(true);
        }
    }

    function updateMapData(addCircleNotRequired) {
        var dataMap = frmFindStoreMapView.mapStore.locationData;
        var clientKey = "";
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            var myLocationString = kony.store.getItem("myLocationDetails") || "";
            try {
                var locationDetails = JSON.parse(myLocationString);
                clientKey = locationDetails.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewStoreLocator.js on updateMapData for myLocationString:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Updating Map Data In Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        for (var i = 0; i < dataMap.length; i++) {
            var dataAti = dataMap[i].data || "";
            var temp = dataMap[i];
            if (dataAti != "") {
                if (dataAti.clientkey == clientKey) {
                    temp.favorite = true;
                    temp.image = "map_pin_red.png";
                    dataMap[i] = temp;
                } else {
                    temp.favorite = false;
                    temp.image = "map_pin_gray.png";
                    dataMap[i] = temp;
                }
            }
        }
        showMapWithPolyline(dataMap, gblOtherResults, addCircleNotRequired);
    }

    function updateSegmentData(isFavourite) {
        var data = frmFindStoreMapView.segStoresListView.data;
        var dataAt0 = {};
        var myLocationString = kony.store.getItem("myLocationDetails") || "";
        var locationDetails = JSON.parse(myLocationString);
        var dataLoc = null;
        var dataPrevLoc = null;
        var dataPrev = {};
        for (var i = 0; i < data.length; i++) {
            if (data[i].clientkey == locationDetails.clientkey) {
                dataLoc = i;
            } else if (data[i].btnFavorite.skin == "sknBtnIcon36pxRed") {
                dataPrevLoc = i;
            }
        }
        dataAt0 = data[dataLoc];
        if (isFavourite) {
            dataAt0.btnFavorite.skin = "sknBtnIcon36pxGray";
            dataAt0.lblTapToSelect.isVisible = true;
        } else {
            dataAt0.btnFavorite.skin = "sknBtnIcon36pxRed";
            dataAt0.lblTapToSelect.isVisible = false;
        }
        if (dataPrevLoc != null) {
            dataPrev = data[dataPrevLoc];
            dataPrev.btnFavorite.skin = "sknBtnIcon36pxGray";
            dataPrev.lblTapToSelect.isVisible = true;
            try {
                if (dataPrev.flexComingSoon) {
                    if (dataPrev.flexComingSoon.isVisible == true) {
                        dataPrev.lblTapToSelect.isVisible = false;
                    }
                }
            } catch (e) {
                kony.print("Error- flexComing soon is not found");
            }
            frmFindStoreMapView.segStoresListView.setDataAt(dataPrev, dataPrevLoc, 0);
        }
        frmFindStoreMapView.segStoresListView.setDataAt(dataAt0, dataLoc, 0);
        MVCApp.getLocatorMapController().setPreviousLocation(dataLoc);
    }

    function goToStoreMap(eventobj) {
        var info = eventobj.info || "";
        if (info !== "") {
            info = gblInfo;
        }
        var params = {};
        params.aisleNo = info.aisle || "";
        params.productName = MVCApp.getLocatorMapController().getProductName() || "";
        params.calloutFlag = false;
        var storeID = info.storeId || "";
        gblTealiumTagObj.storeid = storeID;
        MVCApp.getLocatorMapController().setOriginalStoreId(storeID);
        var dataOfStore = info.dataOfStore;
        MVCApp.sendMetricReport(frmFindStoreMapView, [{
            "storemap": "click"
        }]);
        MVCApp.getStoreMapController().setEntryPoint("ChooseStore");
        MVCApp.getStoreMapController().loadStoreMap(info.storeId, params, dataOfStore);
    }

    function setDataToDetailsFlex(data, onPinClick, isFavourite) {
        frmFindStoreMapView.lblAddLine1.text = data.address1;
        frmFindStoreMapView.lblStoreNameMap.text = data.address2;
        var province = data.province || "";
        if (data.country === "US") frmFindStoreMapView.lblAddLine2.text = data.city + ", " + data.state + " " + data.postalcode;
        else frmFindStoreMapView.lblAddLine2.text = data.city + ", " + data.province + " " + data.postalcode;
        frmFindStoreMapView.btnPhoneNew.text = data.phone;
        var stockOnHand = data.OnHand;
        var aisleNo = data.Location || "";
        var storeId = data.clientkey || "";
        if (stockOnHand !== null && stockOnHand !== undefined) {
            stockOnHand = parseInt(stockOnHand);
        } else {
            stockOnHand = "";
        }
        frmFindStoreMapView.lblAvailabilityMap.text = MVCApp.Toolbox.common.getStockStatusOfNearbyStores(stockOnHand);
        var src = MVCApp.getLocatorMapController().getSourceAddress();
        if (src !== "") {
            frmFindStoreMapView.flxDirectionsNew.setVisibility(true);
            frmFindStoreMapView.lblDistanceNew.text = data._distance + " mi";
            var srcAddr = src.lat + "," + src.lon;
            var destAdd = data.latitude + "," + data.longitude;
            frmFindStoreMapView.btnDirectionsNew.info = {
                "srcAdd": srcAddr,
                "destAdd": destAdd
            };
        } else {
            frmFindStoreMapView.flxDirectionsNew.setVisibility(false);
        }
        frmFindStoreMapView.btnFavoriteMyStore.info = {
            "data": JSON.stringify(data)
        };
        if (onPinClick == true) {
            if (data.isComingSoon) {
                frmFindStoreMapView.flexComingSoon.setVisibility(true);
                var transformObj = kony.ui.makeAffineTransform();
                transformObj.rotate(-45);
                frmFindStoreMapView.flexFlag.anchorPoint = {
                    x: 0,
                    y: 1
                };
                frmFindStoreMapView.flexFlag.transform = transformObj;
                frmFindStoreMapView.btnFavoriteMyStore.setVisibility(false);
                frmFindStoreMapView.lblTapToSelectMap.setVisibility(false);
            } else {
                frmFindStoreMapView.flexComingSoon.setVisibility(false);
                frmFindStoreMapView.btnFavoriteMyStore.setVisibility(true);
                frmFindStoreMapView.lblTapToSelectMap.setVisibility(true);
            }
            if (isFavourite) {
                frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxRed";
                frmFindStoreMapView.lblTapToSelectMap.setVisibility(false);
            } else {
                frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxGray";
                if (data.isComingSoon) {
                    frmFindStoreMapView.lblTapToSelectMap.setVisibility(false);
                } else {
                    frmFindStoreMapView.lblTapToSelectMap.setVisibility(true);
                }
            }
        } else {
            if (data.isComingSoon) {
                frmFindStoreMapView.flexComingSoon.setVisibility(true);
                var transformObj = kony.ui.makeAffineTransform();
                transformObj.rotate(-45);
                frmFindStoreMapView.flexFlag.anchorPoint = {
                    x: 0,
                    y: 1
                };
                frmFindStoreMapView.flexFlag.transform = transformObj;
                frmFindStoreMapView.btnFavoriteMyStore.setVisibility(false);
                frmFindStoreMapView.lblTapToSelectMap.setVisibility(false);
            } else {
                frmFindStoreMapView.btnFavoriteMyStore.setVisibility(true);
                frmFindStoreMapView.lblTapToSelectMap.setVisibility(true);
                frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxRed";
                frmFindStoreMapView.lblTapToSelectMap.setVisibility(false);
            }
        }
        var dataOfStore = data;
        gblInfo = {
            "aisle": aisleNo,
            "storeId": storeId,
            "dataOfStore": dataOfStore
        };
        frmFindStoreMapView.btnStoreMapMap.info = {
            "aisle": aisleNo,
            "storeId": storeId,
            "dataOfStore": dataOfStore
        };
        // If Store Hours Are "CLOSED" Then Use i18 Value 
        if (data.mon_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay1TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay1TimingNew.text = data.mon_hrs_special;
        }
        if (data.tue_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay2TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay2TimingNew.text = data.tue_hrs_special;
        }
        if (data.wed_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay3TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay3TimingNew.text = data.wed_hrs_special;
        }
        if (data.thu_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay4TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay4TimingNew.text = data.thu_hrs_special;
        }
        if (data.fri_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay5TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay5TimingNew.text = data.fri_hrs_special;
        }
        if (data.sat_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay6TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay6TimingNew.text = data.sat_hrs_special;
        }
        if (data.sun_hrs_special === "CLOSED") {
            frmFindStoreMapView.lblDay7TimingNew.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.closed");
        } else {
            frmFindStoreMapView.lblDay7TimingNew.text = data.sun_hrs_special;
        }
    }

    function setDataForFlex(mapId, data) {
        var dataId = data || "";
        if (dataId != "") {
            var index = data.index;
            var isFavourite = data.favorite;
            if (index != null && index != undefined) {
                if (index !== -1) {
                    updateMyStoreFlex(data.data, true, isFavourite);
                    var dataLoc = frmFindStoreMapView.mapStore.locationData;
                    //var index=data.index;
                    var currData = dataLoc[index];
                    currData.image = "map_pin_red.png";
                }
            }
        }
    }

    function showListView() {
        frmFindStoreMapView.flxMapHolder.left = "-100%";
        frmFindStoreMapView.flxListView.left = "0%";
        frmFindStoreMapView.btnListView.skin = "sknBtnTabsActive";
        frmFindStoreMapView.btnMapView.skin = "sknBtnTabsDefault";
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            var locDetailsStr = kony.store.getItem("myLocationDetails");
            try {
                var LocDetails = JSON.parse(locDetailsStr);
                storeID = LocDetails.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewStoreLocator.js on showListView for locDetailsStr:" + JSON.stringify(e)
                }]);
                storeID = "";
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Showing The List View In Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeID = "";
        }
        gblTealiumTagObj.storeid = storeID;
    }

    function showMapView() {
        frmFindStoreMapView.flxMapHolder.left = "0%";
        frmFindStoreMapView.flxListView.left = "100%";
        frmFindStoreMapView.btnMapView.skin = "sknBtnTabsActive";
        frmFindStoreMapView.btnListView.skin = "sknBtnTabsDefault";
    }

    function updateMyStoreFlex(data, onPinClick, isFavourite) {
        var pinClick = onPinClick || false;
        frmFindStoreMapView.flxMyStoreDetailsNew.setVisibility(true);
        var dataString = kony.store.getItem("myLocationDetails");
        if (pinClick) {
            dataString = JSON.stringify(data);
        }
        var dataObj = {};
        if (dataString != "" && dataString != null && dataString != undefined) {
            try {
                dataObj = JSON.parse(dataString);
                setDataToDetailsFlex(dataObj, pinClick, isFavourite);
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewStoreLocator.js on updateMyStoreFlex for dataString:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Updating The Store Flex In Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }

    function expandTheDetails() {
        frmFindStoreMapView.flxMyStoreDetailsExpandNew.setVisibility(true);
        frmFindStoreMapView.flxExpandDetails.setVisibility(false);
    }

    function collapseTheDetails() {
        frmFindStoreMapView.flxMyStoreDetailsExpandNew.setVisibility(false);
        frmFindStoreMapView.flxExpandDetails.setVisibility(true);
    }

    function callPhone(eventobj) {
        var text = eventobj.text;
        try {
            kony.phone.dial(text);
        } catch (e) {
            kony.print("unhandled error from iOS");
            if (e) {
                TealiumManager.trackEvent("Dial Exception In Store Locator Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function resetFavoritesAndShowMap() {
        var storeIDNew = "";
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            var dataString = kony.store.getItem("myLocationDetails");
            try {
                var dataLoc = JSON.parse(dataString);
                storeIDNew = dataLoc.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "viewStoreLocator.js on resetFavoritesAndShowMap for dataString:" + JSON.stringify(e)
                }]);
                storeIDNew = "";
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Resetting Favourites And Showing Map In Store Locator Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeIDNew = "";
        }
        if (storeIDNew != storeID) {
            updateMapData();
        }
        showMapView();
    }
    // Regex for City/Stat or Zip Validation, Based On Country, Searches for Locations By Text
    function determineSearch(eventobj) {
        var text = eventobj.text;
        var cityState = /([A-Za-z]+(?: [A-Za-z]+)*),([A-Za-z]{2})/;
        var citySpaceState = /([A-Za-z]+(?: [A-Za-z]+)*),? ([A-Za-z]{2})/;
        var usZip = /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/;
        var canZip = /^([A-Za-z][0-9][A-Za-z])\s*([0-9][A-Za-z][0-9])$/;
        var canZipAlternate = /^([A-Za-z][0-9][A-Za-z][0-9][A-Za-z][0-9])$/;
        var shopList = MVCApp.getLocatorMapController().getCheckNearby();
        if (kony.store.getItem("languageSelected") === "en") {
            if (cityState.test(text.trim()) || citySpaceState.test(text.trim()) || usZip.test(text.trim())) {
                if (shopList) {
                    MVCApp.getLocatorMapController().checkNearbyFromSearch(text);
                } else {
                    MVCApp.getLocatorMapController().initiateSearch(text);
                }
            } else {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.textSearch"), "");
            }
        } else {
            if (cityState.test(text.trim()) || citySpaceState.test(text.trim()) || canZip.test(text.trim()) || canZipAlternate.test(text.trim())) {
                if (shopList) {
                    MVCApp.getLocatorMapController().checkNearbyFromSearch(text);
                } else {
                    MVCApp.getLocatorMapController().initiateSearch(text);
                }
            } else {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.textSearch"), "");
            }
        }
    }

    function goToRegionChange() {
        MVCApp.getRegionChangeController().load();
    }

    function bindEvents() {
        frmFindStoreMapView.onDeviceBack = back;
        frmFindStoreMapView.btnClearSearchText.onClick = function() {
            frmFindStoreMapView.textSearch.text = "";
            frmFindStoreMapView.segOptionalStoreResults.removeAll();
            frmFindStoreMapView.segOptionalStoreResults.data = [];
            frmFindStoreMapView.flxOptionalResultsContainer.isVisible = false;
        };
        frmFindStoreMapView.flexCouponContainer.isVisible = true;
        frmFindStoreMapView.textSearch.onDone = determineSearch;
        frmFindStoreMapView.segStoresListView.widgetDataMap = widgetData;
        frmFindStoreMapView.btnMapView.onClick = resetFavoritesAndShowMap;
        frmFindStoreMapView.btnListView.onClick = showListView;
        frmFindStoreMapView.btnHome.onClick = back;
        frmFindStoreMapView.btnFavoriteMyStore.onClick = onClickOfFavourite;
        frmFindStoreMapView.btnDirectionsNew.onClick = btnGetDirections;
        frmFindStoreMapView.mapStore.onPinClick = setDataForFlex;
        frmFindStoreMapView.flxExpandDetails.onClick = expandTheDetails;
        frmFindStoreMapView.btnCollapseNew.onClick = collapseTheDetails;
        frmFindStoreMapView.btnPhoneNew.onClick = callPhone;
        frmFindStoreMapView.btnChangeRegion.onClick = goToRegionChange;
        MVCApp.tabbar.bindEvents(frmFindStoreMapView);
        MVCApp.tabbar.bindIcons(frmFindStoreMapView, "frmMore");
        frmFindStoreMapView.btnStoreMapMap.onClick = goToStoreMap;
        frmFindStoreMapView.postShow = function() {
            frmFindStoreMapView.btnExpandDetails.setEnabled(false);
            MVCApp.Toolbox.common.setBrightness("frmStoreLccator");
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Michaels Mobile App: Store Locator";
            lTealiumTagObj.page_name = "Michaels Mobile App: Store Locator";
            lTealiumTagObj.page_type = "store info";
            lTealiumTagObj.page_category_name = "Store Locator";
            lTealiumTagObj.page_category = "Store Locator";
            TealiumManager.trackView("Store Locator Screen", lTealiumTagObj);
            frmFindStoreMapView.flexComingSoon.setVisibility(false);
        };
        try {
            frmFindStoreMapView.flexCouponContainer.btnSignIn.onClick = function() {
                fromCoupons = true;
                isFromShoppingList = false;
                MVCApp.getSignInController().load();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking Sign In Button From Store Locator Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            frmFindStoreMapView.flexCouponContainer.btnSignUp.onClick = function() {
                fromCoupons = true;
                var userObject = kony.store.getItem("userObj");
                if (userObject === "" || userObject === null) {
                    isFromShoppingList = false;
                    MVCApp.getSignInController().load();
                } else {
                    MVCApp.getJoinRewardsController().load(userObject);
                }
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking Sign Up Button From Store Locator Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        try {
            frmFindStoreMapView.flexCouponContainer.btnDone.onClick = MVCApp.CouponsView().hideCoupons;
        } catch (e) {}
        try {
            frmFindStoreMapView.flxHeaderWrap.btnCoupon.onClick = function() {
                frmObject = frmFindStoreMapView;
                MVCApp.getHomeController().loadCoupons();
            };
        } catch (e) {
            if (e) {
                TealiumManager.trackEvent("Exception On Clicking Coupon Icon From Store Locator Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        frmFindStoreMapView.segStoresListView.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
        frmFindStoreMapView.segStoresListView.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
    }

    function clickOptionalResults() {
        var zipcode = "";
        var selItem = frmFindStoreMapView.segOptionalStoreResults.selectedRowItems[0];
        if (selItem) {
            var checkIfHeaderClicked = selItem.header;
            if (!checkIfHeaderClicked) {
                zipcode = selItem.zipcode;
                var searchObj = {
                    "text": zipcode
                };
                determineSearch(searchObj);
            }
        }
    }

    function updateScreen(results) {
        var dataShown = results.getDataForMap();
        var mapData = dataShown.calloutData;
        var segData = dataShown.listSegData;
        var otherResults = dataShown.resultsExist;
        gblOtherResults = otherResults;
        frmFindStoreMapView.segOptionalStoreResults.data = [];
        frmFindStoreMapView.flxOptionalResultsContainer.isVisible = false;
        if (otherResults) {
            showMapWithPolyline(mapData, otherResults);
            if (segData.length > 0) {
                frmFindStoreMapView.lbltextNoResultsFound.isVisible = false;
                frmFindStoreMapView.flxLine.isVisible = false;
                frmFindStoreMapView.segStoresListView.isVisible = true;
                frmFindStoreMapView.segStoresListView.data = segData;
            } else {
                frmFindStoreMapView.lbltextNoResultsFound.isVisible = true;
                frmFindStoreMapView.flxLine.isVisible = true;
                frmFindStoreMapView.segStoresListView.isVisible = false;
            }
        } else {
            var suggestedAddress = results.getSuggestedData();
            if (suggestedAddress.length !== 0) {
                var addressResults = [];
                var headerOptResObj = {
                    "lblOptResHeader": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.DidYouMean"),
                    "header": true
                };
                frmFindStoreMapView.segOptionalStoreResults.removeAll();
                frmFindStoreMapView.segOptionalStoreResults.data = [];
                frmFindStoreMapView.flxOptionalResultsContainer.setVisibility(true);
                headerOptResObj.template = flxOptResHeader;
                addressResults.push(headerOptResObj);
                for (var i = 0; i < suggestedAddress.length; i++) {
                    var newObj = {
                        "lblOptionalResults": suggestedAddress[i].address1 + " " + suggestedAddress[i].city + ", " + suggestedAddress[i].state + " " + suggestedAddress[i].postalcode,
                        "latitude": suggestedAddress[i].latitude,
                        "longitude": suggestedAddress[i].longitude,
                        "zipcode": suggestedAddress[i].postalcode,
                        "header": false
                    };
                    addressResults.push(newObj);
                }
                frmFindStoreMapView.segOptionalStoreResults.widgetDataMap = {
                    "lblOptResHeader": "lblOptResHeader",
                    "lblOptionalResults": "lblOptionalResults"
                };
                frmFindStoreMapView.segOptionalStoreResults.data = addressResults;
            } else {
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"), "", constants.ALERT_TYPE_INFO, null, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"), "");
            }
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        if (MVCApp.getLocatorMapController().getCheckNearby()) {
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            var resultsForCheckNearby = results.getResultsForCheckNearby();
            if (resultsForCheckNearby.length > 0) {
                for (var m = 0; m < resultsForCheckNearby.length; m++) {
                    cust360Obj["storeid" + m] = resultsForCheckNearby[m];
                }
            } else {
                cust360Obj["storeList"] = "";
            }
            MVCApp.Customer360.sendInteractionEvent("SearchFromCheckNearbyResults", cust360Obj)
        }
    }

    function removeFavorites() {
        frmFindStoreMapView.btnFavoriteMyStore.skin = "sknBtnIcon36pxGray";
        //map pins have to be changed.
    }

    function showWithDefaultLoc(mapData, segmentData, newEntry) {
        frmFindStoreMapView.mapStore.locationData = [];
        frmFindStoreMapView.segStoresListView.removeAll();
        var mapDataArr = [];
        var segDataArr = [];
        var src = MVCApp.getLocatorMapController().getSourceAddress();
        if (mapData !== "" && segmentData !== "") {
            mapDataArr.push(mapData);
            segDataArr.push(segmentData);
            MVCApp.getLocatorMapController().setPreviousLocation(0);
        } else {
            frmFindStoreMapView.flxMyStoreDetailsNew.setVisibility(false);
        }
        gblOtherResults = false;
        showMapWithPolyline(mapDataArr, false);
        if (segDataArr !== null && segDataArr.length > 0) {
            frmFindStoreMapView.lbltextNoResultsFound.isVisible = false;
            frmFindStoreMapView.flxLine.isVisible = false;
            frmFindStoreMapView.segStoresListView.isVisible = true;
            frmFindStoreMapView.segStoresListView.data = segDataArr;
        } else {
            frmFindStoreMapView.lbltextNoResultsFound.isVisible = true;
            frmFindStoreMapView.flxLine.isVisible = true;
            frmFindStoreMapView.segStoresListView.isVisible = false;
        }
        clearText();
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            updateMyStoreFlex();
        } else {
            frmFindStoreMapView.flxMyStoreDetailsNew.setVisibility(false);
        }
        if (newEntry) show();
    }

    function resetDataToMapAndSegment() {
        MVCApp.getLocatorMapController().getDataForFavourites();
    }

    function updateScreenForMap(results) {
        frmFindStoreMapView.mapStore.locationData = [];
        frmFindStoreMapView.segStoresListView.removeAll();
        var dataShown = results.getDataForMap();
        var mapData = dataShown.calloutData;
        var segData = dataShown.listSegData;
        var otherResults = dataShown.resultsExist;
        gblOtherResults = otherResults;
        if (MVCApp.getLocatorMapController().getCheckNearby()) {
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            var resultsForCheckNearby = results.getResultsForCheckNearby();
            if (resultsForCheckNearby.length > 0) {
                for (var m = 0; m < resultsForCheckNearby.length; m++) {
                    cust360Obj["storeid" + m] = resultsForCheckNearby[m];
                }
            } else {
                cust360Obj["storeList"] = "";
            }
            MVCApp.Customer360.sendInteractionEvent("CheckNearByStoresResults", cust360Obj)
        }
        showMapWithPolyline(mapData, otherResults);
        if (segData.length > 0) {
            frmFindStoreMapView.lbltextNoResultsFound.isVisible = false;
            frmFindStoreMapView.flxLine.isVisible = false;
            frmFindStoreMapView.segStoresListView.isVisible = true;
            frmFindStoreMapView.segStoresListView.data = segData;
        } else {
            frmFindStoreMapView.lbltextNoResultsFound.isVisible = true;
            frmFindStoreMapView.flxLine.isVisible = true;
            frmFindStoreMapView.segStoresListView.isVisible = false;
        }
        show();
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            updateMyStoreFlex();
        } else {
            frmFindStoreMapView.flxMyStoreDetailsNew.setVisibility(false);
        }
    }

    function showExpandedVersion() {
        frmFindStoreMapView.flxMyStoreDetailsExpandNew.setVisibility(true);
        frmFindStoreMapView.flxExpandDetails.setVisibility(false);
    }

    function showCollapsedVersion() {
        frmFindStoreMapView.flxMyStoreDetailsExpandNew.setVisibility(false);
        frmFindStoreMapView.flxExpandDetails.setVisibility(true);
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
            frmFindStoreMapView.segStoresListView.bottom = "0dp";
        } else {
            MVCApp.Toolbox.common.dispFooter();
            frmFindStoreMapView.segStoresListView.bottom = "54dp";
        }
    }

    function showUI() {
        frmFindStoreMapView.show();
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        updateScreenForMap: updateScreenForMap,
        showWithDefaultLoc: showWithDefaultLoc,
        updateMyStoreFlex: updateMyStoreFlex,
        resetDataToMapAndSegment: resetDataToMapAndSegment,
        removeFavorites: removeFavorites,
        showExpandedVersion: showExpandedVersion,
        showCollapsedVersion: showCollapsedVersion,
        clickOptionalResults: clickOptionalResults,
        showUI: showUI
    };
});