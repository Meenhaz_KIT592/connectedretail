/**
 * PUBLIC
 * This is the model for frmSettings form
 */
var MVCApp = MVCApp || {};
var changeInFrenchLanguage = false;
MVCApp.WeeklyAdHomeModel = (function() {
    var serviceType = MVCApp.serviceType.weeklyAds;
    var operationId = MVCApp.serviceEndPoints.getAllDepartments;

    function getAdDepartments(callback, requestParams) {
        kony.print("WeeklyAdHomeModel.js");
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.opstatus === 0 || results.opstatus === "0") {
                if (results.totalCount && results.totalCount == "0") {
                    if (requestParams.languageid == "2") {
                        requestParams.languageid = "1";
                        changeInFrenchLanguage = true;
                        getAdDepartments(callback, requestParams);
                        return;
                    }
                }
                var weeklyAdsDataObj = new MVCApp.data.WeeklyAds(results);
                gblTagValues = results.resultsTag || [];
                callback(weeklyAdsDataObj);
            }
        });
    }
    return {
        getAdDepartments: getAdDepartments
    };
});