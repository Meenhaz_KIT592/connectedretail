/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.CustomerCareView = (function() {
    /**
     * PUBLIC
     * Open the Events form
     */
    function show() {
        frmCustomerCare.show();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmCustomerCare.btnCancel.onClick = goBackToHelp;
        frmCustomerCare.btnSubmit.onClick = onSubmit;
        frmCustomerCare.btnFindStore.onClick = onFindStore;
        frmCustomerCare.postShow = function() {
            MVCApp.Toolbox.common.destroyStoreLoc();
        };
    }

    function updateScreen(formData) {
        var firstName = formData.firstName || "";
        var lastName = formData.lastName || "";
        var email = formData.email || "";
        var phone = formData.phone || "";
        var store = formData.store || "";
        //assigning text to text fields
        frmCustomerCare.txtFirstName.text = firstName;
        frmCustomerCare.txtLastName.text = lastName;
        frmCustomerCare.txtEmail.text = email;
        frmCustomerCare.txtPhone.text = phone;
        frmCustomerCare.txtStore.text = store;
        //greying out the text 
        frmCustomerCare.txtFirstName.setEnabled(firstName === "");
        frmCustomerCare.txtLastName.setEnabled(lastName === "");
        frmCustomerCare.txtEmail.setEnabled(email === "");
        frmCustomerCare.txtPhone.setEnabled(phone === "");
        frmCustomerCare.txtStore.setEnabled(false);
        show();
    }

    function goBackToHelp() {
        frmHelp.show();
        MVCApp.tabbar.bindEvents(frmHelp);
        MVCApp.tabbar.bindIcons(frmHelp, "frmMore");
    }

    function onSubmit() {
        var formFields = {};
        formFields.firstName = frmCustomerCare.txtFirstName.text || "";
        formFields.lastName = frmCustomerCare.txtLastName.text || "";
        formFields.email = frmCustomerCare.txtEmail.text || "";
        formFields.phone = frmCustomerCare.txtPhone.text || "";
        formFields.store = frmCustomerCare.txtStore.text || "";
        MVCApp.getCustomerCareController().onSubmit(formFields);
    }

    function onFindStore() {
        //need to implement
        //should showup locator map and should come back to same form on back 
        alert("need to implement");
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});