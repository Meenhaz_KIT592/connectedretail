/**
 * PUBLIC
 * This is the view for the CreateAccount form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var createAccountResObj = {};
var gblCreateAccountRegioinChangeFlag = false;
var MVCApp = MVCApp || {};
MVCApp.CreateAccountView = (function() {
    var isAccCreated = false;
    var createAccountObj = {};
    var prevFormName = "";
    /**
     * PUBLIC
     * Open the More form
     */
    function show() {
        frmCreateAccount.flxMainContainer.contentOffset = {
            x: 0,
            y: 0
        };
        clearFields();
        var screenWidth = kony.os.deviceInfo().screenWidth;
        if (screenWidth == 414 || screenWidth == 375) {
            frmCreateAccount.rtTermsandConditions.padding = [0, 0.5, 0, 0];
            frmCreateAccount.rtAge.padding = [0, 0.5, 0, 0];
        }
        if (MVCApp.Toolbox.common.getRegion() == "CA") {
            frmCreateAccount.flxZipCode.setVisibility(false);
            frmCreateAccount.flxPhoneNo.setVisibility(true);
            frmCreateAccount.flxZipCodeCan.setVisibility(true);
            frmCreateAccount.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.lblCreateAcc");
        } else {
            frmCreateAccount.flxPhoneNo.setVisibility(true);
            frmCreateAccount.flxZipCode.setVisibility(true);
            frmCreateAccount.flxZipCodeCan.setVisibility(false);
            frmCreateAccount.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.lblCreateAcc");
        }
        //	frmCreateAccount.flxFooterWrap.setVisibility(false);
        frmCreateAccount.show();
        MVCApp.tabbar.bindEvents(frmCreateAccount);
        MVCApp.tabbar.bindIcons(frmCreateAccount, "frmMore");
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function clearFields() {
        frmCreateAccount.txtFirstName.text = "";
        frmCreateAccount.txtLastName.text = "";
        frmCreateAccount.txtPassword.text = "";
        frmCreateAccount.txtConfirmPassword.text = "";
        frmCreateAccount.txtZipCode.text = "";
        frmCreateAccount.imgCheckAgree.src = "checkbox_off.png";
        frmCreateAccount.imgCheckAge.src = "checkbox_off.png";
        frmCreateAccount.txtZipCodeCan.text = "";
        frmCreateAccount.txtEmail.text = "";
        frmCreateAccount.txtConfrimEmail.text = "";
        frmCreateAccount.imgCheck.src = "checkbox_off.png";
        frmCreateAccount.flxOverlay.setVisibility(false);
        frmCreateAccount.txtPhoneNo.text = "";
    }

    function savePreviousScreenForm() {
        MVCApp.Toolbox.common.destroyStoreLoc();
        var prevForm = kony.application.getPreviousForm().id;
        if (prevForm == "frmForgotPwd") {
            prevFormName = prevForm;
        } else if (prevForm == "frmSignIn") {
            prevFormName = prevForm;
        } else if (prevForm == "frmMore") {
            prevFormName = prevForm;
        } else if (prevForm == "frmGuide") {
            prevFormName = prevForm;
        } else if (prevForm == "frmWebView") {
            prevFormName = prevForm;
        } else if (prevForm == "frmCartView") {
            prevFormName = prevForm;
        }
        var lTealiumTagObj = gblTealiumTagObj;
        lTealiumTagObj.page_id = "Sign Up: Michaels Mobile App";
        lTealiumTagObj.page_name = "Sign Up: Michaels Mobile App";
        lTealiumTagObj.page_type = "Sign Up";
        lTealiumTagObj.page_category_name = "Sign Up";
        lTealiumTagObj.page_category = "Sign Up";
        TealiumManager.trackView("Sign Up screen", lTealiumTagObj);
    }
    /**
     * @function
     *
     */
    function showPreviousScreen() {
        var userObject = kony.store.getItem("userObj") || "";
        if (userObject !== "") {
            isAccCreated = true;
        }
        if (fromCoupons) {
            if (isAccCreated) {
                MVCApp.getHomeController().loadCoupons();
                isAccCreated = false;
            } else {
                let preForm = kony.application.getPreviousForm().id;
                if (preForm === "frmCoupons") {
                    frmCoupons.show();
                } else {
                    frmSignIn.show();
                }
            }
        } else if (rewardsInfoFlag) {
            MVCApp.getRewardsProgramInfoController().load(false);
        } else {
            if (prevFormName == "frmForgotPwd") {
                frmForgotPwd.show();
            } else if (prevFormName == "frmSignIn") {
                if (isAccCreated) {
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblIsFromHomeScreen) {
                        gblNewBrightnessValueStoreMode = gblBrightNessValue;
                        gblIsNewBrightnessValuePresent = true;
                        gblIsFromHomeScreen = false;
                        MVCApp.getHomeController().onClickRewards();
                        frmObject = frmHome;
                    } else {
                        MVCApp.getMoreController().load();
                    }
                    isAccCreated = false;
                } else {
                    frmSignIn.show();
                    MVCApp.tabbar.bindEvents(frmSignIn);
                    MVCApp.tabbar.bindIcons(frmSignIn, "frmMore");
                }
            } else if (prevFormName == "frmMore") {
                if (isAccCreated) {
                    MVCApp.getMoreController().load();
                    isAccCreated = false;
                }
                MVCApp.getMoreController().loadMore();
            } else if (prevFormName == "frmCoupons") {
                frmCoupons.show();
            } else if (prevFormName == "frmGuide") {
                MVCApp.getHomeController().showDefaultView();
                MVCApp.Toolbox.common.showLoadingIndicator("");
                getProductsCategory();
            } else if (prevFormName == "frmWebView") {
                MVCApp.getProductsListWebController().loadWebView(pdpUrl);
                pdpUrl = "";
            } else if (prevFormName == "frmCartView") {
                MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
            } else {
                MVCApp.getHomeController().load();
            }
        }
    }

    function bindEvents() {
        frmCreateAccount.postShow = savePreviousScreenForm;
        frmCreateAccount.btnHeaderLeft.onClick = showPreviousScreen;
        frmCreateAccount.btnNext.onClick = onClickNext;
        frmCreateAccount.btnAgree.onClick = onClickAgree;
        frmCreateAccount.btnAge.onClick = onClickAgeCertify;
        frmCreateAccount.rtTermsandConditions.onClick = showTermsandConditions;
        frmCreateAccount.flxOverlay.onTouchEnd = function() {};
        frmCreateAccount.btnPrivacyPolicy.onClick = checkPrivacyPolicy;
        frmCreateAccount.rtPrivacyPolicy.onClick = showPrivacyPolicy;
        frmCreateAccount.btnYes.onClick = function() {
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblIsFromHomeScreen) {
                gblIsFromHomeScreen = false;
            }
            createAccountResObj = kony.store.getItem("userObj");
            fromTouchId = false;
            if (MVCApp.Toolbox.common.getRegion() === "US") {
                if (gblUrl !== "") {
                    if (gblUrl !== "" && gblUrl.indexOf("favorites") !== -1) {
                        var urlArr1 = gblUrl.split(/&/g);
                        var p_id = "";
                        for (var i = 0; i < urlArr1.length; i++) {
                            if (urlArr1[i].indexOf("pid") !== -1) {
                                p_id = urlArr1[i].split("=")[1];
                            }
                        }
                        if (p_id) {
                            var storeId = MVCApp.Toolbox.common.getStoreID();
                            if (kony.application.getPreviousForm().id === "frmCartView") {
                                MVCApp.getCartController().loadCart(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Cart-MobileAppAddToWishlist?pid=" + p_id + "", true);
                            } else MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Add?pid=" + p_id + "");
                        }
                    } else {
                        //MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "COSinglePageCheckout/Start", true);
                        MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/COSinglePageCheckout-Start", true);
                    }
                    gblUrl = "";
                } else {
                    var url = "Account";
                    MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + url);
                }
            } else {
                MVCApp.getRewardsProfileController().load(createAccountResObj);
            }
        };
        frmCreateAccount.btnRewardProgramInfo.onClick = function() {
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblIsFromHomeScreen) {
                gblIsFromHomeScreen = false;
            }
            MVCApp.getRewardsProgramInfoController().load(false);
        };
        frmCreateAccount.btnNotRightNow.onClick = function() {
            isAccCreated = true;
            showPreviousScreen();
        };
        frmCreateAccount.btnOk.onClick = onClickOfOK;
    }

    function updateScreen(results) {
        show();
    }

    function showTermsandConditions() {
        MVCApp.getBrowserController().load(MVCApp.serviceConstants.createTermsandConditions);
    }

    function onClickNext() {
        createAccountObj.first_name = frmCreateAccount.txtFirstName.text;
        createAccountObj.last_name = frmCreateAccount.txtLastName.text;
        createAccountObj.password = frmCreateAccount.txtPassword.text;
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            createAccountObj.c_postalCode = frmCreateAccount.txtZipCode.text;
        } else {
            createAccountObj.c_postalCode = frmCreateAccount.txtZipCodeCan.text;
        }
        createAccountObj.email = frmCreateAccount.txtEmail.text;
        createAccountObj.user = frmCreateAccount.txtEmail.text;
        // if(MVCApp.Toolbox.common.getRegion()=="CA"){
        createAccountObj.phoneNo = frmCreateAccount.txtPhoneNo.text;
        /* }
        else
          {
            createAccountObj.phoneNo = frmCreateAccount.txtPhoneNo.text;
          }*/
        createAccountObj.prevFormName = prevFormName;
        if (validateFields()) {
            MVCApp.getCreateAccountController().submitUserLoad(createAccountObj);
            TealiumManager.trackEvent("Create Account Button Click", {
                conversion_category: "Create Account",
                conversion_id: "Account Signup Confirmation",
                conversion_action: 2
            });
        }
    }

    function validateFields() {
        var firstName = frmCreateAccount.txtFirstName.text;
        var lastName = frmCreateAccount.txtLastName.text;
        var password = frmCreateAccount.txtPassword.text;
        var confirmPassword = frmCreateAccount.txtConfirmPassword.text;
        var zipcode = "";
        var zipCodeRegEx = "";
        var errFlag = false;
        var errMsg = [];
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            zipcode = frmCreateAccount.txtZipCode.text;
            zipCodeRegEx = /^[0-9-]+$/;
        } else if (MVCApp.Toolbox.common.getRegion() == "CA") {
            zipcode = frmCreateAccount.txtZipCodeCan.text;
            zipCodeRegEx = /^[0-9a-zA-Z]+$/;
        }
        var passwordRegEx = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).*$/;
        var numberRegEx = /^[0-9-]+$/;
        var nameRegEx = /^[-_a-zA-Z ]*$/;
        //var nameRegEx = /^[a-zA-Z-_]+$/;
        var ageAndTermsFlag = true;
        var email = frmCreateAccount.txtEmail.text;
        var confirmEmail = frmCreateAccount.txtConfrimEmail.text;
        var regularEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var phoneNo = frmCreateAccount.txtPhoneNo.text;
        if (firstName === null || firstName.trim() === "") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstName"));
            errFlag = true;
            ageAndTermsFlag = false;
        } else {
            if (!nameRegEx.test(firstName)) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstandLastNameAlert"), "");
                return false;
            }
        }
        if (lastName === null || lastName.trim() === "") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.lastName"));
            errFlag = true;
            ageAndTermsFlag = false;
        } else {
            if (!nameRegEx.test(lastName)) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.firstandLastNameAlert"), "");
                return false;
            }
        }
        if (email === null || email.trim() === "") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAddress"));
            errFlag = true;
            ageAndTermsFlag = false;
        } else {
            if (!regularEx.test(email.trim())) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
                return false;
            }
        }
        if (confirmEmail === null || confirmEmail.trim() === "") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.confirmEmailAddress"));
            errFlag = true;
            ageAndTermsFlag = false;
        } else {
            if (email.trim() != confirmEmail.trim()) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.emailMismatch"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
                return false;
            }
        }
        if (password === null || password.trim() === "") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.password"));
            errFlag = true;
            ageAndTermsFlag = false;
        } else {
            if (password.trim().length >= 8) {
                if (!passwordRegEx.test(password.trim())) {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.passwordAlertContains"), "");
                    return false;
                }
            } else {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.passwordAlertContains"), "");
                return false;
            }
            if (confirmPassword === null || confirmPassword.trim() === "") {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.passwordAndConfirmPasswordAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
                ageAndTermsFlag = false;
                return false;
            }
            if (confirmPassword.trim() != password.trim()) {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.passwordAndConfirmPasswordAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.oops"));
                return false;
            }
        }
        if (zipcode === null || zipcode.trim() === "") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.zipCode"));
            errFlag = true;
            ageAndTermsFlag = false;
        } else {
            if (MVCApp.Toolbox.common.getRegion() == "US") {
                if (zipcode.trim().length == 5) {
                    if (!zipCodeRegEx.test(zipcode.trim())) {
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.zipcodeAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.tryAgain"));
                        return false;
                    }
                } else {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.zipcodeAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.tryAgain"));
                    return false;
                }
            } else if (MVCApp.Toolbox.common.getRegion() == "CA") {
                if (zipcode.trim().length == 6) {
                    if (!zipCodeRegEx.test(zipcode.trim())) {
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.zipcodeAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.tryAgain"));
                        return false;
                    }
                } else if (/^\d{5}$/.test(zipcode.trim())) {
                    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.zipcode.regionMismatch.errorMessage"), null, constants.ALERT_TYPE_CONFIRMATION, function(eventObj) {
                        if (eventObj) {
                            //  var settingsFormDeeplinkURL = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSettings.deeplink");
                            // if (settingsFormDeeplinkURL) {
                            gblCreateAccountRegioinChangeFlag = true;
                            // kony.application.openURL(settingsFormDeeplinkURL);
                            fromMoreFlagRewards = false;
                            MVCApp.getSettingsController().load();
                            MVCApp.tabbar.bindEvents(frmSettings);
                            MVCApp.tabbar.bindIcons(frmSettings, "frmMore");
                            // }
                        }
                    }, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.takeMetoLocationSettings"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.nothanks"));
                    return false;
                } else {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.zipcodeAlert"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.tryAgain"));
                    return false;
                }
            }
        }
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            if (phoneNo === null || phoneNo.trim() === "") {
                errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.phoneNumberAlert"));
                errFlag = true;
                ageAndTermsFlag = false;
            } else {
                if (phoneNo.length == 10) {
                    if (!numberRegEx.test(phoneNo.trim())) {
                        errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContainsNumbers"));
                        errFlag = true;
                        ageAndTermsFlag = false;
                    }
                } else {
                    alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsJoin.phoneNumberContains"));
                    return false;
                }
            }
        }
        if (frmCreateAccount.imgCheckAgree.src != "checkbox_on.png") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.termsAndConditions"));
            errFlag = true;
        }
        if (frmCreateAccount.imgCheckAge.src != "checkbox_on.png") {
            errMsg.push(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.ageLimit"));
            errFlag = true;
        }
        if (errFlag === true) {
            var result = "";
            for (var i = 0; i < errMsg.length; i++) {
                if (i == 0) {
                    result = result + errMsg[i];
                } else {
                    if (i > 0 && (i == (errMsg.length - 1))) {
                        result = result + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.and") + " " + errMsg[i];
                    } else {
                        result = result + ", " + errMsg[i];
                    }
                }
            }
            if (ageAndTermsFlag) {
                if (errMsg.length == 2) {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.ageTermsAndConditions"), "");
                } else {
                    if (errMsg[0] == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.termsAndConditions")) {
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.termsandConditionsCheck"), "");
                    } else {
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.ageCheck"), "");
                    }
                }
            } else {
                {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmSignIn.weNeed") + " " + result + " " + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.toContinue"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.uhOh"));
                }
            }
            return false;
        } else {
            return true;
        }
    }

    function onClickAgree() {
        if (frmCreateAccount.imgCheckAgree.src == "checkbox_off.png") {
            frmCreateAccount.imgCheckAgree.src = "checkbox_on.png";
        } else {
            frmCreateAccount.imgCheckAgree.src = "checkbox_off.png";
        }
    }

    function onClickAgeCertify() {
        if (frmCreateAccount.imgCheckAge.src == "checkbox_off.png") {
            frmCreateAccount.imgCheckAge.src = "checkbox_on.png";
        } else {
            frmCreateAccount.imgCheckAge.src = "checkbox_off.png";
        }
    }

    function showPrivacyPolicy() {
        MVCApp.getBrowserController().load(MVCApp.serviceConstants.privacyPolicy);
    }

    function checkPrivacyPolicy() {
        if (frmCreateAccount.imgCheck.src == "checkbox_off.png") {
            frmCreateAccount.imgCheck.src = "checkbox_on.png";
            TealiumManager.trackEvent("New Email Sign Up", {
                new_email_signup: true
            });
        } else {
            frmCreateAccount.imgCheck.src = "checkbox_off.png";
        }
    }

    function showResponse(response, status) {
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        if (status == "success") {
            cust360Obj.accountCreationStatus = "success";
            sfmcCustomObject.Indiv_Id = response.customer_no ? response.customer_no : "";
            sfmcCustomObject.FirstName = response.first_name ? response.first_name : "";
            sfmcCustomObject.LastName = response.last_name ? response.last_name : "";
            if (OneManager) {
                OneManager.sfmcUpdateCustomAttributes(sfmcCustomObject);
            }
            var encryptresponse = response;
            kony.store.setItem("userObj", encryptresponse);
            setUserDetailsForTealiumTagging(response);
            var loyaltyMemberId = response.c_loyaltyMemberID || "";
            if (loyaltyMemberId != "") {
                kony.setUserID(loyaltyMemberId);
                kony.store.setItem("loyaltyMemberId", loyaltyMemberId);
            }
            var c_loyaltyEmail = response.c_loyaltyEmail || "";
            var c_loyaltyPhoneNo = response.c_loyaltyPhoneNo || "";
            gblPassword = createAccountObj.password;
            var encryptedEmail = MVCApp.Toolbox.common.MVCApp_encryptPin(encryptresponse.email);
            encryptedEmail = kony.convertToBase64(encryptedEmail);
            kony.store.setItem("username", encryptedEmail);
            var encryptedPass = MVCApp.Toolbox.common.MVCApp_encryptPin(createAccountObj.password);
            encryptedPass = kony.convertToBase64(encryptedPass);
            kony.store.setItem("userCredential", encryptedPass);
            if (kony.store.getItem("pushSwitch")) {
                var country = MVCApp.Toolbox.common.getRegion();
                var locale = kony.store.getItem("languageSelected");
                var deviceID = kony.os.deviceInfo().identifierForVendor;
                MVCApp.PushNotification.common.modifyUser(deviceID + "@michaels.com", deviceID, locale, country);
            }
            gblDecryptedPassword = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("userCredential"));
            gblDecryptedUser = MVCApp.Toolbox.common.MVCApp_decryptPin(kony.store.getItem("username"));
            if (MVCApp.Toolbox.common.getRegion() == "US" || MVCApp.Toolbox.common.getRegion() == "CA") {
                var reportData = {};
                if (loyaltyMemberId === "" && c_loyaltyEmail === "" && c_loyaltyPhoneNo === "") {
                    disabilityButtons(response);
                    reportData = {
                        "RewardsAccountCreated": false,
                        "region": "US",
                        "DWAccountCreated": true
                    };
                    //KNYMetricsService.sendEvent("Custom", null,"frmCreateAccount", "", null, {"DWandRewardsAccountCreationSuccess":true,"region":"US"});
                    TealiumManager.trackEvent("User Registered", {
                        event_type: "registration",
                        user_registered: true,
                        customer_email: encryptresponse.email,
                        customer_id: encryptresponse.customer_id
                    });
                } else {
                    reportData = {
                        "RewardsAccountCreated": true,
                        "region": "US",
                        "DWAccountCreated": true
                    };
                    //KNYMetricsService.sendEvent("Custom", null,"frmCreateAccount", "", null, {"RewardsAccountCreationFailure":true,"region":"US"});
                    visibilityButtons();
                    TealiumManager.trackEvent("User Registered", {
                        event_type: "registration",
                        user_registered: true,
                        customer_email: encryptresponse.email,
                        loyalty_id: encryptresponse.c_loyaltyMemberID,
                        customer_id: encryptresponse.customer_id
                    });
                }
                MVCApp.sendMetricReport(frmCreateAccount, [{
                    "SignUPCombined": JSON.stringify(reportData)
                }]);
                frmCreateAccount.flxOverlay.setVisibility(true);
            } else {
                if (isFromShoppingList) {
                    isFromShoppingList = false;
                    if (frmObject.id === "frmPDP" || frmObject.id === "frmPJDP") {
                        MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.shoppingList.rewardsAccount"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"), constants.ALERT_TYPE_INFO, addProdOrProjToShoppingList, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"), "");
                    } else if (isMyListflag === true) {
                        MVCApp.getShoppingListController().getProductListItems();
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.shoppingList.rewardsAccount"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"));
                        isMyListflag = false;
                    } else {
                        MVCApp.getMoreController().load();
                        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.shoppingList.rewardsAccount"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"));
                    }
                } else {
                    MVCApp.getMoreController().load();
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.shoppingList.rewardsAccount"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.welcomeMaker"));
                }
                var reportData = {};
                reportData = {
                    "DWAccountCreationSuccess": true,
                    "region": "CA"
                };
                MVCApp.sendMetricReport(frmCreateAccount, [{
                    "SignUP": JSON.stringify(reportData)
                }]);
                TealiumManager.trackEvent("User Registered", {
                    event_type: "registration",
                    user_registered: true,
                    customer_email: encryptresponse.email,
                    customer_id: encryptresponse.customer_id
                });
                //KNYMetricsService.sendEvent("Custom", null,"frmCreateAccount", "", null, {"DWAccountCreationSuccess":true,"region":"CA"});
            }
            if (MVCApp.Toolbox.common.getRegion() == "US") {
                var count = MVCApp.Toolbox.common.getGuestBasketCount() || 0;
                if (count > 0) MVCApp.Toolbox.common.setMerge(true);
                MVCApp.Toolbox.common.createBasketforUser(function() {
                    MVCApp.Toolbox.common.setGuestBasketCount(0);
                });
            }
        } else {
            var status = response.Status || [];
            if (status.length > 0) {
                var errDesc = status[0].errDescription || "";
                var errCode = status[0].errCode || "";
                cust360Obj.accountCreationStatus = "failure";
                cust360Obj.failureCode = errCode;
                cust360Obj.email = frmCreateAccount.txtEmail.text || "";
                if (errCode == "LoginAlreadyInUseException") {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.loginInUse"), "");
                } else MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.emailInUse"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.tryAgainTitle"), constants.ALERT_TYPE_CONFIRMATION, makeCall, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.callUS"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmTouchId.mayBeLater"));
            }
            var timeStamp = new Date();
            var region = MVCApp.Toolbox.common.getRegion();
            var reportObj = {
                "timeStamp": timeStamp,
                "region": region
            };
            reportObj = JSON.stringify(reportObj);
            MVCApp.sendMetricReport(frmCreateAccount, [{
                "SignUpFailure": reportObj
            }])
        }
        MVCApp.Customer360.sendInteractionEvent("CreateAccount", cust360Obj);
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function makeCall(res) {
        if (res) {
            try {
                kony.phone.dial("1-800-642-4235");
            } catch (e) {
                kony.print("unhandled error from iOS");
                if (e) {
                    TealiumManager.trackEvent("Dial Exception While Creating An Account", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }
    //Here we expose the public variables and functions
    function visibilityButtons() {
        if (isFromShoppingList) {
            frmCreateAccount.btnYes.setVisibility(false);
            frmCreateAccount.btnRewardProgramInfo.setVisibility(false);
            frmCreateAccount.btnNotRightNow.setVisibility(false);
            frmCreateAccount.flxLine.setVisibility(false);
            frmCreateAccount.flxLine2.setVisibility(false);
            frmCreateAccount.flxLine3.setVisibility(false);
            frmCreateAccount.flxLine3.setVisibility(false);
            frmCreateAccount.btnNotRightNow.setVisibility(false);
            //frmCreateAccount.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.shoppingList.rewardsAccount");
            frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileCreate");
            frmCreateAccount.btnOk.setVisibility(true);
        } else if (rewardsInfoFlag) {
            frmCreateAccount.btnYes.setVisibility(true);
            frmCreateAccount.btnRewardProgramInfo.setVisibility(false);
            frmCreateAccount.flxLine.setVisibility(true);
            frmCreateAccount.flxLine2.setVisibility(true);
            frmCreateAccount.flxLine3.setVisibility(false);
            frmCreateAccount.btnNotRightNow.setVisibility(false);
            if (MVCApp.Toolbox.common.getRegion() === "US") {
                frmCreateAccount.btnNotRightNow.setVisibility(false);
                frmCreateAccount.flxLine3.setVisibility(false);
            } else {
                frmCreateAccount.btnNotRightNow.setVisibility(true);
                frmCreateAccount.flxLine3.setVisibility(true);
            }
            //frmCreateAccount.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileSetup");
            frmCreateAccount.btnOk.setVisibility(false);
            frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileCreate");
        } else {
            frmCreateAccount.btnYes.setVisibility(true);
            frmCreateAccount.btnRewardProgramInfo.setVisibility(true);
            frmCreateAccount.btnNotRightNow.setVisibility(true);
            frmCreateAccount.flxLine.setVisibility(true);
            frmCreateAccount.flxLine2.setVisibility(true);
            frmCreateAccount.flxLine3.setVisibility(true);
            frmCreateAccount.flxLine3.setVisibility(false);
            frmCreateAccount.btnNotRightNow.setVisibility(false);
            // frmCreateAccount.lbl2.text =MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileSetup");
            frmCreateAccount.btnOk.setVisibility(false);
            frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.profileCreate");
            //i18n.phone.frmCreateAccount.profileCreate
        }
    }

    function disabilityButtons(response) {
        frmCreateAccount.btnYes.setVisibility(false);
        frmCreateAccount.btnRewardProgramInfo.setVisibility(false);
        frmCreateAccount.btnNotRightNow.setVisibility(false);
        frmCreateAccount.flxLine.setVisibility(false);
        frmCreateAccount.flxLine2.setVisibility(false);
        frmCreateAccount.flxLine3.setVisibility(false);
        var regStatus = response.RegistrationResponse || [];
        if (response.hasOwnProperty("UpdateUserErrorMessage")) {
            if (response.UpdateUserErrorMessage === "Phone number already exists") {
                frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.phoneInUse");
            } else {
                frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.noRewardsAccountError") + response.UpdateUserErrorMessage;
            }
        } else if (!response.hasOwnProperty("c_loyaltyMemberID") && regStatus !== null && regStatus !== undefined && regStatus.length > 0) {
            var state = regStatus[0].Status || "";
            if (state == "FAILED") {
                frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.phoneInUse");
            }
        } else {
            frmCreateAccount.lbl2.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCreateAccount.noRewardsAccount");
        }
        frmCreateAccount.btnOk.setVisibility(true);
    }

    function onClickOfOK() {
        //createAccountResObj = kony.store.getItem("userObj");
        if (isFromShoppingList) {
            if (frmObject.id === "frmPDP" || frmObject.id === "frmPJDP") {
                var prodOrProjID = kony.store.getItem("shoppingListProdOrProjID");
                frmObject.show();
                kony.print("LOG:onClickOfOK - prodOrProjID:" + prodOrProjID);
                if (frmObject.id === "frmPDP") {
                    MVCApp.getShoppingListController().addToProductList(prodOrProjID, MVCApp.Service.Constants.ShoppingList.pdpType);
                } else {
                    MVCApp.getShoppingListController().addProjectListLooping(prodOrProjID);
                }
            } else {
                MVCApp.getShoppingListController().getProductListItems();
            }
            isFromShoppingList = false;
        } else {
            if (MVCApp.Toolbox.common.getRegion() === "US") {
                var createAccountResObj = kony.store.getItem("userObj");
                MVCApp.getJoinRewardsController().load(createAccountResObj);
            } else {
                MVCApp.getMoreController().load();
            }
        }
    }

    function addProdOrProjToShoppingList() {
        var prodOrProjID = kony.store.getItem("shoppingListProdOrProjID");
        if (prodOrProjID) {
            frmObject.show();
            if (frmObject.id === "frmPDP") {
                MVCApp.getShoppingListController().addToProductList(prodOrProjID, MVCApp.Service.Constants.ShoppingList.pdpType);
            } else {
                MVCApp.getShoppingListController().addProjectListLooping(prodOrProjID);
            }
        } else {
            frmObject.show();
        }
    }
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        showResponse: showResponse
    };
});