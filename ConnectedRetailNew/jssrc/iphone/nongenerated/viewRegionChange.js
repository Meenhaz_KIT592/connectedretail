/**
 * PUBLIC
 * This is the view for the WeeklyAd form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.RegionChangeView = (function() {
    var selectedRegion = "";
    /**
     * PUBLIC
     * Open the More form
     */
    function highlightSelectedLanguage() {
        var langReg = kony.store.getItem("languageSelected");
        for (var i = 1; i <= 3; i++) {
            if (langReg == MVCApp.serviceConstants.languageArr[i - 1]) {
                frmRegions["lblTickLang" + i].setVisibility(true);
            } else {
                frmRegions["lblTickLang" + i].setVisibility(false);
            }
        }
    }

    function show() {
        highlightSelectedLanguage();
        frmRegions.show();
    }

    function langChangeSuccess(oldlocalename, newlocalename, info) {
        var locale = kony.i18n.getCurrentLocale();
        kony.print("The locale is " + locale);
        var languagePrev = kony.store.getItem("languageSelected");
        kony.print("the language previous is " + languagePrev);
        if (languagePrev.indexOf("en") !== -1 || languagePrev === "default") {
            languagePrev = "en";
        } else {
            languagePrev = "fr";
        }
        kony.store.setItem("languageSelected", locale);
        sfmcCustomObject.Region = (locale == "fr_CA") ? "fr" : locale;
        sfmcCustomObject.Indiv_Id = "";
        if (OneManager) {
            OneManager.sfmcUpdateCustomAttributes(sfmcCustomObject);
        }
        highlightSelectedLanguage();
        initRuntimePermissionObjects();
        var regionPrev = kony.store.getItem("Region");
        MVCApp.sendMetricReport(frmMore, [{
            "device_Locale": locale
        }]);
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.region = locale;
        MVCApp.Customer360.sendInteractionEvent("RegionLanguageChange", cust360Obj);
        var whenStoreNotSet = {
            "doesLoadCouponsWhenStoreNotSet": false,
            "fromWhichScreen": ""
        };
        whenStoreNotSet = JSON.stringify(whenStoreNotSet);
        kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
        destroyAllForms();
        try {
            frmSignIn.destroy();
        } catch (e) {
            kony.print("form was already destroyed");
            if (e) {
                TealiumManager.trackEvent("Exception While Destroying Sign In Form", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
        clearGlobals();
        var country = "US";
        if (locale == "en" || locale == "en_US") {
            country = "US";
        } else {
            country = "CA";
        }
        kony.store.setItem("Region", country);
        // MVCApp.Toolbox.common.updateCart();
        //	MVCApp.Toolbox.common.setGuestBasketCount(0);
        try {
            MVCApp.Toolbox.common.setProducts([]);
            MVCApp.Toolbox.common.removeBasket();
            MVCApp.Toolbox.common.removeCustomerId();
            MVCApp.Toolbox.common.removeJWTToken();
            MVCApp.Toolbox.common.setNumberOfItemsInCart(0);
            MVCApp.Toolbox.common.updateCart();
            frmWebView.destroy();
            frmCartView.destroy();
            MVCApp.Toolbox.common.addBrowserWidgetToForm();
            MVCApp.Toolbox.common.postLogoutUrl();
            kony.net.clearCookies(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels"));
        } catch (error) {
            kony.print(error);
        }
        if (country === "US") {
            MVCApp.Toolbox.common.updateCartIcon(frmRegions);
        }
        if (country === regionPrev) {
            //dont need to remove the favourite store
        } else {
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                exitStoreMode();
            }
            kony.store.setItem("myLocationDetails", "");
            MVCApp.Toolbox.Service.setMyLocationFlag(false);
        }
        kony.i18n.setDefaultLocaleAsync(newlocalename, function(oldlocale, newlocale) {
            kony.print("the default locale has been set to " + newlocale);
            MVCApp.Toolbox.Service.setServiceType();
            isFromShoppingList = false;
            var userObject = kony.store.getItem("userObj");
            if (userObject === "" || userObject === null) {
                // MVCApp.getHomeController().load();
                getProductsCategory(true, MVCApp.getHomeController().load());
            } else {
                if (regionPrev === country) {
                    // MVCApp.getHomeController().load();
                    getProductsCategory(true, MVCApp.getHomeController().load());
                    //dont need to remove sigin information
                } else {
                    kony.store.setItem("userObj", "");
                    setUserDetailsForTealiumTagging(kony.store.getItem("userObj"));
                    getProductsCategory(true, MVCApp.getSignInController().load());
                }
            }
            if (locale.indexOf("en") !== -1 || locale === "default") {
                locale = "en";
            } else {
                locale = "fr";
            }
            if (kony.store.getItem("pushSwitch")) {
                var deviceID = kony.os.deviceInfo().identifierForVendor;
                MVCApp.PushNotification.common.modifyUser(deviceID + "@michaels.com", deviceID, locale, country);
            }
        }, function(err) {
            kony.print("some error occured IN SETTING DEFAULT lOCALE. " + JSON.stringify(err));
        });
    }

    function langChangeFailure() {
        kony.print("some error occured IN SETTING Current lOCALE.");
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function selectLanguage(eventobj) {
        selectedRegion = eventobj;
        var id = eventobj.info.id;
        var lang = eventobj.info.langRegion;
        var selectedLang = kony.store.getItem("languageSelected");
        var region = kony.store.getItem("Region");
        var country = "US";
        if (lang == "en" || lang == "en_US") {
            country = "US";
        } else {
            country = "CA";
        }
        if (selectedLang == lang) {} else {
            MVCApp.Toolbox.common.showLoadingIndicator("");
            if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
                var userObject = kony.store.getItem("userObj");
                if (userObject === "" || userObject === null) {
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                        if (country === region) {
                            kony.i18n.setCurrentLocaleAsync(lang, langChangeSuccess, langChangeFailure, selectedRegion.info);
                        } else {
                            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRegions.switchingWhenStoreModeEnabled"), "", constants.ALERT_TYPE_CONFIRMATION, onClickRegionChangeSignIn, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertYes"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertNo"));
                        }
                    } else {
                        kony.i18n.setCurrentLocaleAsync(lang, langChangeSuccess, langChangeFailure, selectedRegion.info);
                    }
                } else {
                    if (country === region) {
                        kony.i18n.setCurrentLocaleAsync(lang, langChangeSuccess, langChangeFailure, selectedRegion.info);
                    } else {
                        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRegions.switchingWhenStoreModeEnabledAndUserIsSignedIn"), "", constants.ALERT_TYPE_CONFIRMATION, onClickRegionChangeSignIn, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertYes"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertNo"));
                        } else {
                            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRegions.switchingRegion"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.warningTitle"), constants.ALERT_TYPE_CONFIRMATION, onClickRegionChangeSignIn, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertYes"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmHelp.alertNo"));
                        }
                    }
                }
            } else {
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.noNetworkAvailable"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, function() {}, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"));
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            }
        }
    }

    function onClickRegionChangeSignIn(response) {
        if (response) {
            var lang = selectedRegion.info.langRegion;
            kony.i18n.setCurrentLocaleAsync(lang, langChangeSuccess, langChangeFailure, selectedRegion.info);
        } else {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
    }

    function navigateBack() {
        var prevForm = kony.application.getPreviousForm();
        var prevFormId = prevForm.id || "";
        if (prevFormId == "frmSettings") {
            MVCApp.getSettingsController().load();
        } else if (prevFormId == "frmFindStoreMapView") {
            MVCApp.getLocatorMapController().showUI();
        }
    }

    function bindEvents() {
        for (var i = 1; i <= 3; i++) {
            frmRegions["btnLangRegion" + i].info = {
                id: i,
                langRegion: MVCApp.serviceConstants.languageArr[i - 1]
            };
            frmRegions["btnLangRegion" + i].onClick = selectLanguage;
        }
        frmRegions.btnHeaderLeft.onClick = navigateBack;
        frmRegions.postShow = function() {
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Region and Language Change: Michaels Mobile App";
            lTealiumTagObj.page_name = "Region and Language Change: Michaels Mobile App";
            lTealiumTagObj.page_type = "Regional";
            lTealiumTagObj.page_category_name = "Regional";
            lTealiumTagObj.page_category = "Regional";
            TealiumManager.trackView("Region and Language Change Screen", lTealiumTagObj);
        };
    }
    return {
        show: show,
        bindEvents: bindEvents
    };
});