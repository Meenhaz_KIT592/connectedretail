/**
 * PUBLIC
 * This is the model for formProductCategory form
 */
var MVCApp = MVCApp || {};
MVCApp.ProductCategoryModel = (function() {
    var serviceType = MVCApp.serviceType.getAisleNumbers; //MVCApp.serviceType.DemandwareService;
    var operationId = MVCApp.serviceEndPoints.getAisleNumbersForCategory; //MVCApp.serviceEndPoints.ProductCategory;
    function loadSubCategories(callback, requestParams) {
        kony.print("modelProductCategory.js");
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.opstatus === 0 || results.opstatus === "0") {
                var productsDataObj = new MVCApp.data.Products(results);
                callback(productsDataObj);
            }
        }, function(error) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("Response is " + JSON.stringify(error));
            var productsDataObj = new MVCApp.data.Products(error);
            callback(productsDataObj);
        });
        /* var productList  =   kony.store.getItem("GetProductCategories");
             
          //  var productsDataObj =[];
         
           for(i=0;i<productList.length;i++)
               {
                    kony.print(productList[i].id +" , "+ requestParams.id);
                 //kony.print(" categories "+JSON.stringify(productList[i]));
            		 if(productList[i].id == requestParams.id )
                     { 
                     
                                    
                       var  productsDataObj = new MVCApp.data.Products(productList[i]);
                       
                         callback(productsDataObj);
                     }
              } */
    }
    return {
        loadSubCategories: loadSubCategories
    };
});