//Type your code here
function tapNone() {
    frmHome.flxFooterWrap.flxProducts.skin = 'slFbox';
    frmHome.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmHome.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmHome.flxFooterWrap.flxEvents.skin = 'slFbox';
    frmHome.flxFooterWrap.flxMore.skin = 'slFbox';
}

function NAV_frmHome() {
    frmHome.show();
    tapNone();
}

function preHome() {
    //frmHome.segHomeCarousel.pageSkin = "seg2Normal";
    frmHome.flxHeaderWrap.btnHome.isVisible = false;
    frmHome.flxHeaderWrap.textSearch.text = '';
    exitInStoreInfo();
}

function exitInStoreInfo() {
    /*function test_CALLBACK() {
      	//animateInStoreMode();
    }*/
    frmHome.flxInStoreNew.isVisible = false;
    frmHome.flxInStoreNew.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "top": "-1dp",
            "height": "1dp"
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmHome.lblFindit1.animate(kony.ui.createAnimation({
        "100": {
            "top": "-40dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    });
    frmHome.flxLineVertical.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "height": "1dp"
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmHome.flxLineHorizontal.animate(kony.ui.createAnimation({
        "100": {
            "top": "0dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "width": "1dp"
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(0.01, 0.01);
    frmHome.flxInStoreBtn1.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmHome.flxInStoreBtn2.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmHome.flxInStoreBtn3.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmHome.flxInStoreBtn4.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
}

function showStoreInfo() {
    var form = kony.application.getCurrentForm();

    function anim_STOREINFO() {
        form.flxMyStoreContainer.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "height": "190dp"
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
    }
    form.btnChooseStore.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "top": "-100%"
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
        "animationEnd": anim_STOREINFO
    });
}
// In Store Mode Animation Script
function animateInStoreMode() {
    frmHome.flxInStoreNew.isVisible = true;
    //var form = kony.application.getCurrentForm();
    function anim_FlxInStoreBtn1() {
        frmHome.lblFindit1.animate(kony.ui.createAnimation({
            "100": {
                "top": "0dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
        frmHome.flxLineVertical.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "height": "100%"
            }
        }), {
            "delay": 1.1,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmHome.flxLineHorizontal.animate(kony.ui.createAnimation({
            "100": {
                "top": "0dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "width": "100%"
            }
        }), {
            "delay": 1.2,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        var trans101 = kony.ui.makeAffineTransform();
        trans101.scale(1, 1);
        frmHome.flxInStoreBtn1.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.7,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmHome.flxInStoreBtn2.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.8,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmHome.flxInStoreBtn3.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 1,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmHome.flxInStoreBtn4.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.9,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
    }
    frmHome.flxInStoreNew.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "top": "10dp",
            "height": "250dp"
        }
    }), {
        "delay": 0.8,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
        "animationEnd": anim_FlxInStoreBtn1
    });
}

function hideFooter() {
    //function test_CALLBACK() { }
    try {
        var form = kony.application.getCurrentForm();
        if (form.flxFooterWrap) {
            form.flxFooterWrap.animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "bottom": "-60dp"
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.5
            });
        }
    } catch (e) {
        kony.print("exception in hideFooter " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Hide Footer Exception", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function dispFooter() {
    //function test_CALLBACK() { }
    try {
        var form = kony.application.getCurrentForm();
        form.flxFooterWrap.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "bottom": "0dp"
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        });
    } catch (e) {
        kony.print("exception in " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Display Footer Exception", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function Alert_MyStore(eventobject) {
    return Alert_Location(eventobject);
}

function Alert_Location(eventobject) {
    function SHOW_ALERT_EnableLocation_True() {
        showStoreInfo.call(this);
    }

    function SHOW_ALERT_EnableLocation_False() {}

    function SHOW_ALERT_EnableLocation_Callback(response) {
        if (response === true) {
            SHOW_ALERT_EnableLocation_True();
        } else {
            SHOW_ALERT_EnableLocation_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Enable Location Services",
        "yesLabel": "Continue",
        "noLabel": "Cancel",
        "message": "Please enable Location Services in your device settings to get nearby store info.",
        "alertHandler": SHOW_ALERT_EnableLocation_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}

function Alert_InStoreMode() {
    function SHOW_ALERT_InStoreMode_True() {
        animateInStoreMode.call(this);
    }

    function SHOW_ALERT_InStoreMode_False() {}

    function SHOW_ALERT_InStoreMode_Callback(response) {
        if (response === true) {
            SHOW_ALERT_InStoreMode_True();
        } else {
            SHOW_ALERT_InStoreMode_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "In Store Mode Alert",
        "yesLabel": "Animate",
        "noLabel": "Cancel",
        "message": "This is just dummy alert to showcase in store mode animation. Please follow FSD.",
        "alertHandler": SHOW_ALERT_InStoreMode_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}