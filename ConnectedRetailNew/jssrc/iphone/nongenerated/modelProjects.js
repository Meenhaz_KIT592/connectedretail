/**
 * PUBLIC
 * This is the model for formProjects form
 */
var MVCApp = MVCApp || {};
MVCApp.ProjectsModel = (function() {
    var serviceType = MVCApp.serviceType.DemandwareService;
    var operationId = MVCApp.serviceEndPoints.ProjectCategories; //MVCApp.serviceEndPoints.ProductCategory;
    function loadData(callback, requestParams) {
        kony.print("modelProjects.js");
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.opstatus === 0 || results.opstatus === "0") {
                var projectsDataObj = new MVCApp.data.Projects(results);
                callback(projectsDataObj);
            }
        }, function(error) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("Response is " + JSON.stringify(error));
        });
    }
    return {
        loadData: loadData
    };
});