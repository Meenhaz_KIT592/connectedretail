/**
 * PUBLIC
 * This is the controller for the ProductsList form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProductsListController = (function() {
    var lServiceRequestObj = {};
    var fromImageSearch = false;
    var _prodCountFromImgSearch = 0;
    var _projCountFromImgSearch = 0;
    var _qrlink = "";
    var setHeader;
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    var _total = 0;
    var _start = 0;
    var prevFormName = "";

    function setTotal(total) {
        _total = total;
    }

    function getTotal() {
        return _total;
    }

    function setStart(start) {
        _start = start;
    }

    function setQRlink(qrLink) {
        _qrlink = qrLink;
    }

    function getQRlink() {
        return _qrlink;
    }

    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.ProductsListModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.ProductsListView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    var _params;
    var fromSearchFlag = false;
    var storeNo = "";
    var isStoreModeAlreadyEnabled = false;

    function setStoreModeStatus(pIsStoreModeAlreadyEnabled) {
        isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
    }

    function getStoreModeStatus() {
        return isStoreModeAlreadyEnabled;
    }

    function setStoreId(storeID) {
        storeNo = storeID;
    }

    function getStoreId() {
        return storeNo;
    }

    function _getRequestParams() {
        return _params;
    }

    function _setRequestParams(reqParams) {
        _params = reqParams;
    }

    function _checkIfFromSearch() {
        return fromSearchFlag;
    }

    function _setFromSearch(fromSearch) {
        fromSearchFlag = fromSearch;
    }

    function getQueryString() {
        var queryString = "";
        if (_getRequestParams()) {
            queryString = _getRequestParams().q;
        }
        return queryString;
    }
    var defaultParams = "";

    function setDefaultParams(value) {
        defaultParams = value;
    }

    function getDefaultParams() {
        return defaultParams;
    }
    var backParams = {};

    function setParamsForBack(params) {
        backParams = params;
    }

    function getParamsForBack() {
        return backParams;
    }
    var fromProdSubCat = false;

    function setFromProdSubCat(value) {
        fromProdSubCat = value;
    }

    function getFromProdSubCat() {
        return fromProdSubCat;
    }

    function setPreviousForm(name) {
        prevFormName = name;
    }

    function getPreviousForm() {
        return prevFormName;
    }
    var fromRefine = false;

    function setFromRefineFlag(value) {
        fromRefine = value;
    }

    function getFromRefineFlag() {
        return fromRefine;
    }
    var projectCount = 0;

    function setProjectCount(value) {
        projectCount = value;
    }

    function getProjectCount() {
        return projectCount;
    }

    function load(query, fromSearch, sortValue, refStringCgid, fromSort, fromBack, fromProdCat1) {
        lServiceRequestObj.query = query;
        lServiceRequestObj.fromSearch = fromSearch;
        lServiceRequestObj.sortValue = sortValue;
        lServiceRequestObj.refStringCgid = refStringCgid;
        lServiceRequestObj.fromSort = fromSort;
        lServiceRequestObj.fromBack = fromBack;
        lServiceRequestObj.fromProdCat1 = fromProdCat1;
        setFromRefineFlag(false);
        var fromProdCat = fromProdCat1 || false;
        _setFromSearch(fromSearch);
        var paramsBack = {
            "query": query,
            "fromSearch": fromSearch,
            "sortValue": sortValue,
            "refStringCgid": refStringCgid,
            "fromSort": fromSort
        };
        setParamsForBack(paramsBack);
        var sort = sortValue || "";
        var sortString = MVCApp.Service.Constants.Search.sort;
        if (sort !== "") {
            sortString = sort;
        }
        if (!fromSearch) {
            var prevFormName = "";
            var prevForm = kony.application.getCurrentForm() || "";
            prevFormName = prevForm.id || "";
            setPreviousForm(prevFormName);
        }
        firstRecordIndex = 0;
        lastRecordIndex = 0;
        completeResponse = {};
        completeData = [];
        var storeID = "";
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeID = gblInStoreModeDetails.storeID;
            setStoreModeStatus(true);
        } else {
            if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
                var locDetailsStr = kony.store.getItem("myLocationDetails");
                try {
                    var LocDetails = JSON.parse(locDetailsStr);
                    storeID = LocDetails.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlProductsList.js on load for locDetailsStr:" + JSON.stringify(e)
                    }]);
                    storeID = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception In PLP Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                storeID = "";
            }
        }
        setStoreId(storeID);
        kony.print("ProductsList Controller.load");
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.q = fromSearch ? query : "";
        requestParams.expand = MVCApp.Service.Constants.Search.expand;
        requestParams.sort = sortString;
        requestParams.start = MVCApp.Service.Constants.Search.start;
        requestParams.count = MVCApp.Service.Constants.Search.count;
        requestParams.store_id = storeID;
        _setRequestParams(requestParams);
        if (fromSort) requestParams.refineString = refStringCgid;
        else {
            var queryID = query ? query.id : "";
            requestParams.refineString = fromSearch ? MVCApp.Service.Constants.Search.refineString : "refine=cgid=" + queryID;
            setDefaultParams(requestParams);
        }
        init();
        if (query != "") {
            setHeader = query;
            setHeaderText(query);
        } else setHeaderText(setHeader);
        var count = 0;
        if (fromBack) {
            //           //#ifdef iphone
            //           view.defaultForm();
            //       	  //#endif
        } else {
            setFromProdSubCat(fromProdCat);
            //           view.defaultForm();
        }
        if (voiceSearchFlag) {
            MVCApp.Toolbox.common.showLoadingIndicatorForVoiceSearch("");
        } else {
            MVCApp.Toolbox.common.showLoadingIndicator("");
        }
        model.showSearchResults(view.updateScreen, requestParams, MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchProduct, fromSearch, count, fromSort);
        MVCApp.getSortProductController().setEntryInputParams(requestParams);
        MVCApp.getSortProductController().setSearchValue(fromSearch);
    }

    function loadFromSortRefine(query, refineString, sort) {
        MVCApp.Toolbox.common.showLoadingIndicator("");
        firstRecordIndex = 0;
        lastRecordIndex = 0;
        completeResponse = {};
        completeData = [];
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.refineString = refineString;
        requestParams.q = query;
        requestParams.expand = MVCApp.Service.Constants.Search.expand;
        requestParams.sort = sort;
        requestParams.start = MVCApp.Service.Constants.Search.start;
        requestParams.count = MVCApp.Service.Constants.Search.count;
        _setRequestParams(requestParams);
        var fromSearch = _checkIfFromSearch();
        var count = 0;
        model.refreshSortpageOnly(MVCApp.getSortProductController().load, requestParams, MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchProduct, fromSearch, count);
    }

    function loadListWOService(dataObj) {
        var fromSearch = _checkIfFromSearch();
        var count = 0;
        view.updateScreen(dataObj, fromSearch, count);
    }

    function loadForPDP(barcode) {
        init();
        MVCApp.Toolbox.common.showLoadingIndicator("");
        //MVCApp.getPDPController().setEntryType("barcodeScan");
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        var storeID = "";
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            var locDetailsStr = kony.store.getItem("myLocationDetails");
            try {
                var LocDetails = JSON.parse(locDetailsStr);
                storeID = LocDetails.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "ctrlProductsList.js on loadForPDP for locDetailsStr:" + JSON.stringify(e)
                }]);
                storeID = "";
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception In PLP Screen When Loading From Barcode Scanning Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeID = "";
        }
        gblTealiumTagObj.storeid = storeID;
        requestParams.refineString = MVCApp.Service.Constants.Search.refineString;
        requestParams.q = barcode;
        requestParams.expand = MVCApp.Service.Constants.Search.expand;
        requestParams.sort = MVCApp.Service.Constants.Search.sort;
        requestParams.start = MVCApp.Service.Constants.Search.start;
        requestParams.count = MVCApp.Service.Constants.Search.count;
        requestParams.store_id = storeID;
        _setRequestParams(requestParams);
        var fromSearch = true;
        MVCApp.getPDPController().setQueryString("");
        var count = 0;
        model.setForPDP(MVCApp.getPDPController().load, requestParams, MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchProduct, fromSearch, count, true);
    }

    function checkIfRequired() {
        if ((_start + 30) > _total) {
            return false;
        } else {
            return true;
        }
    }

    function showMore(segui) {
        if (checkIfRequired()) {
            var requestParams = _getRequestParams();
            requestParams.start = requestParams.start + 30;
            var count = requestParams.start;
            model.showSearchResults(view.showMore, requestParams, MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchProduct, _checkIfFromSearch(), count);
        }
    }

    function isServiceRequired() {
        if (completeData !== null && lastRecordIndex !== null && completeData !== undefined && lastRecordIndex !== undefined) return ((completeData.length - lastRecordIndex) > 0 && (completeData.length - lastRecordIndex) <= MVCApp.Service.Constants.Search.showCount);
        return true;
    }

    function pushResults() {
        if (completeData !== null && completeData !== undefined && lastRecordIndex < _total) {
            view.showLoading(true);
        }
        model.pushRecords(view.pushMoreRecords);
        if (isServiceRequired()) {
            showMore(frmProductsList.segProductsList);
        }
    }
    var refineCount = 0;

    function setRefineCount(value) {
        refineCount = value;
    }

    function getRefineCount() {
        return refineCount;
    }

    function backToScreen() {
        if (!_isInit) {
            init();
        }
        view.staticShow();
    }
    var prodList = {};

    function setDataForProductList(value) {
        prodList = value;
    }

    function getDataObjForProductList() {
        return prodList;
    }

    function setHeaderText(headerText) {
        view.setHeaderText(headerText);
    }

    function loadForNoResults(fromSearch) {
        init();
        view.showForNoResults(fromSearch);
    }

    function getHeader() {
        return setHeader;
    }

    function goToProjects() {
        var query = getQueryString();
        frmProjectsList.segProjectList.removeAll();
        frmProjectsList.flxRefineSearchWrap.isVisible = false;
        frmProjectsList.flxCategory.isVisible = false;
        frmProjectsList.flxHeaderWrap.textSearch.text = query;
        frmProjectsList.flxSearchResultsContainer.isVisible = false;
        frmProjectsList.flxProdProjTabs.isVisible = false;
        gblIsFromImageSearch = false;
        imageSearchProjectBasedFlag = false;
        MVCApp.getProjectsListController().load(query, true);
    }

    function resetTextAndClear() {
        var text = MVCApp.getProductsListController().getQueryString();
        frmProductsList.flxHeaderWrap.textSearch.text = text;
    }
    var isFromDeeplink = false;

    function setFromDeepLink(fromDeeplink) {
        isFromDeeplink = fromDeeplink;
    }

    function getFromDeepLink() {
        return isFromDeeplink;
    }
    var headerNameDeeplink = "";

    function getHeaderNameDeeplink() {
        return headerNameDeeplink;
    }

    function setHeaderNameDeeplink(cgid) {
        headerNameDeeplink = cgid;
    }

    function updateScreen(results, fromSearch, count) {
        view.updateScreen(results, fromSearch, count);
    }

    function updateProjectsCount(totalProjects, fromSearch) {
        view.updateProjectsCount(totalProjects, fromSearch)
    }

    function getFromImageSearch() {
        return fromImageSearch;
    }

    function setFromImageSearch(value) {
        fromImageSearch = value;
    }

    function getProductTotalFromImageSearch() {
        return _prodCountFromImgSearch;
    }

    function setProductTotalFromImageSearch(value) {
        _prodCountFromImgSearch = value;
    }

    function getProjectTotalFromImageSearch() {
        return _projCountFromImgSearch;
    }

    function setProjectTotalFromImageSearch(value) {
        _projCountFromImgSearch = value;
    }
    var entryType = "";

    function getEntryType() {
        return entryType;
    }

    function setEntryType(value) {
        entryType = value;
    }
    var productIds = [];

    function setProductIds(values) {
        productIds = values;
    }

    function getProductIds() {
        return productIds;
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        showMore: showMore,
        getQueryString: getQueryString,
        _getRequestParams: _getRequestParams,
        setStart: setStart,
        setTotal: setTotal,
        getTotal: getTotal,
        backToScreen: backToScreen,
        loadFromSortRefine: loadFromSortRefine,
        loadListWOService: loadListWOService,
        getDefaultParams: getDefaultParams,
        setDefaultParams: setDefaultParams,
        setRefineCount: setRefineCount,
        getRefineCount: getRefineCount,
        setDataForProductList: setDataForProductList,
        getDataObjForProductList: getDataObjForProductList,
        loadForPDP: loadForPDP,
        loadForNoResults: loadForNoResults,
        pushResults: pushResults,
        getHeader: getHeader,
        setStoreId: setStoreId,
        getStoreId: getStoreId,
        setStoreModeStatus: setStoreModeStatus,
        setParamsForBack: setParamsForBack,
        getParamsForBack: getParamsForBack,
        getFromProdSubCat: getFromProdSubCat,
        getPreviousForm: getPreviousForm,
        setProjectCount: setProjectCount,
        getProjectCount: getProjectCount,
        goToProjects: goToProjects,
        setFromRefineFlag: setFromRefineFlag,
        getFromRefineFlag: getFromRefineFlag,
        resetTextAndClear: resetTextAndClear,
        lServiceRequestObj: lServiceRequestObj,
        getStoreModeStatus: getStoreModeStatus,
        getFromDeepLink: getFromDeepLink,
        setFromDeepLink: setFromDeepLink,
        setHeaderNameDeeplink: setHeaderNameDeeplink,
        getHeaderNameDeeplink: getHeaderNameDeeplink,
        updateScreen: updateScreen,
        updateProjectsCount: updateProjectsCount,
        setFromImageSearch: setFromImageSearch,
        getFromImageSearch: getFromImageSearch,
        setProductTotalFromImageSearch: setProductTotalFromImageSearch,
        getProductTotalFromImageSearch: getProductTotalFromImageSearch,
        getProjectTotalFromImageSearch: getProjectTotalFromImageSearch,
        setProjectTotalFromImageSearch: setProjectTotalFromImageSearch,
        setEntryType: setEntryType,
        getEntryType: getEntryType,
        setProductIds: setProductIds,
        getProductIds: getProductIds,
        setQRlink: setQRlink,
        getQRlink: getQRlink
    };
});