//Type your code here
function NAV_frmStoreMap() {
    frmStoreMap.show();
}

function displayDepartments() {
    frmStoreMap.browserDepartments.isVisible = true;
    frmStoreMap.browserAisles.isVisible = false;
    frmStoreMap.btnDepartments.skin = 'sknBtnSecondaryRed';
    frmStoreMap.btnDepartments.focusSkin = 'sknBtnSecondaryRedFoc';
    frmStoreMap.btnAisles.skin = 'sknBtnSecondaryGray';
    frmStoreMap.btnAisles.focusSkin = 'sknBtnSecondaryGrayFoc';
}

function displayAisles() {
    frmStoreMap.browserDepartments.isVisible = false;
    frmStoreMap.browserAisles.isVisible = true;
    frmStoreMap.btnDepartments.skin = 'sknBtnSecondaryGray';
    frmStoreMap.btnDepartments.focusSkin = 'sknBtnSecondaryGrayFoc';
    frmStoreMap.btnAisles.skin = 'sknBtnSecondaryRed';
    frmStoreMap.btnAisles.focusSkin = 'sknBtnSecondaryRedFoc';
}