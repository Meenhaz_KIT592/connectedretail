/**
 * PUBLIC
 * This is the controller for the Home form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var gblStoreNoForHome = "";
var glbStoreNoForSearch = "";
var MVCApp = MVCApp || {};
MVCApp.HomeController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    var gblLocale = "";
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.HomeModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.HomeView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    var storeNo = "";
    var isStoreModeAlreadyEnabled = false;

    function setStoreModeStatus(pIsStoreModeAlreadyEnabled) {
        isStoreModeAlreadyEnabled = pIsStoreModeAlreadyEnabled;
    }

    function getStoreModeStatus() {
        return isStoreModeAlreadyEnabled;
    }

    function getStoreID() {
        return storeNo;
    }

    function setStoreID(storeID) {
        storeNo = storeID;
    }

    function showNearest(location) {
        init();
        var reqParams = MVCApp.Service.getCommonInputParamaters();
        //reqParams.client_id = ;
        gblLocale = reqParams.locale;
        reqParams.refineString = MVCApp.serviceConstants.homeContentRefineString;
        var storeNo = "";
        var storeString = kony.store.getItem("myLocationDetails") || "";
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeNo = gblInStoreModeDetails.storeID;
        } else {
            if (storeString !== "") {
                try {
                    var storeObj = JSON.parse(storeString);
                    storeNo = storeObj.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlHome.js on showNearest for storeString:" + JSON.stringify(e)
                    }]);
                    storeNo = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception While Fetching Nearest Store In Home Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                storeNo = "";
            }
        }
        gblTealiumTagObj.storeid = storeNo;
        gblStoreNoForHome = storeNo;
        if (glbStoreNoForSearch === "") {
            glbStoreNoForSearch = storeNo;
        }
        reqParams.store_id = storeNo;
        model.loadData(view.updateScreen, reqParams);
        var searchReqParams = MVCApp.Service.getCommonInputParamaters();
        //searchReqParams.client_id = MVCApp.Service.Constants.Common.clientId;
        searchReqParams.refineString = MVCApp.serviceConstants.searchContentRefineString;
        searchReqParams.store_id = storeNo;
        searchReqParams.locale = reqParams.locale;
        model.loadSearchContentData(searchReqParams);
        var reqParams1 = {};
        reqParams1.client_id = MVCApp.Service.Constants.Common.clientId;
        reqParams1.refineString = MVCApp.serviceConstants.fetureProjectsRefineString;
        reqParams1.store_id = storeNo;
        reqParams1.locale = reqParams.locale;
        model.loadFeatureProject(reqParams1);
        var reqParamsForBlockStore = {};
        reqParamsForBlockStore["filter"] = storeNo;
        if (storeNo !== "") {
            kony.print("@@@LOG:showNearest-for blocked stores-storeNo:" + storeNo);
            model.updateMyStoreLocation(reqParamsForBlockStore);
        }
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load() {
        kony.print("Home Controller.load");
        init();
        var storeId = "";
        var language = kony.store.getItem("languageSelected");
        if (language == "default" || language == "en" || language == "en_US") {
            language = "default";
        } else {
            language = language ? language.replace("_", "-") : "";
        }
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            storeId = gblInStoreModeDetails.storeID;
            setStoreModeStatus(true);
        } else {
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (storeString !== "") {
                try {
                    var storeObj = JSON.parse(storeString);
                    storeId = storeObj.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlHome.js on load for storeString:" + JSON.stringify(e)
                    }]);
                    storeId = "";
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception While Loading The Home Screen", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            } else {
                storeId = "";
            }
        }
        setStoreID(storeId);
        gblTealiumTagObj.storeid = storeId;
        if (storeId === gblStoreNoForHome && language == gblLocale) {
            view.show();
        } else {
            showNearest();
        }
    }
    /**
     * @function
     *
     */
    function getUpdateStatus() {
        kony.print("in UpdateStatus of coupons analytics");
    }

    function loadCoupons() {
        init();
        var inputParams = {};
        var time = Math.floor(Date.now() / 1000);
        var userObject = kony.store.getItem("userObj");
        var serviceType = "couponservice";
        var operation;
        var language = kony.store.getItem("languageSelected");
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            inputParams.storeNo = gblInStoreModeDetails.storeID;
            inputParams.currentTimeValue = time + "";
            /*if(language == "en" || language == "en_US" || language == "en_CA"){
              inputParams.channel_code = "englishwebsite";
            }else{
              inputParams.channel_code = "frenchweb";
            }*/
            if (userObject) {
                if (userObject.hasOwnProperty("email")) {
                    inputParams.email = userObject.email || "";
                }
                if (userObject.hasOwnProperty("c_loyaltyMemberID")) {
                    // inputParams.user = gblDecryptedUser;
                    //inputParams.password = gblDecryptedPassword;
                    //inputParams.client_id = MVCApp.Toolbox.Service.getClientId();
                    var userObject = kony.store.getItem("userObj");
                    inputParams.loyaltyID = userObject.c_loyaltyMemberID || "";
                    // inputParams.loyaltyID=kony.store.getItem("loyaltyMemberId");
                    operation = "couponsrewards";
                } else {
                    operation = "couponsnonrewards";
                }
            } else {
                operation = "couponsnonrewards";
            }
            MVCApp.Toolbox.common.showLoadingIndicator("");
            MVCApp.HomeModel().getCoupons(serviceType, operation, MVCApp.CouponsView().coupons, inputParams);
        } else {
            if (MVCApp.Toolbox.Service.getMyLocationFlag() === "true") {
                var storeString;
                storeString = kony.store.getItem("myLocationDetails") || "";
                try {
                    var storeObj = JSON.parse(storeString);
                    inputParams.storeNo = storeObj.clientkey;
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "ctrlHome.js on loadCoupons for storeString:" + JSON.stringify(e)
                    }]);
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception While Loading Coupons", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
                inputParams.currentTimeValue = time + "";
                /* if(language=="en" || language =="en_US" || language =="en_CA"){
                   inputParams.channel_code="englishwebsite";
                 }else{
                   inputParams.channel_code="frenchweb";
                 }*/
                if (storeString != "") {
                    if (userObject) {
                        if (userObject.hasOwnProperty("email")) {
                            inputParams.email = userObject.email || "";
                        }
                        if (userObject.hasOwnProperty("c_loyaltyMemberID")) {
                            //inputParams.loyaltyID = ""; //userObject.c_loyaltyMemberID+"";
                            inputParams.user = gblDecryptedUser; //userObject.email+"";
                            inputParams.password = gblDecryptedPassword; //kony.store.getItem("userCredential");
                            inputParams.client_id = MVCApp.Toolbox.Service.getClientId();
                            operation = "couponsrewards"; // MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.cRewards");
                        } else {
                            operation = "couponsnonrewards"; //MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.cOnRewards");
                        }
                    } else {
                        operation = "couponsnonrewards"; //MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.cOnRewards");
                    }
                    MVCApp.Toolbox.common.showLoadingIndicator("");
                    kony.print("inputParams---" + JSON.stringify(inputParams));
                    MVCApp.HomeModel().getCoupons(serviceType, operation, MVCApp.CouponsView().coupons, inputParams);
                    //MVCApp.getCouponListController().load();
                    var inputParamsAnalytics = {};
                } else {
                    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreCoupons"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
                    inputParams.storeNo = "";
                }
            } else {
                var whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": true,
                    "fromWhichScreen": "coupons"
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStoreCoupons"), "", constants.ALERT_TYPE_CONFIRMATION, MVCApp.Toolbox.common.redirectToStoreLocation, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.selectStore"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
        }
    }

    function getCouponAnalytics(inputString) {
        var inputParamsAnalytics = {
            offers: inputString
        };
        MVCApp.HomeModel().getCouponAnalytics("couponservice", MVCApp.serviceEndPoints.updateStateCoupons, getUpdateStatus, inputParamsAnalytics);
    }

    function showDefaultView() {
        init();
        var locDetails = "";
        if (kony.store.getItem("myLocationDetails")) {
            locDetails = kony.store.getItem("myLocationDetails");
            try {
                locDetails = JSON.parse(locDetails);
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "ctrlHome.js on showDefaultView for locDetails:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Showing Default View Of Home Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        MVCApp.HomeView().loadMyStore(locDetails);
        view.show();
    }

    function beaconsDetails(UUID, major, minor) {
        var inputParams = {};
        inputParams["filter"] = UUID + "~" + major + "~" + minor;
        inputParams["currTime"] = new Date().getTime() / 1000;
        var detectionTimeStamp = new Date().getTime();
        var uniqueID = MVCApp.Toolbox.common.getDeviceID() + detectionTimeStamp;
        gblUniqueID = uniqueID;
        var obj = {
            "uniqueID": uniqueID,
            "timeStamp": detectionTimeStamp,
            "status": "detected",
            "timeTaken": "",
            "major": major,
            "UUID": UUID
        };
        gblBeaconObjArr.push(obj);
        kony.store.setItem("beaconData", JSON.stringify(gblBeaconObjArr));
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        var entryState = MVCApp.Toolbox.common.getEntryState();
        cust360Obj.entryType = "beacon";
        cust360Obj.major = major;
        cust360Obj.minor = minor;
        cust360Obj.storeid = major;
        cust360Obj.entryState = entryState;
        MVCApp.Customer360.sendInteractionEvent("StoreMode", cust360Obj);
        gblEntryType = "beacon";
        MVCApp.HomeModel().getStoreDetails(MVCApp.serviceType.KonyMFDBService, MVCApp.serviceEndPoints.getStoreUsingBeconId, createLocalnotification, inputParams);
    }

    function addCouponToWallet(requestParams, tag) {
        MVCApp.HomeModel().androidAddCoupon(requestParams, tag);
    }

    function addCouponToWalletIPhone(requestParams, callback, tag) {
        MVCApp.HomeModel().addCouponIphone(requestParams, callback, tag);
    }

    function onClickCarousel(eventObj) {
        view.onClickCarousel(eventObj);
    }

    function setStoreModeDetails() {
        try {
            var lCountry = kony.store.getItem("Region");
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                if (gblInStoreModeDetails.POI) {
                    if (gblInStoreModeDetails.POI.address2) {
                        frmHome.labelStoreName.text = gblInStoreModeDetails.POI.address2;
                    } else if (gblInStoreModeDetails.POI.city) {
                        frmHome.labelStoreName.text = gblInStoreModeDetails.POI.city;
                    } else {
                        frmHome.labelStoreName.text = gblInStoreModeDetails.storeName;
                    }
                } else {
                    frmHome.labelStoreName.text = gblInStoreModeDetails.storeName;
                }
                /*if(lCountry === "US"){
                  frmHome.flxRewards.isVisible = true;
                  frmHome.flxRewards.left = "25%";
                  frmHome.flxStoreMap.left = "0%";
                  frmHome.flxStoreMap.right = "25%";
                }else{
                  frmHome.flxRewards.isVisible = false;
                  frmHome.flxStoreMap.left = "37.5%";
                  frmHome.flxStoreMap.right = "37.5%";
                }*/
                frmHome.flxRewards.isVisible = true;
                frmHome.flxRewards.left = "25%";
                frmHome.flxStoreMap.left = "0%";
                frmHome.flxStoreMap.right = "25%";
                frmHome.flxMyStoreContainer.isVisible = false;
                frmHome.flxStoreModeContainer.isVisible = true;
            }
        } catch (e) {
            TealiumManager.trackEvent("Exception in setStoreModeDetails function", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }

    function onClickRewards() {
        init();
        var inputParams = {};
        var time = Math.floor(Date.now() / 1000);
        var userObject = kony.store.getItem("userObj");
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            if (userObject) {
                if (userObject.hasOwnProperty("email")) {
                    inputParams.email = userObject.email || "";
                }
                if (userObject.hasOwnProperty("c_loyaltyMemberID")) {
                    inputParams.loyaltyID = userObject.c_loyaltyMemberID + "";
                    var serviceType = "couponservice";
                    var operation = "couponsrewards";
                    inputParams.storeNo = gblInStoreModeDetails.storeID;
                    inputParams.currentTimeValue = time + "";
                    MVCApp.Toolbox.common.showLoadingIndicator("");
                    gblIsRewardsBeingShown = true;
                    MVCApp.HomeModel().getCoupons(serviceType, operation, MVCApp.CouponsView().coupons, inputParams);
                } else {
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.signUp"), "");
                }
            } else {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.signIn"), "");
            }
        }
    }

    function fetchInStoreModeStoreDetails() {
        var lReqParams = {};
        lReqParams.storeID = gblInStoreModeDetails.storeID;
        lReqParams.country = MVCApp.Service.Constants.Common.country;
        init();
        model.fetchInStoreModeStoreDetails(checkWhetherAStoreIsBlockedOrNot, lReqParams);
    }

    function checkWhetherAStoreIsBlockedOrNot() {
        var lReqParamsForBlockStore = {};
        if (gblInStoreModeDetails && gblInStoreModeDetails.storeID) {
            lReqParamsForBlockStore.filter = gblInStoreModeDetails.storeID;
            model.fetchListOfBlockedStores(createLocalNotificationForStoreMode, lReqParamsForBlockStore);
        }
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        showNearest: showNearest,
        loadCoupons: loadCoupons,
        getStoreID: getStoreID,
        setStoreID: setStoreID,
        showDefaultView: showDefaultView,
        beaconsDetails: beaconsDetails,
        getCouponAnalytics: getCouponAnalytics,
        addCouponToWallet: addCouponToWallet,
        addCouponToWalletIPhone: addCouponToWalletIPhone,
        getStoreModeStatus: getStoreModeStatus,
        onClickCarousel: onClickCarousel,
        setStoreModeDetails: setStoreModeDetails,
        onClickRewards: onClickRewards,
        fetchInStoreModeStoreDetails: fetchInStoreModeStoreDetails
    };
});