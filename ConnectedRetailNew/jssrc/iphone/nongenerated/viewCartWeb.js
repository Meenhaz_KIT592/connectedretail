/**
 * PUBLIC
 * This is the view for the ProductsList form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var fromCartSignIn = false;
var flag = false;
var isCheckout = false;
var storeChangeInStoreMode = false;
var favoritesFlag = false;
var cartFlag = false;
var fromBrowserViews = false;
var entryBrowserFormName = "";
var isCartPageFromMyAccount = false;
var k = 5;
MVCApp.CartView = (function() {
    /**
     * PUBLIC
     * Open the ProductsList form
     */
    var isCheckOutPlace = false;

    function bindEvents() {
        MVCApp.tabbar.bindIcons(frmCartView, "frmMore");
        MVCApp.tabbar.bindEvents(frmCartView);
        frmCartView.postShow = postShowCallback;
        //frmCartView.preShow = preShowCallback;
        //     frmCartView.btnBack.onClick = goPrevious;
        frmCartView.btnHeaderLeft.onClick = goPrevious;
        frmCartView.lblChevron.isVisible = true;
        frmCartView.btnBack.left = "23%";
        frmCartView.forceLayout();
        frmCartView.onHide = function() {
            // MVCApp.Toolbox.common.cancelTimer();
            //frmCartView.destroy();        
        };
        frmCartView.brwrView.onSuccess = function() {
            MVCApp.Toolbox.common.dismissLoadingIndicator("");
            frmCartView.brwrView.isVisible = true;
        };
        frmCartView.brwrView.onPageStarted = function(eventobject, params) {
            //  kony.print("The onPageStarted eventobject is: " + eventobject + "@@@@ params are: " + params["queryParams"]);
            MVCApp.Toolbox.common.showLoadingIndicatorForWebViews("");
            frmCartView.brwrView.clearCanvasBeforeLoading = true;
            var URL = params.originalURL || "";
            kony.print("In handle Request---onPageStarted-------->" + URL);
            if (URL !== "") {
                /* if (URL.indexOf("cartSubmit") !== -1) {
                   if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                     MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());

                   }
                 }*/
                if (URL.indexOf("cart?dwcont") !== -1 || URL.indexOf("cart?stid") !== -1) {
                    if (flag) {
                        // MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                        if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                            MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                        }
                        flag = false;
                    }
                } else {
                    if (URL.indexOf("on/demandware.store/Sites-MichaelsUS-Site/default/Wishlist-Show") !== -1) {
                        frmCartView.flxCart.isVisible = false;
                        frmCartView.btnCart.isVisible = false;
                        isCheckOutPlace = false;
                        favoritesFlag = true;
                        frmCartView.lblTitle.isVisible = true;
                        frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnMyAccount");
                        frmCartView.forceLayout();
                        flag = false;
                    }
                    if (URL.indexOf("COSummary-Submit") !== -1) {
                        // MVCApp.Toolbox.common.setProducts([]);
                        /* if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                           MVCApp.Toolbox.common.createBasketforUser();
                         }*/
                    }
                    if (URL.indexOf("COSinglePageCheckout") !== -1) {
                        frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.checkout");
                        frmCartView.flxFooterWrap.isVisible = false;
                        isCheckOutPlace = true;
                    }
                    if (URL.indexOf("Cart-MoveBackToCart") !== -1 || URL.indexOf("backToCart") !== -1 || URL.indexOf("default/Cart-MobileAppAddToWishlist?pid") !== -1) {
                        // MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                        if (URL.indexOf("default/Cart-MobileAppAddToWishlist?pid") !== -1) {
                            frmCartView.btnBack.isVisible = true;
                            frmCartView.lblTitle.isVisible = true;
                            frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.cart");
                            frmCartView.flxFooterWrap.isVisible = false;
                            frmCartView.lblChevron.isVisible = true;
                            frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnBack");
                            isCheckOutPlace = false;
                            frmCartView.btnBack.left = "23%";
                            frmCartView.flxCart.isVisible = true;
                            frmCartView.btnCart.isVisible = true;
                        }
                        if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                            MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                        }
                        // return true;
                    }
                }
            }
        };
        frmCartView.btnCart.onClick = function() {
            var cartQuantity = MVCApp.Toolbox.common.getNumberOfItemsInCart() || "0";
            TealiumManager.trackEvent("Cart Icon Click", {
                adobe_app_event: "Cart Icon",
                app_cart_quantity: cartQuantity
            });
            MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
        };
        frmCartView.brwrView.onPageFinished = function(eventobject, params) {
            var URL = params.originalURL || "";
            kony.print("In handle Request---onPageFinished-------->" + URL);
            MVCApp.Toolbox.common.dismissLoadingIndicator("");
            if (MVCApp.Toolbox.common.checkIfGuestUser()) {
                doGetCountForGuest();
            }
        };
        frmCartView.brwrView.onProgressChanged = function(eventobject, params) {
            if (frmCartView.brwrView && frmCartView.brwrView.canGoBack()) MVCApp.Toolbox.common.dismissLoadingIndicator("");
        };
        frmCartView.brwrView.onFailure = function(eventObj, error) {
            kony.print(" in error --> frmCartView.brwrView.onFailure" + error);
            if (error !== undefined && error !== null) {
                if (error.errorCode !== undefined && error.errorCode !== null) {
                    var errorResponseCode = error.errorCode || "";
                    if (errorResponseCode == 1018 || errorResponseCode == 1010 || errorResponseCode == 1002) {
                        frmCartView.brwrView.reload();
                    }
                }
            }
        };
        frmCartView.brwrView.handleRequest = function(browserWidget, params) {
            var URL = params.originalURL || "";
            kony.print("In handle Request only----------->" + URL);
            MVCApp.Toolbox.common.dismissLoadingIndicator("");
            if (URL !== "") {
                URL = MVCApp.Toolbox.common.formatURLBeforeLoading(URL, MVCApp.Toolbox.common.findIfWebviewUrlisExternal(URL));
                kony.print("formatURLBeforeLoading---------->" + URL);
                if (MVCApp.Toolbox.common.findIfWebviewUrlisExternal(URL)) {
                    MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18.common.webview.redirectMessage"), "", constants.ALERT_TYPE_CONFIRMATION, function(response) {
                        if (response) {
                            kony.application.openURL(URL);
                        } else {
                            kony.print("do nothing");
                        }
                    }, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel"));
                    return true;
                } else if (MVCApp.Toolbox.common.findIfWebviewUrlisDeeplink(URL)) {
                    fromBrowserViews = true;
                    entryBrowserFormName = "frmCartView";
                    kony.application.openURL(URL);
                    return true;
                }
                if (URL == "about:blank" && MVCApp.Toolbox.common.checkIfGuestUser()) {
                    flag = true;
                    return false;
                }
                if (URL.indexOf("cart?dwcont") !== -1 || URL.indexOf("cart?stid") !== -1) {
                    flag = true;
                    return false;
                }
                if (URL == "appservices://com.michaels.ConnectedRetail?formToOpen=frmCartHome") {
                    if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                        MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    } else {
                        doGetCountForGuest();
                    }
                    doGetStoreIdInfo();
                    kony.print("In handle Request only------cartSubmit----->" + URL);
                    frmCartView.btnBack.isVisible = true;
                    frmCartView.lblTitle.isVisible = true;
                    frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.cart");
                    frmCartView.flxFooterWrap.isVisible = true;
                    frmCartView.lblChevron.isVisible = true;
                    frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnBack");
                    isCheckOutPlace = false;
                    frmCartView.btnBack.left = "23%";
                    frmCartView.flxCart.isVisible = true;
                    frmCartView.btnCart.isVisible = true;
                    frmCartView.forceLayout();
                    flag = true;
                    return true;
                }
                if (URL.indexOf("frmSignIn") !== -1) {
                    if (URL.indexOf("action=favorites") !== -1) {
                        MVCApp.getSignInController().setButtonTypeForCheckOutButton(true);
                    }
                    doGetSessionBasketForGuest(URL);
                    return true;
                }
                if (URL.indexOf("frmHome") !== -1) {
                    MVCApp.getHomeController().load();
                    return true;
                }
                if (URL.indexOf("frmAddtoCart") !== -1) {
                    // MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                        MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                    } else {
                        doGetCountForGuest();
                    }
                    return true;
                }
                if (URL == "appservices://com.michaels.ConnectedRetail?formToOpen=frmCartView&action=checkout") {
                    isCheckout = true;
                    frmCartView.flxCart.isVisible = false;
                    frmCartView.flxFooterWrap.isVisible = false;
                    frmCartView.lblChevron.isVisible = false;
                    frmCartView.btnBack.isVisible = true;
                    frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel");
                    isCheckOutPlace = true;
                    frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.checkout");
                    frmCartView.btnBack.left = "5%";
                    frmCartView.forceLayout();
                    return true;
                }
                if (URL.indexOf("appservices://com.michaels.ConnectedRetail?formToOpen=frmCoupons") !== -1) {
                    voiceSearchFlag = false;
                    couponsEntryFromVoiceSearch = false;
                    var storeModeEnabled = false;
                    if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                        storeModeEnabled = true;
                    }
                    var region = MVCApp.Toolbox.common.getRegion();
                    var reportData = {
                        "storeModeEnabled": storeModeEnabled,
                        "region": region
                    };
                    MVCApp.sendMetricReport(frmHome, [{
                        "CouponsCLick": JSON.stringify(reportData)
                    }]);
                    MVCApp.getHomeController().loadCoupons();
                    return true;
                }
                if (URL == "appservices://com.michaels.ConnectedRetail?formToOpen=frmCartWeb&status=success") {
                    frmCartView.flxCart.isVisible = false;
                    frmCartView.flxFooterWrap.isVisible = false;
                    frmCartView.lblChevron.isVisible = false;
                    frmCartView.btnBack.left = "5%";
                    frmCartView.btnBack.isVisible = false;
                    isCheckOutPlace = true;
                    frmCartView.lblTitle.isVisible = true;
                    frmCartView.lblTitle.text = "Thank You";
                    frmCartView.btnHeaderLeft.isVisible = false;
                    frmCartView.forceLayout();
                    flag = false;
                    if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                        MVCApp.Toolbox.common.createBasketForSignedInUser();
                    } else {
                        doGetCountForGuest();
                    }
                    return true;
                }
                if (URL.indexOf(".html?appsource=mobileapp") !== -1 || URL.indexOf(".html?productsource=CartYMAL") !== -1) {
                    //var storeId= MVCApp.Toolbox.common.getStoreID();
                    //var appRequest= "?stid="+storeId+"&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                    cartFlag = true;
                    MVCApp.getProductsListWebController().loadWebView(URL.split("?")[0]);
                    return true;
                }
                if (URL.indexOf("formToOpen=frmPDP&action=storechange") !== -1) {
                    frmCartView.brwrView.enableJsInterface = true;
                    var result = frmCartView.brwrView.evaluateJavaScriptAsync("getStoreIdForApp()", function(result, error) {
                        kony.print(">>Result:" + JSON.stringify(result) + "<<");
                        var store = result || "";
                        store = store.replace(/"/g, "");
                        store = store.replace(/\\/g, "");
                        store = store.replace("/", "");
                        store = store.trim();
                        kony.print(">>Result:" + store + "<<");
                        if (store.length > 0) {
                            var reqParams = {};
                            if (gblInStoreModeDetails.storeID !== store) {
                                storeChangeInStoreMode = true;
                            }
                            reqParams.storeid = store;
                            reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
                            MakeServiceCall(MVCApp.serviceType.DemandwareService, MVCApp.serviceConstants.getStore, reqParams, function(results) {
                                if (results.opstatus === 0 || results.opstatus === "0") {
                                    var poi = results.poi ? results.poi : "";
                                    if (poi) {
                                        var poiJson = JSON.parse(poi);
                                        poiJson.clientkey = store;
                                        kony.store.setItem("myLocationDetails", JSON.stringify(poiJson));
                                        MVCApp.Toolbox.Service.setMyLocationFlag(true);
                                    }
                                    //kony.print("Poi data" + JSON.parse(poi));
                                }
                            }, function(error) {
                                kony.print("Response is " + JSON.stringify(error));
                            });
                        }
                    });
                    return true;
                }
                if (URL.indexOf("LoyaltyRegistration-ViewProfile?l=t") !== -1 || URL.indexOf("Account?l=t ") !== -1) {
                    var result = frmCartView.brwrView.evaluateJavaScriptAsync("getLoyaltyIDForApp()", function(result, error) {
                        MVCApp.Toolbox.common.setLMRID();
                    });
                }
            }
            return false;
        };
    }

    function doGetCountForGuest() {
        var res = frmCartView.brwrView.evaluateJavaScriptAsync("getBasketCountForApp()", function(result, error) {
            try {
                kony.print("getBasketCountForApp" + result);
                var count = result || "";
                count = count.replace(/"/g, "");
                count = count.replace(/\\/g, "");
                count = count.replace("/", "");
                count = count.trim();
                if (count !== "null" && count !== "") {
                    MVCApp.Toolbox.common.setGuestBasketCount(parseInt(count));
                    MVCApp.Toolbox.common.getGuestBasketDetails();
                }
            } catch (err) {
                kony.print(err)
            }
        });
    }

    function doGetSessionBasketForGuest(URL) {
        var result = frmCartView.brwrView.evaluateJavaScript("getSessionBasketDeprecated()");
        frmCartView.brwrView.evaluateJavaScriptAsync("$('#sessionBasketJSON').html()", function(result, error) {
            try {
                kony.print("getSessionBasket::" + result);
                var obj = JSON.parse(result || {});
                kony.print("getSessionBasket" + obj.basket_id);
                kony.print("getSessionBasket" + obj.customer_id);
                kony.print("getSessionBasket" + obj.product_count);
                if (obj.product_count == undefined || obj.product_count == "") {
                    // MVCApp.Toolbox.common.setGuestBasketCount(0);
                } else {
                    MVCApp.Toolbox.common.setBasket(obj.basket_id);
                    MVCApp.Toolbox.common.setCustomerId(obj.customer_id);
                    MVCApp.Toolbox.common.setJWTToken(obj.access_token);
                    MVCApp.Toolbox.common.setGuestBasketCount(obj.product_count);
                }
                fromCartSignIn = true;
                fromCoupons = false;
                isFromShoppingList = false;
                gblUrl = URL;
                fromForm = "frmCartView";
                MVCApp.getSignInController().load();
            } catch (err) {
                kony.print("error in navigation------>" + err)
            }
        });
    }

    function doGetStoreIdInfo() {
        var result = frmCartView.brwrView.evaluateJavaScriptAsync("getStoreIdForApp()", function(result, error) {
            kony.print(">>Result:" + JSON.stringify(result) + "<<");
            var store = result || "";
            store = store.replace(/"/g, "");
            store = store.replace(/\\/g, "");
            store = store.replace("/", "");
            store = store.trim();
            kony.print(">>Result:" + store + "<<");
            if (store.length > 0) {
                var reqParams = {};
                var inStoreModeStore = MVCApp.Toolbox.common.getStoreID();
                var favStore = MVCApp.Toolbox.common.getFavouriteStore();
                if (favStore === inStoreModeStore) {
                    inStoreModeStore = favStore;
                }
                if (inStoreModeStore !== "" && inStoreModeStore !== store) {
                    storeChangeInStoreMode = true;
                }
                if (favStore !== store) {
                    reqParams.storeID = store;
                    // reqParams.client_id = MVCApp.Service.Constants.Common.clientId;
                    reqParams.country = MVCApp.Service.Constants.Common.country;
                    MakeServiceCall(MVCApp.serviceType.LocatorService, MVCApp.serviceType.fetchINStoreDetailsByID, reqParams, function(results) {
                        if (results.opstatus === 0 || results.opstatus === "0") {
                            var collection = results.collection || [];
                            var poiColl = collection[0] || "";
                            var poi = poiColl.poi || "";
                            var poiString = "";
                            if (poi !== "") {
                                poiString = JSON.stringify(poi);
                                kony.store.setItem("myLocationDetails", poiString);
                            }
                            //kony.print("Poi data" + JSON.parse(poi));
                        }
                    }, function(error) {
                        kony.print("Response is " + JSON.stringify(error));
                    });
                }
            }
        });
    }

    function postShowCallback() {
        var cartValue = MVCApp.Toolbox.common.getCartItemValue();
        frmCartView.lblItemsCount.text = cartValue;
        MVCApp.Toolbox.common.startTimeToRefreshToken();
        MVCApp.Toolbox.common.destroyStoreLoc();
        var lPreviousForm = kony.application.getPreviousForm();
        var lPrevFormName = "";
        if (lPreviousForm) {
            lPrevFormName = lPreviousForm.id;
        }
    }
    var headerconfig = {
        "Authorization": "Basic c3RvcmVmcm9udDptaWsxMjM="
    };

    function show() {
        //ifdef android
        MVCApp.Toolbox.common.showLoadingIndicator("");
        //endif
        frmCartView.show();
    }

    function loadPdpView(data, flag) {
        if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
            MVCApp.Toolbox.common.showLoadingIndicator();
            MVCApp.Toolbox.common.verifyExpiryFlowOfAuthUser(function() {
                showPdpView(data, flag);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            })
        } else showPdpView(data, flag);
    }

    function showPdpView(data, flag) {
        var uriRequest = MVCApp.Toolbox.common.getSessionURI();
        var jwtToken = MVCApp.Toolbox.common.getJWTToken();
        var request = new kony.net.HttpRequest();
        kony.print(" Data in cart view" + data);
        try {
            frmCartView.flxMainContainer.remove(frmCartView.brwrView);
        } catch (e) {
            kony.print("widget hasnt been created at least once");
        }
        isCartPageFromMyAccount = data.indexOf("Account") > -1;
        var brwrView = new kony.ui.Browser({
            "detectTelNumber": true,
            "enableZoom": false,
            "id": "brwrView",
            "isVisible": true,
            "left": "0%",
            "top": "50dp",
            "width": "100%",
            "height": "87%",
            "htmlString": "&nbsp;",
            "zIndex": 1,
            "info": "sdfds",
        }, {}, {
            "baseURL": " ",
            "browserType": constants.BROWSER_TYPE_WKWEBVIEW,
            "bounces": true,
            "shareCookiesInBrowsers": true
        });
        frmCartView.flxMainContainer.add(brwrView);
        bindEvents();
        gblUrl = "";
        favoritesFlag = false;
        frmCartView.brwrView.enableJsInterface = true;
        var storeId = MVCApp.Toolbox.common.getStoreID();
        var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
        var strCookie = "";
        strCookie = "appsource=mobileapp;";
        if (favStoreId !== "") {
            // appRequest= "?stid="+favStoreId+"&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
            //headerconfig["set-cookie"]="storeId="+favStoreId+"";
            strCookie = strCookie + "storeId=" + favStoreId + ";";
            // headerconfig["set-cookie"]="storeId="+favStoreId+"";
        } else {
            // headerconfig["set-cookie"]="storeId="+storeId+"";
            strCookie = strCookie + "storeId=" + storeId + ";";
            // strCookie="storeId="+favStoreId+";";
        }
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            // headerconfig["set-cookie"]="storeVisit="+storeId+"";
            strCookie = strCookie + "storeVisit=" + storeId + ";";
        }
        // headerconfig["Cookie"] = strCookie;
        headerconfig["set-cookie"] = strCookie;
        isCheckOutPlace = false;
        frmCartView.btnHeaderLeft.isVisible = true;
        if (data.indexOf("COSinglePageCheckout") !== -1) {
            frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.checkout");
            data = data + "&action=checkout";
            frmCartView.flxFooterWrap.isVisible = false;
            frmCartView.lblChevron.isVisible = false;
            frmCartView.btnBack.left = "5%";
            frmCartView.flxCart.isVisible = false;
            frmCartView.btnCart.isVisible = false;
            //frmCartView.lblItemsCount.isVisible=false;
            frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.smallCancel");
            isCheckOutPlace = true;
        } else {
            if (data.indexOf("Account") !== -1 || data.indexOf("Wishlist") !== -1) {
                frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnMyAccount");
                frmCartView.flxCart.isVisible = false;
                frmCartView.btnCart.isVisible = false;
            } else {
                frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.cart");
                frmCartView.flxCart.isVisible = true;
                frmCartView.btnCart.isVisible = true;
                frmCartView.btnBack.isVisible = true;
                frmCartView.flxFooterWrap.isVisible = true;
                frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnBack");
                //           frmCartView.forceLayout();
            }
            frmCartView.flxFooterWrap.isVisible = true;
            frmCartView.lblChevron.isVisible = true;
            frmCartView.btnBack.left = "23%";
            //       frmCartView.btnCart.isVisible=true;
            //      frmCartView.lblItemsCount.isVisible=true;
            frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnBack");
            isCheckOutPlace = false;
        }
        var isSession = flag || false;
        frmCartView.brwrView.evaluateJavaScript("document.open();document.close();");
        var lPreviousForm = kony.application.getCurrentForm() || "";
        kony.print("kony.application.getCurrentForm()" + lPreviousForm.id);
        var lPreviousForm_1 = kony.application.getPreviousForm() || "";
        var browserContent = {
            "URL": data,
            "requestMethod": constants.BROWSER_REQUEST_METHOD_GET,
            "headers": headerconfig
        };
        frmCartView.brwrView.requestURLConfig = browserContent;
        //frmCartView.forceLayout();
        frmCartView.brwrView.addGestureRecognizer(4, {
            fingers: 1,
            continuousEvents: true
        }, function(commonWidget, gestureInfo) {
            var GesType = "" + gestureInfo.gestureType;
            if (GesType == "4") {
                var state = gestureInfo.gestureState;
                if (state == 1) checkonTouchStart(frmCartView.brwrView, gestureInfo.gestureX, gestureInfo.gestureY);
                if (state == 3) checkonTouchEnd(frmCartView.brwrView, gestureInfo.gestureX, gestureInfo.gestureY);
            }
        });
        show();
    }

    function goPrevious() {
        frmCartView.brwrView.evaluateJavaScript("window.stop();");
        var prevForm = kony.application.getPreviousForm() || "";
        var prevFormId = prevForm.id || "";
        var prevFormForBrowser = "";
        if (fromBrowserViews) {
            if (entryFormObjectToBrowser) {
                prevFormId = entryFormObjectToBrowser.id || (prevForm.id || "");
            } else {
                prevFormId = prevForm.id || "";
            }
        }
        if (favoritesFlag) {
            if (prevFormId == "frmWebView") {
                MVCApp.getProductsListWebController().loadWebView(pdpUrl.split("?")[0]);
            } else if (prevFormId == "frmSignIn" && fromForm == "frmWebView") {
                MVCApp.getProductsListWebController().loadWebView(pdpUrl.split("?")[0]);
            } else {
                MVCApp.getMoreController().load();
            }
            fromForm = "";
            favoritesFlag = false;
        } else if (isCheckOutPlace) {
            var url = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "cart";
            var storeId = MVCApp.Toolbox.common.getStoreID();
            var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
            var appRequest = "?stid=" + storeId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                if (storeChangeInStoreMode) {
                    appRequest = "?stid=" + favStoreId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
                }
                appRequest = appRequest + "&storevisit=" + storeId;
            }
            if (url.indexOf("?") !== -1) {
                appRequest = appRequest.replace(/\?/g, "&");
            }
            url = url + appRequest;
            data = decodeURI(url);
            data = data.replace(/%2C/g, ",");
            data = encodeURI(url);
            var browserContent = {
                "URL": url,
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET,
                "headers": headerconfig
            };
            frmCartView.brwrView.requestURLConfig = browserContent;
            frmCartView.lblTitle.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.cart");
            frmCartView.flxCart.isVisible = true;
            frmCartView.btnCart.isVisible = true;
            frmCartView.btnBack.isVisible = true;
            frmCartView.btnBack.left = "23%";
            frmCartView.lblChevron.isVisible = true;
            frmCartView.flxFooterWrap.isVisible = true;
            frmCartView.btnBack.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnBack");
            show();
            isCheckOutPlace = false;
        } else {
            if (prevFormId == "frmShoppingList") {
                MVCApp.getShoppingListController().getProductListItems();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            } else if (prevFormId == "frmSignIn") {
                MVCApp.getMoreController().load();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            } else if (prevFormId == "frmWebView") {
                MVCApp.getProductsListWebController().loadWebView(pdpUrl.split("?")[0]);
            } else if (prevFormId == "frmCreateAccount") {
                MVCApp.getMoreController().load();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            } else if (prevFormId == "frmRewardsJoin" || prevFormId === "frmRewardsProgram" || prevFormId === "frmRewardsProgramInfo") {
                MVCApp.getMoreController().load();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            } else if (prevFormId == "frmCoupons") {
                MVCApp.getHomeController().loadCoupons();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            } else if (prevFormId == "frmHome") {
                MVCApp.getHomeController().showDefaultView();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            } else if (fromBrowserViews) {
                if (entryFormObjectToBrowser) {
                    entryFormObjectToBrowser.show();
                } else {
                    kony.print("prevFormId  " + prevFormId);
                    kony.application.getPreviousForm().show();
                    MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
                }
                fromBrowserViews = false;
            } else {
                kony.application.getPreviousForm().show();
                MVCApp.Toolbox.common.destoryFrmWeb("frmCartView");
            }
        }
    }
    //  }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
        } else {
            MVCApp.Toolbox.common.dispFooter();
        }
        if (checkScroll === 0) {
            MVCApp.Toolbox.common.hideFooter();
        }
    }
    return {
        bindEvents: bindEvents,
        loadPdpView: loadPdpView
    };
});