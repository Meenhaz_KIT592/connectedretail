/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.PDP = (function(json) {
    var typeOf = "Products";
    var _data = json || {};
    _data = _data.products || [];
    var _csets = json || {};
    if (_data.length > 0) {
        _data = _data[0] || {};
    }
    /**
     * PUBLIC - setter for the model
     * Set data from the JSON retrieved by the model
     */
    function getProductPromotions() {
        return _data.product_promotions ? _data.product_promotions : "";
    }

    function getPrices() {
        return _data.prices ? _data.prices : "";
    }

    function getCsets() {
        return _csets.c_sets ? _csets.c_sets : "";
    }

    function getCoStoreInventory() {
        return _data.c_store_inventory ? _data.c_store_inventory : "";
    }

    function getTitle() {
        return _data.name ? _data.name : "";
    }

    function getStepQuantity() {
        return _data.stepQuantity ? _data.stepQuantity : "";
    }

    function getLongDescription() {
        return _data.longDescription ? _data.longDescription : "";
    }

    function getMinimumOrderQuantity() {
        return _data.minOrder ? _data.minOrder : "";
    }

    function getStockLevel() {
        var inventory = _data.inventory || {};
        return inventory.stockLevel ? inventory.stockLevel : "";
    }

    function getSKUId() {
        return _data.sku ? _data.sku : "";
    }

    function getReviewsCount() {
        return _data.reviewCount ? _data.reviewCount : 0;
    }

    function getRating() {
        return _data.avgRating ? _data.avgRating : 0;
    }

    function getShopOnlineURL() {
        return _data.shopOnlineLine ? _data.shopOnlineLine : "";
    }

    function getCAProp64Message() {
        var propFlag = _data.prop65Flag ? _data.prop65Flag : "";
        var tempData = {};
        if (propFlag == "true") {
            tempData.caFlag = true;
            tempData.caMessage = _data.prop65FlagMessage || "";
            return tempData;
        } else {
            tempData.caFlag = false;
            tempData.message = "";
            return tempData;
        }
    }

    function getFlammableMessage() {
        var tempData = {};
        var propFlag = _data.flammableFlag ? _data.flammableFlag : "";
        if (propFlag == "true") {
            tempData.flammableFlag = true;
            var flamMessage = _data.flammableMessage || "";
            tempData.flammableMessage = flamMessage;
            return tempData;
        } else {
            tempData.flammableFlag = false;
            tempData.flammableMessage = "";
            return tempData;
        }
    }

    function getMasterId() {
        var master = _data.master || "";
        var masterObj = "";
        if (master != "" && master != "null" && master != null) {
            try {
                masterObj = JSON.parse(master);
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "dataPDP.js on getMasterId for master:" + JSON.stringify(e)
                }]);
                if (e) {
                    TealiumManager.trackEvent("Master Parse Exception in PDP Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
        return masterObj.master_id ? masterObj.master_id : "";
    }

    function getProductId() {
        return _data.id ? _data.id : "";
    }

    function getBrand() {
        return _data.brand ? _data.brand : "";
    }

    function getPrimaryCategoryId() {
        return _data.primary_category_id ? _data.primary_category_id : "Categories";
    }

    function getDefaultColor() {
        return _data.c_color ? _data.c_color : "";
    }

    function getDefaultSize() {
        return _data.c_size ? _data.c_size : "";
    }

    function getDefaultCount() {
        return _data.c_count ? _data.c_count : "";
    }

    function getImages() {
        var smallImagesArray = [];
        var mediumImagesArray = [];
        var largeImagesArray = [];
        var zoomImagesArray = [];
        var imageGroups = _data.image_groups || [];
        for (var i = 0; i < imageGroups.length; i++) {
            if ((imageGroups[i].view_type || "") == "small") {
                smallImagesArray.push(imageGroups[i].images);
            }
            if ((imageGroups[i].view_type || "") == "medium") {
                mediumImagesArray.push(imageGroups[i].images);
            }
            if ((imageGroups[i].view_type || "") == "large") {
                largeImagesArray.push(imageGroups[i].images);
            }
            if ((imageGroups[i].view_type || "") == "zoom") {
                zoomImagesArray.push(imageGroups[i].images);
            }
        }
        var imagesArray = {};
        imagesArray.smallImages = smallImagesArray;
        imagesArray.mediumImages = mediumImagesArray;
        imagesArray.largeImages = largeImagesArray;
        imagesArray.zoomImages = zoomImagesArray;
        return imagesArray;
    }

    function setImages(imagesGroup) {
        _data.image_groups = imagesGroup;
    }

    function getVariationAttributes() {
        return _data.variationAttributes ? _data.variationAttributes : "";
    }

    function getPrice() {
        var lRegPrice = "";
        var price = _data.price ? _data.price : "";
        if (_data && _data.price && _data.c_list_price_min && (parseFloat(_data.price) != parseFloat(_data.c_list_price_min))) {
            lRegPrice = _data.c_list_price_min;
        }
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            var lCStoreInventoryObj = getCoStoreInventory();
            if (lCStoreInventoryObj) {
                var lPrice = "";
                var lPricesArr = [];
                var isRetailPrice = false;
                try {
                    lCStoreInventoryObj = lCStoreInventoryObj + "";
                    lCStoreInventoryObj = JSON.parse(lCStoreInventoryObj);
                } catch (pError) {
                    kony.print("@@@LOG:getPrice-inside catch condition-pError:" + JSON.stringify(pError));
                    if (pError) {
                        TealiumManager.trackEvent("Store Inventory Parse Exception in PDP Flow", {
                            exception_name: pError.name,
                            exception_reason: pError.message,
                            exception_trace: pError.stack,
                            exception_type: pError
                        });
                    }
                }
                if (lCStoreInventoryObj.retail_price) {
                    isRetailPrice = true;
                    lPricesArr.push({
                        priceType: "retailPrice",
                        price: parseFloat(lCStoreInventoryObj.retail_price)
                    });
                }
                if (lCStoreInventoryObj.clearance_price) {
                    lPricesArr.push({
                        priceType: "clearancePrice",
                        price: parseFloat(lCStoreInventoryObj.clearance_price)
                    });
                }
                if (lCStoreInventoryObj.promo_price) {
                    if (lCStoreInventoryObj.PromotionalStart && lCStoreInventoryObj.PromotionalEnd) {
                        var lIsCurDateWithInPromoDatesRange = checkIfIsCurDateWithInPromoDatesRange(lCStoreInventoryObj.PromotionalStart, lCStoreInventoryObj.PromotionalEnd);
                        if (lIsCurDateWithInPromoDatesRange) {
                            lPricesArr.push({
                                priceType: "promoPrice",
                                price: parseFloat(lCStoreInventoryObj.promo_price)
                            });
                        }
                    }
                }
                if (lPricesArr.length == 1) {
                    lPrice = lPricesArr[0].price;
                } else if (lPricesArr.length > 1) {
                    lPricesArr.sort(function(a, b) {
                        return a.price - b.price;
                    });
                    lPrice = lPricesArr[0].price;
                    if (isRetailPrice && lPrice != parseFloat(lCStoreInventoryObj.retail_price)) {
                        lRegPrice = lCStoreInventoryObj.retail_price;
                    }
                }
                if (lPrice) {
                    price = lPrice;
                    if (lPricesArr[0].priceType === "clearancePrice") {
                        MVCApp.getPDPController().setIsClearancePricePresentFlag(true);
                    } else if (lPricesArr[0].priceType === "promoPrice") {
                        MVCApp.getPDPController().setIsPromoPricePresentFlag(true);
                    }
                }
            }
        }
        return {
            "price": MVCApp.Toolbox.common.showPriceDecimals(price),
            "regPrice": MVCApp.Toolbox.common.showPriceDecimals(lRegPrice)
        };
    }

    function getInventory() {
        return _data.inventory ? _data.inventory : "";
    }

    function getOrderbaleStatus() {
        var inventory = _data.inventory || {};
        return inventory.oderable ? inventory.oderable : "";
    }

    function getCtype() {
        return _data.c_type ? _data.c_type : "";
    }

    function getCAltProductId() {
        return _data.c_alt_product_id ? _data.c_alt_product_id : "";
    }

    function getVariants() {
        return _data.variants ? _data.variants : "";
    }

    function getSmallMagnets() {
        return _data.smallMagnets ? _data.smallMagnets : "";
    }

    function getSharpPoint() {
        return _data.sharpPoint ? _data.sharpPoint : "";
    }

    function getHazardType() {
        return _data.c_hazardType ? _data.c_hazardType : "";
    }
    //Here we expose the public variables and functions
    return {
        typeOf: typeOf,
        getTitle: getTitle,
        getSmallMagnets: getSmallMagnets,
        getSharpPoint: getSharpPoint,
        getHazardType: getHazardType,
        getStepQuantity: getStepQuantity,
        getLongDescription: getLongDescription,
        getMinimumOrderQuantity: getMinimumOrderQuantity,
        getStockLevel: getStockLevel,
        getSKUId: getSKUId,
        getReviewsCount: getReviewsCount,
        getRating: getRating,
        getCAProp64Message: getCAProp64Message,
        getImages: getImages,
        getMasterId: getMasterId,
        getProductId: getProductId,
        getBrand: getBrand,
        getVariants: getVariants,
        getPrice: getPrice,
        getOrderbaleStatus: getOrderbaleStatus,
        getCtype: getCtype,
        getVariationAttributes: getVariationAttributes,
        setImages: setImages,
        getCoStoreInventory: getCoStoreInventory,
        getCsets: getCsets,
        getPrices: getPrices,
        getProductPromotions: getProductPromotions,
        getDefaultColor: getDefaultColor,
        getDefaultSize: getDefaultSize,
        getInventory: getInventory,
        getFlammableMessage: getFlammableMessage,
        getShopOnlineURL: getShopOnlineURL,
        getDefaultCount: getDefaultCount,
        getPrimaryCategoryId: getPrimaryCategoryId,
        getCAltProductId: getCAltProductId
    };
});