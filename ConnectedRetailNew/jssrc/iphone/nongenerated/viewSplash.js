/**
 * PUBLIC
 * This is the view for the Splash Details form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.SplashView = (function() {
    function show() {
        var deviceHeight = kony.os.deviceInfo().deviceHeight;
        if (deviceHeight == 1334) {
            frmSplash.skin = "sknSplashiphone6";
        } else if (deviceHeight >= 2208) {
            frmSplash.skin = "sknSplash6plus";
        }
        kony.print("the info is " + JSON.stringify(deviceHeight));
        var selectedLang = kony.store.getItem("languageSelected") || "";
        var selectedRegion = "";
        var country = "";
        var devLang = kony.i18n.getCurrentDeviceLocale();
        //var devLanguage=kony.i18n.getCurrentLocale();
        //alert(devLang);
        if (selectedLang == "") {
            var languageAndSupport = MVCApp.Toolbox.common.isLocaleSupportedInApp(devLang);
            var language = languageAndSupport.language || "";
            var support = languageAndSupport.support || false;
            if (support) {
                kony.store.setItem("languageSelected", language);
            } else {
                kony.store.setItem("languageSelected", "en");
            }
        }
        var langSetNew = kony.store.getItem("languageSelected");
        if (langSetNew == "en" || langSetNew == "en_US") {
            selectedRegion = "US";
        } else {
            selectedRegion = "CA";
        }
        kony.store.setItem("Region", selectedRegion);
        if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
            loadi18nValues();
            versionCheck();
        }
        MVCApp.Toolbox.Service.setServiceType();
        kony.i18n.setCurrentLocaleAsync(kony.store.getItem("languageSelected"), function() {
            kony.print("The default locale has been set on app init to " + selectedLang);
        }, function() {
            kony.print("Some error occured in default app language setting");
        });
        frmSplash.show();
        //loadStartUpContent();
    }

    function loadStartUpContent() {
        MVCApp.getSplashController().loadStatupContent();
    }

    function updateScreen(results) {
        MVCApp.getHomeController().load();
    }

    function loadi18nValues() {
        var reqParams = {};
        var maxTimeStamp = kony.store.getItem("maxTimeStamp");
        if (maxTimeStamp == null || maxTimeStamp == undefined || maxTimeStamp == "") {
            reqParams.inputTimeStamp = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.maxTimeStamp");
        } else {
            reqParams.inputTimeStamp = maxTimeStamp;
        }
        MakeServiceCall(MVCApp.serviceType.internalizationorchservice, MVCApp.serviceEndPoints.i18orcheservice, reqParams, function(results) {
            if (results.opstatus === 0 || results.opstatus === "0") {
                var values = results.internalization || [];
                var tempEn = {};
                var tempEnCA = {};
                var tempFRCA = {};
                for (var i = 0; i < values.length; i++) {
                    var keyVal = values[i].key;
                    tempEn[keyVal] = "" + values[i].en;
                    tempEnCA[keyVal] = "" + values[i].en_CA;
                    tempFRCA[keyVal] = "" + values[i].fr_CA;
                }
                setLocaleData(tempEn, "en");
                setLocaleData(tempEnCA, "en_CA");
                setLocaleData(tempFRCA, "fr_CA");
                try {
                    if (results.maxTimeStamp != null && results.maxTimeStamp != undefined && results.maxTimeStamp != "") {
                        kony.store.setItem("maxTimeStamp", results.maxTimeStamp);
                    }
                } catch (e) {
                    kony.print("Exception is  " + JSON.stringify(e));
                    if (e) {
                        TealiumManager.trackEvent("Exception While Loading i18n Values", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
                loadStartUpContent();
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
        });
    }

    function setLocaleData(data, locale) {
        try {
            kony.i18n.updateResourceBundle(data, locale);
        } catch (i18nError) {
            kony.print("Exception While updating ResourceBundle  : " + i18nError);
            if (i18nError) {
                TealiumManager.trackEvent("Exception While Updating Resource Bundle", {
                    exception_name: i18nError.name,
                    exception_reason: i18nError.message,
                    exception_trace: i18nError.stack,
                    exception_type: i18nError
                });
            }
        }
    }

    function setCurrentLocaleAsync(data, loc, maxTimeStamp) {
        try {
            setLocaleData(data, loc);
            kony.i18n.setCurrentLocaleAsync(loc, function() {
                onsuccessLocale(maxTimeStamp);
            }, onfailurecallback);
        } catch (err) {
            kony.print("the value of err.message is:" + err);
            if (err) {
                TealiumManager.trackEvent("Exception While Setting Current Locale Asynchronously", {
                    exception_name: err.name,
                    exception_reason: err.message,
                    exception_trace: err.stack,
                    exception_type: err
                });
            }
        }
    }

    function onsuccessLocale(maxTimeStamp) {
        try {
            kony.print("locale changed to ");
            kony.store.setItem("maxTimeStamp", maxTimeStamp);
        } catch (err) {
            kony.print("the value of err.message is:" + err);
            if (err) {
                TealiumManager.trackEvent("Exception in Success Callback While Setting the Current Locale", {
                    exception_name: err.name,
                    exception_reason: err.message,
                    exception_trace: err.stack,
                    exception_type: err
                });
            }
        }
    }

    function onfailurecallback() {
        try {
            kony.print("locale changed to en/ar");
        } catch (err) {
            kony.print("the value of err.message is:" + err);
            if (err) {
                TealiumManager.trackEvent("Exception in Failure Callback While Setting the Current Locale", {
                    exception_name: err.name,
                    exception_reason: err.message,
                    exception_trace: err.stack,
                    exception_type: err
                });
            }
        }
    }

    function versionCheck() {
        var requestParams = MVCApp.versionCheck.getRequestParams();
        MakeServiceCall("versionverifyorcservice", "versionverifyorcheservice", requestParams, function(results) {
            if (results.opstatus === 0 || results.opstatus === "0") {
                var upgradeStatus = MVCApp.versionCheck.succesCallBack(results);
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
        });
    }

    function bindEvents() {
        frmSplash.postShow = function() {
            if (!kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
                MVCApp.getHomeController().showDefaultView();
            }
        };
        frmSplash.onDeviceBack = function() {};
    }
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});