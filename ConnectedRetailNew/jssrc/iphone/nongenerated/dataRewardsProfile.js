var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.RewardsProfile = (function(json) {
    var rewardProfileData = {};
    var cRewardsProfile = {};
    var _data = json.results || [];
    var fetchResult = json.results || [];
    var getAddresData = json.addressResult || "";
    if (getAddresData) {
        try {
            getAddresData = JSON.parse(getAddresData) || {};
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "dataRewardsProfile.js on line 22 for getAddresData:" + JSON.stringify(e)
            }]);
            getAddresData = {};
            if (e) {
                TealiumManager.trackEvent("Get Address Data Parse Exception in Rewards Profile Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    if (json) {
        try {
            rewardProfileData = JSON.parse(fetchResult) || {};
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "dataRewardsProfile.js on line 30 for fetchResult:" + JSON.stringify(e)
            }]);
            rewardProfileData = {};
            if (e) {
                TealiumManager.trackEvent("Fetch Result Parse Exception in Rewards Profile Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    if (rewardProfileData) {
        cRewardsProfile = rewardProfileData.c_RewardsProfile || {};
    }
    var updatedRewardsProfileData = {};
    var addressDetails1 = getAddressDetails();
    var phoneNo = rewardProfileData.c_loyaltyPhoneNo || "";
    phoneNo = phoneNo.replace(/-/gi, "");
    updatedRewardsProfileData.c_interests = rewardProfileData.c_interests || "";
    updatedRewardsProfileData.c_loyaltyBirthday = rewardProfileData.c_loyaltyBirthday || "";
    updatedRewardsProfileData.c_childrendata = rewardProfileData.c_childrendata || "";
    updatedRewardsProfileData.c_surveyQA = rewardProfileData.c_surveyQA || "";
    updatedRewardsProfileData.c_loyaltyEmail = rewardProfileData.c_loyaltyEmail || "";
    rewardsProfileData.c_loyaltyAddressUUID = rewardProfileData.c_loyaltyAddressUUID || "";
    updatedRewardsProfileData.c_loyaltyPhoneNo = phoneNo || "";
    updatedRewardsProfileData.first_name = rewardProfileData.first_name || "";
    updatedRewardsProfileData.last_name = rewardProfileData.last_name || "";
    // this should get from getAddress service
    if (getData()) {
        updatedRewardsProfileData.c_postalCode = addressDetails1.postal_code || "";
    } else {
        updatedRewardsProfileData.c_postalCode = rewardProfileData.c_postalCode || "";
    }

    function onChildTextChange() {
        var data = frmRewardsProfileAddChild.segChildAgeLevel.data;
        var childFlag = false;
        for (var i = 0; i < data.length; i++) {
            var originalData = [];
            var formId = kony.application.getPreviousForm().id;
            if (data[i].txtChild.text != undefined && data[i].txtChild.text != "" && formId != "frmReviewProfile") {
                frmRewardsProfileAddChild.btnNextToRewardsInterests.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
                childFlag = true;
            } else if (formId != "frmReviewProfile") {
                if (childFlag == false) frmRewardsProfileAddChild.btnNextToRewardsInterests.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip");
            }
        }
    }

    function getRewardsProfileChildData() {
        try {
            var rewardsProfile = _data;
            if (rewardsProfile != "" && rewardsProfile != null && rewardsProfile != undefined) {
                rewardsProfile = JSON.parse(rewardsProfile);
                kony.print("Rewards Profile Child:" + rewardsProfile);
                rewardsProfile = rewardsProfile.c_RewardsProfile;
                var childNames = [];
                if (rewardsProfile != "" && rewardsProfile != null) {
                    var rewardsChild = rewardsProfile.children;
                    if (rewardsChild != "" && rewardsChild != null) {
                        var questions = [];
                        questions = rewardsChild.questions;
                        var childData = updatedRewardsProfileData.c_childrendata || "";
                        if (childData !== "") {
                            childData = childData.split(",");
                            for (var i = 0; i < questions.length; i++) {
                                var temp1 = [];
                                temp1.value = questions[i].value;
                                temp1.lblChild = questions[i].displayValue;
                                var count = 0;
                                for (var k = 0; k < childData.length; k++) {
                                    if (childData[k] == questions[i].value) {
                                        count++;
                                    }
                                }
                                if (count !== 0) {
                                    temp1.txtChild = {
                                        text: count,
                                        onTextChange: onChildTextChange
                                    };
                                } else {
                                    temp1.txtChild = {
                                        onTextChange: onChildTextChange
                                    };
                                }
                                childNames.push(temp1);
                            }
                        } else {
                            if (rewardsProfile != "" && rewardsProfile != null) {
                                var rewardsChild = rewardsProfile.children;
                                if (rewardsChild != "" && rewardsChild != null) {
                                    var questions = [];
                                    questions = rewardsChild.questions;
                                    for (var i = 0; i < questions.length; i++) {
                                        var temp1 = [];
                                        temp1.value = questions[i].value;
                                        temp1.lblChild = questions[i].displayValue;
                                        temp1.txtChild = {
                                            onTextChange: onChildTextChange
                                        };
                                        childNames.push(temp1);
                                    }
                                }
                            }
                        }
                    }
                }
                return childNames;
            }
        } catch (e) {
            kony.print("Exception in getRewardsProfileChildData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Rewards Profile Child Data Exception in Rewards Profile Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getRewardsProfileInterestsData() {
        try {
            var rewardsProfile = _data;
            if (rewardsProfile != "" && rewardsProfile != null && rewardsProfile != undefined) {
                rewardsProfile = JSON.parse(rewardsProfile);
                kony.print("Rewards Profile Interests:" + rewardsProfile);
                rewardsProfile = rewardsProfile.c_RewardsProfile;
                var interestss = [];
                if (rewardsProfile != "" && rewardsProfile != null) {
                    var rewardsInterests = rewardsProfile.skills;
                    if (rewardsInterests != "" && rewardsInterests != null) {
                        var questions = [];
                        var validvalues = [];
                        questions = rewardsInterests.questions;
                        if (questions != "" && questions != null) {
                            interestss.questions = questions;
                            validvalues = rewardsInterests.validvalues;
                            var splitIntrst = updatedRewardsProfileData.c_surveyQA || "";
                            interestss.c_surveyQA = splitIntrst;
                            if (splitIntrst !== "" && splitIntrst !== null && splitIntrst.length > 0) {
                                splitIntrst = splitIntrst[0].split(":");
                                for (var i = 0; i < validvalues.length; i++) {
                                    var temp4 = [];
                                    temp4[0] = validvalues[i];
                                    temp4[1] = validvalues[i];
                                    if (splitIntrst[1] == validvalues[i]) interestss.interests = validvalues[i];
                                    interestss.push(temp4);
                                }
                            } else {
                                for (var j = 0; j < validvalues.length; j++) {
                                    var temp3 = [];
                                    temp3[0] = validvalues[j];
                                    temp3[1] = validvalues[j];
                                    interestss.push(temp3);
                                }
                            }
                        }
                    }
                }
                return interestss;
            }
        } catch (e) {
            kony.print("Exception in getRewardsProfileInterestsData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Rewards Profile Interests Data Exception in Rewards Profile Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function resetProfileIntrestSkillData() {
        var interest = {};
        var questions = [];
        var validvalues = [];
        var segData = [];
        if (cRewardsProfile) {
            interest = cRewardsProfile.interest || {};
            questions = interest.questions || {};
            validvalues = interest.validvalues || {};
        }
        if (questions && questions.length > 0) {
            for (var c = 0; c < questions.length; c++) {
                var temp = [];
                temp.skillName = questions[c];
                temp.flexSliderSelector = {
                    left: "0%",
                    isVisible: true
                };
                temp.flexSliderSelector2 = {
                    isVisible: false
                };
                temp.skillMapValue = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.NA");
                temp.btn1 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 0,
                        name: questions[c]
                    }
                };
                temp.btn2 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 1,
                        name: questions[c]
                    }
                };
                temp.btn3 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 2,
                        name: questions[c]
                    }
                };
                temp.btn4 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 3,
                        name: questions[c]
                    }
                };
                temp.btn5 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 4,
                        name: questions[c]
                    }
                };
                temp.btn6 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 5,
                        name: questions[c]
                    }
                };
                temp.levelValue = 0;
                segData.push(temp);
            }
        }
        return segData;
    }

    function getProfileIntrests() {
        var interest = {};
        var questions = [];
        var validvalues = [];
        var segData = [];
        if (cRewardsProfile) {
            interest = cRewardsProfile.interest || {};
            questions = interest.questions || {};
            validvalues = interest.validvalues || {};
        }
        if (updatedRewardsProfileData.c_interests !== "") {
            var interests = updatedRewardsProfileData.c_interests || [];
            var skillIntrestData = [];
            if (interests.length > 1) {
                skillIntrestData = updatedRewardsProfileData.c_interests;
            } else {
                skillIntrestData = updatedRewardsProfileData.c_interests[0].split(",");
            }
            if (questions && questions.length > 0) {
                for (var c = 0; c < questions.length; c++) {
                    var temp = [];
                    var value = "";
                    for (var j = 0; j < skillIntrestData.length; j++) {
                        var skillName = skillIntrestData[j];
                        if (skillName.indexOf(questions[c]) != -1) {
                            var tempSkill = skillName; //JSON.parse(skillName);
                            value = parseInt(tempSkill.split(":")[1]);
                            break;
                        }
                    }
                    if (value !== "") {
                        var skillLevelData = getLevelName(value);
                        var flexLeft = skillLevelData.flexLeft || "";
                        if (flexLeft !== "") {
                            temp.flexSliderSelector = {
                                left: flexLeft,
                                isVisible: true
                            };
                            temp.sliderSelectorInner = {
                                right: "",
                                left: "0%"
                            };
                            temp.flexSliderSelector2 = {
                                isVisible: false
                            };
                        } else {
                            temp.flexSliderSelector = {
                                isVisible: false
                            };
                            temp.flexSliderSelector2 = {
                                isVisible: true
                            };
                        }
                        temp.skillMapValue = skillLevelData.levelName;
                        temp.levelValue = value;
                    } else {
                        temp.flexSliderSelector = {
                            left: "0%",
                            isVisible: true
                        };
                        temp.flexSliderSelector2 = {
                            isVisible: false
                        };
                        temp.skillMapValue = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.NA");
                        temp.levelValue = 0;
                    }
                    temp.skillName = questions[c];
                    temp.btn1 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 0,
                            name: questions[c]
                        }
                    };
                    temp.btn2 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 1,
                            name: questions[c]
                        }
                    };
                    temp.btn3 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 2,
                            name: questions[c]
                        }
                    };
                    temp.btn4 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 3,
                            name: questions[c]
                        }
                    };
                    temp.btn5 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 4,
                            name: questions[c]
                        }
                    };
                    temp.btn6 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 5,
                            name: questions[c]
                        }
                    };
                    segData.push(temp);
                }
            }
            btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
        } else {
            if (questions && questions.length > 0) {
                for (var c = 0; c < questions.length; c++) {
                    var temp = [];
                    temp.skillName = questions[c];
                    temp.flexSliderSelector = {
                        left: "0%",
                        isVisible: true
                    };
                    temp.flexSliderSelector2 = {
                        isVisible: false
                    };
                    temp.skillMapValue = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.NA");
                    temp.btn1 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 0,
                            name: questions[c]
                        }
                    };
                    temp.btn2 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 1,
                            name: questions[c]
                        }
                    };
                    temp.btn3 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 2,
                            name: questions[c]
                        }
                    };
                    temp.btn4 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 3,
                            name: questions[c]
                        }
                    };
                    temp.btn5 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 4,
                            name: questions[c]
                        }
                    };
                    temp.btn6 = {
                        onClick: onSelectionSkill,
                        info: {
                            level: 5,
                            name: questions[c]
                        }
                    };
                    temp.levelValue = 0;
                    segData.push(temp);
                }
            }
            btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip");
        }
        frmRewardsProfileSkills.btnNext.text = btnText;
        return segData;
    }

    function onSelectionSkill(eventObj) {
        var selectedIntrst = frmRewardsProfileSkills.lstInterests.selectedKey;
        var formName = kony.application.getPreviousForm().id;
        if (eventObj !== undefined || selectedIntrst !== "select") {
            if (eventObj !== undefined) {
                var selectedIndex = frmRewardsProfileSkills.segSkills.selectedIndex[1];
                var info = eventObj.info || {};
                var i = info.level;
                var name = info.name;
                var skillLevelData = getLevelName(i);
                //   temp.skillName = {text:questions[c]}; 
                var flexLeft = skillLevelData.flexLeft || "";
                var temp = [];
                if (flexLeft !== "") {
                    temp.flexSliderSelector = {
                        left: flexLeft,
                        isVisible: true
                    };
                    temp.sliderSelectorInner = {
                        right: "",
                        left: "0%"
                    };
                    temp.flexSliderSelector2 = {
                        isVisible: false
                    };
                } else {
                    temp.flexSliderSelector = {
                        isVisible: false
                    };
                    temp.flexSliderSelector2 = {
                        isVisible: true
                    };
                }
                temp.skillMapValue = skillLevelData.levelName;
                temp.btn1 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 0,
                        name: name
                    }
                };
                temp.btn2 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 1,
                        name: name
                    }
                };
                temp.btn3 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 2,
                        name: name
                    }
                };
                temp.btn4 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 3,
                        name: name
                    }
                };
                temp.btn5 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 4,
                        name: name
                    }
                };
                temp.btn6 = {
                    onClick: onSelectionSkill,
                    info: {
                        level: 5,
                        name: name
                    }
                };
                temp.levelValue = i;
                temp.skillName = name;
                frmRewardsProfileSkills.segSkills.setDataAt(temp, selectedIndex, 0);
                var segData = frmRewardsProfileSkills.segSkills.data || [];
                var formId = kony.application.getPreviousForm().id;
                if (formId == "frmReviewProfile") {
                    btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
                } else {
                    btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip");
                }
                for (i = 0; i < segData.length; i++) {
                    var levelValue = segData[i].levelValue;
                    if (levelValue !== 0) {
                        if (formId == "frmReviewProfile") {
                            btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
                        } else {
                            btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
                        }
                        break;
                    }
                }
                frmRewardsProfileSkills.btnNext.text = btnText;
            }
            if (selectedIntrst !== "select") {
                var formIds = kony.application.getPreviousForm().id;
                if (formIds === "frmReviewProfile") btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
                else btnText = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
                frmRewardsProfileSkills.btnNext.text = btnText;
            }
        } else if (formName !== "frmReviewProfile") {
            var segDataa = frmRewardsProfileSkills.segSkills.data || [];
            var btnFlag = false;
            for (var n = 0; n < segDataa.length; n++) {
                var value = segDataa[n].levelValue;
                if (value !== 0) {
                    btnFlag = true;
                    break;
                }
            }
            if (btnFlag === false) frmRewardsProfileSkills.btnNext.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip");
        }
    }

    function getLevelName(level) {
        var levelData = {};
        if (level === 0) {
            levelData.levelName = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.NA");
            levelData.flexLeft = "0%";
        } else if (level === 1) {
            levelData.levelName = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.Beginner");
            levelData.flexLeft = "20%";
        } else if (level === 2) {
            levelData.levelName = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.Novice");
            levelData.flexLeft = "40%";
        } else if (level === 3) {
            levelData.levelName = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.Intermediate");
            levelData.flexLeft = "60%";
        } else if (level === 4) {
            levelData.levelName = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.Advanced");
            levelData.flexLeft = "80%";
        } else if (level === 5) {
            levelData.levelName = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.Expert");
        }
        return levelData;
    }

    function getAddressDetails() {
        rewardsProfileData.isAddressExists = false;
        var addressDetails = {};
        if (null !== getAddresData && undefined !== getAddresData && "" !== getAddresData) {
            //var addressData = getAddresData.data ||[];
            //  if(addressData.length >0 )
            //    {
            //             for(i=0;i<addressData.length;i++)
            //               {
            // var temp=addressData[i];
            var address_id = getAddresData.address_id;
            if (address_id == rewardProfileData.c_loyaltyAddressUUID) {
                rewardsProfileData.isAddressExists = true;
                addressDetails.address1 = getAddresData.address1;
                addressDetails.address2 = getAddresData.address2;
                addressDetails.city = getAddresData.city;
                addressDetails.postal_code = getAddresData.postal_code;
                addressDetails.state_code = getAddresData.state_code;
                addressDetails.first_name = getAddresData.first_name;
                addressDetails.last_name = getAddresData.last_name;
                addressDetails.phone = getAddresData.phone;
            }
            //   }
            //  }
            return addressDetails;
        }
    }

    function getStep1Data() {
        frmRewardsProfileStep1.txtFstName.text = updatedRewardsProfileData.first_name;
        frmRewardsProfileStep1.txtLstName.text = updatedRewardsProfileData.last_name;
        frmRewardsProfileStep1.txtEmlAddrss.text = updatedRewardsProfileData.c_loyaltyEmail;
        //      frmRewardsProfileStep1.txtPhone.text = updatedRewardsProfileData .c_loyaltyPhoneNo;    
        var phoneNo = updatedRewardsProfileData.c_loyaltyPhoneNo;
        phoneNo = phoneNo.replace(/[-' '().]/g, "");
        frmRewardsProfileStep1.txtPhone.text = phoneNo;
        frmRewardsProfileStep1.txtDateOfBirth.text = updatedRewardsProfileData.c_loyaltyBirthday;
        if (updatedRewardsProfileData.c_loyaltyBirthday !== "") {} else {}
    }

    function getStep2Data() {
        frmRewardsProfileStep2.txtZip.text = updatedRewardsProfileData.c_postalCode;
        if (MVCApp.Toolbox.common.getRegion() == "US") {
            frmRewardsProfileStep2.txtZip.text = updatedRewardsProfileData.c_postalCode;
        } else {
            frmRewardsProfileStep2.txtZipCan.text = updatedRewardsProfileData.c_postalCode;
        }
        if (updatedRewardsProfileData.c_postalCode !== "") {} else {}
    }

    function getData() {
        var addressDetails = getAddressDetails();
        if (undefined !== addressDetails && addressDetails.address1 != undefined && addressDetails.address1 != "") return true;
        else return false;
    }

    function sumary() {
        //Personal data
        frmReviewProfile.lblName.text = updatedRewardsProfileData.first_name + " " + updatedRewardsProfileData.last_name;
        frmReviewProfile.lblEmail.text = updatedRewardsProfileData.c_loyaltyEmail;
        frmReviewProfile.lblDOB.text = updatedRewardsProfileData.c_loyaltyBirthday;
        frmReviewProfile.lblPhone.text = updatedRewardsProfileData.c_loyaltyPhoneNo;
        rewardsProfileData.firstName = updatedRewardsProfileData.first_name;
        rewardsProfileData.lastName = updatedRewardsProfileData.last_name;
        rewardsProfileData.email = updatedRewardsProfileData.c_loyaltyEmail;
        rewardsProfileData.phoneNo = updatedRewardsProfileData.c_loyaltyPhoneNo;
        rewardsProfileData.birthday = updatedRewardsProfileData.c_loyaltyBirthday;
        //Adress data
        var addressDetails = getAddressDetails();
        frmReviewProfile.lblAddress1.text = addressDetails.address1;
        var addr2 = addressDetails.address2 || "";
        if (addr2 == "" || addr2 == null || addr2 == "null") {
            frmReviewProfile.lblAddress2.setVisibility(false);
        } else {
            frmReviewProfile.lblAddress2.text = addressDetails.address2;
            frmReviewProfile.lblAddress2.setVisibility(true);
        }
        frmReviewProfile.lblCity.text = addressDetails.city + ", " + addressDetails.state_code;
        frmReviewProfile.lblStateZip.text = addressDetails.postal_code;
        rewardsProfileData.address1 = addressDetails.address1;
        rewardsProfileData.address2 = addressDetails.address2 || "";
        rewardsProfileData.city = addressDetails.city;
        rewardsProfileData.zip = addressDetails.postal_code;
        rewardsProfileData.state = addressDetails.state_code;
        //Child Data    
        var childDat = getRewardsProfileChildData();
        var childData = [];
        var childarr = childDat;
        var childFlag = false;
        var childStr;
        for (var i = 0; i < childarr.length; i++) {
            var temp = [];
            if (childarr[i].txtChild.text != undefined && childarr[i].txtChild.text != "") {
                childFlag = true;
                temp.lblReviewProfile = childarr[i].lblChild;
                childStr = childarr[i].txtChild.text + "";
                temp.lblReviewProfileNo = childStr;
                childData.push(temp);
            }
        }
        frmReviewProfile.segData.data = childData;
        var data = childDat;
        rewardsProfileData.childData = data;
        rewardsProfileData.child = [];
        rewardsProfileData.c_childrendata = "";
        var txtflag = false;
        for (var k = 0; k < data.length; k++) {
            var originalData = [];
            if (data[k].txtChild != undefined) {
                originalData.txtChild = data[k].txtChild;
                originalData.lblChild = data[k].lblChild;
                originalData.value = data[k].value;
                var flagFor = originalData.txtChild.text;
                flagFor = parseInt(flagFor);
                for (var j = 1; j <= flagFor; j++) {
                    if (j == 1 && txtflag == false) {
                        rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata + data[k].value;
                        txtflag = true;
                    } else if (txtflag == true) rewardsProfileData.c_childrendata = rewardsProfileData.c_childrendata + "," + data[k].value;
                }
                rewardsProfileData.child.push(originalData);
            }
        }
        skillData = MVCApp.getRewardsProfileController().getRewardProfileData(rewardsProfileData);
        var data1 = skillData.getRewardsProfileInterestsData();
        var segData = skillData.getProfileIntrests();
        //var skills = rewardsProfileData.displaySkillData;
        var skillFlag = false;
        var skillData = [];
        for (var l = 0; l < segData.length; l++) {
            var temp1 = [];
            if (segData[l].skillMapValue != undefined && segData[l].skillMapValue != "" && segData[l].skillMapValue != "N/A") {
                skillFlag = true;
                temp1.lblReviewProfile = segData[l].skillName;
                temp1.lblReviewProfileNo = segData[l].skillMapValue;
                skillData.push(temp1);
            }
        }
        frmReviewProfile.segSkills.data = skillData;
        var segData1 = getProfileIntrests();
        rewardsProfileData.c_interests = [];
        rewardsProfileData.displaySkillData = [];
        if (frmRewardsProfileSkills.btnNext.text !== MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnSkip")) {
            rewardsProfileData.segSelectedSkillData = segData1;
            var intrestString = "";
            for (i = 0; i < segData1.length; i++) {
                var temp2 = [];
                var levelValue = segData1[i].levelValue;
                var skillName = segData1[i].skillName;
                var skillMapValue = segData1[i].skillMapValue;
                var intrestValue = "\"" + skillName + ":" + levelValue + "\"";
                var intrestValue1 = skillName + ":" + levelValue;
                if (levelValue !== 0) {
                    temp2.name = skillName;
                    temp2.skillMapValue = skillMapValue;
                    if (intrestString === "") {
                        intrestString = intrestValue;
                    } else {
                        intrestString = intrestString + "," + intrestValue;
                    }
                    rewardsProfileData.displaySkillData.push(temp2);
                    rewardsProfileData.c_interests.push(intrestValue1);
                }
            }
        }
        //Interests Data
        rewardsProfileData.interestData = data1;
        rewardsProfileData.interests = data1.interests;
        rewardsProfileData.c_surveyQA = data1.c_surveyQA;
        var selectDefault = [
            ["select", "SELECT AN ANSWER"]
        ];
        var interestsData = selectDefault.concat(rewardsProfileData.interestData);
        frmRewardsProfileSkills.lstInterests.masterData = interestsData;
        if (rewardsProfileData.c_surveyQA != "defaults" && rewardsProfileData.c_surveyQA != undefined && rewardsProfileData.c_surveyQA.length !== 0) {
            frmReviewProfile.flxQuestions.setVisibility(true);
            var intrst = rewardsProfileData.c_surveyQA;
            var intrstArray = intrst[0].split(":");
            frmReviewProfile.lblQuestion.text = intrstArray[0];
            frmReviewProfile.lblAnswer.text = intrstArray[1];
        } else {
            frmReviewProfile.flxQuestions.setVisibility(false);
        }
        //Preffered Store
        var favStore = rewardsFavouriteStore;
        if (favStore != "" && favStore != null && favStore != undefined) {
            try {
                var favStoreObj = JSON.parse(favStore);
                if (favStoreObj != "") {
                    var storeData = new MVCApp.data.dataRewardsPreferedStore();
                    var favData = storeData.getFavouriteStoreData();
                    if (favData.length > 0) {
                        favData = favData[0];
                    }
                    frmReviewProfile.CopyLabel055a9a5cadcf54f.text = favData.lbladdress1 || "";
                    frmReviewProfile.CopyLabel015c44ef24d2d48.text = favData.lbladdress2 || "";
                    frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(true);
                } else {
                    frmReviewProfile.CopyLabel055a9a5cadcf54f.text = "";
                    frmReviewProfile.CopyLabel015c44ef24d2d48.text = "";
                    frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
                }
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "dataRewardsProfile.js on sumary for favStore:" + JSON.stringify(e)
                }]);
                frmReviewProfile.CopyLabel055a9a5cadcf54f.text = "";
                frmReviewProfile.CopyLabel015c44ef24d2d48.text = "";
                frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
                if (e) {
                    TealiumManager.trackEvent("Favourite Store Parse Exception in Rewards Profile Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            frmReviewProfile.CopyLabel055a9a5cadcf54f.text = "";
            frmReviewProfile.CopyLabel015c44ef24d2d48.text = "";
            frmReviewProfile.CopyFlexContainer0cec9ffcf8af54e.setVisibility(false);
        }
    }
    return {
        getProfileIntrests: getProfileIntrests,
        onSelectionSkill: onSelectionSkill,
        onChildTextChange: onChildTextChange,
        getRewardsProfileChildData: getRewardsProfileChildData,
        getRewardsProfileInterestsData: getRewardsProfileInterestsData,
        getAddressDetails: getAddressDetails,
        getStep1Data: getStep1Data,
        resetProfileIntrestSkillData: resetProfileIntrestSkillData,
        sumary: sumary,
        getData: getData,
        getStep2Data: getStep2Data
    };
});