/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.Projects = (function(json) {
    var typeOf = "Projects";
    var _data = json || {};
    /**
     * PUBLIC - setter for the model
     * Set data from the JSON retrieved by the model
     */
    function getProjectsListData() {
        try {
            var projectsList = _data.prjcategories || [];
            projectsList = MVCApp.Toolbox.common.isArray(projectsList) ? projectsList : [projectsList];
            var displayProjectsList = [];
            for (var i = 0; i < projectsList.length; i++) {
                var displayFlag = MVCApp.Toolbox.common.showInMenu(projectsList[i].c_hideOnMobile, projectsList[i].c_showInMenu, projectsList[i].c_showInMobileApp);
                if (displayFlag) {
                    projectsList[i].lblCategoryCount = {
                        isVisible: true,
                        text: "K"
                    };
                    projectsList[i].lblAisle = {
                        isVisible: false
                    };
                    projectsList[i].lblNumber = {
                        isVisible: false
                    };
                    projectsList[i].btnAisle = {
                        isVisible: false
                    };
                    displayProjectsList.push(projectsList[i]);
                }
            }
            return displayProjectsList;
        } catch (e) {
            kony.print("Exception in getProjectsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Projects List Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getSubProjectsListData() {
        try {
            var projectsList = _data.categories || [];
            projectsList = MVCApp.Toolbox.common.isArray(projectsList) ? projectsList : [projectsList];
            var displayProjectsList = [];
            for (var i = 0; i < projectsList.length; i++) {
                var displayFlag = MVCApp.Toolbox.common.showInMenu(projectsList[i].c_hideOnMobile, projectsList[i].c_showInMenu, projectsList[i].c_showInMobileApp);
                if (displayFlag) {
                    var prodName = projectsList[i].name || "";
                    projectsList[i].fullName = prodName;
                    var c_CategoryUrl = projectsList[i].c_CategoryUrl || "";
                    projectsList[i].c_CategoryUrl = c_CategoryUrl;
                    prodName = (prodName.length > 26 ? prodName.substring(0, 26) + "..." : prodName) || "";
                    projectsList[i].name = prodName;
                    projectsList[i].lblCategoryCount = {
                        isVisible: true,
                        text: "K"
                    };
                    projectsList[i].lblAisle = {
                        isVisible: false
                    };
                    projectsList[i].lblNumber = {
                        isVisible: false
                    };
                    projectsList[i].btnAisle = {
                        isVisible: false
                    };
                    displayProjectsList.push(projectsList[i]);
                }
            }
            return displayProjectsList;
        } catch (e) {
            kony.print("Exception in getSubProjectsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Sub Projects List Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    //Here we expose the public variables and functions
    return {
        typeOf: typeOf,
        getProjectsListData: getProjectsListData,
        getSubProjectsListData: getSubProjectsListData
    };
});