/**
 * PUBLIC
 * This is the model for formProductsList form
 */
var MVCApp = MVCApp || {};
MVCApp.ProductsListWebModel = (function() {
    function loadData(callback, url) {
        kony.print("BrowserModel.js");
        // kony.net.clearCookies(url);
        callback(url);
    }

    function load(searchStr, callback) {
        var storeId = MVCApp.Toolbox.common.getStoreID();
        var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
        var appRequest = "&stid=" + storeId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
            if (storeChangeInStoreMode) {
                appRequest = "&stid=" + favStoreId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
            }
            appRequest = appRequest + "&storevisit=" + storeId;
        }
        var url = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels");
        // kony.net.clearCookies(url);
        url = url + searchStr + appRequest;
        callback(url);
    }
    return {
        load: load,
        loadData: loadData
    };
});