/**
 * PUBLIC
 * This is the controller for the Products form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.ProductsController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.ProductsModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.ProductsView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load() {
        kony.print("Products Controller.load");
        init();
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.id = MVCApp.serviceConstants.defaultProductId;
        var storeNo = "";
        var storeId = "";
        var storeString = kony.store.getItem("myLocationDetails") || "";
        if (storeString !== "") {
            try {
                var storeObj = JSON.parse(storeString);
                storeId = storeObj.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "ctrlProducts.js on load for storeString:" + JSON.stringify(e)
                }]);
                storeNo = "";
                storeId = "";
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Loading The Shop Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeNo = "";
            storeId = "";
        }
        gblStoreKeyForProducts = storeId;
        gblTealiumTagObj.storeid = storeId;
        requestParams.storeNo = storeId;
        model.getProducts(view.updateScreen, requestParams);
        view.show();
    }

    function setGlblProductsResult(value) {
        gblProducts = value;
    }

    function getGlblProductsResult(value) {
        return gblProducts;
    }

    function getGlblProducts() {
        init();
        var requestParams = MVCApp.Service.getCommonInputParamaters();
        requestParams.id = MVCApp.serviceConstants.defaultProductId;
        requestParams["$select"] = "*";
        var storeNo = "";
        var storeString = kony.store.getItem("myLocationDetails") || "";
        if (storeString !== "") {
            try {
                var storeObj = JSON.parse(storeString);
                storeNo = "and store_id eq " + storeObj.clientkey;
            } catch (e) {
                MVCApp.sendMetricReport(frmHome, [{
                    "Parse_Error_DW": "ctrlProducts.js on getGlblProducts for storeString:" + JSON.stringify(e)
                }]);
                storeNo = "";
                if (e) {
                    TealiumManager.trackEvent("POI Parse Exception While Getting The Products For The Shop Screen", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        } else {
            storeNo = "";
        }
        requestParams["$filter"] = "status eq '" + "A'" + storeNo;
        model.getProducts(setGlblProductsResult, requestParams);
    }

    function displayPage() {
        init();
        view.displaySamePage();
    }

    function displayScreen() {
        init();
        view.displaySameScreen();
    }

    function updateScreen() {
        init();
        view.show();
        if (null != gblproducts && undefined != gblproducts) {
            view.updateScreen(gblproducts);
        } else {
            load();
        }
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        displayPage: displayPage,
        getGlblProducts: getGlblProducts,
        updateScreen: updateScreen,
        setGlblProductsResult: setGlblProductsResult,
        displayScreen: displayScreen
    };
});