/**
 * PUBLIC
 * This is the model for JoinRewards form
 */
var MVCApp = MVCApp || {};
MVCApp.ResetPasswordModel = (function() {
    var serviceType = "";
    var operationId = "";

    function submitEmail(callback, requestParams) {
        kony.print("ResetPasswordModel.js");
        var country = MVCApp.Toolbox.common.getRegion();
        if (country === "CA") {
            var serviceType = MVCApp.serviceType.NewPasswordResetCanada;
            var operationId = MVCApp.serviceEndPoints.CustomerSearchCanada;
        } else {
            var serviceType = MVCApp.serviceType.NewPasswordReset;
            var operationId = MVCApp.serviceEndPoints.CustomerSearch;
        }
        MakeServiceCall(serviceType, operationId, requestParams, function(results) {
            kony.print("Response is " + JSON.stringify(results));
            if (results.opstatus === 0 || results.opstatus === "0") {
                callback(results);
            }
        }, function(error) {
            kony.print("Response is " + JSON.stringify(error));
        });
        // callback("");
    }
    return {
        submitEmail: submitEmail
    };
});
//Type your code here