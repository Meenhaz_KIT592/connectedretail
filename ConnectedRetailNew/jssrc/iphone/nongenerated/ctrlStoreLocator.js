var gblCurrentLocatiionStoreLocator = 0;
/**
 * PUBLIC
 * This is the controller for the StoreMap form.
 * This is the go-to class for all interactions, this controller will call the 
 * relevant UI functions through the view and the backend/data-related functions through the model.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.StoreLocatorController = (function() {
    var _isInit = false;
    var model = null; //Backend interaction
    var view = null; //UI interaction
    /**
     * PUBLIC
     * Init all that the model and view need.
     * Usually such init methods would be private, but for this main form
     * we grant public access so that we can init the form before it is being loaded.
     * This enables us to predefine the theme and locale.
     */
    function init(theme, locale) {
        setPreviousLocation(null);
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.StoreLocatorModel();
        //Enables us to use different UI interactions depending on form factor
        view = new MVCApp.StoreLocatorView();
        //}
        //Bind events to UI
        view.bindEvents();
        _isInit = true;
    }
    var _isFromProducts = false;

    function isFromProducts() {
        return _isFromProducts;
    }
    var checkNearby = false;

    function getCheckNearby() {
        return checkNearby;
    }

    function setCheckNearby(reqParams) {
        checkNearby = reqParams;
    }
    var formName = "";

    function setFormName(name) {
        formName = name;
    }

    function getFormName() {
        return formName;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function loadStoresWithLatLon(lat, lon) {
        var inputParams = {};
        inputParams.latitude = lat;
        inputParams.longitude = lon;
        inputParams.country = MVCApp.Service.Constants.Common.country;
        inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
        setCheckNearby(false);
        _isFromProducts = false;
        model.loadData(inputParams, view.updateScreenForMap);
    }

    function loadCheckNearby(name) {
        var inputParams = {};
        init();
        setCentralLocation("");
        var latLon = MVCApp.getPDPController().getLatLon() || {};
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.POI) {
            latLon.lat = gblInStoreModeDetails.POI.latitude;
            latLon.lon = gblInStoreModeDetails.POI.longitude;
        }
        kony.print("*******************The lat lon objects ******************* " + JSON.stringify(latLon));
        var prodID = MVCApp.getPDPController().getProductIDForCheck();
        inputParams.latitude = latLon.lat || "";
        inputParams.longitude = latLon.lon || "";
        inputParams.skus = prodID;
        inputParams.country = MVCApp.Service.Constants.Common.country;
        inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
        setCheckNearby(true);
        setFormName(name);
        var lati = latLon.lat || "";
        var longi = latLon.lon || "";
        setLatLon(lati, longi);
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.storeid = getStoreIDForTracking();
        cust360Obj.latitude = lati;
        cust360Obj.longitude = longi;
        cust360Obj.productid = prodID;
        MVCApp.Customer360.sendInteractionEvent("CheckNearByStores", cust360Obj);
        _isFromProducts = false;
        TealiumManager.trackEvent("Check Nearby Stores Button Click From Product page", {
            conversion_category: "Check Nearby Stores",
            conversion_id: "Product Page",
            conversion_action: 2
        });
        if (lati === "" && longi === "") {
            view.showWithDefaultLoc("", "", true);
        } else {
            model.loadCheckNearbyData(inputParams, view.updateScreenForMap);
        }
    }

    function geoErrorcallback(positionerror) {
        if (voiceSearchFlag && tapToCancelFlag) {
            tapToCancelFlag = false;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            showMicPanel(voiceSearchForm);
            //return;
        } else {
            tapToCancelFlag = false;
            gblIsFromStoreLocatorFlow = false;
            kony.print("@@@ In geoErrorcallback() @@@");
            setLatLon("", "");
            if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
                getDataForFavourites(true);
            } else {
                view.showWithDefaultLoc("", "", true);
            }
        }
    }

    function getDataForFavourites(newEntry) {
        var newEntryFlag = newEntry || false;
        var locationDetailsStr = kony.store.getItem("myLocationDetails") || "";
        try {
            var locationDetails = JSON.parse(locationDetailsStr);
            var json = {};
            var calloutData = MVCApp.data.StoreLocator(json).prepareCallOutData(locationDetails, true, 0);
            var segData = MVCApp.data.StoreLocator(json).prepareSegmentData(locationDetails, 0, true);
            view.showWithDefaultLoc(calloutData, segData, newEntryFlag);
        } catch (e) {
            MVCApp.sendMetricReport(frmHome, [{
                "Parse_Error_DW": "ctrlStoreLocator.js on getDataForFavourites for locationDetailsStr:" + JSON.stringify(e)
            }]);
            if (e) {
                TealiumManager.trackEvent("POI Parse Exception in Store Locator Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    var srcObj = {};

    function setLatLon(lat, lon) {
        if (lat !== "" && lon !== "") {
            srcObj = {
                lat: lat,
                lon: lon
            };
        } else {
            srcObj = "";
        }
    }

    function getLatLon() {
        return srcObj;
    }
    var currentLocationForMap;

    function setCurrentLocationForMap(temp) {
        currentLocationForMap = temp;
    }

    function getCurrentLocationForMap() {
        return currentLocationForMap;
    }

    function geoSuccesscallback(position) {
        if (voiceSearchFlag && tapToCancelFlag) {
            tapToCancelFlag = false;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            showMicPanel(voiceSearchForm);
            //return;
        } else {
            tapToCancelFlag = false;
            gblIsFromStoreLocatorFlow = false;
            var currentLat = "";
            var currentLon = "";
            if (position.coords.latitude !== "" && position.coords.longitude !== "") {
                currentLat = position.coords.latitude;
                currentLon = position.coords.longitude;
                var locationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
                setLatLon(currentLat, currentLon);
                loadStoresWithLatLon(currentLat, currentLon);
            }
        }
    }

    function getCurrentLocation() {
        kony.print("LOG:getCurrentLocation-START");
        var positionoptions = {
            timeout: parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.timeoutValForStoreLocator")),
            enableHighAccuracy: true,
            useBestProvider: true
        };
        gblCurrentLocatiionStoreLocator = redirectToSettings.getLocationSettingsStatus();
        kony.location.getCurrentPosition(geoSuccesscallback, geoErrorcallback, positionoptions);
        kony.print("LOG:getCurrentLocation-END");
    }

    function updateStoreInfoFromMyStore() {
        setCentralLocation("");
        MVCApp.Toolbox.common.showLoadingIndicator("");
        getCurrentLocation();
        setCheckNearby(false);
    }
    var storeId = "";

    function getStoreIDForTracking() {
        return storeId;
    }

    function setStoreIDForTracking(value) {
        storeId = value;
    }

    function load(form) {
        gblIsFromStoreLocatorFlow = true;
        kony.print("LOG:load-gblIsFromStoreLocatorFlow:" + gblIsFromStoreLocatorFlow);
        init();
        setFormName(form);
        updateAndLoadStoreInfo(updateStoreInfoFromMyStore);
    }

    function loadfromChange(formName) {
        gblIsFromStoreLocatorFlow = true;
        kony.print("LOG:loadfromChange-gblIsFromStoreLocatorFlow:" + gblIsFromStoreLocatorFlow);
        init();
        setFormName(formName);
        updateAndLoadStoreInfo(updateStoreInfoFromMyStore);
        view.showCollapsedVersion();
    }

    function loadForHome(reqParams) {
        init();
        var nearest = model.loadNearest(reqParams, MVCApp.getHomeController().showNearest);
        setCheckNearby(false);
        setFormName("");
    }

    function initiateSearch(text) {
        //var text=eventobj.text;
        text = text.trim();
        if (text !== "") {
            var inputParams = {};
            inputParams.country = MVCApp.Service.Constants.Common.country;
            inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
            inputParams.addressline = text;
            model.loadDataFromSearch(inputParams, view.updateScreen);
        } else {
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.enterText"));
        }
    }

    function getSourceAddress() {
        return srcObj;
    }

    function updateFavouriteFromList() {
        view.updateMyStoreFlex();
    }

    function resetDataToMapAndSegment() {
        view.resetDataToMapAndSegment();
    }

    function removeFavorites() {
        view.removeFavorites();
    }

    function loadStoreInfo() {
        init();
        load("frmHome");
        view.showExpandedVersion();
    }
    var prodName = "";

    function getProductName() {
        return prodName;
    }

    function setProductName(value) {
        prodName = value;
    }

    function checkNearbyFromSearch(text) {
        text = text.trim();
        if (text !== "") {
            var prodID = MVCApp.getPDPController().getProductIDForCheck();
            var inputParams = {};
            inputParams.country = MVCApp.Service.Constants.Common.country;
            inputParams.searchradius = MVCApp.Service.Constants.Locations.searchRadius;
            inputParams.addressline = text;
            inputParams.skus = prodID;
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.searchKeyword = text;
            cust360Obj.productId = prodID;
            MVCApp.Customer360.sendInteractionEvent("SearchFromCheckNearby", cust360Obj);
            model.loadCheckNearbySearch(inputParams, view.updateScreen);
        } else {
            alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.enterText"));
        }
    }
    var prevLoc = null;

    function setPreviousLocation(value) {
        prevLoc = value;
    }

    function getPreviousLocation() {
        return prevLoc;
    }
    var latLonCentral = {
        "lat": "",
        "lon": ""
    };

    function getCentralLocation() {
        return latLonCentral;
    }

    function setCentralLocation(value) {
        var centralValue = value || "";
        var latCentral = "";
        var lonCentral = "";
        if (centralValue != "") {
            var valueArr = value.split(",");
            latCentral = valueArr[1];
            lonCentral = valueArr[0];
        }
        latLonCentral = {
            "lat": latCentral,
            "lon": lonCentral
        };
    }

    function getUpdatedStoreDetails(storeId, successCallback) {
        MVCApp.Toolbox.common.showLoadingIndicator("");
        var inputParams = {};
        inputParams.storeID = storeId;
        inputParams.country = MVCApp.Service.Constants.Common.country;
        model.fetchStoreDetails(successCallback, inputParams);
    }

    function updateAndLoadStoreInfo(successCallBack) {
        init();
        var myLocationString = kony.store.getItem("myLocationDetails");
        var myLocationObj = {};
        if (myLocationString && myLocationString != null && myLocationString != "" && myLocationString != undefined) {
            myLocationObj = JSON.parse(myLocationString);
            var now = new Date(new Date() - 1 * 3600 * 1000).getTime();
            var lastUpdatedTime = myLocationObj.timelastUpdated;
            if (lastUpdatedTime) {
                lastUpdatedTime = lastUpdatedTime;
            } else {
                lastUpdatedTime = "0";
            }
            var storeId = myLocationObj.clientkey || "";
            if (parseInt(now) > parseInt(lastUpdatedTime)) {
                if (storeId) {
                    getUpdatedStoreDetails(storeId, successCallBack);
                }
            } else {
                successCallBack();
            }
        } else {
            successCallBack();
            kony.print("logic to be written for no store selected");
        }
    }

    function showUI() {
        init();
        view.showUI();
    }
    var storeIdOriginal = "";

    function setOriginalStoreId(value) {
        storeIdOriginal = value;
    }

    function getOriginalStoreId() {
        return storeIdOriginal;
    }
    /**
     * Implement here all actions which you attach in view.bindEvents.
     * These actions should then call functions in the model for getting/saving data
     * and should then call functions in the view for updating the UI.
     */
    //Here we expose the public variables and functions
    return {
        init: init,
        load: load,
        loadForHome: loadForHome,
        isFromProducts: isFromProducts,
        getSourceAddress: getSourceAddress,
        initiateSearch: initiateSearch,
        updateFavouriteFromList: updateFavouriteFromList,
        resetDataToMapAndSegment: resetDataToMapAndSegment,
        getDataForFavourites: getDataForFavourites,
        removeFavorites: removeFavorites,
        setCurrentLocationForMap: setCurrentLocationForMap,
        getCurrentLocationForMap: getCurrentLocationForMap,
        loadStoreInfo: loadStoreInfo,
        loadfromChange: loadfromChange,
        loadCheckNearby: loadCheckNearby,
        loadStoresWithLatLon: loadStoresWithLatLon,
        getCheckNearby: getCheckNearby,
        checkNearbyFromSearch: checkNearbyFromSearch,
        getProductName: getProductName,
        setProductName: setProductName,
        setPreviousLocation: setPreviousLocation,
        getPreviousLocation: getPreviousLocation,
        setFormName: setFormName,
        getFormName: getFormName,
        setCentralLocation: setCentralLocation,
        getCentralLocation: getCentralLocation,
        getLatLon: getLatLon,
        geoErrorcallback: geoErrorcallback,
        updateAndLoadStoreInfo: updateAndLoadStoreInfo,
        setStoreIDForTracking: setStoreIDForTracking,
        getStoreIDForTracking: getStoreIDForTracking,
        showUI: showUI,
        setOriginalStoreId: setOriginalStoreId,
        getOriginalStoreId: getOriginalStoreId
    };
});