/**
 * PUBLIC
 * This is the data object for the addBiller form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.Products = (function(json) {
    var typeOf = "Products";
    var _data = json || {};
    /**
     * PUBLIC - setter for the model
     * Set data from the JSON retrieved by the model
     */
    function onClickAisle(eventObj) {
        var params = {};
        params.aisleNo = eventObj.info.aisleNum;
        params.productName = eventObj.info.name;
        params.calloutFlag = true;
        MVCApp.sendMetricReport(frmProducts, [{
            "storemap": "click"
        }]);
        MVCApp.getStoreMapController().load(params);
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        var storeId = MVCApp.Toolbox.common.getStoreID();
        cust360Obj.productid = params.productName;
        cust360Obj.store_id = storeId;
        cust360Obj.entry_type = "categories";
        MVCApp.Customer360.sendInteractionEvent("ProductLocationInStore", cust360Obj);
    }

    function getProductsListSubCategoryData() {
        try {
            var lMyLocationFlag = false;
            var storeBlockFlag = "";
            var storeObj = {};
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                if (gblInStoreModeDetails.POI) {
                    storeObj = gblInStoreModeDetails.POI;
                }
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
                if (storeString !== "") {
                    try {
                        storeObj = JSON.parse(storeString);
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "dataProducts.js on getProductsListSubCategryData for storeString:" + JSON.stringify(e)
                        }]);
                        if (e) {
                            TealiumManager.trackEvent("POI Parse Exception in Products List Sub Category Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                }
            }
            if (storeObj && storeObj.isblocked) {
                storeBlockFlag = storeObj.isblocked || "";
            }
            var productsList = _data.categories || [];
            var aisleNumbers = _data.SUB_CATEGORY_WAYFINDING || [];
            productsList = MVCApp.Toolbox.common.isArray(productsList) ? productsList : [productsList];
            var displayProductsList = [];
            for (var i = 0; i < productsList.length; i++) {
                var displayFlag = MVCApp.Toolbox.common.showInMenu(productsList[i].c_hideOnMobile, productsList[i].c_showInMenu, productsList[i].c_showInMobileApp);
                if (displayFlag) {
                    var tempData = productsList[i];
                    tempData.carrotFlag = "K";
                    tempData.btnAisle = {
                        isVisible: false,
                        text: ""
                    };
                    var prodName = productsList[i].name || "";
                    var c_CategoryUrl = productsList[i].c_CategoryUrl || "";
                    tempData.fullName = prodName;
                    tempData.c_CategoryUrl = c_CategoryUrl;
                    prodName = (prodName.length > 26 ? prodName.substring(0, 24) + "..." : prodName) || "";
                    tempData.name = prodName;
                    for (var j = 0; j < aisleNumbers.length; j++) {
                        var tempSubCategoryId = aisleNumbers[j].sub_category_id || "";
                        if (tempSubCategoryId == tempData.id) {
                            var templevel = aisleNumbers[j].level || "";
                            if (templevel !== "") {
                                if (lMyLocationFlag === "true" && storeBlockFlag != "true") {
                                    var aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(templevel);
                                    var aisleName = aisleInfo.name || "";
                                    if (aisleName !== "") {
                                        tempData.aisle = aisleInfo.name;
                                        if (tempData.aisle == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.wall")) tempData.aisle = {
                                            "centerY": "50%",
                                            "text": tempData.aisle
                                        };
                                        tempData.number = aisleInfo.number;
                                        tempData.btnAisle = {
                                            isVisible: true,
                                            "zIndex": 5,
                                            text: "",
                                            onClick: onClickAisle,
                                            info: {
                                                name: productsList[i].name,
                                                aisleNum: templevel
                                            }
                                        };
                                        tempData.carrotFlag = "";
                                    } else {
                                        tempData.aisle = "";
                                        tempData.number = "";
                                        tempData.btnAisle = {
                                            text: ""
                                        };
                                    }
                                } else {
                                    tempData.aisle = "";
                                    tempData.number = "";
                                    tempData.btnAisle = {
                                        text: ""
                                    };
                                }
                            } else {
                                tempData.aisle = "";
                                tempData.number = "";
                                tempData.btnAisle = {
                                    text: ""
                                };
                            }
                            break;
                        }
                    }
                    displayProductsList.push(tempData);
                }
            }
            return displayProductsList;
        } catch (e) {
            kony.print("Exception in getProductsListSubCategoryData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Products List Sub Category Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function tempAction(eventObj) {
        kony.print("dummy action");
    }

    function getProductsListData() {
        try {
            var productsList = _data.categories || [];
            var aisleNumbers = _data.CATEGORY_WAYFINDING || [];
            productsList = MVCApp.Toolbox.common.isArray(productsList) ? productsList : [productsList];
            var displayProductsList = [];
            for (var i = 0; i < productsList.length; i++) {
                var displayFlag = MVCApp.Toolbox.common.showInMenu(productsList[i].c_hideOnMobile, productsList[i].c_showInMenu, productsList[i].c_showInMobileApp);
                if (displayFlag) {
                    var tempData = productsList[i];
                    if (!productsList[i].fullName) {
                        productsList[i].fullName = productsList[i].name;
                    }
                    tempData.fullName = productsList[i].fullName;
                    if (!productsList[i].c_CategoryUrl) {
                        productsList[i].c_CategoryUrl = productsList[i].c_CategoryUrl;
                    }
                    tempData.c_CategoryUrl = productsList[i].c_CategoryUrl;
                    displayProductsList.push(tempData);
                }
            }
            return displayProductsList;
        } catch (e) {
            kony.print("Exception in getProductsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Products List Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getProductsListCategoryData() {
        try {
            var lMyLocationFlag = false;
            var storeBlockFlag = "";
            var storeObj = {};
            var storeString = kony.store.getItem("myLocationDetails") || "";
            if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled) {
                if (gblInStoreModeDetails.POI) {
                    storeObj = gblInStoreModeDetails.POI;
                }
                lMyLocationFlag = "true";
            } else {
                lMyLocationFlag = MVCApp.Toolbox.Service.getMyLocationFlag();
                if (storeString !== "") {
                    try {
                        storeObj = JSON.parse(storeString);
                    } catch (e) {
                        MVCApp.sendMetricReport(frmHome, [{
                            "Parse_Error_DW": "dataProductsList.js on getProductsListCategoryData for storeString:" + JSON.stringify(e)
                        }]);
                        if (e) {
                            TealiumManager.trackEvent("POI Parse Exception in Products List Category Flow", {
                                exception_name: e.name,
                                exception_reason: e.message,
                                exception_trace: e.stack,
                                exception_type: e
                            });
                        }
                    }
                }
            }
            if (storeObj && storeObj.isblocked) {
                storeBlockFlag = storeObj.isblocked || "";
            }
            var productsList = _data.categories || [];
            var aisleNumbers = _data.CATEGORY_WAYFINDING || [];
            productsList = MVCApp.Toolbox.common.isArray(productsList) ? productsList : [productsList];
            var displayProductsList = [];
            for (var i = 0; i < productsList.length; i++) {
                var displayFlag = MVCApp.Toolbox.common.showInMenu(productsList[i].c_hideOnMobile, productsList[i].c_showInMenu, productsList[i].c_showInMobileApp);
                if (displayFlag) {
                    var tempData = productsList[i];
                    tempData.carrotFlag = "K";
                    tempData.btnAisle = {
                        isVisible: false,
                        text: ""
                    };
                    var prodName = productsList[i].name || "";
                    tempData.fullName = prodName;
                    var c_CategoryUrl = productsList[i].c_CategoryUrl || "";
                    tempData.c_CategoryUrl = c_CategoryUrl;
                    prodName = (prodName.length > 26 ? prodName.substring(0, 24) + "..." : prodName) || "";
                    tempData.name = prodName;
                    for (var j = 0; j < aisleNumbers.length; j++) {
                        var tempSubCategoryId = aisleNumbers[j].category_id || "";
                        if (tempSubCategoryId == tempData.id) {
                            var templevel = aisleNumbers[j].location || "";
                            var tempWrap_clip_id = aisleNumbers[j].wrap_clip_id || "";
                            if (templevel !== "") {
                                if (lMyLocationFlag === "true" && storeBlockFlag != "true") {
                                    var aisleInfo = MVCApp.Toolbox.common.getStoreMapElementName(templevel);
                                    var aisleName = aisleInfo.name || "";
                                    if (aisleName !== "") {
                                        tempData.aisle = aisleInfo.name;
                                        if (tempData.aisle == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmShoppingList.wall")) tempData.aisle = {
                                            "centerY": "50%",
                                            "text": tempData.aisle
                                        };
                                        tempData.number = aisleInfo.number;
                                        tempData.btnAisle = {
                                            isVisible: true,
                                            "zIndex": 5,
                                            text: "",
                                            onClick: onClickAisle,
                                            info: {
                                                name: productsList[i].name,
                                                aisleNum: templevel,
                                                wrap_clip_id: tempWrap_clip_id
                                            }
                                        };
                                        tempData.carrotFlag = "";
                                    } else {
                                        tempData.aisle = "";
                                        tempData.number = "";
                                        tempData.btnAisle = {
                                            text: ""
                                        };
                                    }
                                } else {
                                    tempData.aisle = "";
                                    tempData.number = "";
                                    tempData.btnAisle = {
                                        text: ""
                                    };
                                }
                            } else {
                                tempData.aisle = "";
                                tempData.number = "";
                                tempData.btnAisle = {
                                    text: ""
                                };
                            }
                            break;
                        }
                    }
                    displayProductsList.push(tempData);
                }
            }
            return displayProductsList;
        } catch (e) {
            kony.print("Exception in getProductsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Products List Category Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getMarketingMessage() {
        try {
            var response = _data.response || [];
            return response;
        } catch (e) {
            kony.print("Exception in getProductsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Marketing Message Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    //Here we expose the public variables and functions
    return {
        typeOf: typeOf,
        getProductsListData: getProductsListData,
        getProductsListSubCategoryData: getProductsListSubCategoryData,
        getProductsListCategoryData: getProductsListCategoryData,
        getMarketingMessage: getMarketingMessage
    };
});