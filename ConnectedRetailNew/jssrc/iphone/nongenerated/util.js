//Type your code here
var cards = null;
var subCards = [];
var cardsYNorm = [];
var cardsTops = [];
var bruteForce = true;
var subTransform1 = [];
var subTransform2 = [];
var cardTints = [];
var gblWrap_Clip_ID = "";
var oldDate;
var timerLaunch = true;
var timerRepeat = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.timerRepeat");
var gblWifiTestUrl = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.TimerURL"); //http://www.tiny-website.com/
var gblTimerInterval = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.TimerInterval"));
var gblThresoldValue = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.ThresholdValue"));
var initialWifiConnectTime = new Date().getTime();

function checkIfIsCurDateWithInPromoDatesRange(pStartDateStr, pEndDateStr) {
    var lIsCurDateWithInPromoDatesRange = false;
    var lStartDateObj = new Date(pStartDateStr);
    var lEndDateObj = new Date(pEndDateStr);
    var lCurDateObj = new Date();
    if ((lCurDateObj.valueOf() > lStartDateObj.valueOf()) && (lCurDateObj.valueOf() < lEndDateObj.valueOf())) {
        lIsCurDateWithInPromoDatesRange = true;
    }
    return lIsCurDateWithInPromoDatesRange;
}

function destroyAllForms() {
    MVCApp.destroyHome();
    MVCApp.destroyMore();
    MVCApp.destroyPDP();
    MVCApp.destroyProductCategory();
    MVCApp.destroyProductCategoryMore();
    MVCApp.destroyProducts();
    MVCApp.destroyProductList();
    MVCApp.destroyProjects();
    MVCApp.destroyProjectsCategory();
    MVCApp.destroyProjectsList();
    MVCApp.destroyPJDP();
    MVCApp.destroyScan();
    MVCApp.destroyStoreMap();
    MVCApp.destroyStoreLocator();
    MVCApp.destroyReviews();
    //MVCApp.destroyPinchandZoom();
    MVCApp.destroyCreateAccount();
    MVCApp.destroyCreateAccounTwo();
    MVCApp.destroySignIn();
    MVCApp.destroyForgotPassword();
    MVCApp.destroyBrowser();
    MVCApp.destroyMyAccountController();
    MVCApp.destroyRewardsProgramInfoController();
    MVCApp.destroyJoinRewardsController();
    MVCApp.destroySettingsController();
    MVCApp.destroyweeklyAdHomeController();
    MVCApp.destroyweeklyAdController();
    MVCApp.destroyweeklyAdDetailController();
    MVCApp.destroyShoppingList();
    MVCApp.destroyHelp();
    frmCoupons.destroy();
    MVCApp.destroyEvents();
    MVCApp.destroyEventDetail();
    MVCApp.destroySortAndRefine();
    MVCApp.destroyRefineProjects();
    MVCApp.destroyImageSearch();
    MVCApp.destroyVoiceSearchVideos();
}

function clearGlobals() {
    gblproducts = null;
    gblProjects = null;
}
var prevContentOffsetHome = 0;

function onreachWidgetEnd(event) {
    try {
        var currFrm = kony.application.getCurrentForm();
        var widgetId = event.id;
        var offsetY = currFrm[widgetId].contentOffsetMeasured.y;
        offsetY = parseInt(offsetY);
        var contentHeight = currFrm[widgetId].contentSizeMeasured.height;
        var viewPortHeight = currFrm[widgetId].frame.height;
        if (contentHeight == offsetY + viewPortHeight) {
            MVCApp.Toolbox.common.dispFooter();
        } else if (offsetY == 0) {
            MVCApp.Toolbox.common.dispFooter();
        } else if (offsetY > prevContentOffsetHome) {
            MVCApp.Toolbox.common.hideFooter();
        } else if (offsetY < prevContentOffsetHome) {
            MVCApp.Toolbox.common.dispFooter();
        }
        prevContentOffsetHome = offsetY;
    } catch (e) {
        kony.print("exception---" + e);
    }
}
scrollContentOffsetHome = 0;

function onreachScrollEnd(event) {
    try {
        var currFrm = kony.application.getCurrentForm();
        var widgetId = event.id;
        var offsetY = currFrm[widgetId].contentOffsetMeasured.y;
        offsetY = parseInt(offsetY);
        if (offsetY == scrollContentOffsetHome) {
            MVCApp.Toolbox.common.dispFooter();
        }
        scrollContentOffsetHome = offsetY;
    } catch (e) {
        kony.print("exception in  " + JSON.stringify(e));
        if (e) {
            TealiumManager.trackEvent("Exception On Scrolling To The End", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function postShowHomeAnimate() {
    try {
        cards = frmHome.cardsContainer.widgets();
        for (i = 0; i < cards.length; i++) {
            cards[i].anchorPoint = {
                "x": 0.5,
                "y": 1
            };
            cardsTops[i] = cards[i].frame.y;
            var mySubCards = cards[i].widgets();
            mySubCards[0].anchorPoint = {
                "x": 0.5,
                "y": 0
            };
            mySubCards[1].anchorPoint = {
                "x": 0.5,
                "y": 1
            };
            var firstSubCardChildren = mySubCards[0].widgets();
            cardTints[i] = firstSubCardChildren[1];
            cardTints[i].opacity = 0;
        }
    } catch (e) {
        alert("LOG:postShowHomeAnimate-e:" + e);
        if (e) {
            TealiumManager.trackEvent("Exception While The Home Screen Is Animating", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function cardScrollOnScrolling() {
    try {
        var scrollPosY = frmHome.cardScroll.contentOffsetMeasured.y;
        kony.print("eglog scrollPosY = " + scrollPosY);
        if (scrollPosY > 1) {
            for (i = 0; i < cards.length; i++) {
                cardsYNorm[i] = Math.min(1, (Math.max(0, scrollPosY - cardsTops[i]) / 50)); //135
                if (bruteForce == true) {
                    transformCards();
                } else if (bruteForce == false && cardsYNorm[i] > 0 && cardsYNorm[i] != 1) {
                    transformCards();
                }
            }
        } else {
            //cards[0].height="120dp";
        }
    } catch (e) {
        alert("LOG:cardScrollOnScrolling-e:" + e);
        if (e) {
            TealiumManager.trackEvent("Exception On Scrolling The Card Scroll", {
                exception_name: e.name,
                exception_reason: e.message,
                exception_trace: e.stack,
                exception_type: e
            });
        }
    }
}

function transformCards() {
    var cardShrink = kony.ui.makeAffineTransform();
    cards[i].transform = cardShrink;
    var mySubCards = cards[i].widgets();
    subTransform1[i] = kony.ui.makeAffineTransform();
    subTransform1[i].setPerspective(950.0);
    subTransform1[i].rotate3D(cardsYNorm[i] * 90, 1, 0, 0);
    subTransform2[i] = kony.ui.makeAffineTransform()
    subTransform2[i].setPerspective(950.0);
    subTransform2[i].rotate3D((cardsYNorm[i] * 90), -1, 0, 0);
    mySubCards[0].transform = subTransform1[i];
    mySubCards[0].top = (cardsYNorm[i] * 106)
    mySubCards[1].transform = subTransform2[i];
    cardTints[i].opacity = cardsYNorm[i];
}

function postShowAnimateCoupons() {
    var lTealiumTagObj = gblTealiumTagObj;
    var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
    var storeId = MVCApp.Toolbox.common.getStoreID();
    var loginStatus = MVCApp.Toolbox.common.getLoginStatus();
    cust360Obj.storeid = storeId;
    cust360Obj.loginStatus = loginStatus;
    var entryType = "";
    if (couponsEntryFromVoiceSearch) {
        entryType = "VoiceSearch";
        MVCApp.Customer360.callVoiceSearchResult("success", "", "Coupons");
    }
    cust360Obj.entryType = entryType;
    if (gblIsRewardsBeingShown) {
        gblIsRewardsBeingShown = false;
        lTealiumTagObj.page_id = "Rewards: Michaels Mobile App";
        lTealiumTagObj.page_name = "Rewards: Michaels Mobile App";
        lTealiumTagObj.page_type = "Rewards";
        lTealiumTagObj.page_category_name = "Rewards";
        lTealiumTagObj.page_category = "Rewards";
        TealiumManager.trackView("Rewards Screen", lTealiumTagObj);
        MVCApp.Customer360.sendInteractionEvent("Rewards", cust360Obj);
    } else {
        lTealiumTagObj.page_id = "Coupons: Michaels Mobile App";
        lTealiumTagObj.page_name = "Coupons: Michaels Mobile App";
        lTealiumTagObj.page_type = "Navigation";
        lTealiumTagObj.page_category_name = "Coupons";
        lTealiumTagObj.page_category = "Coupons";
        TealiumManager.trackView("Coupons Icon", lTealiumTagObj);
        MVCApp.Customer360.sendInteractionEvent("Coupons", cust360Obj);
    }
}

function callScHdrBack() {
    var ht = this.id;
    if (this.frame.height > 14) {
        this.bottom = "15dp";
    } else {
        this.bottom = "0dp";
    }
}
var gBtnheight = 0;

function callScHdrBackDelete() {
    var ht = this.id;
    gBtnheight = this.frame.height;
}

function dummy() {
    frmFooterAnim.flxFooterWrap['btnMyLists'].animate(kony.ui.createAnimation({
        0: {
            top: "-20dp",
            "stepConfig": {}
        },
        49.9: {
            top: "0dp",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        },
        50: {
            top: "5dp",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        },
        100: {
            top: "5dp",
            "stepConfig": {}
        }
    }), {
        delay: 0,
        fillMode: kony.anim.FILL_MODE_FORWARDS,
        duration: 1
    }, {
        animationEnd: function() {}
    });
}
//Switching to wifi and network details
var alreadyAlertedLTEToWifi = false;
var alreadyAlertedWifiToLTE = false;

function notifyUserToChangeNetwork() {
    if (!alreadyAlertedWifiToLTE) {
        MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.ChangeNetworkWifiToLTE"), "");
        alreadyAlertedWifiToLTE = true;
        kony.timer.cancel("MichaelTimerWifi1");
        var currentTimeStamp = new Date().getTime();
        try {
            kony.store.setItem("lastMessageSent", currentTimeStamp);
        } catch (e) {
            kony.print("excpetion in setting data to device store");
            if (e) {
                TealiumManager.trackEvent("Exception To Notify The User To Change Network", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
}

function processURLResponse() {
    //kony.print("the response  "+JSON.stringify(this));
    var strReadyState = this.readyState.toString();
    if (strReadyState === "4") {
        if ("200" == this.status) {
            var newDate = new Date();
            var diffTime = newDate - oldDate;
            if (diffTime > parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.ThresholdValue"))) {
                notifyUserToChangeNetwork();
                kony.print("The response time is greater" + diffTime);
                MVCApp.sendMetricReport(frmHome, [{
                    "Store_Bad_Wifi_Time": diffTime
                }]);
            } else {
                kony.print("The response time is lesser" + diffTime);
                MVCApp.sendMetricReport(frmHome, [{
                    "Store_Good_Wifi_Time": diffTime
                }]);
            }
        } else {}
    }
}

function getResponseTime() {
    var networkType = kony.net.getActiveNetworkType();
    if (networkType === constants.NETWORK_TYPE_WIFI) {
        var allowService = true;
        var currentTimeStamp = new Date().getTime();
        var lastStoreVisitTimeStamp = kony.store.getItem("lastMessageSent");
        var timeIntervals = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.intervalForVisitTimer"));
        kony.print("CurrentTime Stamp - LastStoreVisitStamp" + ((currentTimeStamp - lastStoreVisitTimeStamp) / (1000 * 60 * 60)));
        if (((currentTimeStamp - lastStoreVisitTimeStamp) / (1000 * 60 * 60)) < timeIntervals) {
            allowService = !allowService;
        }
        if ((!alreadyAlertedLTEToWifi && !alreadyAlertedWifiToLTE) && allowService) {
            var httpClient = new kony.net.HttpRequest();
            httpClient.timeout = 5000;
            httpClient.open(constants.HTTP_METHOD_GET, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.TimerURL"));
            oldDate = new Date();
            httpClient.send();
            httpClient.onReadyStateChange = processURLResponse;
            httpClient.onTimeout = notifyUserToChangeNetwork;
        }
    } else {
        kony.timer.cancel("MichaelTimerWifi1");
    }
    //timerLaunch=false;
}

function invokeTimerForWifiSwitch() {
    if (timerRepeat === "False") {
        kony.timer.cancel("MichaelTimerWifi1");
    } else {
        if (timerLaunch) {
            try {
                var time = parseInt(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.TimerInterval"));
                kony.timer.schedule("MichaelTimerWifi1", getResponseTime, time, true);
            } catch (e) {
                kony.print("exception in invoking timer MichaelTimerWifi1 " + JSON.stringify(e));
                if (e) {
                    TealiumManager.trackEvent("Timer Exception For WIFI Switch", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }
}

function getWidthFromStore() {
    var screenWidth = kony.os.deviceInfo().screenWidth;
    return screenWidth;
}