//Type your code here
function tapProductsCategory() {
    frmProductCategory.flxFooterWrap.flxProducts.skin = 'sknFlexBgBlackOpaque10';
    frmProductCategory.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmProductCategory.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmProductCategory.flxFooterWrap.flxEvents.skin = 'slFbox';
    frmProductCategory.flxFooterWrap.flxMore.skin = 'slFbox';
}

function pre_frmProductCategory() {
    searchBlur();
    tapProductsCategory();
    frmProductCategory.flxHeaderWrap.btnHome.isVisible = true;
    frmProductCategory.flxHeaderWrap.textSearch.text = '';
}

function NAV_frmProductCategory() {
    frmProductCategory.show();
    pre_frmProductCategory();
}