//Type your code here
var MVCAppAnalytics = {
    sendCustomMetrics: function(reportType, searchQuery, entryMode, beaconInfo, storeId) {
        var serviceName = MVCApp.serviceType.MFCustomAnalytics;
        var operationId = MVCApp.serviceEndPoints.addCustomReportsToAnalytics;
        var timeStamp = new Date().getTime();
        var storeID = "";
        var inputParams = {};
        if (reportType == "searchReport") {
            storeID = MVCApp.Toolbox.common.getStoreID();
        } else {
            storeID = storeId;
        }
        var platform = "";
        platform = "iPhone";
        var deviceDetails = kony.os.deviceInfo();
        var deviceModel = deviceDetails.model;
        var osVersion = deviceDetails.version;
        var environment = appConfig.isturlbase;
        inputParams.searchQuery = searchQuery;
        inputParams.locale = MVCApp.Toolbox.Service.getLocale();
        inputParams.deviceID = MVCApp.Toolbox.common.getDeviceID();
        inputParams.storeID = storeID;
        inputParams.loyaltyID = MVCApp.Toolbox.common.getLoyaltyID();
        inputParams.deviceTimeStamp = timeStamp;
        inputParams.reportType = reportType;
        inputParams.beaconInfo = beaconInfo;
        inputParams.appVersion = appConfig.appVersion;
        inputParams.os = osVersion;
        inputParams.platform = platform;
        inputParams.kuid = MVCApp.Toolbox.common.getKuid();
        inputParams.entrytype = entryMode;
        inputParams.aName = "Michaels";
        inputParams.env = environment;
        inputParams.dm = deviceModel;
        //inputParams.os = os;
        MVCAppAnalytics.addToBuffer(inputParams);
        MVCAppAnalytics.executeBufferList();
    },
    makeServiceCall: function(serviceName, operationId, inputParams) {
        if (MVCApp.Toolbox.common.checkIfNetworkExists()) {
            var integrationObj = KNYMobileFabric.getIntegrationService(serviceName);
            inputParams.appVersion = appConfig.appVersion;
            kony.print("-------->Making Service Call to SERVICE NAME: " + serviceName + " ; with OPERATION ID: " + operationId);
            kony.print("-------->Input/Request parameters: " + JSON.stringify(inputParams));
            integrationObj.invokeOperation(operationId, {}, inputParams, function(results) {
                //  kony.print("--------->Total time to finish success:"+ new Date() - startDate);
                results = results || {};
                if (results.opstatus === null || results.opstatus === undefined) results.opstatus = 0;
                kony.print("-------->Response Success result: " + JSON.stringify(results));
            }, function(error) {
                MVCAppAnalytics.addToBuffer(inputParams);
            });
        } else {}
    },
    addToBuffer: function(bufferObj) {
        var bufferList = new Array();
        var bufferListObj = kony.store.getItem("analyticsBuffer") || "";
        if (bufferListObj && bufferListObj != "") {
            var objNew = JSON.parse(bufferListObj);
            bufferList = objNew.list || [];
        }
        bufferList.push(bufferObj);
        var newBufferObj = JSON.stringify({
            "list": bufferList
        });
        kony.store.setItem("analyticsBuffer", newBufferObj);
    },
    executeBufferList: function() {
        var bufferList = new Array();
        var bufferListStr = kony.store.getItem("analyticsBuffer") || "";
        if (bufferListStr && bufferListStr != "") {
            var newBufferObj = JSON.parse(bufferListStr);
            bufferList = newBufferObj.list || [];
        }
        var lengthOfList = bufferList.length || 0;
        if (bufferList.length > 0) {
            var bufferObj = bufferList[0];
            var serviceName = MVCApp.serviceType.MFCustomAnalytics;
            var operationId = MVCApp.serviceEndPoints.addCustomReportsToAnalytics;
            MVCAppAnalytics.makeServiceCall(serviceName, operationId, bufferObj);
            bufferList.splice(0, 1);
            if (lengthOfList == 1) {
                kony.store.removeItem("analyticsBuffer");
            } else {
                var newObj = {
                    "list": bufferList
                };
                kony.store.setItem("analyticsBuffer", JSON.stringify(newObj));
            }
            MVCAppAnalytics.executeBufferList();
        }
    }
};