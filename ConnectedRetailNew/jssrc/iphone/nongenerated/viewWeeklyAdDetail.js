/**
 * PUBLIC
 * This is the view for the WeeklyAdDetail form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var loadWeeklyAds = false;
var gblUrlShopOnline = "";
var MVCApp = MVCApp || {};
MVCApp.WeeklyAdDetailView = (function() {
    var url = "";
    /**
     * PUBLIC
     * Open the More form
     */
    function isNullOrEmpty(value) {
        if (value == undefined || value == null || value == "" || value == "null") {
            return "";
        } else {
            return value;
        }
    }

    function show() {
        frmWeeklyAdDetail.flxHeaderWrap.textSearch.text = "";
        frmWeeklyAdDetail.flxSearchResultsContainer.setVisibility(false);
        frmWeeklyAdDetail.defaultAnimationEnabled = true;
        MVCApp.Toolbox.common.setTransition(frmWeeklyAd, 2);
        frmWeeklyAdDetail.show();
    }

    function shopOnline() {
        url = gblUrlShopOnline;
        url = decodeURI(url);
        url = url.replace(/%2C/g, ",");
        url = encodeURI(url);
        var cm_mmc = "MIK_ecMobileApp-_-ShopOnline-_-WeeklyAd-_-NA";
        var inStoreModeEnabled = false;
        var inStoreModeStoreID = "";
        if (gblInStoreModeDetails) {
            inStoreModeEnabled = gblInStoreModeDetails.isInStoreModeEnabled || false;
        }
        if (inStoreModeEnabled) {
            cm_mmc = "MIK_ecMobileApp-_-ShopInstoreMode-_-WeeklyAd-_-NA";
            var storevisitValue = gblInStoreModeDetails.storeID || "";
            if (storevisitValue !== "") {
                storevisitValue = MVCApp.Toolbox.common.encodeBASE64(storevisitValue);
            }
            inStoreModeStoreID = storevisitValue;
        }
        kony.print("url+cm_mmc::" + url + cm_mmc);
        var match_1 = url.match(/(\?)/);
        var match_2 = url.match(/(\#)/);
        var index_1 = match_1 ? match_1[0].length - 1 : -1;
        var index_2 = match_2 ? match_2[0].length - 1 : -1;
        var storeId = MVCApp.Toolbox.common.getStoreID();
        var favStoreId = MVCApp.Toolbox.common.getFavouriteStore();
        var appRequest = "?stid=" + storeId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
        if (favStoreId !== "") {
            appRequest = "?stid=" + favStoreId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
        }
        if (index_2 === 0) {
            kony.print("Index Of test" + url.indexOf("#"));
            if (inStoreModeStoreID !== "") {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    //url=  url.replace("www.michaels.com", "test.www.michaels.com"); 
                    MVCApp.getProductsListWebController().loadWebView(url + appRequest + "&storevisit=" + inStoreModeStoreID);
                } else {
                    MVCApp.Toolbox.common.openApplicationURL(url + "?storevisit=" + inStoreModeStoreID);
                }
            } else {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    //url=  url.replace("www.michaels.com", "test.www.michaels.com");
                    MVCApp.getProductsListWebController().loadWebView(url + appRequest);
                } else {
                    MVCApp.Toolbox.common.openApplicationURL(url);
                }
            }
        } else if (index_1 === 0) {
            appRequest = "&stid=" + storeId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
            if (inStoreModeStoreID !== "") {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    //url=  url.replace("www.michaels.com", "test.www.michaels.com"); 
                    MVCApp.getProductsListWebController().loadWebView(url + appRequest + "&cm_mmc=" + cm_mmc + "&storevisit=" + inStoreModeStoreID);
                } else {
                    MVCApp.Toolbox.common.openApplicationURL(url + "&cm_mmc=" + cm_mmc + "&storevisit=" + inStoreModeStoreID);
                }
            } else {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    //url=  url.replace("www.michaels.com", "test.www.michaels.com");
                    MVCApp.getProductsListWebController().loadWebView(url + appRequest + "&cm_mmc=" + cm_mmc);
                } else {
                    MVCApp.Toolbox.common.openApplicationURL(url + "&cm_mmc=" + cm_mmc);
                }
            }
        } else {
            //appRequest= "?stid="+storeId+"&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
            if (inStoreModeStoreID !== "") {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    //url=  url.replace("www.michaels.com", "test.www.michaels.com");
                    MVCApp.getProductsListWebController().loadWebView(url + appRequest + "&cm_mmc=" + cm_mmc + "&storevisit=" + inStoreModeStoreID);
                } else {
                    MVCApp.Toolbox.common.openApplicationURL(url + "?cm_mmc=" + cm_mmc + "&storevisit=" + inStoreModeStoreID);
                }
            } else {
                if (MVCApp.Toolbox.common.getRegion() == "US") {
                    //url=  url.replace("www.michaels.com", "test.www.michaels.com");
                    MVCApp.getProductsListWebController().loadWebView(url + appRequest + "&cm_mmc=" + cm_mmc);
                } else {
                    MVCApp.Toolbox.common.openApplicationURL(url + "?cm_mmc=" + cm_mmc);
                }
            }
        }
        MVCApp.sendMetricReport(frmWeeklyAdDetail, [{
            "shopOnline": "click"
        }]);
        TealiumManager.trackEvent("Buy Online Button Click From Weekly Ad", {
            conversion_category: "Weekly Ad",
            conversion_id: "Shop Online",
            conversion_action: 2
        });
        //FB Analytics
        //FacebookAnalytics.reportEvents("", "", url, "frmWeeklyAdDetail");
    }

    function shareOffers() {
        var shareTxt = url;
        share(shareTxt);
    }

    function share(shareTxt) {
        var shareArray = new Array(frmWeeklyAdDetail.lblProductName.text + "\n" + shareTxt, frmWeeklyAdDetail.imgProduct.src, "", frmWeeklyAdDetail.lblProductName.text);
        if (kony.os.deviceInfo().name === "android") {
            var ShareTestObject = new shareFFI.ShareTest();
            requestPermissionAtRuntime(kony.os.RESOURCE_EXTERNAL_STORAGE, function() {
                ShareTestObject.shareSomething(shareArray);
            });
        } else {
            var ShareIphoneObject = new shareFFI.ShareIphone();
            ShareIphoneObject.ShareonSocialMedia(
                /**Array*/
                shareArray);
        }
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.weeklyadid = frmWeeklyAdDetail.lblProductName.text || "";
        MVCApp.Customer360.sendInteractionEvent("ShareWeeklyad", cust360Obj);
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmWeeklyAdDetail.lblIconChevron.onTouchStart = function() {
            if (frombrowserdeeplink) {
                frombrowserdeeplink = false;
                MVCApp.getHomeController().load();
            } else {
                loadWeeklyAds = false;
                MVCApp.Toolbox.common.setTransition(frmWeeklyAd, 2);
                frmWeeklyAd.show();
            }
        };
        frmWeeklyAdDetail.btnBuyOnline2.onClick = shopOnline;
        frmWeeklyAdDetail.btnShare.onClick = shareOffers;
        frmWeeklyAdDetail.preShow = function() {
            frmWeeklyAdDetail.flxSearchOverlay.setVisibility(false);
            frmWeeklyAdDetail.flxVoiceSearch.setVisibility(false);
        };
        frmWeeklyAdDetail.postShow = function() {
            var cartValue = MVCApp.Toolbox.common.getCartItemValue();
            frmWeeklyAdDetail.flxHeaderWrap.lblItemsCount.text = cartValue;
            MVCApp.Toolbox.common.requestReview();
            frmWeeklyAdDetail.flxSearchOverlay.setVisibility(false);
            MVCApp.Toolbox.common.setBrightness("frmWeeklyAdDetail");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Product:" + frmWeeklyAdDetail.lblProductName.text;
            lTealiumTagObj.page_name = "Product:" + frmWeeklyAdDetail.lblProductName.text;
            lTealiumTagObj.page_type = "product";
            lTealiumTagObj.page_category_name = frmWeeklyAdDetail.btnDeleteAll.text;
            lTealiumTagObj.page_category = frmWeeklyAdDetail.btnDeleteAll.text;
            lTealiumTagObj.page_subcategory_name = frmWeeklyAdDetail.btnDeleteAll.text;
            lTealiumTagObj.product_name = frmWeeklyAdDetail.lblProductName.text;
            TealiumManager.trackView("Weekly Ad Product", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.weeklyadid = MVCApp.getWeeklyAdDetailController().getAdId();
            cust360Obj.storeid = MVCApp.Toolbox.common.getStoreID();
            MVCApp.Customer360.sendInteractionEvent("WeeklyAdDetailsPage", cust360Obj);
        };
        MVCApp.searchBar.bindEvents(frmWeeklyAdDetail, "frmWeeklyAdDetail");
        MVCApp.tabbar.bindEvents(frmWeeklyAdDetail);
        MVCApp.tabbar.bindIcons(frmWeeklyAdDetail, "frmWeeklyAd");
    }

    function updateScreen(results, headerText) {
        url = results.buyOnlineLinkURL || "";
        gblUrlShopOnline = url;
        frmWeeklyAdDetail.flxWeeklyAdHeader.setVisibility(false);
        var additionalOffer = results.lblAdditionalDeal || "";
        var additionalOfferlbl = additionalOffer.text || "";
        var additionalOfferVisibility = additionalOffer.isVisible || false;
        frmWeeklyAdDetail.lblOffer.setVisibility(additionalOfferVisibility);
        frmWeeklyAdDetail.lblOffer.text = additionalOfferlbl;
        var offerTag = results.offerTag || "";
        var offerImage = offerTag.text || "";
        kony.print("offerImage-" + offerImage);
        var offerVisibility = offerTag.isVisible || false;
        frmWeeklyAdDetail.lblNewExclusive.text = offerImage;
        frmWeeklyAdDetail.flxWeeklyAdHeader.setVisibility(offerVisibility);
        frmWeeklyAdDetail.lblNewExclusive.setVisibility(offerVisibility);
        frmWeeklyAdDetail.lblProductName.text = results.title || "";
        frmWeeklyAdDetail.imgProduct.src = results.image1 || "";
        frmWeeklyAdDetail.CopylblRegPrice0b59ce1127b2d48.text = results.deal || "";
        var dateRange = results.dateRange || "";
        dateRange = dateRange.trim();
        if (dateRange && dateRange != "-") {
            dateRange = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAd.valid") + " " + dateRange;
        }
        frmWeeklyAdDetail.CopylblRegPrice0f0c89a69741241.text = dateRange;
        var desc = results.description || "";
        var finePrint = isNullOrEmpty(results.finePrint);
        frmWeeklyAdDetail.CopylblRegPrice0bcd0f1b8e8bc4f.text = removeEscapeCharacters(desc);
        frmWeeklyAdDetail.CopylblRegPrice0b93ba9bff8704b.text = removeEscapeCharacters(finePrint);
        frmWeeklyAdDetail.lblRegPrice.text = results.originalDeal || "";
        var storeStatus = results.storeStatus || "";
        frmWeeklyAdDetail.lblStoreInfo.text = storeStatus;
        var shopOnlineFlag = results.shopOnlineButton || false;
        if (!shopOnlineFlag) {
            frmWeeklyAdDetail.flxShare1.setVisibility(false);
            frmWeeklyAdDetail.btnBuyOnline2.setVisibility(false);
        } else {
            frmWeeklyAdDetail.flxShare1.setVisibility(true);
            frmWeeklyAdDetail.btnBuyOnline2.setVisibility(true);
        }
        frmWeeklyAdDetail.btnDeleteAll.text = headerText;
        show();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function removeEscapeCharacters(value) {
        var rmdEscapeChartrStrng = value.replace("\r\n", "");
        rmdEscapeChartrStrng.replace("\n", "");
        rmdEscapeChartrStrng.replace("\r", "");
        rmdEscapeChartrStrng.replace("\t", "");
        return rmdEscapeChartrStrng;
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});