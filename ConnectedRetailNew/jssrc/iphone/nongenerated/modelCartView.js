/**
 * PUBLIC
 * This is the model for formProductsList form
 */
var MVCApp = MVCApp || {};
MVCApp.CartViewModel = (function() {
    function loadData(callback, url, flag) {
        kony.print("CartModel.js");
        var lPreviousForm = kony.application.getCurrentForm() || "";
        var isSession = false;
        var lPreviousForm = kony.application.getCurrentForm() || "";
        var lPreviousForm_1 = kony.application.getPreviousForm() || "";
        kony.print("lPreviousForm_1--------->" + lPreviousForm_1);
        if (lPreviousForm.id == 'frmCartView') {
            isSession = (lPreviousForm_1 && lPreviousForm_1.id == 'frmWebView') ? true : false;
        } else isSession = (lPreviousForm && lPreviousForm.id == 'frmWebView') ? true : false;
        if (isSession) {
            callback(url, flag);
        } else {
            //  kony.net.clearCookies(url);
            callback(url, flag);
        }
    }

    function load(searchStr, callback) {
        var storeId = MVCApp.Toolbox.common.getStoreID();
        var appRequest = "&stid=" + storeId + "&appsource=mobileapp&cm_mmc=MIK_ecMobileApp";
        var url = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels");
        // kony.net.clearCookies(url);
        url = url + searchStr + appRequest;
        callback(url);
    }
    return {
        load: load,
        loadData: loadData
    };
});