//Type your code here
//var couponsData = {"code":200,"message":"success","version":"2","Offers":[{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":null,"barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34008","offerDisclaimer":"Excludes Everyday Value, Custom Frame Express, expedited shipping and engraved plates. Must purchase a moulding from the collection(s) listed. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-06-17 11:59:59 PM","offerId":34008,"offerInstance":0,"offerInstructions":"Valid in Canada only.","offerName":"Michaels-170402C","offerPublished":"03-30-17 09:00:00 PM","offerStart":"03-31-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"40% Off Cityscape™ Custom Frame Collection","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/b9109f1b-423e-4498-9fc3-f26f027161c1.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/24b19b16-043a-4804-904d-e2301c2a9138.png","priority":5,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, March 31, 2017 to Thursday, April 06, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":"Tap to Redeem","barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33972","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a moulding from the collection(s) listed. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33972,"offerInstance":0,"offerInstructions":"Valid in US only. Offers may vary by location.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"60% Off Plus Get An Extra 10% Off Sale Price Hampton™ & Oxford Street™ Custom Frame Collections","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/7d128fb4-f48f-474d-8d78-09f8bab4404f.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/0b14fed9-e399-42d0-b24c-954117ed0643.png","priority":5,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":null,"barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33978","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a moulding from the collection(s) listed. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33978,"offerInstance":0,"offerInstructions":"Valid in Ohio only.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"60% Off Plus Get An Extra 10% Off Sale Price Hampton™ Custom Frame Collection","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/3bd4133f-b4be-4d09-b86f-49a825388dcf.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/f07ca50a-458a-4226-8981-29fae683c8c9.png","priority":5,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":null,"barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33981","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a moulding from the collection(s) listed. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33981,"offerInstance":0,"offerInstructions":"Valid in Portland, OR only.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"60% Off Plus Get An Extra 10% Off Sale Price Summit™ & Uptown™ Custom Frame Collections","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/6e2102b0-90a6-4474-8509-c47cf7bdf97b.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/36976e5c-2064-4c66-918b-e9fdb84b28da.png","priority":5,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":null,"barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34002","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a custom frame. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":34002,"offerInstance":0,"offerInstructions":"Valid at Cape Coral, FL Grand Opening store only.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"60% Off Plus Get an Extra 20% Off Sale Price All Custom Frames","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/0fd64bf8-1eef-4d02-bae0-0385af490a2c.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/75021318-4a9a-4d01-9355-7d023cf41812.png","priority":5,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010008598&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010008598","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33993","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON</strong>: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-06-17 11:59:59 PM","offerId":33993,"offerInstance":0,"offerInstructions":"Valid at Cape Coral, FL Grand Opening store only.","offerName":"Michaels-170402","offerPublished":"04-02-17 09:00:00 PM","offerStart":"04-03-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/0d216432-e637-4de8-b8b1-af656a222a86.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/b4072289-1d56-4123-818e-3d30d60a23d1.png","priority":2,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010008598","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010008598&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010008598","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Monday, April 03, 2017 to Thursday, April 06, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010040411&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010040411","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34139","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> <strong>NOT VALID ON:</strong> Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals, Christmas trees, The Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Tax not included in “spend” to calculate discount. Only amount paid counts toward “spend.” Coupon may not be redeemed for cash value. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":34139,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Spend $25, Get $5 off; Spend $50, Get $10 off; Spend $75, Get $15 off; Spend $100, Get $20 off; Spend $125+, Get $25 off","offerTitle":"$5 Off With Every $25 or more Yarn Purchase","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/8eb4503b-a3b0-4846-a946-fc62f8f46c56.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/2938eb14-764b-4e28-ab71-8ddb15c7ef9b.png","priority":2,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010040411","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010040411&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010040411","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010050298&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010050298","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/41487","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON:</strong> Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":41487,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"20170402_Email_APRIL20DM","offerPublished":"03-30-17 01:41:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Entire Purchase Including Sale Items","offerTitle":"20% OFF","offerType":"ENTIRE COUPON","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/1734cc7c-3d44-491e-b0c0-e6930321b0e6.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/7f271bf3-0dd7-4939-8798-6a105366d88f.png","priority":2,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010050298","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010050298&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010050298","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"APRIL20DM","barcodeImage":"","barcodeString":"APRIL20DM","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010035085&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010035085","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/41493","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON:</strong> Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":41493,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"20170402_Email_APRIL25DM","offerPublished":"03-30-17 01:41:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Entire Purchase Including Sale Items","offerTitle":"25% OFF","offerType":"ENTIRE COUPON","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/fd585bfd-4953-40c7-bbf2-08bd7be5766c.png","priority":2,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010035085","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010035085&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010035085","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"APRIL25DM","barcodeImage":"","barcodeString":"APRIL25DM","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":null,"barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34099","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a custom frame. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-15-17 11:59:59 PM","offerId":34099,"offerInstance":0,"offerInstructions":"Valid in US only. Offer may vary in OH.  We accept competitor custom frame coupons.","offerName":"20170317_Email_LPOSDM","offerPublished":"03-16-17 02:00:00 PM","offerStart":"03-17-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Must Present Offer To Redeem.  We accept competitor custom frame coupons.","offerTitle":"70% Off All Custom Frames","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/94983182-6e2a-4b5d-b43a-056c7ec19357.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/0f7e8775-e627-4f1e-a831-55f32fccd61d.png","priority":2,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, March 17, 2017 to Saturday, April 15, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010096672&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010096672","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/41538","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON:</strong> Everyday Value program, Doorbusters, technology, custom frames, cameras and printers, books, magazines, candy and beverages, gift cards; clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, sewing machines, 3D printers, Hatchimals, Christmas trees, The Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-05-17 11:59:59 PM","offerId":41538,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"Michaels-170402","offerPublished":"04-04-17 09:00:00 PM","offerStart":"04-05-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Entire Purchase Including Sale Items","offerTitle":"20% OFF","offerType":"ENTIRE COUPON","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/c1eb6400-7218-44f4-ab5a-2f2456fbbfdb.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/945ed92b-9714-48a9-9aee-8190eefe4625.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010096672","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010096672&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010096672","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"20APRIL517","barcodeImage":"","barcodeString":"20APRIL517","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Wednesday, April 05, 2017  ONLY ","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010039117&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010039117","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34005","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON</strong>: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-06-17 11:59:59 PM","offerId":34005,"offerInstance":0,"offerInstructions":"Valid in Canada only.","offerName":"Michaels-170402C","offerPublished":"03-31-17 09:00:00 PM","offerStart":"04-01-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/5ecd2875-2aa8-4ca2-aa9e-8c2994e9b42d.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/e086f20d-e84f-44b7-9240-629de5f9d9a9.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010039117","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010039117&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010039117","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Saturday, April 01, 2017 to Thursday, April 06, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010039117&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010039117","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33969","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON</strong>: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33969,"offerInstance":0,"offerInstructions":"Valid in US only. Excludes Manhattan stores.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/0d56da09-4613-4876-985d-e9044b1dd92e.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/e4b930a2-cb4c-4704-bc60-02f036967cd0.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010039117","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010039117&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010039117","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"40SAVE4217","barcodeImage":"","barcodeString":"40SAVE4217","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010025125&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010025125","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33975","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON</strong>: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33975,"offerInstance":0,"offerInstructions":"Valid at Manhattan stores only.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"All Regular Price Purchases","offerTitle":"20% OFF","offerType":"ERPP","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/8b64aeae-16c7-481a-b3e4-23c1abc3636d.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/efe49acf-39ed-4ea6-893c-4cf85afbc668.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010025125","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010025125&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010025125","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"20SAVE4217","barcodeImage":"","barcodeString":"20SAVE4217","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010040902&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010040902","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33984","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON:</strong> Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33984,"offerInstance":0,"offerInstructions":"Valid in US only. Excludes Manhattan stores.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/06f15af7-6fa0-4cab-87e9-eb428ccf887b.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/4bbbf9eb-c0ce-488a-adee-9cf03149b0b7.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010040902","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010040902&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010040902","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"40SAVE4217","barcodeImage":"","barcodeString":"40SAVE4217","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010044175&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":"Tap to Redeem","barcodeString":"40010044175","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33987","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON</strong>: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, sewing machines, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-08-17 11:59:59 PM","offerId":33987,"offerInstance":0,"offerInstructions":"Valid in US only. Excludes Manhattan stores.","offerName":"Michaels-170402","offerPublished":"04-01-17 09:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/964bc9d9-84e9-4814-a3e5-6e1448c61e0f.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/3dc4b94e-8b82-4915-8627-34be6da18eaf.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010044175","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010044175&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010044175","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"40SAVE4217","barcodeImage":"","barcodeString":"40SAVE4217","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Saturday, April 08, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":"Tap to Redeem","barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/41490","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a custom frame. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-15-17 11:59:59 PM","offerId":41490,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"20170402_Email_APRIL20DM","offerPublished":"03-30-17 01:43:00 PM","offerStart":"03-31-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"70% Off All Custom Frame Collections","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/d272e17a-cc25-4d6b-98dd-5a1308300af5.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/a5687e91-60d9-4b7b-831c-85fbb993b6de.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid Thru Saturday, April 15, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":null,"activationActionText":"Tap to Redeem","barcodeString":null,"barcodeSymbology":null,"barcodeType":null,"brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/41496","offerDisclaimer":"Excludes Bundle & Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a custom frame. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","offerEnd":"04-15-17 11:59:59 PM","offerId":41496,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"20170402_Email_APRIL25DM","offerPublished":"03-30-17 01:43:00 PM","offerStart":"03-31-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"We accept competitor custom frame coupons.","offerTitle":"70% Off All Custom Frame Collections","offerType":"FRAMING OFFER","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/d9cc2503-1d64-4763-8d86-e5d16f6f01d3.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/d6b1184b-1187-4d7f-875b-07974f1c0eb6.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid Thru Saturday, April 15, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010028951&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010028951","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34112","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day. Not valid on:</strong> Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-23-17 11:59:59 PM","offerId":34112,"offerInstance":0,"offerInstructions":"Valid at Cape Coral Grand Opening store only.","offerName":"20170402_Social_CapeCoralGO","offerPublished":"03-16-17 03:00:00 PM","offerStart":"04-02-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/b985f284-2828-4969-82c5-a6e4e20bc109.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010028951","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010028951&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010028951","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 02, 2017 to Sunday, April 23, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010009415&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010009415","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34334","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON:</strong> Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-30-17 11:59:59 PM","offerId":34334,"offerInstance":0,"offerInstructions":"Valid at Cedar Park, TX Grand Opening store only.","offerName":"20170409_Social_CedarParkGO","offerPublished":"03-22-17 02:26:00 PM","offerStart":"04-09-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item","offerTitle":"40% OFF","offerType":"AORPI","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/7d38b145-7c9f-4a83-82f5-1c9811d0ce1c.jpg","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/90e9a678-27c6-4c16-8a94-82ab103d5b84.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010009415","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010009415&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010009415","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 09, 2017 to Sunday, April 30, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010014277&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010014277","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/34443","offerDisclaimer":"Limit one coupon per product. Limit one coupon of each type per day. <strong>NOT VALID ON:</strong> Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Polaroid products, Typecast typewriters, Hatchimals™, Christmas trees, Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"04-30-17 11:59:59 PM","offerId":34443,"offerInstance":0,"offerInstructions":"","offerName":"Blackstone Marketplace - April 2017","offerPublished":"04-01-17 12:00:00 AM","offerStart":"04-01-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"All Regular Price Purchases","offerTitle":"20% OFF","offerType":"ERPP","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/7772d2eb-0d4e-4a5a-a3c9-35b13e699a3c.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010014277","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010014277&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010014277","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Saturday, April 01, 2017 to Sunday, April 30, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010060673&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010060673","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33805","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, books, candy and beverages, gift cards; sale, clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Hatchimals, Christmas trees, The Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"05-06-17 11:59:59 PM","offerId":33805,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"Fuji coupon test 2","offerPublished":"03-02-17 04:23:00 PM","offerStart":"04-30-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Display Cases & Shadow Boxes","offerTitle":"50% Off Regular Price","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/f437090c-688d-48e8-929e-fd31cc3ec027.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010060673","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010060673&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010060673","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, April 30, 2017 to Saturday, May 06, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010086120&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010086120","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/33802","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: Everyday Value program; Doorbusters; technology, custom frames, cameras and printers, books, candy and beverages, gift cards; clearance or buy & get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Hatchimals, Christmas trees, The Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"05-13-17 11:59:59 PM","offerId":33802,"offerInstance":0,"offerInstructions":"Valid in US only.","offerName":"Fuji coupon test 1","offerPublished":"03-02-17 04:23:00 PM","offerStart":"05-07-17 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Display Cases & Shadow Boxes","offerTitle":"Extra 15% Off Sale Price","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/df79ebb6-6dbb-4958-9d8f-7479c52b2570.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010086120","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010086120&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010086120","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Sunday, May 07, 2017 to Saturday, May 13, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022409&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022409","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26239","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: All 4' & taller Christmas trees, The Elf on the Shelf, As Seen on TV, Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"07-31-17 11:59:59 PM","offerId":26239,"offerInstance":0,"offerInstructions":"Valid in US Only.","offerName":"20160927_Digital_StitchnWin10US","offerPublished":"09-27-16 12:00:00 AM","offerStart":"09-27-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item Bernat Blanket 300 gram / 10.5 oz size yarn","offerTitle":"10% Off","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/18a9d72f-55c6-41c0-bf22-408812535f33.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022409","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022409&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022409","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"10STCHNWIN16","barcodeImage":"","barcodeString":"10STCHNWIN16","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, September 09, 2016 to Monday, July 31, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022410&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022410","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26240","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: All 4' & taller Christmas trees, The Elf on the Shelf, As Seen on TV, Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"07-31-17 11:59:59 PM","offerId":26240,"offerInstance":0,"offerInstructions":"Valid in US Only.","offerName":"20160909_Digital_StitchnWin25US","offerPublished":"08-24-16 01:59:00 PM","offerStart":"09-09-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item Bernat Blanket 300 gram / 10.5 oz size yarn","offerTitle":"25% Off","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/6fa3d421-8256-4b95-8115-102d64076723.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022410","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022410&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022410","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"25STCHNWIN16","barcodeImage":"","barcodeString":"25STCHNWIN16","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, September 09, 2016 to Monday, July 31, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022411&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022411","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26241","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: All 4' & taller Christmas trees, The Elf on the Shelf, As Seen on TV, Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"07-31-17 11:59:59 PM","offerId":26241,"offerInstance":0,"offerInstructions":"Valid in US Only.","offerName":"20160909_Digital_StitchnWin50US","offerPublished":"08-24-16 01:59:00 PM","offerStart":"09-09-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"Any One Regular Price Item Bernat Blanket 300 gram / 10.5 oz size yarn","offerTitle":"50% Off","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/e8d7cc50-1a2a-417c-8ce5-f8279d4e103d.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022411","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022411&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022411","barcodeSymbology":"UPCA","barcodeType":36},{"barcodeDisplayString":"50STCHNWIN16","barcodeImage":"","barcodeString":"50STCHNWIN16","barcodeSymbology":"PLAINTEXT","barcodeType":98}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, September 09, 2016 to Monday, July 31, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022409&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022409","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26242","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: All 4' & taller Christmas trees, The Elf on the Shelf, As Seen on TV, Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products; special order custom floral arrangements, custom frames & materials & services; Wacom products, Brother, Sensu Brush & Stylus, 3D printer & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, colouring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"07-31-17 11:59:59 PM","offerId":26242,"offerInstance":0,"offerInstructions":"Valid in Canada only.","offerName":"20160909C_Digital_StitchnWin10CAN","offerPublished":"08-24-16 01:59:00 PM","offerStart":"09-09-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"One Regular Price Item Bernat Blanket 300 gram / 10.5 oz size yarn","offerTitle":"10% Off","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/96332782-f437-4eda-a733-f0a452bd5ad8.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022409","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022409&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022409","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, September 09, 2016 to Monday, July 31, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022410&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022410","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26243","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: All 4' & taller Christmas trees, The Elf on the Shelf, As Seen on TV, Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products; special order custom floral arrangements, custom frames & materials & services; Wacom products, Brother, Sensu Brush & Stylus, 3D printer & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, colouring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"07-31-17 11:59:59 PM","offerId":26243,"offerInstance":0,"offerInstructions":"Valid in Canada only.","offerName":"20160909C_Digital_StitchnWin25CAN","offerPublished":"08-24-16 01:59:00 PM","offerStart":"09-09-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"One Regular Price Item Bernat Blanket 300 gram / 10.5 oz size yarn","offerTitle":"25% Off","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/fbfdb7f9-b689-4040-84b4-33323dfb2dd1.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022410","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022410&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022410","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, September 09, 2016 to Monday, July 31, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022411&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022411","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26244","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day.</strong> Not valid on: All 4' & taller Christmas trees, The Elf on the Shelf, As Seen on TV, Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products; special order custom floral arrangements, custom frames & materials & services; Wacom products, Brother, Sensu Brush & Stylus, 3D printer & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, colouring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","offerEnd":"07-31-17 11:59:59 PM","offerId":26244,"offerInstance":0,"offerInstructions":"Valid in Canada only.","offerName":"20160909C_Digital_StitchnWin50CAN","offerPublished":"08-24-16 01:59:00 PM","offerStart":"09-09-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"One Regular Price Item Bernat Blanket 300 gram / 10.5 oz size yarn","offerTitle":"50% Off","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/acf35aab-5253-440e-bafe-0574155ef9df.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022411","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022411&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022411","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valid on Friday, September 09, 2016 to Monday, July 31, 2017","visibility":"public"},{"code":0,"message":null,"version":"2","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022410&symbology=36&text-margin=15&bar-height=60&display-text=true","activationActionText":null,"barcodeString":"40010022410","barcodeSymbology":"UPCA","barcodeType":"36","brandColor":null,"channel":"desktop","channelDetails":{"channelCode":"englishwebsite","channelType":"Web","name":"website","style":"Offer Viewer"},"clientid":1,"companyImage":"https://c815555.ssl.cf2.rackcdn.com/be7c8e47-e2ab-4e74-b6f5-1c00d90a78f9.png","consumerPOS":{"limitInterval":null,"redemptionCount":0,"redemptionLimit":0},"cpg":{"adultClass":null,"adultSubclass":null,"ageGateImage":null,"brandName":null,"cpgBrandId":0,"cpgName":null,"identityScheme":null,"isAdult":false,"isAuthenticated":false,"isOriginatingOffer":false,"warningAlgorithm":null},"desktopImage":"","displayStore":null,"doneMessage":null,"emailEnabled":false,"errorMsg":null,"featured":false,"geogating":false,"is_active":true,"is_multi":true,"limitedStores":false,"loyaltyCard":null,"nativeAd":null,"offer":"24be0a30-19c7-11e7-aeb8-abcb4d846284/offers/26246","offerDisclaimer":"<strong>Limité à un coupon par produit. Limité à un coupon de chaque type par jour.</strong> Offre non valide pour : tous les arbres de Noël de 4 pi. et plus, les produits « tels que vus à la télé », les articles du programme Bas prix de tous les jours, les offres à tout casser; les marques Silhouette, Cricut et Canon; les produits Polaroid; les commandes spéciales d’arrangements floraux, les encadrements sur mesure ainsi que les composantes et les services; les produits Wacom, les produits Brother, les pinceaux et stylets Sensu, les imprimantes 3D et les accessoires, le stylo 3D et les accessoires 3Doodler, les machines à coudre, les machines à écrire Typecast, les livres, les livres à colorier, les magazines, les boissons, les CD/DVD, les cartes-cadeaux et cartes de débit; les soldes, liquidations ou ventes « Achetez et recevez »; les frais de cours, d’événements, de fêtes d’anniversaire, de transport, de livraison ou d’installation. Le coupon imprimé doit être remis ou la copie électronique doit être balayée au moment de l'achat. Non applicable aux achats antérieurs. Offre limitée au stock disponible. Nul là où la loi l’interdit. Les exclusions peuvent changer. Voir un(e) membre de l'équipe en magasin pour plus de détails.","offerEnd":"07-31-17 11:59:59 PM","offerId":26246,"offerInstance":0,"offerInstructions":"Valide seulement au Canada.","offerName":"20160909Q_Digital_StitchnWin25FR","offerPublished":"08-24-16 02:09:00 PM","offerStart":"09-09-16 12:00:00 AM","offerStyle":"Coupon","offerSubtitle":"sur une pelote de laine Blanket de Bernat de 300 grammes à prix courant","offerTitle":"25 % de rabais","offerType":"BONUS","passbookEnabled":false,"peerOfferIds":[],"primaryImage":"","print":{"printDescription":null,"printEnabled":false,"printImage":null,"printInstructions":null,"printTitle":null,"smsEnabled":false,"totalPrintLimit":0,"totalPrintsRemaining":0},"printImage":"https://km-cdn.s3.amazonaws.com/static/offerImages/michaels/2ea61797-c7a3-4a1c-be0e-44a1d1850105.png","priority":1,"promotion":null,"receipt":null,"redemptionAssets":[{"barcodeDisplayString":"40010022410","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010022410&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeString":"40010022410","barcodeSymbology":"UPCA","barcodeType":36}],"redemptionTimerRemaining":null,"redirect":null,"secondaryImage":"","showNearbyStores":false,"smsEnabled":true,"storeListRadius":10,"storeSpecific":[],"tags":null,"timer":0,"totalOffersPurchased":0,"totalOffersRemaining":0,"validAtStores":[],"validDateDescription":"Valide du 9 septembre au 31 july 2017","visibility":"public"}],"summary":{"numCoupon":30,"numLoyaltyCard":0,"numNativeAd":0,"numPromotion":0,"numReceipt":0,"numRedirect":0,"tobacco":{"brands":[],"numOffersAvailable":0}}};            
var fromCoupons = false;
var couponsIndex = null;
var gFlexHeight = null;
var gFrmHeight = null;
var restrictions = {};
var gFlexOneAnimated = false;
var modifiedHeight = "145dp";
var MVCApp = MVCApp || {};
var gFlexRewardsHeight = null;
var gblOfferIDForAnalytics = "";
var gblOfferTypeForAnalytics = "";
var promo = [];
MVCApp.CouponsView = (function() {
    /**
     * @function
     *
     */
    function bindEvents() {
        frmCoupons.btnDone.onClick = hideCoupons;
        frmCoupons.btnSignIn.onClick = function() {
            MVCApp.getSignInController().load();
        };
        frmCoupons.btnSignUp.onClick = function() {
            var userObject = kony.store.getItem("userObj");
            if (userObject === "" || userObject === null) {
                isFromShoppingList = false;
                //              MVCApp.getSignInController().load();  
                MVCApp.getCreateAccountController().load();
            } else {
                MVCApp.getJoinRewardsController().load(userObject);
            }
        };
        frmCoupons.postShow = function() {
            MVCApp.Toolbox.common.requestReview();
            postShowAnimateCoupons();
        }
    }

    function show() {
        try {
            fromCoupons = true;
            frmCoupons.btnDone.onClick = hideCoupons;
            if (MVCApp.Toolbox.common.getRegion() == "US") {
                frmCoupons.btnSignUp.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.lblCreateAcc");
                frmCoupons.btnSignUp.skin = "sknBtnMontserratSemiBoldBgWhiteBlueBorder13px";
                frmCoupons.btnSignUp.focusSkin = "sknBtnMontserratSemiBoldBgWhiteBlueBorder13px";
            } else {
                frmCoupons.btnSignUp.skin = "sknBtnPrimaryWhite";
                frmCoupons.btnSignUp.focusSkin = "sknBtnPrimaryWhite";
                frmCoupons.btnSignUp.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.btnSignUp");
            }
            var language = kony.store.getItem("languageSelected");
            frmCoupons.flxCouponRewards.isVisible = true;
            frmCoupons.scroll.top = "98dp";
            frmCoupons.scroll.bottom = "0dp";
            /* if(language=="en" || language =="en_US"){
               frmCoupons.flxCouponRewards.isVisible = true;
               frmCoupons.scroll.top = "98dp";
               frmCoupons.scroll.bottom = "0dp";
             }else{
               frmCoupons.flxCouponRewards.isVisible = false;
               frmCoupons.scroll.doLayout = function(){
                 frmCoupons.scroll.top = "0dp";
                 frmCoupons.scroll.bottom = "0dp";
               };  
             }  */
            frmCoupons.btnSignIn.onClick = function() {
                MVCApp.getSignInController().load();
            };
            frmCoupons.btnSignUp.onClick = function() {
                var userObject = kony.store.getItem("userObj");
                if (userObject === "" || userObject === null) {
                    isFromShoppingList = false;
                    //           MVCApp.getSignInController().load();    
                    MVCApp.getCreateAccountController().load();
                } else {
                    MVCApp.getJoinRewardsController().load(userObject);
                }
            };
            var currFrm = kony.application.getCurrentForm();
            var userObject = kony.store.getItem("userObj");
            if (userObject === "" || userObject === null) {
                frmCoupons.lblRewardText.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.signInToViewRewards");
                frmCoupons.flxBarCodeWrap.isVisible = false;
                frmCoupons.flxNotUser.isVisible = true;
                frmCoupons.btnSignIn.isVisible = true;
                frmCoupons.btnSignUp.right = "51%";
                frmCoupons.btnSignUp.width = "46%";
            } else {
                var c_loyaltyPhoneNo = userObject.c_loyaltyPhoneNo || "";
                if (c_loyaltyPhoneNo !== "") {
                    frmCoupons.lblRewardText.text = userObject.first_name + " " + userObject.last_name + " REWARDS CARD";
                    frmCoupons.flxBarCodeWrap.isVisible = true;
                    frmCoupons.flxNotUser.isVisible = false;
                    frmCoupons.imgBarcode.src = MVCApp.serviceConstants.defaultLoyaltyCardBarcode + userObject.c_loyaltyMemberID || "";
                } else {
                    frmCoupons.lblRewardText.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.signInToViewRewards");
                    frmCoupons.flxNotUser.isVisible = true;
                    frmCoupons.btnSignIn.isVisible = false;
                    frmCoupons.btnSignUp.right = "35%";
                    frmCoupons.btnSignUp.width = "30%";
                    frmCoupons.btnSignUp.skin = "sknBtnPrimaryWhite";
                    frmCoupons.btnSignUp.focusSkin = "sknBtnPrimaryWhite";
                    frmCoupons.btnSignUp.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmMore.btnSignUp");
                    frmCoupons.flxBarCodeWrap.isVisible = false;
                }
            }
            frmCoupons.flxCloseBtnWrap.onClick = function() {
                frmCoupons.flxCouponDetailsPopup.isVisible = false;
            };
            frmCoupons.btnPopupClose.onClick = function() {
                frmCoupons.flxCouponDetailsPopup.isVisible = false;
            };
            var createInstanceObject = new bright.createInstance();
            var returnedValue = createInstanceObject.getBrightness();
            var returnedValueStr = returnedValue + "";
            MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValueStr);
            MVCApp.Toolbox.common.setBrightness("frmCoupons");
            frmCoupons.show();
        } catch (e) {
            kony.print("exception=====" + e);
            if (e) {
                TealiumManager.trackEvent("Exception While Displaying The Coupons Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function barcodeAssignmentForCoupons() {
        barcodeAssignment(gCouponsResponse);
    }

    function barcodeAssignment(response) {
        if (response != undefined && response != null && response != "") {
            //var offerInputString="";
            for (var i = 0; i < response.length; i++) {
                var promoCode = "";
                var barcode = "";
                if (response[i].hasOwnProperty("redemptionAssets")) {
                    if (response[i].redemptionAssets.length > 0) {
                        for (var k = 0; k < response[i].redemptionAssets.length; k++) {
                            if (response[i].redemptionAssets[k].barcodeSymbology == "PLAINTEXT") {
                                if (response[i].redemptionAssets[k].barcodeString != null && response[i].redemptionAssets[k].barcodeString != "null" && response[i].redemptionAssets[k].barcodeString != undefined) {
                                    promoCode = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.promocode") + " " + response[i].redemptionAssets[k].barcodeString;
                                } else {
                                    promoCode = "";
                                }
                            }
                        }
                        for (var k = 0; k < response[i].redemptionAssets.length; k++) {
                            var v_BarcodeSymbology = response[i].redemptionAssets[k].barcodeSymbology;
                            if (v_BarcodeSymbology == "UPCA" || v_BarcodeSymbology == "CODE128C") {
                                barcode = response[i].redemptionAssets[k].barcodeImage;
                            }
                        }
                    } else {
                        promoCode = "";
                        barcode = "";
                    }
                } else {
                    promoCode = "";
                    barcode = "";
                }
                //barcode = "";
                var availability = "";
                var isBarcode = true;
                var barcodeFlag = true;
                var leftAlignRestrictions = "5%";
                var showPromocode = false;
                if (promoCode.length > 0 && promoCode != null && promoCode != "null" && promoCode != undefined) {
                    showPromocode = true;
                } else {
                    showPromocode = false;
                }
                var showBarcode = false;
                if (barcode.length > 0 && barcode != null && barcode != "null" && barcode != undefined) {
                    showBarcode = true;
                } else {
                    showBarcode = false;
                }
                if (promoCode != "" && barcode != "") {
                    availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreAndOnline");
                    isBarcode = true;
                    barcodeFlag = true;
                    leftAlignRestrictions = "5%";
                } else if (promoCode == "" && barcode != "") {
                    availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly");
                    isBarcode = true;
                    barcodeFlag = true;
                    leftAlignRestrictions = "5%";
                } else if (promoCode != "" && barcode == "") {
                    availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                    isBarcode = true;
                    barcodeFlag = false;
                    leftAlignRestrictions = "30%";
                } else if (promoCode == "" && barcode == "") {
                    availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired");
                    isBarcode = true;
                    barcodeFlag = false;
                    leftAlignRestrictions = "30%";
                } else {
                    isBarcode = false;
                }
                var imageName = "imgBarcode2" + i;
                var flex = "flex2Inner2" + i;
                frmCoupons[flex][imageName].src = barcode;
                frmCoupons[flex][imageName].isVisible = showBarcode;
                frmCoupons[flex].isVisible = isBarcode;
            }
        }
    }
    /**
     * @function
     *
     */
    function coupons(couponsData) {
        try {
            //var serviceEnd = new Date().getTime();
            //kony.print("SERVICE END TIME::"+serviceEnd);  
            //var couponsData = {"opstatus_getConsumer":0,"code":"200","opstatus_getCoupons":0,"opstatus":0,"Offers":[{"jwt":"eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJjb21wdXRlLWVuZ2luZS1kZWZhdWx0LXNlcnZpY2VAbWljaGFlbHNhbmRyb2lkcGF5LTE3NDAxMi5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6Imdvb2dsZSIsInR5cCI6InNhdmV0b2FuZHJvaWRwYXkiLCJpYXQiOjE1MDA5MDEyMjAsInBheWxvYWQiOnsib2ZmZXJDbGFzc2VzIjpbXSwib2ZmZXJPYmplY3RzIjpbeyJiYXJjb2RlIjp7ImFsdGVybmF0ZVRleHQiOiIxMjM0NSIsImxhYmVsIjoiVXNlciBJZCIsInR5cGUiOiJ1cGNBIiwidmFsdWUiOiIxMjM0NTY3ODkwMTIifSwiY2xhc3NJZCI6IjMyMjA2MzM0NTIyOTg0MDEyODAuT2ZmZXJDbGFzcyIsImlkIjoiMzIyMDYzMzQ1MjI5ODQwMTI4MC5UZXN0MiIsInN0YXRlIjoiYWN0aXZlIiwidmVyc2lvbiI6IjEifV19LCJvcmlnaW5zIjpbImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3dhbGxldG9iamVjdHMvdjEvb2ZmZXJDbGFzcz9zdHJpY3RcdTAwM2R0cnVlIl19.CyasLZkaJPpqrWk1whzcR-HQBT09yjt9CHABQarR-kuqYYfcGuMc-ZJYB2e8NXDrbcgGNKrZn2Ygm2P8DOJoSdR44N61Y_pKKrTzWliSlvpjOJhgfnDe8o34F0Xy5m_-sWtVwW95eH2bAWxsSqTWe01Qb2wmq7u4D5qOL00FH4zGptFIo25wHlhgIDo401bZEr0T16_F7fUkvQc3Eep7Wu3VEdhcaPiUDB26t8BKFsJpLiEu8sY3Fv1_BXLcfEe8YIm1dbfP2WNM0NAxmG2CY0cI5T8mdMQGKVnQXe-k73DiBCVT99tK4Yl-b-9myvbHAlkihEQPIZhYPNdHoyUg5Q","offerSubtitle":"Any One Regular Price Item","redemptionAssets":[{"barcodeDisplayString":"40010096630","barcodeString":"40010096630","barcodeSymbology":"UPCA","barcodeType":36,"barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010096630&symbology=36&text-margin=15&bar-height=60&display-text=true"},{"barcodeDisplayString":"50MVS72417","barcodeString":"50MVS72417","barcodeSymbology":"PLAINTEXT","barcodeType":98,"barcodeImage":""}],"UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010096630&symbology=36&text-margin=15&bar-height=60&display-text=true","offerDisclaimer":"<strong>Limit one coupon per product. Limit one coupon of each type per day. NOT VALID ON:</strong> Everyday Value program, Doorbusters, technology, custom frames, glass and mats; custom floral arrangements, books, magazines, candy and beverages, gift cards; sale, clearance or buy &amp; get items; online-only products; classes, events, birthday parties; shipping, delivery or installation fees; Cricut, Silhouette, Brother, Canon and Polaroid products, Typecast typewriters, sewing machines, 3D printers, Hatchimals, Christmas trees, The Elf on the Shelf. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.","priority":"1","validDateDescription":"VALID ON SUN 07/23/17 THRU MON 07/24/17","offerEnd":"07-24-17 11:59:59 PM","offerType":"AORPI","offerID":"44246","offerInstructions":"Valid in US only.","offerTitle":"50% OFF","offerStart":"07-23-17 12:00:00 AM"},{"jwt":"eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJjb21wdXRlLWVuZ2luZS1kZWZhdWx0LXNlcnZpY2VAbWljaGFlbHNhbmRyb2lkcGF5LTE3NDAxMi5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6Imdvb2dsZSIsInR5cCI6InNhdmV0b2FuZHJvaWRwYXkiLCJpYXQiOjE1MDA5MDEyMjAsInBheWxvYWQiOnsib2ZmZXJDbGFzc2VzIjpbXSwib2ZmZXJPYmplY3RzIjpbeyJiYXJjb2RlIjp7ImFsdGVybmF0ZVRleHQiOiIxMjM0NSIsImxhYmVsIjoiVXNlciBJZCIsInR5cGUiOiJ1cGNBIiwidmFsdWUiOiIxMjM0NTY3ODkwMTIifSwiY2xhc3NJZCI6IjMyMjA2MzM0NTIyOTg0MDEyODAuT2ZmZXJDbGFzcyIsImlkIjoiMzIyMDYzMzQ1MjI5ODQwMTI4MC5UZXN0MiIsInN0YXRlIjoiYWN0aXZlIiwidmVyc2lvbiI6IjEifV19LCJvcmlnaW5zIjpbImh0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3dhbGxldG9iamVjdHMvdjEvb2ZmZXJDbGFzcz9zdHJpY3RcdTAwM2R0cnVlIl19.CyasLZkaJPpqrWk1whzcR-HQBT09yjt9CHABQarR-kuqYYfcGuMc-ZJYB2e8NXDrbcgGNKrZn2Ygm2P8DOJoSdR44N61Y_pKKrTzWliSlvpjOJhgfnDe8o34F0Xy5m_-sWtVwW95eH2bAWxsSqTWe01Qb2wmq7u4D5qOL00FH4zGptFIo25wHlhgIDo401bZEr0T16_F7fUkvQc3Eep7Wu3VEdhcaPiUDB26t8BKFsJpLiEu8sY3Fv1_BXLcfEe8YIm1dbfP2WNM0NAxmG2CY0cI5T8mdMQGKVnQXe-k73DiBCVT99tK4Yl-b-9myvbHAlkihEQPIZhYPNdHoyUg5Q","offerSubtitle":"We accept competitor custom frame coupons.","redemptionAssets":[],"UPC":"null","offerDisclaimer":"Excludes Bundle &amp; Save Packages, Custom Frame Express, expedited shipping and engraved plates. Must purchase a moulding from the collection(s) listed. May not be applied to prior purchases or combined with any other coupon, sale or discount. See a Certified Framing Expert for details.","priority":"5","validDateDescription":"VALID ON SUN 07/23/17 THRU SAT 07/29/17","offerEnd":"07-29-17 11:59:59 PM","offerType":"FRAMING OFFER","offerID":"44118","offerInstructions":"Valid in US only. Offers may vary by location.","offerTitle":"60% Off Plus Get An Extra 10% Off Sale Price Hampton™ & Oxford Street™ Custom Frame Collections","offerStart":"07-23-17 12:00:00 AM"}],"message":"success","consumer_identity":"10e216f0-7070-11e7-84bd-ff56783e1dbe","httpStatusCode":200};
            //var couponsDataz = {"httpStatusCode":200,"opstatus":0,"code":"200","Offers":[{"redemptionAssets":[{"barcodeSymbology":"UPCA","barcodeString":"40010043702","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010043702&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeType":36,"barcodeDisplayString":"40010043702"}],"offerDisclaimer":"Limité à un coupon par produit. Limité à un coupon de chaque type par jour. <strong>Offre non valide pour</strong> : les articles du programme Bas prix de tous les jours; les offres à tout casser; les articles technologiques, les cadres sur mesure, les passe-partout et le verre; les livres, les magazines, les bonbons et les boissons, les cartes-cadeaux, les soldes, liquidations ou ventes « Achetez et recevez »; les produits en ligne seulement; les frais de cours, d’événements, de fêtes d’anniversaire, de transport, de livraison ou d’installation; les produits Cricut, Silhouette, Brother, Canon et Polaroid, les machines à écrire Typecast, les machines à coudre, les imprimantes 3D, les produits Hatchimals, les arbres de Noël et les produits The Elf on the Shelf. Non applicable aux achats antérieurs. Offre limitée aux stocks disponibles. Nul là où la loi l’interdit. Les exclusions peuvent changer. Voir un(e) membre de l’équipe en magasin pour plus de détails.","validDateDescription":"Valide du 12 mai au 18 mai 2017","offerType":"BONUS","offerTitle":"30 % de rabais","offerSubtitle":"sur les autocollants Paper House et Jolee's à prix courant","priority":"3","offerInstructions":"Valide seulement au Canada.","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010043702&symbology=36&text-margin=15&bar-height=60&display-text=true"},{"redemptionAssets":[{"barcodeSymbology":"UPCA","barcodeString":"40010081048","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010081048&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeType":36,"barcodeDisplayString":"40010081048"}],"offerDisclaimer":"Limité à un coupon par produit. Limité à un coupon de chaque type par jour. <strong>Offre non valide pour</strong> : les articles du programme Bas prix de tous les jours; les offres à tout casser; les articles technologiques, les cadres sur mesure, les passe-partout et le verre; les livres, les magazines, les bonbons et les boissons, les cartes-cadeaux, les soldes, liquidations ou ventes « Achetez et recevez »; les produits en ligne seulement; les frais de cours, d’événements, de fêtes d’anniversaire, de transport, de livraison ou d’installation; les produits Cricut, Silhouette, Brother, Canon et Polaroid, les machines à écrire Typecast, les machines à coudre, les imprimantes 3D, les produits Hatchimals, les arbres de Noël et les produits The Elf on the Shelf. Non applicable aux achats antérieurs. Offre limitée aux stocks disponibles. Nul là où la loi l’interdit. Les exclusions peuvent changer. Voir un(e) membre de l’équipe en magasin pour plus de détails.","validDateDescription":"Valide du 17 mai au 18 mai 2017","offerType":"BONUS","offerTitle":"40 % de rabais","offerSubtitle":"sur les autocollants Paper House et Jolee's à prix courant","priority":"3","offerInstructions":"Valide seulement au Canada.","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010081048&symbology=36&text-margin=15&bar-height=60&display-text=true"},{"redemptionAssets":[{"barcodeSymbology":"UPCA","barcodeString":"40010048232","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010048232&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeType":36,"barcodeDisplayString":"40010048232"}],"offerDisclaimer":"<strong>Limité à un coupon par produit. Limité à un coupon de chaque type par jour. OFFRE NON VALIDE POUR :</strong> les articles du programme Bas prix de tous les jours; les offres à tout casser; les articles technologiques, les cadres sur mesure, les passe-partout et le verre; les livres, les magazines, les bonbons et les boissons, les cartes-cadeaux, les liquidations ou ventes \" Achetez et recevez \"; les produits en ligne seulement; les frais de cours, d'événements, de fêtes d'anniversaire, de transport, de livraison ou d'installation; les produits Cricut, Silhouette, Brother, Canon et Polaroid; les machines à écrire Typecast, les machines à coudre, les imprimantes 3D, les produits Hatchimals, les arbres de Noël et les produits The Elf on the Shelf. Non applicable aux achats antérieurs. Offre limitée au stock disponible. Nul là où la loi l'interdit. Les exclusions peuvent changer. Voir un(e) membre de l'équipe en magasin pour plus de détails.","validDateDescription":"Valide du 17 mai au 18 mai 2017","offerType":"BONUS","offerTitle":"15 % de rabais","offerSubtitle":"sur les ensembles tendance en verre pour l'été d’Ashland à prix courant et de solde","priority":"3","offerInstructions":"Valide seulement au Canada.","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010048232&symbology=36&text-margin=15&bar-height=60&display-text=true"},{"redemptionAssets":[{"barcodeSymbology":"UPCA","barcodeString":"40010080097","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010080097&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeType":36,"barcodeDisplayString":"40010080097"}],"offerDisclaimer":"<strong>Limité à un coupon par produit. Limité à un coupon de chaque type par jour. OFFRE NON VALIDE POUR :</strong> les articles du programme Bas prix de tous les jours; les offres à tout casser; les articles technologiques, les cadres sur mesure, les passe-partout et le verre; les livres, les magazines, les bonbons et les boissons, les cartes-cadeaux, les liquidations ou ventes \" Achetez et recevez \"; les produits en ligne seulement; les frais de cours, d'événements, de fêtes d'anniversaire, de transport, de livraison ou d'installation; les produits Cricut, Silhouette, Brother, Canon et Polaroid; les machines à écrire Typecast, les machines à coudre, les imprimantes 3D, les produits Hatchimals, les arbres de Noël et les produits The Elf on the Shelf. Non applicable aux achats antérieurs. Offre limitée au stock disponible. Nul là où la loi l'interdit. Les exclusions peuvent changer. Voir un(e) membre de l'équipe en magasin pour plus de détails.","validDateDescription":"Valide du 17 mai au 18 mai 2017","offerType":"BONUS","offerTitle":"15 % de rabais","offerSubtitle":"sur les moules de cuisson de Celebrate It et Wilton à prix courant et de solde","priority":"3","offerInstructions":"Valide seulement au Canada.","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010080097&symbology=36&text-margin=15&bar-height=60&display-text=true"},{"redemptionAssets":[{"barcodeSymbology":"UPCA","barcodeString":"40010080108","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010080108&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeType":36,"barcodeDisplayString":"40010080108"}],"offerDisclaimer":"<strong>Limité à un coupon par produit. Limité à un coupon de chaque type par jour. OFFRE NON VALIDE POUR :</strong> paint, brushes, les articles du programme Bas prix de tous les jours; les offres à tout casser; les articles technologiques, les cadres sur mesure, les passe-partout et le verre; les livres, les magazines, les bonbons et les boissons, les cartes-cadeaux, les liquidations ou ventes \" Achetez et recevez \"; les produits en ligne seulement; les frais de cours, d'événements, de fêtes d'anniversaire, de transport, de livraison ou d'installation; les produits Cricut, Silhouette, Brother, Canon et Polaroid; les machines à écrire Typecast, les machines à coudre, les imprimantes 3D, les produits Hatchimals, les arbres de Noël et les produits The Elf on the Shelf. Non applicable aux achats antérieurs. Offre limitée au stock disponible. Nul là où la loi l'interdit. Les exclusions peuvent changer. Voir un(e) membre de l'équipe en magasin pour plus de détails.","validDateDescription":"Valide du 17 mai au 18 mai 2017","offerType":"BONUS","offerTitle":"15 % de rabais","offerSubtitle":"sur l’ameublement et les décorations prêtes pour la finition Créez-le d’ArtMinds à prix courant et de solde.  Exclut la peinture et les pinceaux.","priority":"3","offerInstructions":"Valide seulement au Canada.","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010080108&symbology=36&text-margin=15&bar-height=60&display-text=true"},{"redemptionAssets":[{"barcodeSymbology":"UPCA","barcodeString":"40010039117","barcodeImage":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010039117&symbology=36&text-margin=15&bar-height=60&display-text=true","barcodeType":36,"barcodeDisplayString":"40010039117"}],"offerDisclaimer":"Limité à un coupon par produit. Limité à un coupon de chaque type par jour. <strong>Offre non valide pour :</strong> les articles du programme Bas prix de tous les jours; les offres à tout casser; les articles technologiques, les cadres sur mesure, les passe-partout et le verre; les livres, les magazines, les bonbons et les boissons, les cartes-cadeaux, les soldes, liquidations ou ventes « Achetez et recevez »; les produits en ligne seulement; les frais de cours, d’événements, de fêtes d’anniversaire, de transport, de livraison ou d’installation; les produits Cricut, Silhouette, Brother, Canon et Polaroid, les machines à écrire Typecast, les machines à coudre, les imprimantes 3D, les produits Hatchimals, les arbres de Noël et les produits The Elf on the Shelf. Non applicable aux achats antérieurs. Offre limitée aux stocks disponibles. Nul là où la loi l’interdit. Les exclusions peuvent changer. Voir un(e) membre de l’équipe en magasin pour plus de détails.","validDateDescription":"Valide du 15 mai au 18 mai 2017","offerType":"AORPI","offerTitle":"40 % de rabais","offerSubtitle":"sur un article à prix courant","priority":"1","offerInstructions":"Valide seulement au Canada.","UPC":"https://bar.kouponmedia.com/barcode/linear.aspx?code=40010039117&symbology=36&text-margin=15&bar-height=60&display-text=true"}]};
            bindEvents();
            var result = couponsData;
            gblOfferIDForAnalytics = "";
            gblOfferTypeForAnalytics = "";
            var language = kony.store.getItem("languageSelected");
            kony.print("language----" + language);
            var currFrm = kony.application.getCurrentForm();
            frmCoupons.flxCouponRewards.doLayout = doLayoutCallBackFlex;
            frmCoupons.scroll.doLayout = doLayoutCallBackScroll;
            frmCoupons.flxCouponDetailsPopup.isVisible = false;
            if (result.hasOwnProperty("Offers")) {
                if (result.Offers.length > 0) {
                    frmCoupons.flxCouponContents.scroll.removeAll();
                    show();
                    var response = result.Offers;
                    setDataToDynamicFlexes(currFrm, response);
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                    getCouponAnalytics(response);
                } else {
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                    MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noCouponsAvailableForSelectedStore"), "");
                }
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.serviceCurrentlyDown"), "", constants.ALERT_TYPE_CONFIRMATION, GotoCoupons, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnRetry"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnCancel"));
            }
        } catch (e) {
            kony.print(e);
            if (e) {
                TealiumManager.trackEvent("Exception While Loading Coupons Data On To The UI", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function setDataToDynamicFlexes(currFrm, response) {
        try {
            if (response != undefined && response != null && response != "") {
                gCouponsResponse = response;
                var offerInputString = "";
                for (var i = 0; i < response.length; i++) {
                    kony.print("response----i---" + i + "--value--" + JSON.stringify(response[i]));
                    var offerID = response[i].offerID;
                    if (i == response.length - 1) {
                        offerInputString = offerInputString + offerID;
                    } else {
                        offerInputString = offerID + "," + offerInputString;
                    }
                    var percent = "";
                    var offTitle = response[i].offerTitle;
                    offTitle = offTitle.toUpperCase();
                    var replaceOffwithZ;
                    if (offTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off")) > 0) {
                        replaceOffwithZ = response[i].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off"), "Z");
                        if (replaceOffwithZ.indexOf("Z") > 0) {
                            percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off");
                        }
                    }
                    if (response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff")) > 0) {
                        replaceOffwithZ = response[i].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff"), "Z");
                        if (replaceOffwithZ.indexOf("Z") > 0) {
                            percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff");
                        }
                    }
                    if (response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff")) > 0) {
                        replaceOffwithZ = response[i].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff"), "Z");
                        if (replaceOffwithZ.indexOf("Z") > 0) {
                            percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff");
                        }
                    }
                    if (replaceOffwithZ != null && replaceOffwithZ != undefined && replaceOffwithZ.split("Z")[1] != null && replaceOffwithZ.split("Z")[1] != undefined) {
                        offTitle = replaceOffwithZ.split("Z")[1];
                    }
                    if (response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off")) <= 0 && response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff")) <= 0 && response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff")) <= 0) {
                        percent = "";
                        offTitle = response[i].offerTitle;
                    }
                    kony.print("offTitle----" + offTitle);
                    var showSubOfferTitle = false;
                    var showPercent = false;
                    if (percent.length > 0) {
                        showPercent = true;
                    } else {
                        showPercent = false;
                    }
                    if (offTitle.length > 0) {
                        showSubOfferTitle = true;
                    } else {
                        showSubOfferTitle = false;
                    }
                    var promoCode = "";
                    var barcode = "";
                    if (response[i].hasOwnProperty("redemptionAssets")) {
                        if (response[i].redemptionAssets.length > 0) {
                            for (var k = 0; k < response[i].redemptionAssets.length; k++) {
                                if (response[i].redemptionAssets[k].barcodeSymbology == "PLAINTEXT") {
                                    if (response[i].redemptionAssets[k].barcodeString !== null && response[i].redemptionAssets[k].barcodeString !== "null" && response[i].redemptionAssets[k].barcodeString !== undefined && response[i].redemptionAssets[k].barcodeString !== "UNAVAILABLE") {
                                        promoCode = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.promocode") + " " + response[i].redemptionAssets[k].barcodeString;
                                    } else {
                                        promoCode = "";
                                    }
                                }
                            }
                            for (var k = 0; k < response[i].redemptionAssets.length; k++) {
                                var v_BarcodeSymbology = response[i].redemptionAssets[k].barcodeSymbology;
                                if (v_BarcodeSymbology == "UPCA" || v_BarcodeSymbology == "CODE128C") {
                                    barcode = response[i].redemptionAssets[k].barcodeImage;
                                }
                            }
                        } else {
                            promoCode = "";
                            barcode = "";
                        }
                    } else {
                        promoCode = "";
                        barcode = "";
                    }
                    //barcode = "";
                    var availability = "";
                    var isBarcode = true;
                    var barcodeFlag = true;
                    var leftAlignRestrictions = "5%";
                    var showPromocode = false;
                    if (promoCode.length > 0 && promoCode != null && promoCode != "null" && promoCode != undefined) {
                        showPromocode = true;
                    } else {
                        showPromocode = false;
                    }
                    var showBarcode = false;
                    if (barcode.length > 0 && barcode != null && barcode != "null" && barcode != undefined) {
                        showBarcode = true;
                    } else {
                        showBarcode = false;
                    }
                    if (promoCode != "" && barcode != "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreAndOnline");
                        isBarcode = true;
                        barcodeFlag = true;
                        leftAlignRestrictions = "5%";
                    } else if (promoCode == "" && barcode != "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly");
                        isBarcode = true;
                        barcodeFlag = true;
                        leftAlignRestrictions = "5%";
                    } else if (promoCode != "" && barcode == "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                        isBarcode = true;
                        barcodeFlag = false;
                        leftAlignRestrictions = "30%";
                    } else if (promoCode == "" && barcode == "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired");
                        isBarcode = true;
                        barcodeFlag = false;
                        leftAlignRestrictions = "30%";
                    }
                    //alert(availability);
                    //barcode = "";
                    restrictions.lblOfferTitle1Restrictions = percent;
                    restrictions.lblOfferTitle2Restrictions = offTitle;
                    restrictions.lblOfferSubTitleRestrictions = response[i].offerSubtitle;
                    restrictions.lblValidityRestrictions = response[i].validDateDescription;
                    restrictions.imgBarcodeRestrictions = barcode;
                    restrictions.rchTxtRetrictions = response[i].offerDisclaimer;
                    restrictions.lblPromocodeRestrictions = promoCode;
                    var flxCouponMainContainer = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "focusSkin": "sknFlexCoupons",
                        "id": "flxCouponMainContainer" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "slFbox",
                        "top": "10dp",
                        "width": "90%",
                        "centerX": "50%"
                    }, {}, {});
                    flxCouponMainContainer.setDefaultUnit(kony.flex.DP);
                    var flxCoupon = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "focusSkin": "sknFlexBgWhiteBorderR2",
                        "id": "flxCoupon_" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "skin": "slFbox",
                        "top": "2dp",
                        "width": "100%",
                        "zIndex": 1
                            //"onClick": animateFlexCoupons
                    }, {}, {});
                    flxCoupon.setDefaultUnit(kony.flex.DP);
                    var flxCoupon1Info = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        //"height": "145dp",
                        "id": "flxCoupon1Info" + i,
                        "isVisible": false,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": 0,
                        "skin": "sknFlexCoupons",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 20
                    }, {}, {});
                    flxCoupon1Info.setDefaultUnit(kony.flex.DP);
                    var flexCoupons1Inner = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "id": "flexCoupons1Inner" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "slFbox",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexCoupons1Inner.setDefaultUnit(kony.flex.DP);
                    var lblAvailability = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblAvailability" + i,
                        "isVisible": true,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": availability,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "12dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var flexInner11 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "id": "flexInner11" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_HORIZONTAL,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "10dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexInner11.setDefaultUnit(kony.flex.DP);
                    var lblOfferTitle1 = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitle1" + i,
                        "isVisible": showPercent,
                        "left": "0dp",
                        "skin": "sknLblGothamBold44pxRed",
                        "text": percent,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "0dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    flexInner11.add(lblOfferTitle1);
                    var lblOfferTitle2 = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitle2" + i,
                        "isVisible": showSubOfferTitle,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": offTitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [2, 0, 2, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferSubTitle = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferSubTitle" + i,
                        "isVisible": true,
                        "left": "0dp",
                        "skin": "sknLblGothamBold20pxRed",
                        "text": response[i].offerSubtitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [2, 0, 2, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblValidity = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblValidity" + i,
                        "isVisible": true,
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": response[i].validDateDescription,
                        "textStyle": {
                            "letterSpacing": 0,
                            "lineSpacing": 10,
                            "strikeThrough": false
                        },
                        "top": "5dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblPromoCode = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblPromoCode" + i,
                        "isVisible": showPromocode,
                        "skin": "sknLblGothamBold16pxBlack",
                        "text": promoCode,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "5dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    flexCoupons1Inner.add(lblAvailability, flexInner11, lblOfferTitle2, lblOfferSubTitle, lblValidity);
                    flxCoupon1Info.add(flexCoupons1Inner);
                    var flxCoupon2Info = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "id": "flxCoupon2Info" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "sknFlexCoupons",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 20
                    }, {}, {});
                    flxCoupon2Info.setDefaultUnit(kony.flex.DP);
                    var flex2Inner1 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "id": "flex2Inner1" + i,
                        "isVisible": isBarcode,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "slFbox",
                        "top": "10dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flex2Inner1.setDefaultUnit(kony.flex.DP);
                    var flex2Inner11 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "id": "flex2Inner11" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "2dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flex2Inner11.setDefaultUnit(kony.flex.DP);
                    var lblAvailabilityHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblAvailabilityHidden" + i,
                        "isVisible": true,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": availability,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "12dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferTitleHidden1 = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitleHidden1" + i,
                        "isVisible": showPercent,
                        "left": "0dp",
                        "skin": "sknLblGothamBold30pxRed",
                        "text": percent,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    flex2Inner11.add(lblOfferTitleHidden1);
                    var lblOfferTitleHidden2 = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitleHidden2" + i,
                        "isVisible": showSubOfferTitle,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": offTitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferSubTitleHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferSubTitleHidden" + i,
                        "isVisible": true,
                        "left": "0dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": response[i].offerSubtitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblValidityHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblValidityHidden" + i,
                        "isVisible": true,
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": response[i].validDateDescription,
                        "textStyle": {
                            "letterSpacing": 0,
                            "lineSpacing": 5,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblRegionAvailibility = new kony.ui.RichText({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblRegionAvailibility" + i,
                        "isVisible": true,
                        "skin": "sknRchTxtGothamBold14pxBlack",
                        "text": response[i].offerInstructions,
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
                    });
                    var lblPromoCodeHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblPromoCodeHidden" + i,
                        "isVisible": showPromocode,
                        "skin": "sknLblGothamBold16pxBlack",
                        "text": promoCode,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "5dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    flex2Inner1.add(lblAvailabilityHidden, flex2Inner11, lblOfferTitleHidden2, lblOfferSubTitleHidden, lblValidityHidden, lblPromoCodeHidden);
                    var flex2Inner2 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "id": "flex2Inner2" + i,
                        "isVisible": isBarcode,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "left": "0dp",
                        "skin": "slFbox",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flex2Inner2.setDefaultUnit(kony.flex.DP);
                    var imgBarcode2 = new kony.ui.Image2({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "height": "85dp",
                        "id": "imgBarcode2" + i,
                        "isVisible": showBarcode,
                        "imageWhenFailed": "notavailable_1080x142.png",
                        "imageWhileDownloading": "white_1080x142.png",
                        "skin": "slImage",
                        "src": barcode,
                        "top": "0dp",
                        "width": "80%",
                        "zIndex": 1
                    }, {
                        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {});
                    var flexRestrict = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_NONE,
                        "clipBounds": true,
                        "height": "35dp",
                        "id": "flexRestrict" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "0dp",
                        "bottom": "20dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexRestrict.setDefaultUnit(kony.flex.DP);
                    var flexBtns = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_NONE,
                        "clipBounds": true,
                        "height": "35dp",
                        "id": "flexBtns" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "20dp",
                        "bottom": "20dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexBtns.setDefaultUnit(kony.flex.DP);
                    var btnRestrictions = new kony.ui.Button({
                        "focusSkin": "sknBtnCouponExpandFoc",
                        "height": "28dp",
                        "id": "btnRestrictions_" + i,
                        "isVisible": isBarcode,
                        "left": "5%",
                        "skin": "sknBtnCouponExpand",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.viewRestrictions"),
                        "top": "0dp",
                        "zIndex": 5,
                        "width": "90%", //kony.flex.USE_PREFFERED_SIZE,
                        "onClick": showRestrictions
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    var btnAddToCart = new kony.ui.Button({
                        "focusSkin": "sknBtnBgRedWhiteFont11pxMonteserrat",
                        "height": "28dp",
                        "id": "btnAddToCart_" + i,
                        "isVisible": barcodeFlag,
                        "left": "51%",
                        "skin": "sknBtnBgRedWhiteFont11pxMonteserrat",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.addToCart"),
                        "top": "0dp",
                        "zIndex": 5,
                        "width": "44%", //kony.flex.USE_PREFFERED_SIZE,
                        "onClick": applyCouponToCart
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    var btnAddToWallet = new kony.ui.Button({
                        "focusSkin": "sknBtnCouponExpandFoc",
                        "height": "28dp",
                        "id": "btnAddToWallet_" + i,
                        "isVisible": barcodeFlag,
                        "left": leftAlignRestrictions,
                        "skin": "sknBtnCouponExpand",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.addToWallet"),
                        "top": "0dp",
                        "width": "44%",
                        "zIndex": 5,
                        "onClick": addToWalletOnClick
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    if (MVCApp.Toolbox.common.getRegion() == "US") {
                        if (promoCode === "") {
                            btnRestrictions.left = "51%";
                            btnRestrictions.width = "44%";
                            btnAddToWallet.left = "5%";
                            btnAddToWallet.isVisible = true;
                            flexBtns.add(btnAddToWallet, btnRestrictions);
                            flex2Inner2.add(imgBarcode2, flexBtns);
                        } else if (promoCode !== "") {
                            btnAddToWallet.isVisible = true;
                            btnAddToWallet.left = "5%";
                            btnAddToCart.isVisible = true;
                            if (MVCApp.Toolbox.common.lookForCoupon(promoCode.split(" ")[2])) {
                                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                                    btnAddToCart.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.applied");
                                    btnAddToCart.onClick = function() {};
                                    btnAddToCart.skin = "sknBtnCouponApply";
                                    btnAddToCart.focusSkin = "sknBtnCouponApply";
                                } else {
                                    btnAddToCart.info = {
                                        "promoCode": promoCode
                                    };
                                }
                            } else {
                                btnAddToCart.info = {
                                    "promoCode": promoCode
                                };
                            }
                            flexRestrict.add(btnRestrictions);
                            flexBtns.add(btnAddToWallet, btnAddToCart);
                            flex2Inner2.add(imgBarcode2, flexBtns, flexRestrict);
                        }
                    } else {
                        btnRestrictions.left = "51%";
                        btnRestrictions.width = "44%";
                        flexBtns.add(btnAddToWallet, btnRestrictions);
                        flex2Inner2.add(imgBarcode2, flexBtns);
                    }
                    var flex2Inner3 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "id": "flex2Inner3" + i,
                        "isVisible": !isBarcode,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "10dp",
                        "width": "100%",
                        "zIndex": 2
                    }, {}, {});
                    flex2Inner3.setDefaultUnit(kony.flex.DP);
                    var flexNBCTitle = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "id": "flexNBCTitle" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "2dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexNBCTitle.setDefaultUnit(kony.flex.DP);
                    var lblOfferTitleNBC = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitleNBC" + i,
                        "isVisible": showPercent,
                        "left": "0dp",
                        "skin": "sknLblGothamBold30pxRed",
                        "text": percent,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false,
                        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
                    });
                    flexNBCTitle.add(lblOfferTitleNBC);
                    var lblOfferTitle2NBC = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitle2NBC" + i,
                        "isVisible": showSubOfferTitle,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": offTitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferSubTitleNBC = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferSubTitleNBC" + i,
                        "isVisible": true,
                        "left": "0dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": response[i].offerSubtitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblNoBarcode = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblNoBarcode" + i,
                        "isVisible": true, //!isBarcode,
                        "left": "5dp",
                        "skin": "sknLblBlackPx14",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired"),
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblNoBarCodeDetails = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblNoBarCodeDetails" + i,
                        "isVisible": true, //!isBarcode,
                        "left": "15dp",
                        "skin": "sknLblR194G7B36Px14",
                        "text": response[i].offerSubtitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "11dp",
                        "width": "94%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblNoBarCodeDetailsValidity = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblNoBarCodeDetailsValidity" + i,
                        "isVisible": true, //!isBarcode,
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": response[i].validDateDescription,
                        "textStyle": {
                            "letterSpacing": 0,
                            "lineSpacing": 5,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblValidityRegionNBC = new kony.ui.RichText({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblValidityRegionNBC" + i,
                        "isVisible": true,
                        "skin": "sknLblGothamBold16pxBlack",
                        "text": response[i].offerInstructions,
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
                    });
                    var lblPromoCodeNBC = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblPromoCodeNBC" + i,
                        "isVisible": showPromocode,
                        "skin": "sknLblGothamBold16pxBlack",
                        "text": promoCode,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "5dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var flexBtnsNBC = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_NONE,
                        "clipBounds": true,
                        "height": "35dp",
                        "id": "flexBtnsNBC" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "20dp",
                        "bottom": "20dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexBtnsNBC.setDefaultUnit(kony.flex.DP);
                    var btnRestrictionsNBC = new kony.ui.Button({
                        "focusSkin": "sknBtnCouponExpandFoc",
                        "height": "28dp",
                        "id": "btnRestrictionsNBC_" + i,
                        "isVisible": false,
                        "left": "3%",
                        "skin": "sknBtnCouponExpand",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.viewRestrictions"),
                        "top": "0dp",
                        "width": "45%",
                        "zIndex": 5,
                        "onClick": showRestrictions
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    var btnAddToWalletNBC = new kony.ui.Button({
                        "focusSkin": "sknBtnCouponExpandFoc",
                        "height": "28dp",
                        "id": "btnAddToWalletNBC_" + i,
                        "isVisible": false,
                        "left": "52%",
                        "skin": "sknBtnCouponExpand",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.addToWallet"),
                        "top": "0dp",
                        "width": "45%",
                        "zIndex": 5,
                        "onClick": addToWalletOnClick
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    flexBtnsNBC.add(btnRestrictionsNBC, btnAddToWalletNBC);
                    flex2Inner3.add(flexNBCTitle, lblOfferTitle2NBC, lblOfferSubTitleNBC, lblNoBarCodeDetailsValidity, lblNoBarcode, flexBtnsNBC);
                    var setanimateFlexId = new kony.ui.Label({
                        "centerX": "50%",
                        "id": "setanimateFlexId_" + i,
                        "isVisible": false, //!isBarcode,
                        "left": "15dp",
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": "false",
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "11dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    flxCoupon2Info.add(flex2Inner1, flex2Inner2, flex2Inner3);
                    flxCoupon.add(flxCoupon1Info, flxCoupon2Info, setanimateFlexId);
                    flxCouponMainContainer.add(flxCoupon);
                    frmCoupons.scroll.add(flxCouponMainContainer);
                }
                MVCApp.getHomeController().getCouponAnalytics(offerInputString);
            }
        } catch (e) {
            alert("Something unexpected happened. Please try again later.");
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            if (e) {
                TealiumManager.trackEvent("Exception While Dynamically Creating The Coupons UI", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function doLayoutCallBackFlex() {
        var deviceinfo = kony.os.deviceInfo();
        gFrmHeight = deviceinfo["deviceHeight"];
        var currFrm = kony.application.getCurrentForm();
        try {
            gFlexHeight = frmCoupons.flxCouponRewards.frame["height"];
        } catch (e) {
            kony.print("Exception:::" + e);
            if (e) {
                TealiumManager.trackEvent("UI Exception On Coupons Screen", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function doLayoutCallBackScroll() {
        var currFrm = kony.application.getCurrentForm();
        //kony.print("gFlexHeight---"+gFlexHeight);
        frmCoupons.scroll.top = gFlexHeight;
        frmCoupons.scroll.bottom = "10dp";
    }

    function test() {
        for (var i = 0; i < 2; i++) {
            var skin;
            var top;
            if (i % 2 == 0) {
                skin = "black";
                top = 0;
            } else {
                skin = "green";
                top = 30;
            }
            var flex = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "20dp",
                "id": "flex" + i,
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "skin": skin,
                "width": "100%",
                "zIndex": 1,
                //"top" : top+"dp"
            }, {}, {});
            flex.setDefaultUnit(kony.flex.DP);
            frmHome.flexCouponContainer.scroll.add(flex);
        }
    }
    /**
     * @function
     *
     */
    function hideCoupons() {
        if (gblIsNewBrightnessValuePresent) {
            gblIsNewBrightnessValuePresent = false;
            var createInstanceObject = new bright.createInstance();
            var returnedValueStr = gblNewBrightnessValueStoreMode + "";
            MVCApp.Toolbox.common.setDefaultBrightnessValue(returnedValueStr);
            createInstanceObject.setBrightness(returnedValueStr);
        }
        if (frmObject.flxSearchOverlay) {
            frmObject.flxSearchOverlay.setVisibility(false);
        }
        if (frmObject.flxVoiceSearch) {
            frmObject.flxVoiceSearch.setVisibility(false);
        }
        if (frmObject.id === "frmMore") {
            if (gblBrightnessfrmSignInPage === true) {
                MVCApp.MoreView().brightnesshandlingfrmMore();
            } else {
                gblCouponFormName = "frmCoupons";
                gblCouponPageValue = true;
                gblBrightNessMoreValue = gblBrightNessValue;
            }
            MVCApp.getMoreController().load();
        } else if (frmObject.id === "frmHome") {
            MVCApp.getHomeController().load();
        } else if (frmObject.id === "frmProductCategory") {
            var currentProdCategory = kony.store.getItem("currentProdCategory");
            if (currentProdCategory) {
                MVCApp.getProductCategoryController().load(currentProdCategory);
                kony.store.setItem("currentProdCategory", {});
            }
        } else if (frmObject.id === "frmShoppingList") {
            frmShoppingList.flxSearchOverlay.setVisibility(false);
            MVCApp.getShoppingListController().getProductListItems();
        } else if (frmObject.id === "frmPDP") {
            frmPDP.flxSearchOverlay.setVisibility(false);
            if (frmProductsList.segProductsList.selectedRowItems) {
                MVCApp.ProductsListView().getSelectedItem();
            } else {
                if (kony.store.getItem("searchedProdId")) {
                    var searchQuery = kony.store.getItem("searchedProdId");
                    kony.store.setItem("searchedProdId", "");
                    MVCApp.getProductsListController().load(searchQuery, true);
                } else {
                    frmObject.show();
                }
            }
        } else if (frmObject.id === "frmCartView") {
            MVCApp.getCartController().loadWebView(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileCartWebUrl"));
        } else if (frmObject.id == "frmWebView") {
            var url = "";
            var entry = MVCApp.getProductsListController().getEntryType();
            if (entry === "taxonomy") {
                if (pdpUrl.indexOf(".html") !== -1) {
                    url = pdpUrl.split("?")[0];
                } else {
                    url = taxanomyurl;
                }
            } else {
                if (pdpUrl.indexOf("search?q=") !== -1) {
                    url = pdpUrl.split("&")[0];
                } else {
                    url = pdpUrl.split("?")[0];
                }
            }
            MVCApp.getProductsListWebController().loadWebView(url);
        } else if (frmObject.id === "frmRewardsProgramInfo") {
            gblBrightnessfrmSignInPage = true;
            MVCApp.MoreView().brightnesshandlingfrmMore();
            frmObject.show();
        } else {
            frmObject.show();
        }
    }
    /**
     * @function
     *
     */
    function animateCoupons() {
        try {
            var currFrm = kony.application.getCurrentForm();
            var selItems = frmCoupons.segCouponsList.selectedItems[0];
            var selIndex = frmCoupons.segCouponsList.selectedIndex[1];
            var widgets = [];
            var degrees = 0;
            var mainFlexVisible = false;
            if (selItems.flxCoupon1Info.isVisible) {
                widgets.push("flxCoupon1Info");
                degrees = 180;
                mainFlexVisible = false;
                degrees = -180;
            } else {
                widgets.push("flxCoupon2Info");
                mainFlexVisible = true;
            }
            var transformObject = kony.ui.makeAffineTransform();
            transformObject.rotate3D(degrees, 1, 0, 0);
            var animationDef = {
                100: {
                    "transform": transformObject
                }
            };
            var animationConfig = {
                duration: 5.0,
                fillMode: kony.anim.FILL_MODE_FORWARDS
            };
            var row1 = {
                "sectionIndex": 0,
                "rowIndex": selIndex
            };
            var rows = [];
            rows.push(row1);
            var animationDefObject = kony.ui.createAnimation(animationDef);
            frmCoupons.segCouponsList.animateRows({
                rows: rows,
                widgets: widgets,
                animation: {
                    definition: animationDefObject,
                    config: animationConfig,
                    animationEnd: function() {
                        alert("here");
                    }
                }
            });
            //selItems.flxCoupon1Info.zIndex = 1;
            //selItems.flxCoupon2Info.zIndex = 20;
            selItems.flxCoupon1Info.isVisible = mainFlexVisible;
            selItems.flxCoupon2Info.isVisible = !mainFlexVisible;
            kony.timer.schedule("mytimer12", function() {
                frmCoupons.segCouponsList.setDataAt(selItems, selIndex);
                kony.timer.cancel("mytimer12");
            }, 4, false);
        } catch (e) {
            kony.print("exception---" + e);
            if (e) {
                TealiumManager.trackEvent("Exception While Animating The Coupons", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    /**
     * @function
     *
     */
    function addToWalletOnClick(eventObj) {
        var id = eventObj.id;
        id = id.split("_")[1];
        var promoCode = "";
        var barcodeString = "";
        var v_BarcodeSymbology = "";
        var barcodeSymbology = "";
        var tag = gCouponsResponse[id].tags || "";
        if (gCouponsResponse[id].hasOwnProperty("redemptionAssets")) {
            if (gCouponsResponse[id].redemptionAssets.length > 0) {
                for (var k = 0; k < gCouponsResponse[id].redemptionAssets.length; k++) {
                    if (gCouponsResponse[id].redemptionAssets[k].barcodeSymbology == "PLAINTEXT") {
                        if (gCouponsResponse[id].redemptionAssets[k].barcodeString != null && gCouponsResponse[id].redemptionAssets[k].barcodeString != "null" && gCouponsResponse[id].redemptionAssets[k].barcodeString != undefined) {
                            promoCode = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.promocode") + " " + gCouponsResponse[id].redemptionAssets[k].barcodeString;
                        } else {
                            promoCode = "";
                        }
                    }
                }
                for (var k = 0; k < gCouponsResponse[id].redemptionAssets.length; k++) {
                    v_BarcodeSymbology = gCouponsResponse[id].redemptionAssets[k].barcodeSymbology;
                    if (v_BarcodeSymbology == "UPCA" || v_BarcodeSymbology == "CODE128C") {
                        barcodeString = gCouponsResponse[id].redemptionAssets[k].barcodeString;
                        barcodeSymbology = v_BarcodeSymbology;
                    }
                }
            } else {
                promoCode = "";
                barcodeString = "";
            }
        } else {
            promoCode = "";
            barcodeString = "";
        }
        var availability;
        if (promoCode != "" && barcodeString != "") {
            availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreAndOnline");
        } else if (promoCode == "" && barcodeString != "") {
            availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly");
        } else if (promoCode != "" && barcodeString == "") {
            availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
        } else if (promoCode == "" && barcodeString == "") {
            availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired");
        }
        var inputParams = {};
        var requestParamsIphone = {};
        inputParams.titles = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.offerTitle") + "~" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.offerSubTitle") + "~" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.validDate") + "~" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.wallet.promocode") + "~" + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.walletIphone.restrictions");
        inputParams.couponBarcode = barcodeString;
        inputParams.couponNumber = barcodeString;
        var offerTitle = gCouponsResponse[id].offerTitle || "";
        var offerSubtitle = gCouponsResponse[id].offerSubtitle || "";
        var offerDisclaimer = gCouponsResponse[id].offerDisclaimer || "";
        var validDateDescription = gCouponsResponse[id].validDateDescription || "";
        var offerInstructions = gCouponsResponse[id].offerInstructions || "";
        if (offerDisclaimer != null) {
            offerDisclaimer = offerDisclaimer.replace(/<\/?[^>]+(>|$)/g, "");
        }
        var modifiedOfferSubTitle = offerSubtitle + "(" + availability + ")";
        inputParams.description = offerTitle + "~" + modifiedOfferSubTitle + "~" + validDateDescription + "~" + promoCode + "~" + offerDisclaimer;
        inputParams.offerEndDate = gCouponsResponse[id].offerEnd || "";
        inputParams.offerId = gCouponsResponse[id].offerId || "";
        inputParams.offerType = gCouponsResponse[id].offerType || "";
        inputParams.zone = new Date().getCurrentDeviceOffset();
        inputParams.localDate = new Date().toString();
        inputParams.barcodeSymbology = barcodeSymbology;
        requestParamsIphone.couponBarcode = barcodeString;
        requestParamsIphone.header = availability;
        requestParamsIphone.offerTitle = gCouponsResponse[id].offerTitle || "";
        requestParamsIphone.offerSubtitle = gCouponsResponse[id].offerSubtitle || "";
        requestParamsIphone.validDate = gCouponsResponse[id].validDateDescription || "";
        requestParamsIphone.promoCode = promoCode;
        requestParamsIphone.offerEndDate = gCouponsResponse[id].offerEnd || "";
        requestParamsIphone.desclaimerTitle = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.walletIphone.restrictions");
        requestParamsIphone.desclaimerText = offerDisclaimer;
        requestParamsIphone.zone = new Date().getCurrentDeviceOffset();
        requestParamsIphone.localDate = new Date().toString();
        gblOfferIDForAnalytics = gCouponsResponse[id].offerID || "";
        gblOfferTypeForAnalytics = gCouponsResponse[id].offerType || "";
        MVCApp.Toolbox.common.showLoadingIndicator("");
        MVCApp.getHomeController().addCouponToWalletIPhone(requestParamsIphone, addToWallet, tag);
        TealiumManager.trackEvent("Coupons - Add to Wallet Button Click", {
            conversion_category: "Coupons",
            conversion_id: "Add to Wallet",
            conversion_action: 1
        });
        var couponType = "general";
        if (tag.toLowerCase() == "serialized") {
            couponType = "serialized";
        }
        var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
        cust360Obj.couponNumber = barcodeString;
        cust360Obj.coupon_type = couponType;
        MVCApp.Customer360.sendInteractionEvent("Add To Wallet", cust360Obj);
    }

    function addToWallet(base64String) {
        kony.print("inside add to wallet");
        var pkpass = base64String; //"UEsDBBQACAgIAOuO5EoAAAAAAAAAAAAAAAAKAAAALmNsYXNzcGF0aK2Ub0vDMBDGXyv4HUbBl0nXbU4nm0P3Bxw4YU7fyi2NXdosCUk6tm/vtXMoggq29E0v3P3y5PLk+sPdRja23Dqh1SCIaDNocMV0LFQyCJ6XU3IVDG/OTvtMgnMG/BqDk8+IK2/3jUyoeBA4y4JGsXj4DX/OZFodM7VNKGdSGMdpGnsqIVdsjbvT2WLyOnqcL2/v55NF+D1PKM+tAkljvsoTmouPSm7pkwcVg41fHpZ7w8PReEQiGp1PdY7LHs9ZhL/Jk2J1lDe+DsfccBVjU/YzsC4EoxwCmvjdcQ9dmoL9N2zFTCZ2JI2z6KLUddGqyrN6Wx+P6c1G43nREJwVbWvWhJOSs+IqHGnTeiSuvTfoD6xDZFQLUmjSop1aUBJU0kZhNdF0kuATKW1YjZjksAUS9WiTWFataymwzKHpQCnt4XC5Ldql1SxzpDJteYmrRyQOAlhhei3ILexwHqXoaFIRZbAiE54U06VXCeXkWyclYARKuvz7iencm9wfcdiZMrkffh3771BLBwhpHIW7hgEAACwGAABQSwMEFAAICAgA647kSgAAAAAAAAAAAAAAAAgAAAAucHJvamVjdHVQywrCMBA8K/gPpXcbvXlI24PiTRDUD4jpUlPyIkmLn2+SppYK3nZmZ3YmwfVb8GwAY5mSZb4vdnkGkqqGybbMH/fz9pDX1WaNtVEdUHcCSw3Tzqs9u8KSCKiuxNoLoy8C3GIUqbCjSgiQrsJomgKbDtkI0AI9e8abmwYaUIJHbyWyiUyKU6YtgHKmLRRd4wqqjB/IQKIBzFzBO4hp+5BtE0ZLAqPflMRMNXyk6w0k9Qj+Nxj3oUDUxXPzge9zl7/4AVBLBwhCpJpxwwAAAIQBAABQSwMEFAAICAgA647kSgAAAAAAAAAAAAAAACQAAAAuc2V0dGluZ3Mvb3JnLmVjbGlwc2UuamR0LmNvcmUucHJlZnOVkD+LAyEQxftAvoqQKpVNioM7QrjqelffLnPoKE8N5NvHJemz28gg85v3Bz5KqTCFmEGoRzV3sEpWezoeMheD98p/aMZnYjypSATHELBAjWgUxU/l5dGwflqomyLC1gPNcUH7ja7NmcmezHkr2bVXhGv2LtqRoYJ3fGZTieJG1i1KAVNfzBrw1tME2iELuvZR5g2u1v4cZS1kJ1tzp8eX7AAL89BJxtVRRfsO0CazDNcgM7fS0J72si+zr0afUEsHCNF8eXK+AAAAVgIAAFBLAwQUAAgICADrjuRKAAAAAAAAAAAAAAAAKgAAAGJpbi9jb20va29ueS9wa3Bhc3MvQ3JlYXRlUGtQYXNzRmlsZS5jbGFzc+VYe3wc1XX+zlrSrFZjSd6VZC9gWCkxSNjSWi9brIxBWklmsWTJlmxjuySMdkfSWPvqzMhYBChNTEgLtCUQUgJJGvIgJU1aoF07EU3b0Cdt02faNGlJ26SvtLQ0Tdu0SUO+O6vVyxLm53+7P2nv7LnnnHvuued895x5+fuf+zyATjmpwSeIJHOZ6GwuOx/Nz+YNx4nGbdNwzbHZMf4YstKmhjJB7WnjjBFNG9np6OjkaTPpCurys+PWdNbKTieyUzk7Y7hWLisIDi+zjrs2p3sF/n3JtJW13P2CTc0txwRl8VzKrISgSkc5KgQ1w1bWPDSXmTTtCWMybSpFuaSRPmbYlvq9SKzY56kJYBNqdVTDT1XujOUImoYvtRHaUZYxLNrY0HzqYitbjvnRILiiLaoEJnO52WhfPp82jx8fOBLva0uath/bBI0r5tXDyGzctF1rykpyMact397hxxXcccZKzhhm2gngKlytYbtgV8qMTtpmNmVkcnN29LQyctZyo07Ri9Gxg4v+POpaaR3XICLoT+eM1MVuHrJzmbGD8fH2DrWxvmwqkXVNO2OmLBqxwh41K8g0X7zbN0NpGX6zFq8wrbcSTXirhresCprxecc1Mzp24FqGQG6O8VNfXNDKRce4mss1TSPT60ez4Oq+KW4n4s6YkfWCLIDrsUvDTkFoHRU6WtEm0PKKlOZp16+z/ZZjGnYLtm+wwbGD6mgDiKJWY6YgsiFfv2EnGcoBdKDWjz2CyvaOzq7uPXt7blC0Hh03KGsCjumOmI5jTJt+MBADlpNr7enpvqG1PYAbcZMGpsY2z8wst8PQsSkRjRdHHTejj1uiBw4ZGZ5o23pbGt5IvFdZEtcxgEGm1bIlg1maTlH6oXljYeWqA4KWDVygRtNRoeDmbDNu2KkAhpTbbhVceymZeG4ur84zgVrl7BEdhzDKwOG6Q95pHzNtx4OVsuaEStDDTFAl3cZkb2Oym3zIukxq0zbyeaVjXMeE8ng9dahDnJjPm4mUmVUpoVKY4FO5e/HTrgRu03FCCdRQYNwk2qSLMOTHKZ7T2Mjezu6O8Z7Dg4r3dh1vU7xbyDvBWFtWrGbv0GFgsnjYi3Eh2NG8URYtBY8HPSlixsgSZkQxpWNaLRWislF72shad3nBrwLAD0sAxTWrI624qsg1nJvOTZhnXUXP6sghz62SXnSyYOeGlqw5D88em0pH5iPKhf3EOqWUcTinFqum0gHTSdpWXlnkx508Mnt6srmjvX1XpL1rV6Srq0UJzOu4SwkEPYckZ6ft3Fw2Fc+lc/Tu3aR7Qt3duyKlL0/sXh0/UhJjGJirxH5UsHfadBMDKtTGco47ZueSjOecHYslso6VMj3cMM+ayTnXjGRMdyaXisU6+fHjXZcn3MWPhvtLeDNHfI722bYxP2w5dPc5Fe7vIWxteNBDlplmXjygMOJBnsqUZTuuuvEU7WEdP6G2W8HtHjTn/fgpBlgi2+qohIpcGxnNphc536vjUcXpV6dtTJppP95HWOjavSMyOjSkON6v46fxRJHjmJGeM9fAX/Hy5gFX4Ul8UMNTPM3lPant6PgQPkyMNlKpDWRPVuIj+KiGpze+0szsXMZZDvBiLuv4GD7Oy3cNWVUC7R09BLWNwnNdbR6oPaPjk/jZYpgXyYL2DcN8fT0q2D9FL/b1xwcGhw748WlB+Zw71dqjcOnndfwCnitCEi+ZjGHPe4fJiiO06Jwl13mqXuBBJr008uOXaBlPwUpZ7nzMj/ME2r4zhpVWtUzESKcjKWPeiVjZSNzIGimjTS34WR2fUwsGPTgiuqWWlvTjRZqW592f8+PzCpzUo9oKlf8qbRw7MjoyGh8dGOzc29XNa0jp+4KOl0r6+ubOWmlrhb7fIH2ChYPDMItzKUvlM+m/xd1dTOcqvyNSefFE5P8nSbn3d3X8nnLv5kWIK3n2i5eHNN38KAj8Qx1/hD9mVFqOF0Be3XzSjz9lLRJdW+j2mW7WKK7UZht3quL4z3T8Ob5MHKAN8TSZVLi2rEhmj8gc+gr+UsNXefutmdLxV3iFVwDlj5gO0yhJKGl6g7KDFcPRI8O9Gv5asHUtV/+clU6pK/VviSlTOfUcIXfkjIKoiOXEYgF8Dd9QFX2bevp7Hf+Af2Qm8VJnHgta1wOii4xZXKZXqfimjn/Gv9ADbq44yY6l+WIRFpz/Srhtu9iJr+FbGv59dRXriej4D3ybrqGb3EQ2ZZ4dnSrWJwkl9F86/hvfUZA0N+ksLtzQnEisu/T/CiSqpL6n4//wfVU6uIbtOsctd2aDwvUk2QW6iLB5q0ib2WnFyuhI+IWNWsUUS/4YdV4lFbpowgapKek1QqqONlPsFU5adGqq2Bf12ckZ6wyP9vQblCmKcb2G4XK6g5ZT/QEJiK5JFevDnD0dNfJGcsZUQZ1hTkUVnCWjk1aWMBXtNxxzT5cum6WaSGWqetUs0saXXHuqfz3XSi3vY6/ANSOTKyRWh5wEGWgSWhNfqyN7nfjyS70XM6V6ra2YiJpsFVxVakn6513TKxIS2fzcYm8SkAYJM8qFHWK5svyYJleVCn/KqH5tdM5dwX8lvhGQa6RRE3aD29fxF6USo6pjdHRpEnZdZclcfn65RVDzywb0LhFXLtOrgmcHTRpgRd3ol+toktd8cVN0VeTQ6ETEg6HGxsaAtMhOTa4vFUOeewbPJk2vFtRll+rJa/LFfoyAOGEbSdMvqiOb4tVnphppomFPK0RapwtnXhnLXTerE2o6w+hlYTRmqEgPriQ4zp05m/DQfVmxSEPyHjRec4nYp+2TpXq+6dLlvEr+UjPEsvuSVfdS56QWKtYONOpNV+u8fPKrK5PatYUJb42Nzfaker0GaFW5QYqxumAgPk0u3XFcRr1zcLLJpQuRaGsvXRab+MjzW3098G7I8xgn2Khk8mke44BlE8tz9rw6WZpEcOpzllJH4DvVT7SbXJXzgZwXu8WXG1XWcnQz5YffKP24vJhLbx5WRy7nGla/cWLjuPjWqWONP/ddypn71aF44T9i5D0lmuzW5Kig8zICVZPj9MFKP9IH456biz6oX/uqq00xoxE8BYDfV6iXbHzaBD80/jNC+WsHR/UplxcQeI4PPuj8rvDIrdjsUZR49ZJIlKOaLb/+PGrWiuz0RPQiA7YgyLESIdQVhcufRRm2UeTjwfrh4NaRYPjQrtadF3BlrOx5NAavK6BlAdET59EeK19ABx+6YhXhimB3AXs5xC5gXwH9CxjizC0xbQEJPhyM+cPlZQUMh8uDYwUc4XC0gOMcThbwQ+HycEUBb+evZAEmh5kCTpPoLyDDXz9cgMPhTAFnObyjgHuUIffREDW+0zPoHBd5d6xyAQ/w4cdigXAg+OMFPMThJwt4hMNjBTwergwHPosP+LhwxfP4mQI+QYOfLdr9c0W7w/5wZQGfKSmsKinUw3rweaVQD/6iUqgHC0phVVhfVOgPVxVwoSS3uSRXHa4OLii56uAvK7nq4K8ouc3h6iW5zQX8WkmupiRXG64N/rqSqw3+ppKrDf62kqsJ1y7J1RTwsnLA79MB4fIC/uAJfDn4J7EtQSngS+EtBfxFLMj5BXztRPBvzuPr4WABf1fAP5E9+GosFA5tCodqGwv4twL+MxZ6BuWxuiItSlrZHR41HAr+TwHffQK14VBZOFTA64vkkGzy6FvIX7FiojwcCpddEKWr7oJUxuoXLQhJjTKhviBbiiaEpC7WsCANJ8J152VbbGt464JceSLccF62X5Crj1MsJG8l3zO4xnu+1nsOxcrCZQVp9kitJD0Hn7wir8pr0oJK3yO+xziWqWCXIRxmYLczmToY+J0M7b2k70ENuhn0PQgjhib0Mh32keNGfu/HIG7CIdyMCfThFPoxiTgsDCDPmTsxhHtxAPfjFjzEluJR3IoP4CCexjCexShewBhe5Jov4Qhexjgbg6P4Ko7j67gN38QJfJsav4vbpQxvk814u9ThDpYNhjRhkrdxSjoxJfswI4OwZBinZRyzchJpSSErs8iLC1vugSPn4MqDmJP34ow8hbPyUdwln8I7CA53y3ncIy/hXvkK7pNX8E55Fe+Sb+GcbxPu912Bd/s68YBvPx7yDeJh3+141DeJx3wZvM93Nx733Y/3+97D2HoET/kexwd9T+JDvo/hIx5YfI/eIiBIVHYrePI9Le0emFT4PizKuz76/inuoILe3uK7j1DVxVNo8uWkW/YQZHb6pmSv9HB+0DcsN0iM8HbIF5de7lj14q/IjbKfJ/SifAkPyk0IsEZ+WG4mrYpF5Tly3MRaeh+RuY+0zTJID71IWrW4sl36SauRe6RO4qTV0gtflAGutYW++AJ5hxCkN56RA6SFuP8DcoskUEcv3Cy3klZPP7xFDvKpgd7YJsMygq30QbUcklGUyRj3fD33U7TzMCoXrTtM64o2HaZNRUsO05Ij5DehvS6voU6TcQ2vLf7JhIaohg4NQxoSGp7U8MB63+Q8xm+wMWiR67Z9BxOvowplK3V5czUQuU2hu5z4AVBLBwiJg/TLSg0AAC8aAABQSwMEFAAICAgA647kSgAAAAAAAAAAAAAAAAgAAABpY29uLnBuZwFDCrz1iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAnYSURBVFhHvVh7cJTVFU/7R6djq221UhQEhQDh/ZKAogUEH7XiaO3o2NFaqZaOjxbbqhUQ5SEidip1pmpbFeQlICA+0IrWqpUSNrvZTTbhYRKS3SRkN0+yCSQh4UzP73e/++23D4PtjP1mzuzec89377nn8TvnfjlFF46X01EQv4PGStEFYwwNHmfm7Fjn/msZ/OqYvD4oBwIZdJFODhwthd++UALfG05eaPQlUjzh+6TgkIkkOy7WOWwYGjUtKTN0kq4zISkz5lIjM9LIhPLyzSGg6MAxmTo4lGFBMIsGjJLi8ZdLzfKnpeqhx6TwOxdJ9cJlEtuwVWKvvColF18hJZNn8j941YtW8DBHfrvYlQlPv4ZK1a/dTF5k6WquU3nfQxJbv0XiG7dJ2ZwbJZg7mXJ+nbNKefXJUNB/zlCeMr5pm0QWr5Bw/mzHTWnuA9mxznFh676sMsZKViY4bLL+jpXwJVdL8zvvS8UvFkig/4gUXUCugng50D9PDs69la4qUvfAKkUDRtNV7oYg550MXvq4Lx4U1f9QCvvFX90hB394i/i/m5tiRSoIhv/sIVJx9wIV3G5OYk/8JRMOHzh/JI0DRYPDLmb8272ZJFAmNHKqNL2zR+NhOl8A/7SklsjK/x8ISoXypjJ+qaRaGHzjYmykWYfAxknoUvCzEBdTeQR8oVodv4hbvMO5LGSsNEoKvzXY0FmDFB1GpOyDdzFXt+Z5qdJkg0fBNwqqtqWz5hphb6xkIyinClXe8ztpefcDaX77PfnstvmOkpnyUAKbwX1RzeTG7W9I/fMvS/jSa8R/rsabR0kY58CVN0rVg0s0FoeSl2MDtmHLTuITM855IZ3sKaOPr5L0p0wX9p87LFVeN0eiHbjqR9IZqXEkzdPd0EioCpyXlyIfUIiDZ6ziVDCYO0kadrzFGAg62ZWN4JbwtKvk1MkebtL60afS23Gc/xv1gD51nXUzfqEwMPNkcwtlEgV+ia/bLNJ7iuO6NS+I78xByT0GjSOAV/1msRmrbjkM9CET5MiChcSmvgLfd+YFUvfMc1wcrt2bc4bUrPwjx121dVKk1SWJdxoK6qa2Tws43/r+RzrOlYKvnC2tH37i8P6p1lKAtnsqLhaPu4xhwDEUtJoS8/TXPU066YZ+jZGOkjIu/tmtd8n+r/WT0st+oBbpJa9s1vUS6GdKI0KhYv4D5HfHGySolgmoRXHI+hfWkt/6wccmGey+UHDsdGlQTOSYCmJSTx55bJUEh2t6D87u4sB5I9Vds9S9J+nWkC4El2OuszrKDcvvvI9ZjZj2K5Z2lB4kv1qD3veNAYwrhEHDptfIb9Byh7G7j+qCGo9DW55RUCEmvv1NljiL8OmELD1000+5cHswTAWoiLrt2Cf/Jj/66EpCCKxy+JZ55HVGohLAmopz2Muvh2oPhDhX/fullPfug0OERmtj4YxdBWMbt6obpn6ugnBZ1YJHuHCTJhTG1pWN23aRX//nF6VQgx5WQRzhiS59SnzfHEhZZGzJlCukt7OLcweuvik189XF8AzAmmPVzW230L1Aezu2BNeiHcJJa1et4cJH/2SyD/Pg1z/3EvmNGju+M84n5vW0JeRUV5eUTDJQQlmN84q7f03Zrmgt671NKpKTJA2v7TJjKGi1D0+70sUel9SapZdfq1Vmouz/en+JvbiBi0ceWea6Br82k1u0Kyn46jlS/rN7zfjv/3AtDUKCxP62nnNNu3a7c979EGZRbfM4poLWxXp64KDXxTgxFmoPhY3bVAYPmgpmvcr4zhrMWMJz7OO9sk9hJL52E8dVDyxKxpju49cMT/gCnIsgXr0YaEn3Z6I542SS7MhMEkBG2cy5ZkFtSpF1eA7ffCeTBjJQ9Mj9D5OfKChkFThxuIJjWN9mOppguP5kcyvnDv/4DncNlwDUI6ZI5b0PmrFrQU3tqocfN10Exs4L1i3HtGKcKK+Ulj0fcvGD197sBjc64Yqf309+Yr9fihWKRAsFoIfZq3EFOR529g2UA1QhpFB77V6kNKCmgowP/WOBmmMPwUWRhcuxpxyvOMINCMjOXQWQAtDGk9hX6MZf85vvck27DuTKb5/Pue6jMYO5Tt/nEhVUoN76uhlDQdeCGvjpFgQB5w7dcBsX7unsZNVAW0YcTJtHrbWlsPbJZ2h9uw5hSmMST0f4AJsCbziRYCANN1ja8vqMQZDFrp7jpinoPXGC9xTbhcDVwDM8HcWl0qL1FU/5HfekBDuSLKrVCk/bv/bpe7kZxgDRcjCUM05m8ebtGVlMAk7pvG2XehLtxLYi7boxjyRAMuDpqjsqnTV1/I9y5b0EQcHaFX/gHODIq7xLAGrcBF/aYMaqWxIH1SoZOKiEOIA7E/4gFwcAo2/EtQDz+C2ZNEPrc4eccpoGHIbve3pLKFizbDXnm3a+JT4HplIoDaipoA3QkokzslYSEE6L9gpPz7E2KUFj69xbGOjapnUdrec8npbde1IAmmsAL/WOjadJwwkKeudJ6j1AUY1WLCicTBK4WDHu82oxylrsr+u4eLoF8T6uAG1793MeT/k8p6vxrIFxxTwDR82739OxUdArQ0Jb58HHZJL00c0QahY/wcV72ju0VZ9larQzj0TBnTbhK5I6rdP+foqRWNezBqoI7ht42ouKOU6XwTg4fIpU3PUrd+zCTPWSlVlhBuTFul40AFPnuFkMQmggWRCrPL0eMsM6Gl8BpS5NInQzaE5cL3hk0LA2agjwfSqof7ABShSwKVuiYCEsSAW7uyU4Ip9fHLwyXFDdQyt4+N55emLJk1yn/PZf6sFTwyCg8RzOn6NAvdMoDwXxIlyEelo64zpOgJdOcFvt6mfpavzPJnM6goVg3djLG+XQ9T9xq5E7jyTRCoNSaqsMLYgArtZKUvv0szwlhT1kF4CV4W7cf9Nlvgjx3qxKogSme4thosZBfHvhyeCgvoQuApcVJoo2qFbg/0E4LBoH3Hka39jNe4lNVhODEEQ7pCgOHEKG4VNFtnj8UggWU2p8/W0Tm5po1kNuJQEDhR9QEN+wlYoiMymI0yC7bQI4/0nO+17eF5JRd/N7jdNFoYIcQo+I+PYYxq0kdlHAR6XeZ5u1XS+deR2DNaTY5C7syLmEcRovQyYbTy2Gel2j9bn+L+vIs98GveRaEGSZSBqcCJ/k0FrBovH1W1iqcGIkFHmvbNFOZzabh7jexMCLPPoEZfD5wr4HRYr1KonPHuBFtSYD6KOqXGTRcn7/tlnr1QeUoqAlCuoLjA2cXlMfAAqFCQX6izHIfFCf4I5dmbz8pIxeuuA2V0bDB/sA3lgSdZ9sX8ZAWRXMIMQglNVfKm/HICtjx9lk4NJsMqel8fIfHpDY8JPbN10AAAAASUVORK5CYIJQSwcIRs17O0gKAABDCgAAUEsDBBQACAgIAOuO5EoAAAAAAAAAAAAAAAALAAAAaWNvbkAyeC5wbmcB6B0X4olQTkcNChoKAAAADUlIRFIAAAByAAAAcggGAAAAj92FfQAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAA7CAAAOwgEVKEqAAAAdfUlEQVR4Xu1dd5hURbb3/fPe99Y1oO6aRQFBBRUJKqBgBhXBnAOKObvmNazxraKuyrqy6q5xza6CYhZ1cVWY7p7pyXmGiUyGyZl653fuvTP33q663Tf0iG6f7/t9aE9V3XCqTqpT526RseeBIoWfP1KM/IUgxchfCFKM/IXgp2HkHvuL9F32UwJ/j+mzu0OfXanP6ANi+qTvPkneHlD12c1Ln4mWNvj/DLq2rG2ykDxGEjMiv9lbRHacIDL2mmz9HYw64DCRsf8sG7Tf+O/ml4A++Nepj+3FMRNxXVkf/DZppojghRvX2EtnyNgpyj7pE2eIyK7mPpO1PuOn6X30fvscLCJ0LyF6/tDWo4m51Mf8DpKAwBkJxoW22kOE6eZz550hso9coM1Q/J0eLvzbvUXjR5+Kgb5+0d/XJwZ6h8H/T6h/d7kI7TBWYwyYSP03fL9W2gft8W/Nsy+K0Ki9+Dr8cveeJtrzCsRAP/Uxtef/5j69ouKBJSK07Z5an132FZmTZ4uumlruMzS+0Q/X7ukRpdffMXQdPGv27BNFb8sG7Tr6uP1tbaK7slps/DFNrH/xdZE1/WherfwOkoTAGBkhBqVtM1pkzZgrKh99WnTkFwlQd30DrzCsEGYkPXz10udEW06+aMvOk6Lq8T8zw3nsPQ7gVVD36luiXdknV5Tf9ZAIg/nUh1fjmINEw/KP5dfJIWTliJJrbhXh7fU+xPworbjmVf8S7bmyPvRbNFsUnne5iOwwTutDzAeTMMmMPrjHrpIy0dvUws8PKrrkWhHWmY+ViXcVoncV2QnSyvoevcI/I4lJoa33EFmHnyjq3/yn2KTf/OCmTaLl69X8siI23RKGyNl5HxGWgH83mGgAE8Cpz0770OoYbxVfdF/h34xz6DNBf5F6H4hJmgDoI2sPhGgSRuhfSx+aABCh3IbuA/9CgqSPm8qTOnfe6TypjOeP/Ha8KL7iJlH7/Csia+Y8kfbr3TWJZb53D/DHSKw0QtWTy8SgzsBOmo3ldz8solOOEKHtxgzPxJi+EJsKyNqzmHWA2z6miRXbh3S06z4G0JfeC4wmTDBisrkfVmL9m+/xuxokcV35yFOshlgy+GCmL0ZCtEQPms1M7G1sEqU33SXCNKtDW9Esw8yV9PlPB1Z9hBhWfueDom/DRmZoW0aWyJpzIr83WZ9E4Fu04sZyjjlZRCcfzkYO60JJuxR0QBzTO0rbcjeynGeJhvc+ZGb2d3eLwkXXsJUr7RcHrhkJERDalsQlxIWuqCH3sTr9yvn/KNC7gtWbRvZF6W1/GLItSm+8c8gAcwNXjIR8L7/zAbYgWYFDH0japeAC9A7TfrWLKDjnUrJ0m8X6l14fconcIDFG0uyB/C5afB3Pmn7yldLHT9ccXVn7FFwjDJdm0kyRPnbYwnWD+IyECKCLZM6cK/o7OsXg4KDIO+1C7cKy9il4BocAIeV0lcVuSYJMjctIKOYQ6cDWUAavRjjeEAVBObIpSACDCO7L3tNEBLaHrI0NzoykAdNIpJb9/kFmYvOX33D0JqUbkwtIwPzTLxLd1bWi+PKbNJ1JvJC1NeDISI4PktzuQyyxu0dkHnKs5uBK2qYQHPCOc084kxdPd00tbxiwmLW1M8ORkTBwqpYs5QGrn3mBfJ9d486MFIIBJGHd6+/wuy+7434Og8raGXBkJGZG0UVXicYPPxXR/WdpvqKkXQrBA7551mHHi01kXHaWlIuwYQhJ2gJxdWRkp31EaNSYQAK7KbgD7JHmz1fxqsw/fZEIbz9G2g5wZuRmBI5RYjeDFD8CE2H6F0ZBPN2RTODaiM7gPiC90ncmiRWgIRjedrQouvR6ZuT6F//BIVBZO0DJSOxqYH8vfXf4NT/RSqTrsvuz9WhW+LnzzxYl190u1t1+vyi55haRt+Ac3i4CY0fMksZ7GbUXby4jKJJ9xHyRe9I5vF2VOe0oESZm4n6CkGCsyibNEoP9/aJnfZ2IwOVTxLLljIQTOnaKyD/1AhGdMP2ncTcg1klPhOlh4Lt2FpXwzLRTV3kF/z00AqszvN0Y3t8svOhq0bj8Y3YPBgcGOE4K9LV3iNZwBm/j4aX73ZoCH0I0RvOnX4nu2jr2K1XPKGUkZlzxlb/jF1V60+9JnHmLyHsGmIjI0cSZYsO/1/B9xKP1r7zJ+59JmXQ0JqzI3AXnio1rw/oVnQmZAthsD2+3ly9m8m4SLaYocoYUqxGQMhJuR83S5/mGsJtt5KiMFNJ3msAJTe15hXwPiVLZnQ9oesSnSBsCjYOwGZ6/4uE/iUFjiyJB6qlvFNGpR7JkkY6fCGhlYxXGy/mRM5JkfMNb7/PN5JFegpEha5cU0OxncfLlt3x9N9TX2qZluwW0qc1MJMlQ95rmz3mhltU/8BheAuFuIGckzcDmT77kG8k+4iQtH0bSLnDQCsCKKr35Hr62F6p64hkWg9Lx3YDEWNqoPUXdG+/qI3ungvMuZ/0qvU48QCpgctPiislLMkHKyDS6aNNX2orIPPwEkU4mtqxd0ODkXrIEe+ob+NpeqKuyilMpVNZdokBSVNWf/qKPKidIWliTSDRzIgRU0rZxv8cIaDryYHY/Cs+/QimmNytGQjevu38JX1dGSIvAnmj9eyv0X+RUcNZiXysA4bDCS67VR5NTwwcrRc6xp/L+YdbsE0VrJKr/JZYwMcEML1Y1/NO848/gcRpXfELSRu5Lqhn5zWrunDln/ogwkh1+EiGdZev4unaqpRm55n93EaEtd6V/d+addBUh1dCreIX4ih44W/Q2b9BHsxLWXtldD/L4cC+w3YQJGD1ozlAylZ3QB36mlw0HDqDrjGx4Z7nmM0vayRm57Z6i8VNNR0aRJT0CGXFYQQXnXsbXtFNXRZUIk7HA9wGdgTTDcVNIrNXrLazUnp3H/qdrVwR+Gz17E/ltKkJ+zVqaSJax6Z7SfrWrWP/yG3qrWEKERpka6gBfjEQKR9aseSL/jEWOvkuQQBJSvcKwKL/n/0hn7Tas6PHiaGVWP7VMb2GlgZ4ekXXoscxw+3WUgEill1S0WAuJyajmL38Ta7GpLrFAETJ06lt+671KJjjBFyPxUMio9pLN5QW8G066pru6hm/YTP3tHSy27FIBD5hz3Glik8LQKFx0tasVAP2FVd9ZXqGPYKXWtRHOKFfpORwDyD32FBajMloHH3cb560oGfwxcoSBKA7iprKX0Pz51yzqY/rtrp3qQma7jKoeW6oFB+z9ZMBqJCt13X2P6r2tNNDdLbIOO0FEHEJuCGJkzZzLB35kVPa7u36iFUk35tnycwlYiRUPPsY3ayenTVXM8IZ3l+strdREJn+iESk+1zgBbk+j3ttKlY8ujbupDjGeOeM4YmSv3stKRRebDvK4AE/y+WfzGHBjVO9CriPpwXKOXsgmOO9+JDkqkYYAhG5cmQmbqshiV/lOeCgEzGWEWCfOVMQ3eEjf0mpUTSQE5XHWMV7qJ6zdnCMXKEU9R8gQ4ZH0dQJ2QDKnHiE6ikr5GGB4lHxxSRkJUdb05Td8A5mHHOPOaHALuB0007vWVfL1zARrNX2MtpUl6wsdXnD6Ir21lfqaW7Q8UeysS/oagM6LjCH9XFOr97RS8TW3JhS/xb3kn3aR3stK/V1dIpOtfw8ZFnRdvkdISQc/VM5I+JFf634kObvJ9CMhwpHSMNjXz9czU/NnXzmKR0ywrEOO5cQwO/FqPmph3PAiVjXS9GWEVY0je4m4MdBd0IMy6iwu5VUNvS7rmwh4MjtIRjUjRyiyAz1ceMGVfC07VT2OuKmDwUIPh6PiWLkywqFURz1PLwY5u23pmXoPK2Erj3VSAjm8CArUvvCK3tNKTR9/QRPSW4huCHHU20/OSLyodff+ka9lp+LLbiD/zMFAoIcL04prVewRlt1yj6OlCHGYt/BcqbXcUVSi6dhE/Gh9QmxMi+i9rVT58BPsJ0v7xgNEK90D9loj4INCxG8WjJRFRGA05JBfFm8vD5EY7NbLCCuaV5SkH8Aph/+Qb1GxtUx/l/WzA7ovOmUOH6mQEZKNvfrk0I/R/WaIpo8+EyVX3cwnqmXtflpGYibTA+KIup16yViJJmCsgFG1y17Ue1mp7rW3lSuSx93vUL6OnXoaGskdOTjutQ3ArSi6+Bq9t5V6W8jomhj/OVRg9+PEs3ispg8/U65sJSObv/03d05q0JyMCJRV6ciPzQToyCvQrLQ4hgYYWXG/3JFv/myVlv4xOrZfmBiM+gYyQiiOQ4K2PipgZSNQL6MN9B4xWePpOBV8BQQgruo/WMmJRVHyYTyZzQmAfTNaFVgBdmoh9yeEPE4JE8xAjLPkutv0XlZqJZ2FKiKyl8ib5198rbccJmSsZdPkTTg9gyYaMufas3L1EayEGgGJimgZfDESyhU1ZxAUUMUWgwC7DzMQ1oqNhvCBTwf9ZoCtXrJOZdRZWMxhPLvZr+m0I6Q6rQUrCAlTpvZOYPdJ8QwgZCL6iVn7DtFBpvtKGkoAuMm8E86SWo2w9BJhJPKJcnUdYqeemvWcxGWfjHgZyA6UUcm1typflgyQXiVX36L3tlJvQ5NI3we61vti8MfIEQKvpvOv4Ju0E1eYSiA9AiZ5NhlkqDxlJ2z0Rg88PCYDTbXniApWGWQhujFMYHzUvfymPoKVcAyRdbSkX6LwvSKxGhN5kY4g3QTRo0rlg34rvVau3wrOXkwiKf5L4BIxJCb72tr1nsM00NNLYu84Pr8y1B5MmjRLupvf8N4Kd/4e9CNdX6UfKyBVfOhHAFZr7vxhq1UlpaSMxIPnn3yeKLvtPpEOQ8GjxQVdi+pOSK7FKmARY3JocVPIyrYTRC3yYRIR7Rgzui8ZTJKELfiiuaTnzeM4uQpFl1zHk8s8vhOwR5p58DECpVVklH/ahdrWl6RvosDEix54mNi4JizKb/uDUt9KGYmg+ZD7QYoce22ydk6A2ESi1GBvH1erqHnmBRHeQ1uhBjPByOrH/szXMdNAVze/oIRSTGiyIOiNXQoZ4VyGOS8XK04WgGCxOtGdWMWkKLzoKn0EK2HFY+W7GU8K2Cu7TeLyaJbyaTbIGUlMaFr1L74hr0FzZqRt5iP9P0ziyDhnCUbWSPwvuCOarkrASKDx4G925Bbova1UcOYlw7MYohB+a1Gp/tdhgs5EHmvM+A6A/1itSJncuCbEYTuv0iwGsL4dxlIz0m9kBy+N+m38YS2PY1D9u6SHIL7opqC4jbpsZuoqLddqrdJLl45tBo0TplXempau97ZSkSnlAyIWOyKoTGKn8tvvU+ofFWDINH+hbffZqfaFV73HV2VwqoVHSB4jCViVOApndy8gjqCL4JQ3rfhU/3WYUJst4Sw4TAgyCFq++U7vbaVixCd1vcc6+Y779L8ME295HbnAnbtFkwxFo2T7qCBsjblxY5QYEq22ypQ2JJWRiMpwauVHn/FYBnG6It0YR1ckZzw4rIXddIcZaAaPs/ILvbeVylDoUH+hqgA78n7c7hdyIGDWPOk+KiYuijnAdZD1dQM2dshY3PhjSJTd6tbYCYqRBBgaWXPm0wNbk5IKzrpErP2fnUhv/qj/MkxgvNOGsh0sot/5QO9tJS17jRjJRtFk6d5lA/XlsjOSsVVgH1iRh9u3sVU7BhfnBFUiMOfsuHY/wMjm1d9zZw6auxE5EvBZeJsugW5c8987ijaJbqt//V1XYgkPV6fIPK+8fwn/HemKOcecIs2pKSOz3m1VRoyp2kflfKEEAv6JwFdAAKsBpUEwszI4MuIvaI6Ll9gc/67KahEiUdYejj0zsf65l5QzTwa0rSX3RkYVjz7Nf8c9YKPZTiwG553hWgxyROcleWY5EsncSBQn+IvsYCZNOFhkTj0ykKA5O87Tj+L8UIOwMrLnniZaQ7ErEkfj3DKyUuKPgqqeWsZ/T6MVV/9GrIXMB2wQD3XznDCwSFfJ9lFBtc+9HJjF6jNERy8f2d9BbV/hwUnPttlOLBXffLf0KDdX/3fJSNWe5PplL4rQr3fjOjXtOXn6r8O04bsf3R9EpYmu7aNqBfjttO7uh13dvxP8MTIJQBLV+r+/xjdkUM3fXh0qVmgmVO1Q3bAMaAs/UEZ1dM00MqpQcQNpiXbC6nH70jlaM3EG727ICBEtL8nIMvhckZN5AO7kZqY6AGOV3mBNO2wmyxiuiJ2Kr7/dVcwTbVWby/Wvvi3W/Nd2XNhWRiV0LbebA7yPyscD5HuQeQvO9ZSMLAMHzb2mekRIDBZdcCV/fyMdvwVlfZFvZbYZ8f0NlOeyU/HlN7qa0Rzz1IsC26n+rffFj1tsLSofelz/ZZjY0KHZjnuTjasC2ufZnsUgbKdlH3a8FlOW9HULTr6i1d+48nPe93SXfEUzvIV0ByiTbsqPH2kAPhV8K1jCBnVV14qu9XX6/w1T0YVXsZ8mG0cGbHcVnrNY722luvdWiB+2GCUaP4gNBPQ0NbsOlAO4N9VZTgTfowegbp9/H5KByA7pY+/pkD6D5jGgVQ3fqoN8LIN6iamAnQoT3Is0wE7zwnP13lZq/GClWLPV7qJDUuoFxY0QYXKrPliU63WI7NS1rkKr955InNgN4tyjmpEBRXbMQC4MqjgZhNro/T2xegZ5Lm50DAfDjztVKuqaaSXidDPOWdrJbeDBAPqU3XKvPoqVsMkcr5KjJ2BiODBzRBkJRW3OQYUvKYu05Lis7YP7y8H5EUm6R9P7H4nso0/W/89K61D6zIObgD6qqM7G79bw55+CMhK1oPlErsXn7RBPMlYkvYDyO+/ncUFItwTs5LZwAvzdzGlHiv6O2FUHRqJ6l4xwtN5Lhhueo+rhJ/RRrNSEqA69P1k/L+BjdVOOEB2Fxewrq2yHEWUkbgKGjEGD/cRI2ypiS/KY+EcFzBjy6xpj/ToceK2UbP72d3ZqkSsPQQ9m5CNP6SNZqdHB1/MCdj+Mg64rXB50BSObDas1gKC5AT5nj3P/PLKCkSRqeW8wznE4MzgKRXqwqyJ2b7Bp1WrRYNtGAyWayS4DM/LRp/WRrLT+pTfo7wEyEm6b14AAjBJEPFBFKmN/7+cW7BhKDNYND2zoAmYaHBjUMr3dSAHdIsaug51ao9n8XUc7NfzzI88rhxn5xyf1kaxU/ezfA4uzAr4YCUXNgYAJ03m2S9t4AVleqN5h7KqzoWMzdjbRKs2ZfYI7RtL94puTG7+3ppWAesivk6VKovCDSkzFA/pVKo6q1zz57GbESIBmeRA7HxbghZPfBv9NRdiAhgXqNjKCbaOmlZ/ro8QnP0fdIDrL79K+hWKn6iVLNzNGJgn8wiU6yyAw0kuIC6tEtT9oJ5QDlWWgJwrEZstscWODYAR5Xeky+GMk+S78KVwErgN2bNmXfP5lvjEZ+WFkpUJv2QknnP2kKiK2i+8mywhGUOCM1D/mwuVZ3BQVhHWJs4MoL81fTwuQmXjIivse4RuTkWdGImymOH5gJz7J7OeoG7kE+adcoI9mpRpydYJkJBeBQJlPkjaotaByy6SMRKJuy/daLfEg/UgAsxm7Gyoa7OsXOXRNt4zkeOtJ5+ijqIkzE2zHCNwCrhG+LgAL205BW60cJKeFBJHKLpnroHkSAgKA9sI1B1dGmwZgtZ7ozmolwDDDEbp4RXuRBOY3n4Z3ckjH9m2IDfjXOxx3TyZGnJHI38lyOPiCLHDXfqQOiLQaRT0BEO8VItjgIiAvBYk7xD5ldfCaVqIUSzDZAWawL++g4kaekYjC7D1VdFfFVoIEcWTnKHeRHQOIS+Jgaacq+xuF/SD2FOIpYZCRFNphLJ9utpPTcXdP0L8ywAUlcN+KcUeckexLcm0ceU0aUO5xiR2piwGsbVpt+DzihtU/cK1xhBtQearoshs0kRfQC4b1KKsmglJomKhB+eCIhkGCQWVwdqHioNHIM5KAF1r/tjwzHOQr3Z6YiUmAAg0wapCzmj52SuDijl2Qi2NdEKgGLp3mw5gywxI0X/6J0iKWM5JuskX/Ao5WniWYmzKAm0GNABXlLgwgeYn0CV4mJkSgYUYdrLP2n2VJXTEoXsUtN/AVEECaBZZxWzRbZOx3SGBiwgCfm7hAXjsAlHvyef4ZOQLAUQhU/rcTvobg9iyJCv4iO4QIzTgk4bJD6tc4sIEzzw85hmuPyyh37mm0mjyK1hEEvnslS8LqLC0nq/YA7d1J+rkBMiXydf8YNd9dM5KNgoDDc0OgcVVFFPo7uziZOLAs92QCjjrZD22Z2frda4Ssh+w57n1hGcwZezCuXOnIIYCZAVl5duDYNkqF2YlPMiV6yHUzADLqCiRqAsfu3eTmqoAVaBw+QmjTPSOxaki8wVWQ/t0nIPtlmW/r+NxHMPplREATHXHeeltt9bJb7xXhAJ4DZbqNIEfhhVeyOJe1kzISdczhfOL7vtVPLvO8bxcPcAnMFTYQ3R/Sy5L2mysQiMCnJpr101mYnEjzd5MJqEKEVAzSY1DTjjf6Fe9Gzkg9bgljBEfh8PnboC1XAG4BasUVXniVyFt4Hm86s1kfsHGVdND9woDDJERaP4o98Xl/WVu3gF9MY0Gk4qt1qnejFK0wn2FGg7gKlYtCewkDNwURTgqd3Q1UrpC1+zkAz0KMhM5MlgRzgpKRfDBmkVYnpwElVXzs36XgAVjlJJ1gL3BAQ7ESDSgZCVmMDz731NaJwZ5e/sSslwpYKXgDJyYfNIcz2lFyNZ5qUzISwCqseEgLpdX89SWRtiU+NCZvm0KwQAXn+re1zyMncnDWkZHYQMWnhlBLDk4uqmIEFQxOQQESobAZcuZqidz8EZtxU/ytSAwKa8n45nHpzXeTg+rudG8K7gCGhch/N74QW3T5jVpYzquOHAL5RzCrEf/M2Hta0iI9KWjA9ylRiQTU9NkqrdBhAu88PiMBMBNBbPZj9N/w3/Z2KXgHpB+5LliBIC4bM2lmwvHaxBhpBl0wQroThzl58zfOkk8hMXDIct7pzERsTuctPJ8YG1+kGnDNSChilK/GzkX+mYv428IpcesfMCJzjz+TNw20L+SR3+5ikbhnJJnBKJ0JQqJUyU13ibVkELEISK1OV+BkMXb26f/p3aHQMCRd2INn4F60EhDsLrrsRmF8hhb5NzhoigpTbCanGKoGsuIQsYEleuDhnEXOMVTj7x5tD0+MBLD0s49aINoyc5iZKEtSfucD/MHKZATYf/aAwbjjeHb0kWVX8cBjfNwPJbhVn31yA8+MxKpDbg9qhFcuWcqnj0Eb1oYFCqmbZxbELsAMRnFb/M0M87gGoHft7cyQ9ZG1M6DS47K2Bvz2waSm9wNfHF9MxwdPq+hdGdnwKKmGTWMuHmEe3wO8MxLQVx9CefisEMqdYNcfe2jGA+FB8IGV7Nkn8BfgUKkYR74NIBvAUuIZY0JX7ETMN7UzI0zt+UicuQ/2BLGVJGkPaFtkJrFP/+I++fqS9gA21S0Ba/ShScr5TJL2APSbISrRN7rfoXxKzFxzHVn2CHni9DanaRrj+4A/RpqAFRfadi9OEB76jV4sPvtgHHXpb2sT3ZXVnGqPpGH8iwqLhedeOtSP67zNmifaMrKH25mBPmTZoYSYES7ULL4z2OJT9UFGoHbKS9snxPUKzlnM11f1wbc2MicfzjqN+2w/lr/h2FlYIrokffBby6pvSXRO4wkMCz//7OGKXChpio97Z0yezUUWeRMiACYCgTFSBt7NJn1Q/fRfOfO7s7SM47b9pBsG2jsY/Rtbtdpz0BPUh086ke7tJfEz0KG1sQMfwsbHUQzmYy8z79QL6PcN8j70W299o+Wby7he8aU3cF5qTHsdKA6Msi9GIhgs9rI77hP9rW3S9rhOe27BcP1X7K+Om8L7uUjGCtG1eYM4CYllSWUkg8QMLDScleA0jnFT+UGj+x7CX9DJGD+dRZVZfEEEogwY/q61M+NQLSU/ps9+PLayD40XI1oxBl1f2Yfa2a1w7kMqIra91oc/dY8JrLeHioEtwe6ZWX8GjOQz0gAegpiKF8OMMoAXJWmPl2FpZ4aqj31sM8wv108fMMrDdZKNkWNkCklFipG/EKQY+QtBipG/CBwo/h/5ozreuMjRlgAAAABJRU5ErkJgglBLBwgIPScH7R0AAOgdAABQSwMEFAAICAgA647kSgAAAAAAAAAAAAAAAAgAAABsb2dvLnBuZ+y8VVib3dYuXChQ3N0pWrS4FQmuxd3dgltxK6UUCVa0OMXdXYsXCC5BiwV3C7bp+tb61vv/p/t0nyR5niuZc44x7nmPcY/5XIlU/SiLgUqK+urVKwx5OSn1V6/gS14+Q5CRXl4/aiDTvbzBuavLSryqHCffe/VK/JW8FEDzE9xh5psfy7+uBnX8GksGLs2pm0N61j+iFfZj+5500GFPUcPhUl+KU8OdM4rjUmMLfSaVQKHXnBYWJYuG25Q4GQu+P3rq6rqyr5iOlVkyOlpZXYkWm+C+WM3gdw6cysqSyY9PcR2mWccuJg3xF13LHDAA3EvQ5mZJw9L9cT04PDp2fagS+aV6dz1dLS1smerpCjiVQozDjH/NEw0bleKGvO3sKribU8WCkkDJXQJCl39ifnk1OJ04St7l/8Z9peUA2wVZAIEc3ogAjt3YQPxkx62lPQZ8By9ULeKa9xyeYwJwXvD/Hn1JV6/sB4Pg3CNxYwmRwQ/M7SK0+4DhKFwNjcZsYqf3wSXgRzCjquKZN2fYuRSUCuylSvPpyP62+UzoZXBMuAjxMNpuxH3DYlZilDeMPUKwLieQ2eRJIxgCMElIsnP38ruFqN0jVHUtuUg/sdUryVKY2NxkWHCT+vZ6oYdxeawFNx4cNjefCf57tDcSwWjTej1RWlNcRzwZfKUVXxRMVnKiEmA2U/mPJpCRtdhlLIEjm16O35LO97EfxsehwoOWVtDO0xoF/aKjftVGbvPNga1/206BDMcuE8HY8Snt0lasko3d9rJGQPMqnB8q7LUMjN8mR8w2BjMCfitZedQn54H6k/AaOOHJQUa5E8qDZbiUfE62FvNG3i+rCpbsfomTbvArorzZnBPmlk9pHjU1u/BleynVaxS+3TPevB+AHRieimvT42Y4wKcfCDqXl7iZihihrwY3Eb5Kzvs+ulTHPpDZsl4axzkz51+UIZGku/e9DPl3ma2k8IafnfiNfjBvm8k6+82zmT3vKwR8mx7cofR0b35NhpRCkEPieiv/canxNVaDRst4jUCf1++4LnS7ZC1hE3zc7cdcQbWjOOWI5XQLqvcpwv92JIS6u98mqjhKg9f/EKM8Ynehwdg/wGQy47VycOepTyO5CD+arYDpulsWBr/Jl1W6oQ4Mp+7qCj3yCJP7QfoftBkYFGgk86m9YiLLDLqygeYGoMGr3f9AihyOXfQGiUNmg+ucM87aGDOEIXx7Q0io6CfGO74b9Bu8yvz8wZSuMEKd5y19UhcFXQ5DWVnnwhTr71PXHbRLr0H0OXtm5Uqel8v560gCQ+QRjjm9yeobmwgmEGVHS3e8Oy5wu4tBV+yhj4Efo/u/pvxrUSmyy8ZkZ6RhxG+H+TgRdLYtbwUfgqHWbbxzIv9HHrdDtjcBpNv5twUlYP3Cp3UTawcDYFdWiaPOdshi1Z/3LH8KycKMjQiKZEj4B019hm5DyR5ucqRU+MFkZJ7L8WP2SWdCYv+aKFgyB426O3wXflR6z2FLMcIsuElLM8N8Pa/YtcWlzf5goZW3sKsJrEczIsqFOOduATwkmu0sO+tRILvpsuobTcm9uan8okIbm7LpUhSkFCroP5bDgKrKy+7hjNmIlRLPz/vnNlMFMyT2a7DrirIs+deLRRpVLITmdVuKDWNCyO5Fyy+OXrdPWOLxoKdvXld2U+l4jdef35oAe0ETUEuy8yGVurd+WJk2Fj/ujKPbWSKDkHceN5HQK2FJO2t/Ln+0dyK7lF+He32DbpvGTpPhcOHds+BNn71YFnKJ4wKwh6UKRWWihWA0N1dYkuIS8YkstXXTsZz/sQbz1dQr0/U2ZTHJIAH6qe2cz5twif25ZIqqY3llisq+YqkIqK6UKykVH8zsB7/qig/nvGdMtIsdmwUHMKSFz5vHyxeQ4aX8nQcf+2UeVGohvEzSkEy2/LYItOV5TjcIomOAIqQgb80Fx3g17yHo9varNCV+B0U58cjxCWIO/lljJhEAyo6ubdhihC3wzUL7Gj94LdxaClTWm0/J6WgYR/o6Dk8YIeHtOruQfLybrxFBSLHs595NGTImQwQx2ackaRsmoonSJvq8irl6Sjyr7CcTaS/1pOXu51/daLWFb1iMmtupFgaPPV49+X72DyIaKFmJGHfVx2eMBblsscZOFuFwkRmocESj+FEB2OV48zZTMUNG/npsO7WF0AgBjt1lKRVyy/eBKE9wKOEDAzkeItH11a3/FZMP/FOkNJlaaRcrjIQe62KTvZJdLhO0ADBbf7tP1BpMD5/WaOGg2FxLRCbIjvqRo3IqhssPTlgD3krH4wXb9lLdiCW88NB0b3uEzglEdvvCC0R2D27caFGnshIACjCpZD2F0uSpXEnVch2iDLZQO0cOXC5S4eVe/onXXPUaA/Qa7vCK6W5JQISxigqenNFoLZBiMsz4k9L5PS+vyeQv1nPDOHu4wUhy+O9v10NnFHDcYUYoIWrJPbE1Q6F5zdSeYzluiFYBBEX1/A7VFERraTNBZFv0rBxU+Slb22XEeBTG/uK9OIbjrrfsuZUaZVjw+XcxSSIl683+8t6uHLxLH/QX387gkv0r8El/A68gLsSdiQsHmbfBHVN3AzwQw7pDP/55JAkgM/yUkuNfbMK/lQIuakwq5anAnV4fI/bRwT6h4Jernh91ixdm/9XgfT6D/bU1tznE4lKZt+MUYoCrPzJ1LRj1O/LvFKh/IxIeLHjFHt6NPWprUjvU+9bARYdvGs8JWEw1Ej51e45EZCLS+2EezIDiIJ6E6yVWft1LaVWXf9ZjCIm/kl+/MTK9+ggvUqTOiCDwMcrUtwn5Yl7qNLGa9mZgO/VgZ6ufTuZH7Avhx6HDERUw9A3beXIiNA9dfUnmwTCAQ74MlyfsqUMsKZOu56w9k+9apI3pvVMaywsExkOkaFl7FULdotIw2b5lSrPFHldR+CZl9t053lUkrQydQT5hlNLH1VfQUIGOnIXf+DvhY4YMiL9EP8aV/AdRTET3EmFKOhtXom1CpN7WO+LYlXEmcOCby5g6QUNqm3xvY3F+V5wM3+jFialukYHfll5N6xrYrugM4qCBPUGfFZwF9ryIA9pb+cXGLrRnRftfe39T6vDfiHD9+KE9L9ntxVnKI5dJjkgVkhsYViYzjPGQTfxJVNH5Ukc4yt4giOXeSEhuWyDmc32+hKGbg9omtj3ZXSPOykyr81E1cps542LC+NTyv4LwHp5dDoP2zz5myB3ygrHtFMPR94IbiyRHZ7SUdFy6vaP7CsXo817p7fJfP8byeVD8G+YqFd+URIvZB1ig8LlMzQ9WsnAa5hphr3tzEe9THP/YvHjXruD8vs96DoNbk4PyvkiZa/Av75fKvnhn1mJX38sJAegg7Qg6US7+1WHXFVoSH1LEQeHJfeoZ74/ru0inOETmOEkWdbRH2KL1eSIrFMlWrczcAs0iTHO5C/XHcqPGURA5f6zLBk2syvIblrfphKIRkBR3Xarb9z/bakQm6Fw+/GV9XwTDOH0Rr0qrXRnqMWqtbIV6o+4EpJI3etRVvoTf0aoaZ2ocVbNt2yqqnRe0XHzxH0DDaxGdYaKfgZvIGXY3Qk/XYeX3i4Rh2VGQFb3dYOyv+q1chTKf8kJN0jpaZCCVLS+oMv0bcCRV8lGaV/AD1N9NtzSgZQrYEmIRT4PJfI0cC/jpOHTSgXvI9yd5l1RRjwKTOwtNj2zEeDbUEoEoXhyfky+2k3Q+V4ZcX/hQUIN8URihsD7yCAWbhbAjI0ERhLzQmRper5qSEpFBI6o3n7rp3P9lGR8CkKhwibVtDjME89sruBkusDnOsUrk+mBNAyU7M32afAOip45Lbv3YjMzVwUXQOlGvoFcD5VXke7FKzMA3ErK6l+P1Q6TK+Fe4H8ETQUgslcuiyC7jV6GwcD+OrMGqL3cEc/xR4C6Me/e3lrFHL2XCTHAwmrrDqwneo/oXhFhOlVvxlsxRe+ooFjU4lXEvmRscqF1JqP6RigN/Ps8GYuDLiavxwzcWOjL3DUpD33N9TeFevxBcpGBumzlGLqfY6MkgL0rDqdXl2wVQMe33ken8InVB/VsTaCE3UzcwLxjmf4wNPiK7vkJWb2Dg4XJ1NdSwVF/vvUfnecFGb9/xuepMuBEDpbI5Y0EdAZ9fxKdCe1wKjbbGLbt3n+G1DFpjLXJE7I3sgxCQZJvfc4ZmJzkua4VtPjFfGqrTfpz4W6P/JQPtLynTx6TI9Z5xrZbo5W26W/N6lvJSjSmAmUAlXFqazCuJq+J9G7VQpD+MeqB9yiv+cTgy/A2FBv6UzhbHBwkm70FRhK9lQghTfynfLm8P6EmIAATYL3upqxEviOaTEhWQUl1xh3GnjDtF7r+nCdJMory+6p3XUHNDCyKIKqfuvRaQ4/jB41uLVBIN4F7EIw3ANhrqEsWxqBCSYxsvcCZoHHWd97KY/3xXefwSg4KIYNeJ/svfwoovUY8MZpN7zBnBWbwmiUFXjwKN5VsznOxKlFzFNdBBlEoprbTrKRugQ/wo5Pa3J5CwR1LVrtSa5rR64uAl3WXGvK7SMM1Du7uRPKS2+fP725Fu5/68I7f3fKGXIdjxT53Qk54MMB44f2E4MQXXkbDwXMPWVa7rbBQ8qRjNLojQiv9CGEfqEW0HmI2f4gXJETw4mxPFbp0NwF+Xqg2URRvr6yLfhon4H6DwSht+bpTZkSbuFA7N/hpjVNFA7qQWOPW2LtLjLEYpWsOA14iiBCwQOuZY+R5zrGAFOO1ayyeb4ywFyv7L4aOiW90182s4BzGEXVU4UNvs9QAmDvzZ+ECsoSvBFZZ3QQYz97QtouO5RZmo6iqfxz8buiF7vENY3nxi92xuuzS92ZJwgfOgYYLmufDhez7d1FJrHfWP8yWWHid/hdYdclqIn2SJt6T/gZmCV6aql6h5pzjYe/XzXIh9ZFMJln6HxkjBRuLgtVJIX8GNb/fnZOFU+uP1NNXWFK272YQlZJkTmwwkYIHZwIrgHlXLYOa2IZ2IS+89q3NAf2f9Rf9NzizN2FwlassWNtQNuTltht17qO0wWNG5251G1fRamJ1rCcnO5FwbtBRHEqZ5aS3p2mON3x3PXdKP5Fs+bx/D0Pnu6+qUJbZYPa9mgyKWfyLto4TUkznC2shA1c3pozl0F6nfhCZoudtZKtn14jo434mQ95UMvfQ4xtgf0BceSDFID3rWOG4zRvkEcrdE8vlpp6f4OujLVJ45/lFwVWHXm93U+kkjvKegby3y9AmyGbMkN+5bfrkzr6dicYQGv0joUx0nSOaM0ryUGX9XvhN8t/lMF/RAFeJhxko34helIg1aCn1nr32xmaMevt1MPVtvA0X2j249fWrqfVLe71NWOuuWfuokoFfpFiMlayURiOZUZW2n82S900JZmiuQCNpvwuJBg4WKsmQd/ZIJYjpHfA/ct94WLZakHyhIE3xE8jgkIN9vn64JxJzS1qeKn2eb7KIczH51pLLoMX6wfE+42AL9Ias3+N5OfzxPd1aJ+RoTGfKraDdnsNffnxJeJcgmz9q9T3lkv96IHML/duHNEtyIRLo0L1L/qZHb/8iQkPEXERh85dZiKCp3ai3ugDAo+zibE2vy5VldtSqhFnZ6lOq2dfDlqEd4soCF0q6vOvCnJ5B4UK/+We7ntRLsMjdAN0Lcs4Nm4EkGbAWWsnXC5t9PzBWTeFQSTf8EfWKypTWUCawoMaI9P21AmSLqNkYH0erKiuIJNVJadDt5Q9FtvapVDEhWbHJ/JsDCjvnUruMW4XUtiOonU3ZHHsKb3JvhPoj8ySacDfrpDzTKER0iwrjZ3v+3hAXL46yDno420JDaI8L6fj591JX1nJIEdCiM1BtNWk7v0nAYPBc/+fu8j6hKK0wPFV3iQB9DW8JIjs/UGb4tUMzoz13J0cjNlvQ0ko0IwBIi9xWB+mmSdJzk5CdEo3U45B1+VZli1eljfFhGT7clU9T98rQ2TU58rR2ahXGTWPBReEzJno+TL7fJL0nZspFCOLrRzl1pgbLdegiNP9jbSZdfV/aJ0IOVh1wfVx9/agwyCS4CGH2Xr/py7+L5bwkO3UI+HQyi+Hogvhi9bxoEHRJDGY0iijUIyFRwB7Kb1+5dxDkT1ct3kF1WcGDtnFKh72trDAzAABkvSrV28EdjIHiNwJfDNgJmCNU0aLO3WphFfNozDZIfrRdmqzTxeGvTHxeAYjdmOht5jVodAUjfeCDxIPd4z0NsX7rkUkPDx+PuQ4PAWjFxRee5RKFehmUPoqi4j/3Flcshy3AWHogZXRsB7KfjCxWqf4d2vHFEOKNGkmW8ABuYuhkeGoU3WMOsNQZz91zPrF1ss3v/G1Hsn18QlRFKhPBdPuUOW/VJiBXr4Lb1+9ZCpkWtQX2HDZiwlUzXQwXlrylPG2rXUU/lKTPsjY0RttaYX+Lo0zuUpqgaeQzubYpnaR+LO/Nv6509NbAPyRZL1Y5+MRqRzKjoFxoRTrHj8UsMkOXmPc7Hn6TOOOmCGvXNzz47ortyLQVhVGA3GslnXnfkNm3CviVabFbQPeZ5SyWxWbLQ12wc5HTJLxC2u6rqVaM4g6orTWS9op2nk/Ob+KTq7rpoRlQWLzN/04C1sOUuiX+WBW8MLFphNw+9V+8Y2qh6/rbEVTgLuUEicy13ZkP1bRNM5cPQltgelLyyL/UNGUherUYJIEm9KvAX9Fnsh8zTzOv/SremtEIj0xc9hW+hzJirX4YTLpJUe8cu3iVSmhsJqkrnXXm9+MnNUbkggDuFe+9bBJDag8hDLukjfjbfmuQ1wt2AmpD8bb0JfEepM7tt1VrcAHWQvcICWBHoOigVAA1vKcZ/CpDeu/OfPjx61z6Dv4oiBet7kOkhNxr7QjiXjHRXyNWKt25CEWdExdSW98Jnf2UTux+07bL76t2odyiyC45mcKt8PBGBaoNf3Le1JRtNZ/P5dJgsSQ4Ii6ajI4uOAstrTo95q+M9z9vIGiYKoK1DdtE3nyR9c1GvuTAkaR5dsdAq/onw51Qmn1STQ8l7q1XUCorkwZmrtwvMnEL0nK1lapOk47wKv5dQxXq0G+a/XtiRNfXr0a/OejyB40OPpVC6CHRPCDV3msWwVgi2EEdGkUZ68kQfM9C4zEeWHv/AQxEun3DyD369gCtEvCEouBVBwCkuEw+OHT4d467JzMPAJWQfTnoMNdDGVgNllC+RNKtjham3WlRLiEVCG0WYS65fbkpvhn7yUav3Yz7888IOORZiDGqnuckveDJHwVlcB4/O3nVkEUh5ZiNVLcVggvyDSOo368CGFY3+SK90kg888pIenvmn71sIaP2D8FrRpiKQCxeAFoo98618FInRXvkbz7/p0M6hXYhTgV1VSHc42+nJlKbcrnHfjtWiadrZdU38vymodQ1a5ZGm3q6f0hdUQMZDmFGclHfIJ2jA7/5qmiKBaCdsLerTr7ljhKe9PeaqFVkA+z1XOqKh2dyfYRRvRrQcq6lsT4gcAft0Ju8YnjkD2SnzN6PWrtBedhbVtyfarTk0UaKrZwkHwBSZeKBiaGByFLhmbOklEX+pQgPoEA2bSOBOarFCCN8TkU6DYnsCtgYsUAlRkZ5RULJDwOPgj6pBtq9to/ujvsED/cFc0V2JsitQOz59sl3j6FXmLdqcHvB13J5a60m68s7BArzw2M4gw8Pc+fe+0G+3V9SXLnFEzCrIs4+l1gcV/BideaMU8Jhw3C9lEu/mn+ZMqhDG5tPPNss8ddbLpu3Ig9GkZjWILT+pv4vyVV9quMeNhGtwzC2EjnqzPhJJ1TvnnY4KiAz33BnZ2uOu5GAG2BZee50PWYSu+IAB4Fjwgb2KaIySCW6bmqciM9qPPYdZ8y61sXIAs6NnYj4Hph0hb4xlJqCACjFsPp4llUNRmrVTJVBAs5uxGOHEZz9/WAXr/PU2wettOmlHlMKfJANSYu7Y0IUHeNiQTp4/xzbksZXmb4+lzvoLSUirdXginWOmNqrtmWLP2RknBhX/cE3+2pDYvyVdwt/qwFJAsOLjl1dEVAFWu9/Xq5uLoWf7+gDjYiAal2TpwH6TzLG/tQtSUyWAeYXEZhm+sx+pBOr0xjgA3hEobXBqBoxwRDSZT7zhZu7eIt7nPKjcmg/3XLfTPpLdUgm3Ku17Dv6N74LJuUcEUWIMolGIrQoMbnVM+kjwUGW+9EO8qy3Vg3hT8pRXg2Svbolm/vfOSa4FhtcfGAdvDxbb3q1RpBk7OUCMP0N6ciFPjDNPOc6xLfKQQpPn+1kQzu3gZ8vn3rUaGuMy5nP+HEZ9zigCoBNi5mvpcdaMJcGslyz1nupFKrRy57LQntaancYDUFsbo3J6NElGCQuQ5oGkYZrsn9PDFWoDq8o3sS22VUUMlXELjvGcLEt0LgrP3xkuNDUtJEEHTc/XmCgsUc2b6MTgmBEEtwShJLeQhIxYmxkhLc8LJsedEuyPt6WbRSSaDsEKfh1K7igeDJfiC7G5lIKvlU0y8oIKc2PgJ/zWT8Sffso7/cn1E730M5HRs+iAVDEONUjKbG06HhphGTnWWPxYKRoDLtH5hJoKZQV669eRpVzmLZx34j7uPwhOVg1u7coM1/iQ1wbFv8lOlioR7vxXIv7bCEY1eHyYWksBT7TEPfRxRLhbks2Fkh1xYEEuD4ae3kK2ln4FJU+8ZOEMh70156jMbISjL6NDb2CtheQtndq8IsvjmrInvIkYZCC+MOoD0eKhZ+s6s4RJGY5+vdeIWzrpAzSU79x8Xtcq5i0RQOzHoEPVJqfsX+2a/Jpj9usNUL/BuNKNc1BXL1OViH3s36wgKIdicjJLv58MemfPOtQZoh1tq29KG7QwQ/nU0ejVid5R26Ec4DfkdHGlJOiWOb0BUnuYLCDrzw9YnD98ePqeaBw2B7I/uf3fbj8TTTeihebpW/X2uGW8nTvPS8e6cvo2NcSl5/4mgvH4R6OjGVndnfl4IwqrNlxRr8BdCM/2UEr1omRnVElmZMQxCp8BA4kj5fvFc5+hhhJfLUBqSRc1Vhvo/vGzu+VAytd1Kty5q4Mha4k3CPfIyfEY1559grUn1B1DGsnra8RzOpk8cPq15YBMkkvB3i3QhGRg4xjiFozr+xl3x+lNU8OVFbRi74ZoPF7Pxjyeigwo+Wgx9LVjzURA1iQ7KIDfltFQRhRLiKj0KrzV5eZFDkKTkJsHHzPPq1sB9FLd9eOOPooQ/fmCQIuaiqtohQFx204m7srU+XTpI/KcwV/cNBUd9fSbpEMurw+hPxPJev0NMqNu2MFYjkJDsb8ak0VnaEnmD6ayKQNSxlA3y2Ln6RlkbfOJLuqt5xqXezwwYTuV+Y6LgpLZfoD7W4XA2OP5JG8alfbYGecy8oyAnKic3VxZmoBK3j1e866cClOSoB11juXPiHe553uXs/h/9vwfyFeDeh++wmxw/GtiBrXgYbPKep/YKviOOfqMYWooj1jxjrKVm6ZKpLSXBp/xhr15mQUL/xeYLwXVN+3sogJR3g/3G/Px9HrzhCisR0EHENTdocp3MLy7W6LMyfgBFmzT3W8uz4EUi/UCGlClsytHQd+BlOS8w/yqkzGV2BofC817/bvLip+EHiZPPjq3ha+0sm0bMjlk+++k7q2UHyNl9RmXKVe5VMvc55yxgqqqA6eEHL1DO68c7wTbHRIXxJ2/rTH8oG2btn59B8Mei27OdnIgSzKTzM5sCPBd+OQuk8EdFqAxn9IzAr9jatLI5XgVOT108XZmXgR63bDozvWfmjcP2SXE9upTCsymJ0quOvo3jPgDpsTSM48th66MNnLn0CDCXQ8X16Y0X+VaNbmwN8IR0uhQ+YFxfBX5TNP3SRGh5aaK2KdAzuvL4nrQmAwbF7AKf1asNqO3cWPRiSm6rgyJLF85wPw+vrpy/7QqCtyEFMpgIIqVQSZep+zVT6tn0xiPFW1vAKS6a85qbOUkdrDHenYvznOnfBzKILc8uLO4dv80KlER6r4/dwZzdBTk8+A+wyru52mkxqJgOwCzY6w48v2tDwlw5GBMRSzmmwPLvNip+KSFR/3paTQrCmWNgxFPpKO1i9yevbRSR7ww6Z8UMlAI+4OJbP3ZItl9OMJdFx4UQSsWRRt9G9Jjmh6STTh6bT7B0vMRmUlG/dkqbjflTA1uJz5QhIGKgObZIVUlOz+39qrisqhpzcLGse9aczCr0WhK2+TpzkYd0vcL/ZQPLcmA4IztDa3bGyTLrqITswpStuIfSvcKE3fehDwcLBjMm5Zc+kJqSxEpT8cRlDxQrV+9A7tgHB79lAd5AwuYCT6MVmgiocpq2pMrQlxHshuxxOjoa9vZF5xUXHMppBJD4M4XD5aKj008YINZ3zPzNGSKA3QbqQymRvjEYvymLE61OFRtDvHG4Fyt52g/PLmQbDYCL/HqdzNM3KvU+I6dfyADKiKoneWp94cS46lduS7ILXPVDPJWdV71j8FfvN+QTadYL+Z58dyyOK5i7uuoipJAXptfblLKOtvaVU1kkTr7Q71tlsLnQwmVJnfeoaboHcpEMsiirGfEOn+q2I9MviaIIo4Y6EIp4PC3U4pH+SBD7R/5QBSv8PEFbeZ/O2pRFC78y1wIvYDSM/4eRAY7sNocINgM7RVvolXrV9jYICwc323MSZo+A2TPLVBq9jXIuZhSp2H8Q+hzFJjbVcY+ydnMTkyU+l6aMwe5WqF/8+xiRX1MvXvgz1dluFWCLxfKAcljtbk3bHw8aDquvavipI8MgD49TiFJVrdN1rcBzhJkRxT74RAuhrGs/TJN37PyKc1GLwFT0yPxRQ77nfpTP+oubIUL752bXEYab0kyonfnTXCUUeJBYU6fCZlJXBAjxImkuz8BjvN0m828lV/ssMdHbRjPGzlOURvYOAjVJPyMcDn9Q37W4EZtWOCAQPWYiAnyDbT4GUBCZCHitEIzxJ3kzH2qbrERZs3szUVCVw0+tDUrxBVxpPBQ/7yKd8rBO/z2UwbBPUZUFRoXWC8dVgIwzvF/96QFk1t2ruS/cwTdBXQMYaHiE8xdEmD4vG/m+LrnToR6k5ji2PdLt96hb3UrEj5AMqiRuoE1tZBKZIlKRhQV1tBjXMoK+4S6RDlVZDsoTfImIyBORK7S4iThvypkpTsKGT8U9IpUDc6QFFDqmKw+ig4E2Kdzf1CzwEePF3Xr0RSvq00CRkZVwcfYOArRLMYuNLIB4mAUuXx2Ok2DET/fRn+0VhK1FpN1vqumdKtAE2HNIdLmnG1C5rarGuxLlHhYS/OXVNnduqNZkdI6PEJ1/C63eC6vDmTF+D7nQXqofOoD3aMvEVPrylzGz7Hf0eQuxrpzLB1yDFkbuXQQ8qoC705rXWp54sH8kgZglkcbYI9EGv9Z4MX01mjaVuwuNhT1L8JYyK34ZcjgqdzjVYNxUqaQ1R25S1ptyWwKTk+4bfV/hBEMOYLuX+6VufYonfNv+jxUbNHXdTh2Whx4QjEyPqLW0rLyiSZF1xwQ24EUNaaKPq7xoLC211ITrGhzZEzuKpzmzTGtymQjrrEGJEV+27xVTKNOP/OtZ6oLlWaY5HnUIR/E2xqULJR9SK+Yy6ptUuxaovtADWE9HLJ3LFrBFLuKXA0y9w44Px1zyxWuJpUDZEOMmI6eMkUixGXFqYcRi2ij++6g2MWocOyY6TRoxjMj7DnSfY3d7Gprl1cU+ATtV7+GSHG+GEev64xe2yAqH9mkpkobm0SxMvmy6tZ8b7nd5/RbjjZm43ZoPEbj8odSbfbJStOq1RXrE/l6qTGqyWSP5tjqr927DAVbq+Vbp+UbF6CmMZVlfR6bHWmejD797aTqPmxKr7tjOO+8ww2b8twxPscIc+uKXjVI4n/bBdmdTRWzsVt7Dyg9od2cw0g1rjyf4DbRHOpxxEdErPvbkiX/25J1i1jGWAjuFhYFoUmshM6YU2XfBoS7jaVGKu354NUhZbHJZdrUkJHEfV5qJy+FlC7la3AnNc4A8Qqp79lEF54IA7+2RwbGxkIVOZb9SWmizB34w7R3SfnwD5Od5OIOOE/MGF5DP37O4hsGfgDuyzANj6ojcl/mZpRisfI5cvubTBmDTmUnT6qrMSzdc8V6qSSe7HWNlTMDt0Bs2Cs5pG1XTn23nvntwi1n1qmuFK8ei1cDBe4xQ2Qku68jWib9PZ8EEPbDuyEo6BD4NoQxReYBai0inWItsLp9/HA7NlORWj1iHp9e0+dMB2smO9JM8RonAHPqwcB6HDFVp3glb1lHny8CgX7v2e57MUAAd9i4+wLanyD8AJmxPGML2+zPnpx8fnRqrPu2QcaQFufEO0VL0Z4vobMSeR/IWr99C53Gyy0uQusnJLDqsvYPYGOLrmshfAxFztBX8SP24C4wQmkEOWiUkJN7TJ9xKLZx7/Eff/mfAzQKrJACosxYfq7dGklqQfgTOlXecRc6WyLtO4HfcqoqwnK8SCPSYqEA9QXeMQZ9PCL9jctkrZIGl8252Tnew3fN7zr8p/XTFZTvPy9Zv56NDGXajGZ4iDo7Y3mnlcrHBU1ZVRpkREyQJfaoz4yH7yNT4iLDyogQN1jwQK9tXhsASpJlawmknN5gwFbi/XUQhPgrcw3HTrqbHH7vWSAiMJKESzMIwXpn9i+dDXrBIwiwqD5wxnXPK2+TfV/J50ntkQad8f5fcAzcl/MZT4Gx5ekZbxPlX7Qdzwd4J4aGDax492oMbNvRtJnmqMCyjX1z1J8BAP7KABYvIJqUI9faid2tA5qhv4gY1THR23Osan4WCW28jKhOY817eXhaxdw6I12sLh6NT/KpVDpFa+GTnDF+5jlHWVDpjtkKkvkb+NYaaeC74DJrxlcQjBjY49LUXV4netBJWCB8JtrPANH7FdtCNYe2zaligfC4B/9DpNeaJu0VJnKcQQT+QA7d4zRzk/EkyZyNv8fIfVRvd03JqOPo5K7Uzy30w70oy1XFIlvZ5GbRjTUVzrfoz/ID2R/KxEdlPLW3yJp2I+0SNR9sYCpchQFgbkToM0POJzE7G7gOz5oiooHed1+NZn5XLNUvwVED6fIqMf4QAHXS/4RqZvjLw5QOvC2uMgJlck4tyqbgNwrRvl3jevnlupp+nivFeShd/KGSTk/lzJnjzri/0z/196SbAu9FRXr93txp/0gUQleXYfeoXagFhQUxz6jkOaGpO4I6E3/HMaDcBlA5cjTfCq3dhvO4s1+ifsCrYJFyzvzayRG9PliTP/BYQjq1NmLZKL6/2owZ6nZ4hHWEs1WiJJ609Un/bPBLp5rzj71ClUfrP04SvU5ahhzX7nacKlOBdXQK0pkx1WVpzhq1R5/NplRGotAPoywMuUtoil4YHIpc4LlxBBUQeG7jgz/Bt3kPZCq4MvB5CzBBFAP7RfTS8hWVJi2GzhSj1bamJ+d0KbAvHzBUp8CUmIl8RWBPBqGdtFoCfGPgunhUr5A9tS79WweDm1jUZqgGB9f3ucqltncI7Y/gFoHyCIyKi0fFVOdiAxBjoP2XRcKhSqvqND7F7GgjjhI1j3tAGd2MyVieDqiIfN8JQzEbl42E1ts8QrwIQF2B5lwE9wuj4OBFxxmYyMHeW2/F15FxHulH7inC/VGsSJ0kqFibjpKzY4ToOnKbTOMKcHGHxFbVdppUb/1s6mUTipRGYIRlePPecfXIrD/QeQTFEWl08sk44ckiljzpS0HQTyz4+gAkFjPqX8iGUpDPSYrgJzpy3wECpfRA2Y7yB7yOoOz5Rt5KLZm1gJqzQebD6I8LA/WsknXgTdsgRQmyyFniife2/alIJQ9s3/b6IingMUKC0dTJ/rwXYCR9FdKM5pXe9T3jz8Co9CmvBztouky9UlyQmY2KPvhZNHCsYYheAb/AjcYYYUXZHCjf7gGfqaA5nn/iGIpBPA2Z8Q4rRLpTs7q8mvPtd54fpQxg6SYurqUJiDB0TnAGxt2V2zxIhD4AD9OMb+8QLcBK+2UoQKlKtYHrDbAaJx+zTz2lRWlDaudVRvs3/5q5hDvegeKKvrwpQrVQ7LXsFcwabl+cckr+ODEMb8tiHZgVdxjKoBuyBzrCVPupVzSOMAcFvDXCC/Y3o78tarLqZHSBYqYkrilqH4d5+wxAl+8aYw6Q95RAvTGRfK1MXQ747KYMqOpqaz/WJULsDffxiuI0PwbJig8iXJFMN800iCTMhXUSJemN3OQ7+5kNTfHkp/QTflNVFReA97E0/frQ/7WxYmEBnA5ZazRX7zkJEtchuttkGqxQPCJozBVqbGJoW6BDO2uUajKeJwmIGSVG4GZH3GgkKmlEvyPRCmCRMG1Li7I1t+EmFNlpuivfLFrTSVK5xvwxQwOGwymepwJhIoe7t0TRvhe0fmjspim0c+AkIh379Q6ZDS16IyAW+OZ8Z65iqszMAAQ2iawuRvvJ9v2OXD0Wzdf/KuAxlG23twN+ZqIwUeoLBiIwBcGzBwBoig0DhVtrJhYspLlExT8RpoXOmxG2SFUNDhGIx0rsllZTbvqTr8aLZsPQR7Durr4++JmJXjcMHjhu6X80VYba6klPBYPtWZh+HRfOqGCdP3BtTp0cUMvx4w1kMCzdfPly68h6N7pwO0C99uW6ZKXZXFHX2OA3xsp33SaiQR3z5lDJHG7al3Jy3N9jZYEZsB5DvD3DLrcfgTYmxRXgussKDQKjgX1OkRJBNFXe3/SJylIULocEmfs7WGs1NSWzQQEV8csrVYxDeIj1DMKXs69bkYWIUKu27yl01whs+elV+Qgn5w+vg2J6LZUQHdXkAfZbpB9hcxmX0/mkURPEbxJXpDtdVEFepBCnrY1tUh2EK/RtbWEyU28eoWfapkxaM24xLG77kumb0vqu7x+Y5US/uzI3z0yHRrAFHJnGE0gtgJXnRcxEYMcYJylmgRieFUcr1IB7FjyoAMF/JLPSi2QmThjdPueTYCTf8tBFJh51UrOQLkzjspCYEyvNvC3QrvvOQAsld67DSc59xqmvH06OkSVzpLrUXzCenMDrdZyNkOcfkixiNUWB1MzaBcmBjaujgVrQLoDxXwWeSnRnCk00JfsuTyX5WimwZ0JCJtpfeOjK8+d3asdksfndN+632W8OerhqCZn2rTgTDeI/HNqezJ4B7l9uBUn+XHu3nY2w/fJ5960wKjLYQGxhnKMvG80RV7Wi9GsMXkBGI3nHm9L9bt7/HDvGxrwiCg7PTU6Nkpr5rRxB9sHiCGWMM4boybURjsaZrwEPdGmrZHBlQCgnvXhxV99CNKVkAZRwRvYQ0fT60B8W+5Ur9FCkS0Uj9qG6uFH+tl5syFLnQ8LqYeDw22z82TdHMpIm4ZDee9JapK9D/GG29l3yUQnT2nXpo+Inuh5ZMsaolPkDZpvTga5QFEWqKjlG27DWUIf0JJCyv1xdbQ1Kh8FbIBTJNEr3NRLiEhuSkUMt2U4uPxmp7ftN3f8cPhJgr789tb/9g13uzYdOfLLSc3ZndTu3KLFVdNu6Qb6oo6qBrz3E/r4+jWgnbb8+AGcJntnAF87ZCqoKDSps/BMvGtEYe67sv9WktzQ6pNB47cq19Aw2D2LZatTr3RsyQPSd4cXzhdfk60NRGXzrpv0ByuLbFXXO8k0lwwe8p9rDeC6S79HP+EizYENvtcSOKIJnRbWIb2uVqHyrordE4WiPt2Mm6aC7xr8YUtWzF2WsP5DAlFum9xTJ4MdA2fqGkesw6T9Ph7q/7Q5ft59GlPig/onNJOJNCOcRkJTDf6xzEV557OMYTwObeUMNCvkSECO31lQ7CqYB1gYYG5IEOM5eYnvwXxJfyoTyceTMD27pxEWMm6k1xnUQz1QURscACO99acW45LSEp5IDu5L4g6NDyPwRtw5grfJulyqfodY9Z5HM+fyUzAaSKCetxXx/EpmNsxUyPHOa8g7f+mHtk/HKWHY11gcIlRqlgigCEjy+9M+hb/FLCgNa7Zg7LgA+arZsUF1biSQMPKrHut7O5tZ/t8w8OBGaiR9+e8Rjgy6dbMLCOD3DokG2GwZr/jKBuYhltcrjk/PaUtNhjsUHZ8ANaZZzstDa+pGFTMRMBHr90pDVKTBQ4/k0yORAMDjmp1JjP63d2mRE3pY0BWL2c/CA1B6NH9kUmiYB45IjOsnrNfPnCeo1LGZE056P6w6CEVeFcHf9qodOJFMhHodAfim8+BsUGDY77Tyr7eIU6M8nhHZXJD6grKeHZ75WY3mvBys4A21P1986gXFj8loqkSr2fx9LRodjR47nQKQhmybZMDWUTWVPPsWqjSHy4Ri6vdW4lusKqR/3loY+52gD4K92c6cyjpMIlqUqB3nuiAf7VgNU+7O3C8ZKGCCnEUvv5Zn7xiNwroPCD8kTWd+BrlXEeU82ImN4c6kARRLpZitk+4mW0dCrcNHqO1ZBD1QeJiDe6j74THUZZjqWx+ibi9nxLH0XxjqMBKPnn6RC1nt9VtyZcF5TVURy/13IE/HfPOWC7GL2adqDJsTkglWfNm+0mcdAi8/6U1DsBRFFR7lGI+HUIJbeTJTe0tuSWTT2fbRVVHBd4L29O/ADyiP1+DOToGPD+MWU6fOMwyJZg5bOQquoLYSjTwx5CVylkz59faJ6iG7FYrOv2YPV0Z4MRRS85BEu17uqARe5bsrIcnQ9winwCFfS5LXJJhlZWnE/u1aDZdrQDVHp9VUoZpsiQcpsVnfGP7A+Hv/DwZiD4RzkrdgABy9tJmIIE9OH6bsmus2fK3BxnMS868WyJ13h7UyQoXqEjRsw/8Z1oxDMtwSqYCuVdKRwt7CELUQihP/NMN8gfIVNySIXRDVC6LPFp4lztFqobFdh//NpRpEqm4fWPoBkkoeSDMtPH83grFGlftKHd8yS7Alt1lwc0pB7r+zFsm0my4F0rFrBUZn5Hwy7ITTLQdn+aIOYS55LErEMQtTKUhSLs9M9fkOfXqWTe6rJFKZVQXKfCBZmlGY5wc85tcFaH+S4C4kkIOIYnGG6Pbf6iHy3z/QzGkfDKeHlHKooDgmN9K2vmOsVl7wN9jcyjIlheElQmQE3zI6wGBNxrTVW8PWrxIDbuRXWhPXwGOWcspxa99mZoglJE9DV7EcqeZfhfzekyeG9EJpNy9+sYD2+2ecy6Hqv3YGdL1vpLRm1qGbPJh1EI1dX2iSx+a2gQ7khq8bZ4WjEvv6GGrHidY5ov9sWEGg1B5HN4IRaQ0mhbFC+4tld1RIOzEcS2BKW7QP5qiWU1fb79VnnufGZ1uDRWyhaELmrYOtFzhuaTB0rSKzWsfQGUg4wihNIZRDDpkEGZEM6epWJuec9g773yU5sVh6sAWYDu8bBQ8KVvcj/PuyWChKPeJI8wcz7n2T6Gxn7l+0XXWG28ldjn1e3NDJP4r2Hfn3CBz69muKzQv1NYHuwXUR+SXGJeYl0yXNJbPGY9KFB85eGoh5mfrTPCcEIWXxQhj7jeNz/Nuj/Q6SkCMiqvJst6JpX5yH2ME/Hf9///33t/13+X1yCc3FOhepvjMB3/qInHXNBK1q71rtXh5MdECKHjus/slm7223U+E5D//i1hzmCYdwSAPP4c8O8getxtGDXsE17H/fdAy59k4hza8owKF+UL9HZt3Xw7epmGEPnpZn8KnW4a8A8Rerob2FkbxkcdSz60uPINeOBUf+R1NWQG+YGG1/W1q0YBxXiC8ew5qPlf4Ig8+9xvjCciXzTJgxdWOBk+zl5woZQZWr8gst5R+SkszD6Zq1QGulD5/Vng106Ox2sTNeAiJOkiS2QBdInTa77VYc0eu78L/z8Qd+V1uwE/2E6yV+R/5pq9iHLj4+3S2BP3fojVtmS8sR9emPQxOXIj/nkkLXbwL4m4PDu89eTA+39tpSxH5zAkeZpZsfdS4r/ehT6AdlSeEJVaF7uvCg7i168o6TwlVbc1kVHDl5H1TkHyChgodo3nyf3+SucxT1mWx19il91+ivPztFVmavr6UXvf1hb83dNYZTZOw4nAt+onUH1yxYYtibOxk8oYy6FVty2NlPBGMnE894LLe7ubHinfkU5rLrW+6KDUcZNlh+c1eOoWPPr5KqPR/4ZL5G/8Qoe9Hfriu/gavmtJ29kgbHPaMznhxmyBrFy/HACuHXvn0+fMxT1VnFfy/X7vjfbTTzvfHu81vDblztl4jlZ1HdB6Sxh5gDh96bfJ2UD38VCCh/30hZDsWyWXcHYf6DKCAWOqIBvfTIry24xNc5tstrh9/vxEcuO/D5/+L7ti5blM6ncsnArY1ExrsQJpZu7cGqu7BZXwy4zsRuf2Z27eNo2/hsSX2n2lDaMNha6ukSqz9lBqRkZN0gWhlnBVwPnXsaLC6Oe5IP/hXHIePRfVXwKxY12sncbHfUdtL5I+wYvENr9EwUpBWUpLGVs+zzIZj54OvRBYaQKi3zWfXn34nhwDsRst3sw5bShxH6j0pizNL57J1hrHLi4/U/PKf/13NdBkWwq/eNkftchQJth+qhNI0eK1uCw1XkR6Ij/MFTBAETVfqZet+N/g92zBVWAIF3ZTWiX3I2FtksIZuuVR+8S/3PId/8KRkSGZ4bDl5U/VvkRaw/zK4p4s47EST1BOQY3hQDjpmNaXT9frks9VUqD8e2g2WXnW5QaX+lwHZhTtcQxxJnSYm36StHxE8o3z5o5jNv4LtTMtKv2utR3T7Uzc6qm0WhkC7Hb/93m4kQS3YglGF7VDeOJngVrLN4sa947y8uNv0UbhawR/Jswsy7Pf5k5dwh43c7s1BHu1eiKhfZ09ao8nauxe1+/bIBrlpM6XhvD55hk5+EJ0j7YdA8AJeAxraohkHzrzso42re+5F3bSnXJJpJkEWbmkn/tfFKMAZYwxPnE7R+U08oAXwGfVYkCsIiBzVbVshBShkUsm++43SuJept9HbtwOxmjdh7jz4J9MT1lmnBYCi/a4w6tLa6FbY49lBkf8QM7mCgCknmzWT784cjYprcVyfDqCHNEMcw+Sh6r5AfqX0BpEgdXiZK2Ddy+bKPDYXBcaObsfPun6wF/XW/7RiQlE28bt2dL3UxPFCIkcLktWdTp760xBxHciy5dhWVnTDxocLd0Yohlvze9zWlZG/Nh/hC2P7rblfod69Bx32gqoIaXj+yD1WEUmsdOCkN5x90fVhsBgd/ORjeUtkbevP+0mRSeXc6xmlb00747lU5tJU/2KFXmmv+FU6EZehrXdW50isgKXcbEBQNXJMxDPmsCi+RZetRZ0RN1EwZasP6o+EQCvxWIGOg6MZszHlrLz31ghUUpWnG366DSPEsMnHg8Hr1AttfRBzNomisfnSvyG1H9aS3953ZE/9d2hLs5TztIaP1JlJq+9rSPGyAzO2MNTUp5Fgf7pcgPoCk48Flx7SKFmR9mno/s084lPN0+0crsGOg/9ZHdKv52PlG27jh8J2NgvJMBbDqE5moMGjskRA3KIoxTLzzaJRk+lKcIdt0octDrF5rWbVr+0+VAhE+kh+r5NKdL+rQ66dzuq2UIHoeu6axntTN9ymmFFkgqAT7+zeGiejDH0UNGZL6Iz1YIxaA61nWVluRlkUsdAapbZ4VAM+eo34e/KerflVNZHR3wXNs9YtyX4Yq/oToazeOve7fpKGy5tDA4cthHzG/668uC2GsDMZ5hxQ9eKLrtdj97yro8s/1cSxG+sfyXZh3+HpchiRxb3GNB6KcTWmKC44bHUXFDKJfGfg90yr6CdaaMtWdP2Kxd1N75IHzjx2GqJGzc4bXyunBbvbnS0zCRAd+zOagS6WSety36X3cbMnjjhRx+6/yoxseYMXaMGyhovxyMoT3c9HtROHv4RDL7AAmSOvLja4uShvWoWeKgiH2CcNY/A4X2N1CnZ5QvtNvZnzY6Aj/7bfCXxYPSkwM8GGdbuQOr3WnFa6jpd1u6KTcP3kGDZd3qqmRR9GPTkl1UqVLgACUrCfX7MWB0x69dovuirF/Xmkv03fT/n4w/HIYcoXVP3R29z7RD/CwdbLsg3DAu6rPaDmyEnPo7rWajGpgittLCfvsZX8cZVj0YrXo4N5mbCrX+STMBngNCDZA9bmgYKz2H/0G++tjr6eU3mvUGIDcjAwPqoGcPZf8GlzGCqlyNP2mVXyilGb2Pr0Lyfcd/XKyg/LCWda9EbPRVcF7/gSyYN86B/CXFyHT2Po5kTliwbdTEa0qnkOtufnJFH6LDZquhF/kTgQhD5NN8fse0yk3Qh+8kLkv/TbmhyNhf2UeqdcJ9Acwst4a4ME4e0juJrQGpYCG8kOv3QUjpSAGv39nOOgHyfMA6YEsPMXrnlK3QGa+QjtaPDePc81z3rKvVYtUJsnuRvhZzST6/QXqSrW5vH9O/t0/44Bw0m2kMGOvVvbP5rwMzyeFCP3YodTDW018G7Du6dTDIZSoETQCwADfqz3Htyglo6hCEHQ9hn58nX2nY0pYXf+RAihxa+Bx+KH3L1IOkz2f+4My9oIhSqhxiX724pKn6cPdTbZjzH4UEE3V3Q6BA+Gau0bv5062Ed+ydZsZo5QjFhrvcHIDnECdWpOqG1PrGihE9pJJ3W+E02/on2HSlPxuwfD+X7W3qgb37e9QM46p/8g8RhfF0lF/hPWnWjQCeEaITgPZUY1U1N+HEmYIB79228liuDvghbVdztQ20Y48yTj+GJ+5pQRB+JxZ0AWohzDFWusxfk2kDNOkjrqk7DVuE9meogeRTvsPd/hOfyp/pqAibS+/fdjjp+fWmOkNGTOZBkjnxOOsn2cnzPxD2TvZ+PiQP9EAyzg1HmhK97xAoy171wsByciC+lIgdCSpx/uDpzS1PxcsqhylCsBNY3FZQDiTWsLz3EWrm/z6wHUEvRU46GvZeF1HjraNRMaiRKceXyhd/qsY3ZfnnZ+TgA9LiLMlxm/ExFCUC1kAEuyD3sUpmdm4Gjy4JSsFIS6ClmbnKWfcKGCWJUV+zjBAxrJuLFl0mSEAWAHvNiU6KcBVispD+1e0PnYsq5Ws7jM9I+Q3f35geiJ+X4vIRFIjCr8CtaBzmMxvEgKHfO8nKwQmZDWqd/QZYQZe0by78ne9k4m6QFq/uPi9zxfznvwDC2w/qLtKTx+u3YNut22v7kxbDTTu/H+y5JQbJkDH95JuehjMixTg2yRx0PcM2MHjY8Na3YG8Tx4xXaRO7zMFJO/JXN6NXL7AWQD5tUumabxvmj59/eJwirb4pvnk3t8Ywv6a8krO/Y0iEsK2Gn5SvLTdo0QQ8sTRZ+Cq4SRVsIgxovVsN9t1wNk2qij5xt5ymiRGTrzW+kUAqaPsgatuRzTn5rIRAJZkOnGxQc7jHjxZBE3kl8i32E0Yi8cS0wLS9cE+cu1l03v4fh9+K5tzleFx6AoNFPTN0ieN22R6i91aPSB7jz2itBMsJlBozwirwo1nROzxQnSTPREul9XyR/iC5JwTfVsHmKEN+T176w7H+WAZUPPpLXhwf+gKLe0IPxg+I47xFU/JaHrbIX0zyytPUMumENbRzlk5RujzdxOBWUFckjIKahaRIId4LGwK+5IDCJwGdeNQDyVSjYheVWWZfheVfF8fA/VUPien2XnkHIB/HCVuC3Qjbd7thjuwbt8dFjhyxoPjL1wZpEbj+VN0ZPVa7Guc68R3pJ1l+fQ6TWJMGiWIReG1WrWIfPp2jHY8vf3VyYOiyqLCaoqohoH7cmFcCTz98vJT9OU0/cB+A9FPsMpD5seKCJPdBsBrcX6OM+4RRmP8cd1NlExG9NxjHTomR3Z2HwnhCs9a6kVnRDTidb5AHbIzFVSpna4E/eGqdGqFOIahIMrqom1aty4YsIEClbdEv0WtPhoSt/iwAh7qd3pqwDb/qkai7BYmLFcQESvIyAsd/JTPglinUzyfGBokzajy1MtP2y4SLLQvk51zK2KrYYkElP8HUPIc0EeQA14JJvA4Ql3dibpY3diJ+fFcU3NaEgH0LhOIMvh+JFN00jT1CngszNTZer3QgM9KBXYqTZlRyIXvHYO1T/ZX4Cwg4Zlksd5f60XXa4q0RMUN4EH89BZyJotcqtfOF1LMDeK8yDh5Ivw273060Nr+McCWxbcvrwk+kH7hCT89IYMkpduhtCvE4JfdZyfZ/n7Um/qwzD/fTkvwDkuRPPZOHnfu34Q4aCiL1XSQDBe6XXtCqpGx7LFC3B0pxYXeuyMP1/YwPwAe1IG2fE/z6dJX+5DRyM+/2wGQr0oYTYgFef0R5zHqzl14j/LQvpJEM9qPamRg+YcZjsLwF7KN6qKem9y/E1o+pd3HnxnzRsmBtjXt8o14N86cVHPE+PLFh25vzP7LzYNilQFuF6Jxk35Cm2GeiRwtUKV9K1q0Bh062Df4PACZA2b/ob9Xq+2jMlnxR64xhU8adVq13xR/XurRfClKw9gTniv4IJkq0XwtFzdop6XxTW/2uFPSJlHRsHsSu7njbJOHXH6CTqu0MXxWM3ghpxnUHbG0L72PKA8i/JW+zRN8r05mCOuV1S4zraOiCunVQNMw4NIO6i6r0dzjZKtSrPoeLdhRcf4MdncelhP1joL+UGA1cwPcB0CS5iM8613Rjsgj0DPy+5MaEbjOCH0Pno7eZSQmd/8F/DtJ1To/utYTgF+jrIluW0D8S5iPQ0+jU5WaCPl7Cvo6NZlfnkrdJYjyLQoWZSPp9h053hg6D94t6OEAfX2L3H5ACG/2NXBY6hv50bGtO2JdNrUu+DhKWutHucAk8dsPpm8lbTSY04kDA+BFoC983fNY1VNfBoyiXPRFG9x5kp5HrnK8otQIDAx/QWsb0g+/wCbAULOjAa7P5ceS6e72iIQaWz6wuP7y8t2H3hZsOnz2z5SF+v4Na+eZ5txF07s4vsHAz0UoWqGS7ZX/MY/AeuUz+fHQXBKj84WO8R/X6Xq+rv5725aM0aagglXWtc3iM/zSD40WsjNbAjMT/0YGhaeAfsX/rMsw0dw/y0yjT33pQq5sX8PoUni5oH0J6FK6Lv2bpgtREXRe6jdHdijw2YVcXu03IEwfAsUbtCv3sYrsUXweicXpQC94K+P9TeH8O8tug/Bv6LgWo+KA/dJ/DS4R9Bf4dgm7dg1T62CTgk4LU7wXqFRfB4ywa6UZcQap+P3eSa52gUqugdT6rMf/H/7fwexK0OqRz7Rw8/YRyX5vHyIchj7v46enAabKD/lCyy8mNAWpRR9cKVOLXWUq/moTtkbS7XuOk/DfK/6CsgK7eKfi0QC++NtP/5/XqFNtL0Ucv6Hfqg7r27zwprf6wpevjZiL6uJj605S1Fvg66u+RN3zC581xUjOorWKyBnZ8E3y9Bf808TIZGgP9HdJa8gFQq4Tu9hwLPeHSC1bnthLGVHRwddKsCH1IY02bszl7/hHoBzFtO6SY8V0I/bo9ndej/ubPW1hsn2/ystljF+6+6OVs1j+ZA7FcyS+97FD2GdKSNFDw+GpE/l5mSXcMMBXczfKamnYdOvnl+8f2GfYv+to3KE+7nM1m98jksu/lvXl/TrTbgOdd02PRJX/pv+k/Ye09e8ybMelvPN/TRbuQvIx39bBpzYdyLnrjB4xaLped0bupxdNdOMPhx9G923uDe+be9V56qapZCOyeCv2+YK8N/uO8+zfd6HGmtu2YVM8uGdcD0A/VB319TaYgu5SQLV2qxBTUHv1eUqP8GrR2TJNUxfT9PtSbfqdi+AjyB6nrRY1UL+jYnQWlMrsTBQA+628uTzXSC37w+RroEL9PbOsLccerDl+Bj24sOkTC7+MgBaepJjDg/jfzE2hrSJMW2VQ7SMEYPmoGUMHq+ZTrtm6VsS8OFWYb6ecu+jsCehteD/K6JPq5GdqHPnamgyvIl6lLRwGj9KcA6hroD5TfCYgaqggWmoHU32v/xrKh/EvqDBi/j25vxqG/cZEJvb0hBbiJUviHqoOq9Mn8rPP9D4B6W+7M9hzZxC9b+ihAlQ9eLvvUrLnf/qZVgCpBvrAVl0oxKd9UFqB6mdlNnrfTkAN3eqC577B/1DtAZUb0vVwuv1vOa3kyUYDKiLKed50foOrTr3O+m9QcDFA1aDb7/28h5wIzdPLTnwyf1Nw8ZMrYh0SDV+v19EKZ3PRqA1TZJekPZ1vNbOpmUGt3FBS/Tn4jVHbHWfAqnf8Ks0SMT7/pVciX8Yl67J0sujOgX0K6M1b7S+UWPg+CNlW5LRJ99Yd6F/vS43gFqItSP6UO/XPjWPe0I/5b/4hX4cEu2OwFHQzponxMwIYmHdLuL2A+9eKLqVvEoDCANHu6BxS+7tWjy4pt4ldPSLON08jPg3gZti4pGIx+Sw/L0VeQV49Oh9HHR9AdUN0DVA1AY4I06fEK+aH1GFStNvFrBDbuI+/QAapwYAwrQ0n+1vwTvTWTYhf+sS5RbHhbUgMdUQ+ATob+DSWeTUpznHO9bP+CPc+bzlvtW7GK8dhy+6FH/+UKZg4vfniZpj2zi67+2Lhr7lAgpNmDuiYC01U4RffjLE30qIKz+Ltuiwx4YlyfkRuwNvdu5pWepP1aZU7mzcscxvUb9vNx076cM3NOi9aiVZ0IeN6g8U2+AepXQrtDerGhboljojXf+9DBj+vWyfxtYWRea2x1ISok+v0l9Ce/HpWjo0DwBdreXWyvF3D8R4InRrWrA3+rgM1z8UvHazJ5MBALqNRUrEuAFPLo5FC95ipYaJZ7EvnV0EFQd98o5T6UCzPqPq/B8y8a3L+6u8cxWwz6DR1NJN+zDh0GA1JNlrzFb1uzYHVLjGMgtELdOrAYpl+9KHYldAMUHLulVf1F+MJ1M8O7KfVZ81z/EVTXA+PtScvdk7b2g1Q/+vXXLy2c1EBH1QOo3fD9TfKfQW164uYyuRULuBV+PLkzeA61WBo4chBzXj57wMLdvWfmzJikGae90rCbtg3WmM6cO+PNV3KZec8S4O4QZT+fyR01fo2Ra4bl3KytBGYMN9977LBhS4blFdb/EtDXxf5xaFSAV89iPR8t+zdg9/oD4Hw4lPJFfj1B/ghtDpEeF7HvyLRcQXfMmp1LbU2ZbFqSfC4k+lwD2hua//vxBenlD6RnqtwSfmu2ZGS5pHYOthcsFSo3167Lm8rdsXI08+8SCHBMl4ZugU5NGZBZAXvL8dvWE4TxAV6nLILjfgxMj9VrvWbUjA8+KPbSNlO2323N/TSwgcQxph+k+mNZBNBGU7nQZ3TmnLEuA13MGDUzo7cbCXzqn3hZyg8gUu2M4O+nmW4tr86cM+85/sLV7ZEsIOVqcpyv7BCcrhFnA51N8nNz65Tr5Qoz/gpUvenZjcvlyTn8gX4RekwtOP66u9ONS1vd8dclSMV/BUO6S1cqBKnw9qWsxyyJz3F0dfN0CfnC0KqUZcNPehpR1z/22F8TWs/vsA3ye+rch14cYKWLl4O0Lcu7de6vYJ4+t4XqEhzXwX8XpIZA5dj9HkrzRqP0ZIWuBmP7QmjbULedsso4N2NgY8hXaucBKt5oq8mQdh6qsfvXjFwD0w9SCzOogDYAehiqSxDFH2Wtc3vT4EebsOhbF4cDoH9CpbtJxrsKdDlOaCPpxBF+tU6zOCp9fBlTE9tQ8bWqpwne+lXrm62dglP2Jf0bT+FvtumlJaO/LxZtWvFu315g1rQUaLGTYhpj/Z3fB8fff5rgs+qZK5isR9qccXTjHJ8Gaf3rwXRyPbn/e0/cJ230ezgNeoyyHtMUUpG/t1+vU/6DOtktMwtOeiFsYpkgXcbZRXOX09cllDcm/yzdLiKttdXMd6QDCQXzEup1NTXtADAsjUFzzs2DZsoWNrUe9ag07HYUG4x3KL6+Sn5Qe/hMv3pf4PT26LsR+uTcuwC6Makv/kVLi4vbIo2lk9+2RUemPjg5NN7HAEjjHUD+r6Aect1N6vGKj0tQnFo5l8+kHqQStQ3h5aW7+atTmGVMzdmiIV6OeoWdCLSekVi4sJ4y7S7K7DEzPPm7eR/vwscOLm/uM/TDzFf5L8f1HXYuM9GlN9Xx5e2yhhUyOA/0BZPSY/EKm9eiruUF9UjbFY2y60NhTfJVtZzTtD0B6mtw9BADL03WVmkai7F1S4y8JjH4aTeC/pxnX2PouKKxbciXqclw8sZ6ucrwVCK5gTbSLP2226i/huuGc2QOtCV0v+8cx64H5fP9egr55BRsdFgT4KmlFNdAWha3WhsPZD/6LMzkcow1cfZxG/ffbt0xVk0E+H//EvmhFzi6o3lIIu3alT7AxH9x9K3aTVVugX5197isWlJ+H9of2hH61LcGHjtSPtuv1yMnsEp9tg6/12cGtebgmmD0E9D5l2HcfXK53MXce/+MfoiJ2yBpH9NMy00E3ofRZ2EdYs5jRs/Ll5YbtCyZeSwlTxhbm6fUH5dzHujY7FIcid6M/0eRl/rgsLshpFmJ1BN29TvcPnXDBoP8/jm9MtcaRGmy/BuHBzA6h/GdQn4peZv8lor9VHRxSHPwFdj6vHg8KmjS6VQ1u/4cpGtT6UkSx/AH0AYpjfbFlOx0GDNgyXaN83eM8Z0Gzy0p825EOrPUvt2Y/GDJ8WU2WT/yFaGLYtp0FrE+LjK3ksEoqFmHA5T6xTLCic9wUAHGruRteidHf3p7UZupNwd9o/5f6iPINctbSOBxPFS/2WUvoxsDTtLMm0v2WWoRZihLj5vne9D2/+PLbBbLse0Jx8Urf5mJ9aGlR71t4R1X7i/x6QpQei3Y3+JZvhyWzwzyed2/9lbzyzXm93MOvCcb5G9Ar9doL0nzAUmUKtTZiHO3cJdOvnWFbatR/1k1jaLa4PNw6AnoLqjwO4nSTZE/huM9NUV7JlNHFplrk2sJxhlQmwSoAWf00tkKgXrDFTkOM3DqlYZzrA0d4hj1pruHoLWgo0Ndp7UjSCn4BXPtANIuE0ehsdW7qhnLQxnrh8GOwFsTII+TbxTk16NMH5qg2qRoW9eZt4vlNo2H6jG2OJuM9UGo9HQgTt+XK0hd1a+0Qf6q+sBR5XqcoTuJuif60V3peuQv+Z1xsmi/PK3BVZDTD9oc+Qu+nFyzHHsE6ukV83wMtZhmvPPN8vRzgl9vj5wr5e2sM72Ju4ddmSn9fr2WDCQem+fd2mORbmvzuP9VJrlaBXLfzcuPzWcyekxaSOy39X2/XEvOsW+h/U1FG1+QbwOPruqXOO4rQGm/3blf/Tw2Wt6fMSxmlFTIxI6OtWY1Nq6waa3qV9ZqwNae8WwDbSgd8kGQ/ua0eaJfPTI+os07rrzDWytv0rlacKw2gMYzqov5OzQ9MLo94S8UqFdbLEzWYPsbDOjv6UDK2mbu62oNNno7cOsN6YuChZv4oL/wNAmj5VEjgvw6lDelD8VdSoXjSl03q1sVOJ37v6rWxQssH7C6wsPJr7uY0mNk6tOoVxxVV+ok/egx7qbkpbsnToofwdPaU+0tOA16jLoew5VeYoAnXPSyiX/XQzWl5M3fyJ8ZwZXn5ufdw5WrXmsTEzlMJLYbPhwMtQoIEzVOUYlj9FLWa9p04YV6/GzOrHmXZXL5vzCDqwtrKeUzCz6PWWDmc+uWhLUXbi+a0B+qr6AptZuMtZDa43LOVWG1V2yPKSrQ5+Ip9qmgvdXxTtFVoynOOd0w32wUpsf8fXqmarb0UzBuqxnqap3dudqGnbDdFozpDX9cHDs99dzer9eQ66acP/mF4GxPfgd6wqmbtbremKvPRk1goevw/8hLT+rq4OvIgM0V6WtH6npylMYxDZhurCLn17+hF6rxSoGY1mm2RZpHJ9oC5yfQWtBA6rqDqEsCkG+h/aBfQHP8Tuh3X8qXk7fqm7ruXLf19ZRT1x2rvpDRP8ivtUzXhZlU/hqwaXNmHas93sS0yjuBkM+fvp/NeIcMnTJ2vWw21zJ79uyx4LJDcGgcrKlMwf+ZU+bpIJ8Z4DQD/Gc5Vz4sHveL6Kd0YxPsM+Xy3ina09OBZVO0l9SUbvrSSBPBv63+HumJziyc/jF5RWukKhkox+OH0PqVtKmnLr5oFqk+T4hScBz/vgeleeOZglftauJBetcuEFMDXuwfKFdbDAajuiYrMHuIXEFwl02MX38/hUOq1/wAoGv6ZfrQk1wtaxrl8zpDzrmqG55Soq4Z42NLjAoLClI13V/3xIFYGdJnIfUix2uQ1mXtVI+OAUV3nuuT3xi0T396FHotucYdmYIgo7s0irFtIo2ZBa0CZJMKCtpQ8SS2q/rMJE+LN/9FqbSsVWqH9WdZ7/fLL9O0xpCpY69t7jv8t/Ny2uoouATFe4bZ1Z2GTm3mzegel3NbE77gpzbzxnHXH+47NAqO+0FkbbFH5z70pUdNaaRfpmGkChujGEPNax3BX8ss/l5F/9U2OYA+n6+2cVw7MNFN7l/j9NpB3l7nSZKhDkyiVItO8XceaQL5eOj8SIW2FaxNd7qJ0pM+P2m/3cX9SpV5KZDA1kbYaCbXE6QulzjWH0PCuJDAQTdymlGtxw1/n/m9dOr/XwuN7lfg+3aIl7iqYO3LxNoVKOIUSwpbz3pV0LxqVfq8i8YKUCcGjXDCHUddj++7BfkR5Z/QXjMs/iMR/YiPitCtmE0AqhmcQiJIfK4p4+1C/l6AN9tryu5HkPoAUdOKPj/VnB+ll8mehs0zU7Wb0BjYPpVdqMc6w6c0n77SjjvOau43/DJCw3OIDgvHRxh52cwxw6aOHTV06sv6NCdP+mef7csTdlONmr8uVW1vpd/DoLqt0+J81MVG51dNCTsjoe/XZKTKxvTL4cpsV2XzcLOHw4wkdY6RHif9DJqaRL+oo5uSeqZfgE3dg64kAwCX0t8cfNLLdZsnadcOOmmv0S4MgfHPhE6BHiier7ah6W+Q1mm2e8LXVXFCN+aL+c7AW5hyq6d+vqyCvNW5j826LQEB86+h26D7K/CvLVXH0dna+Ff62wEemknVU1TdaKaZdDw7dQKzdfwBgumd0NV+vZpcQarWitYjnY5zo6B762HcZJO+fg+fgG/BQnMAy0J/g7QZsi6mSdKGKN0AXR1QPpH2+uNQc+KvQ+mCkcl7H+S7LfQSs4er+IZ5hP1ujv1IW3L5c31emjmP1yd4Wb35nVuNFUgXAcqlYHZLmn1E2tJG0gTHQ9ddfbOhbzxf+KMw/po7LiX4PMxvgz9jM90y6w6bMu4v+FX4Yzquz0iWYxi+95vPfOu3Syl/gj79P1YDsHkV9EBKtsvM0NcHMB8sE1TOOLLyJqm20NqqNFJpzRbYfBplENm7UGE2iPxS9HSzdQS/0b5RbQz8Xxt4abK2TNNYLbYMf7uug1evR5q1uLpILY0tbTUZ8yikv+3WBC56uW2QVakNhfiilwkPCHW5e6heabXwd7XSRlXq78FvdE/abk8+LWiDOru4FG4u/xDkt3F5NP31gfbGlxl+32Cux/DaS5VLUu0JO9qbdcXaLSW3UMT3X+SaeFsXWgVaCRoOaby/h3QDoY+ZzEluubUmbadCf4VKk56UdW3+aWvNymvdMPQFwOlxth5vppKw+QyG/Bm6932j8A+nfDc0HNKjfv1Qnoc+g/TIU3cZvSEFbHoLbnny2IRdTdVrxuvaoHLxhLiBXCdh4oS+bOlOVbMzxIuFQFffON5evMSGIhS9TH5mq78Q+Tkbt1LVC0x5Zn1bMaur4PPz+K87cS0AzjG9fX4mm3+aD5teSzA8m2Bw9PC3msewYf4L1fUQ0crzJrFUYQBjCPzAvcnZpuwBQ996+dnMtLGFhhPXXH+Z2bNmBwPUS4cu0+0o76WXSo9fpJjzWs4tnC2h7rxs4e3UELf6KngBV/46LJxKviH1xynXbYaSPnrTxxn0cXK1XmOjG213rbZ9Su30CFK/kcjAMmE/+0kPO1dDh2JzXaqnQD+Emqj3gK+99niprrD7xq3wdX4rQA2ca3Dik/7O1DMNqafxWmyDlS5Wb5BfSf6TWmyl3LYua5IZp64rT6bsa3ua254xLcSxm12lE6XH/VW2r6TZSfiqGfJNoPBM4qLwFoPWhNol4Zv+fj5g6hyZArm3kFX99zlgd7tAua2KB+O/Yq5w+ghGc5DJWJvQ7QNPT3/WgHTd03sOWfK4dAUKj0I/CiiejT31U1Mq/FHHidWgaTVZCjTGsQOpdoeOhdbEtgbfQrkneaI/QrTpgf7R0FlqT25M2JuDYHfy/wYVaDOa+r/Iq14rh82bsDEVG7+Tbeq649AdYU2JgPDvzBz+XEawpyBd64HqcKHwJrKM4BH62pdgcWk6+5q+7qR+EAf+yuWX6Xbk7J49c9Pf/upkIvGT5E8aiZna/yyz0iL7f/7RzKsZ116yCe/q7osteeTaE8a0mvlE7o3rP+KP5D1Z9nAza1MfD/swtv+wXTIt+f+E+QW7Xuavw6aOS20phmziS39If5jaLHEe/Ai6qpoO8VVrQtv9Aoz/Cnh+XM0Y1IYxfB96WmXsHA1doLISfAWiz5CzCsZbAfqE8hnQKQWFKv7DxqOQ/k6knvCrIY5J3MAY/zx0FiVvdVMY165ecnDTewu31ct+0C5j1gsdOt+UKwkL4fAdpImTCdA7kGaHFoc2g3S+bIGPC5O3a8L/odD4apzAf+2kUXMAUU3fjdwGPF/Ev4vA5lrfT3hcPjOjycf4vEpz7CmeGU+uALDNEj6vAU2qtkP8VaCqib6+Nhv08RXyhdEr/C6o63ejbc1m2dolkekOQumD+Vlq/2tWQ28kBgMSPZ5LFKDKC3QVfOoR/YuUryJfNcI7zaTc48vQE0inQ7+lnPX5Veay9X/0fwy2FqKsu1cF2t9Uaa/QLJvtdkk+N5c1hBntKKCLr6gOSbsoFDaEnm97/hYb/oz5oZ98Pm/zzOdfrsAvUHeyC5LnPcZs72fGx+toEeBOYUZ2MpGDZrdaJ7b8GnrwLkd6p5+eG7/Ger/LzZ3LiobMtcOmNJeOUasGe+0FHN0uy81pWZ3Z0iHNfQhIvcKsB3vaeZr96E2AWq9HgK1cCVSWCJTbqvh7zoWrObf0B7HStFGlDeqkr5lP3T1PrtL+YWpH+0/JbvZtYFPnrC4c/u/Z/23X+rj+ar+POuQn1sFm6ibBVE/T9Ajy3dSNV2dQkxttlX7O2G+ooLOX0D0fzBTUb0JZtC31ERXYSFN1yRqMteVMag1utnnT/3FstRRG67Z/qt7JYRViEAVdikuqST/DTpsGqEUnFbdUnRivJgYGkwuLIymvZjIGP3wuav13zQGq+vJnUrvTSbXgm3wu8HBSs4VbYVvrRCdA60QqWwS0V7C4P6Rvxw8Lq2L3HOgEZD9Adhn5gLBONXVsjoF+gL37oG1kg/oh0DXV2Au2eWONkcvOmNPyOj+BZYL8Rigz+zqbR+vzyoJX3zkthGfzAYJYLdFonTQzk8+8iXw8Z9f7bMP7tZfPT+dn3o03nxYGu55gqYtib3RWY8lBX/4KVH9hynoX8vJVrcFKqzHg352QlqO0aQKbQdDESjvF1yugH1Xarh76+H8ydFY1thnDO9CqtP8zpEBUf2B1F797NfZsbbD/CPItyau5KbCZ1kVNN5/PWpUaSAgGR0CXNYJL4KZlVneTRz49S8PP4nHXhINmTWtK+Krrzdnke9ZkqMLG+L419ECFzQrq+LospJtBXdO+INOOOxsXhHX+j/70m9Ps9FeQJiB0HWE7xoqX69As3YRvV0E/whdN3Gjms5/fA3ytra1quR92dE7v4Ntqqxx/tQfubWn1xxh0riuO0zFbCdoJ3ijyUqK/56hsRJ7KjVC3ouWWUg8pFXDwDOgSBqD1p0rhSHs+N8H/2FFEfoUIewpSzyPf0m9K+XhoY6gVWL68htwPnu7HRiFIJdcf0ZqD1IFvvvRZc5+hl/NrPaEG/+rSlOBTwUH0HRgzspFX9vnrewYhHzR/DWlufoZRJY5RIS+UC6UF9ZKgggL7q+oHkXZ6GYNtHqTS5wZQxUEqbQZBjZL0O6k4SOW8WBJatTgI/Zb3payb0o3SHhh/T7Ts5VDy2k6+aMcKy4OixQ0n+Ss4r4BX5xX/1rabg/SvCQGt076RfHS9HME2XVQ9K9bKLexoadBe2FSQeg25Aq+2SL1q6CR47svOKkltMd7pjHGJCvQfRfdJSEsrmqFPsdEq5sCeZvT7I1sbUhC0KaTZzLbCku4KSV/0mklJk3bhGxhNBFQVpNLuPag9knY00slerd+tfMaOznWRn87FvOIubS2qHRJ0XLXLSioBqjopzKSqkMvlgietWDUlnNRay9cgRdpLF51ehFwHv6aEPd1lC/x9ajIU0xhfb4d2px8em+c1Ft11ToVKd1cxJqzi5jWGrsxP4VrOoAHMNq7CAchaG9iEvODDzOStrP59ldNEjxb2Kql73jiO9B3ZfKYbZ84a6C3NSBYmZNQfpz7EjYv7upwQOg8USlbvi2+s2lwztfnMVB77f8tMawv+MqvrTW3yMo/mMj1e9LLzls605EYzM/v9oeuusYt3662t/uBV263fDuy+R3kqeZtiwHmlXRZ+5vuRJMdH/Ra+Jtedf7sn/Nfve0ly/aFPnPBfL0o+lrhBDYr4dih0dQ0mIpsyhtURvk7epudOpEMVCMDkbdR3IH+1gmZ1UQW/M6BT6mK8aJRxrgylutQNnzfBvDaD1xrEuiZ81wu+V1TTCf6VZlKraV9JG3wcAY2tpI108VETJQqA9CRF12HdSLVbYgzz6FznzCeVOoHvCsAnkvsTX5WaqFoffzXJ9wPyuj3dYVy6Dq0LfUA/71btrKFhKUilE73FXqobdGtm4fzqUDAKr9omvq4M1fXuBF+fgjaWk/T1LqQ3Yj+Dlqva8YiGkwZsu9DM7Cd9c/P0RnxOQSuzSd529Fy8k9Q+f/nHiR63DpooPJr3Mj9ju6arfD4ztJMJMvvJ/2zWO2vI5LH3+TJTPrnfyF4zspllszr+C/d/Z073SYs2TW9alctsd9aJ5ppaMr1bcpl1eJN+afYH6EGkzg+NPM/LcV7hs6XFHx7e8Y+AUoGuboG6E2Dyh8bjjwtrWfKZ5REUzjH81g/9TRasTqL8Ftf0yd2yuSneIotMXnvCM3r81K6JY/0wNLotneB4jYUqWt+Gjz+AHmlLP+P6YgxbQBX5xBh0J/6TONu1yvHrRWxoH+X552itBkPtGcPfoZ+H2B2mCi5PQgq02i2Bn2bj9Xi0W1pOMKbCzBg2l/Jtwtscesyvp5XTh97JuDQte1F28P006IwouY2Pf6tC79h00pLh4y+gi2uxh6+66dscOgzajXr0kz4U6pUYx6+giyq1j7/a/eSeStulpY/Pn2FLj+AnpWWzreyUglIAnAul9kfBNAAA2gR60iSrlFf0lZd76vvde/y9BdqbfjRzuz/lKZDuiuqexg/eeKnct9/sT9C3yCI9vBsG7rvzR83X3PEFYWDhMQ/B3Ss4cRhvtz/vO5PffPNur0z7anuenTQPmjr2bZ/fCLl8e/2jGb3WXKxleniLqUbwL+gDx/rX0F+CvHqXOa90IdVLGS1J+8LHyyH94W6YhP//hA5P6hD+nwmdnFS/Fj382hJ6uBYbUW0Zg36XH5AvGqXT6HywaYb8JVpt7i7YKRjhptX+NnFSxxiLbkYug06CtMxDa5wLT5mQFbYyg5dqKo5hCvlqqRoOGcP/W6EFT8xCclsV34ZBiWc36edj7F1Jm4qXstD2euhAmz+VyPBhWfQPhg6nPLCStrXqMo6XIM0YJk74qPWbmkVdOXGjOijit47hoeTWSas6dF2TyUKQCnibQ4/WZMnQGDA0IzYD26tITH0b6H8G1apY2D0HOq6qxhU0wufeqP+Yvs6k3GZBqslFtq+6hhnJ5bJe9pIhB+90j96iN+k5Xm0IcKwHQG1+18n5lXjLEPzTI/4PyfVHsGESY5iNM98j12y5NeF76o/B6Fezpd/D9vLBzuG/AK0f5KVZpr9DoKvStGmyxRi0Vo6uav40Zpl5bH8MrVgmaCMGYxoOvZxGd4zjI+xoe0KthSwkbG8KFWZP4V8DHVIUpZrRh9bq/TZVoyFj+P45LO1NnA+JYqv4pje1E88I0sfmGNUN2JuxxkMKtC09kQyJaq7iz2YY0Y366jUbS2iA8XwfSvzoHN9Ohs5MaL7uavg+lU7ugG6m/FzdO6yxA921Kv15fpbe/wz+P1jTReLBgNW0A6pz6efbgP16FdfB8OSi8cXr1UkSu8OnNh88fFrzdoVPhboANQlkVelwXr0FTamqcW2NCjd0CU3oZY2GClDlNz7pUdwBCcdwKPrF5SIJWwTUOEZaIqGbR82KHQnthPj70GjKcwOqKp4Tqqdd3TdtgxH2xsPfM0JWE5tjsYKoJiO1NU5zt5OTcEVB73HQtpDOs+As8aq1uWpt/alVmoKQ8QirwVWaSvyko2hf+pdU2VfdmvEb1w3HrXXrwGz4V2Z2JPdJ/Kz4RiLSWo0Czpu+0NGQvmQ2GfojpJu3hnwC5Aepa9U4blPzFTkwLQiCQd0Mk2K1POzrTlLBcL2T1gn6wXC7rIWp9wCdfSMCDxm59WUGfy9xPf04TqEd5dsn7FszIVWl4h/+fchPhf4C/Q26G2P6u7Y/f3RLwS98zQDV7W8FfS2H/dFQ3RN9KQi/p44drVdH23Gm341TSCoHJ+0G438S+17a6abz9ED7lQPltIszkxjkvPwuiZ5FR7vdVJTARDtpJApu8e8qGUdfv6ctKupogfJrC4p1KfWpi1WMMv4boKHQ/wJ96CtM/QN1a5G2Y1A426rUTkLG0Q/S9p0K9rVrg969eQ3SkpvN28mtVt36Qeo3rbgpVBigvh6zJaaCtvX4Je30dtoGDfZWh6dHq0rt/lLPfDfYOmGtkSuN7zdsu7H9hp0+YcDwQT7f5akh8HBalvhD9b7JFvx50OyATLMjsYnf1kBoVKxi+ylsgn9LJOi+XwKdKJXjwa7V40f63BDlsdC+yM6H/JvLyyin/SQn6JfePu4mBv1oHXs9g0gFDf7f7qAPaZU3TstQpXbA7Q2oqicYtNPWN4WgytQvmK0CLRmQlV6iCvDSKgb7ibSJP/51JVInRrB0jNwk/hqcYpfiFHXWNxmokNdcoX6l6okDxkoNo6/45VXoAr8tx0y/8539esJcN9KpXU8S9lmRGuNqgvQb0W5GwvRBci2pVCBbz783Vj8Lf1TRmAql+oiHA6I7xAlQIaijnqf8HpR20qxCvdOXdNC32ElVf0CjHNS36+fMza3kNXkzmrpnZsyd6S2Ryc/ZHLBW5z3491l/ujbIDaD8Hp8WfZBdR3UHPBQaNnvmvOV8u3Ny+dPG9hn2ucf3zbnvnYX+dGRTM172ASaWnvfyuRVpyxcz+PZ53pud9/IzWdf6Ots6tQry52ZymJifvDwv55fKC3Z+YOeBAp9r/kItGY+1f5k+Oql5kas/5Z680Y+rmkXPExR4zDznF6UJ24/lF+IlMM1EL4QNbdFCvfi2P0rU5+8MgAGVyQo5NfFxTFnmi2w+e+CQaWMfQ6fe6RGdt6QSDpV2SPvPaLM/pB/5fcH2yN6hvjc0h/Jz9KPf40bQ1VBc+lGcQnvKGUt3xvRDfPh3jB/Lx8iNYmxre7jzfCH9KSD+A/QLyllkd1DW414tO9DsdKtglnraaS8ZpF8FyEdDL6veQdPIdvb7bvr/VRU+6KZt1wra1Rog2rqqJni02YuSJZqxDTbmHOUnkr8e3jFBvqG8BHpV/T5DtsaH6mlXT8Dgo2kblT3G/1Pw0o1v+AnqspX0h40ctg4k1wcCKmpbST+V6Mon9L/CH+O5Cl/Xo+NE6M6j/hJl/Y29h7quzW2S5ISSgkkdiDTTXQxEL3UQNBXS59SDM0Y194Xt30Fb1GzIYgCfv0Z8GXR+US3xgukos+MGbLBKPjfnl2z6deCsWbN7F/QI6VrmhFoUT4NCxn/EajHr/PLLlM6cfGZFrK1BnLg1u+AWoj5ZV9iHHf2XaYFPt5EpX/iMdaT4/9s7EzgpiuuPd8/sLreAN54cnoACKt4KGu/gFaPRGBWPeMXbqFFjRBNv/94ajRowxttoPKLiCSLeXAKKCCyICqKCgJy7M/3//nqne3tm5+jp6Z4dYevz+U1V1/Heq+ruqlevqmtsGqkfO5OtUhKyZXAuGq7s2IYoO9WNdagTK0UQEGrI4nipLLbUSmuXNMrzYQf3XqdHTECAvo6cAXz9Teir0LnbW5Y4DS76xxidqbmJJ20w1yoz0xOXFiRd79TxaZGVebEPYuVUUqlHlRBQ9Icpqz1UWuU4DuhLXymqjhtEQP1Pp1TED05C2D48JMOAFF11/GOIWy9sPmWkp0lwc7p+QZjT5gX6xyZUmTzbh8jXN0kpPUL9bznc1wGZDKNcXiWVtmkdkHZmsdrMiDCv6StHIOvj4Ogw6Tq0oJut/8+q2DllsvnIqf7oZNKey5Zezjhk0SirieBbhC9ALtvoQVgnAHwGtiXOrSNh9dM7gReUh+vXCEsXku6o1Y85+JE420IEQw2MQ8PkgNALoKdOQ/8O1ZPrT0GvsHhAU5vhI/0QAnmXIi9Knm3u15eNWAOL+7LPW9/JvY6sqVsy9XziruAJiXIW72Vb4WFzOg8hFkVrAG0Sk7C08YdcT8RsKhOm/mWiHitqAttqPRHv9T3hsP+U61QD7rmsM6/gF7NX1G1z6vIjF7Kq3Q50vt8JSiR+Ft6bgOoZ+xK/Mb7tSLsD5LQkkXcvoLKROfhL2RoFNLHUcs9mxTKDxgSQrYO3SUFTe+PUT+R10FAbvQz+C7QfbD/iFhHWX+zafRjhvI78vk9NyEsoSyIy/AtIUV5lHO3Fqkv6VopyVI52lHL3Nb7dF0TNkzrqFIrZYfNBflnMtgmLLjLWQQuS6X8SQPy64Lti+aToLMf39f74pY8sUvhXQNcd34hbG0Q2SZRs8OuApz5U+sZBiovSUZ+XQSA+yFeR/QX1scci/DdS909jlfQfnfGa15hAGfXjjNt2H/041xr3QnFOR/BBKNQ8RKhUZ/A/0DMVXXAw8hTPGYTemkBKQ9QKqjoFHV/yDv5x8JOCOoNwoLaa2G2n9eqWTv2Ep+B64L7AOSsaIIHl9jtrqmK94rHYvmYsdl8mCSl8aEecrRc7Bxwfi8eO4fpj5WN9fjwH//8hhcug9VWW8q9A9waBcs9npkNlTjweOwhavmb38FgZr4kPMmKWlvfmiR4yLjBjbQ/m7NdT2LjwCDGTONHgD1yf03fm+AsIPzXh4WcHTO7VvyyWCu75aMSSVc6XI/93QOcC3qQCPDed8NbkWu2lZ8lxUv6kZJ0EXAU1lbivkymHf1iO+FCikXU+hA7CH4ivumtmHcT1pm75lHt3pg6vn0CahYHrBHgUxvpw4Zf4ekY0aVC7ainS1wBL2eUUCV0RScmxA/6xCq9izt6+0Ax10n44Z1wqB/sNw2aC/F2h2TtMutCsBtoi5Tqe6+ngOzeiyAD07PcHGvr6fC+QttpTJDkn+8EEMreLJJzEqHxkXww+hf4p+KGu2OaQuVeOeD/R2ku/2E/GcuZJPQ/P4R+FfEvAPeBQZNgc/z6wLJc8lJG+tz/4O3n08ZUmC6Fsa3A6A5lrMxebc8kTNP7boAWdclRa8krxlXYfmaMtZEX6Hb6UYfH8Q4rZ34nTbKNol4wl1mG5e8uiC/otwJl/fWaMP7dnx9gXCVnakhkHvNOZxTmnNt6+/WnsQ32PhdaJVoJ2tCwNsoYZj11qtF/3IYIdKXsRsm6UyZq/b33GtOJDKdOZXU37ZaZj7ZyTSFgPsDDvt+Nfmayr/4idq7fQqLbSCY9LLGvFFpwH+6FRb71F++/i5TNuq75draT537qlK0fozwG8aRGGxzm0uf8rgJY8XnWeBfxaIKVOZxbKsiGr2v1OGfybqYfqd7niSH8aTx35bwjXKi7DbUV+WQaaOOLjRP66SUJIEcgzAVI74A+Hl559/YmFBp6iXUrW/nkKepVzTYreVl54a4+uJlmyfh6LPxdat+I/g98Ov1incxqlqIbqkEXtc0/KD5V2BRA7i3q1aQY5GrY/lY+x3e+EzE7H+/iaQJXI970Synf1lNUHZyO41uQ0sIPGS+AVCEzF99LKN1ENzC9bQfjOIb7kLXnZaHvjuL98i5G9j/bmyxZOyfjXbGnNHUed2oEngLZT2Uom8s4EpyNbF3w9K7KY5nSUaw/+SIbp+Br7+uXM7CNBnawGBSllUWv2X/iQp1CWY6jwzoUylZJOW0gJPQ3/yRQdHb2hpS9ZgjVwVpzDcvls3x02P/mT7v23mPBD4kOUyMuwNUmZsR0Wy1H8Y1VfzKgdk4sXT6tPJj8y6qxxKJODUzmWUce+1uJvZyST1rU0QOeG+PRfFNNL+WMy/XPGqeRpsl+JuO3g63uQQRHmYW48ogw5V9D6BxnJ+repg63cmHHrKUcKa8iQGJ98PUwaVjRjy09mLTrQSYvS594vAk7Hp3/kOQR+gz08HyVOyz/f0zbayyOrqfbqOE5tMpr4ruS5mbBmqj/hX05cNyeT4xOnAa6vc53h/5J0322cUTbvJTJhuTZ2wa+FhyYA/8Qv1aKWNsnIEGAn5xo+WxG+Ft4P4OsvfdUpatnwRvxa/POA3V9xXazTwBmFOxmZ8inhJfOkHS5K4a1sxEjTObGXZksrJY56rUf535dCI2BZr3ITkETTYrTRUnAj2BN4x7p1m+YOHkO77Unps4JTKKqkLIdBnXcc1bK/ts9dEZRYqtyB0DiU9mUosLfIOeSyjidOYpg+/FtBb9cwaYoWdZoF7Em0h/bmnnCxQW3pmltsoXLlpx1/B68p+K5BBHl1KsS9YCewDRgKVuaSibJa7boQjAVSegNNVrydfttczEKKH4+Qe4DjgLvMJ9pcx8F24M/gNqCBuHUqrRVhpckKVQ4l8XwaXgOl5JIMVymMu5l4b+fWEOvzt8pMaCJQskORezGNiGm+3q6qyzETP5q2Owrkeyie6cpNzLy9XXyDfVestM5LWsmn6D3S2r6BltXGSiavQ/HL22FTthuIzEIAf772tw5zeFDX4X2nT3hWMk7osUNv/hL2WfLs7tTfMpKnOeEy+HeneOzIczGM8Dh8py36EdZy3CbEv4F/sK5T+fUc6d9npHjpzL2LiNcHQ1IAr3byZPE3yxKnqItzxAeKRp6ZnoLayiLFcGv84fgneNKCBnfLU3CQk4Ycej9OwJdixHzIegi/Fh+rfiDrqU0aelpq1JaBUB0yaaJwQ6hEM4gh+9dE3Yqvvkd/TnAMyBwUxmQUC3wJbR2Hdr6HwBmecLmC08NmRJ2WQnNH/EvwR4M6D481POHAQZ4Hvf9nQkAfSXrH1cA0fRT8zkeeXFm8Suo1yFzyswwN9YfbphhO8zBWv1guJ8XI7XvDYMpzswI6mozekkGvW8a178sUzbt8F2iGjLTjWuApcC9wt5oQ7o44WwK9S99INOozWX4uRxkpvR/gd8mVJ1e8d7l0GZna5MoYQvz90NDXlDoeBs9S5eYBzXz0YYatlBKWOzeVZwlhmZ8dZcBOjOoHnuzFNG/30LctXcTNIe42T3zxwWQMy1npeiofE+2B+mzzp1HeNzuse9hPP835NU2K0mO4DxKPDfv5rPPaVMUfX7Lym5cosXfxQkdfAkV0ZbrcKZ7M2uLx+B/G9eh7GOrLKVai7iBS0p8Dy5oYvYQuh7EKpZ7FNOWNOC31a8DQg5110CP9fdJPIV0DmJZTDsfP56TcDfVmoMyhIJ/S583uN3wSsu0J3SFAs/srKaj9RWENtN7B0JUJ+rsCnmXXfUlIyr4GtCvxgyzru8Q8geHUye5IPXElBZFNqxRD8aO2ED2E7FKybUdYXzCvw8UdqSg9j6c44RD8ufCQkeB4oHuhbSc7E6dntywOXlPhKQvOViEyfB1aqsv/4ctQ4jXIlNQpQ2tjaJ4K9G6vj+866qJBXOOJVqpecBPCCywqgVQ/pyyyHeWEQ/AXpmho7HZcYGXOIeDXp82/oz7vgl39lvGRT0qvrIAbZuQttV4PIe/VyBpWX5shXjiXyKeVZYxg1j+guBa4gLBrFSVtFnGaTKvNjyQt11a1nuQZQfoe+NL9fDlv4/zHV4mAmVQpYPPD12C+IVBH2BN4FVSbA3HKozLpiklA/t5iNNC3QOcsojg2OMLXgOuca/huQ9ixWl1NmmbjgV2iXZvAVlgUudkuY8vqaIc5by3WocNB1qJvz+L79397FT0abAkfNx0WN2PvL6urH1upCmqqTm+5dSNAXVfwoyX+R+oTifeNhIX1FMu6ncRX8g1pqSLxEalAOTxN4nI6ZOwIcimoMyl4GM/QcnwpAYfnJNSY8DvyuUocYXWI9zQmhxbSc+1aOuCj2bO3XyiJEbT0oaMGcteJBxd3uxEEiOsK2I9sW51dBZU2K2UgFgt1rKE55FPbDMPfLzSiuQkNy5J0F21SmyW+5CjqtBG4BkJeK/5xJRMunsATxRfJXYI6HQKeBvqww6ugqpC7LSo3hcYUym8AtNJ3BXiJlFr8PwNXQeX+zAB633cH6stKGjsauTcJ0S0Gdu47H5hC/oLeMd1tm/xFQkvV/vXQHPc2BmaAtD4LBiUZ9Xg2voLGcfhfhyZsRISoey9wO/gLcBVUseN6U/AAwUeBFFB79VNpmY58WxD3An7me5iZ1b32WlIfJ/b3bsqqHZibqp7dUDTq91xrz2Af/PlAWv4jXNeQNp2wbkBJbputuiwa/1Hxeir8n+QpkEynuwLwv/I1bav3r/tp8eX0Uhe68akA1tZZppXYK2EfbN7kEOLM7M16jXK9vysAX8CbNTXnGXUrBjI3uY16u0qflNcaI/aretPaLWE1/G95ddzQvSmXm8+94JEobtJEmQ8QUPu0vqWsZqBn+xGYfFpheBv/FXw9ONrvldY5+KHjI49WCHbyka+ULNqCYk+0qEN3wuqkevohSD73GfCT35uHNtcgkL49xpshWFiD73bBivovhexjwReZJYijSaxhxF+VmRbGNbQvy6Cj7VlXwFd9ULnc2+ViBJ+TqJ8sZDJYyKqq907Qfdb40MmDLuRdm+ucjnaaQ6L6ZCk1h+IfmQJeRbl1IpJmeIpubw/9qHh5WKQFdQ/K4VwjV1BmPC+P8py8hv8efo+gdCqk3NrUQ9vd8o7LpGvrzWP4h+MXXMnwKqlHV0hFIxeDxpEyKtiOa3U8mhXbjoabQ5yzd0IW1nonLahvPvVUYny3PigqRfyzl5S2eKsLrfrlUxy+KGtfVcdiB9YtqbsOBXWwE5/uyzpt+FIC0ss1zxWW3yRtfIkRb/V4csUKWb4OdLY0OBKheF/Rs3bcLP4C1u38kkl7EHGyROoj30Keic9g4rtdKfM4+U/EX05ZWU9vKlZIyh1QbJli8kN/52LyB8z7e/iMp+ye4E7CnQPSKbbYn2h75mrhOegtRf6bofjP8KhmpZTPmvgv5LgSOUKzeGeVgEh4dIDXBQT/nCtPBPHOknEEpNNJUr+tiBFCcdDTuPFsKMTyEOGe3EfyC3my5ExCRn0DUu1kgJa2I5RkFXRo4Q+B1rX4O3riQn0HPXRzBTvmSggaTxstoF6Z/daSoPS85aCtLQpXECdLpOuIlxL8MWkD3cjKDlyKrBqvfldITPIdQv3uIN9ZhfJ6O7mDCmVeXdJpQFtBpRG/p85pD04pbYCiJauaL4cyOrx9vMtRKKi3o4w2LH8iTzxm/qYumbi38ct8X+QCZUIGjgKyLQyByvsqxHIuZ7MezNFT85OJFZMYFlFQ013MMG/jfFRbwauyjJVOatJI7ueEy+Q/6JcPz46sT/rQRQqqFEFZ5r3vm19SP9t81N2eJVNvfUj2JWBbSpOOPpL6wVtfcj8SCXEj/weGpfJEbi3jPpmLDukzSQukoOSiWSD+XO7b+gXyhJnsvuNhEl0VaOnZACcC/V3n8oB1SlNIubdp1wFp2sWgdTT4BHi3UcwrhWaAsmsFKJO3CPXJVFCVv8lKR14i+ROf4H5qa4jXadzf2htRyWHaqA8YDLzGz5wik+8PoODqvT1oklF7PzM3BeckvholPM+DsyKs+tLYH/iiZZoj11q/7eFL6+eczmj1K7sMyhznm55bn9R+OOMXaXS0b4+Dd9PiSrxAOX0HEh+yFfTzEkkVKj4Ti+hFnC7wIGab9Bkw9TKN+NF9Zo4/f3L37TeZsNl2OyesmKvo0Q7lnlgN43nIuzdVlSXP7eBvCvNe7Y33Kn5oA4HoVrqj/u8j4y1RyQl9HSmUVZkhXqcoXBIFb+6jlLVIaHvkfRf5Z3quswXvzhYZRRx1bg/dSLYX5JC3Lkd8qNG08TLwErgFDAb7gv2Au6oWKsNwiF2MfMNKJFXuvqhcy+9Os6znBKLyuQeyDjM+huOgpwm9zs3W6o/dr/HeaW9P5HUJpwaBqdxSqI7OgF8TmMWqXXBkqNWLxTRw53Usfb9f07bToB/mLT0gaRpaVmxwllGNMnc3Ws/mTpTj8yTHwRnOtV8fXgtQRptYZGKx2JlxQ18dGvqib3u/9ALl07mihjUQWd5EuzvKpcEXg1Xx+HaxuDGNbRJP1yXrZybrkw+aCNaYx/rKDZchQOehfXlP+mA1XHlou8PwXsLP+rWjDzply0LdPgOLw2JInXcGfwyLnpcOcmpCJstGR8Jp9yNVBy1Ph+6oj569B/GzWVXC5OfHAvw6dZ0eJtMCtE6m3lsXyBNWsm2BD4tYHjr6yOOX4ELwEPk+AvsDLVlWnENG/ZNd45gQXMK2wYsGKjkrUKnghbYNXtR3Se0ZD2W53+EIPVnJb+B6O/yXnfhV2adP0QT4/Hx1tJVUGmQ+WJgv42qaFqY532hV1eYjRrmcHTBp46xO5oH1yxbuz78q6YOpqsZ25yxTw/6LzcaoVEjbAQDFi3YdOc1qkLcUfwzwX65/TPCvE974qMIIPd+MGScZsdgjpmH9U3xQnEcRnlJfnxiVSCTYk2McofoR9yXJHuuZ2RyzzH/4aIvzePmuI5++KG7lI3+zZuHd/xj0RIhLcglCuvYN/5ArPap4eI4CBwDtJSt5lAAAPs9JREFUjbwixWcffC0TXQ8OTcU53t3km+dchOxfyf08KEyayKplvkUOTcJ6vp90rnP55OOVCPfkgly8FE+9tXyrZ7ocrlzvjN2/UrcqcDEV08dOFwLHeFOOuvriwf3WhNzXR5c+CJZ7m1RU72OTqnLv9Jzu3SShxAja/23gtfC/USLJnMXhMxnoWEMd/8bfmKfxzVnuZ5xwKvctp6HU+zKuFpq73xvJg7GUvBP95veTb6vPR8tS5VGyvKXMz9rUVO1nLDT2TVrW414FFcVxMjraJSh0Gph8OPMbNLoPsmbkgYfOFNInQiwm5c/JR+CfnPT5i2Qy+SjxUVuLYGs+ZprVu6K2D9JyP8qoZlVUXWcZyqLhfrwmFbXOqjYvTcaS7lfGylc7cGBrR/4y+e/zbGhDe07HC7cf+FOqw8yZr4IS7MkB8rxI3bSMleaI+46IA4GsTl+nJUZ4Aa+bID8QjAB/pT3/ii+FaQtwF9BeyUyFJpKPVuBzGbhS/MNy1O9xaB0PvvXQfJp4v5OBLz3lyhHUUU5NVnIiYFyud3ok9RmJ/FPwbwBrRlCXkknyPNAdGyfh/1gysQYCg0Oi45dMyR8e+2VEvtO5j6FuXaTdtfdX2+7e9cjxpiccSRC+HwCtLm6EfzbQ1pSwnoFIZA5ClPslXWP3XGW9Suq9uTKtpvF/44HIq4wU2y7fbL99W3qbJh0wSuhMs8bad3ldYjBq6GNeBTVmmpdzky7HivgnyroKZX7eOssv+7FCEHiWNYUepG8jGiiri4mzFT/ocxyL0cGmrY/GTPNaKbP5eTWkkm92gzLtIzdL+fF47KBYVeyOpFE/HL7qAPI48zNW+ffp98X48Wuv3eZz5E02ZLbaLP5yYc6HOw/BwEk8E+I9LjCBCitIfdQBPyaxCM/Ge01hOa71H9w3EOyN/yrQxGcL/Mj2Q0LbHtDwZTnSctotQBau8/DzOspoNejjvJkCJMJ7CLgmQNG0Isj3CvAq0beT4SpoexW/O9IK5bigzBok/S1HcmjRyDsSSJnW5ED9z1mhEc9NqEkfmTtr8BTq0wrsCXoEp1KWkn/nHrjvZQgcvc9bCOQKkqCLj95xH2UZd1ZbQmMIXT2P6o92E1HuRR2eV2FVdGQOfvOAzkeWkaAz2BTI0qq/TNakdk5kzEMgjHxqu0Jul1wZvEoq1roWpxagUYeB68Juje/mmxs3oalBOW6dxHf0d2JBxXLk+SqSfVK8IB0SpvEv1FO9gCU76B3FaFPtErJMjh01F7jXqQD1v4IPtUZzuWVmmveaUWsKLcYRJsbGbEetmFNzJQwTreNMbNu2bdvOjm1jx7aTiW1nYts2ZmLbxnm/qnNxfsDpi77s1auruvqpPj1SsMU9BjTAP6xqR7j7NuYaTZmRvnE22W2yA8Tgo2/aLhO+7HcGIp62Wz3+bkX/e+15SV/AXMphlam+o2ahqoP/9oqAvirxE0cbYKWqW/QzW4EXKD3Fm0N+np0F3I0LxdwiL14d8ebl1nYIQtrtT6B5KE/tvNtqEtAzHSmvLiIAWSuflqcskz43dv0Q2lkDGHcq+ZJdiY4kZyW8iIJu3Bmk/kIURAlrRuDbBl0bWbLGDjJFuLjTdSuKMCpYHPpD7+TIuSLoyH7HZflPgEmKBHL6Y5f4VUy25CXFiB5jIWUakrgYChQrb6suPo2RJNmBhjjY7sYObBKWwrj94fYKgDGE4gohG4AyonOC8sIHbtAHHxnJdtmnkk3g9YS8DOJ54Bo4j/mgaa4xc732LRuAO0MwdjigFaXfQX4obZxY2adnjo4pj7G4MYiWySkfKxsNuTYBRREw09NRy0wGVrJUPXVoFjopcTTvWZvXymTdw3+h60GXNnbOS0KkLt3h5z99bG85cjoMSqndBE+fGaq2Ntme33DjzSZJ9Nbb6GpdrT/8YXGNJcEJl5v6Lb7BcHmv2VSeoZg5WJcM26fb//WnVtd+cliIGe+6JT3Z/THC1BnIHLLeNsvh/djywPeagEx1ZMZHxUBLRFj6KKZaJeY67sLQzk4fPK+7JCZHh3kTYp/LvegeGkMZq9vzitsMfhWU+qggbRt/cmHsBddqUsx7K48/3PI+UfHXprJksfXxJS7eY9VMf5XNXgBuKU3O2s3MwoomRq+PXh/e1Czz9dViiK1oIs6pHvaO3n1gC+MS8RnEL+rPjvFg2qkLbeV2hLsThayhYmb7Qv3AjdWnODpQpX+TszTGfdJGXbmU/D6j7GXPtENYJC5zE26MpAPhgyM7/QULvOel0L1YYSfSwHr0QKrQaJOJ49GzGUNdUFtURCCrFvJiinUzlfUnaHansWzsASsjjm36kfxDFlPyIVnQgO+pvs7hgwf2PObWBUP9MKjUOcJ6VEkirwUfrAyna11TOfdXglJQR1yGBc7zLKeLXJg+RVprirkc9LKnXTKdMMTcNmtBj5Sqv5h7EcKAmAgA5dgH1UvNOOGDyFnIu45DuBWqyUnwMKp+eBfLGGbkix8stKzAYhLSXgHZE8QaFmctF8S36eqB0r330tBIbh4ftgqqC3FXfr79PflBSZzVFud1U9PGaxsTnADmSL4CDO2zA6Nk1pHsoe51R4KY75+DTq4mwuSeR3QwzMvuEHxnJxfGVaI34isSN5E06GVoAHM7mnRmLbXUT1Han7qN0HvayjqZdFn2bBq+7MSjyOJZst3ivnp9FAwhuvRH3bql9Xnbdft4sPT+aJ5Ew86W3uMTsjJfbJpgpvqmXJCwG6Z1EcYhyXdf+iyq2pXSJwVNUpbpav0iasUj4uo+I8UvaFgbtBims+Kqac2txvavfPYBNaxxoVIZftmA7rJOz77MA8dPs+dJjox6/XvMJ97+CeiZDeqpj8faXKc6c3JHh9Qx+ZoOw4ucdNzrJEHhPwRU4FY8NX8Vpz5U3Lvf0QIBqoXQgzXo7By3P4jSA738Eh/xqTc+la1f+aS/lEbxAutrmbhC6NGwTOjGwjbevntdqjSck+FNhJPcuWjbWCW0J5d5hVNKVgbPBCB91p4HEkRF7OM9PllsyvgTmlDz0FCIrn6eO1m2Y/LFflueURUgmc6BbuI2ULTDjDup0Dj02QV7pakjSOP+FuxfSo4XFNVmwHf3i4pGjH4PN47+fWKv/JWSjFO9SUYYcyU3JWRebtwoLDPdhbRcKIKUdxgucKI/RXWd/+mfBasU9Dyra8OFOgpXhoI3OPCK9MXsSQNo4txjSfhXybQ+pn8GASL4G4wmJustbmWB3yx7E2md7JwRrXzdpcKH3sN6rC73I6nPuPJ3Qp1ybMIJoOh5y+Ncik1woyjgxczlzIo3+Uot7LHgqzpEp0Uh/8zUXo7QYJoitY3ql7bLBSawa/HfdwHSLKXbP/9yGIF3AoFCW8CVijxN53FTKwubuvlsGpNS4ElJ/OHag9ldLgRDricPvJNF7Rsi5E2Bv8At04PCMEBywRcTPsyfy8t642eHJ/o/zHsZzmhxVPg8uX2ZTepTLRoLp44spg6TbvJS/UtF8JVG32vmRuGFWIKcg0MAXWzYvFcslLkg1PIUn6g1kngsWP2zjInnh1wseGx5ozrnoI7m4/ihT7jiCvavFl9y+YQvkTOez9r49fb+3Mw4cVo2hPe0eD1G5jJKiNG27F7qmxdPbkbdXuoAH4EbdXybWCiqQX64uru4l9p91PrVYwzbPEzon88HI136SsDBbaTWiIcCZSxBQ852eCbd1V7q4Nj9i77PuaDitzwAlmbpmvAQZ/53GXhvLWFJY4GVoMKnT+hz8xIep/6OA3kK19Bcz0sOsVMKDle+wjLVDoVLzcomf9FO6h3bmOfVB7zsxR51KtaFo0zf+mW/ptx5X1JNOd+nOqaqat3877cf46lTzAScE2QHc+3GOGQxbvRSL4FyeHcx0IaXb14VhF7zpwgPLgN26ppjaTNAhIfRTp6nUFz6cK3va8V5nH15gi9PrrR3nZeQIhRS6mCc5bX/5MVGSOpK+i3sIFBt1GygCSpNdEsPM6tTyAeq2f3QUZbsmjdRA4VSCUPneO9CTvXDm6I7aFKjSmx2LjD1XBn/aUX8TqBFhBoihlzUoIT84q0dzKqp4rkP1lux39EjAiiHYiL3WT+IGc5LCMVeqGuPf8WdNUkYdhOPpKKR12rwi7U2JKmhWDALaJuMl1nMGCH6ifPZzNKsHj5euzKAsutr9aPzGOmLdFnltI5ImC2iJjmo1197u8CH2RFS/zx2sQU+unHsutxGeH2xN/ciu0dL+2u8UXyV3XG/Cr+MpNmMfumlwWUVbcQx+QEDOB1pnaknm+9LhK9TUlKYM6K8yNwveandKanwTsjzuLcMn7oFBVdlbhfvsg4ld2m2iGmy9xdWaBFxTEkh6W67l40qZ4/KUd3bS6tT+MP2k/KlDlkprwNrvJiJbef5OCydklxrn+g6jokDPj+k8I7lN+JVq75tVDnf91/95okXktN8up7F/cqYXpeVEnp2qS8kCKYwSWYQknP2bJO24L3dtXFXDRDWQnSR9I/dgCyx2AssmB/QHzkqRngiQssgvqGCJ9cCkQ+B0Jysmk06GZUp+a10NZlcxZ4SoMR+xVQQ8jeSf3YBfKKSbBI8OFUk1No5UUBIfTvuYNve1gGC8xw4zUghnxc8ZIgENqHr7TcsITF+hwZBb6cUq2LroCOnlMHKtZIPpBO0dujSHYIumD4DPkIjOLba9eQ9Vr5b/3EcwmR+I0RpvUGH+WvHvW1DP25909Nuu//BmFTvXKUy311vLOQX7NFD2cP02Wt5LobUhvvd4+wV4iw0L/0/f6qJ4qASMmcKPisZnFwEAb2zw3cPJ0PBwUMCSHKtEb5zVlxxUjp7ZugawF3cfCzY4nMWCEHCjw62yfy2Z5BQqEj1ovW8hVyiM97npsuE7P+ME4j9I9VJKyUcW7JA2tdKW/QYb0u8dz5txiKoExrBg1mFAo/UErLnW7wGv5YtQgNUISXUFrCgCNUy3i66OnN4hTqVTNJ2O0JhpetmqE6qjfWFsPWwa1pSaADieo/tY13ChDYsXmYnRoQaY8dwEnGmPjfMd4EqSyY42iexuryktAOWzPgoNut39I1U3i2o+KQSOV9B8DLDRk1ptKlF1h8evOKR1QM+kxtlCrXtlZ8j3j5B9LNA9jjvxJ88J4LudSXLxvt6NsMZzVubsKIJ+Tix5o5w0IzPsXRzq7lk3mjIYfo9iSg1xKk6nsYy+6eKvNoUIWyigGloI49/0Sl0g3/HvTnmXWocpQigQiKoPWmVI8qP/K7u/hCspfwyGuC0ifTcmQtfXgwo7EOnFbh11VUZgdwmCiSQO8FmgDwvtPSm+kwYPBb/egFvMmptB2MQ/kiAllBYb0oq6dFDA8QAFtDAz2kvtq52vZ347/MSiMZFbOkhPYcAhJHCSWGJLTmy59nLF1gS0b+5+O6nOr152oOdTv3MBI+Xj/gLPgirigmHBvw7IgcfbBevQV7QDBPpADR3tzu/71ter73UYXuIzSu32Avu5tyvESQbaoWp2YwzfCChrSL7QDKXsmcULhc818wWsWMZJ1B5ZcE/un7NWfz9KqPFyXYcqToe023Tm7e9j8wvQnYxactdlcOeejHnAUy9XpR4SW/oRz1rJ4zGmFit/GlbfnafGTOcYmDjyf8qEHK670dqPM+62VqhCjhlMZ0epDarMcvqZtoKVMLH8c6jRL2W1YemYlyimjPZtwjePnTdEToiU4heBblltPbDwGoNGWMo2LXD+fbmm2k4gpVN6OSH88doBta5LAMIStGfZjvum4e5WkFehFPCcD/fR9f3xZpfiAYURjLolaLUBW6CshX3tANr8JGsDPRoPa2+8HCz+illSzUq+O8ly0pIf05uAViEz75DLd/0h5GirG8KEdeCh2mlfnJ95oPTUgZu54N4xosljFIgAL20XQbb8+pdGOacC2+SqfvmQ5gUi8bmCrRRel+lXoNlAnIUebm4uSTuNPTvz4q7mFaTTPRJpASUw0xIj6CEbwtnCmORVbKZZFLP22UW0S3J2YqwAdp/YPz1m2NLdE/wnkVNWWGCkn8LTOgQ436Uj1lvBY/IVOJ3F4isvDnrQ37+EdtvUDgGWciWMIILb25vq0oUbtsXFvo2KFQ205by25d8xSdpn+TyDhYTJJYKNhTPMIH8YP0WDoWs3MIfl0sCYuCsUXyCfA8YN4ZeX8zzYvRx0mdtnBI/kM6DCvpQekAC122+KPS2TfYLqGJuFY2oH/3Lm4YRIKlo8JdF5PzORYRPngiRUxUrjyoi4OHxyj5VJzaOKud0ISgP6rFCiZusVG8ZsVMGIfbTIQtLWccbCbbHXuscsFM1E13GZ6saFnB3/m0iPyCfp8DuxPxUUAkuG8lG0iD3gFs5ZQgv5jwtjjO9md+RMXOJaDZI12j4v7Yd6pjrGlXYDP4pmVhB2BVtwLo98to+CXRUyzRHyQGrrUBXj4eiqykoM8M5qgYznw39RMNPDxUCLaStoNnMweiWpan4U7nGqNglcSqXSb0is0DTdcDzjjsuneIOJtVdY7m1LyfyxWB8RQSvIKIRVkhFt2f52kSZISYz/8o0cgGNHH4Bk3QRqBmC3zYPg36g9kPbAN0e7sJ2lw89U/af6A/XC+AKoyxFgzsxmJaEulronRslctZXnGF07Y830Txnal7/mq9M73WqEGyq+RYMsInHT/mStmKy31BkjQc220LLZ53UmT3UMHYJMnRhMdzCHVUWaAmiwzJ5xBiexJuzshm3VeWohXn3plUxa5lNJpnarN+Gy3n4Z/nNYiX75PXS5Dzf53ul8IegRuqtArrsTWL8pfNQjBJAoI/iBm4pkddFDcT7nU56r25kwgOrCvgyv2zkSQ7wWGjOpEfrg3jXWxnPY3qdhjaG3z2YXBkf50MynANBuFZysUeabV7a07+Ice7qicwfaUFf5IjyxREvoxfpwgA6BJ+8vwZAJPF4aYgBbbThC9ZwdTNZ6eoKimvecc6IfcGeonxR2qD8KTp8B9HD38/qyxIQM8s2Gz1hy6M55xZZX5dRPxGt4BNLNeY5RUV4NxyMPO9p/ubMsqy3s7bnYg76gXK6qmmGgHApyTb11nzacVQ4pNAq3vT6jeXHDjcCHdyLR2jA8fsDtP425HGZrKhtOv697qqLwaKKDUE6dgQxjFQysHPTb2TdiAH6vSpWIykJMgWvhPt/0uWdI71OrSxouj89ako+vegR/rlBdR1HRoivDjvo0QE1k2GYvPv7JR1U3c+Q6rkzUhSJnoTjr9a/6h9DvCS3K9z3VjwbcekDWFl9Fk+/ENxNaXgV9QRYVc5uT/hqH2aHIV4V916R3wqdt1ioi/af2DiZYz8mufKeEkLVAtkTX7SEU6YpEb9/wMZogvjfwQBoC66RMWZpYsBuxDy3ltG81xuvg6FfatA6W5sRItXICIdEEnliQZTMwFPbwU3BFvEXrXXES/h3tB3QPlIClz7YZyQ57vNNBN2DuxjV4SSR8JaPIDcOmsd3rzaCb9q9CkDKEvFlUJLsaMWeC3CmhtT1V8EXWlbPeZAd6xCpQfQMeQGWK+I8T9gmW3G8KFtgo/5N8UUDvpuY3+tgcjU2b4ovgh2gzWSgrPF9fXPCy0WBDceUuYn3oXApizoh68vW6Vz+JXCuxbiCzagAyb9V3/jtlYo2ZXZ+96eNwcIWVi+nb7JPm2TMZItKwbKJdGcf27tG267RmOJ2hoLl3EgnKu9wCwF3i3PcdjpqbGapLZdTMT0h2mm8TtFC0H9EG58NL9wSSOEcAPa+Y7GT7nLuhu5FmmJIoa+WoFNgq92krISQOnQXg8s+uE+EQbApPVVbW5lB75bE83paj7nSl3GZqKE5DUiTp0YCNcuVcOEC0oQo4H+uWM7236yTN8qjjGNOLNgsV9rJ2lioqVD53ZD6HVgeJX4I38pii/635KenkUJPPBI8Zuu/uE9chWDjimNIb9nG5b8eQcs01+zLR5OHSDFrgzf7R9vhj+f65XyV2kjr4JQtCzLPkYSz/f9RkN1/Ea3l73tSKLCtE4hUIG5+BJHtPuQrw3yQWyDga3NYPyczpuv0qZAWBliydt5DOc4OqGQv+TpMOyviRaNP/uXvXMKh9tXGkffzmzOysZf0+pMmh1lTyg+TKx5WOyGYJIvCTSB71B6ufNCCRhTn2FKKqBq69oeHV+g1TsRgXSs5Z+7JjHqIQj1EfX3ib2KTIqvxPyllWdtSbhoOVbAt9WhhncP23ycfz/R3YUXfch1EXkKXJ9XKx3ZnqfnynzjCfnE1qQqU5RuTb5G+A1KEAJ1Ea0/W3uAbXfLErqJW3sOqXy7nK94Ej8LfhJlvkXQZyRzI24XUlfNJhaSqFBjveCBPbX9E0KS6bPZ5wkJzs8jTacC+u4JCEo9KK7RF9eQGzC+6dp9c1ZXNmX05hn2ZQz2odUUU7sor/oW/hwRpKIoorLBc4TinLhcgaF76LeX90pHQ2lWQFlHRMTiHfEeLSnAVM3uKm5x0RnqUGrz5qAkORbYm8Db7TGEB5IxGNkGmi/NKuZo2tyBQTevPQwrQX5Wtw+pFW4bAjoWZ24t6oU3Cp2ad5PlvFtyn0ZPI3tih77HJT/2dbUrWwiKX1lswbPI6ddR5sc7aAcJkAUhOUMbxtCesZbm6BdR1rLAIJPCUpeTmrJApGqz8QeH4UUvw3fVk/n1LbDyAp681LXcUEGHD1jagq0ehZSXTviXOaChL4LflvP9IdwvI2EVW1QAO1bTu+IC5lM6se9C/1g5sL1ZXzdM1bwIpE43bmQaJkxOgrtQU9K4U8iKajsj8kO++/pSm8VzrmnlTuNeLmecWeXOJ/2jDGp/Si4xOv5QTy0xae6VI+NKuQ+M2BodlUQ2ZGrcIueMLYzAyFOImqLXCuXHCyYq9xDRtBrNgWaH6Ij76hwBoGM0LbZCpwp0uUODZIyErklKXivAR3iG8SitEJSzKEFZLx8ElMOloP0qc2TopU1b/4V+716FzeV1HVnWQOFrLMFHaP9BufmRLwKRQsFmWbk/hVNga+TAUJP4WGjb5POmyhi9jvCgiu0kLyylqENaOONJU5qmidMgb9662LGpgcrVSztj0muZPz5xWCR3jubvvMHRYbe48TZl5vxoU8ozD3am9Qdt+tIC3xfFzY7ufm8DtaEZqSZ1YC+GJOpEO3BEt6GtbGv1CC0pvzdJ+c0F/wfsK2okVvOc4y0hsI6jQhlxRK59KejCou9HAN8YhbFVuz0HsNRg4JrnTR0bNFTXjnEjZMzlXU93JiWi9ITc7FTtGA2LdZiTVLXtkzdMrhIH7jmsri5QytaPbIbmJBveCL5BXCfhzfSbQFtaugIwXXwUk0Fw6I/kOBoUsN7KF/RjIGtB0Qf/6j0NCAbE/mE858Mue8kojXF0R/EIgAhl1S3JecGnpRbqIa2bMtxOP9BBryTZm8ZzzOs59CdEipJp9/LibmgD0fz1ypXEyx0DEkwB0XZ+kJm5XnWl2R9GknqyfRF5YyCeJ1MshHMwDHVY089owVsYjoerijJyxrF6vbbFtTkXKmg5AkxjoyIoXhM77fzPdyPaxzZ8TqjGf+FG0upAbLqZ84W0azS7nGeTPjrTRV1PcvnXD+NAHqt1MN3Ysfg2LiRFkOoYldutWrRg9kr+csDpx/XJviHjWb5hP+BR8qLliA0iYq3UFGxphER8te4S6dqcoxo8DKcsKuRGKjpze6D94YU/KXX7thYzttJfb5cKuiaj5nUjsqdrLxnkAsfSVJi2F0UIMgt6fQxJFqkEy0Bw0I4BygZSrH2TjWvHM+gmQB7kXqF9blReSFIvUHdysNUhJja+bIT8w7dMksgYBYDE5Fxa32dgS7F5GV7TZcCVWi1aLjz8aUDSOR+HWFlidF3zug2vyS2XeMMUVcdWsevVKn/SOQJY9YLQRixvzwCFOxYxU9f6KXpGwogzuxJaK4gemBJ+chZVbGJWv3BVTK55rA/6gwHpc8/mo9FY0b5tCtccaTTyvp/PidOnS3wa0NflvW/5yofQYLvtD2mdvL8MYedOHkg8Fn3ElSS8BsV6Dx17SHQVi2hG/aCLv6PEh1/V9UaugHtECEtSoxFN00hQ1mSMc/7xBvv0Aw0jp5bNuKbx+MlTzRUshVVWMvUa637BJkK9MwR+Bgu/esPgGqHfIsK4JLSdujkCFGnMMwQGxZ0klbifkzj6IMUhKZBcRszPwR4FZTpySJ8UcSb1U26LTpohyeG+FTT3ZczyHulPwJm5I9x9E3XlHHoffhMpXC1HDjSV5ZVauaoKKuHTf0ast1izwGxc8sNN0ZfIN3cdcuh4JmZK15isX80YMC6HHyzTD+xp+LUSYHI9qXl84/EiL018m43falo5Pb1YnI86Wre4sP1K0YZRemKLLEqx9ORSVOkWJ5YijL2HDn3fqX03JZHFvMd4UUX1QvBSH/rdjNZlDUZ+k+KyMaUGwKtA6cQngM9YyTmH3aMKa9rWv+NLPMjQBUL3f5PtSDYNIuYn3VnhqiiP1vwhLY9F4NuYxeWsAk8kH94xqKVrpfSKvaUYqPZGtCLaihnUygUuz8M6YROBSvoOhUzWRIC+a0SoBn0hqI4wSir6QzO4/+bTB+qs1lT47mRyN96QriK4i2DfOjS14itkX4O/sVBQaug9c1o1p3neILv5YLVb8yrzPVSUUi/qsmpmrdNvss/uvPbtxU0Sc5WPTryTAcwBRFiQHtNhm1lBtLZqOqVC+xAhLVIk7hoACdTQ0plm6cuzXAuy2LOXFjQnOz+BvHD4Clfhg6Cdb/c5UL+qXnys1vmtrKXVC3XIxlZO1pah6EMBqhqzzjSvSykbqZUGxQPBmV+pUrmhetcwlpOBuXk4ynan0nTUdaum8+GrtgA8NLtNeH0LJrtS+FZVfYmnpiLkR5G4FpyvKvPHrTnB9YKo8TZ4qr7vDrvQtv9evU6GDFvoOglN4XlCCn2U8q/r8T0Ij8ILHVWJE0mwgH4IEnwf5p95JJ4SQG0XauxKELsfjNdPfhsTY1IU/HpKFQJXypVbVinqGNCS3ZrADS4wydU2hmPvmiFQWe5lziGaMAdvGb7SX/FwnCj6CzZ1aM1pz4sCcJp8+yEVN1ql+XXrDqdEI+/DWyo9huyiUYY6F5K97YcWZb9IbK7V7+CPK3BMtzr43H/f4BCmRpWmGajFn39+PFDyoUvc85yjpSe1Rpfc9TccwJKg4mkXMeYPbai1PS1aReE2vz8Qxl13psoTmD1LoS2o8XRg+djsNsYaNWVloGwsT2yXD0uyzNVN1je1vCezbLL47pmLqi+e56avdWEwPOZ2wiHv9c76/Vpww3pCtosQF6Zy1tjO2JtigAroSX4/I/rvesCngBJLjjLYr9A3paFjDJ70BIIxgTF5TrrUyQ5SvVyJ6xJCqP547KDWQq9B44DSZqvylbtcgUMFiA0a3XAUBUrjQQHsmknyoTbo7sQDCQprSTT4evh29k8A9ZvQQ+VW90utqNhKVd83MzCYDtb4AcPtcGAPv2VEmpb8wy8YR/Gf+ybMRkuSwv66KozK2ft5QyvC9E2luvt03D5QPf0M0W6p/uOGUyWvSLeXZSkC716Br5hX764JuYclXIp9tT1NG0Fe66K42EIDZchK9h0DNMIlUps/jFbKwUdiYGZ5TrEKbX0RMDOOzlSFW7611WJkQCMQbKrMds0vlSj1gfD65pz9BebfKyz26k0DudIdyMXG0Z8OkGvvN5JdFFhfMHhB/9UzwcQw0Ut2eB+63w0TTglfdaSF7nSmvKhdX2TjG77xjWLblIeSTTf9m2dRt3DdNvR5Yxktq6ioxL5v5fNMdNogv1a++LJ0kmvJTteRe3Jt+SCm7TBRElcbp2JlAKyTyGHsQYdSpPMEQHvu9TgCsAxoPL3moortMqsIjHa6wedqwmLscSdRZG3kqyxHqoN+AZOql+iv5VPoZMaLcjw5pwdpUYDpYKYZaAJzqhnI4rxCgeNdA/f4dKBOwaoTcDOggGg9mgwL8+N17clzAb8UyZpKoliIyai7+5HiZrTz4SE16FsU8DTW4nexX9rbTapmwjXpSD2dz/kPGP57wbKTSV030rUADD08XJw8e9Invt+1fM6tFR6n4CQYeydRD1hGez1xQVz5fJss/SH9yeA1n05Tc1pYuivSazzvxZaqeHdHV40m/P7/dQ8iwIVRXiMCOKGtmStWY7fdpsy2lEgp9Vg4DlyIJ5HZ4f4W5sqGvKE1XVX1TR+U/RG7vzgfODdv8qRLagkm/c16OuW3CEJt0DVW5zNAW2Hg16Bb99c97l0uDJb0K1X2uMMZ/tVPjEgs/zV+TNQkYSxigIBWeIwhg9l1KzqPArmly9FqiJeanT4AHJJWJmLTxew/KViHEKEfHp8sVNzhh0JWl97kqPkaTl6EJuCi+q37FAZLfi/zRCszF3N+hwZ5IWkPSe52Oe2wEoQO+eIYOjKVBcm8KXZm/kYwoXxfFXiCUuKUTHQrt6voAisvO6N7mI/HqgqcXcBd9A9V87at0RHOrjPoZrFCIL4twbLF2vPaDfsdDJQawDBNp1cCqWpZ9dt79b5htx1gYCjdYdiaUPuWKNGkInnvaOIpkJTrE+oiBrN3Z5tDHmK0lJrz6nGxLOvK/0+5Bs+z4f39TNCtTc+VGfyPxY2Spv8GEcWa7rBsB1sQLsNuC4q1hK4gVRXEE8n4cO8PvfaVRalkOoIvpBUdRVHPMElV53wKrO+23gp606VOihumo/9iug2Udg99VVYloC74V7XwnHJkcUta/qWafVxmG49jOUW5G1LZ7d+Mk9HTahYU9LXNvcjXGfvFfHrhj6LGHKAGXaJRGy70FOQK5nLu1xsDi52B7EzA3n3ZvFCJ9SaXXiHlX0TRcmDXK2+Qo1SFAvWxuLMpEb81QyGHNv1x74f9SdQGmM8ZkyBy5CpjSW5Ae8fZbbBo7UuKZSotR/4aNazGJ3JZyAyAfp79PtbjvbZR/GHpbJd0gxILQ3cbOrApVU9Omfa/n4wZ3yrCLlnW+Vi1ejRVS9Z/nlwnNEVYF+p92SRPtb3vjn9PA8IZLHXNhRY0xaMX4WqGzjDbjn9deazyQVkSwtqK9ycS7DnZgweplw0mw3a/xoXJF2StOXA95WNKYqhcS+JsKtlNi0otLJHvlI0CuQxLlfpPuz5WawMWME1HTcbkoOckWDPLAGgPmqvoPx+N7rexy6BgcuwkR2fwpWusDTDGuBiJZMsTuler/oEILL5kKsO7oDcXemNZo9/aaL/cgDrlj96LaQWLHin6s+aRqph8s0Wd6v9GqRKP/YBDpmf4rTl9ZAcwRQcDH4tVC33asOSoHAyTzGCGtvdiGaza25a57bFUUde9GtpuPEcmyefML4/H1L+guktvmhA/wbrHdzGE2Rgrzh9akuFSmUx9mQl0jUfmzPOsJnNIIzptRbYaC1ezoYQy+hhRWFf+waH0j31vtZl7MjDz56tsSnZl9hqRKeoHtdjj6R+CrFkiVk+skXJDsStdJvSfFd6sqB8TVmw+K/VIaJLpkmHrq1L85jaNj1VeM8RsK7f4FWvkB3wTQEk/q1lkp2xVhtVZ1FeQGkS8lYIKuhSnn2d9EQbGaQgYuc8VKk6277NkHPVjnv21HMg/NfyPqg8zM3BEhpHiELXbm+TT5zvPCXWbRkFlnMefH5rvGuqUuP2eGkbV7QGerWqsV8sy3l6nzCk7tiB8UtYcduA0t0mTuDJtqbK0OH2QvCZ9/gvH2oq4KsZ9ZZB+P4OM90QY4vkOIAX2fQ+ItK3L08tkNyxhfOuDXyXSW6pb8Y6aHDd2jDj+o6/FUMCHZPOzcp708S9OwzR1RnOBeYX4vbSw26PFj+pJ1eG1bPe/6I3Ptc63p0W0P87v4qMFmXi2wRyTF9l0Mn9+Yw3i619Et9yV8XGqHGEpI+iG78kirtX5NKvLQWH7dBcQIxfmutwlvjMGWQCusujhvy2CeHQSjztFDM+jE5Xqo3we0fzOyAfovUjQQfdyFibrfWsl5P9uRdLfdXthFD4wXofbnSHrx9nJVvTmoNZJ9FQaJTZEKluRXD1SjVUSkDHsCxG7df726GhXjhz3WQLkn0cDoQBRtwR8C9Tb8lNcMtXDEzBJfEzn9ftkl1uOf+Yag3R9NpDyL5jXnS6Belb488+7aTpPnu4V3x/MnhDRRx99mIqyHW55vK43Trc/xqRCd98C54HC7BQ/6vlWdUlcW0hh7//aGzubfAFL9UzgaxG1wYS115kuq+KnYIbcsI0VtkOW1d8gwgf+ea2Ubla9JMS83RlJFWcZtWpxaNSnfTiBwunDSH4J5xdsMGKgqzhGmJ+QbYa5dathfbR9uDB3mrfmY6jSHYzTJe4CHtYGmqdBWTmCpQk3RpKxto9HTz90gNHJj9Qgc86e/EdKVGMMzhRMZlAaOJ64vHSkE+nLxOoxCFcwLHsU5onLOrH3M4vMBgS9XUHEXq56DoQnljI3JE3I5QPyLlmkYrWilf2VkU3g03s/HukTC+lPuXJ9JoIfd0S47t/DPBZfWb5MpN7aLjrMZVFa4zSmpLRg5/OxVDyVvisEZvBwCAQAf2K8lNET8EoInRERZlMQwXUwlP4UfC+Wi1J6QJqpdW1GSF6m7vXdA6bw9Mcb7jRRhdAKaP5iOnvf+PdREdUg/HENKvjThU64E36Yt5a/7PPSZGDC5EPSi6cDAoyYMPXG9s/wMSfCnfP9/M4eALUcLpd7MvNs5jgpLbb8qzbrP9oU31rtua3xeQUAe9eLuB6ebe85fayMHdP6sFMq5ExXii0miJwKw7/pEYPxgs31hbFq64tFyB782YofVocAuG2if0sQnfa/1Cy/b93m5nuMO5Oaab7FfhApn9VbLv0LaDRvck/KmPkpvmg+FOXt6R//JvEWEDn9QivaW/Y3sdxONmncz+tyEe6zyABJCUzrM9d/O5ESkK7pPx9L7+nAqZAzmh3PybPiPmgW/iNQwEgCOn1bdoU/NAd28/tICltWbP8pHl34KaWQ/QxVzlo1t6r4GrtcKYWvrnBzHMAC71iXc8LR26FJvvdSz5oFmV0uyenMmsWUXw81Jrhd30wb36MZrvz4QUX5IBJ2G8P2tF2akOKHy7Th+HVlZk8GF3d0H/ETU5xC0CGBPrpMykY/aAE+qloF9DOoaAAWxRtxifBGBZ6t8v5l+7sAx3+YeDvjR+1+/vjKZ1YGtxHfcW5iyOpRKhz6I397R1cwviE0bKXybxgjMxHZvBRugWZbpwm32MOaumKXbD6yo4H7jYAR+7B7o0mbzD+4yHdD9R5XTB9b1OV9GjMGpScE+5nWP/Hd/7ks03FDtObGsfCf1jEDtdx9rvY8od+r/mAem02e9V8i8NDHq5VUc7Fbbi/0EKmkBaarVio83ch4r8+2x+/smE/DjB1M1lp7EiPUIf0+5PNbHsneBz0XSPd0T3VoXsafJsut5E7roQAa7/bI+1PbPaNrrPP/NYXp0/86n0s/bmSQITp3KNTrzGhZlPuVRx6X5iUDHAMDEqwsDN7+FIUhO2J/cmcfZpLKgy8yJ52RI0ufByYtor6E/8GWzts+QpmwE4RSQEJ6kSSQVmGZIgS1IzYs7ahUjVRHCnToVK8PootLREgLjhS49WhF6a8AYYDYhynRf9R3fWpOJeP7oNzhOubyo02gZu38/TJc712IWsVrPlvsMEYaFK4kdwYvFQVtG3W+InwwYE3tFqrw15pey9dr/gvqRkSRIrp9QVRzrKjAvnn5htPwOhDEVONoOPIHRdwwN4Px9VIJiCAGGsDs/vyA8d5Aw5En/x1GEqcbTj0tK8bNQEdtws7wKdL5NjPLJFpfeG2hARVeVHlJP4heEp0cCarbIx9YNv5+TbRNRpK1SFg3mIKSpvsefWdmW3vGx75uG4sitdFYmM78QKrnaTVxHEOahLnJ+ETskcpoj/mZSW5nF/6X/j+xXnaBVxrIragXhXz7FWhalIiekT2hwFJgOZJfTIR6aat2VDejTkNH+Ocvx/TT5+E566ICQecxXpg+KCgENXAjRC2SjOkU98Ax6uGetEv8wGk6iR1uaYtmMKXs9WjLJpiND3CeYizZqI6kCZ4bH0RM4MVOYk7Lv9HCHNOnVgME+ka+EvGv0NnikVj+oKpwfroifqaYz2v7UKAqEC36vycU9Z8xV/YNou0nWyd3L3yhbXQYrUGL0S2584hMkEkkHlYimdAOgd14rDlYDerdrW5d+HGYK99y95PFaCOQ3htQznsRSk6TjNbSYXYnvVJF6apCJX60ed+sbD8cPCxqDXuDmrdX7exGUyn5oIr6+sE/vmncY58sw36nJCnL5/4GVZk3NvLbMxr+B5+2m4blnw/LBACpBeglGOG8+G3r+nHi1qbgIp9rZE0ZVrqEdDHzJdjzucKeFaFr2FSYC+L8nmLo3wcm5VmKHCYSiTeTn8DO4+zkToVQ8AYku/O/p17YM3wFBrTQ5Ij1Ye1rDuxnFUEpmMVIOOlVZjxLDZyEJbxuTh9HFDKsUV1odiw/DjrIbonRQ3tFcCqIeCo+IYxnBz2FUUpWmy4DCt44Iw1nit9N9XPwnPij8c/2TB5py095w8Ct3IZR2NCKr8X2gZu+9zuLitsyphotQFs03pDhQtudlVp7aTUEAp/fy6wQ6uY1qr7D/bAoWg0nPR1YRB4TdeBsCUG3aovIL5J/WwF9qK1/rwQOZulIgADTdawk2bMXYcRyhLUOZJgekdLbBMXg0ZlfKKkBDOlB1ac+Vwqvif8jjeNicNvep8CeTPtIX/5FR7K3K/OYYwL/wFsLy4PBsnndAf6Hmz9OONQ9z/+QwvU4XVWZJjjaH8kByc7xDEEyd5tjQKjTdwXhqyrDrVdVbgdCTTt6beaQN2OPAHHGBNX3s9vrbPcVezSS3vPaH3l6fmIT1bNRkNEazKM9yShWahI06JMTbYDryJ5qTG/ryODUJ9UvNz4WweVaxeGQV6jxcdKHhMGGF8z5jKqlSO99SdXLfllbqH3virbHudwJJ6ppzagDC5diTFxXj0NxviPXKJD3pRJnoF2rnYkHVS0VN7fhmn1vCeU4QGvbciZRWFyNL+luxDa6Bkf6uNFPVIBP/lNLbrhSXQVXyjL16kIg3iuTMhKbsYLGgXpO0NDv2+3IFabYp9ocnuPE21hj4G5STZyxZqh357jxhzESa15F2suiJnElbJU3w1CldiMyz/OmUoOKcsw53fRlZWHYN/wCg33KGa7D0ZsSh2offGfPzQQrgxQrpE7z/zAh77cQsdYVNKw0l44a1NqDMrrWXHOC9Yz1eLbjHeQuHPFpyBf+7HoFh50p6O23XYVJGhvdXNxi2Lu0OyYm+E0Ly5D7dxbmpkQR2spjUzRCxEwD7sWkhcEzPyPIWj5lOL0/UWVV17cUsC5SkB14PTEf6Gcygn1spL0fkXoFqQbPV65aDuzaNzbUdxnLQ16KdPFu1WFH1GJpd4Y5I7CbRo7gklKVn69fIJWaf+rqpD8uRh8dD9nNAHFL4cqQXvT9IlyRT65zI0dRY+wZuTHVHeapo7357t7vCpdd0UkSXuSuEskWAyculzCXD19Rk6VSegu6ohvWqytIpZC4Wn/vVclcVJWfeQbWgaJn/MhMMUU1u3Af6SPAVyFrRyjqmstlb0SS7554SZJ4sLoxOENcMSTgeQ2dOo4/5YtXNgbhqfbSGqhqrzdmoBz/fdtBC1Oz5Az1AYvDbyHJyQy44E0ciAZil7lNrmMv6IOHGYFt48KjI7jxO9cbBjH5UMQeNrrxDFvP2J2+wJc+JZiAWA0Jvg1E36Zmkf0vwnl0Pc1NrNdbzNiM4eyCfHP5FwW8oDNY3Z1uPBvslBEHgzj+4KfLlNmUhpbz4fx8Pn3+Isx6RxHKWg9nca6/WX41U257I73/662kKXjgzutJDiYSz9XFchgFIV3Osi4QUDkVoUrnss51oTFS9HUCzbHBBKLLEPeXPh4Hs0LkhJo2BTlDe9/k5X6SoyevdFzCants5MLU/J1Wf2mZSCsw7IH0Smwi3jiCqrOB8m8pf22a7vURBUd6wpBnP65PKhRMr28J+PkYZlSWb6XCRipBZ1gmBLly7tFCQ3P9kcE5I8MXJ3KgThVLNvGwSNCzGcGGv6MUfEyE8JrzSzeV8pyRQMKncoy9DXMMaYp/XGX+Mh6exUiZCeyeFICScSLheoOL9F4X7EH9e2QuUrl5tM+qlv4nO8kSIP+6QulG5ijGvM/V1fuibHUNHuxiFLBqtpLces1n8pdtQyNLH0MK/r92h+Pss8Qa/qetPqxVeWjaP3j7GNWsPwH2UiPhGK+pNd6eS5uZ/hLn8OyYE/BvmmhtvxmpGLZPeOVqD6PSPIBJ12J5hRqTFv3+CSFI3INfReqfvS2zPHBHv/gTxGo9Zg8T/HdCTUhFIMikRJJ+527btB6L2YV8gJiTUchNk3kuoYcqM/AS+xA2StNMQvpMk2hKDzswFKuLrgHh9x6EUoiTEz6skXsd3Y+11rbJfZ7cGdsTb5mr5/uLGR18pTwHMBVLij3ALbFHwnbegkuLnmHxBndId/Bz48PrHRaYwSvQ0AtTMrBWjh8p5pKWDvrC3VV/9OlKB70ZaJ+6O0O+BAOVXLuF0nfqHx7Pt05o9jHnDPI7SUGmr83/JotQUVULrcN2HieQ1Gr+TNcUrAqScEsXYMpCYCFTcEedDPXXrgEJjXudsRFaqlVcaVfnotwHBTNG1SPYdPL/Mj8C+80PkkozVPof6+UYmUxj6ntW+QP+WikfaKK90eKvKcwhPDrYgrNkeYLmlrsDxYp6K8Ke4XNtFNP8T/k0gmsV/K+rINrr/oiD36y2YVxpmTeGV85d17gPPdzGTMmM0//y2cXjw+N7HFmMrak7IufaGsrb8N9vGEqjsgK0+JtdOMFFAiE0rxdwdIo1D+7OpgNzfJAf91sD0Zn5wrGatotQyT5f0gX7NVyKfdpk5J+Y5O8FF3YSkz9PLy1gVFjoPyNWjgUspghr+yyiBDN7ZQ/HAi90lNWfen5ab6bsv9Dr1MYHF/u9YwJtXHVSiJ9R84DEt4vb94cLuZpbbTKiB1GeU0W88FGOQbVWzOkiCPe+qoWgvmsmD2xVP9KMCNlR4dZLJaqRFnZyrKDwoRXUgNmKWfeEfUKdNJb8I8EanwfsjWIMLMhbfwMDWVG4GqdVNigBKofEvtAYqSONoeb6KhIVvoR1wahcroXNVYPnXEPpEaZg8V0jM30hksPGaE8MT0fM32uxiwi2QHVji+6swJ6bdCI+5vfqeRsegt129Ogbg2oDc1/PXa1dnEgnEbvHEONcuVcGdTsomXVQAUUCLS7M9Fm031zqBlV3qd9k3I6oUni29DwR8XFVPack29KUIFKXn0CV+wYaF1uc8fnMbtKoRmet4TX25OU/ssgqSX4GLhTLlQddLV6BAadYK1g3uszCDKTqg+HVwQJZZtWEzIU+bElro0VaPhXq8sczgBLGHMkH5RL0x3vJe4nZKFOxenmKC7GK19HCnV/l72m3yBSqF7aC4W1LRw4Hdn3jiyXU3kZi/sBjUgl15rI3MzY4i9/VroFrEqjX1bg2s+QnLiC2g4+tkvzcOzPedaSERTccQuq+gJp19BwIALoD0gUbUzhScl3qMFJRXjtdhGE5UJdZriAzEE0KHSyPaq5CiaphXMqRCupfRAcL0YdPVqPHXJ9DH71d46jrkNmyCb55T7UDOgfyT0rl1A+jpS8LNH/todYYIGlE1wCXu477mLa3iOraE/pB7BV8U5fFNoh+VlGyDzyrT/N5YVazbCLD6KqWW9WYVezE0ehhPqT0kN1ud1vtZbxSbGL5HLmA85/7YU5pOYbBM5nAyWgnNhJMsjLzKYVNDh+ke/3vL1FihaBn/wWFDjKkiFmAcbbxcU/TPTMckhedDhSEEOiHTu9vishWvJZPP+blIEF+pheCoK0VrlbT2zNY4/5ImRir2p2OfUFjpJYhcsI112p36D0qGXrJQs07X6dDbk3/LwUvLEManWJdDtFdTbB1B2m/+GADYnTWn1BcjyAAlgKekTHGoP4onWa9IzAb0Wg+hBIwuwHt4vvesk/9MJyVRarJC85kQaowvM/JCSRtT2BfsYBjrvpDE/BG0AMnXEmsSYMN7L0OzTtQMff6y3HYt/OnwykPQpEChQnS6ucpU12LyApHY5Il/d3HQFf/orK8f/hONpyMO4pMFjzQGqI40MZ+R/fd4dzomnLImsyuxT/vlCzFcoz/2tfrBaFXjgQ48+fJ87qRVcL6HVEBMxSIg+SerqG+mGNvSTHkRZ2Hy48SN4sm4d3G7xe0sArRCLP1b+aooBkFy0AV/jwhCBsT69TvpvRsBCoLeIXanaC4ceM1OeWVaAjbcrqPNPqOblzA3XrVxAI/IZDA5AUJHz9aB/+VEcVJEaWInrzDMnbwTyxRDEVvqzNZBr2nPuNEkN+Z9HIli3oGUqdEYDh/l9zzvilcM1WZamX8FfjyJoaF54tXTAUXrwAFb3rtZzzotzlEvylHF6Aw1Np5l7yW8gWbZWrrNsZx/uSqqyPS71bD8tRoDqGiji3Yr/0wuulrIaA1DYnBLP07rJiN9GE0p1sNZPm/2eV4EZ0kjbNBs5sk2LyhEJ5eeXqHRDemVHl8YTdbxs+NVLdtNL/YB6kdNHZOILoxQEIxuonPXrfzWVvDnV58OQZPcNVp/b+iac6hIK2So0tBJKPoWWBfSGm5+i5RBOl3ckqr+CxReiKYwXUqTesajv3Abl1s8iIawG1+IYAGRdTTcP5I/13PYzNDcL7KmapRlKJ5Cb9nmf4M9Y2yPMRvSlvVZbwNtWn2w11G5CYIswrLcYTb5yFNuDnb5dv10LpmRMorTUvQJI/5yDZ6JN1AaW3og87qet6LMs6oTc1X67nw/hb/BCfoBhD2b+xv24Uw1Vkl7NwxNNFztJJMR16Jr7exXmQAbVQdNeQJrOhaCT+obW2zKR7qwHNageSMO4jZH+4GUUaCkqgRtedAImh8uDHJif7jPS0AVXF49xquDdDCTE/ojjGC1YTFTnsn8Y2ytgzSaIL202J8dwKHA4mqVHqbnRuuxI2IJkC08C/zl9LDILZFPTiGr/owe6N6iPuVihWrLZa5UQz7O1zTFZ8j/saOBiJg80WUbnsdn2lAUDrH9ryToDkO/aztGJhI0LsgqyFwp8VS0JociecJ+Nncelw4BIchLoFKHB1QgpfcUTlC3wd4Lz3xTFfqJ4nAHmvhh6j0P/jgvL9opnKmOnJ6+HFxzs/D/TWb2QRq6MjM1LCFlFuEBkGNgNKB0qvNG59P33Xv6zrqquNCIlgUrLhL/czWvG3jSDGUioLZOGIA8XzUqJgwg10odgczMak8Npr2ISYfLANeR0tMEzWpWlxP/f1X+P6YaVobbn96T6dockBEhkP9CRkJRvE7UKPj/AFBLBwj3QQKgc7wAAC2/AABQSwMEFAAICAgA647kSgAAAAAAAAAAAAAAAAsAAABsb2dvQDJ4LnBuZ+y8VVib3dYuXChQ3N0pWrS4FQmuxd3dgltxK6UUCVa0OMXdXYsXCC5BiwV3C7bp+tb61vv/p/t0nyR5niuZc44x7nmPcY/5XIlU/SiLgUqK+urVKwx5OSn1V6/gS14+Q5CRXl4/aiDTvbzBuavLSryqHCffe/VK/JW8FEDzE9xh5psfy7+uBnX8GksGLs2pm0N61j+iFfZj+5500GFPUcPhUl+KU8OdM4rjUmMLfSaVQKHXnBYWJYuG25Q4GQu+P3rq6rqyr5iOlVkyOlpZXYkWm+C+WM3gdw6cysqSyY9PcR2mWccuJg3xF13LHDAA3EvQ5mZJw9L9cT04PDp2fagS+aV6dz1dLS1smerpCjiVQozDjH/NEw0bleKGvO3sKribU8WCkkDJXQJCl39ifnk1OJ04St7l/8Z9peUA2wVZAIEc3ogAjt3YQPxkx62lPQZ8By9ULeKa9xyeYwJwXvD/Hn1JV6/sB4Pg3CNxYwmRwQ/M7SK0+4DhKFwNjcZsYqf3wSXgRzCjquKZN2fYuRSUCuylSvPpyP62+UzoZXBMuAjxMNpuxH3DYlZilDeMPUKwLieQ2eRJIxgCMElIsnP38ruFqN0jVHUtuUg/sdUryVKY2NxkWHCT+vZ6oYdxeawFNx4cNjefCf57tDcSwWjTej1RWlNcRzwZfKUVXxRMVnKiEmA2U/mPJpCRtdhlLIEjm16O35LO97EfxsehwoOWVtDO0xoF/aKjftVGbvPNga1/206BDMcuE8HY8Snt0lasko3d9rJGQPMqnB8q7LUMjN8mR8w2BjMCfitZedQn54H6k/AaOOHJQUa5E8qDZbiUfE62FvNG3i+rCpbsfomTbvArorzZnBPmlk9pHjU1u/BleynVaxS+3TPevB+AHRieimvT42Y4wKcfCDqXl7iZihihrwY3Eb5Kzvs+ulTHPpDZsl4axzkz51+UIZGku/e9DPl3ma2k8IafnfiNfjBvm8k6+82zmT3vKwR8mx7cofR0b35NhpRCkEPieiv/canxNVaDRst4jUCf1++4LnS7ZC1hE3zc7cdcQbWjOOWI5XQLqvcpwv92JIS6u98mqjhKg9f/EKM8Ynehwdg/wGQy47VycOepTyO5CD+arYDpulsWBr/Jl1W6oQ4Mp+7qCj3yCJP7QfoftBkYFGgk86m9YiLLDLqygeYGoMGr3f9AihyOXfQGiUNmg+ucM87aGDOEIXx7Q0io6CfGO74b9Bu8yvz8wZSuMEKd5y19UhcFXQ5DWVnnwhTr71PXHbRLr0H0OXtm5Uqel8v560gCQ+QRjjm9yeobmwgmEGVHS3e8Oy5wu4tBV+yhj4Efo/u/pvxrUSmyy8ZkZ6RhxG+H+TgRdLYtbwUfgqHWbbxzIv9HHrdDtjcBpNv5twUlYP3Cp3UTawcDYFdWiaPOdshi1Z/3LH8KycKMjQiKZEj4B019hm5DyR5ucqRU+MFkZJ7L8WP2SWdCYv+aKFgyB426O3wXflR6z2FLMcIsuElLM8N8Pa/YtcWlzf5goZW3sKsJrEczIsqFOOduATwkmu0sO+tRILvpsuobTcm9uan8okIbm7LpUhSkFCroP5bDgKrKy+7hjNmIlRLPz/vnNlMFMyT2a7DrirIs+deLRRpVLITmdVuKDWNCyO5Fyy+OXrdPWOLxoKdvXld2U+l4jdef35oAe0ETUEuy8yGVurd+WJk2Fj/ujKPbWSKDkHceN5HQK2FJO2t/Ln+0dyK7lF+He32DbpvGTpPhcOHds+BNn71YFnKJ4wKwh6UKRWWihWA0N1dYkuIS8YkstXXTsZz/sQbz1dQr0/U2ZTHJIAH6qe2cz5twif25ZIqqY3llisq+YqkIqK6UKykVH8zsB7/qig/nvGdMtIsdmwUHMKSFz5vHyxeQ4aX8nQcf+2UeVGohvEzSkEy2/LYItOV5TjcIomOAIqQgb80Fx3g17yHo9varNCV+B0U58cjxCWIO/lljJhEAyo6ubdhihC3wzUL7Gj94LdxaClTWm0/J6WgYR/o6Dk8YIeHtOruQfLybrxFBSLHs595NGTImQwQx2ackaRsmoonSJvq8irl6Sjyr7CcTaS/1pOXu51/daLWFb1iMmtupFgaPPV49+X72DyIaKFmJGHfVx2eMBblsscZOFuFwkRmocESj+FEB2OV48zZTMUNG/npsO7WF0AgBjt1lKRVyy/eBKE9wKOEDAzkeItH11a3/FZMP/FOkNJlaaRcrjIQe62KTvZJdLhO0ADBbf7tP1BpMD5/WaOGg2FxLRCbIjvqRo3IqhssPTlgD3krH4wXb9lLdiCW88NB0b3uEzglEdvvCC0R2D27caFGnshIACjCpZD2F0uSpXEnVch2iDLZQO0cOXC5S4eVe/onXXPUaA/Qa7vCK6W5JQISxigqenNFoLZBiMsz4k9L5PS+vyeQv1nPDOHu4wUhy+O9v10NnFHDcYUYoIWrJPbE1Q6F5zdSeYzluiFYBBEX1/A7VFERraTNBZFv0rBxU+Slb22XEeBTG/uK9OIbjrrfsuZUaZVjw+XcxSSIl683+8t6uHLxLH/QX387gkv0r8El/A68gLsSdiQsHmbfBHVN3AzwQw7pDP/55JAkgM/yUkuNfbMK/lQIuakwq5anAnV4fI/bRwT6h4Jernh91ixdm/9XgfT6D/bU1tznE4lKZt+MUYoCrPzJ1LRj1O/LvFKh/IxIeLHjFHt6NPWprUjvU+9bARYdvGs8JWEw1Ej51e45EZCLS+2EezIDiIJ6E6yVWft1LaVWXf9ZjCIm/kl+/MTK9+ggvUqTOiCDwMcrUtwn5Yl7qNLGa9mZgO/VgZ6ufTuZH7Avhx6HDERUw9A3beXIiNA9dfUnmwTCAQ74MlyfsqUMsKZOu56w9k+9apI3pvVMaywsExkOkaFl7FULdotIw2b5lSrPFHldR+CZl9t053lUkrQydQT5hlNLH1VfQUIGOnIXf+DvhY4YMiL9EP8aV/AdRTET3EmFKOhtXom1CpN7WO+LYlXEmcOCby5g6QUNqm3xvY3F+V5wM3+jFialukYHfll5N6xrYrugM4qCBPUGfFZwF9ryIA9pb+cXGLrRnRftfe39T6vDfiHD9+KE9L9ntxVnKI5dJjkgVkhsYViYzjPGQTfxJVNH5Ukc4yt4giOXeSEhuWyDmc32+hKGbg9omtj3ZXSPOykyr81E1cps542LC+NTyv4LwHp5dDoP2zz5myB3ygrHtFMPR94IbiyRHZ7SUdFy6vaP7CsXo817p7fJfP8byeVD8G+YqFd+URIvZB1ig8LlMzQ9WsnAa5hphr3tzEe9THP/YvHjXruD8vs96DoNbk4PyvkiZa/Av75fKvnhn1mJX38sJAegg7Qg6US7+1WHXFVoSH1LEQeHJfeoZ74/ru0inOETmOEkWdbRH2KL1eSIrFMlWrczcAs0iTHO5C/XHcqPGURA5f6zLBk2syvIblrfphKIRkBR3Xarb9z/bakQm6Fw+/GV9XwTDOH0Rr0qrXRnqMWqtbIV6o+4EpJI3etRVvoTf0aoaZ2ocVbNt2yqqnRe0XHzxH0DDaxGdYaKfgZvIGXY3Qk/XYeX3i4Rh2VGQFb3dYOyv+q1chTKf8kJN0jpaZCCVLS+oMv0bcCRV8lGaV/AD1N9NtzSgZQrYEmIRT4PJfI0cC/jpOHTSgXvI9yd5l1RRjwKTOwtNj2zEeDbUEoEoXhyfky+2k3Q+V4ZcX/hQUIN8URihsD7yCAWbhbAjI0ERhLzQmRper5qSEpFBI6o3n7rp3P9lGR8CkKhwibVtDjME89sruBkusDnOsUrk+mBNAyU7M32afAOip45Lbv3YjMzVwUXQOlGvoFcD5VXke7FKzMA3ErK6l+P1Q6TK+Fe4H8ETQUgslcuiyC7jV6GwcD+OrMGqL3cEc/xR4C6Me/e3lrFHL2XCTHAwmrrDqwneo/oXhFhOlVvxlsxRe+ooFjU4lXEvmRscqF1JqP6RigN/Ps8GYuDLiavxwzcWOjL3DUpD33N9TeFevxBcpGBumzlGLqfY6MkgL0rDqdXl2wVQMe33ken8InVB/VsTaCE3UzcwLxjmf4wNPiK7vkJWb2Dg4XJ1NdSwVF/vvUfnecFGb9/xuepMuBEDpbI5Y0EdAZ9fxKdCe1wKjbbGLbt3n+G1DFpjLXJE7I3sgxCQZJvfc4ZmJzkua4VtPjFfGqrTfpz4W6P/JQPtLynTx6TI9Z5xrZbo5W26W/N6lvJSjSmAmUAlXFqazCuJq+J9G7VQpD+MeqB9yiv+cTgy/A2FBv6UzhbHBwkm70FRhK9lQghTfynfLm8P6EmIAATYL3upqxEviOaTEhWQUl1xh3GnjDtF7r+nCdJMory+6p3XUHNDCyKIKqfuvRaQ4/jB41uLVBIN4F7EIw3ANhrqEsWxqBCSYxsvcCZoHHWd97KY/3xXefwSg4KIYNeJ/svfwoovUY8MZpN7zBnBWbwmiUFXjwKN5VsznOxKlFzFNdBBlEoprbTrKRugQ/wo5Pa3J5CwR1LVrtSa5rR64uAl3WXGvK7SMM1Du7uRPKS2+fP725Fu5/68I7f3fKGXIdjxT53Qk54MMB44f2E4MQXXkbDwXMPWVa7rbBQ8qRjNLojQiv9CGEfqEW0HmI2f4gXJETw4mxPFbp0NwF+Xqg2URRvr6yLfhon4H6DwSht+bpTZkSbuFA7N/hpjVNFA7qQWOPW2LtLjLEYpWsOA14iiBCwQOuZY+R5zrGAFOO1ayyeb4ywFyv7L4aOiW90182s4BzGEXVU4UNvs9QAmDvzZ+ECsoSvBFZZ3QQYz97QtouO5RZmo6iqfxz8buiF7vENY3nxi92xuuzS92ZJwgfOgYYLmufDhez7d1FJrHfWP8yWWHid/hdYdclqIn2SJt6T/gZmCV6aql6h5pzjYe/XzXIh9ZFMJln6HxkjBRuLgtVJIX8GNb/fnZOFU+uP1NNXWFK272YQlZJkTmwwkYIHZwIrgHlXLYOa2IZ2IS+89q3NAf2f9Rf9NzizN2FwlassWNtQNuTltht17qO0wWNG5251G1fRamJ1rCcnO5FwbtBRHEqZ5aS3p2mON3x3PXdKP5Fs+bx/D0Pnu6+qUJbZYPa9mgyKWfyLto4TUkznC2shA1c3pozl0F6nfhCZoudtZKtn14jo434mQ95UMvfQ4xtgf0BceSDFID3rWOG4zRvkEcrdE8vlpp6f4OujLVJ45/lFwVWHXm93U+kkjvKegby3y9AmyGbMkN+5bfrkzr6dicYQGv0joUx0nSOaM0ryUGX9XvhN8t/lMF/RAFeJhxko34helIg1aCn1nr32xmaMevt1MPVtvA0X2j249fWrqfVLe71NWOuuWfuokoFfpFiMlayURiOZUZW2n82S900JZmiuQCNpvwuJBg4WKsmQd/ZIJYjpHfA/ct94WLZakHyhIE3xE8jgkIN9vn64JxJzS1qeKn2eb7KIczH51pLLoMX6wfE+42AL9Ias3+N5OfzxPd1aJ+RoTGfKraDdnsNffnxJeJcgmz9q9T3lkv96IHML/duHNEtyIRLo0L1L/qZHb/8iQkPEXERh85dZiKCp3ai3ugDAo+zibE2vy5VldtSqhFnZ6lOq2dfDlqEd4soCF0q6vOvCnJ5B4UK/+We7ntRLsMjdAN0Lcs4Nm4EkGbAWWsnXC5t9PzBWTeFQSTf8EfWKypTWUCawoMaI9P21AmSLqNkYH0erKiuIJNVJadDt5Q9FtvapVDEhWbHJ/JsDCjvnUruMW4XUtiOonU3ZHHsKb3JvhPoj8ySacDfrpDzTKER0iwrjZ3v+3hAXL46yDno420JDaI8L6fj591JX1nJIEdCiM1BtNWk7v0nAYPBc/+fu8j6hKK0wPFV3iQB9DW8JIjs/UGb4tUMzoz13J0cjNlvQ0ko0IwBIi9xWB+mmSdJzk5CdEo3U45B1+VZli1eljfFhGT7clU9T98rQ2TU58rR2ahXGTWPBReEzJno+TL7fJL0nZspFCOLrRzl1pgbLdegiNP9jbSZdfV/aJ0IOVh1wfVx9/agwyCS4CGH2Xr/py7+L5bwkO3UI+HQyi+Hogvhi9bxoEHRJDGY0iijUIyFRwB7Kb1+5dxDkT1ct3kF1WcGDtnFKh72trDAzAABkvSrV28EdjIHiNwJfDNgJmCNU0aLO3WphFfNozDZIfrRdmqzTxeGvTHxeAYjdmOht5jVodAUjfeCDxIPd4z0NsX7rkUkPDx+PuQ4PAWjFxRee5RKFehmUPoqi4j/3Flcshy3AWHogZXRsB7KfjCxWqf4d2vHFEOKNGkmW8ABuYuhkeGoU3WMOsNQZz91zPrF1ss3v/G1Hsn18QlRFKhPBdPuUOW/VJiBXr4Lb1+9ZCpkWtQX2HDZiwlUzXQwXlrylPG2rXUU/lKTPsjY0RttaYX+Lo0zuUpqgaeQzubYpnaR+LO/Nv6509NbAPyRZL1Y5+MRqRzKjoFxoRTrHj8UsMkOXmPc7Hn6TOOOmCGvXNzz47ortyLQVhVGA3GslnXnfkNm3CviVabFbQPeZ5SyWxWbLQ12wc5HTJLxC2u6rqVaM4g6orTWS9op2nk/Ob+KTq7rpoRlQWLzN/04C1sOUuiX+WBW8MLFphNw+9V+8Y2qh6/rbEVTgLuUEicy13ZkP1bRNM5cPQltgelLyyL/UNGUherUYJIEm9KvAX9Fnsh8zTzOv/SremtEIj0xc9hW+hzJirX4YTLpJUe8cu3iVSmhsJqkrnXXm9+MnNUbkggDuFe+9bBJDag8hDLukjfjbfmuQ1wt2AmpD8bb0JfEepM7tt1VrcAHWQvcICWBHoOigVAA1vKcZ/CpDeu/OfPjx61z6Dv4oiBet7kOkhNxr7QjiXjHRXyNWKt25CEWdExdSW98Jnf2UTux+07bL76t2odyiyC45mcKt8PBGBaoNf3Le1JRtNZ/P5dJgsSQ4Ii6ajI4uOAstrTo95q+M9z9vIGiYKoK1DdtE3nyR9c1GvuTAkaR5dsdAq/onw51Qmn1STQ8l7q1XUCorkwZmrtwvMnEL0nK1lapOk47wKv5dQxXq0G+a/XtiRNfXr0a/OejyB40OPpVC6CHRPCDV3msWwVgi2EEdGkUZ68kQfM9C4zEeWHv/AQxEun3DyD369gCtEvCEouBVBwCkuEw+OHT4d467JzMPAJWQfTnoMNdDGVgNllC+RNKtjham3WlRLiEVCG0WYS65fbkpvhn7yUav3Yz7888IOORZiDGqnuckveDJHwVlcB4/O3nVkEUh5ZiNVLcVggvyDSOo368CGFY3+SK90kg888pIenvmn71sIaP2D8FrRpiKQCxeAFoo98618FInRXvkbz7/p0M6hXYhTgV1VSHc42+nJlKbcrnHfjtWiadrZdU38vymodQ1a5ZGm3q6f0hdUQMZDmFGclHfIJ2jA7/5qmiKBaCdsLerTr7ljhKe9PeaqFVkA+z1XOqKh2dyfYRRvRrQcq6lsT4gcAft0Ju8YnjkD2SnzN6PWrtBedhbVtyfarTk0UaKrZwkHwBSZeKBiaGByFLhmbOklEX+pQgPoEA2bSOBOarFCCN8TkU6DYnsCtgYsUAlRkZ5RULJDwOPgj6pBtq9to/ujvsED/cFc0V2JsitQOz59sl3j6FXmLdqcHvB13J5a60m68s7BArzw2M4gw8Pc+fe+0G+3V9SXLnFEzCrIs4+l1gcV/BideaMU8Jhw3C9lEu/mn+ZMqhDG5tPPNss8ddbLpu3Ig9GkZjWILT+pv4vyVV9quMeNhGtwzC2EjnqzPhJJ1TvnnY4KiAz33BnZ2uOu5GAG2BZee50PWYSu+IAB4Fjwgb2KaIySCW6bmqciM9qPPYdZ8y61sXIAs6NnYj4Hph0hb4xlJqCACjFsPp4llUNRmrVTJVBAs5uxGOHEZz9/WAXr/PU2wettOmlHlMKfJANSYu7Y0IUHeNiQTp4/xzbksZXmb4+lzvoLSUirdXginWOmNqrtmWLP2RknBhX/cE3+2pDYvyVdwt/qwFJAsOLjl1dEVAFWu9/Xq5uLoWf7+gDjYiAal2TpwH6TzLG/tQtSUyWAeYXEZhm+sx+pBOr0xjgA3hEobXBqBoxwRDSZT7zhZu7eIt7nPKjcmg/3XLfTPpLdUgm3Ku17Dv6N74LJuUcEUWIMolGIrQoMbnVM+kjwUGW+9EO8qy3Vg3hT8pRXg2Svbolm/vfOSa4FhtcfGAdvDxbb3q1RpBk7OUCMP0N6ciFPjDNPOc6xLfKQQpPn+1kQzu3gZ8vn3rUaGuMy5nP+HEZ9zigCoBNi5mvpcdaMJcGslyz1nupFKrRy57LQntaancYDUFsbo3J6NElGCQuQ5oGkYZrsn9PDFWoDq8o3sS22VUUMlXELjvGcLEt0LgrP3xkuNDUtJEEHTc/XmCgsUc2b6MTgmBEEtwShJLeQhIxYmxkhLc8LJsedEuyPt6WbRSSaDsEKfh1K7igeDJfiC7G5lIKvlU0y8oIKc2PgJ/zWT8Sffso7/cn1E730M5HRs+iAVDEONUjKbG06HhphGTnWWPxYKRoDLtH5hJoKZQV669eRpVzmLZx34j7uPwhOVg1u7coM1/iQ1wbFv8lOlioR7vxXIv7bCEY1eHyYWksBT7TEPfRxRLhbks2Fkh1xYEEuD4ae3kK2ln4FJU+8ZOEMh70156jMbISjL6NDb2CtheQtndq8IsvjmrInvIkYZCC+MOoD0eKhZ+s6s4RJGY5+vdeIWzrpAzSU79x8Xtcq5i0RQOzHoEPVJqfsX+2a/Jpj9usNUL/BuNKNc1BXL1OViH3s36wgKIdicjJLv58MemfPOtQZoh1tq29KG7QwQ/nU0ejVid5R26Ec4DfkdHGlJOiWOb0BUnuYLCDrzw9YnD98ePqeaBw2B7I/uf3fbj8TTTeihebpW/X2uGW8nTvPS8e6cvo2NcSl5/4mgvH4R6OjGVndnfl4IwqrNlxRr8BdCM/2UEr1omRnVElmZMQxCp8BA4kj5fvFc5+hhhJfLUBqSRc1Vhvo/vGzu+VAytd1Kty5q4Mha4k3CPfIyfEY1559grUn1B1DGsnra8RzOpk8cPq15YBMkkvB3i3QhGRg4xjiFozr+xl3x+lNU8OVFbRi74ZoPF7Pxjyeigwo+Wgx9LVjzURA1iQ7KIDfltFQRhRLiKj0KrzV5eZFDkKTkJsHHzPPq1sB9FLd9eOOPooQ/fmCQIuaiqtohQFx204m7srU+XTpI/KcwV/cNBUd9fSbpEMurw+hPxPJev0NMqNu2MFYjkJDsb8ak0VnaEnmD6ayKQNSxlA3y2Ln6RlkbfOJLuqt5xqXezwwYTuV+Y6LgpLZfoD7W4XA2OP5JG8alfbYGecy8oyAnKic3VxZmoBK3j1e866cClOSoB11juXPiHe553uXs/h/9vwfyFeDeh++wmxw/GtiBrXgYbPKep/YKviOOfqMYWooj1jxjrKVm6ZKpLSXBp/xhr15mQUL/xeYLwXVN+3sogJR3g/3G/Px9HrzhCisR0EHENTdocp3MLy7W6LMyfgBFmzT3W8uz4EUi/UCGlClsytHQd+BlOS8w/yqkzGV2BofC817/bvLip+EHiZPPjq3ha+0sm0bMjlk+++k7q2UHyNl9RmXKVe5VMvc55yxgqqqA6eEHL1DO68c7wTbHRIXxJ2/rTH8oG2btn59B8Mei27OdnIgSzKTzM5sCPBd+OQuk8EdFqAxn9IzAr9jatLI5XgVOT108XZmXgR63bDozvWfmjcP2SXE9upTCsymJ0quOvo3jPgDpsTSM48th66MNnLn0CDCXQ8X16Y0X+VaNbmwN8IR0uhQ+YFxfBX5TNP3SRGh5aaK2KdAzuvL4nrQmAwbF7AKf1asNqO3cWPRiSm6rgyJLF85wPw+vrpy/7QqCtyEFMpgIIqVQSZep+zVT6tn0xiPFW1vAKS6a85qbOUkdrDHenYvznOnfBzKILc8uLO4dv80KlER6r4/dwZzdBTk8+A+wyru52mkxqJgOwCzY6w48v2tDwlw5GBMRSzmmwPLvNip+KSFR/3paTQrCmWNgxFPpKO1i9yevbRSR7ww6Z8UMlAI+4OJbP3ZItl9OMJdFx4UQSsWRRt9G9Jjmh6STTh6bT7B0vMRmUlG/dkqbjflTA1uJz5QhIGKgObZIVUlOz+39qrisqhpzcLGse9aczCr0WhK2+TpzkYd0vcL/ZQPLcmA4IztDa3bGyTLrqITswpStuIfSvcKE3fehDwcLBjMm5Zc+kJqSxEpT8cRlDxQrV+9A7tgHB79lAd5AwuYCT6MVmgiocpq2pMrQlxHshuxxOjoa9vZF5xUXHMppBJD4M4XD5aKj008YINZ3zPzNGSKA3QbqQymRvjEYvymLE61OFRtDvHG4Fyt52g/PLmQbDYCL/HqdzNM3KvU+I6dfyADKiKoneWp94cS46lduS7ILXPVDPJWdV71j8FfvN+QTadYL+Z58dyyOK5i7uuoipJAXptfblLKOtvaVU1kkTr7Q71tlsLnQwmVJnfeoaboHcpEMsiirGfEOn+q2I9MviaIIo4Y6EIp4PC3U4pH+SBD7R/5QBSv8PEFbeZ/O2pRFC78y1wIvYDSM/4eRAY7sNocINgM7RVvolXrV9jYICwc323MSZo+A2TPLVBq9jXIuZhSp2H8Q+hzFJjbVcY+ydnMTkyU+l6aMwe5WqF/8+xiRX1MvXvgz1dluFWCLxfKAcljtbk3bHw8aDquvavipI8MgD49TiFJVrdN1rcBzhJkRxT74RAuhrGs/TJN37PyKc1GLwFT0yPxRQ77nfpTP+oubIUL752bXEYab0kyonfnTXCUUeJBYU6fCZlJXBAjxImkuz8BjvN0m828lV/ssMdHbRjPGzlOURvYOAjVJPyMcDn9Q37W4EZtWOCAQPWYiAnyDbT4GUBCZCHitEIzxJ3kzH2qbrERZs3szUVCVw0+tDUrxBVxpPBQ/7yKd8rBO/z2UwbBPUZUFRoXWC8dVgIwzvF/96QFk1t2ruS/cwTdBXQMYaHiE8xdEmD4vG/m+LrnToR6k5ji2PdLt96hb3UrEj5AMqiRuoE1tZBKZIlKRhQV1tBjXMoK+4S6RDlVZDsoTfImIyBORK7S4iThvypkpTsKGT8U9IpUDc6QFFDqmKw+ig4E2Kdzf1CzwEePF3Xr0RSvq00CRkZVwcfYOArRLMYuNLIB4mAUuXx2Ok2DET/fRn+0VhK1FpN1vqumdKtAE2HNIdLmnG1C5rarGuxLlHhYS/OXVNnduqNZkdI6PEJ1/C63eC6vDmTF+D7nQXqofOoD3aMvEVPrylzGz7Hf0eQuxrpzLB1yDFkbuXQQ8qoC705rXWp54sH8kgZglkcbYI9EGv9Z4MX01mjaVuwuNhT1L8JYyK34ZcjgqdzjVYNxUqaQ1R25S1ptyWwKTk+4bfV/hBEMOYLuX+6VufYonfNv+jxUbNHXdTh2Whx4QjEyPqLW0rLyiSZF1xwQ24EUNaaKPq7xoLC211ITrGhzZEzuKpzmzTGtymQjrrEGJEV+27xVTKNOP/OtZ6oLlWaY5HnUIR/E2xqULJR9SK+Yy6ptUuxaovtADWE9HLJ3LFrBFLuKXA0y9w44Px1zyxWuJpUDZEOMmI6eMkUixGXFqYcRi2ij++6g2MWocOyY6TRoxjMj7DnSfY3d7Gprl1cU+ATtV7+GSHG+GEev64xe2yAqH9mkpkobm0SxMvmy6tZ8b7nd5/RbjjZm43ZoPEbj8odSbfbJStOq1RXrE/l6qTGqyWSP5tjqr927DAVbq+Vbp+UbF6CmMZVlfR6bHWmejD797aTqPmxKr7tjOO+8ww2b8twxPscIc+uKXjVI4n/bBdmdTRWzsVt7Dyg9od2cw0g1rjyf4DbRHOpxxEdErPvbkiX/25J1i1jGWAjuFhYFoUmshM6YU2XfBoS7jaVGKu354NUhZbHJZdrUkJHEfV5qJy+FlC7la3AnNc4A8Qqp79lEF54IA7+2RwbGxkIVOZb9SWmizB34w7R3SfnwD5Od5OIOOE/MGF5DP37O4hsGfgDuyzANj6ojcl/mZpRisfI5cvubTBmDTmUnT6qrMSzdc8V6qSSe7HWNlTMDt0Bs2Cs5pG1XTn23nvntwi1n1qmuFK8ei1cDBe4xQ2Qku68jWib9PZ8EEPbDuyEo6BD4NoQxReYBai0inWItsLp9/HA7NlORWj1iHp9e0+dMB2smO9JM8RonAHPqwcB6HDFVp3glb1lHny8CgX7v2e57MUAAd9i4+wLanyD8AJmxPGML2+zPnpx8fnRqrPu2QcaQFufEO0VL0Z4vobMSeR/IWr99C53Gyy0uQusnJLDqsvYPYGOLrmshfAxFztBX8SP24C4wQmkEOWiUkJN7TJ9xKLZx7/Eff/mfAzQKrJACosxYfq7dGklqQfgTOlXecRc6WyLtO4HfcqoqwnK8SCPSYqEA9QXeMQZ9PCL9jctkrZIGl8252Tnew3fN7zr8p/XTFZTvPy9Zv56NDGXajGZ4iDo7Y3mnlcrHBU1ZVRpkREyQJfaoz4yH7yNT4iLDyogQN1jwQK9tXhsASpJlawmknN5gwFbi/XUQhPgrcw3HTrqbHH7vWSAiMJKESzMIwXpn9i+dDXrBIwiwqD5wxnXPK2+TfV/J50ntkQad8f5fcAzcl/MZT4Gx5ekZbxPlX7Qdzwd4J4aGDax492oMbNvRtJnmqMCyjX1z1J8BAP7KABYvIJqUI9faid2tA5qhv4gY1THR23Osan4WCW28jKhOY817eXhaxdw6I12sLh6NT/KpVDpFa+GTnDF+5jlHWVDpjtkKkvkb+NYaaeC74DJrxlcQjBjY49LUXV4netBJWCB8JtrPANH7FdtCNYe2zaligfC4B/9DpNeaJu0VJnKcQQT+QA7d4zRzk/EkyZyNv8fIfVRvd03JqOPo5K7Uzy30w70oy1XFIlvZ5GbRjTUVzrfoz/ID2R/KxEdlPLW3yJp2I+0SNR9sYCpchQFgbkToM0POJzE7G7gOz5oiooHed1+NZn5XLNUvwVED6fIqMf4QAHXS/4RqZvjLw5QOvC2uMgJlck4tyqbgNwrRvl3jevnlupp+nivFeShd/KGSTk/lzJnjzri/0z/196SbAu9FRXr93txp/0gUQleXYfeoXagFhQUxz6jkOaGpO4I6E3/HMaDcBlA5cjTfCq3dhvO4s1+ifsCrYJFyzvzayRG9PliTP/BYQjq1NmLZKL6/2owZ6nZ4hHWEs1WiJJ609Un/bPBLp5rzj71ClUfrP04SvU5ahhzX7nacKlOBdXQK0pkx1WVpzhq1R5/NplRGotAPoywMuUtoil4YHIpc4LlxBBUQeG7jgz/Bt3kPZCq4MvB5CzBBFAP7RfTS8hWVJi2GzhSj1bamJ+d0KbAvHzBUp8CUmIl8RWBPBqGdtFoCfGPgunhUr5A9tS79WweDm1jUZqgGB9f3ucqltncI7Y/gFoHyCIyKi0fFVOdiAxBjoP2XRcKhSqvqND7F7GgjjhI1j3tAGd2MyVieDqiIfN8JQzEbl42E1ts8QrwIQF2B5lwE9wuj4OBFxxmYyMHeW2/F15FxHulH7inC/VGsSJ0kqFibjpKzY4ToOnKbTOMKcHGHxFbVdppUb/1s6mUTipRGYIRlePPecfXIrD/QeQTFEWl08sk44ckiljzpS0HQTyz4+gAkFjPqX8iGUpDPSYrgJzpy3wECpfRA2Y7yB7yOoOz5Rt5KLZm1gJqzQebD6I8LA/WsknXgTdsgRQmyyFniife2/alIJQ9s3/b6IingMUKC0dTJ/rwXYCR9FdKM5pXe9T3jz8Co9CmvBztouky9UlyQmY2KPvhZNHCsYYheAb/AjcYYYUXZHCjf7gGfqaA5nn/iGIpBPA2Z8Q4rRLpTs7q8mvPtd54fpQxg6SYurqUJiDB0TnAGxt2V2zxIhD4AD9OMb+8QLcBK+2UoQKlKtYHrDbAaJx+zTz2lRWlDaudVRvs3/5q5hDvegeKKvrwpQrVQ7LXsFcwabl+cckr+ODEMb8tiHZgVdxjKoBuyBzrCVPupVzSOMAcFvDXCC/Y3o78tarLqZHSBYqYkrilqH4d5+wxAl+8aYw6Q95RAvTGRfK1MXQ747KYMqOpqaz/WJULsDffxiuI0PwbJig8iXJFMN800iCTMhXUSJemN3OQ7+5kNTfHkp/QTflNVFReA97E0/frQ/7WxYmEBnA5ZazRX7zkJEtchuttkGqxQPCJozBVqbGJoW6BDO2uUajKeJwmIGSVG4GZH3GgkKmlEvyPRCmCRMG1Li7I1t+EmFNlpuivfLFrTSVK5xvwxQwOGwymepwJhIoe7t0TRvhe0fmjspim0c+AkIh379Q6ZDS16IyAW+OZ8Z65iqszMAAQ2iawuRvvJ9v2OXD0Wzdf/KuAxlG23twN+ZqIwUeoLBiIwBcGzBwBoig0DhVtrJhYspLlExT8RpoXOmxG2SFUNDhGIx0rsllZTbvqTr8aLZsPQR7Durr4++JmJXjcMHjhu6X80VYba6klPBYPtWZh+HRfOqGCdP3BtTp0cUMvx4w1kMCzdfPly68h6N7pwO0C99uW6ZKXZXFHX2OA3xsp33SaiQR3z5lDJHG7al3Jy3N9jZYEZsB5DvD3DLrcfgTYmxRXgussKDQKjgX1OkRJBNFXe3/SJylIULocEmfs7WGs1NSWzQQEV8csrVYxDeIj1DMKXs69bkYWIUKu27yl01whs+elV+Qgn5w+vg2J6LZUQHdXkAfZbpB9hcxmX0/mkURPEbxJXpDtdVEFepBCnrY1tUh2EK/RtbWEyU28eoWfapkxaM24xLG77kumb0vqu7x+Y5US/uzI3z0yHRrAFHJnGE0gtgJXnRcxEYMcYJylmgRieFUcr1IB7FjyoAMF/JLPSi2QmThjdPueTYCTf8tBFJh51UrOQLkzjspCYEyvNvC3QrvvOQAsld67DSc59xqmvH06OkSVzpLrUXzCenMDrdZyNkOcfkixiNUWB1MzaBcmBjaujgVrQLoDxXwWeSnRnCk00JfsuTyX5WimwZ0JCJtpfeOjK8+d3asdksfndN+632W8OerhqCZn2rTgTDeI/HNqezJ4B7l9uBUn+XHu3nY2w/fJ5960wKjLYQGxhnKMvG80RV7Wi9GsMXkBGI3nHm9L9bt7/HDvGxrwiCg7PTU6Nkpr5rRxB9sHiCGWMM4boybURjsaZrwEPdGmrZHBlQCgnvXhxV99CNKVkAZRwRvYQ0fT60B8W+5Ur9FCkS0Uj9qG6uFH+tl5syFLnQ8LqYeDw22z82TdHMpIm4ZDee9JapK9D/GG29l3yUQnT2nXpo+Inuh5ZMsaolPkDZpvTga5QFEWqKjlG27DWUIf0JJCyv1xdbQ1Kh8FbIBTJNEr3NRLiEhuSkUMt2U4uPxmp7ftN3f8cPhJgr789tb/9g13uzYdOfLLSc3ZndTu3KLFVdNu6Qb6oo6qBrz3E/r4+jWgnbb8+AGcJntnAF87ZCqoKDSps/BMvGtEYe67sv9WktzQ6pNB47cq19Aw2D2LZatTr3RsyQPSd4cXzhdfk60NRGXzrpv0ByuLbFXXO8k0lwwe8p9rDeC6S79HP+EizYENvtcSOKIJnRbWIb2uVqHyrordE4WiPt2Mm6aC7xr8YUtWzF2WsP5DAlFum9xTJ4MdA2fqGkesw6T9Ph7q/7Q5ft59GlPig/onNJOJNCOcRkJTDf6xzEV557OMYTwObeUMNCvkSECO31lQ7CqYB1gYYG5IEOM5eYnvwXxJfyoTyceTMD27pxEWMm6k1xnUQz1QURscACO99acW45LSEp5IDu5L4g6NDyPwRtw5grfJulyqfodY9Z5HM+fyUzAaSKCetxXx/EpmNsxUyPHOa8g7f+mHtk/HKWHY11gcIlRqlgigCEjy+9M+hb/FLCgNa7Zg7LgA+arZsUF1biSQMPKrHut7O5tZ/t8w8OBGaiR9+e8Rjgy6dbMLCOD3DokG2GwZr/jKBuYhltcrjk/PaUtNhjsUHZ8ANaZZzstDa+pGFTMRMBHr90pDVKTBQ4/k0yORAMDjmp1JjP63d2mRE3pY0BWL2c/CA1B6NH9kUmiYB45IjOsnrNfPnCeo1LGZE056P6w6CEVeFcHf9qodOJFMhHodAfim8+BsUGDY77Tyr7eIU6M8nhHZXJD6grKeHZ75WY3mvBys4A21P1986gXFj8loqkSr2fx9LRodjR47nQKQhmybZMDWUTWVPPsWqjSHy4Ri6vdW4lusKqR/3loY+52gD4K92c6cyjpMIlqUqB3nuiAf7VgNU+7O3C8ZKGCCnEUvv5Zn7xiNwroPCD8kTWd+BrlXEeU82ImN4c6kARRLpZitk+4mW0dCrcNHqO1ZBD1QeJiDe6j74THUZZjqWx+ibi9nxLH0XxjqMBKPnn6RC1nt9VtyZcF5TVURy/13IE/HfPOWC7GL2adqDJsTkglWfNm+0mcdAi8/6U1DsBRFFR7lGI+HUIJbeTJTe0tuSWTT2fbRVVHBd4L29O/ADyiP1+DOToGPD+MWU6fOMwyJZg5bOQquoLYSjTwx5CVylkz59faJ6iG7FYrOv2YPV0Z4MRRS85BEu17uqARe5bsrIcnQ9winwCFfS5LXJJhlZWnE/u1aDZdrQDVHp9VUoZpsiQcpsVnfGP7A+Hv/DwZiD4RzkrdgABy9tJmIIE9OH6bsmus2fK3BxnMS868WyJ13h7UyQoXqEjRsw/8Z1oxDMtwSqYCuVdKRwt7CELUQihP/NMN8gfIVNySIXRDVC6LPFp4lztFqobFdh//NpRpEqm4fWPoBkkoeSDMtPH83grFGlftKHd8yS7Alt1lwc0pB7r+zFsm0my4F0rFrBUZn5Hwy7ITTLQdn+aIOYS55LErEMQtTKUhSLs9M9fkOfXqWTe6rJFKZVQXKfCBZmlGY5wc85tcFaH+S4C4kkIOIYnGG6Pbf6iHy3z/QzGkfDKeHlHKooDgmN9K2vmOsVl7wN9jcyjIlheElQmQE3zI6wGBNxrTVW8PWrxIDbuRXWhPXwGOWcspxa99mZoglJE9DV7EcqeZfhfzekyeG9EJpNy9+sYD2+2ecy6Hqv3YGdL1vpLRm1qGbPJh1EI1dX2iSx+a2gQ7khq8bZ4WjEvv6GGrHidY5ov9sWEGg1B5HN4IRaQ0mhbFC+4tld1RIOzEcS2BKW7QP5qiWU1fb79VnnufGZ1uDRWyhaELmrYOtFzhuaTB0rSKzWsfQGUg4wihNIZRDDpkEGZEM6epWJuec9g773yU5sVh6sAWYDu8bBQ8KVvcj/PuyWChKPeJI8wcz7n2T6Gxn7l+0XXWG28ldjn1e3NDJP4r2Hfn3CBz69muKzQv1NYHuwXUR+SXGJeYl0yXNJbPGY9KFB85eGoh5mfrTPCcEIWXxQhj7jeNz/Nuj/Q6SkCMiqvJst6JpX5yH2ME/Hf9///33t/13+X1yCc3FOhepvjMB3/qInHXNBK1q71rtXh5MdECKHjus/slm7223U+E5D//i1hzmCYdwSAPP4c8O8getxtGDXsE17H/fdAy59k4hza8owKF+UL9HZt3Xw7epmGEPnpZn8KnW4a8A8Rerob2FkbxkcdSz60uPINeOBUf+R1NWQG+YGG1/W1q0YBxXiC8ew5qPlf4Ig8+9xvjCciXzTJgxdWOBk+zl5woZQZWr8gst5R+SkszD6Zq1QGulD5/Vng106Ox2sTNeAiJOkiS2QBdInTa77VYc0eu78L/z8Qd+V1uwE/2E6yV+R/5pq9iHLj4+3S2BP3fojVtmS8sR9emPQxOXIj/nkkLXbwL4m4PDu89eTA+39tpSxH5zAkeZpZsfdS4r/ehT6AdlSeEJVaF7uvCg7i168o6TwlVbc1kVHDl5H1TkHyChgodo3nyf3+SucxT1mWx19il91+ivPztFVmavr6UXvf1hb83dNYZTZOw4nAt+onUH1yxYYtibOxk8oYy6FVty2NlPBGMnE894LLe7ubHinfkU5rLrW+6KDUcZNlh+c1eOoWPPr5KqPR/4ZL5G/8Qoe9Hfriu/gavmtJ29kgbHPaMznhxmyBrFy/HACuHXvn0+fMxT1VnFfy/X7vjfbTTzvfHu81vDblztl4jlZ1HdB6Sxh5gDh96bfJ2UD38VCCh/30hZDsWyWXcHYf6DKCAWOqIBvfTIry24xNc5tstrh9/vxEcuO/D5/+L7ti5blM6ncsnArY1ExrsQJpZu7cGqu7BZXwy4zsRuf2Z27eNo2/hsSX2n2lDaMNha6ukSqz9lBqRkZN0gWhlnBVwPnXsaLC6Oe5IP/hXHIePRfVXwKxY12sncbHfUdtL5I+wYvENr9EwUpBWUpLGVs+zzIZj54OvRBYaQKi3zWfXn34nhwDsRst3sw5bShxH6j0pizNL57J1hrHLi4/U/PKf/13NdBkWwq/eNkftchQJth+qhNI0eK1uCw1XkR6Ij/MFTBAETVfqZet+N/g92zBVWAIF3ZTWiX3I2FtksIZuuVR+8S/3PId/8KRkSGZ4bDl5U/VvkRaw/zK4p4s47EST1BOQY3hQDjpmNaXT9frks9VUqD8e2g2WXnW5QaX+lwHZhTtcQxxJnSYm36StHxE8o3z5o5jNv4LtTMtKv2utR3T7Uzc6qm0WhkC7Hb/93m4kQS3YglGF7VDeOJngVrLN4sa947y8uNv0UbhawR/Jswsy7Pf5k5dwh43c7s1BHu1eiKhfZ09ao8nauxe1+/bIBrlpM6XhvD55hk5+EJ0j7YdA8AJeAxraohkHzrzso42re+5F3bSnXJJpJkEWbmkn/tfFKMAZYwxPnE7R+U08oAXwGfVYkCsIiBzVbVshBShkUsm++43SuJept9HbtwOxmjdh7jz4J9MT1lmnBYCi/a4w6tLa6FbY49lBkf8QM7mCgCknmzWT784cjYprcVyfDqCHNEMcw+Sh6r5AfqX0BpEgdXiZK2Ddy+bKPDYXBcaObsfPun6wF/XW/7RiQlE28bt2dL3UxPFCIkcLktWdTp760xBxHciy5dhWVnTDxocLd0Yohlvze9zWlZG/Nh/hC2P7rblfod69Bx32gqoIaXj+yD1WEUmsdOCkN5x90fVhsBgd/ORjeUtkbevP+0mRSeXc6xmlb00747lU5tJU/2KFXmmv+FU6EZehrXdW50isgKXcbEBQNXJMxDPmsCi+RZetRZ0RN1EwZasP6o+EQCvxWIGOg6MZszHlrLz31ghUUpWnG366DSPEsMnHg8Hr1AttfRBzNomisfnSvyG1H9aS3953ZE/9d2hLs5TztIaP1JlJq+9rSPGyAzO2MNTUp5Fgf7pcgPoCk48Flx7SKFmR9mno/s084lPN0+0crsGOg/9ZHdKv52PlG27jh8J2NgvJMBbDqE5moMGjskRA3KIoxTLzzaJRk+lKcIdt0octDrF5rWbVr+0+VAhE+kh+r5NKdL+rQ66dzuq2UIHoeu6axntTN9ymmFFkgqAT7+zeGiejDH0UNGZL6Iz1YIxaA61nWVluRlkUsdAapbZ4VAM+eo34e/KerflVNZHR3wXNs9YtyX4Yq/oToazeOve7fpKGy5tDA4cthHzG/668uC2GsDMZ5hxQ9eKLrtdj97yro8s/1cSxG+sfyXZh3+HpchiRxb3GNB6KcTWmKC44bHUXFDKJfGfg90yr6CdaaMtWdP2Kxd1N75IHzjx2GqJGzc4bXyunBbvbnS0zCRAd+zOagS6WSety36X3cbMnjjhRx+6/yoxseYMXaMGyhovxyMoT3c9HtROHv4RDL7AAmSOvLja4uShvWoWeKgiH2CcNY/A4X2N1CnZ5QvtNvZnzY6Aj/7bfCXxYPSkwM8GGdbuQOr3WnFa6jpd1u6KTcP3kGDZd3qqmRR9GPTkl1UqVLgACUrCfX7MWB0x69dovuirF/Xmkv03fT/n4w/HIYcoXVP3R29z7RD/CwdbLsg3DAu6rPaDmyEnPo7rWajGpgittLCfvsZX8cZVj0YrXo4N5mbCrX+STMBngNCDZA9bmgYKz2H/0G++tjr6eU3mvUGIDcjAwPqoGcPZf8GlzGCqlyNP2mVXyilGb2Pr0Lyfcd/XKyg/LCWda9EbPRVcF7/gSyYN86B/CXFyHT2Po5kTliwbdTEa0qnkOtufnJFH6LDZquhF/kTgQhD5NN8fse0yk3Qh+8kLkv/TbmhyNhf2UeqdcJ9Acwst4a4ME4e0juJrQGpYCG8kOv3QUjpSAGv39nOOgHyfMA6YEsPMXrnlK3QGa+QjtaPDePc81z3rKvVYtUJsnuRvhZzST6/QXqSrW5vH9O/t0/44Bw0m2kMGOvVvbP5rwMzyeFCP3YodTDW018G7Du6dTDIZSoETQCwADfqz3Htyglo6hCEHQ9hn58nX2nY0pYXf+RAihxa+Bx+KH3L1IOkz2f+4My9oIhSqhxiX724pKn6cPdTbZjzH4UEE3V3Q6BA+Gau0bv5062Ed+ydZsZo5QjFhrvcHIDnECdWpOqG1PrGihE9pJJ3W+E02/on2HSlPxuwfD+X7W3qgb37e9QM46p/8g8RhfF0lF/hPWnWjQCeEaITgPZUY1U1N+HEmYIB79228liuDvghbVdztQ20Y48yTj+GJ+5pQRB+JxZ0AWohzDFWusxfk2kDNOkjrqk7DVuE9meogeRTvsPd/hOfyp/pqAibS+/fdjjp+fWmOkNGTOZBkjnxOOsn2cnzPxD2TvZ+PiQP9EAyzg1HmhK97xAoy171wsByciC+lIgdCSpx/uDpzS1PxcsqhylCsBNY3FZQDiTWsLz3EWrm/z6wHUEvRU46GvZeF1HjraNRMaiRKceXyhd/qsY3ZfnnZ+TgA9LiLMlxm/ExFCUC1kAEuyD3sUpmdm4Gjy4JSsFIS6ClmbnKWfcKGCWJUV+zjBAxrJuLFl0mSEAWAHvNiU6KcBVispD+1e0PnYsq5Ws7jM9I+Q3f35geiJ+X4vIRFIjCr8CtaBzmMxvEgKHfO8nKwQmZDWqd/QZYQZe0by78ne9k4m6QFq/uPi9zxfznvwDC2w/qLtKTx+u3YNut22v7kxbDTTu/H+y5JQbJkDH95JuehjMixTg2yRx0PcM2MHjY8Na3YG8Tx4xXaRO7zMFJO/JXN6NXL7AWQD5tUumabxvmj59/eJwirb4pvnk3t8Ywv6a8krO/Y0iEsK2Gn5SvLTdo0QQ8sTRZ+Cq4SRVsIgxovVsN9t1wNk2qij5xt5ymiRGTrzW+kUAqaPsgatuRzTn5rIRAJZkOnGxQc7jHjxZBE3kl8i32E0Yi8cS0wLS9cE+cu1l03v4fh9+K5tzleFx6AoNFPTN0ieN22R6i91aPSB7jz2itBMsJlBozwirwo1nROzxQnSTPREul9XyR/iC5JwTfVsHmKEN+T176w7H+WAZUPPpLXhwf+gKLe0IPxg+I47xFU/JaHrbIX0zyytPUMumENbRzlk5RujzdxOBWUFckjIKahaRIId4LGwK+5IDCJwGdeNQDyVSjYheVWWZfheVfF8fA/VUPien2XnkHIB/HCVuC3Qjbd7thjuwbt8dFjhyxoPjL1wZpEbj+VN0ZPVa7Guc68R3pJ1l+fQ6TWJMGiWIReG1WrWIfPp2jHY8vf3VyYOiyqLCaoqohoH7cmFcCTz98vJT9OU0/cB+A9FPsMpD5seKCJPdBsBrcX6OM+4RRmP8cd1NlExG9NxjHTomR3Z2HwnhCs9a6kVnRDTidb5AHbIzFVSpna4E/eGqdGqFOIahIMrqom1aty4YsIEClbdEv0WtPhoSt/iwAh7qd3pqwDb/qkai7BYmLFcQESvIyAsd/JTPglinUzyfGBokzajy1MtP2y4SLLQvk51zK2KrYYkElP8HUPIc0EeQA14JJvA4Ql3dibpY3diJ+fFcU3NaEgH0LhOIMvh+JFN00jT1CngszNTZer3QgM9KBXYqTZlRyIXvHYO1T/ZX4Cwg4Zlksd5f60XXa4q0RMUN4EH89BZyJotcqtfOF1LMDeK8yDh5Ivw273060Nr+McCWxbcvrwk+kH7hCT89IYMkpduhtCvE4JfdZyfZ/n7Um/qwzD/fTkvwDkuRPPZOHnfu34Q4aCiL1XSQDBe6XXtCqpGx7LFC3B0pxYXeuyMP1/YwPwAe1IG2fE/z6dJX+5DRyM+/2wGQr0oYTYgFef0R5zHqzl14j/LQvpJEM9qPamRg+YcZjsLwF7KN6qKem9y/E1o+pd3HnxnzRsmBtjXt8o14N86cVHPE+PLFh25vzP7LzYNilQFuF6Jxk35Cm2GeiRwtUKV9K1q0Bh062Df4PACZA2b/ob9Xq+2jMlnxR64xhU8adVq13xR/XurRfClKw9gTniv4IJkq0XwtFzdop6XxTW/2uFPSJlHRsHsSu7njbJOHXH6CTqu0MXxWM3ghpxnUHbG0L72PKA8i/JW+zRN8r05mCOuV1S4zraOiCunVQNMw4NIO6i6r0dzjZKtSrPoeLdhRcf4MdncelhP1joL+UGA1cwPcB0CS5iM8613Rjsgj0DPy+5MaEbjOCH0Pno7eZSQmd/8F/DtJ1To/utYTgF+jrIluW0D8S5iPQ0+jU5WaCPl7Cvo6NZlfnkrdJYjyLQoWZSPp9h053hg6D94t6OEAfX2L3H5ACG/2NXBY6hv50bGtO2JdNrUu+DhKWutHucAk8dsPpm8lbTSY04kDA+BFoC983fNY1VNfBoyiXPRFG9x5kp5HrnK8otQIDAx/QWsb0g+/wCbAULOjAa7P5ceS6e72iIQaWz6wuP7y8t2H3hZsOnz2z5SF+v4Na+eZ5txF07s4vsHAz0UoWqGS7ZX/MY/AeuUz+fHQXBKj84WO8R/X6Xq+rv5725aM0aagglXWtc3iM/zSD40WsjNbAjMT/0YGhaeAfsX/rMsw0dw/y0yjT33pQq5sX8PoUni5oH0J6FK6Lv2bpgtREXRe6jdHdijw2YVcXu03IEwfAsUbtCv3sYrsUXweicXpQC94K+P9TeH8O8tug/Bv6LgWo+KA/dJ/DS4R9Bf4dgm7dg1T62CTgk4LU7wXqFRfB4ywa6UZcQap+P3eSa52gUqugdT6rMf/H/7fwexK0OqRz7Rw8/YRyX5vHyIchj7v46enAabKD/lCyy8mNAWpRR9cKVOLXWUq/moTtkbS7XuOk/DfK/6CsgK7eKfi0QC++NtP/5/XqFNtL0Ucv6Hfqg7r27zwprf6wpevjZiL6uJj605S1Fvg66u+RN3zC581xUjOorWKyBnZ8E3y9Bf808TIZGgP9HdJa8gFQq4Tu9hwLPeHSC1bnthLGVHRwddKsCH1IY02bszl7/hHoBzFtO6SY8V0I/bo9ndej/ubPW1hsn2/ystljF+6+6OVs1j+ZA7FcyS+97FD2GdKSNFDw+GpE/l5mSXcMMBXczfKamnYdOvnl+8f2GfYv+to3KE+7nM1m98jksu/lvXl/TrTbgOdd02PRJX/pv+k/Ye09e8ybMelvPN/TRbuQvIx39bBpzYdyLnrjB4xaLped0bupxdNdOMPhx9G923uDe+be9V56qapZCOyeCv2+YK8N/uO8+zfd6HGmtu2YVM8uGdcD0A/VB319TaYgu5SQLV2qxBTUHv1eUqP8GrR2TJNUxfT9PtSbfqdi+AjyB6nrRY1UL+jYnQWlMrsTBQA+628uTzXSC37w+RroEL9PbOsLccerDl+Bj24sOkTC7+MgBaepJjDg/jfzE2hrSJMW2VQ7SMEYPmoGUMHq+ZTrtm6VsS8OFWYb6ecu+jsCehteD/K6JPq5GdqHPnamgyvIl6lLRwGj9KcA6hroD5TfCYgaqggWmoHU32v/xrKh/EvqDBi/j25vxqG/cZEJvb0hBbiJUviHqoOq9Mn8rPP9D4B6W+7M9hzZxC9b+ihAlQ9eLvvUrLnf/qZVgCpBvrAVl0oxKd9UFqB6mdlNnrfTkAN3eqC577B/1DtAZUb0vVwuv1vOa3kyUYDKiLKed50foOrTr3O+m9QcDFA1aDb7/28h5wIzdPLTnwyf1Nw8ZMrYh0SDV+v19EKZ3PRqA1TZJekPZ1vNbOpmUGt3FBS/Tn4jVHbHWfAqnf8Ks0SMT7/pVciX8Yl67J0sujOgX0K6M1b7S+UWPg+CNlW5LRJ99Yd6F/vS43gFqItSP6UO/XPjWPe0I/5b/4hX4cEu2OwFHQzponxMwIYmHdLuL2A+9eKLqVvEoDCANHu6BxS+7tWjy4pt4ldPSLON08jPg3gZti4pGIx+Sw/L0VeQV49Oh9HHR9AdUN0DVA1AY4I06fEK+aH1GFStNvFrBDbuI+/QAapwYAwrQ0n+1vwTvTWTYhf+sS5RbHhbUgMdUQ+ATob+DSWeTUpznHO9bP+CPc+bzlvtW7GK8dhy+6FH/+UKZg4vfniZpj2zi67+2Lhr7lAgpNmDuiYC01U4RffjLE30qIKz+Ltuiwx4YlyfkRuwNvdu5pWepP1aZU7mzcscxvUb9vNx076cM3NOi9aiVZ0IeN6g8U2+AepXQrtDerGhboljojXf+9DBj+vWyfxtYWRea2x1ISok+v0l9Ce/HpWjo0DwBdreXWyvF3D8R4InRrWrA3+rgM1z8UvHazJ5MBALqNRUrEuAFPLo5FC95ipYaJZ7EvnV0EFQd98o5T6UCzPqPq/B8y8a3L+6u8cxWwz6DR1NJN+zDh0GA1JNlrzFb1uzYHVLjGMgtELdOrAYpl+9KHYldAMUHLulVf1F+MJ1M8O7KfVZ81z/EVTXA+PtScvdk7b2g1Q/+vXXLy2c1EBH1QOo3fD9TfKfQW164uYyuRULuBV+PLkzeA61WBo4chBzXj57wMLdvWfmzJikGae90rCbtg3WmM6cO+PNV3KZec8S4O4QZT+fyR01fo2Ra4bl3KytBGYMN9977LBhS4blFdb/EtDXxf5xaFSAV89iPR8t+zdg9/oD4Hw4lPJFfj1B/ghtDpEeF7HvyLRcQXfMmp1LbU2ZbFqSfC4k+lwD2hua//vxBenlD6RnqtwSfmu2ZGS5pHYOthcsFSo3167Lm8rdsXI08+8SCHBMl4ZugU5NGZBZAXvL8dvWE4TxAV6nLILjfgxMj9VrvWbUjA8+KPbSNlO2323N/TSwgcQxph+k+mNZBNBGU7nQZ3TmnLEuA13MGDUzo7cbCXzqn3hZyg8gUu2M4O+nmW4tr86cM+85/sLV7ZEsIOVqcpyv7BCcrhFnA51N8nNz65Tr5Qoz/gpUvenZjcvlyTn8gX4RekwtOP66u9ONS1vd8dclSMV/BUO6S1cqBKnw9qWsxyyJz3F0dfN0CfnC0KqUZcNPehpR1z/22F8TWs/vsA3ye+rch14cYKWLl4O0Lcu7de6vYJ4+t4XqEhzXwX8XpIZA5dj9HkrzRqP0ZIWuBmP7QmjbULedsso4N2NgY8hXaucBKt5oq8mQdh6qsfvXjFwD0w9SCzOogDYAehiqSxDFH2Wtc3vT4EebsOhbF4cDoH9CpbtJxrsKdDlOaCPpxBF+tU6zOCp9fBlTE9tQ8bWqpwne+lXrm62dglP2Jf0bT+FvtumlJaO/LxZtWvFu315g1rQUaLGTYhpj/Z3fB8fff5rgs+qZK5isR9qccXTjHJ8Gaf3rwXRyPbn/e0/cJ230ezgNeoyyHtMUUpG/t1+vU/6DOtktMwtOeiFsYpkgXcbZRXOX09cllDcm/yzdLiKttdXMd6QDCQXzEup1NTXtADAsjUFzzs2DZsoWNrUe9ag07HYUG4x3KL6+Sn5Qe/hMv3pf4PT26LsR+uTcuwC6Makv/kVLi4vbIo2lk9+2RUemPjg5NN7HAEjjHUD+r6Aect1N6vGKj0tQnFo5l8+kHqQStQ3h5aW7+atTmGVMzdmiIV6OeoWdCLSekVi4sJ4y7S7K7DEzPPm7eR/vwscOLm/uM/TDzFf5L8f1HXYuM9GlN9Xx5e2yhhUyOA/0BZPSY/EKm9eiruUF9UjbFY2y60NhTfJVtZzTtD0B6mtw9BADL03WVmkai7F1S4y8JjH4aTeC/pxnX2PouKKxbciXqclw8sZ6ucrwVCK5gTbSLP2226i/huuGc2QOtCV0v+8cx64H5fP9egr55BRsdFgT4KmlFNdAWha3WhsPZD/6LMzkcow1cfZxG/ffbt0xVk0E+H//EvmhFzi6o3lIIu3alT7AxH9x9K3aTVVugX5197isWlJ+H9of2hH61LcGHjtSPtuv1yMnsEp9tg6/12cGtebgmmD0E9D5l2HcfXK53MXce/+MfoiJ2yBpH9NMy00E3ofRZ2EdYs5jRs/Ll5YbtCyZeSwlTxhbm6fUH5dzHujY7FIcid6M/0eRl/rgsLshpFmJ1BN29TvcPnXDBoP8/jm9MtcaRGmy/BuHBzA6h/GdQn4peZv8lor9VHRxSHPwFdj6vHg8KmjS6VQ1u/4cpGtT6UkSx/AH0AYpjfbFlOx0GDNgyXaN83eM8Z0Gzy0p825EOrPUvt2Y/GDJ8WU2WT/yFaGLYtp0FrE+LjK3ksEoqFmHA5T6xTLCic9wUAHGruRteidHf3p7UZupNwd9o/5f6iPINctbSOBxPFS/2WUvoxsDTtLMm0v2WWoRZihLj5vne9D2/+PLbBbLse0Jx8Urf5mJ9aGlR71t4R1X7i/x6QpQei3Y3+JZvhyWzwzyed2/9lbzyzXm93MOvCcb5G9Ar9doL0nzAUmUKtTZiHO3cJdOvnWFbatR/1k1jaLa4PNw6AnoLqjwO4nSTZE/huM9NUV7JlNHFplrk2sJxhlQmwSoAWf00tkKgXrDFTkOM3DqlYZzrA0d4hj1pruHoLWgo0Ndp7UjSCn4BXPtANIuE0ehsdW7qhnLQxnrh8GOwFsTII+TbxTk16NMH5qg2qRoW9eZt4vlNo2H6jG2OJuM9UGo9HQgTt+XK0hd1a+0Qf6q+sBR5XqcoTuJuif60V3peuQv+Z1xsmi/PK3BVZDTD9oc+Qu+nFyzHHsE6ukV83wMtZhmvPPN8vRzgl9vj5wr5e2sM72Ju4ddmSn9fr2WDCQem+fd2mORbmvzuP9VJrlaBXLfzcuPzWcyekxaSOy39X2/XEvOsW+h/U1FG1+QbwOPruqXOO4rQGm/3blf/Tw2Wt6fMSxmlFTIxI6OtWY1Nq6waa3qV9ZqwNae8WwDbSgd8kGQ/ua0eaJfPTI+os07rrzDWytv0rlacKw2gMYzqov5OzQ9MLo94S8UqFdbLEzWYPsbDOjv6UDK2mbu62oNNno7cOsN6YuChZv4oL/wNAmj5VEjgvw6lDelD8VdSoXjSl03q1sVOJ37v6rWxQssH7C6wsPJr7uY0mNk6tOoVxxVV+ok/egx7qbkpbsnToofwdPaU+0tOA16jLoew5VeYoAnXPSyiX/XQzWl5M3fyJ8ZwZXn5ufdw5WrXmsTEzlMJLYbPhwMtQoIEzVOUYlj9FLWa9p04YV6/GzOrHmXZXL5vzCDqwtrKeUzCz6PWWDmc+uWhLUXbi+a0B+qr6AptZuMtZDa43LOVWG1V2yPKSrQ5+Ip9qmgvdXxTtFVoynOOd0w32wUpsf8fXqmarb0UzBuqxnqap3dudqGnbDdFozpDX9cHDs99dzer9eQ66acP/mF4GxPfgd6wqmbtbremKvPRk1goevw/8hLT+rq4OvIgM0V6WtH6npylMYxDZhurCLn17+hF6rxSoGY1mm2RZpHJ9oC5yfQWtBA6rqDqEsCkG+h/aBfQHP8Tuh3X8qXk7fqm7ruXLf19ZRT1x2rvpDRP8ivtUzXhZlU/hqwaXNmHas93sS0yjuBkM+fvp/NeIcMnTJ2vWw21zJ79uyx4LJDcGgcrKlMwf+ZU+bpIJ8Z4DQD/Gc5Vz4sHveL6Kd0YxPsM+Xy3ina09OBZVO0l9SUbvrSSBPBv63+HumJziyc/jF5RWukKhkox+OH0PqVtKmnLr5oFqk+T4hScBz/vgeleeOZglftauJBetcuEFMDXuwfKFdbDAajuiYrMHuIXEFwl02MX38/hUOq1/wAoGv6ZfrQk1wtaxrl8zpDzrmqG55Soq4Z42NLjAoLClI13V/3xIFYGdJnIfUix2uQ1mXtVI+OAUV3nuuT3xi0T396FHotucYdmYIgo7s0irFtIo2ZBa0CZJMKCtpQ8SS2q/rMJE+LN/9FqbSsVWqH9WdZ7/fLL9O0xpCpY69t7jv8t/Ny2uoouATFe4bZ1Z2GTm3mzegel3NbE77gpzbzxnHXH+47NAqO+0FkbbFH5z70pUdNaaRfpmGkChujGEPNax3BX8ss/l5F/9U2OYA+n6+2cVw7MNFN7l/j9NpB3l7nSZKhDkyiVItO8XceaQL5eOj8SIW2FaxNd7qJ0pM+P2m/3cX9SpV5KZDA1kbYaCbXE6QulzjWH0PCuJDAQTdymlGtxw1/n/m9dOr/XwuN7lfg+3aIl7iqYO3LxNoVKOIUSwpbz3pV0LxqVfq8i8YKUCcGjXDCHUddj++7BfkR5Z/QXjMs/iMR/YiPitCtmE0AqhmcQiJIfK4p4+1C/l6AN9tryu5HkPoAUdOKPj/VnB+ll8mehs0zU7Wb0BjYPpVdqMc6w6c0n77SjjvOau43/DJCw3OIDgvHRxh52cwxw6aOHTV06sv6NCdP+mef7csTdlONmr8uVW1vpd/DoLqt0+J81MVG51dNCTsjoe/XZKTKxvTL4cpsV2XzcLOHw4wkdY6RHif9DJqaRL+oo5uSeqZfgE3dg64kAwCX0t8cfNLLdZsnadcOOmmv0S4MgfHPhE6BHiier7ah6W+Q1mm2e8LXVXFCN+aL+c7AW5hyq6d+vqyCvNW5j826LQEB86+h26D7K/CvLVXH0dna+Ff62wEemknVU1TdaKaZdDw7dQKzdfwBgumd0NV+vZpcQarWitYjnY5zo6B762HcZJO+fg+fgG/BQnMAy0J/g7QZsi6mSdKGKN0AXR1QPpH2+uNQc+KvQ+mCkcl7H+S7LfQSs4er+IZ5hP1ujv1IW3L5c31emjmP1yd4Wb35nVuNFUgXAcqlYHZLmn1E2tJG0gTHQ9ddfbOhbzxf+KMw/po7LiX4PMxvgz9jM90y6w6bMu4v+FX4Yzquz0iWYxi+95vPfOu3Syl/gj79P1YDsHkV9EBKtsvM0NcHMB8sE1TOOLLyJqm20NqqNFJpzRbYfBplENm7UGE2iPxS9HSzdQS/0b5RbQz8Xxt4abK2TNNYLbYMf7uug1evR5q1uLpILY0tbTUZ8yikv+3WBC56uW2QVakNhfiilwkPCHW5e6heabXwd7XSRlXq78FvdE/abk8+LWiDOru4FG4u/xDkt3F5NP31gfbGlxl+32Cux/DaS5VLUu0JO9qbdcXaLSW3UMT3X+SaeFsXWgVaCRoOaby/h3QDoY+ZzEluubUmbadCf4VKk56UdW3+aWvNymvdMPQFwOlxth5vppKw+QyG/Bm6932j8A+nfDc0HNKjfv1Qnoc+g/TIU3cZvSEFbHoLbnny2IRdTdVrxuvaoHLxhLiBXCdh4oS+bOlOVbMzxIuFQFffON5evMSGIhS9TH5mq78Q+Tkbt1LVC0x5Zn1bMaur4PPz+K87cS0AzjG9fX4mm3+aD5teSzA8m2Bw9PC3msewYf4L1fUQ0crzJrFUYQBjCPzAvcnZpuwBQ996+dnMtLGFhhPXXH+Z2bNmBwPUS4cu0+0o76WXSo9fpJjzWs4tnC2h7rxs4e3UELf6KngBV/46LJxKviH1xynXbYaSPnrTxxn0cXK1XmOjG213rbZ9Su30CFK/kcjAMmE/+0kPO1dDh2JzXaqnQD+Emqj3gK+99niprrD7xq3wdX4rQA2ca3Dik/7O1DMNqafxWmyDlS5Wb5BfSf6TWmyl3LYua5IZp64rT6bsa3ua254xLcSxm12lE6XH/VW2r6TZSfiqGfJNoPBM4qLwFoPWhNol4Zv+fj5g6hyZArm3kFX99zlgd7tAua2KB+O/Yq5w+ghGc5DJWJvQ7QNPT3/WgHTd03sOWfK4dAUKj0I/CiiejT31U1Mq/FHHidWgaTVZCjTGsQOpdoeOhdbEtgbfQrkneaI/QrTpgf7R0FlqT25M2JuDYHfy/wYVaDOa+r/Iq14rh82bsDEVG7+Tbeq649AdYU2JgPDvzBz+XEawpyBd64HqcKHwJrKM4BH62pdgcWk6+5q+7qR+EAf+yuWX6Xbk7J49c9Pf/upkIvGT5E8aiZna/yyz0iL7f/7RzKsZ116yCe/q7osteeTaE8a0mvlE7o3rP+KP5D1Z9nAza1MfD/swtv+wXTIt+f+E+QW7Xuavw6aOS20phmziS39If5jaLHEe/Ai6qpoO8VVrQtv9Aoz/Cnh+XM0Y1IYxfB96WmXsHA1doLISfAWiz5CzCsZbAfqE8hnQKQWFKv7DxqOQ/k6knvCrIY5J3MAY/zx0FiVvdVMY165ecnDTewu31ct+0C5j1gsdOt+UKwkL4fAdpImTCdA7kGaHFoc2g3S+bIGPC5O3a8L/odD4apzAf+2kUXMAUU3fjdwGPF/Ev4vA5lrfT3hcPjOjycf4vEpz7CmeGU+uALDNEj6vAU2qtkP8VaCqib6+Nhv08RXyhdEr/C6o63ejbc1m2dolkekOQumD+Vlq/2tWQ28kBgMSPZ5LFKDKC3QVfOoR/YuUryJfNcI7zaTc48vQE0inQ7+lnPX5Veay9X/0fwy2FqKsu1cF2t9Uaa/QLJvtdkk+N5c1hBntKKCLr6gOSbsoFDaEnm97/hYb/oz5oZ98Pm/zzOdfrsAvUHeyC5LnPcZs72fGx+toEeBOYUZ2MpGDZrdaJ7b8GnrwLkd6p5+eG7/Ger/LzZ3LiobMtcOmNJeOUasGe+0FHN0uy81pWZ3Z0iHNfQhIvcKsB3vaeZr96E2AWq9HgK1cCVSWCJTbqvh7zoWrObf0B7HStFGlDeqkr5lP3T1PrtL+YWpH+0/JbvZtYFPnrC4c/u/Z/23X+rj+ar+POuQn1sFm6ibBVE/T9Ajy3dSNV2dQkxttlX7O2G+ooLOX0D0fzBTUb0JZtC31ERXYSFN1yRqMteVMag1utnnT/3FstRRG67Z/qt7JYRViEAVdikuqST/DTpsGqEUnFbdUnRivJgYGkwuLIymvZjIGP3wuav13zQGq+vJnUrvTSbXgm3wu8HBSs4VbYVvrRCdA60QqWwS0V7C4P6Rvxw8Lq2L3HOgEZD9Adhn5gLBONXVsjoF+gL37oG1kg/oh0DXV2Au2eWONkcvOmNPyOj+BZYL8Rigz+zqbR+vzyoJX3zkthGfzAYJYLdFonTQzk8+8iXw8Z9f7bMP7tZfPT+dn3o03nxYGu55gqYtib3RWY8lBX/4KVH9hynoX8vJVrcFKqzHg352QlqO0aQKbQdDESjvF1yugH1Xarh76+H8ydFY1thnDO9CqtP8zpEBUf2B1F797NfZsbbD/CPItyau5KbCZ1kVNN5/PWpUaSAgGR0CXNYJL4KZlVneTRz49S8PP4nHXhINmTWtK+Krrzdnke9ZkqMLG+L419ECFzQrq+LospJtBXdO+INOOOxsXhHX+j/70m9Ps9FeQJiB0HWE7xoqX69As3YRvV0E/whdN3Gjms5/fA3ytra1quR92dE7v4Ntqqxx/tQfubWn1xxh0riuO0zFbCdoJ3ijyUqK/56hsRJ7KjVC3ouWWUg8pFXDwDOgSBqD1p0rhSHs+N8H/2FFEfoUIewpSzyPf0m9K+XhoY6gVWL68htwPnu7HRiFIJdcf0ZqD1IFvvvRZc5+hl/NrPaEG/+rSlOBTwUH0HRgzspFX9vnrewYhHzR/DWlufoZRJY5RIS+UC6UF9ZKgggL7q+oHkXZ6GYNtHqTS5wZQxUEqbQZBjZL0O6k4SOW8WBJatTgI/Zb3payb0o3SHhh/T7Ts5VDy2k6+aMcKy4OixQ0n+Ss4r4BX5xX/1rabg/SvCQGt076RfHS9HME2XVQ9K9bKLexoadBe2FSQeg25Aq+2SL1q6CR47svOKkltMd7pjHGJCvQfRfdJSEsrmqFPsdEq5sCeZvT7I1sbUhC0KaTZzLbCku4KSV/0mklJk3bhGxhNBFQVpNLuPag9knY00slerd+tfMaOznWRn87FvOIubS2qHRJ0XLXLSioBqjopzKSqkMvlgietWDUlnNRay9cgRdpLF51ehFwHv6aEPd1lC/x9ajIU0xhfb4d2px8em+c1Ft11ToVKd1cxJqzi5jWGrsxP4VrOoAHMNq7CAchaG9iEvODDzOStrP59ldNEjxb2Kql73jiO9B3ZfKYbZ84a6C3NSBYmZNQfpz7EjYv7upwQOg8USlbvi2+s2lwztfnMVB77f8tMawv+MqvrTW3yMo/mMj1e9LLzls605EYzM/v9oeuusYt3662t/uBV263fDuy+R3kqeZtiwHmlXRZ+5vuRJMdH/Ra+Jtedf7sn/Nfve0ly/aFPnPBfL0o+lrhBDYr4dih0dQ0mIpsyhtURvk7epudOpEMVCMDkbdR3IH+1gmZ1UQW/M6BT6mK8aJRxrgylutQNnzfBvDaD1xrEuiZ81wu+V1TTCf6VZlKraV9JG3wcAY2tpI108VETJQqA9CRF12HdSLVbYgzz6FznzCeVOoHvCsAnkvsTX5WaqFoffzXJ9wPyuj3dYVy6Dq0LfUA/71btrKFhKUilE73FXqobdGtm4fzqUDAKr9omvq4M1fXuBF+fgjaWk/T1LqQ3Yj+Dlqva8YiGkwZsu9DM7Cd9c/P0RnxOQSuzSd529Fy8k9Q+f/nHiR63DpooPJr3Mj9ju6arfD4ztJMJMvvJ/2zWO2vI5LH3+TJTPrnfyF4zspllszr+C/d/Z073SYs2TW9alctsd9aJ5ppaMr1bcpl1eJN+afYH6EGkzg+NPM/LcV7hs6XFHx7e8Y+AUoGuboG6E2Dyh8bjjwtrWfKZ5REUzjH81g/9TRasTqL8Ftf0yd2yuSneIotMXnvCM3r81K6JY/0wNLotneB4jYUqWt+Gjz+AHmlLP+P6YgxbQBX5xBh0J/6TONu1yvHrRWxoH+X552itBkPtGcPfoZ+H2B2mCi5PQgq02i2Bn2bj9Xi0W1pOMKbCzBg2l/Jtwtscesyvp5XTh97JuDQte1F28P006IwouY2Pf6tC79h00pLh4y+gi2uxh6+66dscOgzajXr0kz4U6pUYx6+giyq1j7/a/eSeStulpY/Pn2FLj+AnpWWzreyUglIAnAul9kfBNAAA2gR60iSrlFf0lZd76vvde/y9BdqbfjRzuz/lKZDuiuqexg/eeKnct9/sT9C3yCI9vBsG7rvzR83X3PEFYWDhMQ/B3Ss4cRhvtz/vO5PffPNur0z7anuenTQPmjr2bZ/fCLl8e/2jGb3WXKxleniLqUbwL+gDx/rX0F+CvHqXOa90IdVLGS1J+8LHyyH94W6YhP//hA5P6hD+nwmdnFS/Fj382hJ6uBYbUW0Zg36XH5AvGqXT6HywaYb8JVpt7i7YKRjhptX+NnFSxxiLbkYug06CtMxDa5wLT5mQFbYyg5dqKo5hCvlqqRoOGcP/W6EFT8xCclsV34ZBiWc36edj7F1Jm4qXstD2euhAmz+VyPBhWfQPhg6nPLCStrXqMo6XIM0YJk74qPWbmkVdOXGjOijit47hoeTWSas6dF2TyUKQCnibQ4/WZMnQGDA0IzYD26tITH0b6H8G1apY2D0HOq6qxhU0wufeqP+Yvs6k3GZBqslFtq+6hhnJ5bJe9pIhB+90j96iN+k5Xm0IcKwHQG1+18n5lXjLEPzTI/4PyfVHsGESY5iNM98j12y5NeF76o/B6Fezpd/D9vLBzuG/AK0f5KVZpr9DoKvStGmyxRi0Vo6uav40Zpl5bH8MrVgmaCMGYxoOvZxGd4zjI+xoe0KthSwkbG8KFWZP4V8DHVIUpZrRh9bq/TZVoyFj+P45LO1NnA+JYqv4pje1E88I0sfmGNUN2JuxxkMKtC09kQyJaq7iz2YY0Y366jUbS2iA8XwfSvzoHN9Ohs5MaL7uavg+lU7ugG6m/FzdO6yxA921Kv15fpbe/wz+P1jTReLBgNW0A6pz6efbgP16FdfB8OSi8cXr1UkSu8OnNh88fFrzdoVPhboANQlkVelwXr0FTamqcW2NCjd0CU3oZY2GClDlNz7pUdwBCcdwKPrF5SIJWwTUOEZaIqGbR82KHQnthPj70GjKcwOqKp4Tqqdd3TdtgxH2xsPfM0JWE5tjsYKoJiO1NU5zt5OTcEVB73HQtpDOs+As8aq1uWpt/alVmoKQ8QirwVWaSvyko2hf+pdU2VfdmvEb1w3HrXXrwGz4V2Z2JPdJ/Kz4RiLSWo0Czpu+0NGQvmQ2GfojpJu3hnwC5Aepa9U4blPzFTkwLQiCQd0Mk2K1POzrTlLBcL2T1gn6wXC7rIWp9wCdfSMCDxm59WUGfy9xPf04TqEd5dsn7FszIVWl4h/+fchPhf4C/Q26G2P6u7Y/f3RLwS98zQDV7W8FfS2H/dFQ3RN9KQi/p44drVdH23Gm341TSCoHJ+0G438S+17a6abz9ED7lQPltIszkxjkvPwuiZ5FR7vdVJTARDtpJApu8e8qGUdfv6ctKupogfJrC4p1KfWpi1WMMv4boKHQ/wJ96CtM/QN1a5G2Y1A426rUTkLG0Q/S9p0K9rVrg969eQ3SkpvN28mtVt36Qeo3rbgpVBigvh6zJaaCtvX4Je30dtoGDfZWh6dHq0rt/lLPfDfYOmGtkSuN7zdsu7H9hp0+YcDwQT7f5akh8HBalvhD9b7JFvx50OyATLMjsYnf1kBoVKxi+ylsgn9LJOi+XwKdKJXjwa7V40f63BDlsdC+yM6H/JvLyyin/SQn6JfePu4mBv1oHXs9g0gFDf7f7qAPaZU3TstQpXbA7Q2oqicYtNPWN4WgytQvmK0CLRmQlV6iCvDSKgb7ibSJP/51JVInRrB0jNwk/hqcYpfiFHXWNxmokNdcoX6l6okDxkoNo6/45VXoAr8tx0y/8539esJcN9KpXU8S9lmRGuNqgvQb0W5GwvRBci2pVCBbz783Vj8Lf1TRmAql+oiHA6I7xAlQIaijnqf8HpR20qxCvdOXdNC32ElVf0CjHNS36+fMza3kNXkzmrpnZsyd6S2Ryc/ZHLBW5z3491l/ujbIDaD8Hp8WfZBdR3UHPBQaNnvmvOV8u3Ny+dPG9hn2ucf3zbnvnYX+dGRTM172ASaWnvfyuRVpyxcz+PZ53pud9/IzWdf6Ots6tQry52ZymJifvDwv55fKC3Z+YOeBAp9r/kItGY+1f5k+Oql5kas/5Z680Y+rmkXPExR4zDznF6UJ24/lF+IlMM1EL4QNbdFCvfi2P0rU5+8MgAGVyQo5NfFxTFnmi2w+e+CQaWMfQ6fe6RGdt6QSDpV2SPvPaLM/pB/5fcH2yN6hvjc0h/Jz9KPf40bQ1VBc+lGcQnvKGUt3xvRDfPh3jB/Lx8iNYmxre7jzfCH9KSD+A/QLyllkd1DW414tO9DsdKtglnraaS8ZpF8FyEdDL6veQdPIdvb7bvr/VRU+6KZt1wra1Rog2rqqJni02YuSJZqxDTbmHOUnkr8e3jFBvqG8BHpV/T5DtsaH6mlXT8Dgo2kblT3G/1Pw0o1v+AnqspX0h40ctg4k1wcCKmpbST+V6Mon9L/CH+O5Cl/Xo+NE6M6j/hJl/Y29h7quzW2S5ISSgkkdiDTTXQxEL3UQNBXS59SDM0Y194Xt30Fb1GzIYgCfv0Z8GXR+US3xgukos+MGbLBKPjfnl2z6deCsWbN7F/QI6VrmhFoUT4NCxn/EajHr/PLLlM6cfGZFrK1BnLg1u+AWoj5ZV9iHHf2XaYFPt5EpX/iMdaT4/9s7EzgpiuuPd8/sLreAN54cnoACKt4KGu/gFaPRGBWPeMXbqFFjRBNv/94ajRowxttoPKLiCSLeXAKKCCyICqKCgJy7M/3//nqne3tm5+jp6Z4dYevz+U1V1/Heq+ruqlevqmtsGqkfO5OtUhKyZXAuGq7s2IYoO9WNdagTK0UQEGrI4nipLLbUSmuXNMrzYQf3XqdHTECAvo6cAXz9Teir0LnbW5Y4DS76xxidqbmJJ20w1yoz0xOXFiRd79TxaZGVebEPYuVUUqlHlRBQ9Icpqz1UWuU4DuhLXymqjhtEQP1Pp1TED05C2D48JMOAFF11/GOIWy9sPmWkp0lwc7p+QZjT5gX6xyZUmTzbh8jXN0kpPUL9bznc1wGZDKNcXiWVtmkdkHZmsdrMiDCv6StHIOvj4Ogw6Tq0oJut/8+q2DllsvnIqf7oZNKey5Zezjhk0SirieBbhC9ALtvoQVgnAHwGtiXOrSNh9dM7gReUh+vXCEsXku6o1Y85+JE420IEQw2MQ8PkgNALoKdOQ/8O1ZPrT0GvsHhAU5vhI/0QAnmXIi9Knm3u15eNWAOL+7LPW9/JvY6sqVsy9XziruAJiXIW72Vb4WFzOg8hFkVrAG0Sk7C08YdcT8RsKhOm/mWiHitqAttqPRHv9T3hsP+U61QD7rmsM6/gF7NX1G1z6vIjF7Kq3Q50vt8JSiR+Ft6bgOoZ+xK/Mb7tSLsD5LQkkXcvoLKROfhL2RoFNLHUcs9mxTKDxgSQrYO3SUFTe+PUT+R10FAbvQz+C7QfbD/iFhHWX+zafRjhvI78vk9NyEsoSyIy/AtIUV5lHO3Fqkv6VopyVI52lHL3Nb7dF0TNkzrqFIrZYfNBflnMtgmLLjLWQQuS6X8SQPy64Lti+aToLMf39f74pY8sUvhXQNcd34hbG0Q2SZRs8OuApz5U+sZBiovSUZ+XQSA+yFeR/QX1scci/DdS909jlfQfnfGa15hAGfXjjNt2H/041xr3QnFOR/BBKNQ8RKhUZ/A/0DMVXXAw8hTPGYTemkBKQ9QKqjoFHV/yDv5x8JOCOoNwoLaa2G2n9eqWTv2Ep+B64L7AOSsaIIHl9jtrqmK94rHYvmYsdl8mCSl8aEecrRc7Bxwfi8eO4fpj5WN9fjwH//8hhcug9VWW8q9A9waBcs9npkNlTjweOwhavmb38FgZr4kPMmKWlvfmiR4yLjBjbQ/m7NdT2LjwCDGTONHgD1yf03fm+AsIPzXh4WcHTO7VvyyWCu75aMSSVc6XI/93QOcC3qQCPDed8NbkWu2lZ8lxUv6kZJ0EXAU1lbivkymHf1iO+FCikXU+hA7CH4ivumtmHcT1pm75lHt3pg6vn0CahYHrBHgUxvpw4Zf4ekY0aVC7ainS1wBL2eUUCV0RScmxA/6xCq9izt6+0Ax10n44Z1wqB/sNw2aC/F2h2TtMutCsBtoi5Tqe6+ngOzeiyAD07PcHGvr6fC+QttpTJDkn+8EEMreLJJzEqHxkXww+hf4p+KGu2OaQuVeOeD/R2ku/2E/GcuZJPQ/P4R+FfEvAPeBQZNgc/z6wLJc8lJG+tz/4O3n08ZUmC6Fsa3A6A5lrMxebc8kTNP7boAWdclRa8krxlXYfmaMtZEX6Hb6UYfH8Q4rZ34nTbKNol4wl1mG5e8uiC/otwJl/fWaMP7dnx9gXCVnakhkHvNOZxTmnNt6+/WnsQ32PhdaJVoJ2tCwNsoYZj11qtF/3IYIdKXsRsm6UyZq/b33GtOJDKdOZXU37ZaZj7ZyTSFgPsDDvt+Nfmayr/4idq7fQqLbSCY9LLGvFFpwH+6FRb71F++/i5TNuq75draT537qlK0fozwG8aRGGxzm0uf8rgJY8XnWeBfxaIKVOZxbKsiGr2v1OGfybqYfqd7niSH8aTx35bwjXKi7DbUV+WQaaOOLjRP66SUJIEcgzAVI74A+Hl559/YmFBp6iXUrW/nkKepVzTYreVl54a4+uJlmyfh6LPxdat+I/g98Ov1incxqlqIbqkEXtc0/KD5V2BRA7i3q1aQY5GrY/lY+x3e+EzE7H+/iaQJXI970Synf1lNUHZyO41uQ0sIPGS+AVCEzF99LKN1ENzC9bQfjOIb7kLXnZaHvjuL98i5G9j/bmyxZOyfjXbGnNHUed2oEngLZT2Uom8s4EpyNbF3w9K7KY5nSUaw/+SIbp+Br7+uXM7CNBnawGBSllUWv2X/iQp1CWY6jwzoUylZJOW0gJPQ3/yRQdHb2hpS9ZgjVwVpzDcvls3x02P/mT7v23mPBD4kOUyMuwNUmZsR0Wy1H8Y1VfzKgdk4sXT6tPJj8y6qxxKJODUzmWUce+1uJvZyST1rU0QOeG+PRfFNNL+WMy/XPGqeRpsl+JuO3g63uQQRHmYW48ogw5V9D6BxnJ+repg63cmHHrKUcKa8iQGJ98PUwaVjRjy09mLTrQSYvS594vAk7Hp3/kOQR+gz08HyVOyz/f0zbayyOrqfbqOE5tMpr4ruS5mbBmqj/hX05cNyeT4xOnAa6vc53h/5J0322cUTbvJTJhuTZ2wa+FhyYA/8Qv1aKWNsnIEGAn5xo+WxG+Ft4P4OsvfdUpatnwRvxa/POA3V9xXazTwBmFOxmZ8inhJfOkHS5K4a1sxEjTObGXZksrJY56rUf535dCI2BZr3ITkETTYrTRUnAj2BN4x7p1m+YOHkO77Unps4JTKKqkLIdBnXcc1bK/ts9dEZRYqtyB0DiU9mUosLfIOeSyjidOYpg+/FtBb9cwaYoWdZoF7Em0h/bmnnCxQW3pmltsoXLlpx1/B68p+K5BBHl1KsS9YCewDRgKVuaSibJa7boQjAVSegNNVrydfttczEKKH4+Qe4DjgLvMJ9pcx8F24M/gNqCBuHUqrRVhpckKVQ4l8XwaXgOl5JIMVymMu5l4b+fWEOvzt8pMaCJQskORezGNiGm+3q6qyzETP5q2Owrkeyie6cpNzLy9XXyDfVestM5LWsmn6D3S2r6BltXGSiavQ/HL22FTthuIzEIAf772tw5zeFDX4X2nT3hWMk7osUNv/hL2WfLs7tTfMpKnOeEy+HeneOzIczGM8Dh8py36EdZy3CbEv4F/sK5T+fUc6d9npHjpzL2LiNcHQ1IAr3byZPE3yxKnqItzxAeKRp6ZnoLayiLFcGv84fgneNKCBnfLU3CQk4Ycej9OwJdixHzIegi/Fh+rfiDrqU0aelpq1JaBUB0yaaJwQ6hEM4gh+9dE3Yqvvkd/TnAMyBwUxmQUC3wJbR2Hdr6HwBmecLmC08NmRJ2WQnNH/EvwR4M6D481POHAQZ4Hvf9nQkAfSXrH1cA0fRT8zkeeXFm8Suo1yFzyswwN9YfbphhO8zBWv1guJ8XI7XvDYMpzswI6mozekkGvW8a178sUzbt8F2iGjLTjWuApcC9wt5oQ7o44WwK9S99INOozWX4uRxkpvR/gd8mVJ1e8d7l0GZna5MoYQvz90NDXlDoeBs9S5eYBzXz0YYatlBKWOzeVZwlhmZ8dZcBOjOoHnuzFNG/30LctXcTNIe42T3zxwWQMy1npeiofE+2B+mzzp1HeNzuse9hPP835NU2K0mO4DxKPDfv5rPPaVMUfX7Lym5cosXfxQkdfAkV0ZbrcKZ7M2uLx+B/G9eh7GOrLKVai7iBS0p8Dy5oYvYQuh7EKpZ7FNOWNOC31a8DQg5110CP9fdJPIV0DmJZTDsfP56TcDfVmoMyhIJ/S583uN3wSsu0J3SFAs/srKaj9RWENtN7B0JUJ+rsCnmXXfUlIyr4GtCvxgyzru8Q8geHUye5IPXElBZFNqxRD8aO2ED2E7FKybUdYXzCvw8UdqSg9j6c44RD8ufCQkeB4oHuhbSc7E6dntywOXlPhKQvOViEyfB1aqsv/4ctQ4jXIlNQpQ2tjaJ4K9G6vj+866qJBXOOJVqpecBPCCywqgVQ/pyyyHeWEQ/AXpmho7HZcYGXOIeDXp82/oz7vgl39lvGRT0qvrIAbZuQttV4PIe/VyBpWX5shXjiXyKeVZYxg1j+guBa4gLBrFSVtFnGaTKvNjyQt11a1nuQZQfoe+NL9fDlv4/zHV4mAmVQpYPPD12C+IVBH2BN4FVSbA3HKozLpiklA/t5iNNC3QOcsojg2OMLXgOuca/huQ9ixWl1NmmbjgV2iXZvAVlgUudkuY8vqaIc5by3WocNB1qJvz+L79397FT0abAkfNx0WN2PvL6urH1upCmqqTm+5dSNAXVfwoyX+R+oTifeNhIX1FMu6ncRX8g1pqSLxEalAOTxN4nI6ZOwIcimoMyl4GM/QcnwpAYfnJNSY8DvyuUocYXWI9zQmhxbSc+1aOuCj2bO3XyiJEbT0oaMGcteJBxd3uxEEiOsK2I9sW51dBZU2K2UgFgt1rKE55FPbDMPfLzSiuQkNy5J0F21SmyW+5CjqtBG4BkJeK/5xJRMunsATxRfJXYI6HQKeBvqww6ugqpC7LSo3hcYUym8AtNJ3BXiJlFr8PwNXQeX+zAB633cH6stKGjsauTcJ0S0Gdu47H5hC/oLeMd1tm/xFQkvV/vXQHPc2BmaAtD4LBiUZ9Xg2voLGcfhfhyZsRISoey9wO/gLcBVUseN6U/AAwUeBFFB79VNpmY58WxD3An7me5iZ1b32WlIfJ/b3bsqqHZibqp7dUDTq91xrz2Af/PlAWv4jXNeQNp2wbkBJbputuiwa/1Hxeir8n+QpkEynuwLwv/I1bav3r/tp8eX0Uhe68akA1tZZppXYK2EfbN7kEOLM7M16jXK9vysAX8CbNTXnGXUrBjI3uY16u0qflNcaI/aretPaLWE1/G95ddzQvSmXm8+94JEobtJEmQ8QUPu0vqWsZqBn+xGYfFpheBv/FXw9ONrvldY5+KHjI49WCHbyka+ULNqCYk+0qEN3wuqkevohSD73GfCT35uHNtcgkL49xpshWFiD73bBivovhexjwReZJYijSaxhxF+VmRbGNbQvy6Cj7VlXwFd9ULnc2+ViBJ+TqJ8sZDJYyKqq907Qfdb40MmDLuRdm+ucjnaaQ6L6ZCk1h+IfmQJeRbl1IpJmeIpubw/9qHh5WKQFdQ/K4VwjV1BmPC+P8py8hv8efo+gdCqk3NrUQ9vd8o7LpGvrzWP4h+MXXMnwKqlHV0hFIxeDxpEyKtiOa3U8mhXbjoabQ5yzd0IW1nonLahvPvVUYny3PigqRfyzl5S2eKsLrfrlUxy+KGtfVcdiB9YtqbsOBXWwE5/uyzpt+FIC0ss1zxWW3yRtfIkRb/V4csUKWb4OdLY0OBKheF/Rs3bcLP4C1u38kkl7EHGyROoj30Keic9g4rtdKfM4+U/EX05ZWU9vKlZIyh1QbJli8kN/52LyB8z7e/iMp+ye4E7CnQPSKbbYn2h75mrhOegtRf6bofjP8KhmpZTPmvgv5LgSOUKzeGeVgEh4dIDXBQT/nCtPBPHOknEEpNNJUr+tiBFCcdDTuPFsKMTyEOGe3EfyC3my5ExCRn0DUu1kgJa2I5RkFXRo4Q+B1rX4O3riQn0HPXRzBTvmSggaTxstoF6Z/daSoPS85aCtLQpXECdLpOuIlxL8MWkD3cjKDlyKrBqvfldITPIdQv3uIN9ZhfJ6O7mDCmVeXdJpQFtBpRG/p85pD04pbYCiJauaL4cyOrx9vMtRKKi3o4w2LH8iTzxm/qYumbi38ct8X+QCZUIGjgKyLQyByvsqxHIuZ7MezNFT85OJFZMYFlFQ013MMG/jfFRbwauyjJVOatJI7ueEy+Q/6JcPz46sT/rQRQqqFEFZ5r3vm19SP9t81N2eJVNvfUj2JWBbSpOOPpL6wVtfcj8SCXEj/weGpfJEbi3jPpmLDukzSQukoOSiWSD+XO7b+gXyhJnsvuNhEl0VaOnZACcC/V3n8oB1SlNIubdp1wFp2sWgdTT4BHi3UcwrhWaAsmsFKJO3CPXJVFCVv8lKR14i+ROf4H5qa4jXadzf2htRyWHaqA8YDLzGz5wik+8PoODqvT1oklF7PzM3BeckvholPM+DsyKs+tLYH/iiZZoj11q/7eFL6+eczmj1K7sMyhznm55bn9R+OOMXaXS0b4+Dd9PiSrxAOX0HEh+yFfTzEkkVKj4Ti+hFnC7wIGab9Bkw9TKN+NF9Zo4/f3L37TeZsNl2OyesmKvo0Q7lnlgN43nIuzdVlSXP7eBvCvNe7Y33Kn5oA4HoVrqj/u8j4y1RyQl9HSmUVZkhXqcoXBIFb+6jlLVIaHvkfRf5Z3quswXvzhYZRRx1bg/dSLYX5JC3Lkd8qNG08TLwErgFDAb7gv2Au6oWKsNwiF2MfMNKJFXuvqhcy+9Os6znBKLyuQeyDjM+huOgpwm9zs3W6o/dr/HeaW9P5HUJpwaBqdxSqI7OgF8TmMWqXXBkqNWLxTRw53Usfb9f07bToB/mLT0gaRpaVmxwllGNMnc3Ws/mTpTj8yTHwRnOtV8fXgtQRptYZGKx2JlxQ18dGvqib3u/9ALl07mihjUQWd5EuzvKpcEXg1Xx+HaxuDGNbRJP1yXrZybrkw+aCNaYx/rKDZchQOehfXlP+mA1XHlou8PwXsLP+rWjDzply0LdPgOLw2JInXcGfwyLnpcOcmpCJstGR8Jp9yNVBy1Ph+6oj569B/GzWVXC5OfHAvw6dZ0eJtMCtE6m3lsXyBNWsm2BD4tYHjr6yOOX4ELwEPk+AvsDLVlWnENG/ZNd45gQXMK2wYsGKjkrUKnghbYNXtR3Se0ZD2W53+EIPVnJb+B6O/yXnfhV2adP0QT4/Hx1tJVUGmQ+WJgv42qaFqY532hV1eYjRrmcHTBp46xO5oH1yxbuz78q6YOpqsZ25yxTw/6LzcaoVEjbAQDFi3YdOc1qkLcUfwzwX65/TPCvE974qMIIPd+MGScZsdgjpmH9U3xQnEcRnlJfnxiVSCTYk2McofoR9yXJHuuZ2RyzzH/4aIvzePmuI5++KG7lI3+zZuHd/xj0RIhLcglCuvYN/5ArPap4eI4CBwDtJSt5lAAAPs9JREFUjbwixWcffC0TXQ8OTcU53t3km+dchOxfyf08KEyayKplvkUOTcJ6vp90rnP55OOVCPfkgly8FE+9tXyrZ7ocrlzvjN2/UrcqcDEV08dOFwLHeFOOuvriwf3WhNzXR5c+CJZ7m1RU72OTqnLv9Jzu3SShxAja/23gtfC/USLJnMXhMxnoWEMd/8bfmKfxzVnuZ5xwKvctp6HU+zKuFpq73xvJg7GUvBP95veTb6vPR8tS5VGyvKXMz9rUVO1nLDT2TVrW414FFcVxMjraJSh0Gph8OPMbNLoPsmbkgYfOFNInQiwm5c/JR+CfnPT5i2Qy+SjxUVuLYGs+ZprVu6K2D9JyP8qoZlVUXWcZyqLhfrwmFbXOqjYvTcaS7lfGylc7cGBrR/4y+e/zbGhDe07HC7cf+FOqw8yZr4IS7MkB8rxI3bSMleaI+46IA4GsTl+nJUZ4Aa+bID8QjAB/pT3/ii+FaQtwF9BeyUyFJpKPVuBzGbhS/MNy1O9xaB0PvvXQfJp4v5OBLz3lyhHUUU5NVnIiYFyud3ok9RmJ/FPwbwBrRlCXkknyPNAdGyfh/1gysQYCg0Oi45dMyR8e+2VEvtO5j6FuXaTdtfdX2+7e9cjxpiccSRC+HwCtLm6EfzbQ1pSwnoFIZA5ClPslXWP3XGW9Suq9uTKtpvF/44HIq4wU2y7fbL99W3qbJh0wSuhMs8bad3ldYjBq6GNeBTVmmpdzky7HivgnyroKZX7eOssv+7FCEHiWNYUepG8jGiiri4mzFT/ocxyL0cGmrY/GTPNaKbP5eTWkkm92gzLtIzdL+fF47KBYVeyOpFE/HL7qAPI48zNW+ffp98X48Wuv3eZz5E02ZLbaLP5yYc6HOw/BwEk8E+I9LjCBCitIfdQBPyaxCM/Ge01hOa71H9w3EOyN/yrQxGcL/Mj2Q0LbHtDwZTnSctotQBau8/DzOspoNejjvJkCJMJ7CLgmQNG0Isj3CvAq0beT4SpoexW/O9IK5bigzBok/S1HcmjRyDsSSJnW5ED9z1mhEc9NqEkfmTtr8BTq0wrsCXoEp1KWkn/nHrjvZQgcvc9bCOQKkqCLj95xH2UZd1ZbQmMIXT2P6o92E1HuRR2eV2FVdGQOfvOAzkeWkaAz2BTI0qq/TNakdk5kzEMgjHxqu0Jul1wZvEoq1roWpxagUYeB68Juje/mmxs3oalBOW6dxHf0d2JBxXLk+SqSfVK8IB0SpvEv1FO9gCU76B3FaFPtErJMjh01F7jXqQD1v4IPtUZzuWVmmveaUWsKLcYRJsbGbEetmFNzJQwTreNMbNu2bdvOjm1jx7aTiW1nYts2ZmLbxnm/qnNxfsDpi77s1auruvqpPj1SsMU9BjTAP6xqR7j7NuYaTZmRvnE22W2yA8Tgo2/aLhO+7HcGIp62Wz3+bkX/e+15SV/AXMphlam+o2ahqoP/9oqAvirxE0cbYKWqW/QzW4EXKD3Fm0N+np0F3I0LxdwiL14d8ebl1nYIQtrtT6B5KE/tvNtqEtAzHSmvLiIAWSuflqcskz43dv0Q2lkDGHcq+ZJdiY4kZyW8iIJu3Bmk/kIURAlrRuDbBl0bWbLGDjJFuLjTdSuKMCpYHPpD7+TIuSLoyH7HZflPgEmKBHL6Y5f4VUy25CXFiB5jIWUakrgYChQrb6suPo2RJNmBhjjY7sYObBKWwrj94fYKgDGE4gohG4AyonOC8sIHbtAHHxnJdtmnkk3g9YS8DOJ54Bo4j/mgaa4xc732LRuAO0MwdjigFaXfQX4obZxY2adnjo4pj7G4MYiWySkfKxsNuTYBRREw09NRy0wGVrJUPXVoFjopcTTvWZvXymTdw3+h60GXNnbOS0KkLt3h5z99bG85cjoMSqndBE+fGaq2Ntme33DjzSZJ9Nbb6GpdrT/8YXGNJcEJl5v6Lb7BcHmv2VSeoZg5WJcM26fb//WnVtd+cliIGe+6JT3Z/THC1BnIHLLeNsvh/djywPeagEx1ZMZHxUBLRFj6KKZaJeY67sLQzk4fPK+7JCZHh3kTYp/LvegeGkMZq9vzitsMfhWU+qggbRt/cmHsBddqUsx7K48/3PI+UfHXprJksfXxJS7eY9VMf5XNXgBuKU3O2s3MwoomRq+PXh/e1Czz9dViiK1oIs6pHvaO3n1gC+MS8RnEL+rPjvFg2qkLbeV2hLsThayhYmb7Qv3AjdWnODpQpX+TszTGfdJGXbmU/D6j7GXPtENYJC5zE26MpAPhgyM7/QULvOel0L1YYSfSwHr0QKrQaJOJ49GzGUNdUFtURCCrFvJiinUzlfUnaHansWzsASsjjm36kfxDFlPyIVnQgO+pvs7hgwf2PObWBUP9MKjUOcJ6VEkirwUfrAyna11TOfdXglJQR1yGBc7zLKeLXJg+RVprirkc9LKnXTKdMMTcNmtBj5Sqv5h7EcKAmAgA5dgH1UvNOOGDyFnIu45DuBWqyUnwMKp+eBfLGGbkix8stKzAYhLSXgHZE8QaFmctF8S36eqB0r330tBIbh4ftgqqC3FXfr79PflBSZzVFud1U9PGaxsTnADmSL4CDO2zA6Nk1pHsoe51R4KY75+DTq4mwuSeR3QwzMvuEHxnJxfGVaI34isSN5E06GVoAHM7mnRmLbXUT1Han7qN0HvayjqZdFn2bBq+7MSjyOJZst3ivnp9FAwhuvRH3bql9Xnbdft4sPT+aJ5Ew86W3uMTsjJfbJpgpvqmXJCwG6Z1EcYhyXdf+iyq2pXSJwVNUpbpav0iasUj4uo+I8UvaFgbtBims+Kqac2txvavfPYBNaxxoVIZftmA7rJOz77MA8dPs+dJjox6/XvMJ97+CeiZDeqpj8faXKc6c3JHh9Qx+ZoOw4ucdNzrJEHhPwRU4FY8NX8Vpz5U3Lvf0QIBqoXQgzXo7By3P4jSA738Eh/xqTc+la1f+aS/lEbxAutrmbhC6NGwTOjGwjbevntdqjSck+FNhJPcuWjbWCW0J5d5hVNKVgbPBCB91p4HEkRF7OM9PllsyvgTmlDz0FCIrn6eO1m2Y/LFflueURUgmc6BbuI2ULTDjDup0Dj02QV7pakjSOP+FuxfSo4XFNVmwHf3i4pGjH4PN47+fWKv/JWSjFO9SUYYcyU3JWRebtwoLDPdhbRcKIKUdxgucKI/RXWd/+mfBasU9Dyra8OFOgpXhoI3OPCK9MXsSQNo4txjSfhXybQ+pn8GASL4G4wmJustbmWB3yx7E2md7JwRrXzdpcKH3sN6rC73I6nPuPJ3Qp1ybMIJoOh5y+Ncik1woyjgxczlzIo3+Uot7LHgqzpEp0Uh/8zUXo7QYJoitY3ql7bLBSawa/HfdwHSLKXbP/9yGIF3AoFCW8CVijxN53FTKwubuvlsGpNS4ElJ/OHag9ldLgRDricPvJNF7Rsi5E2Bv8At04PCMEBywRcTPsyfy8t642eHJ/o/zHsZzmhxVPg8uX2ZTepTLRoLp44spg6TbvJS/UtF8JVG32vmRuGFWIKcg0MAXWzYvFcslLkg1PIUn6g1kngsWP2zjInnh1wseGx5ozrnoI7m4/ihT7jiCvavFl9y+YQvkTOez9r49fb+3Mw4cVo2hPe0eD1G5jJKiNG27F7qmxdPbkbdXuoAH4EbdXybWCiqQX64uru4l9p91PrVYwzbPEzon88HI136SsDBbaTWiIcCZSxBQ852eCbd1V7q4Nj9i77PuaDitzwAlmbpmvAQZ/53GXhvLWFJY4GVoMKnT+hz8xIep/6OA3kK19Bcz0sOsVMKDle+wjLVDoVLzcomf9FO6h3bmOfVB7zsxR51KtaFo0zf+mW/ptx5X1JNOd+nOqaqat3877cf46lTzAScE2QHc+3GOGQxbvRSL4FyeHcx0IaXb14VhF7zpwgPLgN26ppjaTNAhIfRTp6nUFz6cK3va8V5nH15gi9PrrR3nZeQIhRS6mCc5bX/5MVGSOpK+i3sIFBt1GygCSpNdEsPM6tTyAeq2f3QUZbsmjdRA4VSCUPneO9CTvXDm6I7aFKjSmx2LjD1XBn/aUX8TqBFhBoihlzUoIT84q0dzKqp4rkP1lux39EjAiiHYiL3WT+IGc5LCMVeqGuPf8WdNUkYdhOPpKKR12rwi7U2JKmhWDALaJuMl1nMGCH6ifPZzNKsHj5euzKAsutr9aPzGOmLdFnltI5ImC2iJjmo1197u8CH2RFS/zx2sQU+unHsutxGeH2xN/ciu0dL+2u8UXyV3XG/Cr+MpNmMfumlwWUVbcQx+QEDOB1pnaknm+9LhK9TUlKYM6K8yNwveandKanwTsjzuLcMn7oFBVdlbhfvsg4ld2m2iGmy9xdWaBFxTEkh6W67l40qZ4/KUd3bS6tT+MP2k/KlDlkprwNrvJiJbef5OCydklxrn+g6jokDPj+k8I7lN+JVq75tVDnf91/95okXktN8up7F/cqYXpeVEnp2qS8kCKYwSWYQknP2bJO24L3dtXFXDRDWQnSR9I/dgCyx2AssmB/QHzkqRngiQssgvqGCJ9cCkQ+B0Jysmk06GZUp+a10NZlcxZ4SoMR+xVQQ8jeSf3YBfKKSbBI8OFUk1No5UUBIfTvuYNve1gGC8xw4zUghnxc8ZIgENqHr7TcsITF+hwZBb6cUq2LroCOnlMHKtZIPpBO0dujSHYIumD4DPkIjOLba9eQ9Vr5b/3EcwmR+I0RpvUGH+WvHvW1DP25909Nuu//BmFTvXKUy311vLOQX7NFD2cP02Wt5LobUhvvd4+wV4iw0L/0/f6qJ4qASMmcKPisZnFwEAb2zw3cPJ0PBwUMCSHKtEb5zVlxxUjp7ZugawF3cfCzY4nMWCEHCjw62yfy2Z5BQqEj1ovW8hVyiM97npsuE7P+ME4j9I9VJKyUcW7JA2tdKW/QYb0u8dz5txiKoExrBg1mFAo/UErLnW7wGv5YtQgNUISXUFrCgCNUy3i66OnN4hTqVTNJ2O0JhpetmqE6qjfWFsPWwa1pSaADieo/tY13ChDYsXmYnRoQaY8dwEnGmPjfMd4EqSyY42iexuryktAOWzPgoNut39I1U3i2o+KQSOV9B8DLDRk1ptKlF1h8evOKR1QM+kxtlCrXtlZ8j3j5B9LNA9jjvxJ88J4LudSXLxvt6NsMZzVubsKIJ+Tix5o5w0IzPsXRzq7lk3mjIYfo9iSg1xKk6nsYy+6eKvNoUIWyigGloI49/0Sl0g3/HvTnmXWocpQigQiKoPWmVI8qP/K7u/hCspfwyGuC0ifTcmQtfXgwo7EOnFbh11VUZgdwmCiSQO8FmgDwvtPSm+kwYPBb/egFvMmptB2MQ/kiAllBYb0oq6dFDA8QAFtDAz2kvtq52vZ347/MSiMZFbOkhPYcAhJHCSWGJLTmy59nLF1gS0b+5+O6nOr152oOdTv3MBI+Xj/gLPgirigmHBvw7IgcfbBevQV7QDBPpADR3tzu/71ter73UYXuIzSu32Avu5tyvESQbaoWp2YwzfCChrSL7QDKXsmcULhc818wWsWMZJ1B5ZcE/un7NWfz9KqPFyXYcqToe023Tm7e9j8wvQnYxactdlcOeejHnAUy9XpR4SW/oRz1rJ4zGmFit/GlbfnafGTOcYmDjyf8qEHK670dqPM+62VqhCjhlMZ0epDarMcvqZtoKVMLH8c6jRL2W1YemYlyimjPZtwjePnTdEToiU4heBblltPbDwGoNGWMo2LXD+fbmm2k4gpVN6OSH88doBta5LAMIStGfZjvum4e5WkFehFPCcD/fR9f3xZpfiAYURjLolaLUBW6CshX3tANr8JGsDPRoPa2+8HCz+illSzUq+O8ly0pIf05uAViEz75DLd/0h5GirG8KEdeCh2mlfnJ95oPTUgZu54N4xosljFIgAL20XQbb8+pdGOacC2+SqfvmQ5gUi8bmCrRRel+lXoNlAnIUebm4uSTuNPTvz4q7mFaTTPRJpASUw0xIj6CEbwtnCmORVbKZZFLP22UW0S3J2YqwAdp/YPz1m2NLdE/wnkVNWWGCkn8LTOgQ436Uj1lvBY/IVOJ3F4isvDnrQ37+EdtvUDgGWciWMIILb25vq0oUbtsXFvo2KFQ205by25d8xSdpn+TyDhYTJJYKNhTPMIH8YP0WDoWs3MIfl0sCYuCsUXyCfA8YN4ZeX8zzYvRx0mdtnBI/kM6DCvpQekAC122+KPS2TfYLqGJuFY2oH/3Lm4YRIKlo8JdF5PzORYRPngiRUxUrjyoi4OHxyj5VJzaOKud0ISgP6rFCiZusVG8ZsVMGIfbTIQtLWccbCbbHXuscsFM1E13GZ6saFnB3/m0iPyCfp8DuxPxUUAkuG8lG0iD3gFs5ZQgv5jwtjjO9md+RMXOJaDZI12j4v7Yd6pjrGlXYDP4pmVhB2BVtwLo98to+CXRUyzRHyQGrrUBXj4eiqykoM8M5qgYznw39RMNPDxUCLaStoNnMweiWpan4U7nGqNglcSqXSb0is0DTdcDzjjsuneIOJtVdY7m1LyfyxWB8RQSvIKIRVkhFt2f52kSZISYz/8o0cgGNHH4Bk3QRqBmC3zYPg36g9kPbAN0e7sJ2lw89U/af6A/XC+AKoyxFgzsxmJaEulronRslctZXnGF07Y830Txnal7/mq9M73WqEGyq+RYMsInHT/mStmKy31BkjQc220LLZ53UmT3UMHYJMnRhMdzCHVUWaAmiwzJ5xBiexJuzshm3VeWohXn3plUxa5lNJpnarN+Gy3n4Z/nNYiX75PXS5Dzf53ul8IegRuqtArrsTWL8pfNQjBJAoI/iBm4pkddFDcT7nU56r25kwgOrCvgyv2zkSQ7wWGjOpEfrg3jXWxnPY3qdhjaG3z2YXBkf50MynANBuFZysUeabV7a07+Ice7qicwfaUFf5IjyxREvoxfpwgA6BJ+8vwZAJPF4aYgBbbThC9ZwdTNZ6eoKimvecc6IfcGeonxR2qD8KTp8B9HD38/qyxIQM8s2Gz1hy6M55xZZX5dRPxGt4BNLNeY5RUV4NxyMPO9p/ubMsqy3s7bnYg76gXK6qmmGgHApyTb11nzacVQ4pNAq3vT6jeXHDjcCHdyLR2jA8fsDtP425HGZrKhtOv697qqLwaKKDUE6dgQxjFQysHPTb2TdiAH6vSpWIykJMgWvhPt/0uWdI71OrSxouj89ako+vegR/rlBdR1HRoivDjvo0QE1k2GYvPv7JR1U3c+Q6rkzUhSJnoTjr9a/6h9DvCS3K9z3VjwbcekDWFl9Fk+/ENxNaXgV9QRYVc5uT/hqH2aHIV4V916R3wqdt1ioi/af2DiZYz8mufKeEkLVAtkTX7SEU6YpEb9/wMZogvjfwQBoC66RMWZpYsBuxDy3ltG81xuvg6FfatA6W5sRItXICIdEEnliQZTMwFPbwU3BFvEXrXXES/h3tB3QPlIClz7YZyQ57vNNBN2DuxjV4SSR8JaPIDcOmsd3rzaCb9q9CkDKEvFlUJLsaMWeC3CmhtT1V8EXWlbPeZAd6xCpQfQMeQGWK+I8T9gmW3G8KFtgo/5N8UUDvpuY3+tgcjU2b4ovgh2gzWSgrPF9fXPCy0WBDceUuYn3oXApizoh68vW6Vz+JXCuxbiCzagAyb9V3/jtlYo2ZXZ+96eNwcIWVi+nb7JPm2TMZItKwbKJdGcf27tG267RmOJ2hoLl3EgnKu9wCwF3i3PcdjpqbGapLZdTMT0h2mm8TtFC0H9EG58NL9wSSOEcAPa+Y7GT7nLuhu5FmmJIoa+WoFNgq92krISQOnQXg8s+uE+EQbApPVVbW5lB75bE83paj7nSl3GZqKE5DUiTp0YCNcuVcOEC0oQo4H+uWM7236yTN8qjjGNOLNgsV9rJ2lioqVD53ZD6HVgeJX4I38pii/635KenkUJPPBI8Zuu/uE9chWDjimNIb9nG5b8eQcs01+zLR5OHSDFrgzf7R9vhj+f65XyV2kjr4JQtCzLPkYSz/f9RkN1/Ea3l73tSKLCtE4hUIG5+BJHtPuQrw3yQWyDga3NYPyczpuv0qZAWBliydt5DOc4OqGQv+TpMOyviRaNP/uXvXMKh9tXGkffzmzOysZf0+pMmh1lTyg+TKx5WOyGYJIvCTSB71B6ufNCCRhTn2FKKqBq69oeHV+g1TsRgXSs5Z+7JjHqIQj1EfX3ib2KTIqvxPyllWdtSbhoOVbAt9WhhncP23ycfz/R3YUXfch1EXkKXJ9XKx3ZnqfnynzjCfnE1qQqU5RuTb5G+A1KEAJ1Ea0/W3uAbXfLErqJW3sOqXy7nK94Ej8LfhJlvkXQZyRzI24XUlfNJhaSqFBjveCBPbX9E0KS6bPZ5wkJzs8jTacC+u4JCEo9KK7RF9eQGzC+6dp9c1ZXNmX05hn2ZQz2odUUU7sor/oW/hwRpKIoorLBc4TinLhcgaF76LeX90pHQ2lWQFlHRMTiHfEeLSnAVM3uKm5x0RnqUGrz5qAkORbYm8Db7TGEB5IxGNkGmi/NKuZo2tyBQTevPQwrQX5Wtw+pFW4bAjoWZ24t6oU3Cp2ad5PlvFtyn0ZPI3tih77HJT/2dbUrWwiKX1lswbPI6ddR5sc7aAcJkAUhOUMbxtCesZbm6BdR1rLAIJPCUpeTmrJApGqz8QeH4UUvw3fVk/n1LbDyAp681LXcUEGHD1jagq0ehZSXTviXOaChL4LflvP9IdwvI2EVW1QAO1bTu+IC5lM6se9C/1g5sL1ZXzdM1bwIpE43bmQaJkxOgrtQU9K4U8iKajsj8kO++/pSm8VzrmnlTuNeLmecWeXOJ/2jDGp/Si4xOv5QTy0xae6VI+NKuQ+M2BodlUQ2ZGrcIueMLYzAyFOImqLXCuXHCyYq9xDRtBrNgWaH6Ij76hwBoGM0LbZCpwp0uUODZIyErklKXivAR3iG8SitEJSzKEFZLx8ElMOloP0qc2TopU1b/4V+716FzeV1HVnWQOFrLMFHaP9BufmRLwKRQsFmWbk/hVNga+TAUJP4WGjb5POmyhi9jvCgiu0kLyylqENaOONJU5qmidMgb9662LGpgcrVSztj0muZPz5xWCR3jubvvMHRYbe48TZl5vxoU8ozD3am9Qdt+tIC3xfFzY7ufm8DtaEZqSZ1YC+GJOpEO3BEt6GtbGv1CC0pvzdJ+c0F/wfsK2okVvOc4y0hsI6jQhlxRK59KejCou9HAN8YhbFVuz0HsNRg4JrnTR0bNFTXjnEjZMzlXU93JiWi9ITc7FTtGA2LdZiTVLXtkzdMrhIH7jmsri5QytaPbIbmJBveCL5BXCfhzfSbQFtaugIwXXwUk0Fw6I/kOBoUsN7KF/RjIGtB0Qf/6j0NCAbE/mE858Mue8kojXF0R/EIgAhl1S3JecGnpRbqIa2bMtxOP9BBryTZm8ZzzOs59CdEipJp9/LibmgD0fz1ypXEyx0DEkwB0XZ+kJm5XnWl2R9GknqyfRF5YyCeJ1MshHMwDHVY089owVsYjoerijJyxrF6vbbFtTkXKmg5AkxjoyIoXhM77fzPdyPaxzZ8TqjGf+FG0upAbLqZ84W0azS7nGeTPjrTRV1PcvnXD+NAHqt1MN3Ysfg2LiRFkOoYldutWrRg9kr+csDpx/XJviHjWb5hP+BR8qLliA0iYq3UFGxphER8te4S6dqcoxo8DKcsKuRGKjpze6D94YU/KXX7thYzttJfb5cKuiaj5nUjsqdrLxnkAsfSVJi2F0UIMgt6fQxJFqkEy0Bw0I4BygZSrH2TjWvHM+gmQB7kXqF9blReSFIvUHdysNUhJja+bIT8w7dMksgYBYDE5Fxa32dgS7F5GV7TZcCVWi1aLjz8aUDSOR+HWFlidF3zug2vyS2XeMMUVcdWsevVKn/SOQJY9YLQRixvzwCFOxYxU9f6KXpGwogzuxJaK4gemBJ+chZVbGJWv3BVTK55rA/6gwHpc8/mo9FY0b5tCtccaTTyvp/PidOnS3wa0NflvW/5yofQYLvtD2mdvL8MYedOHkg8Fn3ElSS8BsV6Dx17SHQVi2hG/aCLv6PEh1/V9UaugHtECEtSoxFN00hQ1mSMc/7xBvv0Aw0jp5bNuKbx+MlTzRUshVVWMvUa637BJkK9MwR+Bgu/esPgGqHfIsK4JLSdujkCFGnMMwQGxZ0klbifkzj6IMUhKZBcRszPwR4FZTpySJ8UcSb1U26LTpohyeG+FTT3ZczyHulPwJm5I9x9E3XlHHoffhMpXC1HDjSV5ZVauaoKKuHTf0ast1izwGxc8sNN0ZfIN3cdcuh4JmZK15isX80YMC6HHyzTD+xp+LUSYHI9qXl84/EiL018m43falo5Pb1YnI86Wre4sP1K0YZRemKLLEqx9ORSVOkWJ5YijL2HDn3fqX03JZHFvMd4UUX1QvBSH/rdjNZlDUZ+k+KyMaUGwKtA6cQngM9YyTmH3aMKa9rWv+NLPMjQBUL3f5PtSDYNIuYn3VnhqiiP1vwhLY9F4NuYxeWsAk8kH94xqKVrpfSKvaUYqPZGtCLaihnUygUuz8M6YROBSvoOhUzWRIC+a0SoBn0hqI4wSir6QzO4/+bTB+qs1lT47mRyN96QriK4i2DfOjS14itkX4O/sVBQaug9c1o1p3neILv5YLVb8yrzPVSUUi/qsmpmrdNvss/uvPbtxU0Sc5WPTryTAcwBRFiQHtNhm1lBtLZqOqVC+xAhLVIk7hoACdTQ0plm6cuzXAuy2LOXFjQnOz+BvHD4Clfhg6Cdb/c5UL+qXnys1vmtrKXVC3XIxlZO1pah6EMBqhqzzjSvSykbqZUGxQPBmV+pUrmhetcwlpOBuXk4ynan0nTUdaum8+GrtgA8NLtNeH0LJrtS+FZVfYmnpiLkR5G4FpyvKvPHrTnB9YKo8TZ4qr7vDrvQtv9evU6GDFvoOglN4XlCCn2U8q/r8T0Ij8ILHVWJE0mwgH4IEnwf5p95JJ4SQG0XauxKELsfjNdPfhsTY1IU/HpKFQJXypVbVinqGNCS3ZrADS4wydU2hmPvmiFQWe5lziGaMAdvGb7SX/FwnCj6CzZ1aM1pz4sCcJp8+yEVN1ql+XXrDqdEI+/DWyo9huyiUYY6F5K97YcWZb9IbK7V7+CPK3BMtzr43H/f4BCmRpWmGajFn39+PFDyoUvc85yjpSe1Rpfc9TccwJKg4mkXMeYPbai1PS1aReE2vz8Qxl13psoTmD1LoS2o8XRg+djsNsYaNWVloGwsT2yXD0uyzNVN1je1vCezbLL47pmLqi+e56avdWEwPOZ2wiHv9c76/Vpww3pCtosQF6Zy1tjO2JtigAroSX4/I/rvesCngBJLjjLYr9A3paFjDJ70BIIxgTF5TrrUyQ5SvVyJ6xJCqP547KDWQq9B44DSZqvylbtcgUMFiA0a3XAUBUrjQQHsmknyoTbo7sQDCQprSTT4evh29k8A9ZvQQ+VW90utqNhKVd83MzCYDtb4AcPtcGAPv2VEmpb8wy8YR/Gf+ybMRkuSwv66KozK2ft5QyvC9E2luvt03D5QPf0M0W6p/uOGUyWvSLeXZSkC716Br5hX764JuYclXIp9tT1NG0Fe66K42EIDZchK9h0DNMIlUps/jFbKwUdiYGZ5TrEKbX0RMDOOzlSFW7611WJkQCMQbKrMds0vlSj1gfD65pz9BebfKyz26k0DudIdyMXG0Z8OkGvvN5JdFFhfMHhB/9UzwcQw0Ut2eB+63w0TTglfdaSF7nSmvKhdX2TjG77xjWLblIeSTTf9m2dRt3DdNvR5Yxktq6ioxL5v5fNMdNogv1a++LJ0kmvJTteRe3Jt+SCm7TBRElcbp2JlAKyTyGHsQYdSpPMEQHvu9TgCsAxoPL3moortMqsIjHa6wedqwmLscSdRZG3kqyxHqoN+AZOql+iv5VPoZMaLcjw5pwdpUYDpYKYZaAJzqhnI4rxCgeNdA/f4dKBOwaoTcDOggGg9mgwL8+N17clzAb8UyZpKoliIyai7+5HiZrTz4SE16FsU8DTW4nexX9rbTapmwjXpSD2dz/kPGP57wbKTSV030rUADD08XJw8e9Invt+1fM6tFR6n4CQYeydRD1hGez1xQVz5fJss/SH9yeA1n05Tc1pYuivSazzvxZaqeHdHV40m/P7/dQ8iwIVRXiMCOKGtmStWY7fdpsy2lEgp9Vg4DlyIJ5HZ4f4W5sqGvKE1XVX1TR+U/RG7vzgfODdv8qRLagkm/c16OuW3CEJt0DVW5zNAW2Hg16Bb99c97l0uDJb0K1X2uMMZ/tVPjEgs/zV+TNQkYSxigIBWeIwhg9l1KzqPArmly9FqiJeanT4AHJJWJmLTxew/KViHEKEfHp8sVNzhh0JWl97kqPkaTl6EJuCi+q37FAZLfi/zRCszF3N+hwZ5IWkPSe52Oe2wEoQO+eIYOjKVBcm8KXZm/kYwoXxfFXiCUuKUTHQrt6voAisvO6N7mI/HqgqcXcBd9A9V87at0RHOrjPoZrFCIL4twbLF2vPaDfsdDJQawDBNp1cCqWpZ9dt79b5htx1gYCjdYdiaUPuWKNGkInnvaOIpkJTrE+oiBrN3Z5tDHmK0lJrz6nGxLOvK/0+5Bs+z4f39TNCtTc+VGfyPxY2Spv8GEcWa7rBsB1sQLsNuC4q1hK4gVRXEE8n4cO8PvfaVRalkOoIvpBUdRVHPMElV53wKrO+23gp606VOihumo/9iug2Udg99VVYloC74V7XwnHJkcUta/qWafVxmG49jOUW5G1LZ7d+Mk9HTahYU9LXNvcjXGfvFfHrhj6LGHKAGXaJRGy70FOQK5nLu1xsDi52B7EzA3n3ZvFCJ9SaXXiHlX0TRcmDXK2+Qo1SFAvWxuLMpEb81QyGHNv1x74f9SdQGmM8ZkyBy5CpjSW5Ae8fZbbBo7UuKZSotR/4aNazGJ3JZyAyAfp79PtbjvbZR/GHpbJd0gxILQ3cbOrApVU9Omfa/n4wZ3yrCLlnW+Vi1ejRVS9Z/nlwnNEVYF+p92SRPtb3vjn9PA8IZLHXNhRY0xaMX4WqGzjDbjn9deazyQVkSwtqK9ycS7DnZgweplw0mw3a/xoXJF2StOXA95WNKYqhcS+JsKtlNi0otLJHvlI0CuQxLlfpPuz5WawMWME1HTcbkoOckWDPLAGgPmqvoPx+N7rexy6BgcuwkR2fwpWusDTDGuBiJZMsTuler/oEILL5kKsO7oDcXemNZo9/aaL/cgDrlj96LaQWLHin6s+aRqph8s0Wd6v9GqRKP/YBDpmf4rTl9ZAcwRQcDH4tVC33asOSoHAyTzGCGtvdiGaza25a57bFUUde9GtpuPEcmyefML4/H1L+guktvmhA/wbrHdzGE2Rgrzh9akuFSmUx9mQl0jUfmzPOsJnNIIzptRbYaC1ezoYQy+hhRWFf+waH0j31vtZl7MjDz56tsSnZl9hqRKeoHtdjj6R+CrFkiVk+skXJDsStdJvSfFd6sqB8TVmw+K/VIaJLpkmHrq1L85jaNj1VeM8RsK7f4FWvkB3wTQEk/q1lkp2xVhtVZ1FeQGkS8lYIKuhSnn2d9EQbGaQgYuc8VKk6277NkHPVjnv21HMg/NfyPqg8zM3BEhpHiELXbm+TT5zvPCXWbRkFlnMefH5rvGuqUuP2eGkbV7QGerWqsV8sy3l6nzCk7tiB8UtYcduA0t0mTuDJtqbK0OH2QvCZ9/gvH2oq4KsZ9ZZB+P4OM90QY4vkOIAX2fQ+ItK3L08tkNyxhfOuDXyXSW6pb8Y6aHDd2jDj+o6/FUMCHZPOzcp708S9OwzR1RnOBeYX4vbSw26PFj+pJ1eG1bPe/6I3Ptc63p0W0P87v4qMFmXi2wRyTF9l0Mn9+Yw3i619Et9yV8XGqHGEpI+iG78kirtX5NKvLQWH7dBcQIxfmutwlvjMGWQCusujhvy2CeHQSjztFDM+jE5Xqo3we0fzOyAfovUjQQfdyFibrfWsl5P9uRdLfdXthFD4wXofbnSHrx9nJVvTmoNZJ9FQaJTZEKluRXD1SjVUSkDHsCxG7df726GhXjhz3WQLkn0cDoQBRtwR8C9Tb8lNcMtXDEzBJfEzn9ftkl1uOf+Yag3R9NpDyL5jXnS6Belb488+7aTpPnu4V3x/MnhDRRx99mIqyHW55vK43Trc/xqRCd98C54HC7BQ/6vlWdUlcW0hh7//aGzubfAFL9UzgaxG1wYS115kuq+KnYIbcsI0VtkOW1d8gwgf+ea2Ubla9JMS83RlJFWcZtWpxaNSnfTiBwunDSH4J5xdsMGKgqzhGmJ+QbYa5dathfbR9uDB3mrfmY6jSHYzTJe4CHtYGmqdBWTmCpQk3RpKxto9HTz90gNHJj9Qgc86e/EdKVGMMzhRMZlAaOJ64vHSkE+nLxOoxCFcwLHsU5onLOrH3M4vMBgS9XUHEXq56DoQnljI3JE3I5QPyLlmkYrWilf2VkU3g03s/HukTC+lPuXJ9JoIfd0S47t/DPBZfWb5MpN7aLjrMZVFa4zSmpLRg5/OxVDyVvisEZvBwCAQAf2K8lNET8EoInRERZlMQwXUwlP4UfC+Wi1J6QJqpdW1GSF6m7vXdA6bw9Mcb7jRRhdAKaP5iOnvf+PdREdUg/HENKvjThU64E36Yt5a/7PPSZGDC5EPSi6cDAoyYMPXG9s/wMSfCnfP9/M4eALUcLpd7MvNs5jgpLbb8qzbrP9oU31rtua3xeQUAe9eLuB6ebe85fayMHdP6sFMq5ExXii0miJwKw7/pEYPxgs31hbFq64tFyB782YofVocAuG2if0sQnfa/1Cy/b93m5nuMO5Oaab7FfhApn9VbLv0LaDRvck/KmPkpvmg+FOXt6R//JvEWEDn9QivaW/Y3sdxONmncz+tyEe6zyABJCUzrM9d/O5ESkK7pPx9L7+nAqZAzmh3PybPiPmgW/iNQwEgCOn1bdoU/NAd28/tICltWbP8pHl34KaWQ/QxVzlo1t6r4GrtcKYWvrnBzHMAC71iXc8LR26FJvvdSz5oFmV0uyenMmsWUXw81Jrhd30wb36MZrvz4QUX5IBJ2G8P2tF2akOKHy7Th+HVlZk8GF3d0H/ETU5xC0CGBPrpMykY/aAE+qloF9DOoaAAWxRtxifBGBZ6t8v5l+7sAx3+YeDvjR+1+/vjKZ1YGtxHfcW5iyOpRKhz6I397R1cwviE0bKXybxgjMxHZvBRugWZbpwm32MOaumKXbD6yo4H7jYAR+7B7o0mbzD+4yHdD9R5XTB9b1OV9GjMGpScE+5nWP/Hd/7ks03FDtObGsfCf1jEDtdx9rvY8od+r/mAem02e9V8i8NDHq5VUc7Fbbi/0EKmkBaarVio83ch4r8+2x+/smE/DjB1M1lp7EiPUIf0+5PNbHsneBz0XSPd0T3VoXsafJsut5E7roQAa7/bI+1PbPaNrrPP/NYXp0/86n0s/bmSQITp3KNTrzGhZlPuVRx6X5iUDHAMDEqwsDN7+FIUhO2J/cmcfZpLKgy8yJ52RI0ufByYtor6E/8GWzts+QpmwE4RSQEJ6kSSQVmGZIgS1IzYs7ahUjVRHCnToVK8PootLREgLjhS49WhF6a8AYYDYhynRf9R3fWpOJeP7oNzhOubyo02gZu38/TJc712IWsVrPlvsMEYaFK4kdwYvFQVtG3W+InwwYE3tFqrw15pey9dr/gvqRkSRIrp9QVRzrKjAvnn5htPwOhDEVONoOPIHRdwwN4Px9VIJiCAGGsDs/vyA8d5Aw5En/x1GEqcbTj0tK8bNQEdtws7wKdL5NjPLJFpfeG2hARVeVHlJP4heEp0cCarbIx9YNv5+TbRNRpK1SFg3mIKSpvsefWdmW3vGx75uG4sitdFYmM78QKrnaTVxHEOahLnJ+ETskcpoj/mZSW5nF/6X/j+xXnaBVxrIragXhXz7FWhalIiekT2hwFJgOZJfTIR6aat2VDejTkNH+Ocvx/TT5+E566ICQecxXpg+KCgENXAjRC2SjOkU98Ax6uGetEv8wGk6iR1uaYtmMKXs9WjLJpiND3CeYizZqI6kCZ4bH0RM4MVOYk7Lv9HCHNOnVgME+ka+EvGv0NnikVj+oKpwfroifqaYz2v7UKAqEC36vycU9Z8xV/YNou0nWyd3L3yhbXQYrUGL0S2584hMkEkkHlYimdAOgd14rDlYDerdrW5d+HGYK99y95PFaCOQ3htQznsRSk6TjNbSYXYnvVJF6apCJX60ed+sbD8cPCxqDXuDmrdX7exGUyn5oIr6+sE/vmncY58sw36nJCnL5/4GVZk3NvLbMxr+B5+2m4blnw/LBACpBeglGOG8+G3r+nHi1qbgIp9rZE0ZVrqEdDHzJdjzucKeFaFr2FSYC+L8nmLo3wcm5VmKHCYSiTeTn8DO4+zkToVQ8AYku/O/p17YM3wFBrTQ5Ij1Ye1rDuxnFUEpmMVIOOlVZjxLDZyEJbxuTh9HFDKsUV1odiw/DjrIbonRQ3tFcCqIeCo+IYxnBz2FUUpWmy4DCt44Iw1nit9N9XPwnPij8c/2TB5py095w8Ct3IZR2NCKr8X2gZu+9zuLitsyphotQFs03pDhQtudlVp7aTUEAp/fy6wQ6uY1qr7D/bAoWg0nPR1YRB4TdeBsCUG3aovIL5J/WwF9qK1/rwQOZulIgADTdawk2bMXYcRyhLUOZJgekdLbBMXg0ZlfKKkBDOlB1ac+Vwqvif8jjeNicNvep8CeTPtIX/5FR7K3K/OYYwL/wFsLy4PBsnndAf6Hmz9OONQ9z/+QwvU4XVWZJjjaH8kByc7xDEEyd5tjQKjTdwXhqyrDrVdVbgdCTTt6beaQN2OPAHHGBNX3s9vrbPcVezSS3vPaH3l6fmIT1bNRkNEazKM9yShWahI06JMTbYDryJ5qTG/ryODUJ9UvNz4WweVaxeGQV6jxcdKHhMGGF8z5jKqlSO99SdXLfllbqH3virbHudwJJ6ppzagDC5diTFxXj0NxviPXKJD3pRJnoF2rnYkHVS0VN7fhmn1vCeU4QGvbciZRWFyNL+luxDa6Bkf6uNFPVIBP/lNLbrhSXQVXyjL16kIg3iuTMhKbsYLGgXpO0NDv2+3IFabYp9ocnuPE21hj4G5STZyxZqh357jxhzESa15F2suiJnElbJU3w1CldiMyz/OmUoOKcsw53fRlZWHYN/wCg33KGa7D0ZsSh2offGfPzQQrgxQrpE7z/zAh77cQsdYVNKw0l44a1NqDMrrWXHOC9Yz1eLbjHeQuHPFpyBf+7HoFh50p6O23XYVJGhvdXNxi2Lu0OyYm+E0Ly5D7dxbmpkQR2spjUzRCxEwD7sWkhcEzPyPIWj5lOL0/UWVV17cUsC5SkB14PTEf6Gcygn1spL0fkXoFqQbPV65aDuzaNzbUdxnLQ16KdPFu1WFH1GJpd4Y5I7CbRo7gklKVn69fIJWaf+rqpD8uRh8dD9nNAHFL4cqQXvT9IlyRT65zI0dRY+wZuTHVHeapo7357t7vCpdd0UkSXuSuEskWAyculzCXD19Rk6VSegu6ohvWqytIpZC4Wn/vVclcVJWfeQbWgaJn/MhMMUU1u3Af6SPAVyFrRyjqmstlb0SS7554SZJ4sLoxOENcMSTgeQ2dOo4/5YtXNgbhqfbSGqhqrzdmoBz/fdtBC1Oz5Az1AYvDbyHJyQy44E0ciAZil7lNrmMv6IOHGYFt48KjI7jxO9cbBjH5UMQeNrrxDFvP2J2+wJc+JZiAWA0Jvg1E36Zmkf0vwnl0Pc1NrNdbzNiM4eyCfHP5FwW8oDNY3Z1uPBvslBEHgzj+4KfLlNmUhpbz4fx8Pn3+Isx6RxHKWg9nca6/WX41U257I73/662kKXjgzutJDiYSz9XFchgFIV3Osi4QUDkVoUrnss51oTFS9HUCzbHBBKLLEPeXPh4Hs0LkhJo2BTlDe9/k5X6SoyevdFzCants5MLU/J1Wf2mZSCsw7IH0Smwi3jiCqrOB8m8pf22a7vURBUd6wpBnP65PKhRMr28J+PkYZlSWb6XCRipBZ1gmBLly7tFCQ3P9kcE5I8MXJ3KgThVLNvGwSNCzGcGGv6MUfEyE8JrzSzeV8pyRQMKncoy9DXMMaYp/XGX+Mh6exUiZCeyeFICScSLheoOL9F4X7EH9e2QuUrl5tM+qlv4nO8kSIP+6QulG5ijGvM/V1fuibHUNHuxiFLBqtpLces1n8pdtQyNLH0MK/r92h+Pss8Qa/qetPqxVeWjaP3j7GNWsPwH2UiPhGK+pNd6eS5uZ/hLn8OyYE/BvmmhtvxmpGLZPeOVqD6PSPIBJ12J5hRqTFv3+CSFI3INfReqfvS2zPHBHv/gTxGo9Zg8T/HdCTUhFIMikRJJ+527btB6L2YV8gJiTUchNk3kuoYcqM/AS+xA2StNMQvpMk2hKDzswFKuLrgHh9x6EUoiTEz6skXsd3Y+11rbJfZ7cGdsTb5mr5/uLGR18pTwHMBVLij3ALbFHwnbegkuLnmHxBndId/Bz48PrHRaYwSvQ0AtTMrBWjh8p5pKWDvrC3VV/9OlKB70ZaJ+6O0O+BAOVXLuF0nfqHx7Pt05o9jHnDPI7SUGmr83/JotQUVULrcN2HieQ1Gr+TNcUrAqScEsXYMpCYCFTcEedDPXXrgEJjXudsRFaqlVcaVfnotwHBTNG1SPYdPL/Mj8C+80PkkozVPof6+UYmUxj6ntW+QP+WikfaKK90eKvKcwhPDrYgrNkeYLmlrsDxYp6K8Ke4XNtFNP8T/k0gmsV/K+rINrr/oiD36y2YVxpmTeGV85d17gPPdzGTMmM0//y2cXjw+N7HFmMrak7IufaGsrb8N9vGEqjsgK0+JtdOMFFAiE0rxdwdIo1D+7OpgNzfJAf91sD0Zn5wrGatotQyT5f0gX7NVyKfdpk5J+Y5O8FF3YSkz9PLy1gVFjoPyNWjgUspghr+yyiBDN7ZQ/HAi90lNWfen5ab6bsv9Dr1MYHF/u9YwJtXHVSiJ9R84DEt4vb94cLuZpbbTKiB1GeU0W88FGOQbVWzOkiCPe+qoWgvmsmD2xVP9KMCNlR4dZLJaqRFnZyrKDwoRXUgNmKWfeEfUKdNJb8I8EanwfsjWIMLMhbfwMDWVG4GqdVNigBKofEvtAYqSONoeb6KhIVvoR1wahcroXNVYPnXEPpEaZg8V0jM30hksPGaE8MT0fM32uxiwi2QHVji+6swJ6bdCI+5vfqeRsegt129Ogbg2oDc1/PXa1dnEgnEbvHEONcuVcGdTsomXVQAUUCLS7M9Fm031zqBlV3qd9k3I6oUni29DwR8XFVPack29KUIFKXn0CV+wYaF1uc8fnMbtKoRmet4TX25OU/ssgqSX4GLhTLlQddLV6BAadYK1g3uszCDKTqg+HVwQJZZtWEzIU+bElro0VaPhXq8sczgBLGHMkH5RL0x3vJe4nZKFOxenmKC7GK19HCnV/l72m3yBSqF7aC4W1LRw4Hdn3jiyXU3kZi/sBjUgl15rI3MzY4i9/VroFrEqjX1bg2s+QnLiC2g4+tkvzcOzPedaSERTccQuq+gJp19BwIALoD0gUbUzhScl3qMFJRXjtdhGE5UJdZriAzEE0KHSyPaq5CiaphXMqRCupfRAcL0YdPVqPHXJ9DH71d46jrkNmyCb55T7UDOgfyT0rl1A+jpS8LNH/todYYIGlE1wCXu477mLa3iOraE/pB7BV8U5fFNoh+VlGyDzyrT/N5YVazbCLD6KqWW9WYVezE0ehhPqT0kN1ud1vtZbxSbGL5HLmA85/7YU5pOYbBM5nAyWgnNhJMsjLzKYVNDh+ke/3vL1FihaBn/wWFDjKkiFmAcbbxcU/TPTMckhedDhSEEOiHTu9vishWvJZPP+blIEF+pheCoK0VrlbT2zNY4/5ImRir2p2OfUFjpJYhcsI112p36D0qGXrJQs07X6dDbk3/LwUvLEManWJdDtFdTbB1B2m/+GADYnTWn1BcjyAAlgKekTHGoP4onWa9IzAb0Wg+hBIwuwHt4vvesk/9MJyVRarJC85kQaowvM/JCSRtT2BfsYBjrvpDE/BG0AMnXEmsSYMN7L0OzTtQMff6y3HYt/OnwykPQpEChQnS6ucpU12LyApHY5Il/d3HQFf/orK8f/hONpyMO4pMFjzQGqI40MZ+R/fd4dzomnLImsyuxT/vlCzFcoz/2tfrBaFXjgQ48+fJ87qRVcL6HVEBMxSIg+SerqG+mGNvSTHkRZ2Hy48SN4sm4d3G7xe0sArRCLP1b+aooBkFy0AV/jwhCBsT69TvpvRsBCoLeIXanaC4ceM1OeWVaAjbcrqPNPqOblzA3XrVxAI/IZDA5AUJHz9aB/+VEcVJEaWInrzDMnbwTyxRDEVvqzNZBr2nPuNEkN+Z9HIli3oGUqdEYDh/l9zzvilcM1WZamX8FfjyJoaF54tXTAUXrwAFb3rtZzzotzlEvylHF6Aw1Np5l7yW8gWbZWrrNsZx/uSqqyPS71bD8tRoDqGiji3Yr/0wuulrIaA1DYnBLP07rJiN9GE0p1sNZPm/2eV4EZ0kjbNBs5sk2LyhEJ5eeXqHRDemVHl8YTdbxs+NVLdtNL/YB6kdNHZOILoxQEIxuonPXrfzWVvDnV58OQZPcNVp/b+iac6hIK2So0tBJKPoWWBfSGm5+i5RBOl3ckqr+CxReiKYwXUqTesajv3Abl1s8iIawG1+IYAGRdTTcP5I/13PYzNDcL7KmapRlKJ5Cb9nmf4M9Y2yPMRvSlvVZbwNtWn2w11G5CYIswrLcYTb5yFNuDnb5dv10LpmRMorTUvQJI/5yDZ6JN1AaW3og87qet6LMs6oTc1X67nw/hb/BCfoBhD2b+xv24Uw1Vkl7NwxNNFztJJMR16Jr7exXmQAbVQdNeQJrOhaCT+obW2zKR7qwHNageSMO4jZH+4GUUaCkqgRtedAImh8uDHJif7jPS0AVXF49xquDdDCTE/ojjGC1YTFTnsn8Y2ytgzSaIL202J8dwKHA4mqVHqbnRuuxI2IJkC08C/zl9LDILZFPTiGr/owe6N6iPuVihWrLZa5UQz7O1zTFZ8j/saOBiJg80WUbnsdn2lAUDrH9ryToDkO/aztGJhI0LsgqyFwp8VS0JociecJ+Nncelw4BIchLoFKHB1QgpfcUTlC3wd4Lz3xTFfqJ4nAHmvhh6j0P/jgvL9opnKmOnJ6+HFxzs/D/TWb2QRq6MjM1LCFlFuEBkGNgNKB0qvNG59P33Xv6zrqquNCIlgUrLhL/czWvG3jSDGUioLZOGIA8XzUqJgwg10odgczMak8Npr2ISYfLANeR0tMEzWpWlxP/f1X+P6YaVobbn96T6dockBEhkP9CRkJRvE7UKPj/AFBLBwj3QQKgc7wAAC2/AABQSwMEFAAICAgA647kSgAAAAAAAAAAAAAAAA0AAABtYW5pZmVzdC5qc29unZLNjhQxDITfZc7Qkzg/jjmxWsQNacVlz47jzPbMbHeru4VAiHfHs6yAGxI3K1J9rlT5+2HhbRvO2zwd3h28Oh+qT0WrS9HlEDRLyFBKSh6YqMbIJO7w5vBgsjrPl+Pdslz18fHD5/u7QXQ1Su8ZCZECB2lIpUmqwFV7yh2DNqFeAudolEGuxll4fzJdxgoAQch1KKlzKghJndaircTURZONzH9vvw2fLve67mMfhXfdhsWDwRoUM8oCmh1x6zH67my2L5YuRUVahKreYKPM07BMJ1MxceYSs3LSSGwftyQi1xZAvWD16ABzqqa6zqf5PXx9FRo/OYJk9h3H6hrUnLF5xwmoWI5JNIaQbn9e1vmsst9MBswtGRglKEEOpVVMFrEDz+YvNAgUW3xd9x+7Nt33cTptx3k9DSrXcdl0OLd9kHlVc6J9u/WOoXRru8vLbmKqQGJGsNkbtV6jWO/diHWcjjI/Hy/z9O24XG7Xc7xf1ZJ/uNzK+Dhe9Verxk0kjWMS6RQ7W5TNVUD06DlkFzNX5zplNe62yr+5Z/7ChqXqRcRL7a2WjIIcOWEqYCEml6MCSC2lvHb7p6USaksxY+9dfPTWrGdJZBk66s1pyHbw+FLv7wsTfitPPE63496HRZ+NA76qXUXkgtkH6YiRarAGotkq3ZGCcix4+PETUEsHCGLH9MT+AQAAYAMAAFBLAwQUAAgICADrjuRKAAAAAAAAAAAAAAAACQAAAHBhc3MuanNvbuVRXWvbQBD8K+KgpQXFRLZMjN9ixSqhdeS2bl5KH9bSWl18H+LuFKIa//fuyYao2P+gAsHdzOzM7t5B7IxV4J/ROjJazJNYOLQE8qlVW7RiLm7PXyJi0YBzm67Bxwq1px31ggCOSqNGe6ORD9oDabTQNFxRoSstNb43F6suWrN6YcyeOY+g/nFar+4m0/H32dcls8bWoOkPhNInUBjKqfwNKB2z0tRmg6+eUb7xEFhb0+oqM9IEK1tvP4yn0/j8f2TRFsr9FVGSxEkap+lJYkvDLYv5z8N5M6Gtz4sTnvdIxqdkPGO5QuegDp3dL7KHZf7pDVtqLiBdM/djk9/MxPFXLErTNmEPB9FYUmC7nFBWp7Q9dqzdkXX+C68vjAhblIw96hvnecDofVRoeeJeQLYhN719FxV53rs75N1X11zPuW+WzyCpIt/NB1b3L0DMS4xAyqiCzkWkoww0VDDqA6B9JUnXAhprlBn4r8M9bGwYsP5WrIqseFhO7tLpeJL2nuFRLuw2aJXjcTOeh8L7u4H3JTkMuWSj/xPi7R6PfwFQSwcIRVCf1p8BAADfAwAAUEsDBBQACAgIAOuO5EoAAAAAAAAAAAAAAAAYAAAAUGFzc2Jvb2svQXBwbGVXV0RSQ0EuY2VyM2hiUTJoYuZawMzEyMTEwXhvzxHL3AUCBrxsnFptHm3feRkZWVkZDJIMuQ042ZhDWdiEmUKDDYUNBEEcLmEux4KCnFQFz7xkPUM1AxWQILewLETQObWoJDMtMzmxJDM/T8GxtCQjvyizpNJQzEAEpI5ZmBeiLig/v0TB2dFATpzX0NjAyMDcyNDEwsQ8SpzXCJlr0DgNuyN4kB2hY6AFdgSPMkQwPL8oJ6U8MyVVwSW1LDUnvyC1SCEoNQfspmJDFwMnsFN4rIlQjctDBk2MSsjhxcjKwNzEyM8AFOdiamJkZDhlEbLsdNiqEyqWHi973r2J31GvNnGPSXDVuZrkhsTylLil7MrbLP8F6IqGRRTo1h3JO+jVaqf/4V6L1MLwk+urJf5n/RIV8hTlkFyz6raW0dtpyRlBorY9XW/2Z0tMfcC8hrF2Omv6Ob7WqeZZbzm3rUs/G5gy/1jMxT3heemmDWVmS9ob8yz6b2iL+lWr3oja79d8sPfS5KtShVsT5sw19gv9KRTB07ZDjPfg03LX3gCPXdq6T6a0PnxxZO6xjKUbFv9JryvYxRTpXe400XLn17MPfd4fsPboWfZUMfZvVtbu5WKmCZeerf3spnnyxeHul5UHshLTRTd9+Ps0Y09SfN75mXffSyf/m5S6momZkYFxceMyg8bFBrLA8JblYxFjEelQF+dcuU0iofvN613f3COPBoUs3m7AD5IWZmT8z8JqwAykDORBAsosEgZiDSLaFzLdp5Rx/vuS3avnsOy7u289R5yBHkiBPIu6gaqB8gLFBfJtshklJQVW+vrJRTl6iaBY1UvOz9UvAiYyPaCQAR9IPT/IBhZgkm8zEGDjAkdccgobEyMLEzCtoyZ9ZlAM+p9/H7lP5+umHP3+d8LtRi9YTVfkddeezOTZeVN8T7nOlfRHf2flWkXG3G9m3LtpnVZi0cb1Zw+7GvQvbuY9J+u+5ePkrsUls5ccnlHUbm233pV7UshOo90TJIKzufwEtt2UDchRbODq9AvvyY62sV329953XmVZbi+1dvtdEyf5LJEUkswN/1oV0V6d0fWkrcfr9SNPkVVL74YyrFy329tg3cMFG/doNjzjLH+6z2QR46n6O4+4nlnv2P1mV9LUz6zJjk1n+a1lfaYuub3658mpL/2f+D5Xm9n4cs/PLZqXGKvSHur1fFPsOvqj4mHjuv2XJ8xdufbz+r8WUtPSDWQCl8a8Kexq2pLDzyMqDwBQSwcIzi9TwKQDAAAmBAAAUEsDBBQACAgIAOuO5EoAAAAAAAAAAAAAAAAaAAAAUGFzc2Jvb2svY2EtY2hhaW4uY2VydC5wZW2VV8eyg8iW3PMVs1d04AUs3qLwRnjPDoT3CCEQXz+6t03cnu7pF60VkXVURGZVnpP88svnxwqSYvwPJzieIioc8IQv8BdIVxRJ5TmOBe8K7AoLKoUTANh5O1a1KVHq190AtnBjbbBX6X5wJ1DZyghYEHugc0RIdz7FVcwHtq3wIJeTSH3GIUn+wOUPfsswdU1C8qo76y7Z37jGs6oNxZHzTkOjVwT1/Sno4hB95ZFySCdIfn3Tqgvdn9Yqf2BeOcfytidkkM52EkB9Aey7a49qnw1OnYUMem9YN8MYRBEMVneVXQE/KXWsWBtzNiQvyJb+tPuWDQGis0TEewKqe2A3ToHQT/vU0emDKb9i/B/YDlWJ/Rdh/n9d/sz/iz70t/x5EP7On0f6LZeCdxYGXRqKSOJ+c491Nv6mDn24W25IIl9vSUJn/uza/zfeP2lDf/BWFE5pgcFW3VJ3jcTsCPs5fxEAkwMVDb7WuUr7PAugK/n3bpYzLdAAiizL71nuqTdcK+1a84yzc3T8YnJGXXw13QP2J9jpXX5e9pvKlFuwou+w36er7ahq+YYy/dZUJ/7sp/fCSXsh02IYViRVzHfK1arjqWx1neXJvaL8w6wBqlxuJxYngUvMJX90kHrpjZ48X9aw0ib24gt38vYgU9k+O/ISFWvhRhXePYHvsPZE9tROwZk14OpyqtO2fge9GZ3cqpPfUNUe0a1M4ny5bRzDcfxBC4AW6RvBknyl2beFO2Ps0hhjWsKddjpDgeIY1L8W2WNjFG15Nr/jUls5AbUeQDXu9curRvEZm8aab1HVNlmvw5bkZsqZezn+Cn3wDCC9sEK8Kx0YkS/rNkuJZ77ydmFAH10zqlDfenRhQhWuadPTmHO+OcTr8mSyiX7Jq94RUBsEsWArKX673gSZy8mV4CWy8OamvrGrlzweJ82LGAwHFUzSRNoK6ibY5w12iRhhBBhahxJmX3SMmZ7NpyI27h5/XmHTXsxZCjf04cgu96h7wmSZEo34Nxm8nx6ZnIzIolN+haz+hvLuI8Bq+mpezvFlN5eWU/jw3eZ1ARznovDWGBypH8DA8tAymWj5dWS4FY9scxzQrBtx2KvLa3PKpIKdw5T36W36fCPPz8Ze+McsF3PB7rzO55H2mPWa3+uiDUR7SPv9o4Et7Tun3DVsaDSW0NfbDNpkLrHPidwrhAP7p8GlxqBL9i5/OdFBTJaNBVEWP01DiiG8o66wgcdyY6UhvYq79H7qLP3ltFzZ7Y/rUiAKSRG3AJNwY8/1+xSj7LNVXdF5NdqnIynfxcIusPBuKzqIWVDSHLB/6wkOYgFbhj/O4kElSTpAJM5dJFfJcN4WWIjbfQCIT5u2uWhrmuoQgvZK6F77VIiIEuFqv3BPUTkODFyxywUdsROr34DHyOnoemuElLtL1JRPO92jV/SUiaTOEqloIaxJez0SrSyx3AeWk0Qxw11zgVClAmyhg6zW+7qGPuTdd8trsGfHUg2ND2Z5xJVA4T6FFGfwbpipkGbnRh7ng1zCi5U0o44L0WQ8+EFp0hODsCFgiF6Zl6N9nphoTosQiYm2FICSxHbIMXvkmPVGYFrf4yI3KQlnSYzmaYZ2ZPsDQOBZWeZDVspsMvyBrrsxdapapswDLkaDjHSkpsk1tkmyMGL3Nl2NJ+FHfJrbimw5NxxaS7ZcF1UcNtDhZqpsjKddgV6/kE5FVWJMUl26Z3CtXpJLGwDQDkxkrOyIwKQvdyEMbQ9aXd4TjGuDgWLzWVyjt0hcnWc0Rx0mk5Vg7ltJyJMXPPi2H1Te8fh+eWwzrftuKkNmkrSBLhPHpYKLqJhhUFKXCQ5DBI7Fstd8bl0pvVeYDq8aNJEK43m6+UvH1PacwjyErp8DfYRqhIZr3p0aNpY+Er7kvAnUIuGlyetKhEEINRcjjp6IpOfNUFg+unbNrJaWDi1qFGVT4JHSGsJGkSVbOtKI6OFyFIAKfSjqiPdqorNtMpaR2Gd7bheBecEJshckO4fcWriSY2TdNU3qGNHr+nSDxQ72JEOsBpp9rrMRvrsYWK74vt2n8aaqb/s//4G+44Vg8H+NHL/8lzhif8URrvk9jqhAd5uZdXz7sYr/1ylfRtEVadXtdec+XoY+Y1USdvWzr2DrgPiejh/Hyw72NT3FrfiBs/suuxjTZFKwFa2QfQUJ6PsP1e76kojch779jFruPgRbIonPDA/eP+b37TO//7SmiOoLynC7shHhR37R/ym/NDoHflJSIeDEXhqSQ4awzc/dbwPZ5zwoxB05DE/Ydf5+mLyCG3yefrDzV0w5oN/Bn8L8G12gvxPm3+gC/Z0w/0YX6O+E+WddOA40yh+xFfott7Kf3MpXlWKBr4Jq4j7PLLj1A7oF1oCMqOFsIp5Ww2XyULglpduVgSM/oSCDUjs4d549FsXOyHposz8M1pAupPEYLS/C6o9X+zec0XmzSIHVH/rTk8XVncGahuYMfSzb1MrNJBkR5bTXIiTj1JcJQ7ybrv4MlBKMlL7k2uoZiosm/cVRp2201VPXRVgrN2grgV1yPueCN59eXwmWuNEyWBW+BuLjM9UGIUgX3u1EiryjS8JRtOys5nXC33ngjhYLkQveNrd7pqH03arVPUnCJ7f2shkqR4pqnNQW50fYgnJT6nI2RzXhDzi2e4bgtPpqC5CVPt+yo3LdoIx3z/66U/L5mClmL8aGJcTaeqepZJaKrs/JHC/0LumTiIziSbTgvN0hFn9TExyBYGFzcYrxXLwsmXc7/LWl4G3aSOAUbcmaBz15HBBRiUVNy4yleL0EXGgF0Di4WZ7fKrMm3ARLZ7hRSgytMHcjXkJ12A+JrR9kPt6jtlR88bI94EiSioB5FO5jd1yIfMIG1eKTS4+e4rMwkWgCqlz1hlpfiMw32CjeilNFiwWDRVZVZ6uBzfMRL+dw5OPAQbuOm6Ax6d6qBKGWCYyObuy7lwLOz+6j9UkOaoQd70vNOJqfv8W0QrvhpMv9nlM6fQDIuCjYRUh3466uQxx55VLXAIuMuLp3inzz/AtjwjmV99m1WBQwMVyBjhvzRq6vdEXjFJK9XOXQ5ACVzgIgtfHJ1jqLfCcMvrJDlnWkaFB243Mj77Ina5JhZ/Hbr/2MaFpQQl9uk11d+PqiqP652PqudfSvuCKwHuC/Ygr0W2PI+V/zjPDVmeu/fCSsv30kCKCQbrZCgM/tbJE8vTlQMTcrCGAOLu3K2ypz9RgzcNOGGDKKJd4L7krzVnVK4lJbLWZ0bQ1ZrFkRiDcA94oMaYusR85NOeN0X3TgUGvCHRzmiqhykNrKzKHwWtJH3bltea0byyebgvYvnzlm7ejmwBAFq8zzRYLQNbLkHKd7Zwf6g7wPj8Re8DGu4SwRfPZJUzjQsudl0ABBFrdXhBHHXcA6iGGcCyeJycXGmrUBT0apb69svGUx+oL5XcvFm5NGEvsEmSAf1yq5ClVSE8zxBLDSFQrUaKPLeuoh3odZfdPm6Lu6Pe45YfjEe+OkG4UP9Z13mEiE+05+pInMHEnqew576AdnQ5Kl5/7gPstSWM1jHLoubpWjP+4BY1/5xt7PzoVx+xmV0VT2Mdfbj6reUaAbFHV1eR76THr/HqgNEpMIAkf2jIgsz9ESStqXIDFeg+bbc04K+GqazmUxWrh6NHOW4bWS5JXsQy4P46qdVcZAPFLS0A8KeeS135igm/CuWLPp8KbBjbmmRizqZVsH+pq7ZOZjhHvSaQDxdRuUADzTgD9Ni0iW1sajtcRm2xsuSrD4ZShXcri7lDWFFXBAtT+ENw/fvegdCGwBpeNF4/i8yUI8LCkPxJ2ZPbh9tZR6gVW+4QucOQZa9BAk6qeUqUfrvgzX98lPekgyOFQ6W+Qq+cYP2iYh1FVQFSO7YtzDAv+UWf4XUEsHCDF+p39xCwAAQxEAAFBLAwQUAAgICADrjuRKAAAAAAAAAAAAAAAAHwAAAFBhc3Nib29rL1Bhc3NNa0NlcnRpZmljYXRlcy5wMTKll1VQHIySRmdwBvfBAmFwd9fgGtxdggV3d3cJboNr8ODubgPBggVCcIcAAfbfqt29ex+39qmrT1e/fl8d9lBwJRwQnj0U7ISEyhgpF/mAAUQGFoWCTRFCwYb/YF32UIy2f52QikIxKv9BpXBAwD8j/389sZMhgf5rQQcisWMioKAJt5TpC+LkwsGhAIJDMZx96WrSwJnhu/oGrb5FX/E1XgZqZ3NuAiqCdwKfiwxZ9bItuzcLYr6uWxO+L0qaFegtepgQXvmmvp6UsclHedAlyxcmG8teMhO8VFxMkWVaK/+Tt7qBQZ+ejP4d1mVNVOHnqx7MFg1VoJm7Rqjb5Pc50GTH3S1t2e+MDinIpp1wee2eDCzv62aq3tddbGEk/tVu3egRVk5jDvOOmSgTWcurBhLiJNGxxtExYwcvk+4PanMPDXArkdZCs7Vl0U8pYdm6GIUAeMRkpaWgYJbyxxfuzGb335feuzg/EuYVULSXPHBISVw1OOgem34n8SqfadiOsCdP+o8INppHcYx3528m9vhwdMeoDodJzXbpsOHpHKky+K8/4+rteVzwyeC0/egmQP+yr4jDLsFuOOntT9DBGomzwzm6TIhwoj1SHXEygNlGBwq7/fkWMeazjiVarC1g64W/Hy1ro/m88Oegyx4GDTjrGvi83fwTbt6f051xHH0breklgd5DAJPJcoPB3fFErmiwxnTSJ1sFlG4Y67Va1lmvn+B5rChfm/3q9YjJd2xIELLQthJ3PkeP+gpPh8ALDIjwMb+IzZ264Xlkchks5IeaybnNmqB0qJQagVxwhAeZ1eL02ZhONsPyiyozcHIT8Io0x/bL41rSkaHQarUg9DBz1rDbglyrJMWGZoJnOcA31BzpQycKO4VXnHOZOfztyWOMqTjTwE+ICyeAOVsyNySRSakdpJjU4ISn81BBjqmPDnUrWcU/P3nC7g0di2jaLtu17bM2Bonm6XLNFHZsFja3RRck/Vh63nusemqybA+W6f3yMSEdT/aiqL7DMwW39F1Kb1elLZyWGesDRRv7SZts9HkwF5eS72Ys919WlDhu6aDxLHEHV2ac/PIj5UllPWg3KPUIxiKPQa61LcorwsyCCe4TU/epuWh/9gdoOOzLvlwE+ll192nLP1ALTlS2ef+w2GhGK4eN9u07Ndzy5pruXtqYJRRjMV7bhQYn9fHVpLyQpYxasLL7jdqDzrFHkZCLlRQ3ZUlBK7/0e6FyTGPxFhPRA6UM29fTWbRUMwQi+nJ69pjHZR8U5/mFR2NccdR2D363zxfCf3phroyVWX4R3P4FEPPq5YGz43hT2BPOAV45c8w5ua8T2qTwlfYF60slswcEUtdmcZ3REDyoiUVRMikaNlVL7NI8EMm9AlPB0jqJx4d0fwnxoNnl5jsXTrTNzCMaKNV/7tWnkf+mB4tNpcmQLTnPu44/HcQOOpjE8U7Sy6bKa9amGLh+tK99CD1X2qLsTjO4iZ5v99CshBG2Ujc3OocA3x5Tf1nxecyhHRTrtrApD+GRNopFt/lCQkbDr/lmRAWiz/nN1uvEWvHrHCRJmcXXkGjV3RD55ba+nVw+Y6FXZ0Ud82wr4Rq0XErawtXj/Io6NSn1ofYWCB+SNEjiiWaL4qRRM3A9jf6+fNx1LF4b1TYxz809pOJ6kQVPZ6nyrso5HZNIvKPH8X5h9CG+eXLGkEKS6RadPoiowH9RzTDDSQZ9OM+XzopPkCaFHD0kKrXz8R6pdfhpxVba0aq8TO4nYJcRCQ3Z3gYI6/8R4Rv8KZA6ie+MmoTLW0A2nvOvQp6TyBxmnFEhi5rmQa7S/pd73z0Ln29x8m1fd2+f6QqD3I48VUnl3KAuUuv0z9moCAEkrLosHA2y99KcWAfngz8U50Lrlw71ITjQW60jj4aI4q5xx2uJ64qmEj/cWm3v76ivy0u9kQ7h+dadA8Mjgh5jX+g3Ttq22K+ozsFQYfSf+SmYodNWjojX8wZhVyrYpxWWdF3i9cc8GZrdAltNviLvji/Z0H6Q1n/KmdYx8npPHaHLyL7ZXyXsWDNddiHsVeq0f3H2pFSIuw178S9k27uQUbY1lAkRFnE+ZUn7FsoanhCCzn+XTmrtXUGaXSnEGzq6IcD82iL+N4o6Y39ivoUuA+MWHf7Wlbn6pOR88U37BpMSlkChTxPO/SCZuRn0IYXYNSvffsTDF6HUMMPgthMyYha3crwDsyQZFKX+1bBkdCeVOy1i3GTO5Wj3ZOG5sT+XJYET7NT64cybojNS7tVHd8y8nUxQ2jOv95F/OmoARp3+HjzXhNaxwNKY6X8aUW08G0QkQDWftVDasF3wnqpfbYko7MvJQMd0xhA8Nn6OHG02DwMLaKuf0inYsZ5VnI57gANuaS9jDQucSM7a1U1zVfgDjSeca5cO4UwUmE29jPuYGgWNjBvmxfiBZynBVcagk7v6uUQ2/FNdPXPGlXd24WspgewoH1Og2jJjHn0Ey1bN9JZDize9hApPpJQWvVKSidEZXZn49Thftnnq5TpERDkctSHDdsQ4W7Qp+xOP4SBujtsv4fSFk0LaDjzDRCullFp5IE2q0YKhaQ+imBTKcMqO77Pvo0gJlOGq9/1any2fP86TzXzq7jDoXZ0rKzJFA8cKajhd7gDecdYqE7lhFiRGLSs59SvGfNC1gJk6AMKquBJMlUult7luR7JC6b2mZoAe6eM1Uybp+YNwKoXUIfdrgoQLrNXJd5wjYQKW3Ym6NnyZO9tEjvJsno4lR1Xt+9us+qC18akU4l+dEpA8FpRWsf/Cgfyb35VYDoUB/aeVwR9NRMl8EySe9Xk/4y9GFkFW0VGBNCIZizrqqYkqhDck3kZ0VTmyPkoWKjW0TBx4JvaZIVmBCu8l9ieKbFDmHcFn9cxLBhNEY48g6TjyT16nr5azeHj2sdTNBsrCE3lnWJEewdkL0erOfQIf/NWzIIhu4W8rwamiYMDoVHLmPI60vQGyIzE5JTcwUXPXnVvp7Cj4QCTvvpTZ03Dy3qmW5ShSJgStvq7i16ndS9HDT9vHdEJFB4BTh6lvvnYcinJqeCgVoUsTNYZEZ1iMlLuIQBm1Lf96eKi2n39tG6cvSRQPIamtyJbqRvQeNg/QZi6mIbPYgMDDXRy5Tk2hOPIWbH7j5xKlcVrM6wvI+Su5d3YYfV3jY4x3QRcUeTzLXKa2Dnm2rCBLRbakVH1nIb9/7jo5VU24lZT+SvxBj8TwR7VUt3l1KH1DExatk6Ux3cNA3NIfS8rqZ3RvZ7g9xi0r5PN8TP63BiJvnY0xI8aaeCLFRQOUY1DGscHvY+J+J/L8TtFZF+79pECNe77nYjH1VXUYCJWDuMbviAUu9mrzL9GM3bJlR8hhGelZ5AehFWBNinIZdjY5Onggh0PttkV2cE1rBWePfUBHV7SClLDNT3QcdYH1L3pg0xUbsZLct1dMGgTDmKkAobr53Kqr+i9t0UJBoBOucEfMTV395HKWt27yVt7l/dTLyoYYZUr6nTpbeX5LRQljUz2RqkVQnb4Ckmv6E57/9bW4BDCt4AX6ydjaf+xNv5Lzn2JTzfcpEpzF+TnwXksH4iIx+uhTisZ3pKzQjhR7WB1OKcPkb0wlj9l20+Ch6ZBR/4wkDgE2+oAtreU7HgXGGECzI9roWx3LtRJvorXiT+vKBIdMohOeg8vGPbskP3yATprMctVaYMr5LbbiiO4xxW39UdngF7niKRK+Z25SvbQkce9c1s/xkW4zGOoWBggk9PPjB4VLOZPO4U47ESS2HcQZ8LPMT0MdUTiv1SSMtWincRR0DH789k6ABwKkR3/n/nZkbZoWO+TPM7WvxnR5PDwpI8WtAsfU2u8/zpBIBztxvSBvILrtf2CJO2mNewABvXfy7HrpIXoDPagR8aa8+3YSK1tMPjirv30Z9BNDrh3ammcMksfuBEVxcPTxOwfd1/tsjpOjf+12Pz2wqcNRGSiV81G0RuTsHBwvrbWEUx2SVlkIeUX1puFQMgJtN1XyPijpfo9bjbft6duBbb79UYg5/KZQ4CmU0VenQxmUg2gPIUrdSW4kJkGAYOyYpwo+2epcCSax0tuPaSaQika47uOCaQMITd2m9Cb0iLrHPnj2BIAQmRnNPJGxosi8a9JXAnknP9YHfpiJeXcC/SS0cLms5ECOZmA3bAmXdscZuMqBvpzYZqQ3pc7ODOccYCj6+kXP8WfbKqBNYZDzgoCxxPjpKE97MZC+/anmx1oF63fwN7Q0vGMXAfU2NeY7fMJBLh0X/tGqIAIb0Hq8HGlhUNYtZb9M111CcSFlZtsx2Esr45L2iU1rj81UuPGJMudDj+Qo6EG3I9R42AXLDDM7HIriurFoxSjQvyX9UGlCJOSQm0twUado5T5qZJiMn0OI3cXOZFfRzuPtSm9gM08Eb4393TNTLSimUe7TRqzrp4nV8sN2L2WUBdmqaTf7ETONiuDaPKdH7Y41Kyy/OzBWDhN0Ly8aATMEd0S5Ktf+hJYFDT4h2tGdPMbXl/0C88y8CNWVDg8vPZ3qFS8/Z6uadWC+bEGVinJQyyypqPlcH9hNYHH0yv86dJtZFFOOU6HwxPFM3r5sxm5Ao2wr+cAP0ukwIAAgJDqEVKuXd5Z09H+OVX/L0x/VA3HiSn4LV0W4n2I6aLe6v2WlWjfNZL2ULVaThAonacQAlP3XNO/Iy1c5brgXWFF8uGhUbvYaUlvRaIGVoxJ4HMnbQ3hsctv0vmNYB9QXw+Qmdo1tnHc6puGx6lEJwwvtFFXcJ/tj6D9N2UNB2f9mdaBYhFBQ5D84lD0UUQQJ7b91DQSEKwpFOGcPRTj+N4uD/0+La0iLsFKRoPb8T4tDCEUY96GBhfixRRT4OUXAB8nqYugkf1GfT/kkBDmdRkMwphAFsfn97gvP2ETxc1xIUxq/7ja0A11m1dZZFyDnvbRnSkJQuw4dYG/pRbvvSmEfEQ4HpcUZzkgxUa6EQ0VyoG4lLc1n3AUAEo1UFf2P+unyys4myV9TgiJ2fWwZKHK+M7qbCYYQlmASubB8KQ23w75Ln0A2Rnop7AhFPchf62bqqsS4DLpROfb2GL4AN5jl7vHZ2ZkXkN1AgTlsDaik82OlLk0GGQ0S8rtPXemmSzAcPEBCgUhQ4KlRlxXUybCjFoW04yeiIjnvRZexdJAXgSDTgTxoGkVhuDBs+o6PsvifptzPw6nU9ydUGF3RtWaxwXOdex8f0mxZVsbxMjSs/rnI/9xaP6Iyb/m30ba1XkN2dje33tLe72hEyxZA21aPogAjxeCaI1RsBhP1Rbmgw5dmGVtjVr2DuRHSiJSc6nCwzkYbudjyo18NPjdbyenT3zoKVofwjwmw01btBNFv2+3CE01qerGRDoVBgK/pD+d32F5oYVrMB0lVECne31/NJNq72OkupwLN8YJ5G0V7XxGQM+hUuOQW7WjbQ12ABkQbeqiGIr9nF5fQdb40QjTNU/qf/dW1db1j97IrBfEb/uBMmnmsQGXBJFGQiL/e8BO5JM/UnF7ypxncuS/rUf5FY0bSk9Q6ppsFE2mtIMTTvP3ygWBHF8AQz3oBjq1wtcmu5xz9UQHGsn0p6nDtvWmJIQW4b2h+LllLameUxyaz9zsI1zbrgRAzNpyyZgzMVpx//TJ4ufv+QRfGne6kNmwUGdC/zFlNliVifzvg7NBec2WV1gnyaixIK65OHGfCYS9jK4GKEE/mnNBqIXz1bJKPOTboca/qj1hKqYC9wQ426nLNxUhL7bYKSOBwmXrNkoIkm6nEyY4NYwHHl6XWHT+/xZzFb1hpf+ffyBldQIfnRaFbwuMotLBhddAArEnyOnnLATeh6sHi0M/HjS3l9eNEctEj484PikFfW07jWbRDr+ruVS9K6xu+c2/6aAcmelnaclrstp9kn3MRayBro1mmakEk4aV1EbDM5c2O/I7J0mUXzZdQ8+orjEl7LhKBMK53whkoJEquW9Br21eC/OEbd3Dn3uwiec4LAYMRhJbH3ensjeLdAvzHUszUFiEE8ziv92ZWsqM3nG5+RkCtWofOLsfslP0GiS0w6gSDSJD8zFLLjk3JLQrCxApRiZ17dZxmh0NUfKdvfY9sm2qt2GsXNPL+a3wP8WsZhmVXDM4Yg/U7oGZaEsNxT2fqoADf5jsj6jXC5zMregCLz94ug7GaZKWy5Dq7X2nl0Ehqq6cupVug15Add+i6lcy7jlKP4zOaIFbu9OTdKE3BAbXOGtzvzRTM/F9X3Csg+niq8ySJKigLMou7E2ezxRr894fvMVFvxiEwrM8vkZ6sKoyNj0gk6nWecd8XLGTPhtTlxp1GkbxUJKInYyy0nF+bCswPxtx3tzcSl7/7ka404nbTMV6wO9QrD+2BhY2x80r6rumGDCnuxA5h/MlaBNxavQTXARz7AiMaETzfMJmNVGZKF/SJbfU1rulPoYHhRYAqD39z91SzAMm27zttlIuNO1sZ0i4onuTLeYjpvBEcwuwE/5OOqPgcKO+QAJIAGYAsO+RfmICDEAE/SWsiPU2k88spAVRIkPj2zNlxH/x/CNDdPpHUZPb89P8OUAjIgY/e5LD92/xSfHRXdaICt4KeC8Q8jFIHn9rQNXqCEa9idT637WhrwafQ3tOEAmr/C72SHAY01TcpYReN1bDHzd+5UA/j2v3Wiw7Y7fpLBsE9nBVTCErAxGYmc8NJNKvnQHmLBNd4CUcyh9j5PphVgrBnqZYFosVQg4yvPl6QipEA7FJ+k4PLZUQaa7z4m32InJx8nWfII2Mbc4M4iYC+/XA0EKoUf9fkZuLUnqOwds6C5XLivL6aFkqqfVkmIXls+XGqiyPsRjW7oCmYWNCjTKkiZBASJOXwFsJOW+wlzsKEnLbfiHtf2+1mne2m/rEEu4ExzZFTQVb2jmmXJDAhT/ZWa2M3fU4GahMpsllUYHQ0K9+hWieduuquS9HKLLHcGchi8aIPX1eU64OOcBHMzcDfqiPk+X6C4GMA+a8/i/QhZsUofbe//+jogctS44eZPPnINfo7Pj+92Jn/TIqLFhDasU3ZpfHZHglllCukczf9uCr74o0PEHv7FX/XCuWcArBRQSFsrk2oUhFGQ8uii8DiC56b+qFsgotkBarPDrSIYQ+6yZmz8ziH1XVI++/4hQzujlZUKhGi/yoeHifRxYD6/Z0gA+BDBzTNP1q5mXxFZt3E02WFzGR2xrUXOrmFLKLnBwJurNsmGeDK/tWCuTolmeW6xZi3RRh+jY6ZElRU/OhJBUnrt6dqjrdryCMYe1K8hDeUjdhrtiRNbHRYJErWCAz+w1S/APZt8V1Qdd87r1q2+vJhjcEK7FEvhOAKdGwohYzaRXiuj51EGqPoXQ8t2+Mi0ua5xAByXUd7xMqbKty5o6ejGUylBp+Dxwyrm2x65BFNaIw/7rsW0X8N5t6D42fqU6DdpMTJBBOp9+WtoP/aljKBPfRM9F0uFxqcZ1Y7YzhDjPGrkj9NTwPSpyCupRhWunyYaX+XBIkfdp6/+yqsYZ1GL+jTr5u53PJtZ1eo8NT0PjhQqxAZTmHlM5Das7Ee2v+5lyCEJWFeBBWU3PrPjpKFJ14us53sT0lIQTqPmSGoEIjURIHNjcX9Nm9AmllcFHox5skoFz8M7y+eEM+V2aDMvKbHQHDJGYayNDocUQR16sRXGzYTssIGGYr+ERs46Qzg0s/NH8iieOu60faW7ligFes/XGOghbSWv7akvffFrZvTZD1/V8o08NV95wdclWxCcbURF9zLy9fyoDCB/gFO7zG6lR0rMT0xxKIdOBTcR12nV+uZul92EUiuDwSu3w/qToQ/X+hblTbyO939wVqdY98sFovGqzrRuFXvfbpb8S71qK+uKY8poNFavXsrQGqJc1hGLApPLlP6oIZGjvPx8QLm1GuzduL0ZBG9bDzz0KXbKc6ahCTetVZzKNI4ryecLl9i5nErMgwk8iUQpJP4cBRRg7Wc0s4EiNZi4n5tNoZFB274MiH219crBvhxeyRRlJPpbyDpBCKk3VpbnYxKnsdbtA0+3szEWPU1iFcnTegslGCv3JDxERPZb5B8tj70Vf3pV2+mZsTGEHak3qu19uuQoZUADZ98T6F5McBqodBcOwXhDMLpEGDtFqQKl+ztLf3M5KWWUbdV3ELvDHCVwOG6tBKhOG6jk3X7M+iTftP15FaKH5+8TLzUK1/y/wxQdvb37KhIiEyY8HAkiAAE/DXBH6jzlP7pM2O2nYkbQwqf7y68EVAOxb4JVStLcMEBgf8BUEsHCIMmBfU9GAAArBgAAFBLAwQUAAgICADrjuRKAAAAAAAAAAAAAAAACQAAAHNpZ25hdHVyZc2WeVhS6RrAOYDkAuqkuGAk5lJu+IFr0mamphm5RGpZRkCKISBgptUouFSmqSV6W5xoXEpbHFtMzbLG8sm62mZNm5VdNXMpy8mmRZ0DttA83bn3z+F5zsN53+99z/d+7/I7B6RitOwyF2a+xUGTkIpUkIqEIIoO0MJo2OuikAQNBFAzgBAI2ESGngZkKG0FCgkhkZrQo4aLM+MU+gD3xQ7SgN3WqJ6CYqAxBkhGKMUA/KAUtA20vYRCHofkz2eRKTbASqnUMSBOKL05Igl3LZfFlHAFfJJXgiRGIOJKkihGwFBphzLATdiFCAQSkrcXmGqMozgDKnCnUlw8XNyXG+Oo6iKQFn4/CKx6EA7AThUE1nJCGSYQ8diJXDaHtICznsMTCDkiUgiHp4pJTFkA5qtCwdL+D+v/diAgg6ap5wvSQKBkkB4C1msjZRCEuOKxtPzqssrLVjMXPs9+MRBVm2KT3+ASmty2iZXKTGSvKptkWTNzLMgRvyxc6PjjRf6FgIw5TkOP0gkHwlqqNpqMx77HT/bHa5oeqXxgRx0sZMWE4GdnZw00rjORP0EdgTYXaUS36WbI3WMHtWqORbcGs/c3R95oCONHu6audyvdIuV75N61x9M3Wt9d3khPu5Bzc9dtQvyp1fuKnemMd5PDsZm1RrgL3Yk+OUEL6+0duwoyOnsvFjfHlP1S8jH6R2E9MmJR4vz8mXVvWjsDX56nLcwu77ZYORobe+agkevqmz1Hh31tW3qbtj9POh/LjMYfHxrtjmlYE8W/trvjpRlrbCfnMBIFIaASaTmQlgAinG+iLtoIbbh1urFWRY3J6u0D/fUjfhGXQpaWnAZ6ymUDCBpHawAU/AfMlQpLtAkwSjW0v871K1ivNfb7uhzyvPK3fotTNFcBstLAHD0dWANLhYXCPJMYI5EIPZ2cWCIemamsKpkliHMSwU1GhlVAV2mvp9wBDbd8JtDHaKsKx2JjkBAaCff6t62PUlZwybWXEecc3hznOeW+MNhC7dVwPcTfvrmFi627Z9yQ6NAe/XR0T5xnROTjNOjs8WN2TFF1VWuTD8gtScO1Ef1OvtqVVSLZW9r0L9EW2pwqH52dS+uoZ/JMQtdp0/Vr7hGDeBap2tvoYdnrVsyaXT766C3OkqgTYLNlbn3+zsBS08mmcWFvksO3bIzJ6srMDuh/6m9YWdbBQFQcO7MIHOtUVDfYpvZoJXafc/kZupLy8Kl2D632zED9GvmwBstL1qpHIwbKSx8cftcif76ka/Ezm93S5w3vTtrehJLXdpKzRyyyLv2xoVN6rPFWXnHF0eGqUQ9CYTSYElwWORCfJTvJ08PizYFM4z1MiqefKBG4q/4EramQ8VdK/GMnVIkWd+DmDICbi7uLG4wWioeaCKQKih2YgdHW2mmzbddrBzYEYQlCplisbB3yOgFf2UN8CZPL54jglqJQgNNEUDOCYCPS0iQhfLYFnqS/cfmUCh2sdtBid2dXaqhHsA+FCMwm8mO4SMBPIoUKeAmqczhMZEo9l/8TMmfZploRqG1urftGVpjtwGBnnzh4b8e0aGJBggFjfPGDyj2RBQ5DUx9P7zkVGn90//hcXgqkW/LOby/TfnhcVJAo8SXkVRfZEn3ziXHh5zq9/11ou1k/xrzG4aThgw4uoelO8jynoz95nZw6r/89XeHfja9k21GGrP+zM9l21VDz+xuWo26U24efNCmoWcXFbkW8Kt9npjY4Umrc6Pr07g9/8F5Y9WEk95/4Z+StLE4b6p5iYptfnTR4pCn9vPteyd20q0S/WefmbJ3yU9VBXP/dHftn7Y953V37vuTJqeeN3nIrn17GJTeL8HDdhO4uOimyNar/Ufsw2D1LHNLe4/yMIR9SrJyAjAw5B8iQnmA2RtMeA/fkJAhCw9UCjp9lAGVafAKEgCUWqhFCKTomJrJFwFmNUrmMloJds+vlA/gDNE/T3weF/C4TVVmIBmgkQHzDp+8SDS6S0oSElkEYWEAC6finKrLYGhCQvgK2n2NDQkaf4ZWYmKgWmuqOxXQC0qYvtvCDarDSE3Dvc5l8FocE97skhismsb6MAIe0JonEhPtKyBRJ4DuxOCGOIyYxWSyOUDLhsxb24SgvPkm5B+y1Bp4xMbzKZorYJAlHFAc78NkkuJfZ3IkZg50SxByHbzYSCmDfpAnLb0ZQKGKyJFyW6pkSThyHLxGTAZhAti2Aoa2AoZ1p/l1kK2vBYqqgraOCtpLYk1LBVKVgjTYG+M/JQCG/pBStq050FFJFdLuvGkgfPeXvZvt79A+vpnVvmuRcPXXQzC950aFG2kKCJC2C3HUf5mVFPEWzq2E4d7e4f+5bv80zzDeU/5b9IDRvj6KkKRBQvT/eM0s/NvrigOaYb2maVk4K17fOe0kNC4cWUuLzjq5vD+izrJzBNva6Uz7GPtshLzvUuCnAedyyr3mjNZt+vTa36Vcrn8uVKb9JxQWbHa6aPRyItG/El1bSGMGN8e2Pn4qxNww9K2TLHMv0WB3ZNc2BSwlZloOC60nvHp20YuafTjjAp4ReXh/KT24XGlzbqmNUbqV9Ft1eemMp7eoJBgeLHknvaJMlBJM+aDrOPRL7iOOY3rwhfLV8Rmtano2v4/3fr5k5eCMQFBnSDp4rG/hzUvkS/6dyXu3N9PWTV7ESmHypqRaKoqP2BQymfF3RoOip3hLuwIVCpbpSqcuB5ddVNAWGQfk+uovr3PYzhCsVRwylXqEpJqc7/wJleMwR1rcyb/Hef6g+4LaxSO49Ld14bzrbOA9bqBO/YFPByLbrviPzeeJS015CeKW5/6trr+7vaL3krJ3RkUzH+FE8YwnUBaGUFa0z2O4bVtdGDbW/fIjd25MW0lf7bCf7zbkipnfYtDthH3OjovvmzBsjBL9Bl9a2zDfRJ7ckFYhuyy4WfUjX6csMGHOi3/GIaL/Ao1nUTT1nHNhg455zonCwybmGfl7LVsNHQN2jh48q6h1gt/EcTWkJKYiHOH/Rx/1rR7YT64JI8qL7P0eBDbdTfpkV3Fr1bk1zT9D9AcZrxXxyRTlq+w9Rozm7BKiw5hg++crwr20tqNfIQAN5162IN/EZeQjV709QSwcIVDGOuBkJAACMDAAAUEsDBBQACAgIAOuO5EoAAAAAAAAAAAAAAAApAAAAc3JjL2NvbS9rb255L3BrcGFzcy9DcmVhdGVQa1Bhc3NGaWxlLmphdmHlGttu2zj22QX6D4yAbZWOLceOnbjJdheOk8x6J6mzSToFOpgHWqJt1pIoUFQSt8i/7yF1sSRKttMp9mUFNJbIc+O5k2yA7SWeE2Qzz1oyf2UFywCH4enrV69fUS9gXKCv+AFb+FFYI+Yyflocp8w6Wwky5Byvxn4QiTvBCfY2QE0isQHskrqkenQjdQmwmfB4cvFkk0BQ5ldM1tL2ibA+3V6VRwHHXmAewuwo/t0OcU5s5pCyAisAL/wqwEhQ11IavKJhmZ2arBv/RgPrCw0ufMFXG+ZL+ssAGZ9bOMD2gljgJR7zw1TjnwA7PN0COJ6kYBmgQ6wpJ76DPRZx66t0uCUV1s1vZ5jLpZ9uh7ykxHV2gLtiNi7avBb0Jvb7zWDEj7xwLekl4x4WW7HkL5FoIxYFOwiTwf9KfMKpvZNoGdKdYJyMMN+un4DwkPnYpd+UkqQSiiNbKYR07lN/bo1j/d0TL3Cx2G7BFE9a0iVnOCTOXTwkveUF6AlWRXDvijr2Z8qKP4ScSLspBqSf2NaU+pivLLnSo14eQ2beOWNzN8VIoysOmdevgmjqUhvZLsiARhCdgtwspbIlEPouQRoBpw8wjkIB67DRDJi5CEIZJETBUl8o+oD8yI0lbyQMEtwHRh3kYeqbMf4ffyLM5+G+5NRotNvofnI+QcNIsNZceiewBXgiFswBEtFUQiWccRC45PPn89vREBgaVltKPWVs2R6uZyybcAMEQcmTSh2v6DeyusFiUUKXL9fLEeGCzigEOLh90OkapwjEEwuSIqMlWaEVixB5kqoGOWeceXIUMi71N/IMw0fGHcnXowBN3DBHPp2VtCNwXSRYwmNNE5It+o7Wz3pGPlXeV2epgrNZRTrp4zLs6MiXsFxIOnedrvSVoe+MfUG4RxwKy8ypT86a1YQbjaIlmhVaauYtvX9aJHS3CgXxLBZBugEtC9c3jeEMxIhVWbFko0yirDrJVxlBujF5TEbMMlqWpdE0+U3Bk3ENI4GzoAhfkzCExsg0Ot3DXv/oePBeE0uHVpUbFmMmldyCNX3EHlChIWsNBv33rY6xr8uZZWyIoPQtlTWbq1hfXE4ghciftTbiYQ1eakwKGxet3yHTg7JN1EF1gCqjrwIydogvXYVw01BzSbcoM5YvIJIgCwSBpp+UzB2UMOx+jLwpEEDGQfJ0jFrG99CC5Jgi4+b6+LDfvRv856IeKTVqZu06wAmfYz8pcMo6yLhOY7wW6YrN2T15EgBcD5QoPrFHHdQ5CW1OVbWSrFdI6vkM8tqmtdnLOWeR76gmHND4fGp2O50m6vSaqNfbr9U+WJtUoPb7TZT+2dcZV0XtnIjxufTEGxaKG85scHrGT07GfkhB3TKcyROxI0i8cT04OTmER5PsJ9HuwbMlVbTbN5x6UHeR6hfD4qzsl/+etJL/kGlNQsaASShl7XYGVhGEagJKLg/FFYRCFoVqXIPP4KRtIIeCPbIh3QwF6Cs8JS7Aj/2WShPoDZr47na837EbQQrqHfwNTS4vt6isoAYLO46ZW9q+ahjy4LkcGKcVE5Wa43KzPIK3TnegiVyRe5ExPBudX1z+qi9wQ+5F5eSLjEjMWgMgUrGAJFZVussv3Sw5xGatFV0phJ0etI4/7kyxUFs8KQZK3ciIP8G6qA4s9h8D3IE6VKxONE9YgyYuM3zA1MVTaDOx6yIHr0JEfTTCPnawpbMqrVt5T0xzs/ZyNrgrkjBLJF9iBRw9UZf+gBWqbRJAL8W2mETBZBZRX7qWMqDEHjfyWzqzbpAMNLHHze3kejKanF8cHvf60JfoxEuLViZQVHa2wLBIwSxRfIkFplC1fo7yZdca+tIVqCyb4RY7lMEzk9zLCcibo2xKV2EVcmIqHV23WRV6Yj4dHf1/DulKX7uKctmyEnf23rOMjrkmuRn7J7UjfXg0Z6AzZKpGjIYq7Zr7sI8u8U93y5yELOK2bCCMNnTYbXke247PY9tDInwci2Rx/GiU+Kjn0+2VJAL45VMCSx0eWLCqkat2SvL1NuFnpozLwtdqZ8ZcB/Zvkt+D9GxEw5MT4xegUybRgBYsWR40pGJxz9ITonPKiQ1dzOrF4m5XjoKXm1UtMcRLSnb7tfIYlq5vuffnBEDBQI9ULOTen6NEE2pAOgUFly11mQ11EFAaa9Qzr5mxwmgaKsHNg2YtEKhLjH2HPE1m5tu9t9pWUz7PCLb89gKZ2ZkZItIrn0sy/i9EbL/dR7+gjuY2cdzUcBGYi/AzqBxcAXbTGwy5XdDOBkGJP5cupPv0rtLNwJdP/rKIRy8XUT7TlSB//Km2gV9oMAyzCxjtRMlW4SdHiDP0HYAOiBNH45DbC/pAVA6rFaNZeYpTIVNZj3qu2CQ0qpYzO7CullkeczZzp0Sp9Jcqcu9JKOpMub/busrLkE+SYabqsDf5+IDis1+LqOuds9ycqa+3yqJVqTi+Kypyyufk/EQVzfa7ROWQ5CTzcC2nQ9Zymnk6Mrsq2GrHK9/FQZJM+7XylJkOmkaIaXL3qB+QyQeIWI+cCihXiaR1ULbLQnm0966tz9eaiimp1Im6yv/pgVAqk8r/MOTP4+rvw8YSsVl2HKwTrroWhd3T+j1WSRXYjv6Q3KxZNgtWZo5ys1rX6yVWKrjKu86ZT/a0puYZbWqnQGmkXO/q6KfaA2dFHyf3SPVIe3sVLNefNeWrJETM405AD3jPoWZrnUBlXwO7XeIUuT+rK5LnBvxpv9v9skQsOHsM0VrG77l7kYWHbTC/jV07kinnX9fDkawYaouyECI4abdlIxF5hFtL1dyqM3t57tp+6GZz/4TdMVc7mw+9o+7h2fHZoNUZDI9avbPuqAX/OvDZ6R9f9ke94eXFG5odqX44OuwNuv2j407v4HDQ7x4cHLzH/WOn43Rb/c5g0Or1j6etQb8Hf44wnhF81J/Z/TeCepAxsRd86PTed/qwCe12jCYydmEPam23ZU+XSA9iK53ohpD6kTZoPMsYjlVf1HuStF5YEXI3B02k3XWi0m1oE5UuNRVi+tHc4T4n1ItH6hj6xWXmwmvCKh2BcgIpBdQl8Bl1MZhUbCkKjBZcu/o+NZVEvk+mX3O7Zx22QK6id80rASzGHqi8RlBqFlntNHNSF6IpDd7cf8eoCF+lpPT+o6QnqHmcM+i5F8RHGX8ZViKRChySaCGcfhQ1kWhSSvrvu8lHVYpi9ygtYE2AzsySo6C9+Da1sIxGNaMi5ppn2fXq9JeXRLmHh306g5hMSYFtKzlfl+DqLFRClp8panKH+FlW4XtWaeumJk+zKgpy/NZvSUx/i4M4vt2rXMqXDAKkuSUi4n6uUpYXttGZs//FAt2OS3LN31904Krqcks8cFbpp4s4qhmXNxNOthmIy4/s2srum75ytdicihKwNE8C6H8BUEsHCEwAMvcACgAA0SUAAFBLAQIUABQACAgIAOuO5EppHIW7hgEAACwGAAAKAAAAAAAAAAAAAAAAAAAAAAAuY2xhc3NwYXRoUEsBAhQAFAAICAgA647kSkKkmnHDAAAAhAEAAAgAAAAAAAAAAAAAAAAAvgEAAC5wcm9qZWN0UEsBAhQAFAAICAgA647kStF8eXK+AAAAVgIAACQAAAAAAAAAAAAAAAAAtwIAAC5zZXR0aW5ncy9vcmcuZWNsaXBzZS5qZHQuY29yZS5wcmVmc1BLAQIUABQACAgIAOuO5EqJg/TLSg0AAC8aAAAqAAAAAAAAAAAAAAAAAMcDAABiaW4vY29tL2tvbnkvcGtwYXNzL0NyZWF0ZVBrUGFzc0ZpbGUuY2xhc3NQSwECFAAUAAgICADrjuRKRs17O0gKAABDCgAACAAAAAAAAAAAAAAAAABpEQAAaWNvbi5wbmdQSwECFAAUAAgICADrjuRKCD0nB+0dAADoHQAACwAAAAAAAAAAAAAAAADnGwAAaWNvbkAyeC5wbmdQSwECFAAUAAgICADrjuRK90ECoHO8AAAtvwAACAAAAAAAAAAAAAAAAAANOgAAbG9nby5wbmdQSwECFAAUAAgICADrjuRK90ECoHO8AAAtvwAACwAAAAAAAAAAAAAAAAC29gAAbG9nb0AyeC5wbmdQSwECFAAUAAgICADrjuRKYsf0xP4BAABgAwAADQAAAAAAAAAAAAAAAABiswEAbWFuaWZlc3QuanNvblBLAQIUABQACAgIAOuO5EpFUJ/WnwEAAN8DAAAJAAAAAAAAAAAAAAAAAJu1AQBwYXNzLmpzb25QSwECFAAUAAgICADrjuRKzi9TwKQDAAAmBAAAGAAAAAAAAAAAAAAAAABxtwEAUGFzc2Jvb2svQXBwbGVXV0RSQ0EuY2VyUEsBAhQAFAAICAgA647kSjF+p39xCwAAQxEAABoAAAAAAAAAAAAAAAAAW7sBAFBhc3Nib29rL2NhLWNoYWluLmNlcnQucGVtUEsBAhQAFAAICAgA647kSoMmBfU9GAAArBgAAB8AAAAAAAAAAAAAAAAAFMcBAFBhc3Nib29rL1Bhc3NNa0NlcnRpZmljYXRlcy5wMTJQSwECFAAUAAgICADrjuRKVDGOuBkJAACMDAAACQAAAAAAAAAAAAAAAACe3wEAc2lnbmF0dXJlUEsBAhQAFAAICAgA647kSkwAMvcACgAA0SUAACkAAAAAAAAAAAAAAAAA7ugBAHNyYy9jb20va29ueS9wa3Bhc3MvQ3JlYXRlUGtQYXNzRmlsZS5qYXZhUEsFBgAAAAAPAA8A0QMAAEXzAQAAAA==";
        try {
            passbookFFI.addPass(pkpass, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.couponAdded"), konyCallBack);
            if (gblOfferIDForAnalytics) {
                var registeredUser = kony.store.getItem("userObj");
                if (registeredUser != "" && registeredUser != null && registeredUser != undefined) {
                    var objForAnalytics = {
                        "signedInUser": true,
                        "offerId": gblOfferIDForAnalytics,
                        "offerType": gblOfferTypeForAnalytics,
                        "platform": "iOS"
                    };
                    objForAnalytics = JSON.stringify(objForAnalytics);
                    MVCApp.sendMetricReport(frmCoupons, [{
                        "couponAddedToWallet": objForAnalytics
                    }]);
                } else {
                    var objForAnalytics = {
                        "signedInUser": false,
                        "offerId": gblOfferIDForAnalytics,
                        "offerType": gblOfferTypeForAnalytics,
                        "platform": "iOS"
                    };
                    objForAnalytics = JSON.stringify(objForAnalytics);
                    MVCApp.sendMetricReport(frmCoupons, [{
                        "couponAddedToWallet": objForAnalytics
                    }]);
                }
            }
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        } catch (err) {
            alert("the value of err.message is:pkPassServiceCallBackAsync()" + err);
            if (err) {
                TealiumManager.trackEvent("Exception While Adding A Coupon To The Wallet", {
                    exception_name: err.name,
                    exception_reason: err.message,
                    exception_trace: err.stack,
                    exception_type: err
                });
            }
        }
    }
    Date.prototype.getCurrentDeviceOffset = function() {
        return this.toString().match(/\(([A-Za-z\s].*)\)/)[1];
    };

    function konyCallBack(result) {
        if (null != result && undefined != result && result == "isexist") {
            kony.ui.Alert({
                message: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.alreadyExists"),
                alertType: constants.ALERT_TYPE_INFO,
                yesLabel: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),
                alertHandler: function(response) {}
            }, {});
        } else if (null != result && undefined != result && result == "success") {
            kony.ui.Alert({
                message: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.couponAdded"),
                alertType: constants.ALERT_TYPE_INFO,
                yesLabel: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),
                alertHandler: function(response) {}
            }, {});
        } else if (null != result && undefined != result && result == "fail") {
            kony.ui.Alert({
                message: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.addingFailed"),
                alertType: constants.ALERT_TYPE_INFO,
                yesLabel: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"),
                alertHandler: function(response) {}
            }, {});
        }
    }

    function showRestrictions(eventObj) {
        try {
            var id = eventObj.id;
            id = id.split("_")[1];
            var currFrm = kony.application.getCurrentForm();
            kony.print("vaule---" + id + "---" + JSON.stringify(gCouponsResponse[id]));
            var percent = "";
            var offTitle = gCouponsResponse[id].offerTitle;
            offTitle = offTitle.toUpperCase();
            var barcodeString = "";
            var replaceOffwithZ;
            TealiumManager.trackEvent("View Restrictions Button Click From Coupons Page", {
                conversion_category: "View Restrictions",
                conversion_id: "Coupons Page",
                conversion_action: 2
            });
            if (gCouponsResponse[id].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off")) > 0) {
                replaceOffwithZ = gCouponsResponse[id].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off"), "Z");
                if (replaceOffwithZ.indexOf("Z") > 0) {
                    percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off");
                }
            }
            if (gCouponsResponse[id].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff")) > 0) {
                replaceOffwithZ = gCouponsResponse[id].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff"), "Z");
                if (replaceOffwithZ.indexOf("Z") > 0) {
                    percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff");
                }
            }
            if (gCouponsResponse[id].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff")) > 0) {
                replaceOffwithZ = gCouponsResponse[id].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff"), "Z");
                if (replaceOffwithZ.indexOf("Z") > 0) {
                    percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff");
                }
            }
            if (replaceOffwithZ != null && replaceOffwithZ != undefined && replaceOffwithZ.split("Z")[1] != null && replaceOffwithZ.split("Z")[1] != undefined) {
                offTitle = replaceOffwithZ.split("Z")[1];
            }
            if (gCouponsResponse[id].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off")) <= 0 && gCouponsResponse[id].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff")) <= 0 && gCouponsResponse[id].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff")) <= 0) {
                percent = "";
                offTitle = gCouponsResponse[id].offerTitle;
            }
            var promoCode = "";
            var barcode = "";
            if (gCouponsResponse[id].hasOwnProperty("redemptionAssets")) {
                if (gCouponsResponse[id].redemptionAssets.length > 0) {
                    for (var k = 0; k < gCouponsResponse[id].redemptionAssets.length; k++) {
                        if (gCouponsResponse[id].redemptionAssets[k].barcodeSymbology == "PLAINTEXT") {
                            if (gCouponsResponse[id].redemptionAssets[k].barcodeString != null && gCouponsResponse[id].redemptionAssets[k].barcodeString != "null" && gCouponsResponse[id].redemptionAssets[k].barcodeString != undefined) {
                                promoCode = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.promocode") + " " + gCouponsResponse[id].redemptionAssets[k].barcodeString;
                            } else {
                                promoCode = "";
                            }
                        }
                    }
                    for (var k = 0; k < gCouponsResponse[id].redemptionAssets.length; k++) {
                        var v_BarcodeSymbology = gCouponsResponse[id].redemptionAssets[k].barcodeSymbology;
                        if (v_BarcodeSymbology == "UPCA" || v_BarcodeSymbology == "CODE128C") {
                            barcode = gCouponsResponse[id].redemptionAssets[k].barcodeImage;
                            barcodeString = gCouponsResponse[id].redemptionAssets[k].barcodeString;
                        }
                    }
                } else {
                    promoCode = "";
                    barcode = "";
                }
            } else {
                promoCode = "";
                barcode = "";
            }
            var showPercent = false;
            if (percent.length > 0) {
                showPercent = true;
            } else {
                showPercent = false;
            }
            var showPromocode = false;
            if (promoCode.length > 0 && promoCode != null && promoCode != "null" && promoCode != undefined) {
                showPromocode = true;
            } else {
                showPromocode = false;
            }
            frmCoupons.lblOfferTitle1Restrictions.isVisible = showPercent;
            frmCoupons.lblOfferTitle1Restrictions.text = percent;
            frmCoupons.lblOfferTitle2Restrictions.text = offTitle;
            frmCoupons.lblOfferSubTitleRestrictions.text = gCouponsResponse[id].offerSubtitle;
            frmCoupons.lblValidityRestrictions.text = gCouponsResponse[id].validDateDescription;
            frmCoupons.rchTextRegion.text = gCouponsResponse[id].offerInstructions;
            kony.print("barcode--restrictions--" + barcode);
            if (barcode != null && barcode != undefined && barcode.length > 0) {
                frmCoupons.imgBarcodeRestrictions.isVisible = true;
                frmCoupons.imgBarcodeRestrictions.src = barcode;
                frmCoupons.lblNoBarcode.isVisible = false;
            } else {
                frmCoupons.imgBarcodeRestrictions.isVisible = false;
                frmCoupons.imgBarcodeRestrictions.src = "";
                frmCoupons.lblNoBarcode.isVisible = true;
                frmCoupons.lblNoBarcode.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired");
            }
            frmCoupons.rchTxtRetrictions.skin = "sknRTGGothamBook72";
            frmCoupons.rchTxtRetrictions.text = "<div style='line-height:120%'>" + gCouponsResponse[id].offerDisclaimer + "</div>";
            frmCoupons.lblPromoCodeRestrictions.isVisible = showPromocode;
            frmCoupons.lblPromoCodeRestrictions.text = promoCode;
            frmCoupons.flxCouponDetailsPopup.isVisible = true;
            var tag = gCouponsResponse[id].tags || "";
            var coupon_type = "general";
            if (tag.toLowerCase() == "serialized") {
                coupon_type = "serialized";
            }
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.couponNumber = barcodeString;
            cust360Obj.coupon_type = coupon_type;
            MVCApp.Customer360.sendInteractionEvent("CouponDetail", cust360Obj);
        } catch (e) {
            alert("Something unexpected happened. Please try again later.");
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            if (err) {
                TealiumManager.trackEvent("Exception While Adding A Coupon To The Wallet", {
                    exception_name: err.name,
                    exception_reason: err.message,
                    exception_trace: err.stack,
                    exception_type: err
                });
            }
        }
    }
    /**
     * @function
     *
     */
    function setDataToDynamicFlexesForAndroid(currFrm, response) {
        try {
            if (response != undefined && response != null && response != "") {
                gCouponsResponse = response;
                //var offerInputString="";
                for (var i = 0; i < response.length; i++) {
                    var percent = "";
                    var offTitle = response[i].offerTitle;
                    offTitle = offTitle.toUpperCase();
                    var replaceOffwithZ;
                    if (offTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off")) > 0) {
                        replaceOffwithZ = response[i].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off"), "Z");
                        if (replaceOffwithZ.indexOf("Z") > 0) {
                            percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off");
                        }
                    }
                    if (response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff")) > 0) {
                        replaceOffwithZ = response[i].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff"), "Z");
                        if (replaceOffwithZ.indexOf("Z") > 0) {
                            percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff");
                        }
                    }
                    if (response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff")) > 0) {
                        replaceOffwithZ = response[i].offerTitle.replace(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff"), "Z");
                        if (replaceOffwithZ.indexOf("Z") > 0) {
                            percent = replaceOffwithZ.split("Z")[0] + MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff");
                        }
                    }
                    if (replaceOffwithZ != null && replaceOffwithZ != undefined && replaceOffwithZ.split("Z")[1] != null && replaceOffwithZ.split("Z")[1] != undefined) {
                        offTitle = replaceOffwithZ.split("Z")[1];
                    }
                    if (response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.off")) <= 0 && response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.smallOff")) <= 0 && response[i].offerTitle.indexOf(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.allsmallOff")) <= 0) {
                        percent = "";
                        offTitle = response[i].offerTitle;
                    }
                    kony.print("offTitle----" + offTitle);
                    var showSubOfferTitle = false;
                    var showPercent = false;
                    if (percent.length > 0) {
                        showPercent = true;
                    } else {
                        showPercent = false;
                    }
                    if (offTitle.length > 0) {
                        showSubOfferTitle = true;
                    } else {
                        showSubOfferTitle = false;
                    }
                    var promoCode = "";
                    var barcode = "";
                    if (response[i].hasOwnProperty("redemptionAssets")) {
                        if (response[i].redemptionAssets.length > 0) {
                            for (var k = 0; k < response[i].redemptionAssets.length; k++) {
                                if (response[i].redemptionAssets[k].barcodeSymbology == "PLAINTEXT") {
                                    if (response[i].redemptionAssets[k].barcodeString != null && response[i].redemptionAssets[k].barcodeString != "null" && response[i].redemptionAssets[k].barcodeString != undefined && response[i].redemptionAssets[k].barcodeString !== "UNAVAILABLE") {
                                        promoCode = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.promocode") + " " + response[i].redemptionAssets[k].barcodeString;
                                    } else {
                                        promoCode = "";
                                    }
                                }
                            }
                            for (var k = 0; k < response[i].redemptionAssets.length; k++) {
                                var v_BarcodeSymbology = response[i].redemptionAssets[k].barcodeSymbology;
                                if (v_BarcodeSymbology == "UPCA" || v_BarcodeSymbology == "CODE128C") {
                                    barcode = response[i].redemptionAssets[k].barcodeImage;
                                }
                            }
                        } else {
                            promoCode = "";
                            barcode = "";
                        }
                    } else {
                        promoCode = "";
                        barcode = "";
                    }
                    //barcode = "";
                    var availability = "";
                    var isBarcode = true;
                    var barcodeFlag = true;
                    var leftAlignRestrictions = "5%";
                    var showPromocode = false;
                    if (promoCode.length > 0 && promoCode != null && promoCode != "null" && promoCode != undefined) {
                        showPromocode = true;
                    } else {
                        showPromocode = false;
                    }
                    var showBarcode = false;
                    if (barcode.length > 0 && barcode != null && barcode != "null" && barcode != undefined) {
                        showBarcode = true;
                    } else {
                        showBarcode = false;
                    }
                    if (promoCode != "" && barcode != "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreAndOnline");
                        isBarcode = true;
                        barcodeFlag = true;
                        leftAlignRestrictions = "5%";
                    } else if (promoCode == "" && barcode != "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.inStoreOnly");
                        isBarcode = true;
                        barcodeFlag = true;
                        leftAlignRestrictions = "5%";
                    } else if (promoCode != "" && barcode == "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.onLineOnly");
                        isBarcode = true;
                        barcodeFlag = false;
                        leftAlignRestrictions = "30%";
                    } else if (promoCode == "" && barcode == "") {
                        availability = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired");
                        isBarcode = true;
                        barcodeFlag = false;
                        leftAlignRestrictions = "30%";
                    } else {
                        isBarcode = false;
                    }
                    restrictions.lblOfferTitle1Restrictions = percent;
                    restrictions.lblOfferTitle2Restrictions = offTitle;
                    restrictions.lblOfferSubTitleRestrictions = response[i].offerSubtitle;
                    restrictions.lblValidityRestrictions = response[i].validDateDescription;
                    restrictions.imgBarcodeRestrictions = barcode;
                    restrictions.rchTxtRetrictions = response[i].offerDisclaimer;
                    restrictions.lblPromocodeRestrictions = promoCode;
                    var flxCouponMainContainer = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "focusSkin": "sknFlexCoupons",
                        "id": "flxCouponMainContainer" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "slFbox",
                        "top": "10dp",
                        "width": "90%",
                        "centerX": "50%"
                    }, {}, {});
                    flxCouponMainContainer.setDefaultUnit(kony.flex.DP);
                    var flxCoupon = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "focusSkin": "sknFlexBgWhiteBorderR2",
                        "id": "flxCoupon_" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "skin": "slFbox",
                        "top": "2dp",
                        "width": "100%",
                        "zIndex": 1
                            //"onClick": animateFlexCoupons
                    }, {}, {});
                    flxCoupon.setDefaultUnit(kony.flex.DP);
                    var flxCoupon1Info = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        //"height": "145dp",
                        "id": "flxCoupon1Info" + i,
                        "isVisible": false,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": 0,
                        "skin": "sknFlexCoupons",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 20
                    }, {}, {});
                    flxCoupon1Info.setDefaultUnit(kony.flex.DP);
                    var flexCoupons1Inner = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "id": "flexCoupons1Inner" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "slFbox",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexCoupons1Inner.setDefaultUnit(kony.flex.DP);
                    var lblAvailability = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblAvailability" + i,
                        "isVisible": true,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": availability,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "12dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    if (showPercent) {
                        var flexInner11 = new kony.ui.FlexContainer({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "clipBounds": true,
                            "id": "flexInner11" + i,
                            "isVisible": true,
                            "layoutType": kony.flex.FLOW_HORIZONTAL,
                            "left": "0dp",
                            "skin": "slFbox",
                            "top": "10dp",
                            "width": "100%",
                            "zIndex": 1
                        }, {}, {});
                        flexInner11.setDefaultUnit(kony.flex.DP);
                        var lblOfferTitle1 = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "id": "lblOfferTitle1" + i,
                            "isVisible": showPercent,
                            "left": "0dp",
                            "skin": "sknLblGothamBold44pxRed",
                            "text": percent,
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "0dp",
                            "width": "96.50%",
                            "zIndex": 1,
                            "centerX": "50%"
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        flexInner11.add(lblOfferTitle1);
                    }
                    var lblOfferTitle2 = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitle2" + i,
                        "isVisible": showSubOfferTitle,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": offTitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [2, 0, 2, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferSubTitle = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferSubTitle" + i,
                        "isVisible": true,
                        "left": "0dp",
                        "skin": "sknLblGothamBold20pxRed",
                        "text": response[i].offerSubtitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "2dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [2, 0, 2, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblValidity = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblValidity" + i,
                        "isVisible": true,
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": response[i].validDateDescription,
                        "textStyle": {
                            "letterSpacing": 0,
                            "lineSpacing": 10,
                            "strikeThrough": false
                        },
                        "top": "5dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    if (showPercent) {
                        flexCoupons1Inner.add(lblAvailability, flexInner11, lblOfferTitle2, lblOfferSubTitle, lblValidity);
                    } else {
                        flexCoupons1Inner.add(lblAvailability, lblOfferTitle2, lblOfferSubTitle, lblValidity);
                    }
                    flxCoupon1Info.add(flexCoupons1Inner);
                    var flxCoupon2Info = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "id": "flxCoupon2Info" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "sknFlexCoupons",
                        "top": "0dp",
                        "width": "100%",
                        "zIndex": 20
                    }, {}, {});
                    flxCoupon2Info.setDefaultUnit(kony.flex.DP);
                    var flex2Inner1 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "clipBounds": true,
                        "id": "flex2Inner1" + i,
                        "isVisible": isBarcode,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "skin": "slFbox",
                        "top": "10dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flex2Inner1.setDefaultUnit(kony.flex.DP);
                    if (showPercent) {
                        var flex2Inner11 = new kony.ui.FlexContainer({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "clipBounds": true,
                            "id": "flex2Inner11" + i,
                            "isVisible": true,
                            "layoutType": kony.flex.FLOW_VERTICAL,
                            "left": "0dp",
                            "skin": "slFbox",
                            "top": "2dp",
                            "width": "100%",
                            "zIndex": 1
                        }, {}, {});
                        flex2Inner11.setDefaultUnit(kony.flex.DP);
                        var lblOfferTitleHidden1 = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "id": "lblOfferTitleHidden1" + i,
                            "isVisible": showPercent,
                            "left": "0dp",
                            "skin": "sknLblGothamBold30pxRed",
                            "text": percent,
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "2dp",
                            "width": "94%",
                            "zIndex": 1,
                            "centerX": "50%"
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        flex2Inner11.add(lblOfferTitleHidden1);
                    }
                    var lblAvailabilityHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblAvailabilityHidden" + i,
                        "isVisible": true,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": availability,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "12dp",
                        "width": "96.50%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferTitleHidden2 = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferTitleHidden2" + i,
                        "isVisible": showSubOfferTitle,
                        "left": "5dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": offTitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblOfferSubTitleHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "id": "lblOfferSubTitleHidden" + i,
                        "isVisible": true,
                        "left": "0dp",
                        "skin": "sknLblGothamBold28pxRed",
                        "text": response[i].offerSubtitle,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1,
                        "centerX": "50%"
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblValidityHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblValidityHidden" + i,
                        "isVisible": true,
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": response[i].validDateDescription,
                        "textStyle": {
                            "letterSpacing": 0,
                            "lineSpacing": 5,
                            "strikeThrough": false
                        },
                        "top": "10dp",
                        "width": "94%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    var lblPromoCodeHidden = new kony.ui.Label({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "id": "lblPromoCodeHidden" + i,
                        "isVisible": showPromocode,
                        "skin": "sknLblGothamBold16pxBlack",
                        "text": promoCode,
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "5dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    if (showPercent) {
                        flex2Inner1.add(lblAvailabilityHidden, flex2Inner11, lblOfferTitleHidden2, lblOfferSubTitleHidden, lblValidityHidden, lblPromoCodeHidden);
                    } else {
                        flex2Inner1.add(lblAvailabilityHidden, lblOfferTitleHidden2, lblOfferSubTitleHidden, lblValidityHidden, lblPromoCodeHidden);
                    }
                    var flex2Inner2 = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": true,
                        "id": "flex2Inner2" + i,
                        "isVisible": isBarcode,
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "left": "0dp",
                        "skin": "slFbox",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flex2Inner2.setDefaultUnit(kony.flex.DP);
                    var imgBarcode2 = new kony.ui.Image2({
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "height": "85dp",
                        "id": "imgBarcode2" + i,
                        "isVisible": showBarcode,
                        "imageWhenFailed": "notavailable_1080x142.png",
                        "imageWhileDownloading": "white_background.png",
                        "skin": "slImage",
                        "src": "white_background.png",
                        "top": "0dp",
                        "width": "80%",
                        "zIndex": 1,
                        "onDownloadComplete": onBarcodeDownloadCompleteCallback
                    }, {
                        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {});
                    var flexBtns = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_NONE,
                        "clipBounds": true,
                        "height": "35dp",
                        "id": "flexBtns" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "20dp",
                        "bottom": "20dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexBtns.setDefaultUnit(kony.flex.DP);
                    var flexRestrict = new kony.ui.FlexContainer({
                        "autogrowMode": kony.flex.AUTOGROW_NONE,
                        "clipBounds": true,
                        "height": "35dp",
                        "id": "flexRestrict" + i,
                        "isVisible": true,
                        "layoutType": kony.flex.FREE_FORM,
                        "left": "0dp",
                        "skin": "slFbox",
                        "top": "0dp",
                        "bottom": "20dp",
                        "width": "100%",
                        "zIndex": 1
                    }, {}, {});
                    flexRestrict.setDefaultUnit(kony.flex.DP);
                    var btnRestrictions = new kony.ui.Button({
                        "focusSkin": "sknBtnCouponExpandFoc",
                        "height": "28dp",
                        "id": "btnRestrictions_" + i,
                        "isVisible": isBarcode,
                        "left": "5%",
                        "skin": "sknBtnCouponExpand",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.viewRestrictions"),
                        "top": "0dp",
                        "zIndex": 5,
                        "width": "90%", //kony.flex.USE_PREFFERED_SIZE,
                        "onClick": showRestrictions
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    var btnAddToCart = new kony.ui.Button({
                        "focusSkin": "sknBtnBgRedWhiteFont11pxMonteserrat",
                        "height": "28dp",
                        "id": "btnAddToCart_" + i,
                        "isVisible": barcodeFlag,
                        "left": "51%",
                        "skin": "sknBtnBgRedWhiteFont11pxMonteserrat",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.addToCart"),
                        "top": "0dp",
                        "zIndex": 5,
                        "width": "44%", //kony.flex.USE_PREFFERED_SIZE,
                        "onClick": applyCouponToCart
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    var btnAddToWallet = new kony.ui.Button({
                        "focusSkin": "sknBtnCouponExpandFoc",
                        "height": "28dp",
                        "id": "btnAddToWallet_" + i,
                        "isVisible": barcodeFlag,
                        "left": leftAlignRestrictions,
                        "skin": "sknBtnCouponExpand",
                        "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.addToWallet"),
                        "top": "0dp",
                        "width": "44%",
                        "zIndex": 5,
                        "onClick": addToWalletOnClick
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                        "displayText": true,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "showProgressIndicator": false
                    });
                    if (MVCApp.Toolbox.common.getRegion() == "US") {
                        if (promoCode === "") {
                            btnRestrictions.left = "51%";
                            btnRestrictions.width = "44%";
                            btnAddToWallet.left = "5%";
                            btnAddToWallet.isVisible = true;
                            flexBtns.add(btnAddToWallet, btnRestrictions);
                            flex2Inner2.add(imgBarcode2, flexBtns);
                        } else if (promoCode !== "") {
                            btnAddToWallet.isVisible = true;
                            btnAddToWallet.left = "5%";
                            btnAddToCart.isVisible = true;
                            if (MVCApp.Toolbox.common.lookForCoupon(promoCode.split(" ")[2])) {
                                if (!MVCApp.Toolbox.common.checkIfGuestUser()) {
                                    btnAddToCart.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.applied");
                                    btnAddToCart.onClick = function() {};
                                    btnAddToCart.skin = "sknBtnCouponApply";
                                    btnAddToCart.focusSkin = "sknBtnCouponApply";
                                } else {
                                    btnAddToCart.info = {
                                        "promoCode": promoCode
                                    };
                                }
                            } else {
                                btnAddToCart.info = {
                                    "promoCode": promoCode
                                };
                            }
                            flexRestrict.add(btnRestrictions);
                            flexBtns.add(btnAddToWallet, btnAddToCart);
                            flex2Inner2.add(imgBarcode2, flexBtns, flexRestrict);
                        }
                    } else {
                        btnRestrictions.left = "51%";
                        btnRestrictions.width = "44%";
                        flexBtns.add(btnAddToWallet, btnRestrictions);
                        flex2Inner2.add(imgBarcode2, flexBtns);
                    }
                    //flex2Inner2.add(flexBtns);
                    if (isBarcode) {
                        var flex2Inner3 = new kony.ui.FlexContainer({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "clipBounds": true,
                            "id": "flex2Inner3" + i,
                            "isVisible": !isBarcode,
                            "layoutType": kony.flex.FLOW_VERTICAL,
                            "left": "0dp",
                            "skin": "slFbox",
                            "top": "10dp",
                            "width": "100%",
                            "zIndex": 2
                        }, {}, {});
                        flex2Inner3.setDefaultUnit(kony.flex.DP);
                        if (showPercent) {
                            var flexNBCTitle = new kony.ui.FlexContainer({
                                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                                "clipBounds": true,
                                "id": "flexNBCTitle" + i,
                                "isVisible": true,
                                "layoutType": kony.flex.FLOW_VERTICAL,
                                "left": "0dp",
                                "skin": "slFbox",
                                "top": "2dp",
                                "width": "100%",
                                "zIndex": 1
                            }, {}, {});
                            flexNBCTitle.setDefaultUnit(kony.flex.DP);
                            var lblOfferTitleNBC = new kony.ui.Label({
                                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                                "id": "lblOfferTitleNBC" + i,
                                "isVisible": showPercent,
                                "left": "0dp",
                                "skin": "sknLblGothamBold30pxRed",
                                "text": percent,
                                "textStyle": {
                                    "letterSpacing": 0,
                                    "strikeThrough": false
                                },
                                "top": "2dp",
                                "width": "94%",
                                "zIndex": 1,
                                "centerX": "50%"
                            }, {
                                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                                "padding": [0, 0, 0, 0],
                                "paddingInPixel": false
                            }, {
                                "textCopyable": false,
                                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
                            });
                            flexNBCTitle.add(lblOfferTitleNBC);
                        }
                        var lblOfferTitle2NBC = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "id": "lblOfferTitle2NBC" + i,
                            "isVisible": showSubOfferTitle,
                            "left": "5dp",
                            "skin": "sknLblGothamBold28pxRed",
                            "text": offTitle,
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "2dp",
                            "width": "96.50%",
                            "zIndex": 1,
                            "centerX": "50%"
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        var lblOfferSubTitleNBC = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "id": "lblOfferSubTitleNBC" + i,
                            "isVisible": true,
                            "left": "0dp",
                            "skin": "sknLblGothamBold28pxRed",
                            "text": response[i].offerSubtitle,
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "10dp",
                            "width": "94%",
                            "zIndex": 1,
                            "centerX": "50%"
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        var lblNoBarcode = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "centerX": "50%",
                            "id": "lblNoBarcode" + i,
                            "isVisible": true, //!isBarcode,
                            "left": "5dp",
                            "skin": "sknLblBlackPx14",
                            "text": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmCoupons.noBarcodeRequired"),
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "10dp",
                            "width": "96.50%",
                            "zIndex": 1
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        var lblNoBarCodeDetails = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "centerX": "50%",
                            "id": "lblNoBarCodeDetails" + i,
                            "isVisible": true, //!isBarcode,
                            "left": "15dp",
                            "skin": "sknLblR194G7B36Px14",
                            "text": response[i].offerSubtitle,
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "11dp",
                            "width": "94%",
                            "zIndex": 1
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        var lblNoBarCodeDetailsValidity = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "centerX": "50%",
                            "id": "lblNoBarCodeDetailsValidity" + i,
                            "isVisible": true, //!isBarcode,
                            "skin": "sknLblGothamBold14pxBlack",
                            "text": response[i].validDateDescription,
                            "textStyle": {
                                "letterSpacing": 0,
                                "lineSpacing": 5,
                                "strikeThrough": false
                            },
                            "top": "10dp",
                            "width": "94%",
                            "zIndex": 1
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        var lblValidityRegionNBC = new kony.ui.RichText({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "centerX": "50%",
                            "id": "lblValidityRegionNBC" + i,
                            "isVisible": true,
                            "skin": "sknLblGothamBold16pxBlack",
                            "text": response[i].offerInstructions,
                            "top": "10dp",
                            "width": "94%",
                            "zIndex": 1
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
                        });
                        var lblPromoCodeNBC = new kony.ui.Label({
                            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                            "centerX": "50%",
                            "id": "lblPromoCodeNBC" + i,
                            "isVisible": showPromocode,
                            "skin": "sknLblGothamBold16pxBlack",
                            "text": promoCode,
                            "textStyle": {
                                "letterSpacing": 0,
                                "strikeThrough": false
                            },
                            "top": "5dp",
                            "width": "96.50%",
                            "zIndex": 1
                        }, {
                            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                            "padding": [0, 0, 0, 0],
                            "paddingInPixel": false
                        }, {
                            "textCopyable": false
                        });
                        if (showPercent) {
                            flex2Inner3.add(flexNBCTitle, lblOfferTitle2NBC, lblOfferSubTitleNBC, lblNoBarCodeDetailsValidity, lblNoBarcode);
                        } else {
                            flex2Inner3.add(lblOfferTitle2NBC, lblOfferSubTitleNBC, lblNoBarCodeDetailsValidity, lblNoBarcode);
                        }
                        flxCoupon2Info.add(flex2Inner1, flex2Inner2, flex2Inner3);
                    } else {
                        flxCoupon2Info.add(flex2Inner1, flex2Inner2);
                    }
                    var setanimateFlexId = new kony.ui.Label({
                        "centerX": "50%",
                        "id": "setanimateFlexId_" + i,
                        "isVisible": false, //!isBarcode,
                        "left": "15dp",
                        "skin": "sknLblGothamBold14pxBlack",
                        "text": "false",
                        "textStyle": {
                            "letterSpacing": 0,
                            "strikeThrough": false
                        },
                        "top": "11dp",
                        "width": "96.50%",
                        "zIndex": 1
                    }, {
                        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                        "padding": [0, 0, 0, 0],
                        "paddingInPixel": false
                    }, {
                        "textCopyable": false
                    });
                    flxCoupon.add(flxCoupon1Info, flxCoupon2Info, setanimateFlexId);
                    flxCouponMainContainer.add(flxCoupon);
                    frmCoupons.scroll.add(flxCouponMainContainer);
                    frmCoupons.scroll.forceLayout();
                }
            }
        } catch (e) {
            alert("Something unexpected happened. Please try again later.");
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            if (e) {
                TealiumManager.trackEvent("Exception While Dynamically Creating The Coupons UI", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }
    var httpRequestSentCounter = 0;

    function applyCouponToCart(eventObj) {
        var info = eventObj.info.promoCode;
        var id = eventObj.id;
        kony.print("Coupon code" + info);
        MVCApp.Toolbox.common.showLoadingIndicator("");
        info = info.split(" ")[2];
        var inputParams = {};
        inputParams.Authorization = MVCApp.Toolbox.common.getJWTToken() || "";
        inputParams.couponCode = info || "";
        gblCouponCode = info;
        inputParams.basketId = MVCApp.Toolbox.common.getBasket() || "";
        if (inputParams.Authorization == "" || inputParams.basketId == "") {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.coupons.noproductsInBasket"), null, constants.ALERT_TYPE_INFO, function() {}, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.okayButton"), "");
            return;
        }
        var service = MVCApp.serviceType.AppcommerceCart;
        var operation = MVCApp.serviceEndPoints.addCouponToCart;
        MakeServiceCall(service, operation, inputParams, function(results) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            frmCoupons["btnAddToCart_" + id.split("_")[1]].text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.applied");
            frmCoupons["btnAddToCart_" + id.split("_")[1]].skin = "sknBtnCouponApply";
            TealiumManager.trackEvent("Apply to cart", {
                adobe_app_event: "App Add Coupon",
                app_coupon_code: info
            });
            frmCoupons["btnAddToCart_" + id.split("_")[1]].onClick = function() {};
            MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
            kony.print("Coupon applied to cart");
        }, function(results) {
            var fault = results.fault || "";
            var errCode = fault.type || "";
            if (errCode == "InvalidCouponCodeException") {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                frmCoupons["btnAddToCart_" + id.split("_")[1]].text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.applied");
                frmCoupons["btnAddToCart_" + id.split("_")[1]].skin = "sknBtnCouponApply";
                frmCoupons["btnAddToCart_" + id.split("_")[1]].onClick = function() {};
                MVCApp.Toolbox.common.getBasketDetails(MVCApp.Toolbox.common.getJWTToken());
                kony.print("Coupon applied to cart");
            } else if (errCode == "ExpiredTokenException") {
                kony.print("ExpiredTokenException");
                httpRequestSentCounter++;
                sendHttpClientRequesttoFetchSession(eventObj);
            } else {
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            }
        });
    }

    function sendHttpClientRequesttoFetchSession(eventObj) {
        if (httpRequestSentCounter <= 2) {
            var httpClient = new kony.net.HttpRequest();
            httpClient.timeout = 5000;
            httpClient.open(constants.HTTP_METHOD_GET, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "on/demandware.store/Sites-MichaelsUS-Site/default/MobileAppController-GetSessionBasket");
            httpClient.onReadyStateChange = (response) => {
                kony.print("coupon response for applyto carrt 2nd time======" + JSON.stringify(response));
                if (response.readyState == constants.HTTP_READY_STATE_DONE) {
                    kony.print("call done" + response.responseText);
                    var resultHttp = "";
                    try {
                        resultHttp = JSON.parse(response.responseText);
                    } catch (e) {
                        kony.print("could not find response Text");
                    }
                    if (resultHttp.success == "true") {
                        MVCApp.Toolbox.common.setJWTToken(resultHttp.access_token || "");
                        MVCApp.Toolbox.common.setBasket(resultHttp.basket_id || "");
                        MVCApp.Toolbox.common.setCustomerId(resultHttp.customer_id || "");
                        applyCouponToCart(eventObj);
                        httpRequestSentCounter = 0;
                    } else if (resultHttp.success == "" && resultHttp.success === "false") {
                        httpRequestSentCounter++;
                        sendHttpClientRequesttoFetchSession(eventObj)
                    }
                }
            }
            httpClient.send();
        } else {
            httpRequestSentCounter = 0;
            MVCApp.Toolbox.common.dismissLoadingIndicator();
        }
    }

    function couponSuccess(results) {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        MVCApp.Toolbox.common.customAlertNoTitle("Coupon successfully applied to Cart", "");
        kony.print("Coupon applied to cart");
    }

    function couponFailure(results) {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        kony.print("Failed to apply Coupon to cart");
    }
    /**
     * @function getCouponAnalytics
     *
     */
    function getCouponAnalytics(response) {
        if (response != undefined && response != null && response != "") {
            gCouponsResponse = response;
            var offerInputString = "";
            for (var i = 0; i < response.length; i++) {
                //kony.print("response----i---"+i+"--value--"+JSON.stringify(response[i]));
                var offerID = response[i].offerID;
                if (i == response.length - 1) {
                    offerInputString = offerInputString + offerID;
                } else {
                    offerInputString = offerID + "," + offerInputString;
                }
            }
        }
        //var analyticsStart = new Date().getTime();
        //kony.print("analyticsStart TIME::"+analyticsStart);
        MVCApp.getHomeController().getCouponAnalytics(offerInputString);
    }
    /**
     * @function
     *
     */
    function animateFlexCoupons(eventObj) {
        var id = eventObj.id;
        id = id.split("_")[1];
        var flex1 = "flxCoupon1Info" + id;
        var flex2 = "flxCoupon2Info" + id;
        if (frmCoupons[flex1].isVisible) {
            frmCoupons[flex1].isVisible = false;
            frmCoupons[flex2].isVisible = true;
        } else {
            frmCoupons[flex1].isVisible = true;
            frmCoupons[flex2].isVisible = false;
        }
    }

    function onBarcodeDownloadCompleteCallback() {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function GotoCoupons(response) {
        if (response) {
            MVCApp.getHomeController().loadCoupons();
        }
    }
    return {
        bindEvents: bindEvents,
        coupons: coupons,
        hideCoupons: hideCoupons,
        animateCoupons: animateCoupons,
        showRestrictions: showRestrictions,
        setDataToDynamicFlexes: setDataToDynamicFlexes,
        setDataToDynamicFlexesForAndroid: setDataToDynamicFlexesForAndroid,
        animateFlexCoupons: animateFlexCoupons,
        test: test,
        doLayoutCallBackFlex: doLayoutCallBackFlex,
        doLayoutCallBackScroll: doLayoutCallBackScroll,
        barcodeAssignment: barcodeAssignment,
        show: show
    };
});