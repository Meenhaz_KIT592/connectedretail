/**
 * PUBLIC
 * This is the view for the WeeklyAdHome form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.WeeklyAdHomeView = (function() {
    /**
     * PUBLIC
     * Open the More form
     */
    //show WeeklyAdForm
    function show() {
        frmWeeklyAdHome.flxHeaderWrap.textSearch.text = "";
        frmWeeklyAdHome.flxSearchResultsContainer.setVisibility(false);
        frmWeeklyAdHome.show();
    }

    function getDepartmentDetails(eventObj) {
        var segData = frmWeeklyAdHome.segAds.data;
        var selectedIndex = frmWeeklyAdHome.segAds.selectedRowIndex[1];
        var selectedData = segData[selectedIndex];
        MVCApp.getWeeklyAdController().setFormEntryFromCartOrWebView(false);
        MVCApp.getWeeklyAdController().loadWeeklyAds(selectedData.id, selectedData.description, "frmWeeklyAdHome");
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmWeeklyAdHome.segAds.onRowClick = function(eventObj) {
            getDepartmentDetails(eventObj);
        };
        MVCApp.searchBar.bindEvents(frmWeeklyAdHome, "frmWeeklyAdHome");
        MVCApp.tabbar.bindEvents(frmWeeklyAdHome);
        MVCApp.tabbar.bindIcons(frmWeeklyAdHome, "frmWeeklyAd");
        frmWeeklyAdHome.segAds.onTouchStart = function(eventObj, x, y) {
            checkonTouchStart(eventObj, x, y);
        };
        frmWeeklyAdHome.segAds.onTouchEnd = function(eventObj, x, y) {
            checkonTouchEnd(eventObj, x, y);
        };
        frmWeeklyAdHome.preShow = function() {
            try {
                var numItems = MVCApp.Toolbox.common.getNumberOfItemsInCart();
                if (numItems == 0) {
                    frmWeeklyAdHome.flxHeaderWrap.lblItemsCount.isVisible = false;
                } else {
                    frmWeeklyAdHome.flxHeaderWrap.lblItemsCount.isVisible = true;
                }
                frmWeeklyAdHome.flxSearchOverlay.setVisibility(false);
                frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
            } catch (e) {
                kony.print("Exception in frmWeeklyAd preShow " + e);
            }
            //       frmWeeklyAdHome.flxSearchOverlay.setVisibility(false);
            //       frmWeeklyAdHome.flxVoiceSearch.setVisibility(false);
        };
        frmWeeklyAdHome.postShow = function() {
            var cartValue = MVCApp.Toolbox.common.getCartItemValue();
            frmWeeklyAdHome.flxHeaderWrap.lblItemsCount.text = cartValue;
            MVCApp.Toolbox.common.setBrightness("frmWeeklyAdHome");
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Weekly Ad Navigation: Michaels Mobile App";
            lTealiumTagObj.page_name = "Weekly Ad Navigation: Michaels Mobile App";
            lTealiumTagObj.page_type = "Navigation";
            lTealiumTagObj.page_category_name = "Weekly Ad";
            lTealiumTagObj.page_category = "Weekly Ad";
            lTealiumTagObj.category_ID = "Weekly Ad";
            TealiumManager.trackView("Weekly Ad Tab", lTealiumTagObj);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            var entryType = "";
            var storeid = MVCApp.Toolbox.common.getStoreID();
            cust360Obj.storeid = storeid;
            if (weeklyAdEntryFromVoiceSearch) {
                entryType = "VoiceSearch";
                MVCApp.Customer360.callVoiceSearchResult("success", "", "WeeklyAd");
            }
            cust360Obj.entryType = entryType;
            if (!formFromDeeplink) {
                MVCApp.Customer360.sendInteractionEvent("WeeklyAdCategories", cust360Obj);
            } else {
                formFromDeeplink = false;
            }
        };
        frmWeeklyAdHome.onDeviceBack = function() {
            try {
                if (voiceSearchFlag) {
                    onXButtonClick(voiceSearchForm);
                    if (voiceSearchForm.flxVoiceSearch.lblMicStatusText.text == MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusListening")) {
                        voiceSearchFlag = false;
                        voiceSearchForm.flxVoiceSearch.setVisibility(false);
                    } else {
                        voiceSearchFlag = true;
                        tapToCancelFlag = true;
                        deviceBackFlag = true;
                    }
                    MVCApp.Toolbox.common.sendTealiumDataForAppSearchCancellation();
                }
                if (kony.application.getPreviousForm().id == "frmHome") {
                    MVCApp.getHomeController().load();
                }
            } catch (e) {
                if (e) {
                    kony.print("Exception in getting the previous form" + e);
                }
            }
        };
    }

    function updateScreen(results) {
        var departmentsData = results.getAdDepartmentsData();
        frmWeeklyAdHome.segAds.removeAll();
        frmWeeklyAdHome.segAds.data = [];
        if ((departmentsData && departmentsData.length > 0) && (departmentsData.length > 1 || departmentsData[0].data.length > 0)) {
            frmWeeklyAdHome.segAds.widgetDataMap = {
                lblCategoryName: "description",
                lblAisle: "listingCount",
                lblNumber: "deals"
            };
            frmWeeklyAdHome.segAds.data = departmentsData;
            show();
        } else {
            MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmWeeklyAd.noAds"), MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Alert"), constants.ALERT_TYPE_INFO, null, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"), "");
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    //Code for Footer Animation
    var checkX;
    var checkY;

    function checkonTouchStart(eventObj, x, y) {
        checkX = x;
        checkY = y;
    }

    function checkonTouchEnd(eventObj, x, y) {
        var tempX;
        var tempY;
        tempX = checkX;
        tempY = checkY;
        checkX = x;
        checkY = y;
        var checkScroll;
        checkScroll = tempY - checkY;
        if (checkScroll > 1) {
            MVCApp.Toolbox.common.hideFooter();
            frmWeeklyAdHome.segAds.bottom = "0dp";
        } else {
            MVCApp.Toolbox.common.dispFooter();
            frmWeeklyAdHome.segAds.bottom = "54dp";
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen
    };
});