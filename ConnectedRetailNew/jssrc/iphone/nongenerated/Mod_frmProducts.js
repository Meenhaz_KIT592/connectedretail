//Type your code here
function tapProducts() {
    frmProducts.flxFooterWrap.flxProducts.skin = 'sknFlexBgBlackOpaque10';
    frmProducts.flxFooterWrap.flxProjects.skin = 'slFbox';
    frmProducts.flxFooterWrap.flxWeeklyAd.skin = 'slFbox';
    frmProducts.flxFooterWrap.flxEvents.skin = 'slFbox';
    frmProducts.flxFooterWrap.flxMore.skin = 'slFbox';
}

function pre_frmProducts() {
    searchBlur();
    tapProducts();
    animateDepartments();
    frmProducts.flxHeaderWrap.btnHome.isVisible = true;
    frmProducts.flxHeaderWrap.textSearch.text = '';
}

function NAV_frmProducts() {
    frmProducts.show();
    pre_frmProducts();
}

function animateDepartments() {
    function test_CALLBACK() {}

    function anim_flxDepartments() {
        //alert('hi');
        frmProducts.flxDepartment1.isVisible = true;
        frmProducts.flxDepartment2.isVisible = true;
        frmProducts.flxDepartment3.isVisible = true;
        frmProducts.flxDepartment4.isVisible = true;
        frmProducts.flxDepartment5.isVisible = true;
        frmProducts.flxDepartment6.isVisible = true;
        frmProducts.flxDepartment7.isVisible = true;
        frmProducts.flxDepartment8.isVisible = true;
        var trans101 = kony.ui.makeAffineTransform();
        trans101.scale(1, 1);
        frmProducts.flxDepartment1.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.5,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment2.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.6,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment3.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.7,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment4.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.8,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment5.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 0.9,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment6.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 1.0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment7.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 1.1,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
        frmProducts.flxDepartment8.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans101
            }
        }), {
            "delay": 1.2,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        });
    }
    /*var departments  = ['frmProducts.flxDepartment1','frmProducts.flxDepartment2','frmProducts.flxDepartment3'];
	var i;
  	var mydep = "";
	for(i = 1; i < 9; i++) {
		mydep = "frmProducts.flxDepartment"+i+".isVisible = false";
      	alert(mydep);
      	var trans102 = kony.ui.makeAffineTransform();
        trans102.scale(0.2, 0.2);
        departments[i].animate(
          kony.ui.createAnimation({"100":{"stepConfig":{"timingFunction":kony.anim.EASE}, "transform":trans102}}),
          {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.1},
          {"animationEnd": test_CALLBACK }
        );
    }*/
    frmProducts.flxDepartment1.isVisible = false;
    frmProducts.flxDepartment2.isVisible = false;
    frmProducts.flxDepartment3.isVisible = false;
    frmProducts.flxDepartment4.isVisible = false;
    frmProducts.flxDepartment5.isVisible = false;
    frmProducts.flxDepartment6.isVisible = false;
    frmProducts.flxDepartment7.isVisible = false;
    frmProducts.flxDepartment8.isVisible = false;
    var trans100 = kony.ui.makeAffineTransform();
    trans100.scale(1, 0.01);
    frmProducts.flxDepartment1.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment2.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment3.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment4.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment5.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment6.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment7.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    });
    frmProducts.flxDepartment8.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0.1,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.1
    }, {
        "animationEnd": anim_flxDepartments
    });
}