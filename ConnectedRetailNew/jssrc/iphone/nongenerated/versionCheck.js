//Type your code here
MVCApp = MVCApp || {};
MVCApp.versionCheck = MVCApp.versionCheck || {};
MVCApp.versionCheck.getRequestParams = function() {
    var deviceInfo = kony.os.deviceInfo();
    var requestParams = {};
    requestParams.appVersion = appConfig.appVersion;
    requestParams.appId = "michael";
    var currLocale = MVCApp.Toolbox.Service.getLocale();
    requestParams.locale = currLocale;
    requestParams.deviceVersion = deviceInfo.version;
    requestParams.deviceOsType = deviceInfo.name;
    //requestParams['$select'] = "*";
    return requestParams;
};
MVCApp.versionCheck.succesCallBack = function(result) {
    if ((result.needUpgrade || "") == "true") {
        kony.ui.Alert({
            "message": result.upgradeMessage || "",
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.upgradeTitle") || "",
            "yesLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.upgradeLbl"),
            "noLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Exit"),
            "alertIcon": "",
            "alertHandler": function(response) {
                if (response) {
                    var upgradeURL = result.upgradeURL;
                    upgradeURL = decodeURI(upgradeURL);
                    upgradeURL = upgradeURL.replace(/%2C/g, ",");
                    upgradeURL = encodeURI(upgradeURL);
                    kony.store.setItem("VersionUpdate", "true");
                    MVCApp.Toolbox.common.openApplicationURL(upgradeURL);
                    kony.print("response::" + response);
                    kony.timer.schedule("AppUpdateTimer", exitTheApp, 5, false);
                } else {
                    kony.application.exit();
                }
            }
        }, {});
    } else if ((result.needUpgrade || "") == "false" && (result.upgradeType || "") == "OPTIONAL") {
        kony.ui.Alert({
            "message": result.upgradeMessage || "",
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.upgradeTitle") || "",
            "yesLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.upgradeLbl"),
            "noLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Later"),
            "alertIcon": "",
            "alertHandler": function(response) {
                if (response) {
                    var upgradeURL = result.upgradeURL;
                    upgradeURL = decodeURI(upgradeURL);
                    upgradeURL = upgradeURL.replace(/%2C/g, ",");
                    upgradeURL = encodeURI(upgradeURL);
                    kony.store.setItem("VersionUpdate", "true");
                    MVCApp.Toolbox.common.openApplicationURL(upgradeURL);
                }
            }
        }, {});
    } else {}
};

function exitTheApp() {
    try {
        kony.timer.cancel("AppUpdateTimer");
        kony.application.exit();
    } catch (exception) {
        kony.print("exception = " + exception);
    }
}
MVCApp.versionCheck.succesCallBackLogin = function(result) {
    MVCAppToolBox_dismissLoadingIndicator();
    if ((result.needUpgrade || "") == "true") {
        kony.ui.Alert({
            "message": result.upgradeMessage || "",
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.upgradeTitle") || "",
            "yesLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.upgradeLbl"),
            "noLabel": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.Exit"),
            "alertIcon": "",
            "alertHandler": function(response) {
                if (response) {
                    upgradeURL = decodeURI(upgradeURL);
                    upgradeURL = upgradeURL.replace(/%2C/g, ",");
                    upgradeURL = encodeURI(upgradeURL);
                    MVCApp.Toolbox.common.openApplicationURL(result.upgradeURL);
                    kony.timer.schedule("AppUpdateTimer", exitTheApp, 5, false);
                } else {
                    kony.application.exit();
                }
            }
        }, {});
        return false;
    } else {
        return true;
    }
};
MVCApp.versionCheck.failureCallBack = function(result) {};