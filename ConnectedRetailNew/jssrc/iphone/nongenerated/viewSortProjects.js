/**
 * PUBLIC
 * This is the view for the Scan form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.RefineProjectsView = (function() {
    var gblRefineCraft = [];
    var gblRefineSkill = [];
    var craftVisible = false;
    var skillVisible = false;
    var prodVisible = false;
    var extraFiltersAdded = false;
    var latestRefSelected = "";
    /**
     * PUBLIC
     * Open the Refine Projects form
     */
    function show() {
        extraFiltersAdded = false;
        frmRefineProjects.show();
    }

    function showSkillFields() {
        frmRefineProjects.flxSkillLevelCategories.setVisibility(true);
        frmRefineProjects.lblTickSkill.text = "U";
    }

    function hideSkillFields() {
        frmRefineProjects.flxSkillLevelCategories.setVisibility(false);
        frmRefineProjects.lblTickSkill.text = "D";
    }

    function showTimeFields() {
        frmRefineProjects.flxTimeReqCategories.setVisibility(true);
        frmRefineProjects.lblTickTime.text = "U";
    }

    function hideTimeFields() {
        frmRefineProjects.flxTimeReqCategories.setVisibility(false);
        frmRefineProjects.lblTickTime.text = "D";
    }

    function expandCollapseSkill(eventobj) {
        if (eventobj.lblTickSkill.text == "D") {
            showSkillFields();
            hideTimeFields();
        } else {
            hideSkillFields();
        }
    }

    function expandCollapseTime(eventobj) {
        if (eventobj.lblTickTime.text == "D") {
            showTimeFields();
            hideSkillFields();
        } else {
            hideTimeFields();
        }
    }

    function findIfrefinedAlready(refstring, refineAttr) {
        var newString = "";
        if (refineAttr.length !== 0) {
            for (var i = 0; i < refineAttr.length; i++) {
                if (refstring === refineAttr[i]) {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    function removeRefinements(refstring, refineAttr) {
        for (var i = 0; i < refineAttr.length; i++) {
            if (refstring === refineAttr[i]) {
                refineAttr.splice(i, 1);
            }
        }
        return refineAttr;
    }

    function selectRefineMents(attribute, refineValue) {
        var selectedRefs = MVCApp.getSortProjectController().getRefinementsExcludingCgid();
        var refineAttr = [];
        for (var i = 0; i < selectedRefs.length; i++) {
            var temp = selectedRefs[i] || "";
            var attributeTemp = temp.attr || "";
            if (attributeTemp === attribute) {
                var refinetemp = temp.refValues || [];
                for (var j = 0; j < refinetemp.length; j++) {
                    refineAttr.push(refinetemp[j]);
                }
            }
        }
        kony.print("The array refine Attr is  " + JSON.stringify(refineAttr));
        if (refineAttr.length !== 0) {
            var refinementIfExists = findIfrefinedAlready(refineValue, refineAttr);
            if (refinementIfExists) {
                refineAttr = removeRefinements(refineValue, refineAttr);
            } else {
                refineAttr.push(refineValue);
            }
        }
        var refineArrNew = MVCApp.getSortProjectController().getRefinementsExcludingCgid();
        var foundKey = false;
        kony.print("refine Arr  " + JSON.stringify(refineAttr));
        if (refineArrNew.length !== 0) {
            for (var i = 0; i < refineArrNew.length; i++) {
                var tempNew = refineArrNew[i] || "";
                var attriNew = refineArrNew[i].attr;
                if (attriNew === attribute) {
                    if (refineAttr.length !== 0) {
                        tempNew.refValues = refineAttr;
                    } else {
                        refineArrNew.splice(i, 1);
                    }
                    foundKey = true;
                }
            }
        }
        if (!foundKey) {
            refineAttr.push(refineValue);
            var record = {
                "attr": attribute,
                "refValues": refineAttr
            };
            refineArrNew.push(record);
        }
        kony.print("refine new Projects " + JSON.stringify(refineArrNew));
        MVCApp.getSortProjectController().setRefinementsExcludingCgid(refineArrNew);
        MVCApp.getSortProjectController().prepareForJustServiceCall();
        extraFiltersAdded = true;
    }

    function selectRefineMentsSkill(eventobj) {
        var attribute = "";
        if (MVCApp.Toolbox.common.getRegion() === "US") {
            attribute = "c_skillLevel";
        } else {
            attribute = "c_projectSkillLevelAdult";
        }
        var idSkill = eventobj.id.split("flxRefineSkill")[1];
        idSkill = parseInt(idSkill);
        var skill = gblRefineSkill[idSkill];
        selectRefineMents(attribute, skill);
        latestRefSelected = "SKILL";
    }

    function selectRefineMentsCraft(eventobj) {
        var attribute = "";
        if (MVCApp.Toolbox.common.getRegion() === "US") {
            attribute = "c_ecomCraftTime";
        } else {
            attribute = "c_craftTime";
        }
        var idCraft = eventobj.id.split("flxRefineCraft")[1];
        idCraft = parseInt(idCraft);
        var craft = gblRefineCraft[idCraft];
        selectRefineMents(attribute, craft);
        latestRefSelected = "CRAFT";
    }

    function updateCraftValues(craftTimeValues) {
        frmRefineProjects.flxTimeReqCategories.removeAll();
        gblRefineCraft = []
        if (craftTimeValues.length !== 0) {
            craftVisible = true;
            for (var i = 0; i < craftTimeValues.length; i++) {
                var temp = craftTimeValues[i];
                var craftValue = temp.value;
                gblRefineCraft.push(craftValue);
                var count = i;
                if (i < 10) {
                    count = "0" + i;
                }
                var gapVisible = false;
                if (i + 1 === craftTimeValues.length) {
                    gapVisible = true;
                }
                var flxRefineCraft = new kony.ui.FlexContainer({
                    "autogrowMode": kony.flex.AUTOGROW_NONE,
                    "clipBounds": true,
                    "height": "30dp",
                    "id": "flxRefineCraft" + count,
                    "isVisible": true,
                    "layoutType": kony.flex.FREE_FORM,
                    "left": "0dp",
                    "skin": "slFbox",
                    "top": "0dp",
                    "width": "100%",
                    "zIndex": 1,
                    "onClick": selectRefineMentsCraft
                }, {}, {});
                flxRefineCraft.setDefaultUnit(kony.flex.DP);
                var lblRefineCraft = new kony.ui.Label({
                    "centerY": "50%",
                    "id": "lblRefineCraft" + count,
                    "isVisible": true,
                    "left": "20dp",
                    "skin": "sknLblGothamBook24pxBlack",
                    "text": temp.label + " (" + temp.hit_count + ")",
                    "textStyle": {
                        "letterSpacing": 0,
                        "strikeThrough": false
                    },
                    "width": kony.flex.USE_PREFFERED_SIZE,
                    "zIndex": 1
                }, {
                    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                    "padding": [0, 0, 0, 0],
                    "paddingInPixel": false
                }, {
                    "textCopyable": false
                });
                var lblSelectedRefineCraft = new kony.ui.Label({
                    "centerY": "50%",
                    "height": "100%",
                    "id": "lblSelectedRefineCraft" + count,
                    "isVisible": false,
                    "right": "10dp",
                    "skin": "sknLblIcon32pxRed",
                    "text": "u",
                    "textStyle": {
                        "letterSpacing": 0,
                        "strikeThrough": false
                    },
                    "width": "50dp",
                    "zIndex": 1
                }, {
                    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                    "padding": [0, 0, 1, 0],
                    "paddingInPixel": false
                }, {
                    "textCopyable": false
                });
                var flxGapSubRefine1 = new kony.ui.FlexContainer({
                    "autogrowMode": kony.flex.AUTOGROW_NONE,
                    "clipBounds": true,
                    "height": "1dp",
                    "id": "flxGapRefineCraft" + count,
                    "isVisible": gapVisible,
                    "layoutType": kony.flex.FREE_FORM,
                    "left": "0dp",
                    "skin": "sknFlexBgBlackOpaque20",
                    "top": "0dp",
                    "width": "100%",
                    "zIndex": 1
                }, {}, {});
                flxGapSubRefine1.add();
                flxRefineCraft.add(lblRefineCraft, lblSelectedRefineCraft);
                frmRefineProjects["flxTimeReqCategories"].add(flxRefineCraft, flxGapSubRefine1);
            }
        } else {
            craftVisible = false;
            frmRefineProjects.flxTimeReq.setVisibility(false);
        }
    }

    function updateSkillValues(skillValues) {
        frmRefineProjects.flxSkillLevelCategories.removeAll();
        gblRefineSkill = [];
        if (skillValues.length !== 0) {
            skillVisible = true;
            for (var i = 0; i < skillValues.length; i++) {
                var temp = skillValues[i];
                var count = i;
                if (i < 10) {
                    count = "0" + i;
                }
                var gapVisible = false;
                if (i + 1 === skillValues.length) {
                    gapVisible = true;
                }
                gblRefineSkill.push(temp.value);
                var flxRefineSkill = new kony.ui.FlexContainer({
                    "autogrowMode": kony.flex.AUTOGROW_NONE,
                    "clipBounds": true,
                    "height": "30dp",
                    "id": "flxRefineSkill" + count,
                    "isVisible": true,
                    "layoutType": kony.flex.FREE_FORM,
                    "left": "0dp",
                    "skin": "slFbox",
                    "top": "0dp",
                    "width": "100%",
                    "zIndex": 1,
                    "onClick": selectRefineMentsSkill
                }, {}, {});
                flxRefineSkill.setDefaultUnit(kony.flex.DP);
                var lblRefineSkill = new kony.ui.Label({
                    "centerY": "50%",
                    "id": "lblRefineSkill" + count,
                    "isVisible": true,
                    "left": "20dp",
                    "skin": "sknLblGothamBook24pxBlack",
                    "text": temp.label + " (" + temp.hit_count + ")",
                    "textStyle": {
                        "letterSpacing": 0,
                        "strikeThrough": false
                    },
                    "width": kony.flex.USE_PREFFERED_SIZE,
                    "zIndex": 1
                }, {
                    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                    "padding": [0, 0, 0, 0],
                    "paddingInPixel": false
                }, {
                    "textCopyable": false
                });
                var lblSelectedRefineSkill = new kony.ui.Label({
                    "centerY": "50%",
                    "height": "100%",
                    "id": "lblSelectedRefineSkill" + count,
                    "isVisible": false,
                    "right": "10dp",
                    "skin": "sknLblIcon32pxRed",
                    "text": "u",
                    "textStyle": {
                        "letterSpacing": 0,
                        "strikeThrough": false
                    },
                    "width": "50dp",
                    "zIndex": 1
                }, {
                    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                    "padding": [0, 0, 1, 0],
                    "paddingInPixel": false
                }, {
                    "textCopyable": false
                });
                var flxGapSubRefine1 = new kony.ui.FlexContainer({
                    "autogrowMode": kony.flex.AUTOGROW_NONE,
                    "clipBounds": true,
                    "height": "1dp",
                    "id": "flxGapRefineSubCat" + count,
                    "isVisible": gapVisible,
                    "layoutType": kony.flex.FREE_FORM,
                    "left": "0dp",
                    "skin": "sknFlexBgBlackOpaque20",
                    "top": "0dp",
                    "width": "100%",
                    "zIndex": 1
                }, {}, {});
                flxGapSubRefine1.add();
                flxRefineSkill.add(lblRefineSkill, lblSelectedRefineSkill);
                frmRefineProjects["flxSkillLevelCategories"].add(flxRefineSkill, flxGapSubRefine1);
            }
        } else {
            skillVisible = false;
            frmRefineProjects.flxSkillLevel.setVisibility(false);
        }
    }

    function updateProductCount(prodCount) {
        prodVisible = false;
        frmRefineProjects.flxProducts.setVisibility(false);
    }

    function updateSkillLabel(skillLabel) {
        frmRefineProjects.lblSkillLevel.text = skillLabel;
    }

    function updateCraftLabel(craftLabel) {
        frmRefineProjects.lblTimeReq.text = craftLabel;
    }

    function updateSelectedSkillOrTime(lblSelectedBaseName, lblBaseName, arrTobeChecked, refArr) {
        for (var i = 0; i < refArr.length; i++) {
            for (var j = 0; j < arrTobeChecked.length; j++) {
                if (refArr[i] == arrTobeChecked[j]) {
                    if (j < 10) {
                        j = "0" + j;
                    }
                    frmRefineProjects[lblBaseName + j].skin = "sknLbl24pxRedGothamBook";
                    frmRefineProjects[lblSelectedBaseName + j].setVisibility(true);
                }
            }
        }
    }

    function updateSelectedRefinements(selectedRefinements) {
        if (selectedRefinements.length !== 0) {
            for (var i = 0; i < selectedRefinements.length; i++) {
                var temp = selectedRefinements[i];
                if (temp.attr === "c_ecomCraftTime" || temp.attr === "c_craftTime") {
                    var refSelValues = temp.refValues;
                    updateSelectedSkillOrTime("lblSelectedRefineCraft", "lblRefineCraft", gblRefineCraft, refSelValues);
                } else if (temp.attr === "c_skillLevel" || temp.attr === "c_projectSkillLevelAdult") {
                    var refSelValues = temp.refValues;
                    updateSelectedSkillOrTime("lblSelectedRefineSkill", "lblRefineSkill", gblRefineSkill, refSelValues);
                }
            }
        }
    }

    function showIfFiltersDoNotExist() {
        if (prodVisible == false && craftVisible == false && skillVisible == false) {
            alert("No filters are applicable");
        }
    }

    function showDefaultExpansion() {
        if (latestRefSelected !== "") {
            if (latestRefSelected === "SKILL") {
                showSkillFields();
                hideTimeFields();
            } else {
                showTimeFields();
                hideSkillFields();
            }
        } else {
            hideSkillFields();
            hideTimeFields();
        }
    }

    function showProjSortPage(firstTime) {
        var skillValues = MVCApp.getSortProjectController().getSkillValues();
        var skillLabel = MVCApp.getSortProjectController().getSkillLabel();
        var craftValues = MVCApp.getSortProjectController().getCraftValues();
        var craftLabel = MVCApp.getSortProjectController().getCraftLabel();
        var prodCount = MVCApp.getSortProjectController().getProductCount();
        var selectedRefinements = MVCApp.getSortProjectController().getRefinementsExcludingCgid();
        updateCraftValues(craftValues);
        updateSkillValues(skillValues);
        updateProductCount(prodCount);
        updateSkillLabel(skillLabel);
        updateCraftLabel(craftLabel);
        updateSelectedRefinements(selectedRefinements);
        if (firstTime) {
            show();
        }
        showDefaultExpansion();
        showIfFiltersDoNotExist();
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function onClickCancel() {
        if (!extraFiltersAdded) {
            MVCApp.getProjectsListController().showUI();
        } else {
            MVCApp.getSortProjectController().loadDefaultProjList();
        }
    }

    function clearAllGlobals() {
        extraFiltersAdded = true;
        latestRefSelected = "";
    }

    function bindEvents() {
        frmRefineProjects.flxProducts.onClick = MVCApp.getSortProjectController().goToProductList;
        frmRefineProjects.btnDone.onClick = MVCApp.getSortProjectController().gotToProjectsList;
        frmRefineProjects.btnCancel.onClick = onClickCancel;
        frmRefineProjects.btnClearAll.onClick = MVCApp.getSortProjectController().clearFilter;
        frmRefineProjects.flxSkillLevel.onClick = expandCollapseSkill;
        frmRefineProjects.flxTimeReq.onClick = expandCollapseTime;
        frmRefineProjects.postShow = function() {
            MVCApp.Toolbox.common.destroyStoreLoc();
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Refine Projects: Michaels Mobile App";
            lTealiumTagObj.page_name = "Refine Projects: Michaels Mobile App";
            lTealiumTagObj.page_type = "Filter";
            lTealiumTagObj.page_category_name = "Sort and Refine";
            lTealiumTagObj.page_category = "Sort and Refine";
            TealiumManager.trackView("Refine Projects Screen", lTealiumTagObj);
        };
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        showProjSortPage: showProjSortPage,
        bindEvents: bindEvents,
        clearAllGlobals: clearAllGlobals
    };
});