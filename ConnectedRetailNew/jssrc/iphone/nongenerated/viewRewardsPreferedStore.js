/**
 * PUBLIC
 * This is the view for the Events form.
 * All actions that impact the UI are implemented here.
 * _variable or _functions indicate elements which are not publicly exposed
 */
var MVCApp = MVCApp || {};
MVCApp.RewardsProfilePreferedStoreView = (function() {
    /**
     * PUBLIC
     * Open the Events form
     */
    function show() {
        frmRewardsProfileStep4.flxMainContainer.contentOffset = {
            x: 0,
            y: 0
        };
        frmRewardsProfileStep4.txtHidebox.setFocus(true);
        frmRewardsProfileStep4.txtHidebox.setEnabled(true);
        frmRewardsProfileStep4.textSearch.setFocus(false);
        frmRewardsProfileStep4.textSearch.text = "";
        frmRewardsProfileStep4.show();
    }

    function determineSearch(eventobj) {
        var text = eventobj.text;
        var cityState = /([A-Za-z]+(?: [A-Za-z]+)*),([A-Za-z]{2})/;
        var citySpaceState = /([A-Za-z]+(?: [A-Za-z]+)*),? ([A-Za-z]{2})/;
        var usZip = /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/;
        var canZip = /^([A-Za-z][0-9][A-Za-z])\s*([0-9][A-Za-z][0-9])$/;
        var canZipAlternate = /^([A-Za-z][0-9][A-Za-z][0-9][A-Za-z][0-9])$/;
        if (kony.store.getItem("languageSelected") === "en") {
            if (cityState.test(text.trim()) || citySpaceState.test(text.trim()) || usZip.test(text.trim())) {
                MVCApp.getRewardsProfilePreferedStoreController().initiateSearch(text);
            } else {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.textSearch"), "");
            }
        } else {
            if (cityState.test(text.trim()) || citySpaceState.test(text.trim()) || canZip.test(text.trim()) || canZipAlternate.test(text.trim())) {
                MVCApp.getRewardsProfilePreferedStoreController().initiateSearch(text);
            } else {
                MVCApp.Toolbox.common.customAlertNoTitle(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.textSearch"), "");
            }
        }
    }
    /**
     * PUBLIC
     * Here we define and attach all event handlers.
     */
    function bindEvents() {
        frmRewardsProfileStep4.postShow = function() {
            var lTealiumTagObj = gblTealiumTagObj;
            lTealiumTagObj.page_id = "Rewards Profile Creation: Step 5: Michaels Mobile App";
            lTealiumTagObj.page_name = "Rewards Profile Creation: Step 5: Michaels Mobile App";
            lTealiumTagObj.page_type = "Rewards";
            lTealiumTagObj.page_category_name = "Rewards";
            lTealiumTagObj.page_category = "Rewards";
            TealiumManager.trackView("Rewards Profile Creation Step 5 Screen", lTealiumTagObj);
        };
        frmRewardsProfileStep4.segOptionalStoreResults.onRowClick = clickOptionalResults;
        MVCApp.tabbar.bindEvents(frmRewardsProfileStep4);
        MVCApp.tabbar.bindIcons(frmRewardsProfileStep4, "frmMore");
        frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.onClick = reviewProfile;
        frmRewardsProfileStep4.textSearch.onDone = determineSearch;
        frmRewardsProfileStep4.btnHeaderLeft.onClick = showPreviousScreen;
        frmRewardsProfileStep4.btnClearSearchText.onClick = function() {
            frmRewardsProfileStep4.segOptionalStoreResults.data = [];
            frmRewardsProfileStep4.flxOptionalResultsContainer.isVisible = false;
            frmRewardsProfileStep4.textSearch.text = "";
            //frmRewardsProfileStep4.segStoresListView.data=[];
            frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip");
            //setDefaultFavStore();
        };
        frmRewardsProfileStep4.btnBuyOnline2.onClick = onClickBtnBuyOnline2;
        frmRewardsProfileStep4.btnYes.onClick = navigateToMoreScreen;
        frmRewardsProfileStep4.flxOverlay.onTouchEnd = dummyOverlay;
    }

    function onClickBtnBuyOnline2() {
        MVCApp.getReviewProfileController().updateRewardProfile("step5");
    }

    function navigateToMoreScreen() {
        frmRewardsProfileStep4.flxOverlay.isVisible = false;
        MVCApp.getMoreController().load();
    }

    function dummyOverlay() {
        kony.print("LOG:dummyOverlay-viewRewardsPreferedStore.js-START");
    }

    function updateScreen(editFlag) {
        frmRewardsProfileStep4.segStoresListView.isVisible = true;
        frmRewardsProfileStep4.segOptionalStoreResults.data = [];
        frmRewardsProfileStep4.flxOptionalResultsContainer.isVisible = false;
        frmRewardsProfileStep4.segStoresListView.widgetDataMap = {
            lblAddressLine1: "lbladdress1",
            lblAddressLine2: "lbladdress2",
            lblPhone: "lblPhone",
            lblDistance: "distanceinMiles",
            btnFavorite: "btnFavorite",
        };
        setDefaultFavStore();
        show();
    }

    function setDefaultFavStore(resetFlag) {
        var favStore = "";
        if (resetFlag) {
            favStore = rewardsDefaultFavouriteStore;
        } else {
            favStore = rewardsFavouriteStore;
        }
        var storeData = new MVCApp.data.dataRewardsPreferedStore();
        var segData = [];
        if (resetFlag) {
            segData = storeData.getDefaultFavouriteStoreData();
        } else {
            segData = storeData.getFavouriteStoreData();
        }
        if (segData.length > 0) {
            frmRewardsProfileStep4.flxTapToSelFavStore.isVisible = true;
            frmRewardsProfileStep4.flxLineSeparator.isVisible = false;
            rewardsFavouriteStore = JSON.stringify(segData[0]);
            frmRewardsProfileStep4.segStoresListView.data = segData;
            MVCApp.getRewardsProfilePreferedStoreController().setPreviousLocation(0);
            frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.btnNext");
        } else {
            frmRewardsProfileStep4.flxTapToSelFavStore.isVisible = false;
            frmRewardsProfileStep4.flxLineSeparator.isVisible = true;
            MVCApp.getRewardsProfilePreferedStoreController().setPreviousLocation(null);
            frmRewardsProfileStep4.segStoresListView.data = [];
            frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip");
        }
        frmRewardsProfileStep4.btnBuyOnline2.isVisible = true;
        var editStoreFlag = MVCApp.getRewardsProfilePreferedStoreController().getEditFlag();
        if (editStoreFlag) {
            frmRewardsProfileStep4.btnBuyOnline2.isVisible = false;
            frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.update");
        }
    }

    function showPreviousScreen() {
        var prevForm = kony.application.getPreviousForm();
        var prevFormId = ""
        if (prevForm != undefined && prevForm != null && prevForm != "") {
            prevFormId = prevForm.id;
        }
        var editStoreFlag = MVCApp.getRewardsProfilePreferedStoreController().getEditFlag();
        if (prevFormId == "frmRewardsProfileSkills" || editStoreFlag === false || editStoreFlag === undefined) {
            MVCApp.getRewardsProfileSkillsController().updateData(false);
        } else if (prevFormId == "frmReviewProfile") {
            rewardsFavouriteStore = rewardsDefaultFavouriteStore;
            MVCApp.getReviewProfileController().load();
        }
    }

    function reviewProfile() {
        if (frmRewardsProfileStep4.CopybtnBuyOnline069ef7a9f8bf74c.text == MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmRewardsProfile.lblSkip")) {
            rewardsFavouriteStore = "";
        }
        updateFlag = true;
        rewardsDefaultFavouriteStore = rewardsFavouriteStore;
        MVCApp.getReviewProfileController().load();
    }

    function updateRewardsStoreScreen(results) {
        frmRewardsProfileStep4.segOptionalStoreResults.data = [];
        frmRewardsProfileStep4.flxOptionalResultsContainer.isVisible = false;
        frmRewardsProfileStep4.segStoresListView.widgetDataMap = {
            lblAddressLine1: "lbladdress1",
            lblAddressLine2: "lbladdress2",
            lblPhone: "lblPhone",
            lblDistance: "distanceinMiles",
            btnFavorite: "btnFavorite",
        };
        var listData = results.getListSegmentData();
        var segData = listData.listSegData;
        var resultExist = listData.resultsExist;
        if (resultExist) {
            if (segData.length > 0) {
                frmRewardsProfileStep4.segStoresListView.data = segData;
            } else {
                frmRewardsProfileStep4.segStoresListView.data = [];
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"), "", constants.ALERT_TYPE_INFO, null, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"), "");
            }
            frmRewardsProfileStep4.segStoresListView.isVisible = true;
            frmRewardsProfileStep4.flxTapToSelFavStore.isVisible = true;
            frmRewardsProfileStep4.flxLineSeparator.isVisible = false;
        } else {
            frmRewardsProfileStep4.segStoresListView.isVisible = false;
            frmRewardsProfileStep4.flxTapToSelFavStore.isVisible = false;
            frmRewardsProfileStep4.flxLineSeparator.isVisible = true;
            var suggestedAddress = listData.sugStoreData;
            if (suggestedAddress.length !== 0) {
                var addressResults = [];
                var headerOptResObj = {
                    "lblOptResHeader": MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.DidYouMean"),
                    "header": true
                };
                frmRewardsProfileStep4.segOptionalStoreResults.removeAll();
                frmRewardsProfileStep4.segOptionalStoreResults.data = [];
                frmRewardsProfileStep4.flxOptionalResultsContainer.setVisibility(true);
                headerOptResObj.template = flxOptResHeader;
                addressResults.push(headerOptResObj);
                for (var i = 0; i < suggestedAddress.length; i++) {
                    var newObj = {
                        "lblOptionalResults": suggestedAddress[i].address1 + " " + suggestedAddress[i].city + ", " + suggestedAddress[i].state + " " + suggestedAddress[i].postalcode,
                        "latitude": suggestedAddress[i].latitude,
                        "longitude": suggestedAddress[i].longitude,
                        "zipcode": suggestedAddress[i].postalcode,
                        "header": false
                    };
                    addressResults.push(newObj);
                }
                frmRewardsProfileStep4.segOptionalStoreResults.widgetDataMap = {
                    "lblOptResHeader": "lblOptResHeader",
                    "lblOptionalResults": "lblOptionalResults"
                };
                frmRewardsProfileStep4.segOptionalStoreResults.data = addressResults;
            } else {
                MVCApp.Toolbox.common.customAlert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblNoStoreFoundForYourSearchCountry"), "", constants.ALERT_TYPE_INFO, null, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.signIn.ok"), "");
            }
        }
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function clickOptionalResults() {
        var zipcode = "";
        var selItem = frmRewardsProfileStep4.segOptionalStoreResults.selectedRowItems[0];
        if (selItem) {
            var checkIfHeaderClicked = selItem.header;
            if (!checkIfHeaderClicked) {
                zipcode = selItem.zipcode;
                var searchObj = {
                    "text": zipcode
                };
                determineSearch(searchObj);
            }
        }
    }
    //Here we expose the public variables and functions
    return {
        show: show,
        bindEvents: bindEvents,
        updateScreen: updateScreen,
        updateRewardsStoreScreen: updateRewardsStoreScreen
    };
});