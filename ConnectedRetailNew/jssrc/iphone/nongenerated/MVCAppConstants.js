//Type your code here
var MVCApp = MVCApp || {};
MVCApp.serviceType = {};
MVCApp.serviceEndPoints = {};
MVCApp.serviceConstants = {};
MVCApp.regEx = {};
MVCApp.iPhone = {};
MVCApp.serviceType.imagesearch = "imagesearch";
MVCApp.serviceType.DemandwareService = "DemandwareService";
MVCApp.serviceConstants.getStore = "getStore";
MVCApp.serviceType.Coremetrics = "Coremetrics";
MVCApp.serviceType.Einstein = "Einstein";
MVCApp.serviceType.GetRecommendations = "getRecommendations";
MVCApp.serviceType.checkNearbyProviders = "checkNearbyProviders";
MVCApp.serviceType.MFCustomAnalytics = "MichaelsCustomMetrics";
MVCApp.serviceEndPoints.addCustomReportsToAnalytics = "addMetricsCustom";
MVCApp.serviceEndPoints.searchProject = "searchProject";
MVCApp.serviceEndPoints.fetchNearByProviders = "fetchNearByProviders";
MVCApp.serviceEndPoints.ProjectCategories = "getProjectCategories";
MVCApp.serviceEndPoints.ProductCategories = "getProductCategories";
MVCApp.serviceEndPoints.ProductCategory = "getProductCategory";
MVCApp.serviceEndPoints.searchProduct = "searchProduct";
MVCApp.serviceConstants.defaultProductId = "Categories";
MVCApp.serviceEndPoints.getProductByID = "getProductByID";
MVCApp.serviceConstants.campaignID = "495dca5c0cde6302";
MVCApp.serviceConstants.canadaCampaignID = "7e85b23b556e9e50";
MVCApp.serviceConstants.defaultProjectId = "Projects";
MVCApp.serviceConstants.getProductImagesByID = "getProductImagesByID";
MVCApp.serviceConstants.defaultProjectExpand = "images,set_products,prices,availability";
MVCApp.serviceConstants.shareURL = "https://www.michaels.com/m/";
MVCApp.serviceConstants.shareCanadaURL = "https://canada.michaels.com/m/";
MVCApp.serviceConstants.getProductVarientsById = "getProductVarientsById";
MVCApp.serviceType.BazaarVoiceService = "BazaarVoiceService";
MVCApp.serviceEndPoints.Reviews = "getReviews";
MVCApp.serviceType.LocatorService = "StoreLocatorService";
MVCApp.serviceType.fetchINStoreDetailsByID = "fetchINStoreDetailsByID";
MVCApp.serviceEndPoints.searchByLatLon = "searchByLatlong";
MVCApp.serviceEndPoints.searchStoreByLatLon = "searchStoresByLatLong";
MVCApp.serviceEndPoints.searchByZip = "searchByAddressOrZip";
MVCApp.serviceEndPoints.getStaticContent = "getStaticContent";
MVCApp.serviceConstants.createTermsandConditions = "mobile-termsandconditions";
MVCApp.serviceConstants.writeAReviewTermsandConditions = "mobile-reviewterms";
MVCApp.serviceConstants.privacyPolicy = "mobile-privacypolicy";
MVCApp.serviceConstants.rewardTermsandConditions = "mobile-rewardsterms";
MVCApp.serviceEndPoints.authenticate = "authenticate";
MVCApp.serviceEndPoints.resetPassword = "resetPassword";
MVCApp.serviceEndPoints.logoutUser = "logoutUser";
MVCApp.serviceType.Registration = "Registration";
MVCApp.serviceEndPoints.signUp = "signUp";
MVCApp.serviceEndPoints.updateRegisterUser = "updateRegisterUser";
MVCApp.serviceEndPoints.signInUser = "signInUser";
MVCApp.serviceEndPoints.rewardslanding = "rewardslanding";
MVCApp.serviceConstants.rewardInfoRefine = "fdid=mobile-rewardslanding";
MVCApp.serviceConstants.defaultLoyaltyCardBarcode = "https://bar.kouponmedia.com/barcode/linear.aspx?symbology=128&text-margin=15&bar-height=60&display-text=true&code=";
MVCApp.serviceConstants.mobileRewardsFAQ = "mobile-rewardsfaq";
MVCApp.serviceConstants.rewardsShareURL = "https://www.michaels.com/rewards";
MVCApp.serviceConstants.resetPasswordType = "email";
MVCApp.serviceType.KonyMFDBService = "KonyMFDBService";
MVCApp.serviceEndPoints.fetchStoreWifiDetails = "fetchStoreWifiDetails";
MVCApp.serviceEndPoints.getStoreMapData = "getStoreMapData";
MVCApp.serviceEndPoints.searchContent = "searchContent";
MVCApp.serviceConstants.searchContentRefineString = "fdid=mobile-featuredsearch";
MVCApp.serviceType.getAisleNumbers = "getAisleNumbers";
MVCApp.serviceEndPoints.getAisleNumbersForSubcategory = "getAisleNumbersForSubcategory";
MVCApp.serviceType.fetchWelcomeMessage = "fetchWelcomeMessage";
MVCApp.serviceEndPoints.fetchWelcomeMessageByStoreId = "fetchWelcomeMessageByStoreId";
MVCApp.serviceConstants.aisleNoFilter = "category_id eq '";
MVCApp.serviceConstants.aisleNoCategoryFilter = "and parent_cat_id eq '";
MVCApp.serviceConstants.TealiumAddToList = "Add to My List Conversion From Search";
MVCApp.serviceConstants.TealiumAppSearchInitiation = "App Search Initialisation";
MVCApp.serviceConstants.TealiumAppSearchCompletion = "App Search Completion";
MVCApp.serviceConstants.TealiumAppSearchCancellation = "App SearchCancellation";
MVCApp.serviceConstants.TealiumProductSearchClicks = "Product Search Clicks";
MVCApp.serviceEndPoints.getAisleNumbersForCategory = "getAisleNumbersForCategory";
MVCApp.serviceConstants.buyOnlineLink = "";
MVCApp.serviceConstants.buyOnlineLink_US = "https://www.michaels.com/on/demandware.store/Sites-MichaelsUS-Site/default/Product-Show?pid=";
MVCApp.serviceConstants.buyOnlineLink_en_CA = "https://canada.michaels.com/on/demandware.store/Sites-MichaelsCA-Site/en_CA/Product-Show?pid=";
MVCApp.serviceConstants.buyOnlineLink_fr_CA = "https://canada.michaels.com/on/demandware.store/Sites-MichaelsCA-Site/fr_CA/Product-Show?pid=";
MVCApp.serviceType.getProductDetailsService = "variants";
MVCApp.serviceEndPoints.getProductDetailsOperation = "getproductdetailsbyId";
MVCApp.serviceEndPoints.fetchNearByZip = "fetchNearbyZip";
MVCApp.serviceType.CofactorService = "CofactorService";
MVCApp.serviceEndPoints.getAdDepartments = "getAdDepartments";
MVCApp.serviceEndPoints.getFeaturedListing = "getFeaturedListing";
MVCApp.serviceEndPoints.getListings = "getListings";
MVCApp.serviceType.weeklyAds = "getAdInfoList";
MVCApp.serviceEndPoints.getAllDepartments = "getFeaturedListingAds";
MVCApp.serviceEndPoints.getAllTags = "getAllTags";
MVCApp.serviceEndPoints.getProductCategories = "getProductCategories";
MVCApp.serviceEndPoints.getProductProjectCategory = "getProjectAndProductAISLECat";
MVCApp.serviceEndPoints.imageSearchProduct = "imageSearchProduct";
MVCApp.serviceEndPoints.imageSearchProject = "imageSearchProject";
MVCApp.serviceEndPoints.getWelcomeMessage = "getWelcomeMessage";
MVCApp.serviceType.NewPasswordReset = "DWLookupAndResetPassword";
MVCApp.serviceEndPoints.CustomerSearch = "lookupAndReset";
MVCApp.serviceEndPoints.CustomerSearchCanada = "lookupAndResetCanada";
MVCApp.serviceType.NewPasswordResetCanada = "DWLookupAndREsetPasswordCanada";
MVCApp.serviceConstants.languageArr = ["en", "en_CA", "fr_CA"]; //if french_canada is needed in the app.
//MVCApp.serviceConstants.languageArr=["en","en_CA"];//if french_canada is NOT needed in the app.
MVCApp.serviceConstants.homeContentRefineString = "fdid=mobile-home";
MVCApp.serviceType.events = "CREvents";
MVCApp.serviceEndPoints.storeEvents = "storeEvents";
MVCApp.serviceType.fetchCustomerInfo = "fetchCustomerInfo";
//MVCApp.serviceType.signUpWithRewards= "signUpWithRewards";
MVCApp.serviceType.signUpWithRewards = "loyaltyRegistration";
//MVCApp.serviceType.SigninRewardsProfile= "SigninRewardsProfile";
MVCApp.serviceType.SigninRewardsProfile = "signupforloyaltyservice";
MVCApp.serviceEndPoints.updateStateCoupons = "updateState";
MVCApp.serviceType.CreateProductList = "createProductList";
MVCApp.serviceType.GetProductList = "getProductList";
MVCApp.serviceType.GetProductListItems = "getProductListItems";
MVCApp.serviceType.AddToProductList = "addToProductList";
MVCApp.serviceType.RemoveFromProductList = "removeFromProductList";
MVCApp.serviceType.UpdateToProductList = "updateToProductList";
MVCApp.serviceType.RemoveProductListLooping = "removeProductListLooping";
MVCApp.serviceType.ShoppingList = "shoppinglist";
MVCApp.serviceType.AddProductListLooping = "addProductListLooping";
MVCApp.serviceConstants.fetureProjectsRefineString = "fdid=mobile-featuredprojects";
MVCApp.serviceType.MichaelJavaService = "MichaelTest";
MVCApp.serviceEndPoints.getHelpData = "Help";
MVCApp.serviceType.internalizationorchservice = "internalizationorchservice";
MVCApp.serviceEndPoints.i18orcheservice = "i18orcheservice";
MVCApp.serviceEndPoints.getBlockedStore = "getBlockedStoreList";
MVCApp.serviceEndPoints.getStoreUsingBeconId = "getStoreUsingBeconId";
MVCApp.serviceType.PushNotification = "PushNotification";
MVCApp.serviceEndPoints.addUser = "addUser";
MVCApp.serviceEndPoints.modifyUser = "modifyUser";
MVCApp.serviceType.Appcommerce = "Appcommerce";
MVCApp.serviceEndPoints.getSessionForWebView = "getSessionForWebView";
MVCApp.serviceEndPoints.refresh = "refresh";
MVCApp.serviceEndPoints.logout = "logout";
MVCApp.serviceType.AppcommerceCart = "AppCommerceCart";
MVCApp.serviceEndPoints.getCartDetails = "GetBasketItems";
MVCApp.serviceEndPoints.addCouponToCart = "addCouponsToCart"
MVCApp.serviceEndPoints.CreateBasketForUser = "GetBasketForUser";
MVCApp.serviceEndPoints.CreateBasket = "CreateBasket";
MVCApp.serviceEndPoints.AddItemToBasket = "AddItemToBasket";
MVCApp.serviceEndPoints.deleteItemFromBasket = "deleteItemFromBasket";
MVCApp.serviceType.mergeBasketTosignedUser = "mergeBasketTosignedUser";
MVCApp.serviceEndPoints.merge = "merge";
MVCApp.encryptionConstants = {
    keyalgo: "passphrase",
    subalgo: "aes",
    passphrasehashalgo: "md5",
    padding: "pkcs5",
    mode: "cbc",
    initializationvector: "1234567890123456"
};
MVCApp.regEx.emailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
MVCApp.iPhone.reviewRequestVersion = 10.3;
MVCApp.sfmc = {
    android: {
        appid: "6691c473-bd64-4acd-a290-80f7e76249af",
        accessToken: "2btenrnn535d56zyrhrwyz7f"
    },
    iphone: {
        appid: "2ce19da9-1f98-4999-b586-97c49760cf0c",
        accessToken: "vfuqhs7t9b42q49n7c2y66rf"
    }
};
MVCApp.wit = {
    ai: {
        en: {
            token: "RZCWZMDHMATDTLGMDFO5E4PGQGZG7PKZ"
        },
        fr: {
            token: "5UOBZF2DA23CO4ZJ4IETNHQMUMGZWRVY"
        },
    }
};
MVCApp.beacon = {
    android: {
        appid: "michaelsstores-hnm",
        accessToken: "58c97e10c28ef49d5a2f5919e2c15ab7"
    },
    iphone: {
        appid: "michaelsstores-hnm",
        accessToken: "58c97e10c28ef49d5a2f5919e2c15ab7"
    }
};