var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.StoreLocator = (function(json) {
    var typeOf = "StoreLocator";
    var _data = json || {};
    if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
        prev = 0;
    }
    var prevIndex = null;

    function setData(data) {
        _data = data;
    }

    function getData() {
        return _data;
    }

    function getCollection() {
        var collection = _data.collection || [];
        var centralLocation = _data.center;
        MVCApp.getLocatorMapController().setCentralLocation(centralLocation);
        var collectionArr = [];
        if (MVCApp.Toolbox.common.isArray(collection)) {
            collectionArr = collection;
        } else {
            collectionArr.push(collection);
        }
        if (_data.comingSoon && _data.comingSoon.length > 0) {
            for (var i = 0; i < _data.comingSoon.length; i++) {
                _data.comingSoon[i].isComingSoon = true;
                collectionArr.push({
                    "poi": _data.comingSoon[i]
                });
            }
            collectionArr.sort(function(a, b) {
                return a.poi._distance - b.poi._distance;
            });
        }
        return collectionArr;
    }

    function getNearestStore() {
        var collectionArr = getCollection();
        var objNearest = collectionArr[0] || {};
        var posNearest = objNearest.poi || "";
        return posNearest;
    }

    function getDirection(eventobj) {
        var srcAdd = eventobj.info.srcAdd;
        var destAdd = eventobj.info.destAdd;
        var url = "http://maps.apple.com/?daddr=" + destAdd + "&saddr=" + srcAdd;
        url = decodeURI(url);
        url = url.replace(/%2C/g, ",");
        url = encodeURI(url);
        MVCApp.Toolbox.common.openApplicationURL(url);
    }

    function callToStore(eventobj) {
        var text = eventobj.text;
        try {
            kony.phone.dial(text);
        } catch (e) {
            kony.print("unhandled error from iOS");
            if (e) {
                TealiumManager.trackEvent("Call To Store Exception in Store Locator Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function setFavorite(eventobj) {
        kony.print("info is " + JSON.stringify(eventobj.info));
        var i = eventobj.info.index;
        var dataSeg = frmFindStoreMapView.segStoresListView.data;
        var prev = MVCApp.getLocatorMapController().getPreviousLocation();
        var currData = dataSeg[i];
        if (currData.btnFavorite.skin == "sknBtnIcon36pxRed") {
            currData.btnFavorite.skin = "sknBtnIcon36pxGray";
            currData.lblTapToSelect.isVisible = true;
            kony.store.setItem("myLocationDetails", "");
            MVCApp.Toolbox.Service.setMyLocationFlag(false);
            MVCApp.getLocatorMapController().removeFavorites();
            frmFindStoreMapView.segStoresListView.setDataAt(currData, i, 0);
        } else {
            currData.btnFavorite.skin = "sknBtnIcon36pxRed";
            currData.lblTapToSelect.isVisible = false;
            kony.store.setItem("myLocationDetails", currData.dataForFlex);
            var storeObj = JSON.parse(currData.dataForFlex);
            MVCApp.sendMetricReport(frmFindStoreMapView, [{
                "STORESELECTED": storeObj.clientkey + ""
            }]);
            var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
            cust360Obj.storeid = storeObj.clientkey;
            MVCApp.Customer360.sendInteractionEvent("Favourite Store", cust360Obj);
            MVCApp.Toolbox.Service.setMyLocationFlag(true);
            frmFindStoreMapView.segStoresListView.setDataAt(currData, i, 0);
            MVCApp.getLocatorMapController().updateFavouriteFromList();
            //Invoking DW /pixel service which would set selected fav. store to storeId cookie.
            var cookies = kony.net.getCookies(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "pixel?appsource=mobileapp") || [];
            var httpClient = new kony.net.HttpRequest();
            httpClient.timeout = 5000;
            httpClient.open(constants.HTTP_METHOD_GET, MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.mobileWebUrlMichaels") + "pixel?appsource=mobileapp&stid=" + storeObj.clientkey);
            var cookieString = "";
            for (index = 0; index < cookies.length; index++) {
                kony.print("Cookie is: " + cookies[index]);
                cookieString += cookies[index] + ";";
                //httpClient.setRequestHeader("cookie"  cookies[index]);     
            }
            httpClient.setRequestHeader("Cookie", cookieString);
            httpClient.send();
            //This is where display of Coupons/Events/WeeklyAds logic comes when store is not set
            var whenStoreNotSet = kony.store.getItem("whenStoreNotSet");
            whenStoreNotSet = JSON.parse(whenStoreNotSet);
            if (whenStoreNotSet.doesLoadCouponsWhenStoreNotSet) {
                if (whenStoreNotSet.fromWhichScreen === "coupons") {
                    MVCApp.getHomeController().loadCoupons();
                } else if (whenStoreNotSet.fromWhichScreen === "weeklyAds") {
                    loadWeeklyAds = true;
                    MVCApp.getWeeklyAdHomeController().load();
                } else if (whenStoreNotSet.fromWhichScreen === "weeklyAdsList") {
                    MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId, "", weeklyAdFormID);
                } else if (whenStoreNotSet.fromWhichScreen === "weeklyAdDetail") {
                    MVCApp.getWeeklyAdController().loadWeeklyAds(weeklyAdId, "", weeklyAdFormID, dealId);
                } else if (whenStoreNotSet.fromWhichScreen === "events") {
                    var previousForm = kony.application.getPreviousForm().id;
                    if (previousForm == "frmHome") {
                        MVCApp.getEventsController().load(false);
                    } else {
                        MVCApp.getEventsController().load(true);
                    }
                } else if (whenStoreNotSet.fromWhichScreen === "rewards") {
                    frmObject = frmMore;
                    MVCApp.getMoreController().rewards();
                } else if (whenStoreNotSet.fromWhichScreen === "home") {
                    if (whenStoreNotSet.contentURL) {
                        MVCApp.Toolbox.common.openApplicationURL(whenStoreNotSet.contentURL);
                    }
                } else if (whenStoreNotSet.fromWhichScreen === "storeMapFullStore") {
                    var params = {};
                    params.aisleNo = "";
                    params.productName = "";
                    params.calloutFlag = false;
                    MVCApp.getStoreMapController().load(params);
                } else if (whenStoreNotSet.fromWhichScreen === "storeMapForAisle") {
                    MVCApp.getStoreMapController().showStoreMapWithAisleFor();
                }
                whenStoreNotSet = {
                    "doesLoadCouponsWhenStoreNotSet": false,
                    "fromWhichScreen": ""
                };
                whenStoreNotSet = JSON.stringify(whenStoreNotSet);
                kony.store.setItem("whenStoreNotSet", whenStoreNotSet);
            }
        }
        if (prev !== null && prev !== i) {
            var prevData = dataSeg[prev];
            prevData.btnFavorite.skin = "sknBtnIcon36pxGray";
            prevData.lblTapToSelect.isVisible = true;
            frmFindStoreMapView.segStoresListView.setDataAt(prevData, prev, 0);
        }
        prev = i;
        MVCApp.getLocatorMapController().setPreviousLocation(prev);
    }

    function expandTheList(eventobj) {
        kony.print("info is " + JSON.stringify(eventobj.info));
        var i = eventobj.info.index;
        var visibility = eventobj.info.visible;
        var data = frmFindStoreMapView.segStoresListView.data;
        var currData = data[i];
        currData.flxStoreDetailsExpand.isVisible = visibility;
        currData.flxExpand = {
            isVisible: !visibility,
            onClick: expandTheList,
            info: {
                "index": i,
                "visible": true
            }
        };
        frmFindStoreMapView.segStoresListView.setDataAt(currData, i, 0);
        if (prevIndex !== null && prevIndex !== i) {
            var prevData = data[prevIndex];
            prevData.flxStoreDetailsExpand.isVisible = false;
            prevData.flxExpand = {
                isVisible: true,
                onClick: expandTheList,
                info: {
                    "index": prevIndex,
                    "visible": true
                }
            };
            frmFindStoreMapView.segStoresListView.setDataAt(prevData, prevIndex, 0);
        }
        prevIndex = i;
    }

    function goToStoreMap(eventObj) {
        var info = eventObj.info;
        var params = {};
        params.aisleNo = info.aisle;
        params.productName = MVCApp.getLocatorMapController().getProductName();
        params.calloutFlag = false;
        var dataOfStore = info.dataOfStore;
        dataOfStore = JSON.parse(dataOfStore);
        MVCApp.getStoreMapController().setEntryPoint("ChooseStore");
        MVCApp.getStoreMapController().loadStoreMap(info.storeId, params, dataOfStore);
        MVCApp.getLocatorMapController().setOriginalStoreId(info.storeId);
    }

    function prepareSegmentData(poi, i, isFavourite) {
        var temp1 = {};
        poi.timelastUpdated = new Date().getTime();
        var temp = poi;
        var temp2 = poi;
        temp.dataForFlex = JSON.stringify(poi);
        var srcAddObj = MVCApp.getLocatorMapController().getSourceAddress() || "";
        var srcAdd = srcAddObj.lat + "," + srcAddObj.lon;
        var destAdd = poi.latitude + "," + poi.longitude;
        var isDistanceVisible = false;
        if (srcAddObj !== "") {
            isDistanceVisible = true;
        } else {
            isDistanceVisible = false;
        }
        var favSkin = "sknBtnIcon36pxGray";
        var visibilityOfTapToSelect = true;
        if (isFavourite) {
            favSkin = "sknBtnIcon36pxRed";
            visibilityOfTapToSelect = false;
        }
        temp.lbladdress1 = poi.address1;
        var province = poi.province || "";
        temp.lblNameStore = poi.address2 || "";
        if (poi.country === "US") temp.lbladdress2 = poi.city + ", " + poi.state + " " + poi.postalcode;
        else temp.lbladdress2 = poi.city + ", " + province + " " + poi.postalcode;
        var btnFavouriteVisibility = true;
        if (poi.isComingSoon) {
            btnFavouriteVisibility = false;
            visibilityOfTapToSelect = false;
        }
        temp.lblPhone = {
            text: poi.phone,
            onClick: callToStore
        };
        temp.flxDirections = {
            isVisible: isDistanceVisible
        };
        temp.distanceinMiles = poi._distance + " mi";
        var aisleNo = temp.Location || "";
        var storeId = temp.clientkey;
        temp.btnStoreMap = {
            text: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.Common.btnStoreMap"),
            onClick: goToStoreMap,
            info: {
                "aisle": aisleNo,
                "storeId": storeId,
                "dataOfStore": JSON.stringify(poi)
            }
        };
        temp.btnDirections = {
            text: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.btnDirections"),
            onClick: getDirection,
            info: {
                srcAdd: srcAdd,
                destAdd: destAdd
            }
        };
        temp.lblStoreHours = {
            text: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblStoreHours")
        };
        temp.flxStoreDetailsExpand = {
            isVisible: false
        };
        temp.btnCollapse = {
            isVisible: false
        };
        temp.lblTapToSelect = {
            isVisible: visibilityOfTapToSelect,
            text: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblTapToSelect")
        };
        temp.btnFavorite = {
            isVisible: btnFavouriteVisibility,
            skin: favSkin,
            onClick: setFavorite,
            info: {
                "index": i
            }
        };
        temp.lblStoreHoursNew = {
            text: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmFindStoreMapView.lblStoreHoursSmall"),
            isVisible: true
        };
        temp.btnExpand = {
            text: "D",
            isVisible: true,
            isEnabled: false
        };
        temp.flxExpand = {
            isVisible: true,
            onClick: expandTheList,
            info: {
                "index": i,
                "visible": true
            }
        };
        temp.btnCollapse = {
            text: "U",
            onClick: expandTheList,
            info: {
                "index": i,
                "visible": false
            }
        };
        temp.flxItemLocation = {
            isVisible: true
        };
        if (poi.isComingSoon) {
            temp.lblComingSoon = {
                text: MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.comingSoon")
            }
            temp.flexComingSoon = {
                isVisible: true
            };
            var transformObj = kony.ui.makeAffineTransform();
            transformObj.rotate(-45);
            temp.flexFlag = {
                anchorPoint: {
                    x: 0,
                    y: 1
                },
                transform: transformObj
            };
        }
        var stockOnHand = temp.OnHand;
        if (stockOnHand == null || stockOnHand == undefined) {
            stockOnHand = "";
        } else {
            stockOnHand = parseInt(stockOnHand);
        }
        if (MVCApp.getLocatorMapController().getCheckNearby()) {
            temp.template = flxStoreDetailsNearby;
            temp.lblAvailability = MVCApp.Toolbox.common.getStockStatusOfNearbyStores(stockOnHand);
        } else {
            temp.template = flxStoreDetailsWrap;
            temp.lblAvailability = "";
        }
        return temp;
    }

    function prepareCallOutData(poi, isFavourite, index, partOfResult) {
        var temp = {};
        if (isFavourite) {
            temp.image = "map_pin_red.png";
        } else {
            temp.image = "map_pin_gray.png";
        }
        temp.partOfResult = partOfResult;
        temp.lat = poi.latitude;
        temp.lon = poi.longitude;
        temp.index = index;
        temp.favorite = isFavourite;
        temp.showcallout = false;
        temp.data = JSON.parse(JSON.stringify(poi));
        return temp;
    }
    var suggestedData = [];

    function setSuggestedData(value) {
        suggestedData = value || [];
    }

    function getSuggestedData() {
        return suggestedData;
    }

    function getDataForMap() {
        var collectionArr = getCollection();
        var calloutData = [];
        var listSegData = [];
        var sugStoreData = [];
        var collectionType = "Poi";
        var j = 0;
        var jSeg = 0;
        var resultsCheckNearby = [];
        var locationDetails = {};
        if (MVCApp.Toolbox.Service.getMyLocationFlag() == "true") {
            var myLocationString = kony.store.getItem("myLocationDetails") || "";
            locationDetails = JSON.parse(myLocationString);
            calloutData.push(prepareCallOutData(locationDetails, true, j, false));
            listSegData.push(prepareSegmentData(locationDetails, jSeg, true));
            jFav = j;
            j++;
            jSeg++;
            MVCApp.getLocatorMapController().setPreviousLocation(0);
        }
        if (collectionArr.length !== 0) {
            for (var i = 0; i < collectionArr.length; i++) {
                if (collectionArr[i].suggestedaddress) {
                    collectionType = "Suggested Address";
                    resultsExist = false;
                    sugStoreData.push(collectionArr[i].suggestedaddress);
                } else {
                    resultsExist = true;
                    var poi = collectionArr[i].poi || {};
                    poi.timelastUpdated = new Date().getTime();
                    if (poi.clientkey == locationDetails.clientkey) {
                        var collectionType = "Poi";
                        var poiString = JSON.stringify(poi);
                        kony.store.setItem("myLocationDetails", poiString);
                        var favDataSeg = prepareSegmentData(poi, 0, true);
                        var favDataMap = prepareCallOutData(poi, true, jFav, false);
                        listSegData.splice(0, 1, favDataSeg);
                        calloutData.splice(0, 1, favDataMap);
                    } else {
                        if (MVCApp.getLocatorMapController().getCheckNearby()) {
                            resultsCheckNearby.push(poi.clientkey);
                        }
                        calloutData.push(prepareCallOutData(poi, false, j, true));
                        listSegData.push(prepareSegmentData(poi, jSeg, false));
                        j++;
                        jSeg++;
                    }
                }
            }
            setSuggestedData(sugStoreData);
        } else {
            resultsCheckNearby = [];
            resultsExist = false;
        }
        setResultsForCheckNearby(resultsCheckNearby);
        return {
            calloutData: calloutData,
            listSegData: listSegData,
            resultsExist: resultsExist
        };
    }
    var checkNearbyResults = [];

    function setResultsForCheckNearby(values) {
        checkNearbyResults = values;
    }

    function getResultsForCheckNearby() {
        return checkNearbyResults;
    }
    return {
        typeOf: typeOf,
        getData: getData,
        setData: setData,
        getNearestStore: getNearestStore,
        getDataForMap: getDataForMap,
        prepareCallOutData: prepareCallOutData,
        prepareSegmentData: prepareSegmentData,
        getSuggestedData: getSuggestedData,
        getResultsForCheckNearby: getResultsForCheckNearby
    };
});