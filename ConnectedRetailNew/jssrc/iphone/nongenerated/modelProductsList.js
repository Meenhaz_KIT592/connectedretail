/**
 * PUBLIC
 * This is the model for formProductsList form
 */
var MVCApp = MVCApp || {};
var completeData = [];
var completeResponse = {};
var firstRecordIndex = 0;
var lastRecordIndex = 0;
var gblStoreForSkuSearch = "";
MVCApp.ProductsListModel = (function() {
    var serviceType = "";
    var operationId = "";

    function loadData(callback, requestParams) {
        kony.print("modelProductsList.js");
        /* MVCApp.makeServiceCall( serviceType,operationId,requestParams,
                                function(results)
                                {
           kony.print("Response is "+JSON.stringify(results)); 
           callback(results,requestParams);
           
         },function(error){
         kony.print("Response is "+JSON.stringify(error));
         }
                               );*/
        callback("");
    }

    function showSearchResults(successCallBack, requestParams, serviceName, operation, fromSearch, count, fromSort) {
        kony.print("in search results " + JSON.stringify(requestParams));
        MakeServiceCall(serviceName, operation, requestParams, function(results) {
            //kony.print("response   "+JSON.stringify(results));
            completeResponse = results;
            completeData.push.apply(completeData, results.hits);
            completeResponse["hits"] = [];
            completeResponse["hits"] = completeData;
            firstRecordIndex = lastRecordIndex;
            lastRecordIndex += MVCApp.Service.Constants.Search.showCount;
            var dataProducts = MVCApp.data.ProductsList(results);
            var totalProducts = dataProducts.getTotalResults();
            if (totalProducts == 1) {
                sorryPageFlag = false;
                if (!fromSort) {
                    var queryString = MVCApp.getProductsListController().getQueryString() || "";
                    MVCApp.getPDPController().setQueryString(queryString);
                    var entryType = MVCApp.getProductsListController().getEntryType();
                    MVCApp.getPDPController().setEntryType(entryType);
                    setForPDP(MVCApp.getPDPController().load, requestParams, MVCApp.serviceType.DemandwareService, MVCApp.serviceEndPoints.searchProduct, fromSearch, count, false);
                } else {
                    MVCApp.getProductsListController().setDataForProductList(dataProducts);
                    successCallBack(dataProducts, fromSearch, count);
                }
            } else if (totalProducts == 0) {
                if (voiceSearchFlag) {
                    sorryPageFlag = true;
                    var queryString = MVCApp.getProductsListController().getQueryString() || "";
                    frmProductsList.flxVoiceSearch.lblUtterenace2.text = queryString;
                    frmProductsList.flxVoiceSearch.lblUtterence1.setVisibility(false);
                    frmProductsList.flxVoiceSearch.lblUtterance3.setVisibility(false);
                    frmProductsList.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
                    frmProductsList.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.NoResultsFoundForVoiceSearch");
                    frmProductsList.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
                    MVCApp.Toolbox.common.dismissLoadingIndicator();
                    var productIds = [];
                    MVCApp.getProductsListController().setProductIds(productIds);
                    MVCApp.Customer360.callVoiceSearchResult("failure", "No Products Found", "");
                    frmProductsList.flxVoiceSearch.setVisibility(true);
                    frmProductsList.show();
                } else {
                    sorryPageFlag = false;
                    MVCApp.getProductsListController().setDataForProductList(dataProducts);
                    successCallBack(dataProducts, fromSearch, count);
                }
            } else {
                sorryPageFlag = false;
                MVCApp.getProductsListController().setDataForProductList(dataProducts);
                successCallBack(dataProducts, fromSearch, count);
            }
        }, function(error) {
            if (voiceSearchFlag) {
                sorryPageFlag = true;
                var queryString = MVCApp.getProductsListController().getQueryString() || "";
                frmProductsList.flxVoiceSearch.lblUtterenace2.text = queryString;
                frmProductsList.flxVoiceSearch.lblUtterence1.setVisibility(false);
                frmProductsList.flxVoiceSearch.lblUtterance3.setVisibility(false);
                frmProductsList.flxVoiceSearch.lblVoiceSearchHeading.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.voiceSearchHeadingSorry");
                frmProductsList.flxVoiceSearch.lblSomething.text = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.frmProductsList.noProjectsFound");
                frmProductsList.flxVoiceSearch.lblMicStatusText.text = MVCApp.Toolbox.common.geti18nValueVA("i18.voice.micStatusTapToTryAgain");
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                frmProductsList.flxVoiceSearch.setVisibility(true);
                kony.print("Error is " + JSON.stringify(error));
            } else {
                MVCApp.getProductsListController().setProductIds([]);
                MVCApp.getProductsListController().loadForNoResults(fromSearch);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
                kony.print("Error is " + JSON.stringify(error));
            }
            if (fromSearch) {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                cust360Obj.searchResult = "failure";
                cust360Obj.productsCount = 0;
                cust360Obj.projectsCount = 0;
                cust360Obj.failureCode = error.message || ""; //"No Results Found For search term"
                MVCApp.getProductsListController().setProductIds([]);
                MVCApp.Customer360.sendInteractionEvent("TextSearchResult", cust360Obj);
                TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {
                    conversion_category: "App SearchUnsuccessful",
                    conversion_id: "TextSearch",
                    conversion_action: 2
                });
            }
        });
    }

    function refreshSortpageOnly(successCallBack, requestParams, serviceName, operation, fromSearch, count) {
        kony.print("in search results " + JSON.stringify(requestParams));
        MakeServiceCall(serviceName, operation, requestParams, function(results) {
            //kony.print("response   "+JSON.stringify(results));
            completeResponse = results;
            completeData.push.apply(completeData, results.hits);
            completeResponse["hits"] = [];
            completeResponse["hits"] = completeData;
            firstRecordIndex = lastRecordIndex;
            lastRecordIndex += MVCApp.Service.Constants.Search.showCount;
            var dataProducts = MVCApp.data.ProductsList(results);
            var dataProductsList = dataProducts.getDataForProductList();
            MVCApp.getProductsListController().setDataForProductList(dataProducts);
            successCallBack(false, dataProducts);
        }, function(error) {
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("Error is " + JSON.stringify(error));
        });
    }

    function setForPDP(successCallBack, requestParams, serviceName, operation, fromSearch, count, scanFlag) {
        kony.print("in search results " + JSON.stringify(requestParams));
        MakeServiceCall(serviceName, operation, requestParams, function(results) {
            //kony.print("response   "+JSON.stringify(results));
            var dataProducts = MVCApp.data.ProductsList(results);
            var dataProductsList = dataProducts.getDataForProductList();
            var prodObj = dataProducts.getProductID();
            var prodID = prodObj.productID;
            var defVariantID = prodObj.defaultVariantID;
            var isVariant = prodObj.isVariant;
            var prodIdTracking = "";
            MVCApp.Toolbox.common.clearPdpGlobalVariables();
            MVCApp.getPDPController().setFromBarcode(fromSearch);
            var upcFlag = true;
            if (scanFlag) {
                var cust360Obj = MVCApp.Customer360.getGlobalAttributes();
                var upc = prodObj.upc;
                if (MVCApp.getProductsListController().getEntryType() === "barcodeScan") {
                    if (upc == requestParams.q) {
                        upcFlag = true;
                        MVCApp.sendMetricReport(frmScan, [{
                            "scan": "success"
                        }]);
                        cust360Obj.productid = prodID;
                        cust360Obj.searchResult = "success";
                        TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {
                            conversion_category: "App SearchSuccessful",
                            conversion_id: "BarcodeSearch",
                            conversion_action: 2,
                            barcode_search_upc: requestParams.q,
                            barcode_product_id: prodID,
                            barcode_search_status: "successful"
                        });
                    } else {
                        upcFlag = false;
                        cust360Obj.searchResult = "failure";
                        cust360Obj.failureCode = "upc mismatch";
                        MVCApp.getProductsListController().setEntryType("barcodeScan");
                        MVCApp.sendMetricReport(frmScan, [{
                            "scan": "failure"
                        }]);
                        TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {
                            conversion_category: "App SearchUnSuccessful",
                            conversion_id: "BarcodeSearch",
                            conversion_action: 2,
                            barcode_search_upc: requestParams.q,
                            barcode_product_id: prodID,
                            barcode_search_status: "unsuccessful"
                        });
                        MVCApp.getProductsListController().setProductIds([]);
                    }
                    MVCApp.Customer360.sendInteractionEvent("BarcodeResult", cust360Obj);
                } else {
                    cust360Obj.searchResult = "failure";
                    MVCApp.Customer360.sendInteractionEvent("QRcodeResult", cust360Obj);
                }
            } else {
                if (fromSearch) {
                    if (!defVariantID && defVariantID !== "") {
                        prodIdTracking = defVariantID
                    } else {
                        prodIdTracking = prodID;
                    }
                    var cust3600bj = MVCApp.Customer360.getGlobalAttributes();
                    cust3600bj.productsCount = 1;
                    cust3600bj.projectsCount = 0;
                    cust3600bj.searchResult = "success";
                    cust3600bj.resultId0 = prodIdTracking;
                    MVCApp.Customer360.sendInteractionEvent("TextSearchResult", cust3600bj);
                    var searchTerm = MVCApp.getProductsListController().getQueryString();
                    TealiumManager.trackEvent(MVCApp.serviceConstants.TealiumAppSearchCompletion, {
                        conversion_category: "App SearchSuccessful",
                        conversion_id: "TextSearch",
                        conversion_action: 2,
                        text_search_term: searchTerm,
                        text_search_results: 1,
                        text_search_status: "unsuccessful"
                    });
                }
            }
            if (prodID != "" && upcFlag) {
                var headerText = MVCApp.getProductsListController().getHeader();
                var headerTitle = "";
                var bgImage = "";
                if (headerText == undefined || headerText == "") {
                    headerTitle = "";
                    bgImage = "";
                } else {
                    headerTitle = headerText.name || "";
                    bgImage = headerText.headerImage || "";
                }
                gblStoreForSkuSearch = requestParams.store_id;
                var skuSearch = "skuSearch";
                if (!fromSearch) {
                    var prevName = MVCApp.getProductsListController().getPreviousForm() || "";
                    if (prevName !== "") {
                        skuSearch = skuSearch + " " + prevName;
                    }
                }
                successCallBack(prodID, isVariant, false, [headerTitle, bgImage], skuSearch, defVariantID, true);
            } else {
                gblWhenTheProductScannedIsNotPresent = true;
                MVCApp.getProductsListController().loadForNoResults(true);
                MVCApp.Toolbox.common.dismissLoadingIndicator();
            }
            //successCallBack(false,dataProducts);
        }, function(error) {
            //alert("No Product Found"); 
            MVCApp.getProductsListController().loadForNoResults(true);
            MVCApp.Toolbox.common.dismissLoadingIndicator();
            kony.print("Error is " + JSON.stringify(error));
        });
    }

    function pushRecords(callBack) {
        if (completeResponse["hits"] && completeResponse["hits"].length > lastRecordIndex) {
            firstRecordIndex = lastRecordIndex;
            lastRecordIndex += MVCApp.Service.Constants.Search.showCount;
            var dataProducts = MVCApp.data.ProductsList(completeResponse);
            MVCApp.getProductsListController().setDataForProductList(dataProducts);
            callBack(dataProducts);
        }
    }
    return {
        loadData: loadData,
        showSearchResults: showSearchResults,
        refreshSortpageOnly: refreshSortpageOnly,
        setForPDP: setForPDP,
        pushRecords: pushRecords
    };
});