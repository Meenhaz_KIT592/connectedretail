/**
 * PUBLIC
 * This is the data object for the Events form.
 */
var MVCApp = MVCApp || {};
MVCApp.data = MVCApp.data || {};
MVCApp.data.Events = (function(json) {
    var typeOf = "Events";
    var _data = json || {};
    /**
     * PUBLIC - setter for the model
     * Set data from the JSON retrieved by the model
     */
    function getEventsListData() {
        try {
            var eventsList = _data.hits || [];
            eventsList = MVCApp.Toolbox.common.isArray(eventsList) ? eventsList : [eventsList];
            var processedEventsList = [];
            var eventIdSet = [];
            for (var i = 0; i < eventsList.length; i++) {
                eventIdSet.push(eventsList[i].id);
                var newRecord = {};
                if (eventsList[i].c_active === "true" || eventsList[i].c_active) {
                    var image = eventsList[i].c_image || "";
                    var title = eventsList[i].c_title || "";
                    var eventStartDate = eventsList[i].c_eventStartDate || "";
                    var eventEndDate = eventsList[i].c_eventEndDate || "";
                    var cost = eventsList[i].c_fee || "";
                    var description = eventsList[i].description || "";
                    var eventStartTime = eventsList[i].c_startTime || "";
                    var eventEndTime = eventsList[i].c_endTime || "";
                    var eventDate = "";
                    var eventTime = "";
                    if (eventStartDate !== "" && eventEndDate !== "") {
                        var eventDateAndTime = getEventDateAndTime(eventStartDate, eventEndDate, eventStartTime, eventEndTime);
                        eventDate = eventDateAndTime.date;
                        eventTime = eventDateAndTime.time;
                    }
                    if (image !== "") {
                        image = MVCApp.Toolbox.common.replaceWithHttps(image);
                    }
                    newRecord.eventTilte = title;
                    newRecord.lblEventName = (title.length > 60 ? title.substring(0, 60) + "..." : title) || "";
                    newRecord.imgEvent = {
                        src: image
                    };
                    newRecord.lblEventDate = eventDate;
                    newRecord.lblEventTime = eventTime;
                    newRecord.cost = cost;
                    newRecord.description = description;
                    newRecord.lblChevron = {
                        text: "K",
                        isVisible: true
                    };
                    newRecord.eventIDTrack = eventsList[i].id;
                    processedEventsList.push(newRecord);
                }
            }
            MVCApp.getEventsController().setResultEvents(eventIdSet);
            return processedEventsList;
        } catch (e) {
            kony.print("Exception in getEventsListData" + JSON.stringify(e));
            if (e) {
                TealiumManager.trackEvent("Get Events List Data Exception", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function getEventDateAndTime(startDate, endDate, startTime, endTime) {
        var dateAndTime = {};
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        if (isSameDate(startDate, endDate)) {
            dateAndTime.date = getDateInFormat(startDate);
            dateAndTime.time = startTime + " - " + endTime;
        } else {
            dateAndTime.date = getDateInFormat(startDate) + " " + startTime + " - ";
            dateAndTime.time = getDateInFormat(endDate) + " " + endTime;
        }
        return dateAndTime;
    }

    function isSameDate(day1, day2) {
        return day1.getFullYear() === day2.getFullYear() && day1.getMonth() === day2.getMonth() && day1.getDate() === day2.getDate();
    }

    function getTimein12HourFormat(date) {
        return ((hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours() === 0 ? 12 : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()) + " " + (date.getHours() >= 12 ? "PM" : "AM"));
    }

    function getDateInFormat(date) {
        var day = "";
        var month = "";
        switch (date.getDay()) {
            case 0:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay7");
                break;
            case 1:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay1");
                break;
            case 2:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay2");
                break;
            case 3:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay3");
                break;
            case 4:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay4");
                break;
            case 5:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay5");
                break;
            case 6:
                day = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.tempStoreDetails.lblDay6");
                break;
            default:
                day = "";
        }
        switch (date.getMonth()) {
            case 0:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.January");
                break;
            case 1:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.February");
                break;
            case 2:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18.phone.March");
                break;
            case 3:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.April");
                break;
            case 4:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.May");
                break;
            case 5:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.June");
                break;
            case 6:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.July");
                break;
            case 7:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.August");
                break;
            case 8:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.September");
                break;
            case 9:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.October");
                break;
            case 10:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.November");
                break;
            case 11:
                month = MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.December");
                break;
            default:
                month = "";
        }
        return day + " " + month + " " + date.getDate() + "," + date.getFullYear();
    }
    //Here we expose the public variables and functions
    return {
        typeOf: typeOf,
        getEventsListData: getEventsListData
    };
});