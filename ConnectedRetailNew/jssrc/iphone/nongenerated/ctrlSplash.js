var MVCApp = MVCApp || {};
var gblDeviceFp = "";
var initLaunchFlag = false;
MVCApp.SplashController = (function() {
    var _isInit = false;
    var model = null;
    var view = null;

    function init(theme, locale) {
        if (_isInit === true) return;
        //Init the model which exposes the backend services to this controller
        model = new MVCApp.SplashModel();
        view = new MVCApp.SplashView();
        view.bindEvents();
        _isInit = true;
    }
    /**
     * PUBLIC
     * Open the form.
     */
    function load() {
        init();
        view.show();
    }

    function loadStatupContent() {
        if (MVCApp.Toolbox.Service.getFlagForInstallation() == "true") {
            initLaunchFlag = true;
            kony.store.setItem("firstIntallation", "false");
            MVCApp.getGuideController().load();
        } else {
            MVCApp.getHomeController().showDefaultView();
            MVCApp.Toolbox.common.showLoadingIndicator("");
            getProductsCategory();
        }
    }

    function updateScreen() {
        view.updateScreen();
    }
    return {
        init: init,
        load: load,
        loadStatupContent: loadStatupContent,
        updateScreen: updateScreen
    };
});