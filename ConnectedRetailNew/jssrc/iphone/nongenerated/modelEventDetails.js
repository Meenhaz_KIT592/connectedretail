/**
 * PUBLIC
 * This is the model for Events form
 */
var eventDetails;
var MVCApp = MVCApp || {};
MVCApp.EventDetailsModel = (function() {
    function getEventDetail(callback) {
        var event = MVCApp.getEventDetailsController().getEventDetails() || [];
        //     var storeDetails = JSON.parse(kony.store.getItem("myLocationDetails"));
        var storeDetails = {};
        if (gblInStoreModeDetails && gblInStoreModeDetails.isInStoreModeEnabled && gblInStoreModeDetails.POI) {
            storeDetails = gblInStoreModeDetails.POI;
        } else {
            storeDetails = kony.store.getItem("myLocationDetails");
            if (storeDetails) {
                try {
                    storeDetails = JSON.parse(storeDetails);
                } catch (e) {
                    MVCApp.sendMetricReport(frmHome, [{
                        "Parse_Error_DW": "modelEventDetails.js on getEventDetail for storeDetails:" + JSON.stringify(e)
                    }]);
                    if (e) {
                        TealiumManager.trackEvent("POI Parse Exception in Event Details Service Call Flow", {
                            exception_name: e.name,
                            exception_reason: e.message,
                            exception_trace: e.stack,
                            exception_type: e
                        });
                    }
                }
            }
        }
        eventDetails = event[0];
        if (ifEventDetailDeeplink) {
            ifEventDetailDeeplink = false;
            eventDetails = event;
        }
        if (storeDetails) {
            eventDetails.address1 = storeDetails.address1 || "";
            //     eventDetails.address2 = storeDetails.address2 || "";
            eventDetails.city = storeDetails.city || "";
            eventDetails.state = storeDetails.state || "";
            eventDetails.postalcode = storeDetails.postalcode || "";
            eventDetails.phone = storeDetails.phone || "";
            eventDetails.latitude = storeDetails.latitude || "";
            eventDetails.longitude = storeDetails.longitude || "";
        }
        MVCApp.getEventDetailsController().setEventDetails(eventDetails);
        callback(eventDetails);
    }

    function geoErrorcallback(positionerror) {
        kony.print("@@@ In geoErrorcallback() @@@");
        alert(MVCApp.Toolbox.common.geti18nValueVA("i18n.phone.common.enableLocationService"));
        MVCApp.Toolbox.common.dismissLoadingIndicator();
    }

    function geoSuccesscallback(position) {
        MVCApp.Toolbox.common.dismissLoadingIndicator();
        var currentLat = position.coords.latitude || "";
        var currentLon = position.coords.longitude || "";
        if (currentLat !== "" && currentLon !== "") {
            var source = currentLat + "," + currentLon;
            var destination = MVCApp.getEventDetailsController().getEventDetails() || {};
            var newDestination = "";
            if (destination) {
                //destination =  destination.latitude+","+destination.longitude;
                newDestination = destination.address1 + "," + destination.city + "," + destination.state + " " + destination.postalcode;
            }
            openNativeMap({
                "sourceAddress": source,
                "destinationAddress": newDestination
            });
        }
    }

    function getDirections() {
        MVCApp.Toolbox.common.showLoadingIndicator("");
        var positionoptions = {
            timeout: 5000,
            enableHighAccuracy: true,
            useBestProvider: true
        };
        TealiumManager.trackEvent("Directions Button click From Events Details Page", {
            conversion_category: "Event Details",
            conversion_id: "Directions",
            conversion_action: 2
        });
        try {
            kony.location.getCurrentPosition(geoSuccesscallback, geoErrorcallback, positionoptions);
        } catch (e) {
            kony.print("exception in getCurrentLocation in events");
            if (e) {
                TealiumManager.trackEvent("Get Current Position Exception in Event Details Model Flow", {
                    exception_name: e.name,
                    exception_reason: e.message,
                    exception_trace: e.stack,
                    exception_type: e
                });
            }
        }
    }

    function openNativeMap(address) {
        var sourceAddress = address.sourceAddress;
        var destinationAddress = address.destinationAddress;
        var url = "";
        url = "http://maps.apple.com/?daddr=" + destinationAddress + "&saddr=" + sourceAddress;
        url = decodeURI(url);
        url = url.replace(/%2C/g, ",");
        url = encodeURI(url);
        MVCApp.Toolbox.common.openApplicationURL(url);
    }

    function callStore() {
        var phone = MVCApp.getEventDetailsController().getEventDetails().phone || "";
        if (phone !== "") {
            TealiumManager.trackEvent("Call Phone Number click From Events Details Page", {
                conversion_category: "Event Details",
                conversion_id: "Call Phone Number",
                conversion_action: 2
            });
            try {
                kony.phone.dial(phone);
            } catch (e) {
                kony.print("excpetion in dialing to store");
                if (e) {
                    TealiumManager.trackEvent("Call Store Exception in Event Details Model Flow", {
                        exception_name: e.name,
                        exception_reason: e.message,
                        exception_trace: e.stack,
                        exception_type: e
                    });
                }
            }
        }
    }
    return {
        getEventDetail: getEventDetail,
        getDirections: getDirections,
        callStore: callStore
    };
});