//startup.js
var globalhttpheaders = {};
var appConfig = {
    appId: "ConnectedRetailM",
    appName: "Michaels",
    appVersion: "12.1.3",
    isturlbase: "https://michaels.konycloud.com/services",
    isDebug: false,
    isMFApp: true,
    appKey: "f7a857b80cc12f8f9245e05d9d796e6f",
    appSecret: "918647f650ea9587fbfba55d99e1b4f0",
    serviceUrl: "https://100013766.auth.konycloud.com/appconfig",
    svcDoc: {
        "selflink": "https://100013766.auth.konycloud.com/appconfig",
        "app_version": "1.0",
        "messagingsvc": {
            "appId": "6625ab8d-d14b-4826-81e9-8437a13d1dbd",
            "url": "https://michaels.messaging.konycloud.com/api/v1"
        },
        "integsvc": {
            "Einstein": "https://michaels.konycloud.com/services/Einstein",
            "DemandwareWebService": "https://michaels.konycloud.com/services/DemandwareWebService",
            "shoppinglist": "https://michaels.konycloud.com/services/shoppinglist",
            "internalizationorchservice": "https://michaels.konycloud.com/services/internalizationorchservice",
            "checkNearbyProviders": "https://michaels.konycloud.com/services/checkNearbyProviders",
            "variants": "https://michaels.konycloud.com/services/variants",
            "versionverifyorcservice": "https://michaels.konycloud.com/services/versionverifyorcservice",
            "DemandwareWebServiceForCanada": "https://michaels.konycloud.com/services/DemandwareWebServiceForCanada",
            "DWLookupAndREsetPasswordCanada": "https://michaels.konycloud.com/services/DWLookupAndREsetPasswordCanada",
            "WalletiOS": "https://michaels.konycloud.com/services/WalletiOS",
            "versionverifyorcserviceNew": "https://michaels.konycloud.com/services/versionverifyorcserviceNew",
            "test500": "https://michaels.konycloud.com/services/test500",
            "Registration": "https://michaels.konycloud.com/services/Registration",
            "CofactorService": "https://michaels.konycloud.com/services/CofactorService",
            "getAisleNumbersCanada": "https://michaels.konycloud.com/services/getAisleNumbersCanada",
            "signupforloyaltyservice": "https://michaels.konycloud.com/services/signupforloyaltyservice",
            "vsdemandware": "https://michaels.konycloud.com/services/vsdemandware",
            "WalletLoyalty": "https://michaels.konycloud.com/services/WalletLoyalty",
            "StoreEventsUS": "https://michaels.konycloud.com/services/StoreEventsUS",
            "StoreEventsUSCanada": "https://michaels.konycloud.com/services/StoreEventsUSCanada",
            "LoyaltyCustomerProfileUpdate": "https://michaels.konycloud.com/services/LoyaltyCustomerProfileUpdate",
            "WalletAndroid": "https://michaels.konycloud.com/services/WalletAndroid",
            "AppCommerceCart": "https://michaels.konycloud.com/services/AppCommerceCart",
            "getAisleNumbers": "https://michaels.konycloud.com/services/getAisleNumbers",
            "variantsCanada": "https://michaels.konycloud.com/services/variantsCanada",
            "locatorservices": "https://michaels.konycloud.com/services/locatorservices",
            "imagesearch": "https://michaels.konycloud.com/services/imagesearch",
            "LoyaltyCustomerLookup": "https://michaels.konycloud.com/services/LoyaltyCustomerLookup",
            "LoyaltyServiceRegistration": "https://michaels.konycloud.com/services/LoyaltyServiceRegistration",
            "MichaelTest": "https://michaels.konycloud.com/services/MichaelTest",
            "_internal_logout": "https://michaels.konycloud.com/services/IST",
            "KouponMediaService": "https://michaels.konycloud.com/services/KouponMediaService",
            "RichMessaagePushService": "https://michaels.konycloud.com/services/RichMessaagePushService",
            "PreprocessLoyaltyUpdate": "https://michaels.konycloud.com/services/PreprocessLoyaltyUpdate",
            "internalization": "https://michaels.konycloud.com/services/internalization",
            "shoppinglistCanada": "https://michaels.konycloud.com/services/shoppinglistCanada",
            "PushNotification": "https://michaels.konycloud.com/services/PushNotification",
            "SigninRewardsProfile": "https://michaels.konycloud.com/services/SigninRewardsProfile",
            "KMFAuth": "https://michaels.konycloud.com/services/KMFAuth",
            "rewards": "https://michaels.konycloud.com/services/rewards",
            "BazaarVoiceServiceOrch": "https://michaels.konycloud.com/services/BazaarVoiceServiceOrch",
            "vsdbservice": "https://michaels.konycloud.com/services/vsdbservice",
            "fetchWelcomeMessage": "https://michaels.konycloud.com/services/fetchWelcomeMessage",
            "newCampaign": "https://michaels.konycloud.com/services/newCampaign",
            "EstimoteClound": "https://michaels.konycloud.com/services/EstimoteClound",
            "MichaelsCustomMetrics": "https://michaels.konycloud.com/services/MichaelsCustomMetrics",
            "couponservice": "https://michaels.konycloud.com/services/couponservice",
            "PushTestNotification": "https://michaels.konycloud.com/services/PushTestNotification",
            "DWLookupAndResetPassword": "https://michaels.konycloud.com/services/DWLookupAndResetPassword",
            "BOPIS": "https://michaels.konycloud.com/services/BOPIS",
            "RichPushService": "https://michaels.konycloud.com/services/RichPushService",
            "versionverification": "https://michaels.konycloud.com/services/versionverification",
            "applicationlanuchservices": "https://michaels.konycloud.com/services/applicationlanuchservices",
            "ManageUserforPush": "https://michaels.konycloud.com/services/ManageUserforPush",
            "addCouponToAndroidWallet": "https://michaels.konycloud.com/services/addCouponToAndroidWallet",
            "PushTest": "https://michaels.konycloud.com/services/PushTest",
            "AppCommerceCartCanada": "https://michaels.konycloud.com/services/AppCommerceCartCanada",
            "StoreLocatorService": "https://michaels.konycloud.com/services/StoreLocatorService",
            "Coremetrics": "https://michaels.konycloud.com/services/Coremetrics",
            "voicesearch": "https://michaels.konycloud.com/services/voicesearch",
            "AddCouponToKM": "https://michaels.konycloud.com/services/AddCouponToKM",
            "KonyMFDBService": "https://michaels.konycloud.com/services/KonyMFDBService",
            "getAdInfoList": "https://michaels.konycloud.com/services/getAdInfoList",
            "Wallet": "https://michaels.konycloud.com/services/Wallet",
            "Appcommerce": "https://michaels.konycloud.com/services/Appcommerce",
            "TibcoRewards": "https://michaels.konycloud.com/services/TibcoRewards",
            "DemandwareService": "https://michaels.konycloud.com/services/DemandwareService",
            "imagesearchCA": "https://michaels.konycloud.com/services/imagesearchCA",
            "AddandUpdateWalletToKM": "https://michaels.konycloud.com/services/AddandUpdateWalletToKM",
            "CREvents": "https://michaels.konycloud.com/services/CREvents",
            "ManhattanInventory": "https://michaels.konycloud.com/services/ManhattanInventory",
            "mergeBasketTosignedUser": "https://michaels.konycloud.com/services/mergeBasketTosignedUser",
            "BazaarVoiceService": "https://michaels.konycloud.com/services/BazaarVoiceService",
            "DemandwareServiceCanada": "https://michaels.konycloud.com/services/DemandwareServiceCanada",
            "TibcoServiceForInventory": "https://michaels.konycloud.com/services/TibcoServiceForInventory",
            "RegistrationCanada": "https://michaels.konycloud.com/services/RegistrationCanada"
        },
        "service_doc_etag": "0000017186B6E3A8",
        "appId": "6625ab8d-d14b-4826-81e9-8437a13d1dbd",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "ConnectedRetail",
        "reportingsvc": {
            "session": "https://michaels.konycloud.com/services/IST",
            "custom": "https://michaels.konycloud.com/services/CMS"
        },
        "baseId": "cb138a56-0851-454e-9289-814a80d98530",
        "app_default_version": "1.0",
        "services_meta": {
            "Einstein": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/Einstein"
            },
            "DemandwareWebService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/DemandwareWebService"
            },
            "shoppinglist": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/shoppinglist"
            },
            "internalizationorchservice": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/internalizationorchservice"
            },
            "checkNearbyProviders": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/checkNearbyProviders"
            },
            "variants": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/variants"
            },
            "versionverifyorcservice": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/versionverifyorcservice"
            },
            "DemandwareWebServiceForCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/DemandwareWebServiceForCanada"
            },
            "DWLookupAndREsetPasswordCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/DWLookupAndREsetPasswordCanada"
            },
            "WalletiOS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/WalletiOS"
            },
            "versionverifyorcserviceNew": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/versionverifyorcserviceNew"
            },
            "test500": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/test500"
            },
            "Registration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/Registration"
            },
            "CofactorService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/CofactorService"
            },
            "getAisleNumbersCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/getAisleNumbersCanada"
            },
            "signupforloyaltyservice": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/signupforloyaltyservice"
            },
            "vsdemandware": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/vsdemandware"
            },
            "WalletLoyalty": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/WalletLoyalty"
            },
            "StoreEventsUS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/StoreEventsUS"
            },
            "StoreEventsUSCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/StoreEventsUSCanada"
            },
            "LoyaltyCustomerProfileUpdate": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/LoyaltyCustomerProfileUpdate"
            },
            "WalletAndroid": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/WalletAndroid"
            },
            "AppCommerceCart": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/AppCommerceCart"
            },
            "getAisleNumbers": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/getAisleNumbers"
            },
            "variantsCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/variantsCanada"
            },
            "locatorservices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/locatorservices"
            },
            "imagesearch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/imagesearch"
            },
            "LoyaltyCustomerLookup": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/LoyaltyCustomerLookup"
            },
            "LoyaltyServiceRegistration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/LoyaltyServiceRegistration"
            },
            "MichaelTest": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/MichaelTest"
            },
            "KouponMediaService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/KouponMediaService"
            },
            "RichMessaagePushService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/RichMessaagePushService"
            },
            "PreprocessLoyaltyUpdate": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/PreprocessLoyaltyUpdate"
            },
            "internalization": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/internalization"
            },
            "shoppinglistCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/shoppinglistCanada"
            },
            "PushNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/PushNotification"
            },
            "SigninRewardsProfile": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/SigninRewardsProfile"
            },
            "KMFAuth": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/KMFAuth"
            },
            "rewards": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/rewards"
            },
            "BazaarVoiceServiceOrch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/BazaarVoiceServiceOrch"
            },
            "vsdbservice": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/vsdbservice"
            },
            "fetchWelcomeMessage": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/fetchWelcomeMessage"
            },
            "newCampaign": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/newCampaign"
            },
            "EstimoteClound": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/EstimoteClound"
            },
            "MichaelsCustomMetrics": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/MichaelsCustomMetrics"
            },
            "couponservice": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/couponservice"
            },
            "PushTestNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/PushTestNotification"
            },
            "DWLookupAndResetPassword": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/DWLookupAndResetPassword"
            },
            "BOPIS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/BOPIS"
            },
            "RichPushService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/RichPushService"
            },
            "versionverification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/versionverification"
            },
            "applicationlanuchservices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/applicationlanuchservices"
            },
            "ManageUserforPush": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/ManageUserforPush"
            },
            "addCouponToAndroidWallet": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/addCouponToAndroidWallet"
            },
            "PushTest": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/PushTest"
            },
            "AppCommerceCartCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/AppCommerceCartCanada"
            },
            "StoreLocatorService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/StoreLocatorService"
            },
            "Coremetrics": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/Coremetrics"
            },
            "voicesearch": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/voicesearch"
            },
            "AddCouponToKM": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/AddCouponToKM"
            },
            "KonyMFDBService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/KonyMFDBService"
            },
            "getAdInfoList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/getAdInfoList"
            },
            "Wallet": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/Wallet"
            },
            "Appcommerce": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/Appcommerce"
            },
            "TibcoRewards": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/TibcoRewards"
            },
            "DemandwareService": {
                "type": "integsvc",
                "version": "2.0",
                "url": "https://michaels.konycloud.com/services/DemandwareService"
            },
            "imagesearchCA": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/imagesearchCA"
            },
            "AddandUpdateWalletToKM": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/AddandUpdateWalletToKM"
            },
            "CREvents": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/CREvents"
            },
            "ManhattanInventory": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/ManhattanInventory"
            },
            "mergeBasketTosignedUser": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/mergeBasketTosignedUser"
            },
            "BazaarVoiceService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/BazaarVoiceService"
            },
            "DemandwareServiceCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/DemandwareServiceCanada"
            },
            "TibcoServiceForInventory": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/TibcoServiceForInventory"
            },
            "RegistrationCanada": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://michaels.konycloud.com/services/RegistrationCanada"
            }
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "ServiceResponse", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 7200
    })
};

function appInit(params) {
    skinsInit();
    kony.application.setCheckBoxSelectionImageAlignment(constants.CHECKBOX_SELECTION_IMAGE_ALIGNMENT_RIGHT);
    kony.application.setDefaultTextboxPadding(false);
    kony.application.setRespectImageSizeForImageWidgetAlignment(true);
    initializeMVCTemplates();
    initializeUserWidgets();
    initializeTemp0cc7cdec9374c4c();
    initializetempCategories();
    initializetempCoupon();
    initializetempDisclaimer();
    initializetempEvents();
    initializetempHazardousPDP();
    initializetempHomeCarousel();
    initializetempMenuList();
    initializetempMenuListUS();
    initializetempNewFeature();
    initializetempPDPProductList();
    initializetempProdList();
    initializetempProductList();
    initializetempProjInstructions();
    initializetempProjectList();
    initializetempProjectMarket();
    initializetempProjectsCarousel();
    initializetempReviewProfile();
    initializetempReviews();
    initializetempRewardProfileChild();
    initializetempRewardStoreLocator();
    initializetempRewardsInfo();
    initializetempRewardsProfileInterests();
    initializetempRewardsProfileSkills();
    initializetempSearchOffers();
    initializetempSearchResults();
    initializetempSelectState();
    initializetempShoppingList();
    initializetempShoppingListHeader();
    initializetempStoreDetails();
    initializetempStoreDetailsCheckNearBy();
    initializetempStoreLocatorOptResHeader();
    initializetempStoreLocatorOptionalResults();
    initializetempSurvey();
    initializetempWeeklyAd();
    initializetempWeeklyAdHeader();
    initializetempWeeklyAdHome();
    initializetempHeaderWlogo();
    frmCartViewGlobals();
    frmChatBoxGlobals();
    frmContactUsGlobals();
    frmCouponsGlobals();
    frmCreateAccountGlobals();
    frmCreateAccountStep2Globals();
    frmCustomerCareGlobals();
    frmEventDetailGlobals();
    frmEventsGlobals();
    frmFindStoreMapViewGlobals();
    frmForgotPwdGlobals();
    frmGuideGlobals();
    frmHelpGlobals();
    frmHomeGlobals();
    frmImageOverlayGlobals();
    frmImageSearchGlobals();
    frmLocatorMapGlobals();
    frmMoreGlobals();
    frmMyAccountGlobals();
    frmMyRewardsGlobals();
    frmPDPGlobals();
    frmPJDPGlobals();
    frmPinchandZoomGlobals();
    frmProductCategoryGlobals();
    frmProductCategoryMoreGlobals();
    frmProductsGlobals();
    frmProductsListGlobals();
    frmProjectsGlobals();
    frmProjectsCategoryGlobals();
    frmProjectsListGlobals();
    frmRefineProjectsGlobals();
    frmRegionsGlobals();
    frmReviewProfileGlobals();
    frmReviewsGlobals();
    frmRewardsJoinGlobals();
    frmRewardsProfileAddChildGlobals();
    frmRewardsProfileInterestsGlobals();
    frmRewardsProfileSkillsGlobals();
    frmRewardsProfileStep1Globals();
    frmRewardsProfileStep2Globals();
    frmRewardsProfileStep3Globals();
    frmRewardsProfileStep4Globals();
    frmRewardsProgramGlobals();
    frmRewardsProgramInfoGlobals();
    frmScanGlobals();
    frmSelectStateGlobals();
    frmSettingsGlobals();
    frmShoppingListGlobals();
    frmSignInGlobals();
    frmSortAndRefineGlobals();
    frmSplashGlobals();
    frmStoreMapGlobals();
    frmVoiceSearchVideosGlobals();
    frmWebViewGlobals();
    frmWeeklyAdGlobals();
    frmWeeklyAdDetailGlobals();
    frmWeeklyAdHomeGlobals();
    setAppBehaviors();
};
kony.visualizer.actions.postAppInitCallBack = function(eventObj) {
    return AS_AppEvents_e03ca6ab607143009126168263169d29(eventObj);
};

function themeCallBack() {
    initializeGlobalVariables();
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        preappinit: AS_AppEvents_5fdabb4cf27c4bfd8fb014d69af44ddb,
        appservice: function(eventObject) {
            var value = AS_AppEvents_b698c148e02c43cfa4130ee982bf7512(eventObject);
            return value;
        },
        postappinit: kony.visualizer.actions.postAppInitCallBack,
        showstartupform: function() {
            frmHome.show();
        }
    });
};

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};

function loadResources() {
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//This is the entry point for the application.When Locale comes,Local API call will be the entry point.
kony.i18n.setDefaultLocaleAsync("en", onSuccess, onFailure, null);
kony.print = function() {
    return;
};