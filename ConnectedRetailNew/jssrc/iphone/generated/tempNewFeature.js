function initializetempNewFeature() {
    FlxSegmentData = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlxSegmentData",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    FlxSegmentData.setDefaultUnit(kony.flex.DP);
    var lblTitle = new kony.ui.Label({
        "id": "lblTitle",
        "isVisible": true,
        "left": "5%",
        "right": "5%",
        "skin": "sknLblImageSearchPopupMessage",
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSpace = new kony.ui.Label({
        "height": "20dp",
        "id": "lblSpace",
        "isVisible": true,
        "left": "5%",
        "right": "5%",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 12
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlxSegmentData.add(lblTitle, lblSpace);
}