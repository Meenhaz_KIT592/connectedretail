function addWidgetsfrmReviews() {
    frmReviews.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxWriteReviewWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxWriteReviewWrap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%"
    }, {}, {});
    flxWriteReviewWrap.setDefaultUnit(kony.flex.DP);
    var CopyflxWriteReview0a8d0d1710e5c4c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyflxWriteReview0a8d0d1710e5c4c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxWriteReview0a8d0d1710e5c4c.setDefaultUnit(kony.flex.DP);
    var CopyLabel0488e83714d4d46 = new kony.ui.Label({
        "centerX": "50%",
        "height": "100%",
        "id": "CopyLabel0488e83714d4d46",
        "isVisible": true,
        "skin": "sknLblGothamBold24pxWhite",
        "text": "Tekwriterusa GelWriter   Gel Pens",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxWriteReview0a8d0d1710e5c4c.add(CopyLabel0488e83714d4d46);
    var flxWriteReview = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxWriteReview",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWriteReview.setDefaultUnit(kony.flex.DP);
    var flxItemStock = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxItemStock",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxItemStock.setDefaultUnit(kony.flex.DP);
    var flxStarRating = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStarRating",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "90dp"
    }, {}, {});
    flxStarRating.setDefaultUnit(kony.flex.DP);
    var lblStar1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar2",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar3",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar4",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblStar5",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "18dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgStar1 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStar1",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "16dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar2 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStar2",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "16dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar3 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStar3",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "16dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar4 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStar4",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "16dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar5 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgStar5",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "16dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStarRating.add(lblStar1, lblStar2, lblStar3, lblStar4, lblStar5, imgStar1, imgStar2, imgStar3, imgStar4, imgStar5);
    var lblReviewCount = new kony.ui.Label({
        "height": "100%",
        "id": "lblReviewCount",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium28px818181",
        "text": "(88)",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxItemStock.add(flxStarRating, lblReviewCount);
    var lblDropdown = new kony.ui.Label({
        "centerX": "93%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblDropdown",
        "isVisible": true,
        "skin": "sknLblIconBlue34px",
        "text": "D",
        "top": "0dp",
        "width": "8%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnWriteAReview = new kony.ui.Button({
        "focusSkin": "sknGothamBtnBg28pxBlueBoldFOC",
        "height": "100%",
        "id": "btnWriteAReview",
        "isVisible": true,
        "right": "3%",
        "skin": "sknGothamBtnBg28pxBlueBold",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.btnWriteAReview"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 9, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxWriteReview.add(flxItemStock, lblDropdown, btnWriteAReview);
    var flxWriteReviewDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxWriteReviewDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWriteReviewDetails.setDefaultUnit(kony.flex.DP);
    var CopyflxLine0d1181b539b3e49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyflxLine0d1181b539b3e49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque10",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine0d1181b539b3e49.setDefaultUnit(kony.flex.DP);
    CopyflxLine0d1181b539b3e49.add();
    var lblOverallRating = new kony.ui.Label({
        "id": "lblOverallRating",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.overallRating"),
        "top": "10dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var FlexContainer0220d26e5b56d4e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "FlexContainer0220d26e5b56d4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0220d26e5b56d4e.setDefaultUnit(kony.flex.DP);
    var CopyflxStarRating0031da7081ec342 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "CopyflxStarRating0031da7081ec342",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%"
    }, {}, {});
    CopyflxStarRating0031da7081ec342.setDefaultUnit(kony.flex.DP);
    var lblRating1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRating1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRating2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRating2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRating3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRating3",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRating4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRating4",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRating5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRating5",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIconStarLrg",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxStarRating0031da7081ec342.add(lblRating1, lblRating2, lblRating3, lblRating4, lblRating5);
    var flxRatingDesc = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRatingDesc",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    flxRatingDesc.setDefaultUnit(kony.flex.DP);
    var lblRating = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblRating",
        "isVisible": true,
        "left": "0dp",
        "right": "40dp",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.excellent"),
        "top": "0dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgRating = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgRating",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnRating = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnRating",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxRatingDesc.add(lblRating, imgRating, btnRating);
    FlexContainer0220d26e5b56d4e.add(CopyflxStarRating0031da7081ec342, flxRatingDesc);
    var lblReviewTitle1 = new kony.ui.Label({
        "id": "lblReviewTitle1",
        "isVisible": true,
        "left": "3.02%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.reviewTitle"),
        "top": "0dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxFirstName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxFirstName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxFirstName.setDefaultUnit(kony.flex.DP);
    var txtReviewTitle = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "40dp",
        "id": "txtReviewTitle",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmReviews.exampleReviewTitle"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextBoxReviewsPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearTitleText = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnClearTitleText",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxFirstName.add(txtReviewTitle, btnClearTitleText);
    var lblReview = new kony.ui.Label({
        "id": "lblReview",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.review"),
        "top": "20dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxReviewDesc = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxReviewDesc",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxReviewDesc.setDefaultUnit(kony.flex.DP);
    var btnTextReview = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnTextReview",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 2, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var txtReview = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTxtArea",
        "height": "100%",
        "id": "txtReview",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "numberOfVisibleLines": 3,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmReviews.exampleReview"),
        "skin": "sknTxtArea",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [2, 2, 2, 2],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTAREA_KEYBOARD_LABEL_DONE,
        "pasteboardType": constants.TEXTAREA_PASTE_BOARD_TYPE_DEFAULT,
        "showCloseButton": true,
        "showProgressIndicator": false,
        "placeholderSkin": "sknTextAreaReviewsPlaceholder"
    });
    var btnClearReviews = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIconGraySmall",
        "height": "37dp",
        "id": "btnClearReviews",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknBtnIconGraySmall",
        "text": "X",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 2, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxReviewDesc.add(btnTextReview, txtReview, btnClearReviews);
    var lblNickName = new kony.ui.Label({
        "id": "lblNickName",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.nickName"),
        "top": "20dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxNickName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxNickName.setDefaultUnit(kony.flex.DP);
    var txtNickName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "40dp",
        "id": "txtNickName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmReviews.exampleNickName"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextBoxReviewsPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearNickNameText = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnClearNickNameText",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxNickName.add(txtNickName, btnClearNickNameText);
    var CopyLabel0384ddd6b22ab40 = new kony.ui.Label({
        "id": "CopyLabel0384ddd6b22ab40",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.location"),
        "top": "20dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxLocation.setDefaultUnit(kony.flex.DP);
    var txtLocation = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "40dp",
        "id": "txtLocation",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmReviews.exampleCity"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextBoxReviewsPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearLocationText = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnClearLocationText",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxLocation.add(txtLocation, btnClearLocationText);
    var lblEmailReview = new kony.ui.Label({
        "id": "lblEmailReview",
        "isVisible": true,
        "left": "2.99%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.email"),
        "top": "20dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxEmail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxEmail.setDefaultUnit(kony.flex.DP);
    var txtEmail = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "40dp",
        "id": "txtEmail",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmReviews.exampleMail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextBoxReviewsPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearEmailText = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnClearEmailText",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxEmail.add(txtEmail, btnClearEmailText);
    var lblBottomMessage = new kony.ui.Label({
        "id": "lblBottomMessage",
        "isVisible": false,
        "left": "2.54%",
        "skin": "sknLblGothamBold28pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.clickAgree"),
        "top": "20dp",
        "width": "97%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxTermsConditions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35dp",
        "id": "flxTermsConditions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "18dp",
        "width": "83%",
        "zIndex": 1
    }, {}, {});
    flxTermsConditions.setDefaultUnit(kony.flex.DP);
    var rtTermsandConditions = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "rtTermsandConditions",
        "isVisible": true,
        "left": "59dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.termsCondition"),
        "top": "5dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheckAgree = new kony.ui.Image2({
        "centerY": "46%",
        "height": "14dp",
        "id": "imgCheckAgree",
        "isVisible": true,
        "left": "36dp",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "top": "0dp",
        "width": "12dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAgree = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "btnAgree",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxTermsConditions.add(rtTermsandConditions, imgCheckAgree, btnAgree);
    var flxEmailReceiveFuture = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxEmailReceiveFuture",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxEmailReceiveFuture.setDefaultUnit(kony.flex.DP);
    var rtAge = new kony.ui.RichText({
        "id": "rtAge",
        "isVisible": true,
        "left": "0dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.emailCommunicationMsg"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxEmailReceiveFuture.add(rtAge);
    var btnSubmit = new kony.ui.Button({
        "bottom": "7dp",
        "centerX": "50%",
        "focusSkin": "sknBtnRedGothamBook",
        "height": "43dp",
        "id": "btnSubmit",
        "isVisible": true,
        "skin": "sknBtnRedGothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.postReview"),
        "top": "7dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxWriteReviewDetails.add(CopyflxLine0d1181b539b3e49, lblOverallRating, FlexContainer0220d26e5b56d4e, lblReviewTitle1, flxFirstName, lblReview, flxReviewDesc, lblNickName, flxNickName, CopyLabel0384ddd6b22ab40, flxLocation, lblEmailReview, flxEmail, lblBottomMessage, flxTermsConditions, flxEmailReceiveFuture, btnSubmit);
    var flxCollapsible = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "151dp",
        "id": "flxCollapsible",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCollapsible.setDefaultUnit(kony.flex.DP);
    var flxRating5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20dp",
        "id": "flxRating5",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxRating5.setDefaultUnit(kony.flex.DP);
    var Label03dd5efc244ce41 = new kony.ui.Label({
        "height": "100%",
        "id": "Label03dd5efc244ce41",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium28px818181",
        "text": "5",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel039554cf7d3d44f = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel039554cf7d3d44f",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon18px9b9b9b",
        "text": "Y",
        "top": "0dp",
        "width": "4%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var FlexContainer0954143aa96504c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "10dp",
        "id": "FlexContainer0954143aa96504c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0954143aa96504c.setDefaultUnit(kony.flex.DP);
    var flxRatingBar5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRatingBar5",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxRatingBar5.setDefaultUnit(kony.flex.DP);
    flxRatingBar5.add();
    FlexContainer0954143aa96504c.add(flxRatingBar5);
    var lblRatingBar5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRatingBar5",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblGothamMedium24px818181",
        "text": "0",
        "top": "0dp",
        "width": "9%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRating5.add(Label03dd5efc244ce41, CopyLabel039554cf7d3d44f, FlexContainer0954143aa96504c, lblRatingBar5);
    var flxRating4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20dp",
        "id": "flxRating4",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxRating4.setDefaultUnit(kony.flex.DP);
    var CopyLabel0517d3dba9ba749 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0517d3dba9ba749",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium28px818181",
        "text": "4",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel0138ac7aabb374b = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0138ac7aabb374b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon18px9b9b9b",
        "text": "Y",
        "top": "0dp",
        "width": "4%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyFlexContainer0f52ace05ced842 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0f52ace05ced842",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0f52ace05ced842.setDefaultUnit(kony.flex.DP);
    var flxRatingBar4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRatingBar4",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxRatingBar4.setDefaultUnit(kony.flex.DP);
    flxRatingBar4.add();
    CopyFlexContainer0f52ace05ced842.add(flxRatingBar4);
    var lblRatingBar4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRatingBar4",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblGothamMedium24px818181",
        "text": "0",
        "top": "0dp",
        "width": "9%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRating4.add(CopyLabel0517d3dba9ba749, CopyLabel0138ac7aabb374b, CopyFlexContainer0f52ace05ced842, lblRatingBar4);
    var flxRating3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20dp",
        "id": "flxRating3",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxRating3.setDefaultUnit(kony.flex.DP);
    var CopyLabel05f91d46308de42 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel05f91d46308de42",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium28px818181",
        "text": "3",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel0dfa329a6040d4b = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0dfa329a6040d4b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon18px9b9b9b",
        "text": "Y",
        "top": "0dp",
        "width": "4%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyFlexContainer0cb81019ca0a140 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0cb81019ca0a140",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0cb81019ca0a140.setDefaultUnit(kony.flex.DP);
    var flxRatingBar3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRatingBar3",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxRatingBar3.setDefaultUnit(kony.flex.DP);
    flxRatingBar3.add();
    CopyFlexContainer0cb81019ca0a140.add(flxRatingBar3);
    var lblRatingBar3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRatingBar3",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblGothamMedium24px818181",
        "text": "0",
        "top": "0dp",
        "width": "9%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRating3.add(CopyLabel05f91d46308de42, CopyLabel0dfa329a6040d4b, CopyFlexContainer0cb81019ca0a140, lblRatingBar3);
    var flxRating2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20dp",
        "id": "flxRating2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxRating2.setDefaultUnit(kony.flex.DP);
    var CopyLabel0c4d4fcf5fd7940 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0c4d4fcf5fd7940",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium28px818181",
        "text": "2",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel063927a20e13443 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel063927a20e13443",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon18px9b9b9b",
        "text": "Y",
        "top": "0dp",
        "width": "4%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyFlexContainer02cbadece7e4c43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer02cbadece7e4c43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer02cbadece7e4c43.setDefaultUnit(kony.flex.DP);
    var flxRatingBar2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRatingBar2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxRatingBar2.setDefaultUnit(kony.flex.DP);
    flxRatingBar2.add();
    CopyFlexContainer02cbadece7e4c43.add(flxRatingBar2);
    var lblRatingBar2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRatingBar2",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblGothamMedium24px818181",
        "text": "0",
        "top": "0dp",
        "width": "9%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRating2.add(CopyLabel0c4d4fcf5fd7940, CopyLabel063927a20e13443, CopyFlexContainer02cbadece7e4c43, lblRatingBar2);
    var flxRating1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "20dp",
        "id": "flxRating1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxRating1.setDefaultUnit(kony.flex.DP);
    var CopyLabel027bc9d67cc454e = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel027bc9d67cc454e",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium28px818181",
        "text": "1",
        "top": "0dp",
        "width": "5%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel0d358c4f8b0714d = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0d358c4f8b0714d",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblIcon18px9b9b9b",
        "text": "Y",
        "top": "0dp",
        "width": "4%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyFlexContainer0400e688b42c547 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0400e688b42c547",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "width": "78%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0400e688b42c547.setDefaultUnit(kony.flex.DP);
    var flxRatingBar1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRatingBar1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxRatingBar1.setDefaultUnit(kony.flex.DP);
    flxRatingBar1.add();
    CopyFlexContainer0400e688b42c547.add(flxRatingBar1);
    var lblRatingBar1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblRatingBar1",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblGothamMedium24px818181",
        "text": "0",
        "top": "0dp",
        "width": "9%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRating1.add(CopyLabel027bc9d67cc454e, CopyLabel0d358c4f8b0714d, CopyFlexContainer0400e688b42c547, lblRatingBar1);
    var flxSort = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSort",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSort.setDefaultUnit(kony.flex.DP);
    var Label0b0f9494dcfa949 = new kony.ui.Label({
        "height": "100%",
        "id": "Label0b0f9494dcfa949",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamBold28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.sort"),
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblDropdown0393b7cc335b345 = new kony.ui.Label({
        "height": "100%",
        "id": "CopylblDropdown0393b7cc335b345",
        "isVisible": true,
        "right": "3%",
        "skin": "sknLblIcon34px333333",
        "text": " D",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lstbxReviewSort = new kony.ui.ListBox({
        "focusSkin": "sknListboxGothamRegular24pxBlack",
        "height": "100%",
        "id": "lstbxReviewSort",
        "isVisible": true,
        "masterData": [
            ["mstRecent", kony.i18n.getLocalizedString("i18n.phone.frmReviews.lb1MostRecent")],
            ["lwtohgh", kony.i18n.getLocalizedString("i18n.phone.frmReviews.lb2RatingsLowtoHigh")],
            ["hghtolw", kony.i18n.getLocalizedString("i18n.phone.frmReviews.lb3RatingsHightoLow")]
        ],
        "right": "20dp",
        "selectedKey": "hghtolw",
        "selectedKeyValue": ["hghtolw", "Ratings High to Low    "],
        "skin": "sknListboxGothamRegular24pxBlack",
        "top": "0dp",
        "width": "78%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "dropDownImage": "transparent.png",
        "groupCells": false,
        "viewConfig": {
            "toggleViewConfig": {
                "viewStyle": constants.LISTBOX_TOGGLE_VIEW_STYLE_PLAIN
            }
        },
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    flxSort.add(Label0b0f9494dcfa949, CopylblDropdown0393b7cc335b345, lstbxReviewSort);
    flxCollapsible.add(flxRating5, flxRating4, flxRating3, flxRating2, flxRating1, flxSort);
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque10",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxWriteReviewWrap.add(CopyflxWriteReview0a8d0d1710e5c4c, flxWriteReview, flxWriteReviewDetails, flxCollapsible, flxLine);
    var segReviews = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblReviewComment": "Review Comment",
            "lblReviewDate": "Date",
            "lblReviewTitle": "Title",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y",
            "rchtxtNameLoc": "<a href=\"javascript:void()\">Name</a> Place"
        }, {
            "lblReviewComment": "Review Comment",
            "lblReviewDate": "Date",
            "lblReviewTitle": "Title",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y",
            "rchtxtNameLoc": "<a href=\"javascript:void()\">Name</a> Place"
        }, {
            "lblReviewComment": "Review Comment",
            "lblReviewDate": "Date",
            "lblReviewTitle": "Title",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y",
            "rchtxtNameLoc": "<a href=\"javascript:void()\">Name</a> Place"
        }, {
            "lblReviewComment": "Review Comment",
            "lblReviewDate": "Date",
            "lblReviewTitle": "Title",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y",
            "rchtxtNameLoc": "<a href=\"javascript:void()\">Name</a> Place"
        }, {
            "lblReviewComment": "Review Comment",
            "lblReviewDate": "Date",
            "lblReviewTitle": "Title",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y",
            "rchtxtNameLoc": "<a href=\"javascript:void()\">Name</a> Place"
        }, {
            "lblReviewComment": "Review Comment",
            "lblReviewDate": "Date",
            "lblReviewTitle": "Title",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y",
            "rchtxtNameLoc": "<a href=\"javascript:void()\">Name</a> Place"
        }],
        "groupCells": false,
        "id": "segReviews",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "segRowTrans",
        "rowSkin": "segRowTrans",
        "rowTemplate": flxReviewContainerWrap,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "00000050",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxItemStock": "flxItemStock",
            "flxReivewerWrap": "flxReivewerWrap",
            "flxReviewContainer": "flxReviewContainer",
            "flxReviewContainerWrap": "flxReviewContainerWrap",
            "flxSpacer": "flxSpacer",
            "flxStarRating": "flxStarRating",
            "lblReviewComment": "lblReviewComment",
            "lblReviewDate": "lblReviewDate",
            "lblReviewTitle": "lblReviewTitle",
            "lblStar1": "lblStar1",
            "lblStar2": "lblStar2",
            "lblStar3": "lblStar3",
            "lblStar4": "lblStar4",
            "lblStar5": "lblStar5",
            "rchtxtNameLoc": "rchtxtNameLoc"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    var showMoreReviews = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "30dp",
        "id": "showMoreReviews",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.ShowMoreProducts"),
        "top": "10dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    flxMainContainer.add(flxWriteReviewWrap, segReviews, showMoreReviews, flxTopSpacerDND);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmReviews.reviews"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var Label0ee00528ffdfe4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0ee00528ffdfe4c",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, Label0ee00528ffdfe4c, btnHeaderLeft);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexOverLayBgTrans",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    flxOverlay.add();
    var km959715e4d1d4d5a961ab8cace299358 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km959715e4d1d4d5a961ab8cace299358.setDefaultUnit(kony.flex.DP);
    var km383348b2be84cfc998496b23dbe9c1e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km383348b2be84cfc998496b23dbe9c1e.setDefaultUnit(kony.flex.DP);
    var km62891185f0a43b59d758e48390e296a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km35c5afe1a5f482e97d763198649cb34 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km383348b2be84cfc998496b23dbe9c1e.add(km62891185f0a43b59d758e48390e296a, km35c5afe1a5f482e97d763198649cb34);
    var kmb0c316a5a15418ebb6c6a5d55d9ce01 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb0c316a5a15418ebb6c6a5d55d9ce01.setDefaultUnit(kony.flex.DP);
    var kma6ed0bd38214d5cb4c03b15babd29d9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km3af9827bdb04a4a9e7696ca1204d481 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmb868c96ce9d4f84a9341086e3f62c35 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb0c316a5a15418ebb6c6a5d55d9ce01.add(kma6ed0bd38214d5cb4c03b15babd29d9, km3af9827bdb04a4a9e7696ca1204d481, kmb868c96ce9d4f84a9341086e3f62c35);
    var kma1eba3c55db42a49196ee6bb8917215 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma1eba3c55db42a49196ee6bb8917215.setDefaultUnit(kony.flex.DP);
    var km2a69c473da8498a8c1f8d0fb3d0fa9c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km8f41b4026024729a2a118eab53eb258 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kma1eba3c55db42a49196ee6bb8917215.add(km2a69c473da8498a8c1f8d0fb3d0fa9c, km8f41b4026024729a2a118eab53eb258);
    var kmc76a32d37aa417b9aa1691a3f5fe021 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc76a32d37aa417b9aa1691a3f5fe021.setDefaultUnit(kony.flex.DP);
    var km05a587ec5cb4504bbb0d11c9ff85f55 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmacaadb823be4468a2be934b78d9cd54 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmc76a32d37aa417b9aa1691a3f5fe021.add(km05a587ec5cb4504bbb0d11c9ff85f55, kmacaadb823be4468a2be934b78d9cd54);
    var kmda2b60d1a8b4aa4bb1d67e9932ec07e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmda2b60d1a8b4aa4bb1d67e9932ec07e.setDefaultUnit(kony.flex.DP);
    var km66a79af2f784afa8f1508e4d0b0c432 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmf8923073ebb483eaa0b98f59cdb32d0 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmda2b60d1a8b4aa4bb1d67e9932ec07e.add(km66a79af2f784afa8f1508e4d0b0c432, kmf8923073ebb483eaa0b98f59cdb32d0);
    var km5d0a4fa556b4dd6a15f2de86076f0ba = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km5d0a4fa556b4dd6a15f2de86076f0ba.setDefaultUnit(kony.flex.DP);
    var kma720fdb8d4547b19d43953f98a75369 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km5872fe5df2b41a69c3799af07b43f31 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km5d0a4fa556b4dd6a15f2de86076f0ba.add(kma720fdb8d4547b19d43953f98a75369, km5872fe5df2b41a69c3799af07b43f31);
    var km8e772919f494f34b770b02efabefc70 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8e772919f494f34b770b02efabefc70.setDefaultUnit(kony.flex.DP);
    var kmb9099dd657b482caf934ef1683776d5 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km49ed2f41a4542cdb8b85d5bcc43d9b6 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km8e772919f494f34b770b02efabefc70.add(kmb9099dd657b482caf934ef1683776d5, km49ed2f41a4542cdb8b85d5bcc43d9b6);
    var kmb848e10cefe4c27973a45654810ed03 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb848e10cefe4c27973a45654810ed03.setDefaultUnit(kony.flex.DP);
    var km744912ec7364116a2e27af7c482cd10 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km1074496d1ee4a4ba8b57b17b98ecc84 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmb848e10cefe4c27973a45654810ed03.add(km744912ec7364116a2e27af7c482cd10, km1074496d1ee4a4ba8b57b17b98ecc84);
    km959715e4d1d4d5a961ab8cace299358.add(km383348b2be84cfc998496b23dbe9c1e, kmb0c316a5a15418ebb6c6a5d55d9ce01, kma1eba3c55db42a49196ee6bb8917215, kmc76a32d37aa417b9aa1691a3f5fe021, kmda2b60d1a8b4aa4bb1d67e9932ec07e, km5d0a4fa556b4dd6a15f2de86076f0ba, km8e772919f494f34b770b02efabefc70, kmb848e10cefe4c27973a45654810ed03);
    frmReviews.add(flxMainContainer, flxHeaderTitleContainer, flxOverlay, km959715e4d1d4d5a961ab8cace299358);
};

function frmReviewsGlobals() {
    frmReviews = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmReviews,
        "allowHorizontalBounce": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmReviews",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "maxZoomScale": 1,
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};