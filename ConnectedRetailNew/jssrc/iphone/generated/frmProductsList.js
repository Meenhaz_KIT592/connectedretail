function addWidgetsfrmProductsList() {
    frmProductsList.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "bouncesZoom": false
    });
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxGroupElements = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxGroupElements",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxGroupElements.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxRefineSearchWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxRefineSearchWrap",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBurgundy",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxRefineSearchWrap.setDefaultUnit(kony.flex.DP);
    var Label0a6eb8864dd5043 = new kony.ui.Label({
        "height": "100%",
        "id": "Label0a6eb8864dd5043",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLblIcon32pxWhite",
        "text": "K",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnRefineSearch = new kony.ui.Button({
        "focusSkin": "sknBtnBgTrans28pxBoldWhiteFoc",
        "height": "100%",
        "id": "btnRefineSearch",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTrans28pxBoldWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.refine"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxRefineSearchWrap.add(Label0a6eb8864dd5043, btnRefineSearch);
    var flxStore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxStore",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDark1",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStore.setDefaultUnit(kony.flex.DP);
    var lblStoreName = new kony.ui.Label({
        "height": "100%",
        "id": "lblStoreName",
        "isVisible": true,
        "left": "42dp",
        "right": "3%",
        "skin": "sknLblGothamRegular24pxWhite",
        "text": "At Prestonwood Town Center",
        "top": "0dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTick = new kony.ui.Label({
        "height": "100%",
        "id": "lblTick",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblIcon32pxWhite",
        "text": "t",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxStore.add(lblStoreName, lblTick);
    var flxCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxCategory",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCategory.setDefaultUnit(kony.flex.DP);
    var imgCategoryImg = new kony.ui.Image2({
        "id": "imgCategoryImg",
        "imageWhenFailed": "notavailable_1080x142.png",
        "imageWhileDownloading": "white_1080x142.png",
        "isVisible": false,
        "left": "0dp",
        "skin": "slImage",
        "src": "sample_category.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnCategoryName = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransMed32px818181",
        "height": "100%",
        "id": "btnCategoryName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTransMed32px818181",
        "text": "Colored Pencils",
        "top": "0dp",
        "width": "100%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [5, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblCategoryBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblCategoryBack",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon34px000000",
        "text": "L",
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblGothamMedium32pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnBack"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCategory.add(imgCategoryImg, btnCategoryName, lblCategoryBack, lblBack);
    var flxProjects = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxProjects",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProjects.setDefaultUnit(kony.flex.DP);
    var lblProjects = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblProjects",
        "isVisible": true,
        "skin": "sknLblGothamRegular28pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxProjects.add(lblProjects);
    var flxTotalResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxTotalResults",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTotalResults.setDefaultUnit(kony.flex.DP);
    var lblTotalResults = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblTotalResults",
        "isVisible": true,
        "skin": "sknLblGothamMedium28pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmProductsList.lblTotalResults"),
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTotalResults.add(lblTotalResults);
    var flexPromoOffer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flexPromoOffer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flexPromoOffer.setDefaultUnit(kony.flex.DP);
    var Image01c42148de6a645 = new kony.ui.Image2({
        "height": "100%",
        "id": "Image01c42148de6a645",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "sample_ad1.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexPromoOffer.add(Image01c42148de6a645);
    var flxProdProjTabs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxProdProjTabs",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxProdProjTabs.setDefaultUnit(kony.flex.DP);
    var btnProducts = new kony.ui.Button({
        "focusSkin": "sknBtnTabsActive",
        "height": "100%",
        "id": "btnProducts",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTabsActive",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnProjects = new kony.ui.Button({
        "focusSkin": "sknBtnTabsDefault",
        "height": "100%",
        "id": "btnProjects",
        "isVisible": true,
        "left": "50%",
        "skin": "sknBtnTabsDefault",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxProdProjTabs.add(btnProducts, btnProjects, flxLine);
    flxGroupElements.add(flxTopSpacerDND, flxRefineSearchWrap, flxStore, flxCategory, flxProjects, flxTotalResults, flexPromoOffer, flxProdProjTabs);
    var segProductsList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "centerX": "50%",
        "data": [{
            "btnBuyOnline": "BUY ONLINE",
            "btnStoreMap": "AISLE 55",
            "imgItem": "sample_product.png",
            "imgStar1": "imagedrag.png",
            "imgStar2": "starempty.png",
            "imgStar3": "starempty.png",
            "imgStar4": "starempty.png",
            "imgStar5": "starempty.png",
            "lblAisleNo": "IN STOCK",
            "lblItemName": "Complete Art Easel & Portfolio Set By Artist’s Loft",
            "lblItemNumberReviews": "()",
            "lblItemPrice": "$26.99  - $29.99",
            "lblItemPriceOld": "",
            "lblItemStock": "12 IN STOCK",
            "lblMapIcon": "c",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y"
        }, {
            "btnBuyOnline": "BUY ONLINE",
            "btnStoreMap": "AISLE 55",
            "imgItem": "sample_product.png",
            "imgStar1": "imagedrag.png",
            "imgStar2": "starempty.png",
            "imgStar3": "starempty.png",
            "imgStar4": "starempty.png",
            "imgStar5": "starempty.png",
            "lblAisleNo": "IN STOCK",
            "lblItemName": "Complete Art Easel & Portfolio Set By Artist’s Loft",
            "lblItemNumberReviews": "()",
            "lblItemPrice": "$26.99  - $29.99",
            "lblItemPriceOld": "",
            "lblItemStock": "12 IN STOCK",
            "lblMapIcon": "c",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y"
        }, {
            "btnBuyOnline": "BUY ONLINE",
            "btnStoreMap": "AISLE 55",
            "imgItem": "sample_product.png",
            "imgStar1": "imagedrag.png",
            "imgStar2": "starempty.png",
            "imgStar3": "starempty.png",
            "imgStar4": "starempty.png",
            "imgStar5": "starempty.png",
            "lblAisleNo": "IN STOCK",
            "lblItemName": "Complete Art Easel & Portfolio Set By Artist’s Loft",
            "lblItemNumberReviews": "()",
            "lblItemPrice": "$26.99  - $29.99",
            "lblItemPriceOld": "",
            "lblItemStock": "12 IN STOCK",
            "lblMapIcon": "c",
            "lblStar1": "Y",
            "lblStar2": "Y",
            "lblStar3": "Y",
            "lblStar4": "Y",
            "lblStar5": "Y"
        }],
        "groupCells": false,
        "id": "segProductsList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxProductItem,
        "scrollingEvents": {
            "onReachingEnd": AS_Segment_ba053565b962409c8047afe045b58456
        },
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "142dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "CopyflxClearance04dd36e9de23549": "CopyflxClearance04dd36e9de23549",
            "CopylblIconBack04eaa8b2f367a48": "CopylblIconBack04eaa8b2f367a48",
            "CopylblIconBack0b2b53bc248d74b": "CopylblIconBack0b2b53bc248d74b",
            "FlexContainer0679bd4f1bb6746": "FlexContainer0679bd4f1bb6746",
            "FlexGroup0b8478a21c4a44a": "FlexGroup0b8478a21c4a44a",
            "btnBuyOnline": "btnBuyOnline",
            "btnStoreMap": "btnStoreMap",
            "flxBottomSpacerDND": "flxBottomSpacerDND",
            "flxClearanceText": "flxClearanceText",
            "flxItemDetails": "flxItemDetails",
            "flxItemDetailsWrap": "flxItemDetailsWrap",
            "flxItemLocation": "flxItemLocation",
            "flxItemStock": "flxItemStock",
            "flxProductItem": "flxProductItem",
            "flxStarRating": "flxStarRating",
            "imgItem": "imgItem",
            "imgStar1": "imgStar1",
            "imgStar2": "imgStar2",
            "imgStar3": "imgStar3",
            "imgStar4": "imgStar4",
            "imgStar5": "imgStar5",
            "lblAisleNo": "lblAisleNo",
            "lblItemName": "lblItemName",
            "lblItemNumberReviews": "lblItemNumberReviews",
            "lblItemPrice": "lblItemPrice",
            "lblItemPriceOld": "lblItemPriceOld",
            "lblItemStock": "lblItemStock",
            "lblMapIcon": "lblMapIcon",
            "lblStar1": "lblStar1",
            "lblStar2": "lblStar2",
            "lblStar3": "lblStar3",
            "lblStar4": "lblStar4",
            "lblStar5": "lblStar5"
        },
        "width": "94%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "64dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    var flxNoResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxNoResults",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "72dp",
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    flxNoResults.setDefaultUnit(kony.flex.DP);
    var rtxtSearchNotFound = new kony.ui.RichText({
        "id": "rtxtSearchNotFound",
        "isVisible": true,
        "left": "3%",
        "skin": "sknRTGothamRegularBlack14px",
        "text": "Rich Text",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbltextNoResultsFound = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbltextNoResultsFound",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxDarkGray",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmProductsList.lbltextNoResultsFound"),
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNoResults.add(rtxtSearchNotFound, lbltextNoResultsFound);
    var flexLoading = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "-40dp",
        "centerX": "50%",
        "clipBounds": true,
        "height": "40dp",
        "id": "flexLoading",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flexLoading.setDefaultUnit(kony.flex.DP);
    var txtLoading = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "txtLoading",
        "isVisible": true,
        "skin": "sknLblGothamMedium28pxDark",
        "text": "Loading ....",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flexLoading.add(txtLoading);
    flxMainContainer.add(flxGroupElements, segProductsList, flxBottomSpacerDND, flxNoResults, flexLoading);
    var km20356f0423a4c6cb7dbe76b32815adb = new kony.ui.FlexScrollContainer({
        "isMaster": true,
        "id": "flxSearchResultsContainer",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    km20356f0423a4c6cb7dbe76b32815adb.setDefaultUnit(kony.flex.DP);
    var km847a65d04d740139d6e5be6aadbc1d2 = new kony.ui.FlexContainer({
        "id": "flxTopSpacerDNDSearch",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km847a65d04d740139d6e5be6aadbc1d2.setDefaultUnit(kony.flex.DP);
    km847a65d04d740139d6e5be6aadbc1d2.add();
    var kmde2c11fbbcd48ada8341a12210279d7 = new kony.ui.SegmentedUI2({
        "id": "segSearchOffers",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    var kmef9c1ec7990492ea3eb4e63d53cb2e9 = new kony.ui.SegmentedUI2({
        "id": "segSearchResults",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    km20356f0423a4c6cb7dbe76b32815adb.add(km847a65d04d740139d6e5be6aadbc1d2, kmde2c11fbbcd48ada8341a12210279d7, kmef9c1ec7990492ea3eb4e63d53cb2e9);
    var km00ef78827f24b6b937c14cfe3774144 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    km00ef78827f24b6b937c14cfe3774144.setDefaultUnit(kony.flex.DP);
    var km721f23cd7c04af28b468accddef5d3e = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km721f23cd7c04af28b468accddef5d3e.setDefaultUnit(kony.flex.DP);
    km721f23cd7c04af28b468accddef5d3e.add();
    var km57ad71144ee4e909e51ef592a4ea2eb = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km57ad71144ee4e909e51ef592a4ea2eb.setDefaultUnit(kony.flex.DP);
    var kmaae02f328e747ccb1b865c8b72b4654 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmef1fd09601946c2b67a2d9d631c6701 = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmf41bd504d0748b1b667b3e7b301d928 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kma04b06e1b034c3abee7d22465fb109f = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    kma04b06e1b034c3abee7d22465fb109f.setDefaultUnit(kony.flex.DP);
    kma04b06e1b034c3abee7d22465fb109f.add();
    var kmf3fd419ab4342e8ab66783c77f49173 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km57ad71144ee4e909e51ef592a4ea2eb.add(kmaae02f328e747ccb1b865c8b72b4654, kmef1fd09601946c2b67a2d9d631c6701, kmf41bd504d0748b1b667b3e7b301d928, kma04b06e1b034c3abee7d22465fb109f, kmf3fd419ab4342e8ab66783c77f49173);
    var km972ab9f2b3b4b26af183805ce8eed52 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km972ab9f2b3b4b26af183805ce8eed52.setDefaultUnit(kony.flex.DP);
    var km0451b4fc5184fa9b210e781ed737f5b = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km0451b4fc5184fa9b210e781ed737f5b.setDefaultUnit(kony.flex.DP);
    var km809804625924c52bf2b806d6349ecc0 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km809804625924c52bf2b806d6349ecc0.setDefaultUnit(kony.flex.DP);
    var kmac730d050214c1a962a0fcc858851f2 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km809804625924c52bf2b806d6349ecc0.add(kmac730d050214c1a962a0fcc858851f2);
    var kmc0d9861235a4f78a71336419fe6b450 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    kmc0d9861235a4f78a71336419fe6b450.setDefaultUnit(kony.flex.DP);
    var km331374ba68f4813bbbffd67edadd5f7 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmc0d9861235a4f78a71336419fe6b450.add(km331374ba68f4813bbbffd67edadd5f7);
    var km052032d8a7d4aef8c14bc6edb38a6ce = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km052032d8a7d4aef8c14bc6edb38a6ce.setDefaultUnit(kony.flex.DP);
    var kmf413a5f996e43a6bdda39db073be6a2 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var kme65cfacd0f344659e2282746a14be58 = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    km052032d8a7d4aef8c14bc6edb38a6ce.add(kmf413a5f996e43a6bdda39db073be6a2, kme65cfacd0f344659e2282746a14be58);
    var kmac16bd264944b2b9baca27d15b8022f = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km0451b4fc5184fa9b210e781ed737f5b.add(km809804625924c52bf2b806d6349ecc0, kmc0d9861235a4f78a71336419fe6b450, km052032d8a7d4aef8c14bc6edb38a6ce, kmac16bd264944b2b9baca27d15b8022f);
    var km5dceaedee3348b68fbdd4d8dcb1625d = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km5dceaedee3348b68fbdd4d8dcb1625d.setDefaultUnit(kony.flex.DP);
    km5dceaedee3348b68fbdd4d8dcb1625d.add();
    km972ab9f2b3b4b26af183805ce8eed52.add(km0451b4fc5184fa9b210e781ed737f5b, km5dceaedee3348b68fbdd4d8dcb1625d);
    var kmb06b48907664496b636334f1db5280c = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmb06b48907664496b636334f1db5280c.setDefaultUnit(kony.flex.DP);
    var km40c992bee6c43dbb8e09ad6b74905b0 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km40c992bee6c43dbb8e09ad6b74905b0.setDefaultUnit(kony.flex.DP);
    var km8049485719044e0809cf9a9be4c5b8e = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km8049485719044e0809cf9a9be4c5b8e.setDefaultUnit(kony.flex.DP);
    var km9cb0614247840919580ef90d5d944dd = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km0a1a1e8a1234e7cb570340769842400 = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km8049485719044e0809cf9a9be4c5b8e.add(km9cb0614247840919580ef90d5d944dd, km0a1a1e8a1234e7cb570340769842400);
    var km513ce9ac83f442bab7f1f93e6ae37cf = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km513ce9ac83f442bab7f1f93e6ae37cf.setDefaultUnit(kony.flex.DP);
    var km6b7e33ac5ff40c690e3aaa25bd94caf = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km6b7e33ac5ff40c690e3aaa25bd94caf.setDefaultUnit(kony.flex.DP);
    var km679190f2a9745dca90d3e65a89659bf = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km823f6ef21d14a8788b0ee9ef0b4ec15 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km566f8d1d6f845c79e0043e4be81451a = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmb92e2ffe6064bbcb1572213bdf23c0f = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km2b1522663184f9fab49a79236e3628b = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km0d8fea7536446aa80aa131a680be050 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km6b7e33ac5ff40c690e3aaa25bd94caf.add(km679190f2a9745dca90d3e65a89659bf, km823f6ef21d14a8788b0ee9ef0b4ec15, km566f8d1d6f845c79e0043e4be81451a, kmb92e2ffe6064bbcb1572213bdf23c0f, km2b1522663184f9fab49a79236e3628b, km0d8fea7536446aa80aa131a680be050);
    var km0bf211247c6411ca7a9976b93d74bd7 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km0bf211247c6411ca7a9976b93d74bd7.setDefaultUnit(kony.flex.DP);
    var km00e97c7dcab4ed4a5a21c0d9e41afc5 = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc2e5c6a534a4db2ba0ee57de41257a1 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmd0351ead1684c4bbd65eacc064bcec7 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km0bf211247c6411ca7a9976b93d74bd7.add(km00e97c7dcab4ed4a5a21c0d9e41afc5, kmc2e5c6a534a4db2ba0ee57de41257a1, kmd0351ead1684c4bbd65eacc064bcec7);
    km513ce9ac83f442bab7f1f93e6ae37cf.add(km6b7e33ac5ff40c690e3aaa25bd94caf, km0bf211247c6411ca7a9976b93d74bd7);
    var kme1a836cd99e46299cebab58272ad0aa = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmb4cc29ee47346e3adc5f9c34fed371a = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km40c992bee6c43dbb8e09ad6b74905b0.add(km8049485719044e0809cf9a9be4c5b8e, km513ce9ac83f442bab7f1f93e6ae37cf, kme1a836cd99e46299cebab58272ad0aa, kmb4cc29ee47346e3adc5f9c34fed371a);
    kmb06b48907664496b636334f1db5280c.add(km40c992bee6c43dbb8e09ad6b74905b0);
    km00ef78827f24b6b937c14cfe3774144.add(km721f23cd7c04af28b468accddef5d3e, km57ad71144ee4e909e51ef592a4ea2eb, km972ab9f2b3b4b26af183805ce8eed52, kmb06b48907664496b636334f1db5280c);
    var km493c62223e245a4b60b665ab7aab95e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km493c62223e245a4b60b665ab7aab95e.setDefaultUnit(kony.flex.DP);
    var kmb4f87b17de048dc913b882599574396 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    kmb4f87b17de048dc913b882599574396.setDefaultUnit(kony.flex.DP);
    var kmb93c6e3de724dd782b71dfbaa4bbe51 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb93c6e3de724dd782b71dfbaa4bbe51.setDefaultUnit(kony.flex.DP);
    var km20936c909f344e69c260d25a18878f7 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km518f7b299da44a1a92b9c99e77c38b0 = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km518f7b299da44a1a92b9c99e77c38b0.setDefaultUnit(kony.flex.DP);
    var km64807c3d25d4097884dbaf55e2d93c1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmdfae3a0e51048f783bc6a5f0c287aa2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km0ee811ae27d4b4bbcf9822315e618cf = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km518f7b299da44a1a92b9c99e77c38b0.add(km64807c3d25d4097884dbaf55e2d93c1, kmdfae3a0e51048f783bc6a5f0c287aa2, km0ee811ae27d4b4bbcf9822315e618cf);
    var kmd233aeeac5149f3baf7f41eba23b373 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km849e9acb6c44369abe67466ae7d4c24 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmb93c6e3de724dd782b71dfbaa4bbe51.add(km20936c909f344e69c260d25a18878f7, km518f7b299da44a1a92b9c99e77c38b0, kmd233aeeac5149f3baf7f41eba23b373, km849e9acb6c44369abe67466ae7d4c24);
    var kmb9893e5735340b4ba7a142e9207f721 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    kmb9893e5735340b4ba7a142e9207f721.setDefaultUnit(kony.flex.DP);
    var kmf0bd213cf0545069222afa0f15bf8fc = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf0bd213cf0545069222afa0f15bf8fc.setDefaultUnit(kony.flex.DP);
    var km286c7a7efa44fad9f7c1fa540d21782 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf0bd213cf0545069222afa0f15bf8fc.add(km286c7a7efa44fad9f7c1fa540d21782);
    kmb9893e5735340b4ba7a142e9207f721.add(kmf0bd213cf0545069222afa0f15bf8fc);
    var km83260af64bb4b18b71eda745bb121ee = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmb4f87b17de048dc913b882599574396.add(kmb93c6e3de724dd782b71dfbaa4bbe51, kmb9893e5735340b4ba7a142e9207f721, km83260af64bb4b18b71eda745bb121ee);
    km493c62223e245a4b60b665ab7aab95e.add(kmb4f87b17de048dc913b882599574396);
    var km706e08d94d849d89ab36d13e93a960a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km706e08d94d849d89ab36d13e93a960a.setDefaultUnit(kony.flex.DP);
    var kmb936dd6ccb148f18bebfad2cda3eab3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    kmb936dd6ccb148f18bebfad2cda3eab3.setDefaultUnit(kony.flex.DP);
    var km4f80db873b04bbc9105e9005c3ef567 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km4f80db873b04bbc9105e9005c3ef567.setDefaultUnit(kony.flex.DP);
    var km2cf483c84bb45f19ab62b9a4863cb6e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    km2cf483c84bb45f19ab62b9a4863cb6e.setDefaultUnit(kony.flex.DP);
    km2cf483c84bb45f19ab62b9a4863cb6e.add();
    var kmef8a2995bb04d518f76d0bf714ab4c7 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmc715ed9bace4f31a7ffc3135d80ab8c = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km73a88afa9314cdd85e5c392ba67f8f9 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km73a88afa9314cdd85e5c392ba67f8f9.setDefaultUnit(kony.flex.DP);
    var km06b00ff5de142b6bf51288c1e3a7c08 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km9b43e643927496fb387fb5d4a8d2665 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km73a88afa9314cdd85e5c392ba67f8f9.add(km06b00ff5de142b6bf51288c1e3a7c08, km9b43e643927496fb387fb5d4a8d2665);
    var km1e0327e76eb4bb2982f1adb72f622ae = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km1e0327e76eb4bb2982f1adb72f622ae.setDefaultUnit(kony.flex.DP);
    var kmeee9bbfe61a42f9bd94b17730714e38 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc80f89c7e7748d0b8dadec8731de91d = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "captureOrientation": constants.CAMERA_CAPTURE_ORIENTATION_PORTRAIT,
        "enableOverlay": true,
        "nativeUserInterface": false,
        "overlayConfig": {
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay
        }
    });
    km1e0327e76eb4bb2982f1adb72f622ae.add(kmeee9bbfe61a42f9bd94b17730714e38, kmc80f89c7e7748d0b8dadec8731de91d);
    var km56d0f3ba5e04cdabe8c07ee9baf67df = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km56d0f3ba5e04cdabe8c07ee9baf67df.setDefaultUnit(kony.flex.DP);
    var km626ac05e4f5413db9bff1857539666c = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kme762b389ce949708d26e4dc9186975c = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km56d0f3ba5e04cdabe8c07ee9baf67df.add(km626ac05e4f5413db9bff1857539666c, kme762b389ce949708d26e4dc9186975c);
    km4f80db873b04bbc9105e9005c3ef567.add(km2cf483c84bb45f19ab62b9a4863cb6e, kmef8a2995bb04d518f76d0bf714ab4c7, kmc715ed9bace4f31a7ffc3135d80ab8c, km73a88afa9314cdd85e5c392ba67f8f9, km1e0327e76eb4bb2982f1adb72f622ae, km56d0f3ba5e04cdabe8c07ee9baf67df);
    kmb936dd6ccb148f18bebfad2cda3eab3.add(km4f80db873b04bbc9105e9005c3ef567);
    km706e08d94d849d89ab36d13e93a960a.add(kmb936dd6ccb148f18bebfad2cda3eab3);
    var km6e3a5e17f1e4673be6ef1dacfdb53fb = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km6e3a5e17f1e4673be6ef1dacfdb53fb.setDefaultUnit(kony.flex.DP);
    var kmced4b8ae6034da1bae206d5d5bfddeb = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmced4b8ae6034da1bae206d5d5bfddeb.setDefaultUnit(kony.flex.DP);
    var km6391479a3034d69b5d9d2272c22f43d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmfda1f446f2b43d78038f1b139fe5559 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmced4b8ae6034da1bae206d5d5bfddeb.add(km6391479a3034d69b5d9d2272c22f43d, kmfda1f446f2b43d78038f1b139fe5559);
    var km3403c118f7545f4b59dec2b40b92274 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3403c118f7545f4b59dec2b40b92274.setDefaultUnit(kony.flex.DP);
    var kmcffd03adccd48688a3c44082bdc3302 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km79c4957ef5d41dd8eb7c5a7ff8c7740 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km6f1102cd1b1457e9e683b43ab68b80f = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km3403c118f7545f4b59dec2b40b92274.add(kmcffd03adccd48688a3c44082bdc3302, km79c4957ef5d41dd8eb7c5a7ff8c7740, km6f1102cd1b1457e9e683b43ab68b80f);
    var km8174255863f4a46a03f8bf0ae2c3f32 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8174255863f4a46a03f8bf0ae2c3f32.setDefaultUnit(kony.flex.DP);
    var kmdaa53f21c92418a94b1bffc37460b5e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km1d915aa61a1412dbc36b4b3b8be3d90 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km8174255863f4a46a03f8bf0ae2c3f32.add(kmdaa53f21c92418a94b1bffc37460b5e, km1d915aa61a1412dbc36b4b3b8be3d90);
    var kma1f58f1616f43b6a0118b232665568b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma1f58f1616f43b6a0118b232665568b.setDefaultUnit(kony.flex.DP);
    var kmb4a5a6c77fa46338a1d1596d21a015d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7a1b963ee5a4cf3aee60dcb44230fa7 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kma1f58f1616f43b6a0118b232665568b.add(kmb4a5a6c77fa46338a1d1596d21a015d, km7a1b963ee5a4cf3aee60dcb44230fa7);
    var km0e0d4a001e44fd9ab1f17d87b2688d4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0e0d4a001e44fd9ab1f17d87b2688d4.setDefaultUnit(kony.flex.DP);
    var kmc0f250f47064b45bb40786c913877a9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km27f26a7c8544aff9d928a97fb40ea83 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km0e0d4a001e44fd9ab1f17d87b2688d4.add(kmc0f250f47064b45bb40786c913877a9, km27f26a7c8544aff9d928a97fb40ea83);
    var km74ddfbc089b41b696e29617d7b21200 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km74ddfbc089b41b696e29617d7b21200.setDefaultUnit(kony.flex.DP);
    var kmf28d8570aa1465a93d388b40b3ccc85 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km872c42e8d4f4f51a27e53a6c3584f76 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km74ddfbc089b41b696e29617d7b21200.add(kmf28d8570aa1465a93d388b40b3ccc85, km872c42e8d4f4f51a27e53a6c3584f76);
    var km5db6466336e41aebf20f2805f54e621 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km5db6466336e41aebf20f2805f54e621.setDefaultUnit(kony.flex.DP);
    var km0e73a05d93a4cafb65cbeba4561f19d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmcad02f184394166b0e6191cb2ec9183 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km5db6466336e41aebf20f2805f54e621.add(km0e73a05d93a4cafb65cbeba4561f19d, kmcad02f184394166b0e6191cb2ec9183);
    var kmd7178b372584e619adb87c1d7b69e8a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd7178b372584e619adb87c1d7b69e8a.setDefaultUnit(kony.flex.DP);
    var km16eec78717a465c818273cb199b5eb6 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km797c761f8d549b78ce35e395a29cb92 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmd7178b372584e619adb87c1d7b69e8a.add(km16eec78717a465c818273cb199b5eb6, km797c761f8d549b78ce35e395a29cb92);
    km6e3a5e17f1e4673be6ef1dacfdb53fb.add(kmced4b8ae6034da1bae206d5d5bfddeb, km3403c118f7545f4b59dec2b40b92274, km8174255863f4a46a03f8bf0ae2c3f32, kma1f58f1616f43b6a0118b232665568b, km0e0d4a001e44fd9ab1f17d87b2688d4, km74ddfbc089b41b696e29617d7b21200, km5db6466336e41aebf20f2805f54e621, kmd7178b372584e619adb87c1d7b69e8a);
    var km82b6a11cdc54da28979c78220e954af = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    km82b6a11cdc54da28979c78220e954af.setDefaultUnit(kony.flex.DP);
    var km5c776af022b4039a05219c1ce5c566c = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kme6807fa067f4441bb29d44ff56dda47 = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    kme6807fa067f4441bb29d44ff56dda47.setDefaultUnit(kony.flex.DP);
    var kmfc8452668654309bedd79870464eee5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmfc8452668654309bedd79870464eee5.setDefaultUnit(kony.flex.DP);
    var kmf08d5b4b4a341718922bba0bb2e0d82 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmfc8452668654309bedd79870464eee5.add(kmf08d5b4b4a341718922bba0bb2e0d82);
    var km5f049dad6514fcb9c725ae0408d3756 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km5f049dad6514fcb9c725ae0408d3756.setDefaultUnit(kony.flex.DP);
    var kma214cfc94f8441792cf53b31501727c = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    km5f049dad6514fcb9c725ae0408d3756.add(kma214cfc94f8441792cf53b31501727c);
    var km4ce260c2a2f47948ef776ad9ece7f9c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km4ce260c2a2f47948ef776ad9ece7f9c.setDefaultUnit(kony.flex.DP);
    var km85158b0f6c44bd58b1bc7f9ec010ae0 = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "captureOrientation": constants.CAMERA_CAPTURE_ORIENTATION_PORTRAIT,
        "enableOverlay": true,
        "nativeUserInterface": false,
        "overlayConfig": {
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay
        }
    });
    km4ce260c2a2f47948ef776ad9ece7f9c.add(km85158b0f6c44bd58b1bc7f9ec010ae0);
    var km41b80e26bb64c6fa5c8679e36fec21a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km41b80e26bb64c6fa5c8679e36fec21a.setDefaultUnit(kony.flex.DP);
    var km78087a48ad94aba9b44b6773bb5a8ca = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km41b80e26bb64c6fa5c8679e36fec21a.add(km78087a48ad94aba9b44b6773bb5a8ca);
    var kma75ff4482fe44e1a3b8ab0a963586c9 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma75ff4482fe44e1a3b8ab0a963586c9.setDefaultUnit(kony.flex.DP);
    var kmdcc90f91b3c42ac9de870a715295e3b = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kma75ff4482fe44e1a3b8ab0a963586c9.add(kmdcc90f91b3c42ac9de870a715295e3b);
    kme6807fa067f4441bb29d44ff56dda47.add(kmfc8452668654309bedd79870464eee5, km5f049dad6514fcb9c725ae0408d3756, km4ce260c2a2f47948ef776ad9ece7f9c, km41b80e26bb64c6fa5c8679e36fec21a, kma75ff4482fe44e1a3b8ab0a963586c9);
    var kmd28850028c5442bacf1e4f757a6b1f1 = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmdd793d43d3043a2be2b79906adc08e6 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmdd793d43d3043a2be2b79906adc08e6.setDefaultUnit(kony.flex.DP);
    var km09179e0bd4246a9a569842dad50974f = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km6847c7da1fb48be9ab248ad09cbd995 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmdd793d43d3043a2be2b79906adc08e6.add(km09179e0bd4246a9a569842dad50974f, km6847c7da1fb48be9ab248ad09cbd995);
    km82b6a11cdc54da28979c78220e954af.add(km5c776af022b4039a05219c1ce5c566c, kme6807fa067f4441bb29d44ff56dda47, kmd28850028c5442bacf1e4f757a6b1f1, kmdd793d43d3043a2be2b79906adc08e6);
    frmProductsList.add(flxMainContainer, km20356f0423a4c6cb7dbe76b32815adb, km00ef78827f24b6b937c14cfe3774144, km493c62223e245a4b60b665ab7aab95e, km706e08d94d849d89ab36d13e93a960a, km6e3a5e17f1e4673be6ef1dacfdb53fb, km82b6a11cdc54da28979c78220e954af);
};

function frmProductsListGlobals() {
    frmProductsList = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmProductsList,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmProductsList",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "bouncesZoom": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NONE,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};