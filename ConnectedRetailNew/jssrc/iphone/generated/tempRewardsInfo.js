function initializetempRewardsInfo() {
    flxMainRewardInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMainRewardInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxMainRewardInfo.setDefaultUnit(kony.flex.DP);
    var FlexContainer06bdc793499af41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "FlexContainer06bdc793499af41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    FlexContainer06bdc793499af41.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "height": "70dp",
        "id": "imgLogo",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_couponrewards.png",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexContainer0ebaf1a4b1fea49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer0ebaf1a4b1fea49",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "95dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer0ebaf1a4b1fea49.setDefaultUnit(kony.flex.DP);
    var FlexContainer0d609b1d81f1345 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer0d609b1d81f1345",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0d609b1d81f1345.setDefaultUnit(kony.flex.DP);
    var Label0a61c395176a840 = new kony.ui.Label({
        "id": "Label0a61c395176a840",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxRed",
        "text": "EXCLUSIVE",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblHeader = new kony.ui.Label({
        "id": "lblHeader",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "OFFERS",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexContainer0d609b1d81f1345.add(Label0a61c395176a840, lblHeader);
    var rchTxtBody = new kony.ui.RichText({
        "id": "rchTxtBody",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknRTGothamBook808080",
        "text": "We’ll deliver members-only offers straight to your inbox. Save on the décor and DIYs you love most!",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBody = new kony.ui.Label({
        "id": "lblBody",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "We’ll deliver members-only offers straight to your inbox. Save on the décor and DIYs you love most!",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexContainer0ebaf1a4b1fea49.add(FlexContainer0d609b1d81f1345, rchTxtBody, lblBody);
    FlexContainer06bdc793499af41.add(imgLogo, FlexContainer0ebaf1a4b1fea49);
    flxMainRewardInfo.add(FlexContainer06bdc793499af41);
}