function initializetempReviewProfile() {
    flxReviweProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxReviweProfile",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxReviweProfile.setDefaultUnit(kony.flex.DP);
    var lblReviewProfile = new kony.ui.Label({
        "id": "lblReviewProfile",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Label",
        "top": "3dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblReviewProfileNo = new kony.ui.Label({
        "id": "lblReviewProfileNo",
        "isVisible": true,
        "right": "2dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Label",
        "top": "3dp",
        "width": "35%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxReviweProfile.add(lblReviewProfile, lblReviewProfileNo);
}