function initializetempDisclaimer() {
    flxWeeklyAdDisclaimer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxWeeklyAdDisclaimer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxWeeklyAdDisclaimer.setDefaultUnit(kony.flex.DP);
    var flxWeeklyAdContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxWeeklyAdContents",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWeeklyAdContents.setDefaultUnit(kony.flex.DP);
    var imgOffer = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgOffer",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": false,
        "left": "3%",
        "skin": "slImage",
        "src": "white_1012x628.png",
        "top": "10dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContents.setDefaultUnit(kony.flex.DP);
    var txtDisclaimer = new kony.ui.Label({
        "centerX": "50%",
        "id": "txtDisclaimer",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknLblGothamMedium24pxDarkGray",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmWeeklyAd.disclaimer2"),
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "30dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var txtDisclaimer1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "txtDisclaimer1",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknLblGothamMedium24pxDarkGray",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmWeeklyAd.disclaimer1"),
        "textStyle": {
            "lineSpacing": 1,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxContents.add(txtDisclaimer, txtDisclaimer1);
    var flxSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpacer.setDefaultUnit(kony.flex.DP);
    flxSpacer.add();
    flxWeeklyAdContents.add(imgOffer, flxContents, flxSpacer);
    var flxBottomSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxBottomSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacer.setDefaultUnit(kony.flex.DP);
    flxBottomSpacer.add();
    flxWeeklyAdDisclaimer.add(flxWeeklyAdContents, flxBottomSpacer);
}