function addWidgetsfrmMore() {
    frmMore.setDefaultUnit(kony.flex.DP);
    var flxMenuList = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMenuList",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgLtGray",
        "top": "7.50%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "bouncesZoom": false
    });
    flxMenuList.setDefaultUnit(kony.flex.DP);
    var flxShortProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "flxShortProfile",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxShortProfile.setDefaultUnit(kony.flex.DP);
    var lblProfileName = new kony.ui.Label({
        "id": "lblProfileName",
        "isVisible": true,
        "left": "45dp",
        "skin": "sknLblGothamRegular24pxBlack",
        "text": "Gennie",
        "textStyle": {
            "lineSpacing": 2
        },
        "top": "50dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnMyAccount = new kony.ui.Button({
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "id": "btnMyAccount",
        "isVisible": false,
        "left": "50dp",
        "skin": "sknBtnPrimaryWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnMyAccount"),
        "top": "50dp",
        "width": "120dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnSignIn = new kony.ui.Button({
        "focusSkin": "sknBtnRed24pxMedFoc",
        "height": "30dp",
        "id": "btnSignIn",
        "isVisible": true,
        "left": "2%",
        "skin": "sknBtnRed24pxMed",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.SignInBtn"),
        "top": "50dp",
        "width": "47%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnSignUp = new kony.ui.Button({
        "focusSkin": "sknBtnRed24pxMedFoc",
        "height": "30dp",
        "id": "btnSignUp",
        "isVisible": true,
        "left": "50%",
        "skin": "sknBtnMontserratSemiBold13pxBlueBorder",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.btnSignUp"),
        "top": "50dp",
        "width": "47%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnDown = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxRed",
        "id": "btnDown",
        "isVisible": false,
        "right": "10dp",
        "skin": "sknBtnIcon36pxRed",
        "text": "X ",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 2, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblSeparator1 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSeparator1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorRed",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxWelcomeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxWelcomeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWelcomeContainer.setDefaultUnit(kony.flex.DP);
    var lblIconProfile = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "lblIconProfile",
        "isVisible": false,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "P",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblWelcome = new kony.ui.Label({
        "centerY": "60%",
        "id": "lblWelcome",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamMedium32pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.lblWelcome"),
        "textStyle": {},
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxWelcomeContainer.add(lblIconProfile, lblWelcome);
    flxShortProfile.add(lblProfileName, btnMyAccount, btnSignIn, btnSignUp, btnDown, lblSeparator1, flxWelcomeContainer);
    var flxStoreInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "48dp",
        "id": "flxStoreInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreInfo.setDefaultUnit(kony.flex.DP);
    var lblIconStore = new kony.ui.Label({
        "height": "40dp",
        "id": "lblIconStore",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "V",
        "textStyle": {},
        "top": "5dp",
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMyStore = new kony.ui.Label({
        "id": "lblMyStore",
        "isVisible": true,
        "left": "49dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.lblMyStore"),
        "textStyle": {},
        "top": "12dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var labelSeparator = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "labelSeparator",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorRed",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxStoreInfo.add(lblIconStore, lblMyStore, labelSeparator);
    var flxStoreMap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreMap",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreMap.setDefaultUnit(kony.flex.DP);
    var lblMyStoreName = new kony.ui.Label({
        "id": "lblMyStoreName",
        "isVisible": true,
        "left": "49dp",
        "skin": "sknLbl28pxGothamBookRed",
        "text": "Prestonwood Town Center",
        "textStyle": {},
        "top": "6dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnStoreMap = new kony.ui.Button({
        "bottom": "10dp",
        "focusSkin": "sknBtnRed24pxMedFoc",
        "height": "30dp",
        "id": "btnStoreMap",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknBtnRed24pxMed",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "top": "10dp",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblSeparator2 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSeparator2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorRed",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxStoreMap.add(lblMyStoreName, btnStoreMap, lblSeparator2);
    var flxRewardMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "48dp",
        "id": "flxRewardMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRewardMainContainer.setDefaultUnit(kony.flex.DP);
    var flxMainReward = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "48dp",
        "id": "flxMainReward",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxMainReward.setDefaultUnit(kony.flex.DP);
    var CopylblIconRewards017c338fec13e44 = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconRewards017c338fec13e44",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "R",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIconDropdownRewards = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "lblIconDropdownRewards",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon40pxRed",
        "text": "D",
        "textStyle": {},
        "width": "40dp",
        "zIndex": 4
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblMyRewardsCard0c884c7d0e9f946 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyRewardsCard0c884c7d0e9f946",
        "isVisible": true,
        "left": "49dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.michaelRewards"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnMichaelsRewards = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhiteFoc",
        "height": "100%",
        "id": "btnMichaelsRewards",
        "isVisible": true,
        "left": "0dp",
        "right": "50dp",
        "skin": "sknBtnIcon36pxWhite",
        "top": "0dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": false,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxMainReward.add(CopylblIconRewards017c338fec13e44, lblIconDropdownRewards, CopylblMyRewardsCard0c884c7d0e9f946, btnMichaelsRewards);
    var CopylblSeparator0e922236f2ea140 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator0e922236f2ea140",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorGray",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyflxMainReward0398206ddf00541 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "31dp",
        "id": "CopyflxMainReward0398206ddf00541",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "97%"
    }, {}, {});
    CopyflxMainReward0398206ddf00541.setDefaultUnit(kony.flex.DP);
    var CopylblIconRewards0134ed984ee6b4b = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "CopylblIconRewards0134ed984ee6b4b",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "g",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblIconDropdown0cb91a4bf71574e = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconDropdown0cb91a4bf71574e",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblIcon40pxRed",
        "text": "U",
        "textStyle": {},
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblMyRewardsCard0563f26fa115a4e = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyRewardsCard0563f26fa115a4e",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.rewardsCard"),
        "textStyle": {},
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnRewards059f1ae1c5e4d48 = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhiteFoc",
        "height": "100%",
        "id": "CopybtnRewards059f1ae1c5e4d48",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": false,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    CopyflxMainReward0398206ddf00541.add(CopylblIconRewards0134ed984ee6b4b, CopylblIconDropdown0cb91a4bf71574e, CopylblMyRewardsCard0563f26fa115a4e, CopybtnRewards059f1ae1c5e4d48);
    var flxRewardInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "125dp",
        "id": "flxRewardInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "97%",
        "zIndex": 1
    }, {}, {});
    flxRewardInfo.setDefaultUnit(kony.flex.DP);
    var lblRewardCardHolderName = new kony.ui.Label({
        "id": "lblRewardCardHolderName",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamRegular24pxBlack",
        "text": "Join Michaels Rewards Program",
        "textStyle": {},
        "top": "6dp",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgBarCode = new kony.ui.Image2({
        "centerX": "50%",
        "height": "46dp",
        "id": "imgBarCode",
        "imageWhenFailed": "notavailable_1080x142.png",
        "imageWhileDownloading": "white_1080x142.png",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "20dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnRewardsSignUp = new kony.ui.Button({
        "focusSkin": "sknBtnRed24pxMedFoc",
        "height": "30dp",
        "id": "btnRewardsSignUp",
        "isVisible": false,
        "left": "50dp",
        "skin": "sknBtnRed24pxMed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.btnSignUp"),
        "top": "40dp",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnAddToWallet = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnCouponExpandFoc",
        "height": "28dp",
        "id": "btnAddToWallet",
        "isVisible": true,
        "left": "3%",
        "skin": "sknBtnCouponExpand",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCoupons.addToWallet"),
        "top": "66dp",
        "width": "150dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxRewardInfo.add(lblRewardCardHolderName, imgBarCode, btnRewardsSignUp, btnAddToWallet);
    var CopylblSeparator09c83c445c20c4c = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator09c83c445c20c4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnMyRewards = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "btnMyRewards",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "97%"
    }, {}, {});
    btnMyRewards.setDefaultUnit(kony.flex.DP);
    var CopylblIconRewards01a0124e2ef0c4b = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconRewards01a0124e2ef0c4b",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "h",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblIconDropdown0853c8bb006f14c = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconDropdown0853c8bb006f14c",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblIcon40pxRed",
        "text": "D",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblMyRewardsCard06a47d7fa5f084d = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyRewardsCard06a47d7fa5f084d",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.MyRewardsLwrletters"),
        "textStyle": {},
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnRewards06efaa4a4fbc848 = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhiteFoc",
        "height": "100%",
        "id": "CopybtnRewards06efaa4a4fbc848",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": false,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    btnMyRewards.add(CopylblIconRewards01a0124e2ef0c4b, CopylblIconDropdown0853c8bb006f14c, CopylblMyRewardsCard06a47d7fa5f084d, CopybtnRewards06efaa4a4fbc848);
    var CopylblSeparator0d2e11b9e7b4748 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator0d2e11b9e7b4748",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblSeparator0d4cdc9aebeff47 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator0d4cdc9aebeff47",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblSeparatorGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnMyRewards0f04f9fe33ef747 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "CopybtnMyRewards0f04f9fe33ef747",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "97%"
    }, {}, {});
    CopybtnMyRewards0f04f9fe33ef747.setDefaultUnit(kony.flex.DP);
    var CopylblIconRewards0174a457002d34e = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconRewards0174a457002d34e",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "j",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblIconDropdown02c95484facf849 = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconDropdown02c95484facf849",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblIcon40pxRed",
        "text": "D",
        "textStyle": {},
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblMyRewardsCard012f96ac058074f = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyRewardsCard012f96ac058074f",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.rewardsProfile"),
        "textStyle": {},
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnRewardsProfile1 = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhiteFoc",
        "height": "100%",
        "id": "btnRewardsProfile1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": false,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    CopybtnMyRewards0f04f9fe33ef747.add(CopylblIconRewards0174a457002d34e, CopylblIconDropdown02c95484facf849, CopylblMyRewardsCard012f96ac058074f, btnRewardsProfile1);
    var CopylblSeparator0dca3cc96d91743 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator0dca3cc96d91743",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorGray",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxRewards = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxRewards",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "3%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "97%",
        "zIndex": 1
    }, {}, {});
    flxRewards.setDefaultUnit(kony.flex.DP);
    var flxRewardTxt = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "49dp",
        "id": "flxRewardTxt",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxRewardTxt.setDefaultUnit(kony.flex.DP);
    var lblIconRewards = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "lblIconRewards",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "i",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIconDropdown1 = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "lblIconDropdown1",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblIcon40pxRed",
        "text": "D",
        "textStyle": {},
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMyRewardsCard = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMyRewardsCard",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmRewardsProgramInfo.ProgramInfo"),
        "textStyle": {},
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnRewardsProgmInfo = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhiteFoc",
        "height": "100%",
        "id": "btnRewardsProgmInfo",
        "isVisible": true,
        "skin": "sknBtnIcon36pxWhite",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": false,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxRewardTxt.add(lblIconRewards, lblIconDropdown1, lblMyRewardsCard, btnRewardsProgmInfo);
    var lblSeparator3 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSeparator3",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRewards.add(flxRewardTxt, lblSeparator3);
    flxRewardMainContainer.add(flxMainReward, CopylblSeparator0e922236f2ea140, CopyflxMainReward0398206ddf00541, flxRewardInfo, CopylblSeparator09c83c445c20c4c, btnMyRewards, CopylblSeparator0d2e11b9e7b4748, CopylblSeparator0d4cdc9aebeff47, CopybtnMyRewards0f04f9fe33ef747, CopylblSeparator0dca3cc96d91743, flxRewards);
    var flexRewardsProfile = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "48dp",
        "id": "flexRewardsProfile",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flexRewardsProfile.setDefaultUnit(kony.flex.DP);
    var CopylblSeparator06d70efdae8cc48 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator06d70efdae8cc48",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorRed",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblIconRewards004f712d26cb044 = new kony.ui.Label({
        "centerY": "50%",
        "height": "30dp",
        "id": "CopylblIconRewards004f712d26cb044",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconMoreMenu",
        "text": "j",
        "textStyle": {},
        "width": "30dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblIconDropdown04915d5d9686e44 = new kony.ui.Label({
        "centerY": "50%",
        "height": "40dp",
        "id": "CopylblIconDropdown04915d5d9686e44",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblIcon40pxRed",
        "text": "D",
        "textStyle": {},
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblMyRewardsCard081998359f0ab48 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblMyRewardsCard081998359f0ab48",
        "isVisible": true,
        "left": "50dp",
        "skin": "sknLblGothamMedium32pxRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMore.rewardsProfile"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnRewards095ad7242c64d4d = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhiteFoc",
        "height": "100%",
        "id": "CopybtnRewards095ad7242c64d4d",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": false,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var CopylblSeparator035933ff5149a40 = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "CopylblSeparator035933ff5149a40",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorRed",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnRewardsProfile = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhite",
        "height": "100%",
        "id": "btnRewardsProfile",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 4
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flexRewardsProfile.add(CopylblSeparator06d70efdae8cc48, CopylblIconRewards004f712d26cb044, CopylblIconDropdown04915d5d9686e44, CopylblMyRewardsCard081998359f0ab48, CopybtnRewards095ad7242c64d4d, CopylblSeparator035933ff5149a40, btnRewardsProfile);
    var lblStoreRewardsGap = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblStoreRewardsGap",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorRed",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxScrlSegment = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "255dp",
        "horizontalScrollIndicator": true,
        "id": "flxScrlSegment",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "bouncesZoom": false
    });
    flxScrlSegment.setDefaultUnit(kony.flex.DP);
    var segMenuList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblIconMenuArrow": "",
            "lblIconMenuList": "",
            "lblMenuListItem": ""
        }],
        "groupCells": false,
        "id": "segMenuList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "segMenuListFocus",
        "rowSkin": "segMenuListNormal",
        "rowTemplate": flexMenuListContainer,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "0000005a",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flexMenuListContainer": "flexMenuListContainer",
            "lblIconMenuArrow": "lblIconMenuArrow",
            "lblIconMenuList": "lblIconMenuList",
            "lblMenuListItem": "lblMenuListItem"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    flxScrlSegment.add(segMenuList);
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "54dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flxMenuList.add(flxShortProfile, flxStoreInfo, flxStoreMap, flxRewardMainContainer, flexRewardsProfile, lblStoreRewardsGap, flxScrlSegment, flxBottomSpacerDND);
    var kmb48d3384a3146848028c462a79dd193 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    kmb48d3384a3146848028c462a79dd193.setDefaultUnit(kony.flex.DP);
    var km3528c5eddcf40bfa20422696f7aaa41 = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km3528c5eddcf40bfa20422696f7aaa41.setDefaultUnit(kony.flex.DP);
    km3528c5eddcf40bfa20422696f7aaa41.add();
    var kmbf5d72ddc86403dbe3b89e489e90b24 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    kmbf5d72ddc86403dbe3b89e489e90b24.setDefaultUnit(kony.flex.DP);
    var kmba910fcda224febaa202340cef8fa3d = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km5ecf056c04d47f6aa6325236f152d1e = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmf334d0ad4b7471ab91ad9c17a04ba54 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmf2a675ea0d44c79920e3f2e06d302f6 = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    kmf2a675ea0d44c79920e3f2e06d302f6.setDefaultUnit(kony.flex.DP);
    kmf2a675ea0d44c79920e3f2e06d302f6.add();
    var km604ddb7092a4feab2080bf3a099d45c = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmbf5d72ddc86403dbe3b89e489e90b24.add(kmba910fcda224febaa202340cef8fa3d, km5ecf056c04d47f6aa6325236f152d1e, kmf334d0ad4b7471ab91ad9c17a04ba54, kmf2a675ea0d44c79920e3f2e06d302f6, km604ddb7092a4feab2080bf3a099d45c);
    var kmbcbd55d499e407185dfc11d32755fc5 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmbcbd55d499e407185dfc11d32755fc5.setDefaultUnit(kony.flex.DP);
    var km828f6f3eb5d488f9192d7188d8fb5d3 = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km828f6f3eb5d488f9192d7188d8fb5d3.setDefaultUnit(kony.flex.DP);
    var kmdcc4b37ebca429084e84f1400c4e351 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    kmdcc4b37ebca429084e84f1400c4e351.setDefaultUnit(kony.flex.DP);
    var kmc4a4692104741d8ab978cf02ecddfae = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmdcc4b37ebca429084e84f1400c4e351.add(kmc4a4692104741d8ab978cf02ecddfae);
    var km59e69bb12d549efbc68fd0854cb6839 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    km59e69bb12d549efbc68fd0854cb6839.setDefaultUnit(kony.flex.DP);
    var kmdd15862a0e149d89708e4be966e5268 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km59e69bb12d549efbc68fd0854cb6839.add(kmdd15862a0e149d89708e4be966e5268);
    var km3cb153ffdd347ffa8cb2c21415b550e = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km3cb153ffdd347ffa8cb2c21415b550e.setDefaultUnit(kony.flex.DP);
    var kmb4356f289e44621b9d99039afe9c7d4 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var kmc59162b0d5f4c90abfb07491d5dbf4b = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    km3cb153ffdd347ffa8cb2c21415b550e.add(kmb4356f289e44621b9d99039afe9c7d4, kmc59162b0d5f4c90abfb07491d5dbf4b);
    var kmd71dc1306414517b08887e5329a5511 = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km828f6f3eb5d488f9192d7188d8fb5d3.add(kmdcc4b37ebca429084e84f1400c4e351, km59e69bb12d549efbc68fd0854cb6839, km3cb153ffdd347ffa8cb2c21415b550e, kmd71dc1306414517b08887e5329a5511);
    var km2fb631da8a7423a8df6736502b60d06 = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km2fb631da8a7423a8df6736502b60d06.setDefaultUnit(kony.flex.DP);
    km2fb631da8a7423a8df6736502b60d06.add();
    kmbcbd55d499e407185dfc11d32755fc5.add(km828f6f3eb5d488f9192d7188d8fb5d3, km2fb631da8a7423a8df6736502b60d06);
    var kmc72676f54f142ddbe694769165f38a3 = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmc72676f54f142ddbe694769165f38a3.setDefaultUnit(kony.flex.DP);
    var km0c5d94b3f3e463fb39a18d04a677b94 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km0c5d94b3f3e463fb39a18d04a677b94.setDefaultUnit(kony.flex.DP);
    var km531ff8df0084e67b1984e864739be83 = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km531ff8df0084e67b1984e864739be83.setDefaultUnit(kony.flex.DP);
    var kma5d8de9f87c4d568bfdc8f04f59f345 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km892f85a71ea4dab82032cad772894ec = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km531ff8df0084e67b1984e864739be83.add(kma5d8de9f87c4d568bfdc8f04f59f345, km892f85a71ea4dab82032cad772894ec);
    var km01cfc382f8f4f61b3e56c493edafd63 = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km01cfc382f8f4f61b3e56c493edafd63.setDefaultUnit(kony.flex.DP);
    var kmf75b9bff7c445129053d8f136fd211a = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmf75b9bff7c445129053d8f136fd211a.setDefaultUnit(kony.flex.DP);
    var km8101c90c20a4142b54540c20342fef4 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km17ea673ada2406e897772c2c69858f4 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc0d621d5641459daaece927e5a35379 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km48b467ac91c46038076316b93890591 = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km96719d0f8ec4be69bf027cbb3091799 = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmaf48bcb4a0548e88e2bd9371177da78 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf75b9bff7c445129053d8f136fd211a.add(km8101c90c20a4142b54540c20342fef4, km17ea673ada2406e897772c2c69858f4, kmc0d621d5641459daaece927e5a35379, km48b467ac91c46038076316b93890591, km96719d0f8ec4be69bf027cbb3091799, kmaf48bcb4a0548e88e2bd9371177da78);
    var kma9b4ea7b71041feb63d9569c228ab8b = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kma9b4ea7b71041feb63d9569c228ab8b.setDefaultUnit(kony.flex.DP);
    var km93782addbc84a01b95592f50d18c2da = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km52642cbdb15421da74f16c3eed05cce = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km16ca0e7585f487db0a3ff8ed1e17349 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kma9b4ea7b71041feb63d9569c228ab8b.add(km93782addbc84a01b95592f50d18c2da, km52642cbdb15421da74f16c3eed05cce, km16ca0e7585f487db0a3ff8ed1e17349);
    km01cfc382f8f4f61b3e56c493edafd63.add(kmf75b9bff7c445129053d8f136fd211a, kma9b4ea7b71041feb63d9569c228ab8b);
    var km7f41c9d119142a088274ae648767fc7 = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km07dc1fd27134306bb68c700828402fd = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km0c5d94b3f3e463fb39a18d04a677b94.add(km531ff8df0084e67b1984e864739be83, km01cfc382f8f4f61b3e56c493edafd63, km7f41c9d119142a088274ae648767fc7, km07dc1fd27134306bb68c700828402fd);
    kmc72676f54f142ddbe694769165f38a3.add(km0c5d94b3f3e463fb39a18d04a677b94);
    kmb48d3384a3146848028c462a79dd193.add(km3528c5eddcf40bfa20422696f7aaa41, kmbf5d72ddc86403dbe3b89e489e90b24, kmbcbd55d499e407185dfc11d32755fc5, kmc72676f54f142ddbe694769165f38a3);
    var kmaf1881457974b1eb11a3534af8c3b9d = new kony.ui.FlexScrollContainer({
        "isMaster": true,
        "id": "flxSearchResultsContainer",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 6
    }, {}, {});
    kmaf1881457974b1eb11a3534af8c3b9d.setDefaultUnit(kony.flex.DP);
    var kma0fb22d54a144fa8c5d41e7c9eb1b40 = new kony.ui.FlexContainer({
        "id": "flxTopSpacerDNDSearch",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kma0fb22d54a144fa8c5d41e7c9eb1b40.setDefaultUnit(kony.flex.DP);
    kma0fb22d54a144fa8c5d41e7c9eb1b40.add();
    var km3775412eadf4beaa25d8a6736c61622 = new kony.ui.SegmentedUI2({
        "id": "segSearchOffers",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchOffer": "25% Off of all Yarn This Week"
        }, {
            "lblSearchOffer": "FREE In-Store Yarn Event"
        }, {
            "lblSearchOffer": "20% Off Your Entire Purchase"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowBlack7",
        "rowTemplate": flxSearchOffer,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "dadada00",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchOffer": "flxSearchOffer",
            "lblSearchOffer": "lblSearchOffer"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    var kmd044968de844b65b60ea9b70095dc2d = new kony.ui.SegmentedUI2({
        "id": "segSearchResults",
        "scrollingEvents": {},
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblSearchResults": "Paint",
            "lblSearchResultsCount": "(12,563)"
        }, {
            "lblSearchResults": "Paint Colors",
            "lblSearchResultsCount": "(1025)"
        }, {
            "lblSearchResults": "Paint Thinners",
            "lblSearchResultsCount": "(12)"
        }, {
            "lblSearchResults": "Paint Brushes",
            "lblSearchResultsCount": "(362)"
        }, {
            "lblSearchResults": "Paint Canvas",
            "lblSearchResultsCount": "(256)"
        }, {
            "lblSearchResults": "Paint Cart",
            "lblSearchResultsCount": "(64)"
        }, {
            "lblSearchResults": "Paint Mixers",
            "lblSearchResultsCount": "(32)"
        }, {
            "lblSearchResults": "Paint Acrylic",
            "lblSearchResultsCount": "(16)"
        }, {
            "lblSearchResults": "Paint Oil",
            "lblSearchResultsCount": "(128)"
        }],
        "groupCells": false,
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknSegRowBlack20",
        "rowSkin": "sknSegRowWhite",
        "rowTemplate": flxSearchResults,
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxSearchResults": "flxSearchResults",
            "lblSearchResults": "lblSearchResults",
            "lblSearchResultsCount": "lblSearchResultsCount"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    kmaf1881457974b1eb11a3534af8c3b9d.add(kma0fb22d54a144fa8c5d41e7c9eb1b40, km3775412eadf4beaa25d8a6736c61622, kmd044968de844b65b60ea9b70095dc2d);
    var kmb40613c0da1427bb034cd5867027876 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb40613c0da1427bb034cd5867027876.setDefaultUnit(kony.flex.DP);
    var km62b0ee1d41544339b0e76fddb297918 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    km62b0ee1d41544339b0e76fddb297918.setDefaultUnit(kony.flex.DP);
    var kmd32218280f347c5b4bf68eb0af36806 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 8,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kmd32218280f347c5b4bf68eb0af36806.setDefaultUnit(kony.flex.DP);
    var kmd6af447c54c4001b258dd8b1f7a956a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    kmd6af447c54c4001b258dd8b1f7a956a.setDefaultUnit(kony.flex.DP);
    kmd6af447c54c4001b258dd8b1f7a956a.add();
    var km8d88bbdf0dd4ee5881f792535423fa0 = new kony.ui.Image2({
        "height": "20dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km15bd3ebcbd44859a65e50a928835e5c = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "30dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 1,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km74fd1ccf8844ffbb5ac37c59c8e9274 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "left": "14%",
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km74fd1ccf8844ffbb5ac37c59c8e9274.setDefaultUnit(kony.flex.DP);
    var km74c7d4ca88a469484b09076174ed593 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km5010b50093b41699538d533e3fefc6f = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {},
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km74fd1ccf8844ffbb5ac37c59c8e9274.add(km74c7d4ca88a469484b09076174ed593, km5010b50093b41699538d533e3fefc6f);
    var kmfd0e89fc8e8474e95a225eb13277cc2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmfd0e89fc8e8474e95a225eb13277cc2.setDefaultUnit(kony.flex.DP);
    var kmc27994958d4424798a8598314aba2f8 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {},
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kma7b76fdf61c4428b3164f803313474a = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "captureOrientation": constants.CAMERA_CAPTURE_ORIENTATION_PORTRAIT,
        "enableOverlay": true,
        "nativeUserInterface": false,
        "overlayConfig": {
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay
        }
    });
    kmfd0e89fc8e8474e95a225eb13277cc2.add(kmc27994958d4424798a8598314aba2f8, kma7b76fdf61c4428b3164f803313474a);
    var kmf8a5acd9e1947e386a32af542d9110e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "66%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf8a5acd9e1947e386a32af542d9110e.setDefaultUnit(kony.flex.DP);
    var km0682ca3d4bd4a5f8bd2e087f1768c81 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km0bf5c8c52d24a898531a1d032a633a0 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {},
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmf8a5acd9e1947e386a32af542d9110e.add(km0682ca3d4bd4a5f8bd2e087f1768c81, km0bf5c8c52d24a898531a1d032a633a0);
    kmd32218280f347c5b4bf68eb0af36806.add(kmd6af447c54c4001b258dd8b1f7a956a, km8d88bbdf0dd4ee5881f792535423fa0, km15bd3ebcbd44859a65e50a928835e5c, km74fd1ccf8844ffbb5ac37c59c8e9274, kmfd0e89fc8e8474e95a225eb13277cc2, kmf8a5acd9e1947e386a32af542d9110e);
    km62b0ee1d41544339b0e76fddb297918.add(kmd32218280f347c5b4bf68eb0af36806);
    kmb40613c0da1427bb034cd5867027876.add(km62b0ee1d41544339b0e76fddb297918);
    var km563595e83a44b069e6ac69c1019c303 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km563595e83a44b069e6ac69c1019c303.setDefaultUnit(kony.flex.DP);
    var km4d36206c3a34422a637af914aebcc4b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    km4d36206c3a34422a637af914aebcc4b.setDefaultUnit(kony.flex.DP);
    var kmfd69410b47c405fa58cbd75d42f86c2 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmfd69410b47c405fa58cbd75d42f86c2.setDefaultUnit(kony.flex.DP);
    var km14d00a9fef24b06b3d1fd591175b111 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km64cd147d2114bdb9ba61ac78434194c = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km64cd147d2114bdb9ba61ac78434194c.setDefaultUnit(kony.flex.DP);
    var km7138986085c4b9695f6b75b5ce861aa = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km4671a3602e54614b4e0f326761225b3 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmf1d3fdb1deb4023aa07a1505db1fab8 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km64cd147d2114bdb9ba61ac78434194c.add(km7138986085c4b9695f6b75b5ce861aa, km4671a3602e54614b4e0f326761225b3, kmf1d3fdb1deb4023aa07a1505db1fab8);
    var km57bb6a6490b43e2aa5088e45b45f929 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmb54b73f77c4409da44d886fb953ce1b = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmfd69410b47c405fa58cbd75d42f86c2.add(km14d00a9fef24b06b3d1fd591175b111, km64cd147d2114bdb9ba61ac78434194c, km57bb6a6490b43e2aa5088e45b45f929, kmb54b73f77c4409da44d886fb953ce1b);
    var kmb0e34acfaa24704ad30d3f4b8b6ab86 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    kmb0e34acfaa24704ad30d3f4b8b6ab86.setDefaultUnit(kony.flex.DP);
    var kmb46b79fe61f407382c1112a13196acc = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb46b79fe61f407382c1112a13196acc.setDefaultUnit(kony.flex.DP);
    var km44310b36eda4d7788f51389bc38c883 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmb46b79fe61f407382c1112a13196acc.add(km44310b36eda4d7788f51389bc38c883);
    kmb0e34acfaa24704ad30d3f4b8b6ab86.add(kmb46b79fe61f407382c1112a13196acc);
    var kme7e0e4550a6427aa65c31cde3b00a3b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km4d36206c3a34422a637af914aebcc4b.add(kmfd69410b47c405fa58cbd75d42f86c2, kmb0e34acfaa24704ad30d3f4b8b6ab86, kme7e0e4550a6427aa65c31cde3b00a3b);
    km563595e83a44b069e6ac69c1019c303.add(km4d36206c3a34422a637af914aebcc4b);
    var kmde98ab8d1534593a16ed94ca0812b00 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    kmde98ab8d1534593a16ed94ca0812b00.setDefaultUnit(kony.flex.DP);
    var km0a595c5a65948228573e8a93ee19e7c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0a595c5a65948228573e8a93ee19e7c.setDefaultUnit(kony.flex.DP);
    var km1aca3ac943149a2b4b292552eb6b4e2 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmea52a768b5a4167960c90a4396745f1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km0a595c5a65948228573e8a93ee19e7c.add(km1aca3ac943149a2b4b292552eb6b4e2, kmea52a768b5a4167960c90a4396745f1);
    var km86b8ae3f55f4507a0c9e0a32b932a84 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km86b8ae3f55f4507a0c9e0a32b932a84.setDefaultUnit(kony.flex.DP);
    var km716c75c7b9c4a1a9f8de627170846e8 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km173e54b40da419d895010746200aae0 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km1af8814287b47338edb43199409995f = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km86b8ae3f55f4507a0c9e0a32b932a84.add(km716c75c7b9c4a1a9f8de627170846e8, km173e54b40da419d895010746200aae0, km1af8814287b47338edb43199409995f);
    var km355456734be4ba0be06c1db683d616e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km355456734be4ba0be06c1db683d616e.setDefaultUnit(kony.flex.DP);
    var km016013e398b41178ef262adae7de3e8 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kme225178c9ae430e9c4966de5f52263f = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km355456734be4ba0be06c1db683d616e.add(km016013e398b41178ef262adae7de3e8, kme225178c9ae430e9c4966de5f52263f);
    var kmc8083f6800e497ca47cf1b77f7d8b94 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc8083f6800e497ca47cf1b77f7d8b94.setDefaultUnit(kony.flex.DP);
    var km43e8684346d4e5dafef6cbb796787d3 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km04abf2153204c3c84bcbd99ec8886fe = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmc8083f6800e497ca47cf1b77f7d8b94.add(km43e8684346d4e5dafef6cbb796787d3, km04abf2153204c3c84bcbd99ec8886fe);
    var kmb154d77f6aa4ad59ea17c625378beef = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb154d77f6aa4ad59ea17c625378beef.setDefaultUnit(kony.flex.DP);
    var kmb1dd84fa6c24edfa206c69512f80eea = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km4640226c42e4f1e93cea8fa89ee1924 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmb154d77f6aa4ad59ea17c625378beef.add(kmb1dd84fa6c24edfa206c69512f80eea, km4640226c42e4f1e93cea8fa89ee1924);
    var kmf25b9de17df46349480bfe612db081f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf25b9de17df46349480bfe612db081f.setDefaultUnit(kony.flex.DP);
    var km2ea89c89df048b1a364d4d6428dcfde = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km12e9f3acd1e439dbbcceb0c2b615178 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmf25b9de17df46349480bfe612db081f.add(km2ea89c89df048b1a364d4d6428dcfde, km12e9f3acd1e439dbbcceb0c2b615178);
    var km197212c54b347ecbf804f3b10d42333 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km197212c54b347ecbf804f3b10d42333.setDefaultUnit(kony.flex.DP);
    var km9fe512335e34109beeab1adaea5a86b = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmb9b39888fde4a40a41ad188d265bfcb = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km197212c54b347ecbf804f3b10d42333.add(km9fe512335e34109beeab1adaea5a86b, kmb9b39888fde4a40a41ad188d265bfcb);
    var km47607b90f0e41a4abd39aa74c76e9de = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km47607b90f0e41a4abd39aa74c76e9de.setDefaultUnit(kony.flex.DP);
    var km299fcf8d28242f6bbd7c712b072dced = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km86ef459eb01403c9e8be88e40ecb7dc = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km47607b90f0e41a4abd39aa74c76e9de.add(km299fcf8d28242f6bbd7c712b072dced, km86ef459eb01403c9e8be88e40ecb7dc);
    kmde98ab8d1534593a16ed94ca0812b00.add(km0a595c5a65948228573e8a93ee19e7c, km86b8ae3f55f4507a0c9e0a32b932a84, km355456734be4ba0be06c1db683d616e, kmc8083f6800e497ca47cf1b77f7d8b94, kmb154d77f6aa4ad59ea17c625378beef, kmf25b9de17df46349480bfe612db081f, km197212c54b347ecbf804f3b10d42333, km47607b90f0e41a4abd39aa74c76e9de);
    var km50d6a1ae2e64c748ae283ef84ee0b6d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    km50d6a1ae2e64c748ae283ef84ee0b6d.setDefaultUnit(kony.flex.DP);
    var km9eeedaa589c4de8bc2869add3a652cb = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmad945dbfd06412895b3768d07f56351 = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    kmad945dbfd06412895b3768d07f56351.setDefaultUnit(kony.flex.DP);
    var km9ee933e68d542aab3f3d5feb4710481 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km9ee933e68d542aab3f3d5feb4710481.setDefaultUnit(kony.flex.DP);
    var kmda0f462ff04459d8756513f5bf5ece9 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km9ee933e68d542aab3f3d5feb4710481.add(kmda0f462ff04459d8756513f5bf5ece9);
    var kmb4beb1d5292427d97c1b8349bad16a2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmb4beb1d5292427d97c1b8349bad16a2.setDefaultUnit(kony.flex.DP);
    var km70813ab5c414a8d8e806d8c1114c3c0 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    kmb4beb1d5292427d97c1b8349bad16a2.add(km70813ab5c414a8d8e806d8c1114c3c0);
    var km48b3c89b7174176b9a9a90b47d5e401 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km48b3c89b7174176b9a9a90b47d5e401.setDefaultUnit(kony.flex.DP);
    var kma76ee1b68df4ca587198644bafb4d50 = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "captureOrientation": constants.CAMERA_CAPTURE_ORIENTATION_PORTRAIT,
        "enableOverlay": true,
        "nativeUserInterface": false,
        "overlayConfig": {
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay
        }
    });
    km48b3c89b7174176b9a9a90b47d5e401.add(kma76ee1b68df4ca587198644bafb4d50);
    var km6ddcba3d6ef4d0f8d5b74e3d02bcd4e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km6ddcba3d6ef4d0f8d5b74e3d02bcd4e.setDefaultUnit(kony.flex.DP);
    var km763988e787e4b5e8c1e36865fbefab4 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km6ddcba3d6ef4d0f8d5b74e3d02bcd4e.add(km763988e787e4b5e8c1e36865fbefab4);
    var kmca413d9741c4158ae35f59577e69be5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmca413d9741c4158ae35f59577e69be5.setDefaultUnit(kony.flex.DP);
    var km1763f1f75a945a3a44a1663aa12e706 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmca413d9741c4158ae35f59577e69be5.add(km1763f1f75a945a3a44a1663aa12e706);
    kmad945dbfd06412895b3768d07f56351.add(km9ee933e68d542aab3f3d5feb4710481, kmb4beb1d5292427d97c1b8349bad16a2, km48b3c89b7174176b9a9a90b47d5e401, km6ddcba3d6ef4d0f8d5b74e3d02bcd4e, kmca413d9741c4158ae35f59577e69be5);
    var km9403bfe56c24734b5f8b73dbeea9301 = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km38888644b124671b8ebba794d6a65d2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km38888644b124671b8ebba794d6a65d2.setDefaultUnit(kony.flex.DP);
    var kmcf785a5172f4deb8fdb4bd7c9e78ec0 = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km550aabf60204a7a8238f3a936201453 = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km38888644b124671b8ebba794d6a65d2.add(kmcf785a5172f4deb8fdb4bd7c9e78ec0, km550aabf60204a7a8238f3a936201453);
    km50d6a1ae2e64c748ae283ef84ee0b6d.add(km9eeedaa589c4de8bc2869add3a652cb, kmad945dbfd06412895b3768d07f56351, km9403bfe56c24734b5f8b73dbeea9301, km38888644b124671b8ebba794d6a65d2);
    frmMore.add(flxMenuList, kmb48d3384a3146848028c462a79dd193, kmaf1881457974b1eb11a3534af8c3b9d, kmb40613c0da1427bb034cd5867027876, km563595e83a44b069e6ac69c1019c303, kmde98ab8d1534593a16ed94ca0812b00, km50d6a1ae2e64c748ae283ef84ee0b6d);
};

function frmMoreGlobals() {
    frmMore = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMore,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmMore",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NONE,
        "inTransitionConfig": {
            "transitionDirection": "fromRight",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};