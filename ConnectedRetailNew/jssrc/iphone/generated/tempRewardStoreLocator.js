function initializetempRewardStoreLocator() {
    flxRewardStoreLoc = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxRewardStoreLoc",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite"
    }, {}, {});
    flxRewardStoreLoc.setDefaultUnit(kony.flex.DP);
    var flxStoreDetailsCollapse = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetailsCollapse",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxStoreDetailsCollapse.setDefaultUnit(kony.flex.DP);
    var btnFavorite = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxRed",
        "height": "75dp",
        "id": "btnFavorite",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxRed",
        "text": "f",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxStoreDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "60dp",
        "isModalContainer": false,
        "right": "3%",
        "skin": "slFbox",
        "top": "10dp",
        "zIndex": 1
    }, {}, {});
    flxStoreDetails.setDefaultUnit(kony.flex.DP);
    var lblAddressLine1 = new kony.ui.Label({
        "id": "lblAddressLine1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "5301 Beltline Rd, Ste 101",
        "textStyle": {
            "lineSpacing": 1,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAddressLine2 = new kony.ui.Label({
        "id": "lblAddressLine2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "Dallas, TX 75254",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPhone = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxRed",
        "id": "lblPhone",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTransReg24pxRed",
        "text": "972.385.9228",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxDirections = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "32dp",
        "id": "flxDirections",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDirections.setDefaultUnit(kony.flex.DP);
    var lblDistance = new kony.ui.Label({
        "height": "28dp",
        "id": "lblDistance",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "0.6 mi",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDirections.add(lblDistance);
    flxStoreDetails.add(lblAddressLine1, lblAddressLine2, lblPhone, flxDirections);
    var flxStockInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "45dp",
        "id": "flxStockInfo",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "sknFlexBgLtGray",
        "top": "5dp",
        "width": "70dp",
        "zIndex": 1
    }, {}, {});
    flxStockInfo.setDefaultUnit(kony.flex.DP);
    var lblStockTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "35%",
        "id": "lblStockTitle",
        "isVisible": true,
        "skin": "sknLblGothamMedium24px818181",
        "text": "In Stock",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStock = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "70%",
        "id": "lblStock",
        "isVisible": true,
        "skin": "sknLblGothamMedium28px818181",
        "text": "5",
        "top": "1dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxStockInfo.add(lblStockTitle, lblStock);
    flxStoreDetailsCollapse.add(btnFavorite, flxStoreDetails, flxStockInfo);
    var flxSeparatorTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxSeparatorTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSeparatorTop.setDefaultUnit(kony.flex.DP);
    flxSeparatorTop.add();
    flxRewardStoreLoc.add(flxStoreDetailsCollapse, flxSeparatorTop);
}