function addWidgetsfrmFindStoreMapView() {
    frmFindStoreMapView.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopysknFlxSboxBgWhite09fdf31c8974a4f",
        "top": "134dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxScrlMapList = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxScrlMapList",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrlMapList.setDefaultUnit(kony.flex.DP);
    var flxMapHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMapHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMapHolder.setDefaultUnit(kony.flex.DP);
    var mapStore = new kony.ui.Map({
        "bottom": "50dp",
        "calloutWidth": 1,
        "defaultPinImage": "transparent.png",
        "id": "mapStore",
        "isVisible": true,
        "left": "0dp",
        "provider": constants.MAP_PROVIDER_GOOGLE,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "mode": constants.MAP_VIEW_MODE_NORMAL,
        "showCurrentLocation": constants.MAP_VIEW_SHOW_CURRENT_LOCATION_AS_CIRCLE,
        "zoomLevel": 11
    });
    var flxMyStoreDetailsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMyStoreDetailsNew",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMyStoreDetailsNew.setDefaultUnit(kony.flex.DP);
    var flxMyStoreDetailsCollapseNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMyStoreDetailsCollapseNew",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxMyStoreDetailsCollapseNew.setDefaultUnit(kony.flex.DP);
    var btnFavoriteMyStore = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxRed",
        "height": "75dp",
        "id": "btnFavoriteMyStore",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxRed",
        "text": "F",
        "top": "5dp",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxStoreDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "60dp",
        "isModalContainer": false,
        "right": "3%",
        "skin": "slFbox",
        "top": "10dp",
        "zIndex": 1
    }, {}, {});
    flxStoreDetails.setDefaultUnit(kony.flex.DP);
    var flexComingSoon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "5dp",
        "clipBounds": true,
        "focusSkin": "sknFlexBgWhiteFoc",
        "height": "20dp",
        "id": "flexComingSoon",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "160dp",
        "zIndex": 1
    }, {}, {});
    flexComingSoon.setDefaultUnit(kony.flex.DP);
    var flexComingSoonText = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknFlexRedNoBorderFoc",
        "height": "20dp",
        "id": "flexComingSoonText",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexRedNoBorder",
        "top": "0dp",
        "width": "150dp",
        "zIndex": 1
    }, {}, {});
    flexComingSoonText.setDefaultUnit(kony.flex.DP);
    var lblComingSoon = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblComingSoon",
        "isVisible": true,
        "skin": "sknLblGothamRegular24pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.comingSoon"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flexFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20dp",
        "id": "flexFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-10dp",
        "skin": "sknFlexBgWhite",
        "top": "-10dp",
        "width": "20dp",
        "zIndex": 1
    }, {}, {});
    flexFlag.setDefaultUnit(kony.flex.DP);
    flexFlag.add();
    flexComingSoonText.add(lblComingSoon, flexFlag);
    flexComingSoon.add(flexComingSoonText);
    var lblStoreNameMap = new kony.ui.Label({
        "id": "lblStoreNameMap",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 1,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAddLine1 = new kony.ui.Label({
        "id": "lblAddLine1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 1,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAddLine2 = new kony.ui.Label({
        "id": "lblAddLine2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnPhoneNew = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxRed",
        "id": "btnPhoneNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnBgTransReg24pxBlue",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxDirectionsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "32dp",
        "id": "flxDirectionsNew",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDirectionsNew.setDefaultUnit(kony.flex.DP);
    var lblDistanceNew = new kony.ui.Label({
        "height": "28dp",
        "id": "lblDistanceNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnDirectionsNew = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "28dp",
        "id": "btnDirectionsNew",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknBtn13px489ca7GothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.btnDirections"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxDirectionsNew.add(lblDistanceNew, btnDirectionsNew);
    flxStoreDetails.add(flexComingSoon, lblStoreNameMap, lblAddLine1, lblAddLine2, btnPhoneNew, flxDirectionsNew);
    var lblAvailabilityMap = new kony.ui.Label({
        "height": "45dp",
        "id": "lblAvailabilityMap",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknLblGothamMedium81818124pxGrayBg",
        "text": "Not available",
        "top": "10dp",
        "width": "70dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxExpandDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "40dp",
        "id": "flxExpandDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "5dp",
        "skin": "slFbox",
        "width": "80dp",
        "zIndex": 5
    }, {}, {});
    flxExpandDetails.setDefaultUnit(kony.flex.DP);
    var lblStoreHoursExpand = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblStoreHoursExpand",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLbl9pxBlackGothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.lblStoreHoursSmall"),
        "top": "0dp",
        "width": "65dp",
        "zIndex": 4
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnExpandDetails = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "btnExpandDetails",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLbl9pxBlack",
        "text": "D",
        "width": "15dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxExpandDetails.add(lblStoreHoursExpand, btnExpandDetails);
    var lblTapToSelectMap = new kony.ui.Label({
        "centerX": "30dp",
        "id": "lblTapToSelectMap",
        "isVisible": true,
        "left": "9dp",
        "skin": "sknLbl8px66666",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.lblTapToSelect"),
        "top": "55dp",
        "width": "50dp",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMyStoreDetailsCollapseNew.add(btnFavoriteMyStore, flxStoreDetails, lblAvailabilityMap, flxExpandDetails, lblTapToSelectMap);
    var flxMyStoreDetailsExpandNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMyStoreDetailsExpandNew",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxMyStoreDetailsExpandNew.setDefaultUnit(kony.flex.DP);
    var flxStoreHorizontal = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreHorizontal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxStoreHorizontal.setDefaultUnit(kony.flex.DP);
    var flxStoreTimingsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreTimingsNew",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "60dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "60%"
    }, {}, {});
    flxStoreTimingsNew.setDefaultUnit(kony.flex.DP);
    var lblStoreHours = new kony.ui.Label({
        "id": "lblStoreHours",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.lblStoreHours"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxMon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMon",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMon.setDefaultUnit(kony.flex.DP);
    var lblDay1 = new kony.ui.Label({
        "id": "lblDay1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay1"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay1TimingNew = new kony.ui.Label({
        "id": "lblDay1TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMon.add(lblDay1, lblDay1TimingNew);
    var flxDay2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay2.setDefaultUnit(kony.flex.DP);
    var lblDay2 = new kony.ui.Label({
        "id": "lblDay2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay2"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay2TimingNew = new kony.ui.Label({
        "id": "lblDay2TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay2.add(lblDay2, lblDay2TimingNew);
    var flexDay3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexDay3",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexDay3.setDefaultUnit(kony.flex.DP);
    var lblDay3 = new kony.ui.Label({
        "id": "lblDay3",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay3"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay3TimingNew = new kony.ui.Label({
        "id": "lblDay3TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flexDay3.add(lblDay3, lblDay3TimingNew);
    var flxDay4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay4",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay4.setDefaultUnit(kony.flex.DP);
    var lblDay4 = new kony.ui.Label({
        "id": "lblDay4",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay4"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay4TimingNew = new kony.ui.Label({
        "id": "lblDay4TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay4.add(lblDay4, lblDay4TimingNew);
    var flxDay5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay5",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay5.setDefaultUnit(kony.flex.DP);
    var lblDay5 = new kony.ui.Label({
        "id": "lblDay5",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay5"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay5TimingNew = new kony.ui.Label({
        "id": "lblDay5TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay5.add(lblDay5, lblDay5TimingNew);
    var flxDay6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay6",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay6.setDefaultUnit(kony.flex.DP);
    var lblDay6 = new kony.ui.Label({
        "id": "lblDay6",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay6"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay6TimingNew = new kony.ui.Label({
        "id": "lblDay6TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay6.add(lblDay6, lblDay6TimingNew);
    var flxDay7 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay7",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay7.setDefaultUnit(kony.flex.DP);
    var lblDay7 = new kony.ui.Label({
        "id": "lblDay7",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": kony.i18n.getLocalizedString("i18n.phone.tempStoreDetails.lblDay7"),
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDay7TimingNew = new kony.ui.Label({
        "id": "lblDay7TimingNew",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px333333",
        "text": "9:00 AM - 9:00 PM",
        "textStyle": {
            "lineSpacing": 6,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay7.add(lblDay7, lblDay7TimingNew);
    flxStoreTimingsNew.add(lblStoreHours, flxMon, flxDay2, flexDay3, flxDay4, flxDay5, flxDay6, flxDay7);
    var btnCollapseNew = new kony.ui.Button({
        "bottom": "0dp",
        "focusSkin": "sknBtnIcon36pxDark",
        "height": "60dp",
        "id": "btnCollapseNew",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnIcon36pxDark",
        "text": "U",
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxStoreHorizontal.add(flxStoreTimingsNew, btnCollapseNew);
    var flxItemLocationMap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxItemLocationMap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemLocationMap.setDefaultUnit(kony.flex.DP);
    var lblAisleNoMap = new kony.ui.Label({
        "height": "100%",
        "id": "lblAisleNoMap",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxBlue",
        "text": "AISLE 55",
        "top": "0dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMapIconMap = new kony.ui.Label({
        "height": "100%",
        "id": "lblMapIconMap",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon34pxBlue",
        "text": "e",
        "top": "0dp",
        "width": "12%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnStoreMapMap = new kony.ui.Button({
        "focusSkin": "sknBtnGothamMedium24pxBlueFocus",
        "height": "100%",
        "id": "btnStoreMapMap",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnGothamMedium24pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 12, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxSeparatorTopMap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxSeparatorTopMap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSeparatorTopMap.setDefaultUnit(kony.flex.DP);
    flxSeparatorTopMap.add();
    flxItemLocationMap.add(lblAisleNoMap, lblMapIconMap, btnStoreMapMap, flxSeparatorTopMap);
    flxMyStoreDetailsExpandNew.add(flxStoreHorizontal, flxItemLocationMap);
    var flxSeparatorBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxSeparatorBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSeparatorBottom.setDefaultUnit(kony.flex.DP);
    flxSeparatorBottom.add();
    flxMyStoreDetailsNew.add(flxMyStoreDetailsCollapseNew, flxMyStoreDetailsExpandNew, flxSeparatorBottom);
    flxMapHolder.add(mapStore, flxMyStoreDetailsNew);
    var flxListView = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxListView",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "100%",
        "isModalContainer": false,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxListView.setDefaultUnit(kony.flex.DP);
    var segStoresListView = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "btnCollapse": "U",
            "btnDirections": "DIRECTIONS",
            "btnExpand": "Label",
            "btnFavorite": "F",
            "btnStoreMap": "STORE MAP",
            "lblAddressLine1": "5301 Beltline Rd, Ste 101",
            "lblAddressLine2": "Dallas, TX 75254",
            "lblComingSoon": "0.6 mi",
            "lblDay1": "Mon:",
            "lblDay1Time": "9:00 AM - 9:00 PM",
            "lblDay2": "Tue:",
            "lblDay2Time": "9:00 AM - 9:00 PM",
            "lblDay3": "Wed:",
            "lblDay3Time": "9:00 AM - 9:00 PM",
            "lblDay4": "Thu:",
            "lblDay4Time": "9:00 AM - 9:00 PM",
            "lblDay5": "Fri:",
            "lblDay5Time": "9:00 AM - 9:00 PM",
            "lblDay6": "Sat:",
            "lblDay6Time": "9:00 AM - 9:00 PM",
            "lblDay7": "Sun:",
            "lblDay7Time": "9:00 AM - 9:00 PM",
            "lblDistance": "0.6 mi",
            "lblGap": "STORE HOURS",
            "lblMapIcon": "e",
            "lblNameStore": "",
            "lblPhone": "972.385.9228",
            "lblStock": "5",
            "lblStockTitle": "In Stock",
            "lblStoreHours": "STORE HOURS",
            "lblStoreHoursNew": "Label",
            "lblTapToSelect": "Label"
        }, {
            "btnCollapse": "U",
            "btnDirections": "DIRECTIONS",
            "btnExpand": "Label",
            "btnFavorite": "f",
            "btnStoreMap": "STORE MAP",
            "lblAddressLine1": "5301 Beltline Rd, Ste 101",
            "lblAddressLine2": "Dallas, TX 75254",
            "lblComingSoon": "0.6 mi",
            "lblDay1": "Mon:",
            "lblDay1Time": "9:00 AM - 9:00 PM",
            "lblDay2": "Tue:",
            "lblDay2Time": "9:00 AM - 9:00 PM",
            "lblDay3": "Wed:",
            "lblDay3Time": "9:00 AM - 9:00 PM",
            "lblDay4": "Thu:",
            "lblDay4Time": "9:00 AM - 9:00 PM",
            "lblDay5": "Fri:",
            "lblDay5Time": "9:00 AM - 9:00 PM",
            "lblDay6": "Sat:",
            "lblDay6Time": "9:00 AM - 9:00 PM",
            "lblDay7": "Sun:",
            "lblDay7Time": "9:00 AM - 9:00 PM",
            "lblDistance": "0.6 mi",
            "lblGap": "STORE HOURS",
            "lblMapIcon": "e",
            "lblNameStore": "",
            "lblPhone": "972.385.9228",
            "lblStock": "20+",
            "lblStockTitle": "In Stock",
            "lblStoreHours": "STORE HOURS",
            "lblStoreHoursNew": "Label",
            "lblTapToSelect": "Label"
        }, {
            "btnCollapse": "U",
            "btnDirections": "DIRECTIONS",
            "btnExpand": "Label",
            "btnFavorite": "f",
            "btnStoreMap": "STORE MAP",
            "lblAddressLine1": "5301 Beltline Rd, Ste 101",
            "lblAddressLine2": "Dallas, TX 75254",
            "lblComingSoon": "0.6 mi",
            "lblDay1": "Mon:",
            "lblDay1Time": "9:00 AM - 9:00 PM",
            "lblDay2": "Tue:",
            "lblDay2Time": "9:00 AM - 9:00 PM",
            "lblDay3": "Wed:",
            "lblDay3Time": "9:00 AM - 9:00 PM",
            "lblDay4": "Thu:",
            "lblDay4Time": "9:00 AM - 9:00 PM",
            "lblDay5": "Fri:",
            "lblDay5Time": "9:00 AM - 9:00 PM",
            "lblDay6": "Sat:",
            "lblDay6Time": "9:00 AM - 9:00 PM",
            "lblDay7": "Sun:",
            "lblDay7Time": "9:00 AM - 9:00 PM",
            "lblDistance": "0.6 mi",
            "lblGap": "STORE HOURS",
            "lblMapIcon": "e",
            "lblNameStore": "",
            "lblPhone": "972.385.9228",
            "lblStock": "",
            "lblStockTitle": "Out of Stock",
            "lblStoreHours": "STORE HOURS",
            "lblStoreHoursNew": "Label",
            "lblTapToSelect": "Label"
        }],
        "groupCells": false,
        "id": "segStoresListView",
        "isVisible": true,
        "left": "0%",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxStoreDetailsWrap,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "btnCollapse": "btnCollapse",
            "btnDirections": "btnDirections",
            "btnExpand": "btnExpand",
            "btnFavorite": "btnFavorite",
            "btnStoreMap": "btnStoreMap",
            "flexComingSoon": "flexComingSoon",
            "flexComingSoonText": "flexComingSoonText",
            "flexFlag": "flexFlag",
            "flxDay1": "flxDay1",
            "flxDay2": "flxDay2",
            "flxDay3": "flxDay3",
            "flxDay4": "flxDay4",
            "flxDay5": "flxDay5",
            "flxDay6": "flxDay6",
            "flxDay7": "flxDay7",
            "flxDirections": "flxDirections",
            "flxExpandDetailsNew": "flxExpandDetailsNew",
            "flxItemLocation": "flxItemLocation",
            "flxSeparator": "flxSeparator",
            "flxSeparatorTop": "flxSeparatorTop",
            "flxStockInfo": "flxStockInfo",
            "flxStoreDetails": "flxStoreDetails",
            "flxStoreDetailsCollapse": "flxStoreDetailsCollapse",
            "flxStoreDetailsExpand": "flxStoreDetailsExpand",
            "flxStoreDetailsWrap": "flxStoreDetailsWrap",
            "flxStoreHorizontal": "flxStoreHorizontal",
            "flxStoreTimings": "flxStoreTimings",
            "lblAddressLine1": "lblAddressLine1",
            "lblAddressLine2": "lblAddressLine2",
            "lblComingSoon": "lblComingSoon",
            "lblDay1": "lblDay1",
            "lblDay1Time": "lblDay1Time",
            "lblDay2": "lblDay2",
            "lblDay2Time": "lblDay2Time",
            "lblDay3": "lblDay3",
            "lblDay3Time": "lblDay3Time",
            "lblDay4": "lblDay4",
            "lblDay4Time": "lblDay4Time",
            "lblDay5": "lblDay5",
            "lblDay5Time": "lblDay5Time",
            "lblDay6": "lblDay6",
            "lblDay6Time": "lblDay6Time",
            "lblDay7": "lblDay7",
            "lblDay7Time": "lblDay7Time",
            "lblDistance": "lblDistance",
            "lblGap": "lblGap",
            "lblMapIcon": "lblMapIcon",
            "lblNameStore": "lblNameStore",
            "lblPhone": "lblPhone",
            "lblStock": "lblStock",
            "lblStockTitle": "lblStockTitle",
            "lblStoreHours": "lblStoreHours",
            "lblStoreHoursNew": "lblStoreHoursNew",
            "lblTapToSelect": "lblTapToSelect"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var lbltextNoResultsFound = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbltextNoResultsFound",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular28pxDarkGray",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.lbltextNoResultsFound"),
        "top": "25dp",
        "width": "80%",
        "zIndex": 13
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxListView.add(segStoresListView, lbltextNoResultsFound);
    flxScrlMapList.add(flxMapHolder, flxListView);
    flxMainContainer.add(flxScrlMapList);
    var flxTopPart = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTopPart",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopPart.setDefaultUnit(kony.flex.DP);
    var flxSearchHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxSearchHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxSearchHeader.setDefaultUnit(kony.flex.DP);
    var btnHome = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxWhite",
        "height": "100%",
        "id": "btnHome",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnIcon36pxWhite",
        "text": "L",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxSearchContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "isModalContainer": false,
        "right": "16dp",
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    flxSearchContents.setDefaultUnit(kony.flex.DP);
    var textSearch = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTextSearch",
        "height": "100%",
        "id": "textSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.searchLocationPlaceholder"),
        "secureTextEntry": false,
        "skin": "sknTextSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearSearchText = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnClearSearchText",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxSearchContents.add(textSearch, btnClearSearchText);
    flxSearchHeader.add(btnHome, flxSearchContents);
    var flxTabs = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxTabs",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxTabs.setDefaultUnit(kony.flex.DP);
    var btnMapView = new kony.ui.Button({
        "focusSkin": "sknBtnTabsActive",
        "height": "100%",
        "id": "btnMapView",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTabsActive",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.btnMapView"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnListView = new kony.ui.Button({
        "focusSkin": "sknBtnTabsDefault",
        "height": "100%",
        "id": "btnListView",
        "isVisible": true,
        "left": "50%",
        "skin": "sknBtnTabsDefault",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.btnListView"),
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    flxTabs.add(btnMapView, btnListView, flxLine);
    var flxRegionInfo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxRegionInfo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxf1f1f1",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRegionInfo.setDefaultUnit(kony.flex.DP);
    var CopyflxLine000aef2afca4446 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "CopyflxLine000aef2afca4446",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBorder",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxLine000aef2afca4446.setDefaultUnit(kony.flex.DP);
    CopyflxLine000aef2afca4446.add();
    var flxCountryFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "41dp",
        "id": "flxCountryFlag",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {}, {});
    flxCountryFlag.setDefaultUnit(kony.flex.DP);
    var lblCountry = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCountry",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBlack13pxGothamBook",
        "text": kony.i18n.getLocalizedString("i18n.common.address.country"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imageCountry = new kony.ui.Image2({
        "centerY": "50%",
        "height": "23dp",
        "id": "imageCountry",
        "isVisible": true,
        "left": "2dp",
        "skin": "slImage",
        "src": "flag_united_states.png",
        "width": "15dp",
        "zIndex": 2
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxCountryFlag.add(lblCountry, imageCountry);
    var btnChangeRegion = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnGothamMed18pxBlueFoc",
        "height": "41dp",
        "id": "btnChangeRegion",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnChangeRegionFoc",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.btnRegionChange"),
        "width": "150dp",
        "zIndex": 4
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxRegionInfo.add(CopyflxLine000aef2afca4446, flxCountryFlag, btnChangeRegion);
    flxTopPart.add(flxSearchHeader, flxTabs, flxRegionInfo);
    var kmfb035be53f347e9bd53e528719addeb = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    kmfb035be53f347e9bd53e528719addeb.setDefaultUnit(kony.flex.DP);
    var kmabdcca960384e92a1717f1b43dbc561 = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmabdcca960384e92a1717f1b43dbc561.setDefaultUnit(kony.flex.DP);
    kmabdcca960384e92a1717f1b43dbc561.add();
    var km54c958383a04ee9b41116ad4468aa8c = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km54c958383a04ee9b41116ad4468aa8c.setDefaultUnit(kony.flex.DP);
    var kma01e41ce5444696831e8314de6ef759 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmca77722b3a5403dbf0187fac555858a = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km6c4ac222a9a43878822b9b067c05841 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km983a337d52b421195b737b57378db96 = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    km983a337d52b421195b737b57378db96.setDefaultUnit(kony.flex.DP);
    km983a337d52b421195b737b57378db96.add();
    var kmc4c3668111f4cb9bd864bfade39b753 = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km54c958383a04ee9b41116ad4468aa8c.add(kma01e41ce5444696831e8314de6ef759, kmca77722b3a5403dbf0187fac555858a, km6c4ac222a9a43878822b9b067c05841, km983a337d52b421195b737b57378db96, kmc4c3668111f4cb9bd864bfade39b753);
    var kmc73a430021347a1984bd631af8e5bb3 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmc73a430021347a1984bd631af8e5bb3.setDefaultUnit(kony.flex.DP);
    var km92cb7f9b7de40d38b04f3a182036b97 = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km92cb7f9b7de40d38b04f3a182036b97.setDefaultUnit(kony.flex.DP);
    var km5a19f58f3a342ac8d43aac962bc576d = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km5a19f58f3a342ac8d43aac962bc576d.setDefaultUnit(kony.flex.DP);
    var km4d19be1aac640f1a671ff8fba1cb24b = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km5a19f58f3a342ac8d43aac962bc576d.add(km4d19be1aac640f1a671ff8fba1cb24b);
    var kmeaa83217aca4fc685fdbd3866a619e3 = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    kmeaa83217aca4fc685fdbd3866a619e3.setDefaultUnit(kony.flex.DP);
    var km5bb0c47ad004be6a7acde3f23cd6639 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmeaa83217aca4fc685fdbd3866a619e3.add(km5bb0c47ad004be6a7acde3f23cd6639);
    var km69cb8a5f773437faef65dd170d3bc34 = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km69cb8a5f773437faef65dd170d3bc34.setDefaultUnit(kony.flex.DP);
    var kmed723ed497a43df8e6592b9834e5eb9 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var km4050c52ddfc45cd8c0d7eab92a277f1 = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    km69cb8a5f773437faef65dd170d3bc34.add(kmed723ed497a43df8e6592b9834e5eb9, km4050c52ddfc45cd8c0d7eab92a277f1);
    var km2b3187fc6784d95b01f3d984a4b34b1 = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km92cb7f9b7de40d38b04f3a182036b97.add(km5a19f58f3a342ac8d43aac962bc576d, kmeaa83217aca4fc685fdbd3866a619e3, km69cb8a5f773437faef65dd170d3bc34, km2b3187fc6784d95b01f3d984a4b34b1);
    var km9e7c96b66a240baa650175208ef93ab = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km9e7c96b66a240baa650175208ef93ab.setDefaultUnit(kony.flex.DP);
    km9e7c96b66a240baa650175208ef93ab.add();
    kmc73a430021347a1984bd631af8e5bb3.add(km92cb7f9b7de40d38b04f3a182036b97, km9e7c96b66a240baa650175208ef93ab);
    var kmae6f06a50ae4cf4b552d0014c33f0d7 = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    kmae6f06a50ae4cf4b552d0014c33f0d7.setDefaultUnit(kony.flex.DP);
    var kmc770072a0c04b9fb3093526353a8b0a = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    kmc770072a0c04b9fb3093526353a8b0a.setDefaultUnit(kony.flex.DP);
    var km55a66b1b3b34bfd86face1624ba8a79 = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    km55a66b1b3b34bfd86face1624ba8a79.setDefaultUnit(kony.flex.DP);
    var km3f10ed382fd463e8027acff7b826068 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmc49c8d0f99e404687c6a5eb880f0cfe = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km55a66b1b3b34bfd86face1624ba8a79.add(km3f10ed382fd463e8027acff7b826068, kmc49c8d0f99e404687c6a5eb880f0cfe);
    var km452e4f3f5ff47b1b43725b0c654c28b = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km452e4f3f5ff47b1b43725b0c654c28b.setDefaultUnit(kony.flex.DP);
    var km0ac2f6677334744997c3a0924c7cec5 = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km0ac2f6677334744997c3a0924c7cec5.setDefaultUnit(kony.flex.DP);
    var km2b46801f14e412b8efe7b715c21cef2 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc98ffa27e73413d9125db34f5d8e1eb = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km1ec67ffd512421aade5c6170ec7be39 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc05772ce4ea4ea387dfc9873148cb2e = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmf393095366146429890528cf9a0f52c = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km2189a9221b4469c95bd73a276633717 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km0ac2f6677334744997c3a0924c7cec5.add(km2b46801f14e412b8efe7b715c21cef2, kmc98ffa27e73413d9125db34f5d8e1eb, km1ec67ffd512421aade5c6170ec7be39, kmc05772ce4ea4ea387dfc9873148cb2e, kmf393095366146429890528cf9a0f52c, km2189a9221b4469c95bd73a276633717);
    var km2b3d0c030f34967a62e2d5c3b5753d0 = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km2b3d0c030f34967a62e2d5c3b5753d0.setDefaultUnit(kony.flex.DP);
    var kmc0b81d5a46c4804b14142fe8cd94b4e = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km8b4dcc8ccfb4a3196b11a022f91f686 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kme10c2c0d19b46199e233636aa9c5edf = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km2b3d0c030f34967a62e2d5c3b5753d0.add(kmc0b81d5a46c4804b14142fe8cd94b4e, km8b4dcc8ccfb4a3196b11a022f91f686, kme10c2c0d19b46199e233636aa9c5edf);
    km452e4f3f5ff47b1b43725b0c654c28b.add(km0ac2f6677334744997c3a0924c7cec5, km2b3d0c030f34967a62e2d5c3b5753d0);
    var km78492dc32d54bbc82b9352bfe484da7 = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km4c9fceaec30499cb462dee497ecada0 = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmc770072a0c04b9fb3093526353a8b0a.add(km55a66b1b3b34bfd86face1624ba8a79, km452e4f3f5ff47b1b43725b0c654c28b, km78492dc32d54bbc82b9352bfe484da7, km4c9fceaec30499cb462dee497ecada0);
    kmae6f06a50ae4cf4b552d0014c33f0d7.add(kmc770072a0c04b9fb3093526353a8b0a);
    kmfb035be53f347e9bd53e528719addeb.add(kmabdcca960384e92a1717f1b43dbc561, km54c958383a04ee9b41116ad4468aa8c, kmc73a430021347a1984bd631af8e5bb3, kmae6f06a50ae4cf4b552d0014c33f0d7);
    var flxOptionalResultsContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "focusSkin": "sknFlxSboxBgWhite",
        "horizontalScrollIndicator": true,
        "id": "flxOptionalResultsContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "49dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxOptionalResultsContainer.setDefaultUnit(kony.flex.DP);
    var segOptionalStoreResults = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }],
        "groupCells": false,
        "id": "segOptionalStoreResults",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_e3f9a54fc68c4a7c87956e3ee6153572,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxOptionalResults,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxOptionalResults": "flxOptionalResults",
            "lblOptionalResults": "lblOptionalResults"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    flxOptionalResultsContainer.add(segOptionalStoreResults);
    var flxLocationPrompt = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "80%",
        "clipBounds": true,
        "id": "flxLocationPrompt",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxLocationPrompt.setDefaultUnit(kony.flex.DP);
    var lblLocationPrompt = new kony.ui.Label({
        "bottom": "15dp",
        "centerX": "50%",
        "id": "lblLocationPrompt",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSFNSRegular",
        "text": kony.i18n.getLocalizedString("i18.phone.LocationServices.EnablePrompt"),
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxLocationPrompt.add(lblLocationPrompt);
    var km9053ed4867548b29035f51044b98225 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km9053ed4867548b29035f51044b98225.setDefaultUnit(kony.flex.DP);
    var kmc1adde313014f2eb280be7a316b2ec5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc1adde313014f2eb280be7a316b2ec5.setDefaultUnit(kony.flex.DP);
    var km2cd40418a7f460f880606c10a60cb04 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km1e2a7f19e3a4fb5af150be78a76ed1d = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmc1adde313014f2eb280be7a316b2ec5.add(km2cd40418a7f460f880606c10a60cb04, km1e2a7f19e3a4fb5af150be78a76ed1d);
    var kmaa76b78bcce439b95874255d1f6e5c9 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmaa76b78bcce439b95874255d1f6e5c9.setDefaultUnit(kony.flex.DP);
    var kmad1934109a3400c870bf975411aaf90 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km9860afa0aaa4a92b84fd5f2ed6c63a0 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km39fa235bfb84f3fa9fb06ba2f8f05de = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmaa76b78bcce439b95874255d1f6e5c9.add(kmad1934109a3400c870bf975411aaf90, km9860afa0aaa4a92b84fd5f2ed6c63a0, km39fa235bfb84f3fa9fb06ba2f8f05de);
    var kmd24278f4966472289cd57d0f48ac16b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd24278f4966472289cd57d0f48ac16b.setDefaultUnit(kony.flex.DP);
    var km1ee0fa75bf04c96846577c2c831c85f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km62ee99864d34c18a6b89faeb51ee1be = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmd24278f4966472289cd57d0f48ac16b.add(km1ee0fa75bf04c96846577c2c831c85f, km62ee99864d34c18a6b89faeb51ee1be);
    var km242c14ca1fc4f82bec3ed075e6b5577 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km242c14ca1fc4f82bec3ed075e6b5577.setDefaultUnit(kony.flex.DP);
    var km52edac84f86450d92ccc1d2f1b10654 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kma2e62a51a3748ddafdacc704b25118a = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km242c14ca1fc4f82bec3ed075e6b5577.add(km52edac84f86450d92ccc1d2f1b10654, kma2e62a51a3748ddafdacc704b25118a);
    var km0accc037e01420a89ada08243b7c4e6 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0accc037e01420a89ada08243b7c4e6.setDefaultUnit(kony.flex.DP);
    var km4296bc8744b4f319fa743a4c333a160 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmdc377b0dfdd4350b4c69c3337c2e94c = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km0accc037e01420a89ada08243b7c4e6.add(km4296bc8744b4f319fa743a4c333a160, kmdc377b0dfdd4350b4c69c3337c2e94c);
    var kmcc00c8f57d047c9b4e9b4286c4dd07a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmcc00c8f57d047c9b4e9b4286c4dd07a.setDefaultUnit(kony.flex.DP);
    var km96742e4a0a24b9fb6b8c8333a15d5c3 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmd15a3b295fa4950a3261df9b3da6316 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmcc00c8f57d047c9b4e9b4286c4dd07a.add(km96742e4a0a24b9fb6b8c8333a15d5c3, kmd15a3b295fa4950a3261df9b3da6316);
    var km4ee24d531b9405486788cf5330b8141 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4ee24d531b9405486788cf5330b8141.setDefaultUnit(kony.flex.DP);
    var km49f475cc3174c1fbe0d0e8f07bef28e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmff23a177aa24d7abdfd0c2ebeec9305 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km4ee24d531b9405486788cf5330b8141.add(km49f475cc3174c1fbe0d0e8f07bef28e, kmff23a177aa24d7abdfd0c2ebeec9305);
    var km42beaa69bd64a7ebf2171251a3a3e8f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km42beaa69bd64a7ebf2171251a3a3e8f.setDefaultUnit(kony.flex.DP);
    var kme0110823b864905b08841037f2f43cd = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km34e2658ee584a9caeb9f369c1a3a6e4 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km42beaa69bd64a7ebf2171251a3a3e8f.add(kme0110823b864905b08841037f2f43cd, km34e2658ee584a9caeb9f369c1a3a6e4);
    km9053ed4867548b29035f51044b98225.add(kmc1adde313014f2eb280be7a316b2ec5, kmaa76b78bcce439b95874255d1f6e5c9, kmd24278f4966472289cd57d0f48ac16b, km242c14ca1fc4f82bec3ed075e6b5577, km0accc037e01420a89ada08243b7c4e6, kmcc00c8f57d047c9b4e9b4286c4dd07a, km4ee24d531b9405486788cf5330b8141, km42beaa69bd64a7ebf2171251a3a3e8f);
    frmFindStoreMapView.add(flxMainContainer, flxTopPart, kmfb035be53f347e9bd53e528719addeb, flxOptionalResultsContainer, flxLocationPrompt, km9053ed4867548b29035f51044b98225);
};

function frmFindStoreMapViewGlobals() {
    frmFindStoreMapView = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmFindStoreMapView,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmFindStoreMapView",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "inTransitionConfig": {
            "transitionDirection": "fromLeft",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "fromRight",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};