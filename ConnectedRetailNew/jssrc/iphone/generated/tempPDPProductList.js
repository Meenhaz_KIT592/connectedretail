function initializetempPDPProductList() {
    CopyflxProductItem0677dbad2027649 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyflxProductItem0677dbad2027649",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxProductItem0677dbad2027649.setDefaultUnit(kony.flex.DP);
    var FlexGroup0b8478a21c4a44a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexGroup0b8478a21c4a44a",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray1234",
        "top": "10dp",
        "width": "100%"
    }, {}, {});
    FlexGroup0b8478a21c4a44a.setDefaultUnit(kony.flex.DP);
    var flxItemDetailsWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemDetailsWrap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemDetailsWrap.setDefaultUnit(kony.flex.DP);
    var imgItem = new kony.ui.Image2({
        "height": "100dp",
        "id": "imgItem",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "10dp",
        "skin": "slImage",
        "src": "sample_product.png",
        "top": "10dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxItemDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxItemDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "120dp",
        "isModalContainer": false,
        "right": "10dp",
        "skin": "slFbox",
        "top": "10dp",
        "zIndex": 1
    }, {}, {});
    flxItemDetails.setDefaultUnit(kony.flex.DP);
    var lblItemName = new kony.ui.Label({
        "id": "lblItemName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamMedium24pxDark",
        "text": "Complete Art Easel & Portfolio Set By Artist’s Loft",
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxStarRating = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStarRating",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%"
    }, {}, {});
    flxStarRating.setDefaultUnit(kony.flex.DP);
    var lblStar1 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar1",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar2 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar2",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar3 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar3",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar4 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar4",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStarActive",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStar5 = new kony.ui.Label({
        "height": "15dp",
        "id": "lblStar5",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblIconStar",
        "text": "Y",
        "textStyle": {
            "letterSpacing": 1,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblItemNumberReviews = new kony.ui.Label({
        "height": "15dp",
        "id": "lblItemNumberReviews",
        "isVisible": false,
        "left": "5dp",
        "skin": "sknLblGothamMedium20pxDark",
        "text": "()",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgStar1 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar1",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar2 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar2",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar3 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar3",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar4 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar4",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgStar5 = new kony.ui.Image2({
        "height": "15dp",
        "id": "imgStar5",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "starempty.png",
        "top": "0dp",
        "width": "6%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxStarRating.add(lblStar1, lblStar2, lblStar3, lblStar4, lblStar5, lblItemNumberReviews, imgStar1, imgStar2, imgStar3, imgStar4, imgStar5);
    flxItemDetails.add(lblItemName, flxStarRating);
    flxItemDetailsWrap.add(imgItem, flxItemDetails);
    var FlexContainer082ed50f8d1314b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "FlexContainer082ed50f8d1314b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer082ed50f8d1314b.setDefaultUnit(kony.flex.DP);
    FlexContainer082ed50f8d1314b.add();
    FlexGroup0b8478a21c4a44a.add(flxItemDetailsWrap, FlexContainer082ed50f8d1314b);
    CopyflxProductItem0677dbad2027649.add(FlexGroup0b8478a21c4a44a);
}