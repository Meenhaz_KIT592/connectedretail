function addWidgetsfrmCreateAccountStep2() {
    frmCreateAccountStep2.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxEmail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxEmail.setDefaultUnit(kony.flex.DP);
    var txtEmail = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "49.97%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtEmail",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxEmail.add(txtEmail);
    var flxConfirmEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxConfirmEmail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxConfirmEmail.setDefaultUnit(kony.flex.DP);
    var txtConfrimEmail = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtConfrimEmail",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtConfrimEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxConfirmEmail.add(txtConfrimEmail);
    var Label01c395a531eb648 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label01c395a531eb648",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknLblGothamRegular18pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccountStep2.signInInfo"),
        "top": "15dp",
        "width": "200dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var FlexContainer08545d32dca7842 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "FlexContainer08545d32dca7842",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    FlexContainer08545d32dca7842.setDefaultUnit(kony.flex.DP);
    var rtPrivacyPolicy = new kony.ui.RichText({
        "id": "rtPrivacyPolicy",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.rtPrivacyPolicy"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheck = new kony.ui.Image2({
        "height": "14dp",
        "id": "imgCheck",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "top": "0dp",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPrivacyPolicy = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "20dp",
        "id": "btnPrivacyPolicy",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlexContainer08545d32dca7842.add(rtPrivacyPolicy, imgCheck, btnPrivacyPolicy);
    var btnCreateAccount = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedGothamBook",
        "height": "40dp",
        "id": "btnCreateAccount",
        "isVisible": true,
        "skin": "sknBtnRedGothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.lblCreateAcc"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxMainContainer.add(flxTopSpacerDND, flxEmail, flxConfirmEmail, Label01c395a531eb648, FlexContainer08545d32dca7842, btnCreateAccount);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "53%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.lblCreateAcc"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var Label0ee00528ffdfe4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0ee00528ffdfe4c",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "25%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, Label0ee00528ffdfe4c, btnHeaderLeft);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialogbox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxDialogbox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxDialogbox.setDefaultUnit(kony.flex.DP);
    var lbl1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl1",
        "isVisible": true,
        "skin": "sknLblSFNSBold",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.accountSetUp"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbl2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSFNSRegular",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.takeOneMoreStep"),
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "40dp",
        "id": "btnOk",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.ok"),
        "top": "10dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    var btnYes = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.ok"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine2.setDefaultUnit(kony.flex.DP);
    flxLine2.add();
    var btnRewardProgramInfo = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnRewardProgramInfo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.rewardProgramInfo"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine3.setDefaultUnit(kony.flex.DP);
    flxLine3.add();
    var btnNotRightNow = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnNotRightNow",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.notRightNow"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxDialogbox.add(lbl1, lbl2, btnOk, flxLine, btnYes, flxLine2, btnRewardProgramInfo, flxLine3, btnNotRightNow);
    flxOverlay.add(flxDialogbox);
    frmCreateAccountStep2.add(flxMainContainer, flxHeaderTitleContainer, flxOverlay);
};

function frmCreateAccountStep2Globals() {
    frmCreateAccountStep2 = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCreateAccountStep2,
        "allowHorizontalBounce": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmCreateAccountStep2",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};