function addWidgetsfrmWebView() {
    frmWebView.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "clipBounds": false,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "CopysknFormWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "bouncesZoom": false
    });
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "42dp",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var btnBack = new kony.ui.Button({
        "focusSkin": "sknBtnCategorytabMain",
        "height": "100%",
        "id": "btnBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [8, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblGothamMedium32pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnBack"),
        "textStyle": {},
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "textStyle": {},
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCategoryBack = new kony.ui.Label({
        "height": "100%",
        "id": "lblCategoryBack",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon34px000000",
        "text": "L",
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 10, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(btnBack, lblBack, lblFormTitle, lblCategoryBack);
    flxMainContainer.add(flxTopSpacerDND, flxBack);
    var km704bf0103da417aaaab0737c021a39e = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km704bf0103da417aaaab0737c021a39e.setDefaultUnit(kony.flex.DP);
    var km459da2d8cca461babf08041141c18e4 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km459da2d8cca461babf08041141c18e4.setDefaultUnit(kony.flex.DP);
    var km7176c3bd1294729847abf6814dd6d78 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7525c1dd12048cb96d8c84cb1651407 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km459da2d8cca461babf08041141c18e4.add(km7176c3bd1294729847abf6814dd6d78, km7525c1dd12048cb96d8c84cb1651407);
    var kmc3e452131b0481888844a935cde6a22 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc3e452131b0481888844a935cde6a22.setDefaultUnit(kony.flex.DP);
    var km15e86676d7f486f96bfb9ec3150a8ae = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kma65ab2456854b21ad1f8b6dc23165eb = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km3b779fc70f4426ead0c12e9f50ea102 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmc3e452131b0481888844a935cde6a22.add(km15e86676d7f486f96bfb9ec3150a8ae, kma65ab2456854b21ad1f8b6dc23165eb, km3b779fc70f4426ead0c12e9f50ea102);
    var km449838aa4d440a2a4bb484c8519c0ef = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km449838aa4d440a2a4bb484c8519c0ef.setDefaultUnit(kony.flex.DP);
    var km6d1fc91db954de89bac709a1f115d26 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km81e438dd09742e8bb68db9c1892c244 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km449838aa4d440a2a4bb484c8519c0ef.add(km6d1fc91db954de89bac709a1f115d26, km81e438dd09742e8bb68db9c1892c244);
    var km2f4249edf8740f0bab05a5ddd4912be = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2f4249edf8740f0bab05a5ddd4912be.setDefaultUnit(kony.flex.DP);
    var km2f21206eadf453f9269825b316e69ce = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km9dd0d09709b4e0095b314fea7dc2cfa = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km2f4249edf8740f0bab05a5ddd4912be.add(km2f21206eadf453f9269825b316e69ce, km9dd0d09709b4e0095b314fea7dc2cfa);
    var km0c03b0f78d748daa071efe64fdf6c31 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km0c03b0f78d748daa071efe64fdf6c31.setDefaultUnit(kony.flex.DP);
    var km83537c1465c4950bccd6e3e96f94b98 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kma06d05c5a134708862851888bf48e4c = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km0c03b0f78d748daa071efe64fdf6c31.add(km83537c1465c4950bccd6e3e96f94b98, kma06d05c5a134708862851888bf48e4c);
    var km27ca980caa64d88bdabfaf2c772225d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km27ca980caa64d88bdabfaf2c772225d.setDefaultUnit(kony.flex.DP);
    var km579e6517cfd430ea806aa9d8b6efc44 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kme8aef81254246ad8ac1eb1563a336dc = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km27ca980caa64d88bdabfaf2c772225d.add(km579e6517cfd430ea806aa9d8b6efc44, kme8aef81254246ad8ac1eb1563a336dc);
    var km128af7ba9e547569a1100d4514aabbd = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km128af7ba9e547569a1100d4514aabbd.setDefaultUnit(kony.flex.DP);
    var kmdcc782d4f504767806ed01fa898bb60 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km865987c65114f49a43aa23218dfb94a = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km128af7ba9e547569a1100d4514aabbd.add(kmdcc782d4f504767806ed01fa898bb60, km865987c65114f49a43aa23218dfb94a);
    var km5f3e5f2287b41a5a20866f70e2f0c0c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km5f3e5f2287b41a5a20866f70e2f0c0c.setDefaultUnit(kony.flex.DP);
    var km5fa45c2501748578791512f0521f35c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7e4901c71744f76af57e85269dd1cc6 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km5f3e5f2287b41a5a20866f70e2f0c0c.add(km5fa45c2501748578791512f0521f35c, km7e4901c71744f76af57e85269dd1cc6);
    km704bf0103da417aaaab0737c021a39e.add(km459da2d8cca461babf08041141c18e4, kmc3e452131b0481888844a935cde6a22, km449838aa4d440a2a4bb484c8519c0ef, km2f4249edf8740f0bab05a5ddd4912be, km0c03b0f78d748daa071efe64fdf6c31, km27ca980caa64d88bdabfaf2c772225d, km128af7ba9e547569a1100d4514aabbd, km5f3e5f2287b41a5a20866f70e2f0c0c);
    var km5fcd0cc2019426095d5be252448b85f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxSearchOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km5fcd0cc2019426095d5be252448b85f.setDefaultUnit(kony.flex.DP);
    var km3031a4566234a178cfac44fbebb0f36 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxContainerSearchExpand",
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.02%",
        "top": "-0.03%",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay"
    }, {}, {});
    km3031a4566234a178cfac44fbebb0f36.setDefaultUnit(kony.flex.DP);
    var kmc38486438614602a643222db8a4515d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "130dp",
        "id": "flxContainerSearchInner",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2%",
        "right": "2%",
        "top": "67dp",
        "width": "96%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    kmc38486438614602a643222db8a4515d.setDefaultUnit(kony.flex.DP);
    var km7f45def362a44cdb799e4b688619f1c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "0.50%",
        "id": "flxSeparator",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "37dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexSeparatorGrey"
    }, {}, {});
    km7f45def362a44cdb799e4b688619f1c.setDefaultUnit(kony.flex.DP);
    km7f45def362a44cdb799e4b688619f1c.add();
    var kmd928b2fb0f849b7b8a51401ded56352 = new kony.ui.Image2({
        "height": "40dp",
        "id": "imgBack",
        "left": "17dp",
        "src": "arrowback.png",
        "top": "10dp",
        "width": "20dp",
        "zIndex": 10,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km008428ce7c04a67a970f5e2bda634f5 = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "50dp",
        "id": "txtSearchField",
        "left": "40dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "5dp",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "2dp",
        "zIndex": 10,
        "focusSkin": "sknTextSearchOverlay",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearchOverlay"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var km771bb2c1d0a4f3fb37c1d494619fe98 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVoice",
        "layoutType": kony.flex.FREE_FORM,
        "left": "14%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km771bb2c1d0a4f3fb37c1d494619fe98.setDefaultUnit(kony.flex.DP);
    var kma8dd59f9f454112884d8a22450f4ceb = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgVoice",
        "src": "microphone_icon.png",
        "top": "21dp",
        "width": "13dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmd9a1d1db2aa40cf990bd354bf78e56d = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoice",
        "text": "Voice Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km771bb2c1d0a4f3fb37c1d494619fe98.add(kma8dd59f9f454112884d8a22450f4ceb, kmd9a1d1db2aa40cf990bd354bf78e56d);
    var km4b373871267402db38901e4b6e2fe44 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxVisual",
        "layoutType": kony.flex.FREE_FORM,
        "left": "33%",
        "top": "40dp",
        "width": "33%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4b373871267402db38901e4b6e2fe44.setDefaultUnit(kony.flex.DP);
    var km4ee23f24a874a2aade1c4af86efbf1f = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVisual",
        "text": "Visual Search",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmd808f4e39584135b89bdf6fc2c41299 = new kony.ui.Camera({
        "centerX": "50%",
        "height": "24dp",
        "id": "cmrImageSearch",
        "left": "55dp",
        "top": "21dp",
        "width": "29dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraOverlayImage",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraOverlayImage"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "captureOrientation": constants.CAMERA_CAPTURE_ORIENTATION_PORTRAIT,
        "enableOverlay": true,
        "nativeUserInterface": false,
        "overlayConfig": {
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay
        }
    });
    km4b373871267402db38901e4b6e2fe44.add(km4ee23f24a874a2aade1c4af86efbf1f, kmd808f4e39584135b89bdf6fc2c41299);
    var kma84318d07654664a4b92aa1420b0edf = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "89dp",
        "id": "flxBarCode",
        "layoutType": kony.flex.FREE_FORM,
        "left": "52%",
        "top": "40dp",
        "width": "33.30%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma84318d07654664a4b92aa1420b0edf.setDefaultUnit(kony.flex.DP);
    var km23f1c4b135b4e72839adeef7bafd344 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "24dp",
        "id": "imgBarCode",
        "left": "55dp",
        "src": "iconbarcode.png",
        "top": "21dp",
        "width": "39dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var kmb88c5a095db4a5287e7389f16ad966b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBarCode",
        "text": "Bar Code",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "67%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontReg11pxDarkGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kma84318d07654664a4b92aa1420b0edf.add(km23f1c4b135b4e72839adeef7bafd344, kmb88c5a095db4a5287e7389f16ad966b);
    kmc38486438614602a643222db8a4515d.add(km7f45def362a44cdb799e4b688619f1c, kmd928b2fb0f849b7b8a51401ded56352, km008428ce7c04a67a970f5e2bda634f5, km771bb2c1d0a4f3fb37c1d494619fe98, km4b373871267402db38901e4b6e2fe44, kma84318d07654664a4b92aa1420b0edf);
    km3031a4566234a178cfac44fbebb0f36.add(kmc38486438614602a643222db8a4515d);
    km5fcd0cc2019426095d5be252448b85f.add(km3031a4566234a178cfac44fbebb0f36);
    var km83a9c6d33334d98ac176c03ba677214 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "100%",
        "id": "flxVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "CopysknFlexBgWhiteBorderR00fdd9177217841"
    }, {}, {});
    km83a9c6d33334d98ac176c03ba677214.setDefaultUnit(kony.flex.DP);
    var kmc88eedaadab47c883a46c7c8759d71b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMicPanel",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    kmc88eedaadab47c883a46c7c8759d71b.setDefaultUnit(kony.flex.DP);
    var km8b0e9b9f76c4d5f8a8518027b7de563 = new kony.ui.FlexContainer({
        "bottom": 0,
        "clipBounds": true,
        "height": "60%",
        "id": "flxMicPanelContent",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8b0e9b9f76c4d5f8a8518027b7de563.setDefaultUnit(kony.flex.DP);
    var km11eed823b0a4c3caf8d5bf7c963a335 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblSomething",
        "text": "Say something like...",
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMed16pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km3063ee2028248cebe98b334923f18fd = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "132dp",
        "id": "flxSampleUtterances",
        "layoutType": kony.flex.FREE_FORM,
        "top": "60dp",
        "width": "220dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlxContainerVoiceBox"
    }, {}, {});
    km3063ee2028248cebe98b334923f18fd.setDefaultUnit(kony.flex.DP);
    var km07245a875bb4494a3f3fb3ffa76aab8 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterence1",
        "left": "9%",
        "text": "\"Show me oil paints\"",
        "textStyle": {},
        "top": "20%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmfaa720afe764147adf9cb92075dba8d = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterenace2",
        "left": "9%",
        "text": "\"Show me Craft Projects\"",
        "textStyle": {},
        "top": "40.70%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmcffc36cc5534819835492a6bec3b84b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblUtterance3",
        "left": "9%",
        "text": "\"Nearest store \"",
        "textStyle": {},
        "top": "60%",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontMeditalic13pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km3063ee2028248cebe98b334923f18fd.add(km07245a875bb4494a3f3fb3ffa76aab8, kmfaa720afe764147adf9cb92075dba8d, kmcffc36cc5534819835492a6bec3b84b);
    var km67212f4c4dc4dad887003e456edcf57 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "64%",
        "height": "70dp",
        "id": "imgMicStatus",
        "src": "microphone_btn.png",
        "top": "209dp",
        "width": "70dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var km726b2f6474c44d5aa42a0f58009a693 = new kony.ui.Label({
        "id": "lblMicStatusText",
        "left": "0%",
        "text": "listening...",
        "textStyle": {},
        "top": "75%",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold15pxGrey"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km8b0e9b9f76c4d5f8a8518027b7de563.add(km11eed823b0a4c3caf8d5bf7c963a335, km3063ee2028248cebe98b334923f18fd, km67212f4c4dc4dad887003e456edcf57, km726b2f6474c44d5aa42a0f58009a693);
    var km8f125da4e1a47fb9167e4baadcca374 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerRed",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "top": "0%",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRed"
    }, {}, {});
    km8f125da4e1a47fb9167e4baadcca374.setDefaultUnit(kony.flex.DP);
    var km8e227da56134b6ca6fe51ccca8a2f2b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "top": 0,
        "width": "50dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8e227da56134b6ca6fe51ccca8a2f2b.setDefaultUnit(kony.flex.DP);
    var kmb3faf21f3de4d17b649f7a126975bd0 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "left": "0dp",
        "src": "page.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km8e227da56134b6ca6fe51ccca8a2f2b.add(kmb3faf21f3de4d17b649f7a126975bd0);
    km8f125da4e1a47fb9167e4baadcca374.add(km8e227da56134b6ca6fe51ccca8a2f2b);
    var km500c2a1681d4908ae282147b8258b6e = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblVoiceSearchHeading",
        "text": "What can we help you find today?",
        "textStyle": {},
        "top": "70dp",
        "width": "75%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLabelMontSemiBold26pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmc88eedaadab47c883a46c7c8759d71b.add(km8b0e9b9f76c4d5f8a8518027b7de563, km8f125da4e1a47fb9167e4baadcca374, km500c2a1681d4908ae282147b8258b6e);
    km83a9c6d33334d98ac176c03ba677214.add(kmc88eedaadab47c883a46c7c8759d71b);
    var kmb21661ae1a546e3ad5d6bd706112baf = new kony.ui.FlexContainer({
        "bottom": "54dp",
        "clipBounds": true,
        "isMaster": true,
        "id": "flexCouponContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "-100%",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFormBgWhiteOverlay"
    }, {}, {});
    kmb21661ae1a546e3ad5d6bd706112baf.setDefaultUnit(kony.flex.DP);
    var km5561e7542e142579c71607d778efbef = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr"
    }, {}, {});
    km5561e7542e142579c71607d778efbef.setDefaultUnit(kony.flex.DP);
    km5561e7542e142579c71607d778efbef.add();
    var km39f7c00874947cfbcf715aa947f5f6f = new kony.ui.FlexContainer({
        "clipBounds": false,
        "height": "50dp",
        "id": "flxCouponHeader",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "top": "10dp",
        "width": "95%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2"
    }, {}, {});
    km39f7c00874947cfbcf715aa947f5f6f.setDefaultUnit(kony.flex.DP);
    var kme5c30e2cb494629ab6567e773441931 = new kony.ui.Label({
        "centerY": "53%",
        "id": "lblCouponTitle",
        "left": "20%",
        "text": "COUPONS",
        "textStyle": {},
        "width": "60%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamMedium36pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc55c840e44f4583b976d3c42bac5dfd = new kony.ui.Button({
        "height": "100%",
        "id": "btnDone",
        "right": "0dp",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1,
        "focusSkin": "sknBtnDoneFoc",
        "isVisible": true,
        "skin": "sknBtnDone"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmd5dddc1f2884e44ba30cb9368701513 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2,
        "focusSkin": "sknBtnCoupon",
        "isVisible": false,
        "skin": "sknBtnCoupon"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmbffca6db274451fa4628911154eb2fc = new kony.ui.FlexContainer({
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxSeparatorLine",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "width": "100%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed"
    }, {}, {});
    kmbffca6db274451fa4628911154eb2fc.setDefaultUnit(kony.flex.DP);
    kmbffca6db274451fa4628911154eb2fc.add();
    var km93a2c3bb818450caa8f18821ae22b96 = new kony.ui.Button({
        "centerY": "50%",
        "height": "35dp",
        "id": "btnCouponMainMaster",
        "left": "11dp",
        "right": "2.50%",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1,
        "focusSkin": "sknBtnCouponFoc",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km39f7c00874947cfbcf715aa947f5f6f.add(kme5c30e2cb494629ab6567e773441931, kmc55c840e44f4583b976d3c42bac5dfd, kmd5dddc1f2884e44ba30cb9368701513, kmbffca6db274451fa4628911154eb2fc, km93a2c3bb818450caa8f18821ae22b96);
    var kmd75d94ec29a40e7b6bc5a577fdaf534 = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "id": "flxCouponContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": 2,
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknFlexBoxDarkRed"
    }, {}, {});
    kmd75d94ec29a40e7b6bc5a577fdaf534.setDefaultUnit(kony.flex.DP);
    var kma7752b120d840f7878157826b4b4456 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxCouponRewards",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed"
    }, {}, {});
    kma7752b120d840f7878157826b4b4456.setDefaultUnit(kony.flex.DP);
    var kme4c91f84538469b933a25a8d5045da8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "15dp",
        "id": "flxRewardTxt",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "17dp",
        "width": "100%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme4c91f84538469b933a25a8d5045da8.setDefaultUnit(kony.flex.DP);
    var kmf079034062f4226bbdea477711bd7cf = new kony.ui.Label({
        "centerY": "50%",
        "height": "15dp",
        "id": "lblRewardText",
        "left": "3%",
        "text": "MICHAELS REWARDS CARD",
        "textStyle": {},
        "top": "17dp",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamMedium24pxWhite"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kme4c91f84538469b933a25a8d5045da8.add(kmf079034062f4226bbdea477711bd7cf);
    var kmed0e6b072244b54bbb0eb383042322d = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "id": "flxBarCodeWrap",
        "layoutType": kony.flex.FREE_FORM,
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite"
    }, {}, {});
    kmed0e6b072244b54bbb0eb383042322d.setDefaultUnit(kony.flex.DP);
    var km7aeab3646174dbf82f93829a031cafc = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "id": "imgBarcode",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmed0e6b072244b54bbb0eb383042322d.add(km7aeab3646174dbf82f93829a031cafc);
    var km55d46e74bcb4f34b7a2f9ff2a17558a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "30dp",
        "id": "flxNotUser",
        "layoutType": kony.flex.FREE_FORM,
        "top": "21dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km55d46e74bcb4f34b7a2f9ff2a17558a.setDefaultUnit(kony.flex.DP);
    var kmb72e38698114a5bb29f210422ab2e9a = new kony.ui.Button({
        "centerY": "50%",
        "height": "30dp",
        "id": "btnSignUp",
        "right": "51%",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1,
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "isVisible": true,
        "skin": "sknBtnPrimaryWhite"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var km965af10f8d44f4492d0daf71df748be = new kony.ui.Button({
        "centerY": "50%",
        "height": "30dp",
        "id": "btnSignIn",
        "left": "51%",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1,
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "isVisible": true,
        "skin": "sknBtnPrimaryWhite"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    km55d46e74bcb4f34b7a2f9ff2a17558a.add(kmb72e38698114a5bb29f210422ab2e9a, km965af10f8d44f4492d0daf71df748be);
    var km3c5ac9446db43d8bd27c485e37b96d2 = new kony.ui.Label({
        "height": "1dp",
        "id": "lblSeparator100",
        "left": "0dp",
        "textStyle": {},
        "top": "19dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblSeparatorWhiteOpaque"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kma7752b120d840f7878157826b4b4456.add(kme4c91f84538469b933a25a8d5045da8, kmed0e6b072244b54bbb0eb383042322d, km55d46e74bcb4f34b7a2f9ff2a17558a, km3c5ac9446db43d8bd27c485e37b96d2);
    var kmcd84b3feec94fc1bf28b3dc16c923ee = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "scroll",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": 2,
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknFlexBoxDarkRed"
    }, {}, {});
    kmcd84b3feec94fc1bf28b3dc16c923ee.setDefaultUnit(kony.flex.DP);
    kmcd84b3feec94fc1bf28b3dc16c923ee.add();
    kmd75d94ec29a40e7b6bc5a577fdaf534.add(kma7752b120d840f7878157826b4b4456, kmcd84b3feec94fc1bf28b3dc16c923ee);
    var km7f3b8cf1eef4237908a31ac6eeb8d3a = new kony.ui.FlexContainer({
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "id": "flxCouponDetailsPopup",
        "layoutType": kony.flex.FREE_FORM,
        "top": "10dp",
        "width": "95%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2"
    }, {}, {});
    km7f3b8cf1eef4237908a31ac6eeb8d3a.setDefaultUnit(kony.flex.DP);
    var kmeab99e9c40f415dbea0e9772dc347b6 = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "id": "flxCouponDetailsScroll",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": 2,
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slFSbox"
    }, {}, {});
    kmeab99e9c40f415dbea0e9772dc347b6.setDefaultUnit(kony.flex.DP);
    var km39456077bd64a7ba1fbb9a3f1bfdb35 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "40dp",
        "id": "flxCloseBtnWrap",
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "top": "0dp",
        "width": "50dp",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km39456077bd64a7ba1fbb9a3f1bfdb35.setDefaultUnit(kony.flex.DP);
    var km8e828e24ace4371a676731cec233351 = new kony.ui.Button({
        "height": "40dp",
        "id": "btnPopupClose",
        "left": "0dp",
        "right": "0dp",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnPopupCloseFoc",
        "isVisible": false,
        "skin": "sknBtnIconWhite36Px"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmaf88468aa69405f8288592c0fc59263 = new kony.ui.Image2({
        "height": "100%",
        "id": "Image0a5620b6427ec41",
        "left": "0dp",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km39456077bd64a7ba1fbb9a3f1bfdb35.add(km8e828e24ace4371a676731cec233351, kmaf88468aa69405f8288592c0fc59263);
    var kmdd49b02cafa4371816db0eab3dcde72 = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "id": "flexRestrictionsCode",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexCoupons"
    }, {}, {});
    kmdd49b02cafa4371816db0eab3dcde72.setDefaultUnit(kony.flex.DP);
    var km96b9d96c20c483c88225cb502191abe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km96b9d96c20c483c88225cb502191abe.setDefaultUnit(kony.flex.DP);
    var kma759b173e3247d28ca8c723e6cdfaa8 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "left": "0dp",
        "text": "40",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamBold30pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7a1d52ebab34a3083951c308cc31b0f = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "left": "0dp",
        "text": "Any One Regular Price Item",
        "textStyle": {},
        "top": "2dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamBold28pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kma8c3ef4de5d4b2d8a0c727c1d2145d5 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "left": "0dp",
        "text": "Any One Regular Price Item",
        "textStyle": {},
        "top": "2dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamBold20pxRed"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kma4bc477e61f46cb82be5e7940a8b9ce = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblValidityRestrictions",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {},
        "top": "5dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km3b1ff15190c485cb8854959fcd0c550 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPromoCodeRestrictions",
        "text": "PROMO CODE 40SAVE11517",
        "textStyle": {},
        "top": "2dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km308cbd19e094b208d65b825ece8c17f = new kony.ui.Image2({
        "centerX": "50%",
        "height": "60dp",
        "id": "imgBarcodeRestrictions",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km96b9d96c20c483c88225cb502191abe.add(kma759b173e3247d28ca8c723e6cdfaa8, km7a1d52ebab34a3083951c308cc31b0f, kma8c3ef4de5d4b2d8a0c727c1d2145d5, kma4bc477e61f46cb82be5e7940a8b9ce, km3b1ff15190c485cb8854959fcd0c550, km308cbd19e094b208d65b825ece8c17f);
    var km3f80752bbfa48e6b915c43bd7eb3951 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3f80752bbfa48e6b915c43bd7eb3951.setDefaultUnit(kony.flex.DP);
    var km9d30f5c266841f8b46bd9cee9f9596b = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoBarcode",
        "left": "5dp",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {},
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblBlackPx14"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km0fc33ce50664d2bb3519048d6ca286e = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoBarCodeDetails",
        "left": "15dp",
        "text": "NO BAR CODE REQUIRED",
        "textStyle": {},
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblR194G7B36Px14"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km0efb66733e041deab871eae7bc6c46d = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoBarcodeValidityRestrictions",
        "text": "VALID THRU SAT 1/21/17",
        "textStyle": {},
        "top": "10dp",
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km3f80752bbfa48e6b915c43bd7eb3951.add(km9d30f5c266841f8b46bd9cee9f9596b, km0fc33ce50664d2bb3519048d6ca286e, km0efb66733e041deab871eae7bc6c46d);
    kmdd49b02cafa4371816db0eab3dcde72.add(km96b9d96c20c483c88225cb502191abe, km3f80752bbfa48e6b915c43bd7eb3951);
    var kmba6061342a24cc59a03346178dcd6d3 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRestrictions",
        "left": "0dp",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1,
        "isVisible": false,
        "skin": "sknLblGothamRegular20pxWhite"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmdc4822c305547c29145d56d4e13c840 = new kony.ui.RichText({
        "centerX": "50%",
        "id": "rchTxtRetrictions",
        "left": "0dp",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "sknRTGGothamBook72"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmeab99e9c40f415dbea0e9772dc347b6.add(km39456077bd64a7ba1fbb9a3f1bfdb35, kmdd49b02cafa4371816db0eab3dcde72, kmba6061342a24cc59a03346178dcd6d3, kmdc4822c305547c29145d56d4e13c840);
    km7f3b8cf1eef4237908a31ac6eeb8d3a.add(kmeab99e9c40f415dbea0e9772dc347b6);
    kmb21661ae1a546e3ad5d6bd706112baf.add(km5561e7542e142579c71607d778efbef, km39f7c00874947cfbcf715aa947f5f6f, kmd75d94ec29a40e7b6bc5a577fdaf534, km7f3b8cf1eef4237908a31ac6eeb8d3a);
    var km03aef571b144f6484b89057c6612294 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "isMaster": true,
        "height": "50dp",
        "id": "flxHeaderWrap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c"
    }, {}, {});
    km03aef571b144f6484b89057c6612294.setDefaultUnit(kony.flex.DP);
    var km6012ef0b3f84a90b4282ff3f402cd1c = new kony.ui.Button({
        "height": "100%",
        "id": "btnHome",
        "left": "0dp",
        "text": "H",
        "top": "0dp",
        "width": "45dp",
        "zIndex": 1,
        "focusSkin": "sknBtnIconHomeFoc",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km3037e01aa164f579ede5fac73fb901d = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "right": "50dp",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBdr2"
    }, {}, {});
    km3037e01aa164f579ede5fac73fb901d.setDefaultUnit(kony.flex.DP);
    var km9ce4c6351b74b0295c3043d159735c3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchIcon",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "12.50%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km9ce4c6351b74b0295c3043d159735c3.setDefaultUnit(kony.flex.DP);
    var km37c018efea747fc949b4b43a2553d8d = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "19dp",
        "id": "imgSearchIcon",
        "src": "search_icon.png",
        "width": "23dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km9ce4c6351b74b0295c3043d159735c3.add(km37c018efea747fc949b4b43a2553d8d);
    var km1be79407c7e4beb8e095ec869b45167 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxTextboxContents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "12.50%",
        "top": "0dp",
        "width": "57%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    km1be79407c7e4beb8e095ec869b45167.setDefaultUnit(kony.flex.DP);
    var kmcd524f9825e451ba64c4b8eb57ca8ae = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "100%",
        "id": "textSearch",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.findItLowercase"),
        "right": "12%",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "88%",
        "zIndex": 1,
        "focusSkin": "sknTextSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "skin": "sknTextSearch"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    km1be79407c7e4beb8e095ec869b45167.add(kmcd524f9825e451ba64c4b8eb57ca8ae);
    var kmf75f834e8db4ca1b214357da014901a = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCameraContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "14%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmf75f834e8db4ca1b214357da014901a.setDefaultUnit(kony.flex.DP);
    var km43c6e5e62fa4bc4b8fe019477f1a9e0 = new kony.ui.Camera({
        "centerX": "44%",
        "centerY": "48%",
        "height": "19dp",
        "id": "cmrImageSearch",
        "right": "11%",
        "top": "9dp",
        "width": "23dp",
        "zIndex": 1,
        "cameraSource": constants.CAMERA_SOURCE_REAR,
        "focusSkin": "sknCameraImageSearch",
        "isVisible": true,
        "scaleFactor": 100,
        "skin": "sknCameraImageSearch"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_INMEMORY,
        "captureOrientation": constants.CAMERA_CAPTURE_ORIENTATION_PORTRAIT,
        "enableOverlay": true,
        "nativeUserInterface": false,
        "overlayConfig": {
            "startVideoButtonText": "",
            "stopVideoButtonText": "",
            "overlayForm": frmImageOverlay
        }
    });
    kmf75f834e8db4ca1b214357da014901a.add(km43c6e5e62fa4bc4b8fe019477f1a9e0);
    var kmd36de21baee4651b05bcea7960b6036 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxBarcodeContainer",
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "top": "0dp",
        "width": "12%",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    kmd36de21baee4651b05bcea7960b6036.setDefaultUnit(kony.flex.DP);
    var km29ae9e2e13742c68b7adabb5a23f6b1 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "height": "18dp",
        "id": "btnBarCode",
        "right": 0,
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "focusSkin": "sknBtnBarcodeFocus",
        "isVisible": true,
        "skin": "sknBtnBarcode"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmd36de21baee4651b05bcea7960b6036.add(km29ae9e2e13742c68b7adabb5a23f6b1);
    var kmf736a721fb340239ef96fc6d7822a76 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxImageVoiceSearch",
        "layoutType": kony.flex.FREE_FORM,
        "right": "18%",
        "top": "0dp",
        "width": "12%",
        "zIndex": 2,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmf736a721fb340239ef96fc6d7822a76.setDefaultUnit(kony.flex.DP);
    var km5624566b2464e90baf005fcbab35771 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "20dp",
        "id": "imageVoiceSearch",
        "src": "microphone_icon_home.png",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmf736a721fb340239ef96fc6d7822a76.add(km5624566b2464e90baf005fcbab35771);
    km3037e01aa164f579ede5fac73fb901d.add(km9ce4c6351b74b0295c3043d159735c3, km1be79407c7e4beb8e095ec869b45167, kmf75f834e8db4ca1b214357da014901a, kmd36de21baee4651b05bcea7960b6036, kmf736a721fb340239ef96fc6d7822a76);
    var kma882a12215f4376b97c948518f0fe13 = new kony.ui.Button({
        "height": "50dp",
        "id": "btnCart",
        "right": "-0.08%",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 10,
        "focusSkin": "sknBtnIconHome",
        "isVisible": true,
        "skin": "sknBtnIconHome"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmd69b90848b047fd8ecfecc8748dfe88 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxCart",
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "top": "0dp",
        "width": "17.64%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd69b90848b047fd8ecfecc8748dfe88.setDefaultUnit(kony.flex.DP);
    var km3055a2f130448548de83205235c7aa2 = new kony.ui.Label({
        "height": "42%",
        "id": "lblItemsCount",
        "right": "3%",
        "textStyle": {},
        "top": "7%",
        "width": "30%",
        "zIndex": 2,
        "isVisible": true,
        "skin": "sknCartItems"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmb0fa15ecd504aefad0e0b84fb05414b = new kony.ui.Image2({
        "height": "100%",
        "id": "imgCart",
        "right": "9%",
        "src": "iconcart.png",
        "top": "0%",
        "width": "58.01%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmd69b90848b047fd8ecfecc8748dfe88.add(km3055a2f130448548de83205235c7aa2, kmb0fa15ecd504aefad0e0b84fb05414b);
    km03aef571b144f6484b89057c6612294.add(km6012ef0b3f84a90b4282ff3f402cd1c, km3037e01aa164f579ede5fac73fb901d, kma882a12215f4376b97c948518f0fe13, kmd69b90848b047fd8ecfecc8748dfe88);
    frmWebView.add(flxMainContainer, km704bf0103da417aaaab0737c021a39e, km5fcd0cc2019426095d5be252448b85f, km83a9c6d33334d98ac176c03ba677214, kmb21661ae1a546e3ad5d6bd706112baf, km03aef571b144f6484b89057c6612294);
};

function frmWebViewGlobals() {
    frmWebView = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmWebView,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmWebView",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};