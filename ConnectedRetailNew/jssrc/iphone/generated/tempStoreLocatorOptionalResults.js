function initializetempStoreLocatorOptionalResults() {
    flxOptionalResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxOptionalResults",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    flxOptionalResults.setDefaultUnit(kony.flex.DP);
    var lblOptionalResults = new kony.ui.Label({
        "id": "lblOptionalResults",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblOptionalResults",
        "textStyle": {
            "lineSpacing": 3,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 3, 0, 3],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxOptionalResults.add(lblOptionalResults);
}