function initializetempProjectList() {
    FlexContainer09240b69cf4c54f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer09240b69cf4c54f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    FlexContainer09240b69cf4c54f.setDefaultUnit(kony.flex.DP);
    var flxProductDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxProductDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray1234",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProductDetails.setDefaultUnit(kony.flex.DP);
    var lblName = new kony.ui.Label({
        "bottom": "6dp",
        "centerX": "50%",
        "id": "lblName",
        "isVisible": true,
        "skin": "sknLblGothamMedium24px818181",
        "text": "Day of the Dead Sugar Skull",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "6dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgProject = new kony.ui.Image2({
        "centerX": "50%",
        "height": "215dp",
        "id": "imgProject",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "project_thumb.png",
        "top": "6dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxItemLocation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxItemLocation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxItemLocation.setDefaultUnit(kony.flex.DP);
    var imgDuration = new kony.ui.Image2({
        "centerY": "50%",
        "height": "16dp",
        "id": "imgDuration",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "time_2.png",
        "top": "0dp",
        "width": "16dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblDuration = new kony.ui.Label({
        "height": "100%",
        "id": "lblDuration",
        "isVisible": true,
        "left": "9%",
        "skin": "sknLblGothamMedium24pxDark",
        "text": "About 30 Minutes",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxSkillsLevels = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxSkillsLevels",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "48%",
        "zIndex": 1
    }, {}, {});
    flxSkillsLevels.setDefaultUnit(kony.flex.DP);
    var lblExpertiseDesc = new kony.ui.Label({
        "height": "100%",
        "id": "lblExpertiseDesc",
        "isVisible": true,
        "right": "45dp",
        "skin": "sknLblGothamMedium24pxDark",
        "text": "Intermeditate",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgExpertise = new kony.ui.Image2({
        "centerY": "49.72%",
        "height": "12dp",
        "id": "imgExpertise",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "skill_new_2.png",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxSkillsLevels.add(lblExpertiseDesc, imgExpertise);
    flxItemLocation.add(imgDuration, lblDuration, flxSkillsLevels);
    flxProductDetails.add(lblName, imgProject, flxItemLocation);
    FlexContainer09240b69cf4c54f.add(flxProductDetails);
}