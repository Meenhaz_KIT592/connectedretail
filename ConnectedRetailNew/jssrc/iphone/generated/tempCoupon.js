function initializetempCoupon() {
    flxCouponMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "focusSkin": "sknFlexBgDarkRed",
        "id": "flxCouponMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxCouponMainContainer.setDefaultUnit(kony.flex.DP);
    var flxCoupon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "focusSkin": "sknFlexBgWhiteBorderR2",
        "id": "flxCoupon",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCoupon.setDefaultUnit(kony.flex.DP);
    var flxCoupon1Info = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "145dp",
        "id": "flxCoupon1Info",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxCoupon1Info.setDefaultUnit(kony.flex.DP);
    var flexCoupons1Inner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flexCoupons1Inner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexCoupons1Inner.setDefaultUnit(kony.flex.DP);
    var lblAvailability = new kony.ui.Label({
        "id": "lblAvailability",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "IN STORE",
        "top": "6dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flexInner11 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexInner11",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexInner11.setDefaultUnit(kony.flex.DP);
    var lblOfferTitle1 = new kony.ui.Label({
        "id": "lblOfferTitle1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold44pxRed",
        "text": "Extra 40% ",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flexInner11.add(lblOfferTitle1);
    var lblOfferTitle2 = new kony.ui.Label({
        "id": "lblOfferTitle2",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblOfferSubTitle = new kony.ui.Label({
        "id": "lblOfferSubTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblValidity = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblValidity",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "7dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPromoCode = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPromoCode",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flexCoupons1Inner.add(lblAvailability, flexInner11, lblOfferTitle2, lblOfferSubTitle, lblValidity, lblPromoCode);
    flxCoupon1Info.add(flexCoupons1Inner);
    var flxCoupon2Info = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "205dp",
        "id": "flxCoupon2Info",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 20
    }, {}, {});
    flxCoupon2Info.setDefaultUnit(kony.flex.DP);
    var flex2Inner1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "80dp",
        "id": "flex2Inner1",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex2Inner1.setDefaultUnit(kony.flex.DP);
    var flex2Inner11 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flex2Inner11",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex2Inner11.setDefaultUnit(kony.flex.DP);
    var lblOfferTitleHidden1 = new kony.ui.Label({
        "height": "36dp",
        "id": "lblOfferTitleHidden1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "Extra 40% OFF",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flex2Inner11.add(lblOfferTitleHidden1);
    var lblOfferTitleHidden2 = new kony.ui.Label({
        "id": "lblOfferTitleHidden2",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblOfferSubTitleHidden = new kony.ui.Label({
        "id": "lblOfferSubTitleHidden",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblR207G32B47Px7",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblValidityHidden = new kony.ui.Label({
        "centerX": "50%",
        "height": "10dp",
        "id": "lblValidityHidden",
        "isVisible": true,
        "skin": "sknLblBlack7Px",
        "text": "VALID THRU SAT 1/21/17",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flex2Inner1.add(flex2Inner11, lblOfferTitleHidden2, lblOfferSubTitleHidden, lblValidityHidden);
    var flex2Inner2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flex2Inner2",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "92dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex2Inner2.setDefaultUnit(kony.flex.DP);
    var imgBarcode2 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "50dp",
        "id": "imgBarcode2",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "0dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flexBtns = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35dp",
        "id": "flexBtns",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexBtns.setDefaultUnit(kony.flex.DP);
    var btnRestrictions = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnCouponExpandFoc",
        "height": "28dp",
        "id": "btnRestrictions",
        "isVisible": true,
        "left": "3%",
        "skin": "sknBtnCouponExpand",
        "text": "VIEW RESTRICTIONS",
        "top": "0dp",
        "width": "150dp",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnAddToWallet = new kony.ui.Button({
        "focusSkin": "sknBtnCouponExpandFoc",
        "height": "32dp",
        "id": "btnAddToWallet",
        "isVisible": false,
        "right": "3%",
        "skin": "sknBtnCouponExpand",
        "text": "ADD TO WALLET",
        "top": "0dp",
        "width": "120dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flexBtns.add(btnRestrictions, btnAddToWallet);
    flex2Inner2.add(imgBarcode2, flexBtns);
    var flex2Inner3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flex2Inner3",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flex2Inner3.setDefaultUnit(kony.flex.DP);
    var lblNoBarcode = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoBarcode",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblNoBarCodeDetails = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblNoBarCodeDetails",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flex2Inner3.add(lblNoBarcode, lblNoBarCodeDetails);
    flxCoupon2Info.add(flex2Inner1, flex2Inner2, flex2Inner3);
    flxCoupon.add(flxCoupon1Info, flxCoupon2Info);
    flxCouponMainContainer.add(flxCoupon);
}