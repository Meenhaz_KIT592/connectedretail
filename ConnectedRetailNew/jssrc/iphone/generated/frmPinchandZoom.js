function addWidgetsfrmPinchandZoom() {
    frmPinchandZoom.setDefaultUnit(kony.flex.DP);
    var flxmaincontainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxmaincontainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxmaincontainer.setDefaultUnit(kony.flex.DP);
    var btnClose = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransMed32pxRed",
        "height": "20dp",
        "id": "btnClose",
        "isVisible": true,
        "right": "3%",
        "skin": "sknBtnBgTransMed32pxRed",
        "top": "2%",
        "width": "20dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var zoomImageIOS = new CustomWidgetsIOS.ZoomImage({
        "id": "zoomImageIOS",
        "isVisible": true,
        "left": "0dp",
        "top": "2%",
        "width": "90%",
        "height": "70%",
        "centerX": "50%",
        "zIndex": 1,
        "imageHeight": "1300",
        "imageWidth": "1200"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "widgetName": "ZoomImage"
    });
    var flxAndroid = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70%",
        "id": "flxAndroid",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "70%",
        "zIndex": 1
    }, {}, {});
    flxAndroid.setDefaultUnit(kony.flex.DP);
    var imageAndroid = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100%",
        "id": "imageAndroid",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "white_1012x628.png",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAndroid.add(imageAndroid);
    var FlexContainer0646087cf4e3247 = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "20%",
        "horizontalScrollIndicator": false,
        "id": "FlexContainer0646087cf4e3247",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "top": "30dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0646087cf4e3247.setDefaultUnit(kony.flex.DP);
    var flxThumbs = new kony.ui.FlexContainer({
        "centerX": "50%",
        "clipBounds": true,
        "height": "34%",
        "id": "flxThumbs",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxThumbs.setDefaultUnit(kony.flex.DP);
    flxThumbs.add();
    FlexContainer0646087cf4e3247.add(flxThumbs);
    flxmaincontainer.add(btnClose, zoomImageIOS, flxAndroid, FlexContainer0646087cf4e3247);
    frmPinchandZoom.add(flxmaincontainer);
};

function frmPinchandZoomGlobals() {
    frmPinchandZoom = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPinchandZoom,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmPinchandZoom",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};