function addWidgetsfrmMyAccount() {
    frmMyAccount.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var FlexContainer09cd932d3576b4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "FlexContainer09cd932d3576b4a",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    FlexContainer09cd932d3576b4a.setDefaultUnit(kony.flex.DP);
    var lblWelcomeName = new kony.ui.Label({
        "id": "lblWelcomeName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxDark",
        "text": "Welcome Gennie Williams",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblLoggedmMailId = new kony.ui.Label({
        "id": "lblLoggedmMailId",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24pxDark",
        "text": "You’re signed-in as gwilliams@gmail.com",
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnViewRewards = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnViewRewards",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyAccount.btnViewRewards"),
        "top": "20dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnSignOut = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnGothamMedium28pxBlueFoc",
        "height": "35dp",
        "id": "btnSignOut",
        "isVisible": true,
        "skin": "sknBtnGothamMedium28pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyAccount.btnSignOut"),
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    FlexContainer09cd932d3576b4a.add(lblWelcomeName, lblLoggedmMailId, btnViewRewards, btnSignOut);
    flxMainContainer.add(FlexContainer09cd932d3576b4a);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterHamMenu.btnMyAccount"),
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "25%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeaderTitleContainer.add(lblFormTitle, btnHeaderLeft, lblChevron);
    var km5f2dad8f3b2435f8303ec1795c88246 = new kony.ui.FlexContainer({
        "isMaster": true,
        "id": "flexCouponContainer",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "54dp",
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "-100%",
        "skin": "sknFormBgWhiteOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    km5f2dad8f3b2435f8303ec1795c88246.setDefaultUnit(kony.flex.DP);
    var kmb5c1807045d48ff893fc4a2ef2a8908 = new kony.ui.FlexContainer({
        "id": "flxOverlay",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRedNoBdr",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmb5c1807045d48ff893fc4a2ef2a8908.setDefaultUnit(kony.flex.DP);
    kmb5c1807045d48ff893fc4a2ef2a8908.add();
    var km41e6ca0a09f47d7b4f00111b44e55d9 = new kony.ui.FlexContainer({
        "id": "flxCouponHeader",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteBdrRound2",
        "top": "10dp",
        "width": "95%"
    }, {}, {});
    km41e6ca0a09f47d7b4f00111b44e55d9.setDefaultUnit(kony.flex.DP);
    var kmaae304097f04f2c86629a63f23c98c1 = new kony.ui.Label({
        "id": "lblCouponTitle",
        "centerY": "53%",
        "isVisible": true,
        "left": "20%",
        "skin": "sknLblGothamMedium36pxRed",
        "text": "COUPONS",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km43d52249597402ab1c94a296315b3ce = new kony.ui.Button({
        "id": "btnDone",
        "focusSkin": "sknBtnDoneFoc",
        "height": "100%",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnDone",
        "text": "Done",
        "top": "0dp",
        "width": "80dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 4, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km93d3845551c4b21b83f519110f43281 = new kony.ui.Button({
        "id": "btnCoupon",
        "focusSkin": "sknBtnCoupon",
        "height": "100%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnCoupon",
        "text": "C ",
        "top": "0dp",
        "width": "40dp",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km68164b088d5485887404141386f0509 = new kony.ui.FlexContainer({
        "id": "flxSeparatorLine",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2dp",
        "clipBounds": true,
        "height": "1dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    km68164b088d5485887404141386f0509.setDefaultUnit(kony.flex.DP);
    km68164b088d5485887404141386f0509.add();
    var km7c15c67c1614ee0b16b0b688297a9ae = new kony.ui.Button({
        "id": "btnCouponMainMaster",
        "centerY": "50%",
        "focusSkin": "sknBtnCouponFoc",
        "height": "35dp",
        "isVisible": true,
        "left": "11dp",
        "right": "2.50%",
        "skin": "sknBtnCouponBox",
        "text": "C ",
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km41e6ca0a09f47d7b4f00111b44e55d9.add(kmaae304097f04f2c86629a63f23c98c1, km43d52249597402ab1c94a296315b3ce, km93d3845551c4b21b83f519110f43281, km68164b088d5485887404141386f0509, km7c15c67c1614ee0b16b0b688297a9ae);
    var km68d6a679450457c927e0d73e39a6f3b = new kony.ui.FlexScrollContainer({
        "id": "flxCouponContents",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "10dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": false,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "2.50%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "58dp",
        "verticalScrollIndicator": true,
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km68d6a679450457c927e0d73e39a6f3b.setDefaultUnit(kony.flex.DP);
    var km43a69d2bad444a880c32c475bc9db0c = new kony.ui.FlexContainer({
        "id": "flxCouponRewards",
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km43a69d2bad444a880c32c475bc9db0c.setDefaultUnit(kony.flex.DP);
    var km48244b0a4a543c5a5e0467ec5e308f6 = new kony.ui.FlexContainer({
        "id": "flxRewardTxt",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "17dp",
        "width": "100%"
    }, {}, {});
    km48244b0a4a543c5a5e0467ec5e308f6.setDefaultUnit(kony.flex.DP);
    var km3008c46589441cbb1389e257d44b280 = new kony.ui.Label({
        "id": "lblRewardText",
        "centerY": "50%",
        "height": "15dp",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblGothamMedium24pxWhite",
        "text": "MICHAELS REWARDS CARD",
        "top": "17dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km48244b0a4a543c5a5e0467ec5e308f6.add(km3008c46589441cbb1389e257d44b280);
    var km6562b814a2d4dfb9b811d4f8deb51de = new kony.ui.FlexContainer({
        "id": "flxBarCodeWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "90dp",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "21dp",
        "width": "280dp",
        "zIndex": 1
    }, {}, {});
    km6562b814a2d4dfb9b811d4f8deb51de.setDefaultUnit(kony.flex.DP);
    var km09be51dba5d4dd1b43187a09b2e7e70 = new kony.ui.Image2({
        "id": "imgBarcode",
        "centerX": "50%",
        "centerY": "50%",
        "height": "55dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "barcode.png",
        "top": "8dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km6562b814a2d4dfb9b811d4f8deb51de.add(km09be51dba5d4dd1b43187a09b2e7e70);
    var kmc55d9017108473093da0503b2cf16ac = new kony.ui.FlexContainer({
        "id": "flxNotUser",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "21dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmc55d9017108473093da0503b2cf16ac.setDefaultUnit(kony.flex.DP);
    var km830a3ac239e430a9356d32921d8b4c7 = new kony.ui.Button({
        "id": "btnSignUp",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "right": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN UP",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var km504c6192c5e49709168c6345cff7b5c = new kony.ui.Button({
        "id": "btnSignIn",
        "centerY": "50%",
        "focusSkin": "sknBtnPrimaryWhiteFoc",
        "height": "30dp",
        "isVisible": true,
        "left": "51%",
        "skin": "sknBtnPrimaryWhite",
        "text": "SIGN IN",
        "width": "110dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    kmc55d9017108473093da0503b2cf16ac.add(km830a3ac239e430a9356d32921d8b4c7, km504c6192c5e49709168c6345cff7b5c);
    var km6137ac47fc14da7b63c50d0cf03e30c = new kony.ui.Label({
        "id": "lblSeparator100",
        "height": "1dp",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSeparatorWhiteOpaque",
        "top": "19dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km43a69d2bad444a880c32c475bc9db0c.add(km48244b0a4a543c5a5e0467ec5e308f6, km6562b814a2d4dfb9b811d4f8deb51de, kmc55d9017108473093da0503b2cf16ac, km6137ac47fc14da7b63c50d0cf03e30c);
    var kmaeb82d966df403eb07f4ceeeeba17a6 = new kony.ui.FlexScrollContainer({
        "id": "scroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlexBoxDarkRed",
        "top": "98dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmaeb82d966df403eb07f4ceeeeba17a6.setDefaultUnit(kony.flex.DP);
    kmaeb82d966df403eb07f4ceeeeba17a6.add();
    km68d6a679450457c927e0d73e39a6f3b.add(km43a69d2bad444a880c32c475bc9db0c, kmaeb82d966df403eb07f4ceeeeba17a6);
    var km7a65e190ac5442dace115b70e5cb90b = new kony.ui.FlexContainer({
        "id": "flxCouponDetailsPopup",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "10dp",
        "centerX": "50%",
        "clipBounds": false,
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgDarkRedBdrRound2",
        "top": "10dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    km7a65e190ac5442dace115b70e5cb90b.setDefaultUnit(kony.flex.DP);
    var km355dfea886f497bae5a5fe2512234e2 = new kony.ui.FlexScrollContainer({
        "id": "flxCouponDetailsScroll",
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "99%",
        "horizontalScrollIndicator": true,
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    km355dfea886f497bae5a5fe2512234e2.setDefaultUnit(kony.flex.DP);
    var kma78c0d329784fcc941f558997b77640 = new kony.ui.FlexContainer({
        "id": "flxCloseBtnWrap",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50dp"
    }, {}, {});
    kma78c0d329784fcc941f558997b77640.setDefaultUnit(kony.flex.DP);
    var km2b17e1f998c478a83c94800a0018628 = new kony.ui.Button({
        "id": "btnPopupClose",
        "focusSkin": "sknBtnPopupCloseFoc",
        "height": "40dp",
        "isVisible": false,
        "left": "0dp",
        "right": "0dp",
        "skin": "sknBtnIconWhite36Px",
        "text": "X",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmb84d9aaa46b432893eb0a730fd63a01 = new kony.ui.Image2({
        "id": "Image0a5620b6427ec41",
        "height": "100%",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_close_white.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kma78c0d329784fcc941f558997b77640.add(km2b17e1f998c478a83c94800a0018628, kmb84d9aaa46b432893eb0a730fd63a01);
    var km92a8a7512da4e26b0e56a58c0be0b7e = new kony.ui.FlexContainer({
        "id": "flexRestrictionsCode",
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "177dp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexCoupons",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km92a8a7512da4e26b0e56a58c0be0b7e.setDefaultUnit(kony.flex.DP);
    var km28dd339e2044c58adc34e12f002314f = new kony.ui.FlexContainer({
        "id": "flexInner1",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": true,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    km28dd339e2044c58adc34e12f002314f.setDefaultUnit(kony.flex.DP);
    var kmbec617442df42439d91d87fd4466227 = new kony.ui.Label({
        "id": "lblOfferTitle1Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold30pxRed",
        "text": "40",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc61765e98054ef68abcb9ce7670ca41 = new kony.ui.Label({
        "id": "lblOfferTitle2Restrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km02e056c920042f2ae479005334ba252 = new kony.ui.Label({
        "id": "lblOfferSubTitleRestrictions",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold20pxRed",
        "text": "Any One Regular Price Item",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmba7ae9a99dd4177acd2428d29eb5044 = new kony.ui.Label({
        "id": "lblValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km6afb41ce7fd4a279e7a0fb37365371b = new kony.ui.Label({
        "id": "lblPromoCodeRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold16pxBlack",
        "text": "PROMO CODE 40SAVE11517",
        "top": "2dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km9858fa2396c4538978c7b8537d03058 = new kony.ui.Image2({
        "id": "imgBarcodeRestrictions",
        "centerX": "50%",
        "height": "60dp",
        "isVisible": true,
        "skin": "slImage",
        "src": "promocode.png",
        "top": "17dp",
        "width": "215dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km28dd339e2044c58adc34e12f002314f.add(kmbec617442df42439d91d87fd4466227, kmc61765e98054ef68abcb9ce7670ca41, km02e056c920042f2ae479005334ba252, kmba7ae9a99dd4177acd2428d29eb5044, km6afb41ce7fd4a279e7a0fb37365371b, km9858fa2396c4538978c7b8537d03058);
    var kmcf1fbebd7f748a78eb2c6891fd191ee = new kony.ui.FlexContainer({
        "id": "flexInner2",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "isVisible": false,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    kmcf1fbebd7f748a78eb2c6891fd191ee.setDefaultUnit(kony.flex.DP);
    var kme48f9bfaeb346cea17a765fb8e18e47 = new kony.ui.Label({
        "id": "lblNoBarcode",
        "centerX": "50%",
        "isVisible": true,
        "left": "5dp",
        "skin": "sknLblBlackPx14",
        "text": "NO BAR CODE REQUIRED",
        "top": "49dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km03b052a5e1e4b30a7f67cdd89392764 = new kony.ui.Label({
        "id": "lblNoBarCodeDetails",
        "centerX": "50%",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblR194G7B36Px14",
        "text": "NO BAR CODE REQUIRED",
        "top": "11dp",
        "width": "96.50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km0b0708fdcfb4e0fa6bff0ca12079fe0 = new kony.ui.Label({
        "id": "lblNoBarcodeValidityRestrictions",
        "centerX": "50%",
        "isVisible": true,
        "skin": "sknLblGothamBold14pxBlack",
        "text": "VALID THRU SAT 1/21/17",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    kmcf1fbebd7f748a78eb2c6891fd191ee.add(kme48f9bfaeb346cea17a765fb8e18e47, km03b052a5e1e4b30a7f67cdd89392764, km0b0708fdcfb4e0fa6bff0ca12079fe0);
    km92a8a7512da4e26b0e56a58c0be0b7e.add(km28dd339e2044c58adc34e12f002314f, kmcf1fbebd7f748a78eb2c6891fd191ee);
    var kma0f5b84cc8245efb31dad0ca9be587e = new kony.ui.Label({
        "id": "lblRestrictions",
        "centerX": "50%",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblGothamRegular20pxWhite",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "textStyle": {
            "lineSpacing": 5,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kme6e2e81083d45a6a2c28ff6f48c3ce3 = new kony.ui.RichText({
        "id": "rchTxtRetrictions",
        "centerX": "50%",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknRTGGothamBook72",
        "text": "Limit one coupon per product. Limit one coupon of each type per day. Not valid on: Doorbusters; Everyday Value program; Silhouette, Cricut & Canon brands; Polaroid products, special order custom floral arrangements, custom frames & materials, services & package pricing; custom invitations, canvas prints & photo center purchases; Wacom products, Brother, Sensu Brush & Stylus, 3D printers & accessories, 3Doodler 3D Printing Pen & accessories, sewing machines, Typecast typewriters, books, coloring books, magazines, beverages, CD/DVDs, gift cards & debit card products; sale, clearance or buy & get items; online-only products & specials; class, event, birthday party, shipping, delivery or installation fees. Printed coupon must be surrendered or electronic copy scanned at purchase. Not applicable to prior purchases. Limited to stock on hand. Void where prohibited. Exclusions subject to change. See Team Member for details.",
        "top": "18dp",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    km355dfea886f497bae5a5fe2512234e2.add(kma78c0d329784fcc941f558997b77640, km92a8a7512da4e26b0e56a58c0be0b7e, kma0f5b84cc8245efb31dad0ca9be587e, kme6e2e81083d45a6a2c28ff6f48c3ce3);
    km7a65e190ac5442dace115b70e5cb90b.add(km355dfea886f497bae5a5fe2512234e2);
    km5f2dad8f3b2435f8303ec1795c88246.add(kmb5c1807045d48ff893fc4a2ef2a8908, km41e6ca0a09f47d7b4f00111b44e55d9, km68d6a679450457c927e0d73e39a6f3b, km7a65e190ac5442dace115b70e5cb90b);
    var km2e94263c440446090f998206f76157b = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km2e94263c440446090f998206f76157b.setDefaultUnit(kony.flex.DP);
    var km4b872cec78b4b4d91cb805997e84aa0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4b872cec78b4b4d91cb805997e84aa0.setDefaultUnit(kony.flex.DP);
    var kmc2885561304433db4f2f5489ede0f0d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km451d24c3ce84e10b12749f078ebc437 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km4b872cec78b4b4d91cb805997e84aa0.add(kmc2885561304433db4f2f5489ede0f0d, km451d24c3ce84e10b12749f078ebc437);
    var kmff44e35f8dd438cbf86f47181bead55 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmff44e35f8dd438cbf86f47181bead55.setDefaultUnit(kony.flex.DP);
    var km5be22c2af6e4548ba3ce036e6dd3a8c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmd9b4fb88ac5467b96c9f418bc4a8fa1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km2f2deedd7134bef9b4d76cf16d4b5e1 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmff44e35f8dd438cbf86f47181bead55.add(km5be22c2af6e4548ba3ce036e6dd3a8c, kmd9b4fb88ac5467b96c9f418bc4a8fa1, km2f2deedd7134bef9b4d76cf16d4b5e1);
    var km9b9f4f9b3a5465f92467ff58fe983d1 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km9b9f4f9b3a5465f92467ff58fe983d1.setDefaultUnit(kony.flex.DP);
    var km20ddf280048419382a722de9c644c5d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc8907cd66c3410dbd3a53ece4faff32 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km9b9f4f9b3a5465f92467ff58fe983d1.add(km20ddf280048419382a722de9c644c5d, kmc8907cd66c3410dbd3a53ece4faff32);
    var km94609517e0849f5b2d25f7c93a9bd8f = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km94609517e0849f5b2d25f7c93a9bd8f.setDefaultUnit(kony.flex.DP);
    var kmd7672defc5343b4a4ccb1b4a7c56e0b = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7de98f1540449e0948ae86f8bdfe5f2 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km94609517e0849f5b2d25f7c93a9bd8f.add(kmd7672defc5343b4a4ccb1b4a7c56e0b, km7de98f1540449e0948ae86f8bdfe5f2);
    var km1aff38547624ecb8e30e5eb2da2dd04 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km1aff38547624ecb8e30e5eb2da2dd04.setDefaultUnit(kony.flex.DP);
    var kmb40d7ad24e8460582e92e00f83875ad = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7bf874c71414d7ab5077e384f4bba73 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km1aff38547624ecb8e30e5eb2da2dd04.add(kmb40d7ad24e8460582e92e00f83875ad, km7bf874c71414d7ab5077e384f4bba73);
    var kmc08cfc921c84e1eba9ff2494a8d3899 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc08cfc921c84e1eba9ff2494a8d3899.setDefaultUnit(kony.flex.DP);
    var km0c7fa9c038c48c2a25f69c108c306b0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km9a1e8640bb648fea475ca4b239a7b81 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmc08cfc921c84e1eba9ff2494a8d3899.add(km0c7fa9c038c48c2a25f69c108c306b0, km9a1e8640bb648fea475ca4b239a7b81);
    var km174b0b379c14f6fb5c45d7d0037579b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km174b0b379c14f6fb5c45d7d0037579b.setDefaultUnit(kony.flex.DP);
    var kmc81e4550ff84dab9968a1eab27d84ed = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmf10a900ccc64da194405ea4e9d74c2d = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km174b0b379c14f6fb5c45d7d0037579b.add(kmc81e4550ff84dab9968a1eab27d84ed, kmf10a900ccc64da194405ea4e9d74c2d);
    var km09eb5b382b341e2bf26d8706144eb3b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km09eb5b382b341e2bf26d8706144eb3b.setDefaultUnit(kony.flex.DP);
    var km8f01c0f1ba14ff19d012db9e6cafd95 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km7da1e33c19a4ba6b704436ce11a6108 = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km09eb5b382b341e2bf26d8706144eb3b.add(km8f01c0f1ba14ff19d012db9e6cafd95, km7da1e33c19a4ba6b704436ce11a6108);
    km2e94263c440446090f998206f76157b.add(km4b872cec78b4b4d91cb805997e84aa0, kmff44e35f8dd438cbf86f47181bead55, km9b9f4f9b3a5465f92467ff58fe983d1, km94609517e0849f5b2d25f7c93a9bd8f, km1aff38547624ecb8e30e5eb2da2dd04, kmc08cfc921c84e1eba9ff2494a8d3899, km174b0b379c14f6fb5c45d7d0037579b, km09eb5b382b341e2bf26d8706144eb3b);
    frmMyAccount.add(flxMainContainer, flxHeaderTitleContainer, km5f2dad8f3b2435f8303ec1795c88246, km2e94263c440446090f998206f76157b);
};

function frmMyAccountGlobals() {
    frmMyAccount = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMyAccount,
        "enabledForIdleTimeout": true,
        "id": "frmMyAccount",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};