function initializetempProjectMarket() {
    flxProjectMarketWrap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "170dp",
        "id": "flxProjectMarketWrap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgTransBorderLtGray"
    }, {}, {});
    flxProjectMarketWrap.setDefaultUnit(kony.flex.DP);
    var imgProjectMarket = new kony.ui.Image2({
        "height": "100%",
        "id": "imgProjectMarket",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblProjectMarketTitle = new kony.ui.Label({
        "bottom": "0dp",
        "centerX": "50%",
        "height": "40dp",
        "id": "lblProjectMarketTitle",
        "isVisible": true,
        "skin": "sknLblProjectMarketTitle",
        "text": "Label",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxProjectMarketWrap.add(imgProjectMarket, lblProjectMarketTitle);
}