function initializetempShoppingListHeader() {
    flxPpoducts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxPpoducts",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayBordere0e0e0",
        "top": "10dp",
        "width": "100%"
    }, {}, {});
    flxPpoducts.setDefaultUnit(kony.flex.DP);
    var flexMyListHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flexMyListHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skinFlexBG246",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flexMyListHeader.setDefaultUnit(kony.flex.DP);
    var lblShoppingListHdr = new kony.ui.Label({
        "id": "lblShoppingListHdr",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBook24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.products"),
        "textStyle": {
            "lineSpacing": 10,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "10dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 1],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnExpand = new kony.ui.Button({
        "focusSkin": "sknBtnIcon36pxDark",
        "id": "btnExpand",
        "isVisible": true,
        "right": "2%",
        "skin": "sknBtnIcon36pxDark",
        "text": "U",
        "top": "10dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flexMyListHeader.add(lblShoppingListHdr, btnExpand);
    flxPpoducts.add(flexMyListHeader);
}