function addWidgetsfrmRewardsProfileStep4() {
    frmRewardsProfileStep4.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var FlexContainer0b4738c7579e24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "FlexContainer0b4738c7579e24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    FlexContainer0b4738c7579e24b.setDefaultUnit(kony.flex.DP);
    var Label0c5601159c0bb4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0c5601159c0bb4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold28pxWhite",
        "text": "Step 4 - Preferred Store",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexContainer0b4738c7579e24b.add(Label0c5601159c0bb4c);
    var CopyFlexContainer07bd5a01d54e54f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "56dp",
        "id": "CopyFlexContainer07bd5a01d54e54f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer07bd5a01d54e54f.setDefaultUnit(kony.flex.DP);
    var flxSearchContents = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "36dp",
        "id": "flxSearchContents",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "45dp",
        "isModalContainer": false,
        "right": "56dp",
        "skin": "sknFlxBorderTextbox",
        "width": "94%"
    }, {}, {});
    flxSearchContents.setDefaultUnit(kony.flex.DP);
    var lblSearchIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSearchIcon",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblIconSearch",
        "text": "S",
        "top": "5dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var txtHidebox = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "0dp",
        "id": "txtHidebox",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "text": "TextBox2",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "8dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var textSearch = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTextSearch",
        "height": "100%",
        "id": "textSearch",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.searchLocationPlaceholder"),
        "secureTextEntry": false,
        "skin": "sknTextSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [15, 1, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_SEARCH,
        "placeholderSkin": "sknTextSearchPlaceholder",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnClearSearchText = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransNoText",
        "height": "40dp",
        "id": "btnClearSearchText",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnTransNoText",
        "width": "40dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxSearchContents.add(lblSearchIcon, txtHidebox, textSearch, btnClearSearchText);
    var flxLineSeparator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLineSeparator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD1D5DD",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLineSeparator.setDefaultUnit(kony.flex.DP);
    flxLineSeparator.add();
    CopyFlexContainer07bd5a01d54e54f.add(flxSearchContents, flxLineSeparator);
    var flxTapToSelFavStore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "4%",
        "id": "flxTapToSelFavStore",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxTapToSelFavStore",
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxTapToSelFavStore.setDefaultUnit(kony.flex.DP);
    var lblTap = new kony.ui.Label({
        "height": "100%",
        "id": "lblTap",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknTapToSelFavStore",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.tap"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnFavorite = new kony.ui.Button({
        "focusSkin": "sknBtnFavStoreIcon",
        "height": "100%",
        "id": "btnFavorite",
        "isVisible": true,
        "left": "3dp",
        "right": "3dp",
        "skin": "sknBtnFavStoreIcon",
        "text": "f",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var lblSelFavStore = new kony.ui.Label({
        "height": "100%",
        "id": "lblSelFavStore",
        "isVisible": true,
        "skin": "sknTapToSelFavStore",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmFindStoreMapView.selFavStore"),
        "textTruncatePosition": constants.TEXT_TRUNCATE_NONE,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTapToSelFavStore.add(lblTap, btnFavorite, lblSelFavStore);
    var segStoresListView = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "btnFavorite": "f",
            "lblAddressLine1": "5301 Beltline Rd, Ste 101",
            "lblAddressLine2": "Dallas, TX 75254",
            "lblDistance": "0.6 mi",
            "lblPhone": "972.385.9228",
            "lblStock": "5",
            "lblStockTitle": "In Stock"
        }, {
            "btnFavorite": "f",
            "lblAddressLine1": "5301 Beltline Rd, Ste 101",
            "lblAddressLine2": "Dallas, TX 75254",
            "lblDistance": "0.6 mi",
            "lblPhone": "972.385.9228",
            "lblStock": "5",
            "lblStockTitle": "In Stock"
        }, {
            "btnFavorite": "f",
            "lblAddressLine1": "5301 Beltline Rd, Ste 101",
            "lblAddressLine2": "Dallas, TX 75254",
            "lblDistance": "0.6 mi",
            "lblPhone": "972.385.9228",
            "lblStock": "5",
            "lblStockTitle": "In Stock"
        }],
        "groupCells": false,
        "id": "segStoresListView",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxRewardStoreLoc,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "btnFavorite": "btnFavorite",
            "flxDirections": "flxDirections",
            "flxRewardStoreLoc": "flxRewardStoreLoc",
            "flxSeparatorTop": "flxSeparatorTop",
            "flxStockInfo": "flxStockInfo",
            "flxStoreDetails": "flxStoreDetails",
            "flxStoreDetailsCollapse": "flxStoreDetailsCollapse",
            "lblAddressLine1": "lblAddressLine1",
            "lblAddressLine2": "lblAddressLine2",
            "lblDistance": "lblDistance",
            "lblPhone": "lblPhone",
            "lblStock": "lblStock",
            "lblStockTitle": "lblStockTitle"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    var btnBuyOnline2 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnBuyOnline2",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.exitAndFinishLater"),
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var CopybtnBuyOnline069ef7a9f8bf74c = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "CopybtnBuyOnline069ef7a9f8bf74c",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": "SKIP",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "64dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flxMainContainer.add(flxTopSpacerDND, FlexContainer0b4738c7579e24b, CopyFlexContainer07bd5a01d54e54f, flxTapToSelFavStore, segStoresListView, btnBuyOnline2, CopybtnBuyOnline069ef7a9f8bf74c, flxBottomSpacerDND);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": "REWARDS PROFILE",
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, lblChevron, btnHeaderLeft);
    var flxOptionalResultsContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "focusSkin": "sknFlxSboxBgWhite",
        "horizontalScrollIndicator": true,
        "id": "flxOptionalResultsContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "146dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxOptionalResultsContainer.setDefaultUnit(kony.flex.DP);
    var segOptionalStoreResults = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }, {
            "lblOptionalResults": ""
        }],
        "groupCells": false,
        "id": "segOptionalStoreResults",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxOptionalResults,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxOptionalResults": "flxOptionalResults",
            "lblOptionalResults": "lblOptionalResults"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    flxOptionalResultsContainer.add(segOptionalStoreResults);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialogbox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxDialogbox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxDialogbox.setDefaultUnit(kony.flex.DP);
    var lbl1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl1",
        "isVisible": true,
        "skin": "sknLblSFNSBold",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.confirmationAlert"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbl2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSFNSRegular",
        "text": kony.i18n.getLocalizedString("i18n.rewardsProfile.confirmMessage"),
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "40dp",
        "id": "btnOk",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.ok"),
        "top": "10dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    var btnYes = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.okayButton"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxDialogbox.add(lbl1, lbl2, btnOk, flxLine, btnYes);
    flxOverlay.add(flxDialogbox);
    var kme685b22b3684049a552e0d660da2cbb = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    kme685b22b3684049a552e0d660da2cbb.setDefaultUnit(kony.flex.DP);
    var kmc1dfb9505d74413acfaf4ee5c3596ca = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmc1dfb9505d74413acfaf4ee5c3596ca.setDefaultUnit(kony.flex.DP);
    var kmd2e7e3f5bfd42669428e578453cce5a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km10ed343a17f46248001530eed0c5dc9 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmc1dfb9505d74413acfaf4ee5c3596ca.add(kmd2e7e3f5bfd42669428e578453cce5a, km10ed343a17f46248001530eed0c5dc9);
    var km4626cf980394d50965ae4f1612a22af = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km4626cf980394d50965ae4f1612a22af.setDefaultUnit(kony.flex.DP);
    var kmbddfc2ed1644f988cbfcbbf7c4fa46e = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km61a45be05704aa78c4dbb42af95bbc9 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km807b66ef1c445afaeeb729e1e633864 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km4626cf980394d50965ae4f1612a22af.add(kmbddfc2ed1644f988cbfcbbf7c4fa46e, km61a45be05704aa78c4dbb42af95bbc9, km807b66ef1c445afaeeb729e1e633864);
    var kma2a5a1836374526b847c7e0c8eba125 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma2a5a1836374526b847c7e0c8eba125.setDefaultUnit(kony.flex.DP);
    var km74cdec56a4f415c937689a217c7ddb9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km535ae54640b49e5af9b99bc9486233a = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kma2a5a1836374526b847c7e0c8eba125.add(km74cdec56a4f415c937689a217c7ddb9, km535ae54640b49e5af9b99bc9486233a);
    var km264f0ff315c46508baaca132857a398 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km264f0ff315c46508baaca132857a398.setDefaultUnit(kony.flex.DP);
    var km873fd5db727458fb7c816e3585bda9c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmca38144f1b0401cb0bca0638c37bcba = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km264f0ff315c46508baaca132857a398.add(km873fd5db727458fb7c816e3585bda9c, kmca38144f1b0401cb0bca0638c37bcba);
    var kmd0b46fc365149adbaaa3bf840c7e463 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd0b46fc365149adbaaa3bf840c7e463.setDefaultUnit(kony.flex.DP);
    var km0751dc9c5ab48979df4394b45d014c2 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km1ed85b620bd44aa920203b812093230 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmd0b46fc365149adbaaa3bf840c7e463.add(km0751dc9c5ab48979df4394b45d014c2, km1ed85b620bd44aa920203b812093230);
    var km56e6438d4c84d06ac336e131c42b3ef = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km56e6438d4c84d06ac336e131c42b3ef.setDefaultUnit(kony.flex.DP);
    var km8a60ed0bb684da49333367f6dbca86a = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmc97dbac69c54e0dbef917e4b18cd9c4 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km56e6438d4c84d06ac336e131c42b3ef.add(km8a60ed0bb684da49333367f6dbca86a, kmc97dbac69c54e0dbef917e4b18cd9c4);
    var km62a95df6bde4d438d2e7cc8d6ca4ee2 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km62a95df6bde4d438d2e7cc8d6ca4ee2.setDefaultUnit(kony.flex.DP);
    var km569f2db025a4201b15e897524d85815 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km12a9ae20fd04ccf987e1c14623debf1 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km62a95df6bde4d438d2e7cc8d6ca4ee2.add(km569f2db025a4201b15e897524d85815, km12a9ae20fd04ccf987e1c14623debf1);
    var kmd03760252a3470e8649772d1e5e14e3 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd03760252a3470e8649772d1e5e14e3.setDefaultUnit(kony.flex.DP);
    var km702343efa654ebd885f537a76f9386d = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmcff026e44dc487d8034125eb7970eee = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmd03760252a3470e8649772d1e5e14e3.add(km702343efa654ebd885f537a76f9386d, kmcff026e44dc487d8034125eb7970eee);
    kme685b22b3684049a552e0d660da2cbb.add(kmc1dfb9505d74413acfaf4ee5c3596ca, km4626cf980394d50965ae4f1612a22af, kma2a5a1836374526b847c7e0c8eba125, km264f0ff315c46508baaca132857a398, kmd0b46fc365149adbaaa3bf840c7e463, km56e6438d4c84d06ac336e131c42b3ef, km62a95df6bde4d438d2e7cc8d6ca4ee2, kmd03760252a3470e8649772d1e5e14e3);
    frmRewardsProfileStep4.add(flxMainContainer, flxHeaderTitleContainer, flxOptionalResultsContainer, flxOverlay, kme685b22b3684049a552e0d660da2cbb);
};

function frmRewardsProfileStep4Globals() {
    frmRewardsProfileStep4 = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmRewardsProfileStep4,
        "enabledForIdleTimeout": true,
        "id": "frmRewardsProfileStep4",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "transitionFade"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};