function addWidgetsfrmLocatorMap() {
    frmLocatorMap.setDefaultUnit(kony.flex.DP);
    var flxTopPart = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTopPart",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopPart.setDefaultUnit(kony.flex.DP);
    var flxNewHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxNewHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNewHeader.setDefaultUnit(kony.flex.DP);
    var btnHome = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "100%",
        "id": "btnHome",
        "isVisible": true,
        "left": "0dp",
        "skin": "slButtonGlossBlue",
        "text": "H",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxSearchNewContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxSearchNewContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50dp",
        "isModalContainer": false,
        "right": "50dp",
        "skin": "slFbox",
        "top": "17dp",
        "zIndex": 1
    }, {}, {});
    flxSearchNewContent.setDefaultUnit(kony.flex.DP);
    var txtSearchNew = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "100%",
        "id": "txtSearchNew",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0dp",
        "right": "0dp",
        "secureTextEntry": false,
        "skin": "slTextBox",
        "text": "TextBox2",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var btnNewCross = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "100%",
        "id": "btnNewCross",
        "isVisible": true,
        "right": 0,
        "skin": "slButtonGlossBlue",
        "text": "X",
        "top": "10dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxSearchNewContent.add(txtSearchNew, btnNewCross);
    var btnCoupon = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "100%",
        "id": "btnCoupon",
        "isVisible": true,
        "right": "0dp",
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "5dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxNewHeader.add(btnHome, flxSearchNewContent, btnCoupon);
    var flxProductsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxProductsNew",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProductsNew.setDefaultUnit(kony.flex.DP);
    flxProductsNew.add();
    var flxTabsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTabsNew",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTabsNew.setDefaultUnit(kony.flex.DP);
    var btnMapView = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "100%",
        "id": "btnMapView",
        "isVisible": true,
        "left": "0dp",
        "skin": "slButtonGlossBlue",
        "text": "Map",
        "top": "20dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnListView = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnListView",
        "isVisible": true,
        "left": "50%",
        "skin": "slButtonGlossBlue",
        "text": "List",
        "top": "17dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxTabsNew.add(btnMapView, btnListView);
    flxTopPart.add(flxNewHeader, flxProductsNew, flxTabsNew);
    var flxScrlMapList = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxScrlMapList",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "100dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrlMapList.setDefaultUnit(kony.flex.DP);
    var flxMapHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMapHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMapHolder.setDefaultUnit(kony.flex.DP);
    var flxMyStoreDetailsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxMyStoreDetailsNew",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxMyStoreDetailsNew.setDefaultUnit(kony.flex.DP);
    var flxStoreDetailsNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxStoreDetailsNew",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreDetailsNew.setDefaultUnit(kony.flex.DP);
    var btnFavouriteNew = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnFavouriteNew",
        "isVisible": true,
        "left": "10dp",
        "skin": "slButtonGlossBlue",
        "text": "F",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxStoreMyDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMyDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "60dp",
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "zIndex": 1
    }, {}, {});
    flxStoreMyDetails.setDefaultUnit(kony.flex.DP);
    var lblAddress1New = new kony.ui.Label({
        "id": "lblAddress1New",
        "isVisible": true,
        "left": "37dp",
        "skin": "sknLblIcon32pxBlue",
        "text": "t",
        "top": "8dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAddress2New = new kony.ui.Label({
        "id": "lblAddress2New",
        "isVisible": true,
        "left": "43dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "31dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnPhoneNew = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnPhoneNew",
        "isVisible": true,
        "left": "153dp",
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "40dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnExpandNew = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnExpandNew",
        "isVisible": true,
        "left": "41dp",
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "66dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxDirectionsNew = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "220dp",
        "id": "flxDirectionsNew",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "56dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "91dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDirectionsNew.setDefaultUnit(kony.flex.DP);
    var btnDirectionsNew = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnDirectionsNew",
        "isVisible": true,
        "left": "143dp",
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "99dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblDistanceNew = new kony.ui.Label({
        "id": "lblDistanceNew",
        "isVisible": true,
        "left": "77dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "83dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDirectionsNew.add(btnDirectionsNew, lblDistanceNew);
    flxStoreMyDetails.add(lblAddress1New, lblAddress2New, btnPhoneNew, btnExpandNew, flxDirectionsNew);
    flxStoreDetailsNew.add(btnFavouriteNew, flxStoreMyDetails);
    var flxStoreDetailsExpandNew = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxStoreDetailsExpandNew",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxStoreDetailsExpandNew.setDefaultUnit(kony.flex.DP);
    var flxDay1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay1.setDefaultUnit(kony.flex.DP);
    var lblDay1 = new kony.ui.Label({
        "id": "lblDay1",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime1 = new kony.ui.Label({
        "id": "lblDayTime1",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay1.add(lblDay1, lblDayTime1);
    var flxDay2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay2.setDefaultUnit(kony.flex.DP);
    var lblDay2 = new kony.ui.Label({
        "id": "lblDay2",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime2 = new kony.ui.Label({
        "id": "lblDayTime2",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay2.add(lblDay2, lblDayTime2);
    var flxDay3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay3.setDefaultUnit(kony.flex.DP);
    var lblDay3 = new kony.ui.Label({
        "id": "lblDay3",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime3 = new kony.ui.Label({
        "id": "lblDayTime3",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay3.add(lblDay3, lblDayTime3);
    var flxDay4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay4.setDefaultUnit(kony.flex.DP);
    var lblDay4 = new kony.ui.Label({
        "id": "lblDay4",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime4 = new kony.ui.Label({
        "id": "lblDayTime4",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay4.add(lblDay4, lblDayTime4);
    var flxDay5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay5.setDefaultUnit(kony.flex.DP);
    var lblDay5 = new kony.ui.Label({
        "id": "lblDay5",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime5 = new kony.ui.Label({
        "id": "lblDayTime5",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay5.add(lblDay5, lblDayTime5);
    var flxDay6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay6",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay6.setDefaultUnit(kony.flex.DP);
    var lblDay6 = new kony.ui.Label({
        "id": "lblDay6",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime6 = new kony.ui.Label({
        "id": "lblDayTime6",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay6.add(lblDay6, lblDayTime6);
    var flxDay7 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxDay7",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "9dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDay7.setDefaultUnit(kony.flex.DP);
    var lblDay7 = new kony.ui.Label({
        "id": "lblDay7",
        "isVisible": true,
        "left": "20dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDayTime7 = new kony.ui.Label({
        "id": "lblDayTime7",
        "isVisible": true,
        "left": "167dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "3dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDay7.add(lblDay7, lblDayTime7);
    var btnCollapseNew = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "btnCollapseNew",
        "isVisible": true,
        "left": "245dp",
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "37dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxStoreDetailsExpandNew.add(flxDay1, flxDay2, flxDay3, flxDay4, flxDay5, flxDay6, flxDay7, btnCollapseNew);
    flxMyStoreDetailsNew.add(flxStoreDetailsNew, flxStoreDetailsExpandNew);
    var mapLocatorNew = new kony.ui.Map({
        "calloutWidth": 80,
        "defaultPinImage": "pinb.png",
        "height": "100%",
        "id": "mapLocatorNew",
        "isVisible": true,
        "left": "0dp",
        "provider": constants.MAP_PROVIDER_GOOGLE,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {
        "mode": constants.MAP_VIEW_MODE_NORMAL,
        "showCurrentLocation": constants.MAP_VIEW_SHOW_CURRENT_LOCATION_NONE,
        "zoomLevel": 4
    });
    flxMapHolder.add(flxMyStoreDetailsNew, mapLocatorNew);
    var flxSegHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSegHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSegHolder.setDefaultUnit(kony.flex.DP);
    var segLocatorNew = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "groupCells": false,
        "height": "100%",
        "id": "segLocatorNew",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {},
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_ROW_SELECT,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    flxSegHolder.add(segLocatorNew);
    flxScrlMapList.add(flxMapHolder, flxSegHolder);
    frmLocatorMap.add(flxTopPart, flxScrlMapList);
};

function frmLocatorMapGlobals() {
    frmLocatorMap = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmLocatorMap,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmLocatorMap",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "bouncesZoom": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};