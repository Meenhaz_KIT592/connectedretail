function initializetempSearchOffers() {
    flxSearchOffer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxSearchOffer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxSearchOffer.setDefaultUnit(kony.flex.DP);
    var lblSearchOffer = new kony.ui.Label({
        "height": "100%",
        "id": "lblSearchOffer",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular32pxBlue",
        "text": "Label",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [10, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSearchOffer.add(lblSearchOffer);
}