function initializetempProjInstructions() {
    flxProjInstructions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxProjInstructions",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProjInstructions.setDefaultUnit(kony.flex.DP);
    var flxRichProjInstructionsMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxRichProjInstructionsMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "15dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRichProjInstructionsMain.setDefaultUnit(kony.flex.DP);
    var lblSno = new kony.ui.Label({
        "height": "25dp",
        "id": "lblSno",
        "isVisible": true,
        "left": "3%",
        "skin": "sknlblRoundedRedtxtWhite",
        "text": "1",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxRichProjInstructions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxRichProjInstructions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "13%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "76%",
        "zIndex": 1
    }, {}, {});
    flxRichProjInstructions.setDefaultUnit(kony.flex.DP);
    var rtxtProjInstructions = new kony.ui.RichText({
        "id": "rtxtProjInstructions",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopyslRichText069851950a5d341",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRichProjInstructions.add(rtxtProjInstructions);
    flxRichProjInstructionsMain.add(lblSno, flxRichProjInstructions);
    var lblSeperator1 = new kony.ui.Label({
        "height": "1dp",
        "id": "lblSeperator1",
        "isVisible": false,
        "left": "0%",
        "skin": "sknLblSeparatorGray",
        "top": "18dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxProjInstructions.add(flxRichProjInstructionsMain, lblSeperator1);
}