function initializetempProjectsCarousel() {
    CopyflxCarousel0e3d3ae00cb5f4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "168dp",
        "id": "CopyflxCarousel0e3d3ae00cb5f4d",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhite"
    }, {}, {});
    CopyflxCarousel0e3d3ae00cb5f4d.setDefaultUnit(kony.flex.DP);
    var imgBanner = new kony.ui.Image2({
        "height": "168dp",
        "id": "imgBanner",
        "imageWhenFailed": "notavailable_1012x628.png",
        "imageWhileDownloading": "white_1012x628.png",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var featureName = new kony.ui.Label({
        "height": "40dp",
        "id": "featureName",
        "isVisible": true,
        "left": "0dp",
        "skin": "skLbl32GothamMediumBorder",
        "text": "Label",
        "top": "-40dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxCarousel0e3d3ae00cb5f4d.add(imgBanner, featureName);
}