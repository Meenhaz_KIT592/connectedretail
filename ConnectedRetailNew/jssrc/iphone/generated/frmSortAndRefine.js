function addWidgetsfrmSortAndRefine() {
    frmSortAndRefine.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblHeader",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnRefineSearch"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnCancel = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransReg16pxWhiteFoc",
        "height": "50dp",
        "id": "btnCancel",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknBtnTransReg16pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.smallCancel"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnDone = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransReg16pxWhiteFoc",
        "height": "100%",
        "id": "btnDone",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransReg16pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnDone"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxHeader.add(lblHeader, btnCancel, btnDone);
    var flxClearAll = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxClearAll",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlxMedGrayBG",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxClearAll.setDefaultUnit(kony.flex.DP);
    var btnClearAll = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed32pxBlueFoc",
        "height": "100%",
        "id": "btnClearAll",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnClearAll"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxClearAll.add(btnClearAll);
    var flxScrlMain = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": "0dp",
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "flxScrlMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "92dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxScrlMain.setDefaultUnit(kony.flex.DP);
    var flxSortBy = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxSortBy",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSortBy.setDefaultUnit(kony.flex.DP);
    var lblSortBY = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSortBY",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold28pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.lblSortBY"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnExpandSortBy = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIcon36pxWhite",
        "height": "100%",
        "id": "btnExpandSortBy",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnIcon36pxWhite",
        "text": "D",
        "top": "10dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxSortBy.add(lblSortBY, btnExpandSortBy);
    var flxContainerSortOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContainerSortOptions",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerSortOptions.setDefaultUnit(kony.flex.DP);
    flxContainerSortOptions.add();
    var flxShopBy = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxShopBy",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxShopBy.setDefaultUnit(kony.flex.DP);
    var lblShopBy = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblShopBy",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold28pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.lblShopBy"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnExpandShopBy = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIcon36pxWhite",
        "height": "100%",
        "id": "btnExpandShopBy",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnIcon36pxWhite",
        "text": "U",
        "top": "10dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxShopBy.add(lblShopBy, btnExpandShopBy);
    var flxContainerShopOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContainerShopOptions",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerShopOptions.setDefaultUnit(kony.flex.DP);
    var flxProjects = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxProjects",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxProjects.setDefaultUnit(kony.flex.DP);
    var lblProjects = new kony.ui.RichText({
        "centerY": "50%",
        "height": "100%",
        "id": "lblProjects",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknRTGothamBold24pxDark",
        "text": "<u>Projects</u>\n",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxProjects.add(lblProjects);
    var flxGap = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGap",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGap.setDefaultUnit(kony.flex.DP);
    flxGap.add();
    var flxCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCategory.setDefaultUnit(kony.flex.DP);
    var lblCategory = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblCategory",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.categories"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTickCategory = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTickCategory",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblIcon34px333333",
        "text": "D",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCategory.add(lblCategory, lblTickCategory);
    var flxGap1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGap1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGap1.setDefaultUnit(kony.flex.DP);
    flxGap1.add();
    var flxContainerCategoryOptions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContainerCategoryOptions",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerCategoryOptions.setDefaultUnit(kony.flex.DP);
    var flxCategory1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxCategory1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxCategory1.setDefaultUnit(kony.flex.DP);
    var lblCategory1 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblCategory1",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblGothamBook24pxBlack",
        "text": "Arts and Supplies",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCategorySelected1 = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblCategorySelected1",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblIcon32pxRed",
        "text": "u",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCategory1.add(lblCategory1, lblCategorySelected1);
    var flxGapCategory1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGapCategory1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGapCategory1.setDefaultUnit(kony.flex.DP);
    flxGapCategory1.add();
    flxContainerCategoryOptions.add(flxCategory1, flxGapCategory1);
    var flxSale = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxSale",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSale.setDefaultUnit(kony.flex.DP);
    var lblSale = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSale",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.saleClearance"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTickSaleClearance = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTickSaleClearance",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknLblIcon34px333333",
        "text": "D",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSale.add(lblSale, lblTickSaleClearance);
    var flxGapSale3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGapSale3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGapSale3.setDefaultUnit(kony.flex.DP);
    flxGapSale3.add();
    var flxContainerSaleClearance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxContainerSaleClearance",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerSaleClearance.setDefaultUnit(kony.flex.DP);
    var flxSaleCont = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxSaleCont",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSaleCont.setDefaultUnit(kony.flex.DP);
    var lblSaleCont = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSaleCont",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblGothamBook24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.sale"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSaleSelected = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblSaleSelected",
        "isVisible": false,
        "right": "10dp",
        "skin": "sknLblIcon32pxRed",
        "text": "u",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSaleCont.add(lblSaleCont, lblSaleSelected);
    var flxGapSale1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGapSale1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGapSale1.setDefaultUnit(kony.flex.DP);
    flxGapSale1.add();
    var flxClearanceCont = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxClearanceCont",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxClearanceCont.setDefaultUnit(kony.flex.DP);
    var lblClearance = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblClearance",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblGothamBook24pxBlack",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmSortAndRefine.clearance"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblClearanceSelected = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblClearanceSelected",
        "isVisible": false,
        "right": "10dp",
        "skin": "sknLblIcon32pxRed",
        "text": "u",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxClearanceCont.add(lblClearance, lblClearanceSelected);
    var flxGapClearance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxGapClearance",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxGapClearance.setDefaultUnit(kony.flex.DP);
    flxGapClearance.add();
    flxContainerSaleClearance.add(flxSaleCont, flxGapSale1, flxClearanceCont, flxGapClearance);
    flxContainerShopOptions.add(flxProjects, flxGap, flxCategory, flxGap1, flxContainerCategoryOptions, flxSale, flxGapSale3, flxContainerSaleClearance);
    var flxRefineBy = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "42dp",
        "id": "flxRefineBy",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOverlay",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRefineBy.setDefaultUnit(kony.flex.DP);
    var lblRefine = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblRefine",
        "isVisible": true,
        "left": "10dp",
        "skin": "sknLblGothamBold28pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnRefineSearch"),
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnExpandRefine = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnIcon36pxWhite",
        "height": "100%",
        "id": "btnExpandRefine",
        "isVisible": true,
        "right": "10dp",
        "skin": "sknBtnIcon36pxWhite",
        "text": "U",
        "top": "10dp",
        "width": "94%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxRefineBy.add(lblRefine, btnExpandRefine);
    var flxRefineContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxRefineContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRefineContainer.setDefaultUnit(kony.flex.DP);
    flxRefineContainer.add();
    var flxEmptySpace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxEmptySpace",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEmptySpace.setDefaultUnit(kony.flex.DP);
    flxEmptySpace.add();
    flxScrlMain.add(flxSortBy, flxContainerSortOptions, flxShopBy, flxContainerShopOptions, flxRefineBy, flxRefineContainer, flxEmptySpace);
    var km8ed524b229849f1bb4f2a22fc438693 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km8ed524b229849f1bb4f2a22fc438693.setDefaultUnit(kony.flex.DP);
    var km808461f31584e98abbd9d6b33dc3ee5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km808461f31584e98abbd9d6b33dc3ee5.setDefaultUnit(kony.flex.DP);
    var km3c30f0ff3cb496c9d64037b95d9a117 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km90028b417844ca4bb615b992eec3371 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km808461f31584e98abbd9d6b33dc3ee5.add(km3c30f0ff3cb496c9d64037b95d9a117, km90028b417844ca4bb615b992eec3371);
    var km691c76703a04976be4603d8b9712a83 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km691c76703a04976be4603d8b9712a83.setDefaultUnit(kony.flex.DP);
    var kmef48502ee104ac49ceb3fce6deef7da = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km78d4855a29f4de78ee46310c4300e26 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmabdce0a893e41128983495a87683ee6 = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km691c76703a04976be4603d8b9712a83.add(kmef48502ee104ac49ceb3fce6deef7da, km78d4855a29f4de78ee46310c4300e26, kmabdce0a893e41128983495a87683ee6);
    var km17c6f9968ba4e4098e71d1e545b211b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km17c6f9968ba4e4098e71d1e545b211b.setDefaultUnit(kony.flex.DP);
    var km4a2c237bfcd4257b3eb824a3ecc33ce = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmddc1d40a05940e49356ac6173763c3d = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km17c6f9968ba4e4098e71d1e545b211b.add(km4a2c237bfcd4257b3eb824a3ecc33ce, kmddc1d40a05940e49356ac6173763c3d);
    var km8676ef012f84e74ae2af4e7efac7171 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8676ef012f84e74ae2af4e7efac7171.setDefaultUnit(kony.flex.DP);
    var kme50c3f2f5bc4d518d0a5b103dbb77ea = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km052419c963241899d250c84b67e54c3 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km8676ef012f84e74ae2af4e7efac7171.add(kme50c3f2f5bc4d518d0a5b103dbb77ea, km052419c963241899d250c84b67e54c3);
    var km295979d65774bc7a2a7e46fb4a89da8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km295979d65774bc7a2a7e46fb4a89da8.setDefaultUnit(kony.flex.DP);
    var km98834bc0c0448928512f4c1d51b6773 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km9d08422fb95443983290732598e1d64 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km295979d65774bc7a2a7e46fb4a89da8.add(km98834bc0c0448928512f4c1d51b6773, km9d08422fb95443983290732598e1d64);
    var km3831aa14f304c83a2dbc44769933ba7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3831aa14f304c83a2dbc44769933ba7.setDefaultUnit(kony.flex.DP);
    var km4cb06ee7f364cec8beced2cf375f7bb = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmb880a156a8f44d0853539d116c0a3e6 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km3831aa14f304c83a2dbc44769933ba7.add(km4cb06ee7f364cec8beced2cf375f7bb, kmb880a156a8f44d0853539d116c0a3e6);
    var kmd96f61dac894d0facd7327c4ec64a5c = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd96f61dac894d0facd7327c4ec64a5c.setDefaultUnit(kony.flex.DP);
    var kmff9b703700d421189a473ff38898fd7 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km163d524f936426395af29d40c8cf943 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmd96f61dac894d0facd7327c4ec64a5c.add(kmff9b703700d421189a473ff38898fd7, km163d524f936426395af29d40c8cf943);
    var km3411cf058af4e6e9486db8f4017cc3d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3411cf058af4e6e9486db8f4017cc3d.setDefaultUnit(kony.flex.DP);
    var km5397694e01e472ca9743162aa4972d6 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km41ef24c96ef4f339da568275c72db2e = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km3411cf058af4e6e9486db8f4017cc3d.add(km5397694e01e472ca9743162aa4972d6, km41ef24c96ef4f339da568275c72db2e);
    km8ed524b229849f1bb4f2a22fc438693.add(km808461f31584e98abbd9d6b33dc3ee5, km691c76703a04976be4603d8b9712a83, km17c6f9968ba4e4098e71d1e545b211b, km8676ef012f84e74ae2af4e7efac7171, km295979d65774bc7a2a7e46fb4a89da8, km3831aa14f304c83a2dbc44769933ba7, kmd96f61dac894d0facd7327c4ec64a5c, km3411cf058af4e6e9486db8f4017cc3d);
    frmSortAndRefine.add(flxHeader, flxClearAll, flxScrlMain, km8ed524b229849f1bb4f2a22fc438693);
};

function frmSortAndRefineGlobals() {
    frmSortAndRefine = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSortAndRefine,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmSortAndRefine",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};