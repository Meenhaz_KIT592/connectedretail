function initializetempWeeklyAdHome() {
    flxWeeklyAdHome = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxWeeklyAdHome",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknBgf7f7f7"
    }, {}, {});
    flxWeeklyAdHome.setDefaultUnit(kony.flex.DP);
    var lblCategoryName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCategoryName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular32pxDarkGray",
        "text": "Label",
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCategoryCount = new kony.ui.Label({
        "height": "100%",
        "id": "lblCategoryCount",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblIcon32pxLIghtGray",
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [1, 0, 5, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAisle = new kony.ui.Label({
        "centerY": "33%",
        "id": "lblAisle",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblGothamRegular28pxBlue",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblNumber = new kony.ui.Label({
        "centerY": "67%",
        "id": "lblNumber",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLblGothamMedium20pxBlue",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxWhiteLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "2dp",
        "id": "flxWhiteLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgWhite",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWhiteLine.setDefaultUnit(kony.flex.DP);
    flxWhiteLine.add();
    flxWeeklyAdHome.add(lblCategoryName, lblCategoryCount, lblAisle, lblNumber, flxWhiteLine);
}