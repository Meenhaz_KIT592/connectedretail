function addWidgetsfrmCreateAccount() {
    frmCreateAccount.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var flxFirstName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxFirstName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxFirstName.setDefaultUnit(kony.flex.DP);
    var txtFirstName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtFirstName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtFirstName"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxFirstName.add(txtFirstName);
    var flxLastName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxLastName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxLastName.setDefaultUnit(kony.flex.DP);
    var txtLastName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtLastName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtLastName"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxLastName.add(txtLastName);
    var flxEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxEmail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxEmail.setDefaultUnit(kony.flex.DP);
    var txtEmail = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "49.97%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtEmail",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxEmail.add(txtEmail);
    var flxConfirmEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxConfirmEmail",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxConfirmEmail.setDefaultUnit(kony.flex.DP);
    var txtConfrimEmail = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtConfrimEmail",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtConfrimEmail"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxConfirmEmail.add(txtConfrimEmail);
    var lblPasswordHint = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPasswordHint",
        "isVisible": true,
        "left": "108dp",
        "skin": "sknlblgothambook85",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccountStep2.signInInfo"),
        "textStyle": {
            "lineSpacing": 2,
            "letterSpacing": 0,
            "strikeThrough": false,
            "baseline": 0
        },
        "top": "15dp",
        "width": "200dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxPassword = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxPassword",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxPassword.setDefaultUnit(kony.flex.DP);
    var txtPassword = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtPassword",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtPassword"),
        "secureTextEntry": true,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxPassword.add(txtPassword);
    var flxConfirmPassword = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxConfirmPassword",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxConfirmPassword.setDefaultUnit(kony.flex.DP);
    var txtConfirmPassword = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtConfirmPassword",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.txtConfirmPassword"),
        "secureTextEntry": true,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxConfirmPassword.add(txtConfirmPassword);
    var flxZipCode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxZipCode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxZipCode.setDefaultUnit(kony.flex.DP);
    var txtZipCode = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtZipCode",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "maxTextLength": 5,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.txtZipCode"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxZipCode.add(txtZipCode);
    var flxZipCodeCan = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "37dp",
        "id": "flxZipCodeCan",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxZipCodeCan.setDefaultUnit(kony.flex.DP);
    var txtZipCodeCan = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtZipCodeCan",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": 6,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.txtZipCode"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxZipCodeCan.add(txtZipCodeCan);
    var flxPhoneNo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "37dp",
        "id": "flxPhoneNo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flxPhoneNo.setDefaultUnit(kony.flex.DP);
    var txtPhoneNo = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerX": "50%",
        "focusSkin": "sknTextbox",
        "height": "37dp",
        "id": "txtPhoneNo",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "maxTextLength": 10,
        "placeholder": kony.i18n.getLocalizedString("i18n.phone.common.txtPhoneNo"),
        "secureTextEntry": false,
        "skin": "sknTextbox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "sknTextbox",
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxPhoneNo.add(txtPhoneNo);
    var FlexContainer08545d32dca7842 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35dp",
        "id": "FlexContainer08545d32dca7842",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "20dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    FlexContainer08545d32dca7842.setDefaultUnit(kony.flex.DP);
    var rtTermsandConditions = new kony.ui.RichText({
        "height": "100%",
        "id": "rtTermsandConditions",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.termsCondition"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheckAgree = new kony.ui.Image2({
        "centerY": "50%",
        "height": "14dp",
        "id": "imgCheckAgree",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAgree = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "btnAgree",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.termsCondition"),
        "top": "0dp",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlexContainer08545d32dca7842.add(rtTermsandConditions, imgCheckAgree, btnAgree);
    var CopyFlexContainer008a1f175435b45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35dp",
        "id": "CopyFlexContainer008a1f175435b45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer008a1f175435b45.setDefaultUnit(kony.flex.DP);
    var rtAge = new kony.ui.RichText({
        "height": "100%",
        "id": "rtAge",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.rtAge"),
        "top": "1dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheckAge = new kony.ui.Image2({
        "centerY": "50%",
        "height": "14dp",
        "id": "imgCheckAge",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAge = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "btnAge",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    CopyFlexContainer008a1f175435b45.add(rtAge, imgCheckAge, btnAge);
    var CopyFlexContainer062a32526985248 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer062a32526985248",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer062a32526985248.setDefaultUnit(kony.flex.DP);
    var rtPrivacyPolicy = new kony.ui.RichText({
        "id": "rtPrivacyPolicy",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmMyRewards.rtPrivacyPolicy"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheck = new kony.ui.Image2({
        "height": "14dp",
        "id": "imgCheck",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "top": "0dp",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPrivacyPolicy = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "20dp",
        "id": "btnPrivacyPolicy",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    CopyFlexContainer062a32526985248.add(rtPrivacyPolicy, imgCheck, btnPrivacyPolicy);
    var btnNext = new kony.ui.Button({
        "bottom": "63dp",
        "centerX": "50%",
        "focusSkin": "sknBtnRedGothamBook",
        "height": "40dp",
        "id": "btnNext",
        "isVisible": true,
        "skin": "sknBtnRedGothamBook",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.lblCreateAcc"),
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "64dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    flxMainContainer.add(flxTopSpacerDND, flxFirstName, flxLastName, flxEmail, flxConfirmEmail, lblPasswordHint, flxPassword, flxConfirmPassword, flxZipCode, flxZipCodeCan, flxPhoneNo, FlexContainer08545d32dca7842, CopyFlexContainer008a1f175435b45, CopyFlexContainer062a32526985248, btnNext, flxBottomSpacerDND);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgD30e2c",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "53%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.lblCreateAcc"),
        "textStyle": {},
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.btnCancel"),
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, btnHeaderLeft);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialogbox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxDialogbox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxDialogbox.setDefaultUnit(kony.flex.DP);
    var lbl1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl1",
        "isVisible": true,
        "skin": "sknLblSFNSBold",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.accountCreated"),
        "textStyle": {},
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbl2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSFNSRegular",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.profileSetup"),
        "textStyle": {},
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "40dp",
        "id": "btnOk",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.ok"),
        "top": "10dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    var btnYes = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmCreateAccount.okay"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine2.setDefaultUnit(kony.flex.DP);
    flxLine2.add();
    var btnRewardProgramInfo = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnRewardProgramInfo",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.rewardProgramInfo"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine3.setDefaultUnit(kony.flex.DP);
    flxLine3.add();
    var btnNotRightNow = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnNotRightNow",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.notRightNow"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxDialogbox.add(lbl1, lbl2, btnOk, flxLine, btnYes, flxLine2, btnRewardProgramInfo, flxLine3, btnNotRightNow);
    flxOverlay.add(flxDialogbox);
    var km32d49962e3a4edebc3bcd2d1d84d92b = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    km32d49962e3a4edebc3bcd2d1d84d92b.setDefaultUnit(kony.flex.DP);
    var km3087ac32a21400fb1ae68c13917c482 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km3087ac32a21400fb1ae68c13917c482.setDefaultUnit(kony.flex.DP);
    var km49f49508e25497987ebfd804c436e9c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km4363b09ba3241de88bff7ca33bc1a72 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km3087ac32a21400fb1ae68c13917c482.add(km49f49508e25497987ebfd804c436e9c, km4363b09ba3241de88bff7ca33bc1a72);
    var km2821842e5ce48c48a8a3dc764dc349e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2821842e5ce48c48a8a3dc764dc349e.setDefaultUnit(kony.flex.DP);
    var km0d85d5de30544b49bcff76ae5d47f5f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km527202fed54490184e208a6980b6f2e = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var kmc74acd9a7444443a4f7373892561bed = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    km2821842e5ce48c48a8a3dc764dc349e.add(km0d85d5de30544b49bcff76ae5d47f5f, km527202fed54490184e208a6980b6f2e, kmc74acd9a7444443a4f7373892561bed);
    var km8beef7afe744d34b864656bfb2dca7e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8beef7afe744d34b864656bfb2dca7e.setDefaultUnit(kony.flex.DP);
    var kmaf2f10c1b9849d19e7a2e1ecc4d6a0c = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km994004bed364e099a38f171f8e7eb73 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km8beef7afe744d34b864656bfb2dca7e.add(kmaf2f10c1b9849d19e7a2e1ecc4d6a0c, km994004bed364e099a38f171f8e7eb73);
    var kmbbde209a9e847c1ab54fe4ed6a15af5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmbbde209a9e847c1ab54fe4ed6a15af5.setDefaultUnit(kony.flex.DP);
    var km85bd1f3fd0e458889e793f6c70a1919 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kme537dd838d5475fb1ff88915c478bf0 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmbbde209a9e847c1ab54fe4ed6a15af5.add(km85bd1f3fd0e458889e793f6c70a1919, kme537dd838d5475fb1ff88915c478bf0);
    var km6412da6e9354df6b7318e55375b4f1e = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km6412da6e9354df6b7318e55375b4f1e.setDefaultUnit(kony.flex.DP);
    var km8875c5e71c3407b84189a9ac7da6d47 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km700ff5df21949f88322035b88f0c156 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km6412da6e9354df6b7318e55375b4f1e.add(km8875c5e71c3407b84189a9ac7da6d47, km700ff5df21949f88322035b88f0c156);
    var kme69b7eecc234fcaa6a09373764a96b0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kme69b7eecc234fcaa6a09373764a96b0.setDefaultUnit(kony.flex.DP);
    var km15093b5e9b14e2fbe0ebb75ef8822a1 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmfee1914e8664f818dfd1de4d77773c9 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kme69b7eecc234fcaa6a09373764a96b0.add(km15093b5e9b14e2fbe0ebb75ef8822a1, kmfee1914e8664f818dfd1de4d77773c9);
    var km6a56d5883a2445ea401d0a3013d229b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km6a56d5883a2445ea401d0a3013d229b.setDefaultUnit(kony.flex.DP);
    var kmd79a36a97be449984eedcd01b5a6736 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kme63f741d2f54df0a4b4bac39b0c06ff = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km6a56d5883a2445ea401d0a3013d229b.add(kmd79a36a97be449984eedcd01b5a6736, kme63f741d2f54df0a4b4bac39b0c06ff);
    var kma4ddf6f0da3498e97d4ecdfafccc4f0 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kma4ddf6f0da3498e97d4ecdfafccc4f0.setDefaultUnit(kony.flex.DP);
    var km69547a31bd74af3a8c7ab1e80559cee = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km80e33828df04046a67b421fdb4d670f = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kma4ddf6f0da3498e97d4ecdfafccc4f0.add(km69547a31bd74af3a8c7ab1e80559cee, km80e33828df04046a67b421fdb4d670f);
    km32d49962e3a4edebc3bcd2d1d84d92b.add(km3087ac32a21400fb1ae68c13917c482, km2821842e5ce48c48a8a3dc764dc349e, km8beef7afe744d34b864656bfb2dca7e, kmbbde209a9e847c1ab54fe4ed6a15af5, km6412da6e9354df6b7318e55375b4f1e, kme69b7eecc234fcaa6a09373764a96b0, km6a56d5883a2445ea401d0a3013d229b, kma4ddf6f0da3498e97d4ecdfafccc4f0);
    frmCreateAccount.add(flxMainContainer, flxHeaderTitleContainer, flxOverlay, km32d49962e3a4edebc3bcd2d1d84d92b);
};

function frmCreateAccountGlobals() {
    frmCreateAccount = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCreateAccount,
        "allowHorizontalBounce": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmCreateAccount",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};