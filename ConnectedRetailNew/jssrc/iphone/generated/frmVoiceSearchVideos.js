function addWidgetsfrmVoiceSearchVideos() {
    frmVoiceSearchVideos.setDefaultUnit(kony.flex.DP);
    var flxYoutubeResults = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "92%",
        "id": "flxYoutubeResults",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgDarkGray",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxYoutubeResults.setDefaultUnit(kony.flex.DP);
    var browserYoutubeResults = new kony.ui.Browser({
        "detectTelNumber": true,
        "enableZoom": false,
        "height": "100.00%",
        "id": "browserYoutubeResults",
        "isVisible": true,
        "left": "0dp",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxYoutubeResults.add(browserYoutubeResults);
    var flxContainerClose = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxContainerClose",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "92%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxContainerClose.setDefaultUnit(kony.flex.DP);
    var btnClose = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "100%",
        "id": "btnClose",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnCross = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "sknBtnBgGrayWhiteFont14px",
        "height": "70%",
        "id": "btnCross",
        "isVisible": true,
        "skin": "sknBtnBgGrayWhiteFont14px",
        "text": kony.i18n.getLocalizedString("i18n.phone.frmPDP.close"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [2, 0, 2, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flexBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexBack",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flexBack.setDefaultUnit(kony.flex.DP);
    var imgBack = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_back.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexBack.add(imgBack);
    var flexForward = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flexForward",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "3%",
        "skin": "slFbox",
        "top": 0,
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flexForward.setDefaultUnit(kony.flex.DP);
    var imgForward = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "40%",
        "id": "imgForward",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "ic_forward.png",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flexForward.add(imgForward);
    flxContainerClose.add(btnClose, btnCross, flexBack, flexForward);
    frmVoiceSearchVideos.add(flxYoutubeResults, flxContainerClose);
};

function frmVoiceSearchVideosGlobals() {
    frmVoiceSearchVideos = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmVoiceSearchVideos,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmVoiceSearchVideos",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "bouncesZoom": true,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NEXTPREV,
        "needsIndicatorDuringPostShow": true,
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};