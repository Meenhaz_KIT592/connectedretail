function addWidgetsfrmReviewProfile() {
    frmReviewProfile.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknFlxSboxBgWhite",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxTopSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxTopSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTopSpacerDND.setDefaultUnit(kony.flex.DP);
    flxTopSpacerDND.add();
    var FlexContainer0b4738c7579e24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "FlexContainer0b4738c7579e24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    FlexContainer0b4738c7579e24b.setDefaultUnit(kony.flex.DP);
    var Label0c5601159c0bb4c = new kony.ui.Label({
        "height": "100%",
        "id": "Label0c5601159c0bb4c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxDark",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.personal"),
        "textStyle": {},
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnPersonal = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnPersonal",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.edit"),
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    FlexContainer0b4738c7579e24b.add(Label0c5601159c0bb4c, btnPersonal);
    var FlexContainer0c7681a8a5e9640 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "FlexContainer0c7681a8a5e9640",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "7dp",
        "width": "94%",
        "zIndex": 7
    }, {}, {});
    FlexContainer0c7681a8a5e9640.setDefaultUnit(kony.flex.DP);
    var lblName = new kony.ui.Label({
        "id": "lblName",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Gennie Williams",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblEmail = new kony.ui.Label({
        "id": "lblEmail",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "gwilliams1234@gmail.com",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDOB = new kony.ui.Label({
        "id": "lblDOB",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "September 8, 1986",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblPhone = new kony.ui.Label({
        "id": "lblPhone",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "972.123.4567",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexContainer0c7681a8a5e9640.add(lblName, lblEmail, lblDOB, lblPhone);
    var CopyFlexContainer05dc65fa01d7e42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFlexContainer05dc65fa01d7e42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer05dc65fa01d7e42.setDefaultUnit(kony.flex.DP);
    var CopyLabel086c5f3f8403d48 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel086c5f3f8403d48",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxDark",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.address"),
        "textStyle": {},
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnAddress = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnAddress",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.edit"),
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    CopyFlexContainer05dc65fa01d7e42.add(CopyLabel086c5f3f8403d48, btnAddress);
    var CopyFlexContainer0ab67a429e8c741 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer0ab67a429e8c741",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "7dp",
        "width": "94%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer0ab67a429e8c741.setDefaultUnit(kony.flex.DP);
    var lblAddress1 = new kony.ui.Label({
        "id": "lblAddress1",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "1235 Sycamore Valley Dr.",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAddress2 = new kony.ui.Label({
        "id": "lblAddress2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Apt 1202 ",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCity = new kony.ui.Label({
        "id": "lblCity",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Richardson,",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblStateZip = new kony.ui.Label({
        "id": "lblStateZip",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "TX 75080",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexContainer0ab67a429e8c741.add(lblAddress1, lblAddress2, lblCity, lblStateZip);
    var flxChild = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxChild",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxChild.setDefaultUnit(kony.flex.DP);
    var CopyLabel0574acc64f15448 = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0574acc64f15448",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.children"),
        "textStyle": {},
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnChildren = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnChildren",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.edit"),
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxChild.add(CopyLabel0574acc64f15448, btnChildren);
    var CopyFlexContainer0093217c7c1ee45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer0093217c7c1ee45",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "7dp",
        "width": "94%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer0093217c7c1ee45.setDefaultUnit(kony.flex.DP);
    var FlexGroup0b6f897d097434c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexGroup0b6f897d097434c",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "3dp",
        "width": "100%"
    }, {}, {});
    FlexGroup0b6f897d097434c.setDefaultUnit(kony.flex.DP);
    var CopyLabel03e225c3af4fd47 = new kony.ui.Label({
        "id": "CopyLabel03e225c3af4fd47",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Pre-School or Younge",
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel0eb9ea1e89dda49 = new kony.ui.Label({
        "id": "CopyLabel0eb9ea1e89dda49",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "1",
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlexGroup0b6f897d097434c.add(CopyLabel03e225c3af4fd47, CopyLabel0eb9ea1e89dda49);
    var CopyFlexGroup0f1309ee7be6a44 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexGroup0f1309ee7be6a44",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "3dp",
        "width": "100%"
    }, {}, {});
    CopyFlexGroup0f1309ee7be6a44.setDefaultUnit(kony.flex.DP);
    var CopyLabel0121d994844f140 = new kony.ui.Label({
        "id": "CopyLabel0121d994844f140",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Elementary",
        "top": "0dp",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel050fd03a6451240 = new kony.ui.Label({
        "id": "CopyLabel050fd03a6451240",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "2",
        "top": "0dp",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexGroup0f1309ee7be6a44.add(CopyLabel0121d994844f140, CopyLabel050fd03a6451240);
    var segData = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblReviewProfile": "Label",
            "lblReviewProfileNo": "Label"
        }, {
            "lblReviewProfile": "Label",
            "lblReviewProfileNo": "Label"
        }, {
            "lblReviewProfile": "Label",
            "lblReviewProfileNo": "Label"
        }],
        "groupCells": false,
        "id": "segData",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxReviweProfile,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxReviweProfile": "flxReviweProfile",
            "lblReviewProfile": "lblReviewProfile",
            "lblReviewProfileNo": "lblReviewProfileNo"
        },
        "width": "100%",
        "zIndex": 7
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    CopyFlexContainer0093217c7c1ee45.add(FlexGroup0b6f897d097434c, CopyFlexGroup0f1309ee7be6a44, segData);
    var flxInterests = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxInterests",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxInterests.setDefaultUnit(kony.flex.DP);
    var lblInterests = new kony.ui.Label({
        "height": "100%",
        "id": "lblInterests",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxDark",
        "text": "INTERESTS",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnInterestsEdit = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnInterestsEdit",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.edit"),
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxInterests.add(lblInterests, btnInterestsEdit);
    var flxInterestsvalues = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "flxInterestsvalues",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "7dp",
        "width": "94%",
        "zIndex": 7
    }, {}, {});
    flxInterestsvalues.setDefaultUnit(kony.flex.DP);
    var lblInterestsQuestion = new kony.ui.Label({
        "id": "lblInterestsQuestion",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIntrstSlctd = new kony.ui.Label({
        "id": "lblIntrstSlctd",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxInterestsvalues.add(lblInterestsQuestion, lblIntrstSlctd);
    var flxInterestsSkills = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxInterestsSkills",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxInterestsSkills.setDefaultUnit(kony.flex.DP);
    var CopyLabel0b7a9bd6b8c2d4b = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0b7a9bd6b8c2d4b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.interestskills"),
        "textStyle": {},
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnSkills = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnSkills",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.edit"),
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxInterestsSkills.add(CopyLabel0b7a9bd6b8c2d4b, btnSkills);
    var CopyFlexContainer087d3b7dd178c44 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer087d3b7dd178c44",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "7dp",
        "width": "94%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer087d3b7dd178c44.setDefaultUnit(kony.flex.DP);
    var flxQuestions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxQuestions",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "3dp",
        "width": "100%"
    }, {}, {});
    flxQuestions.setDefaultUnit(kony.flex.DP);
    var lblQuestion = new kony.ui.Label({
        "id": "lblQuestion",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Painting & Drawing",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAnswer = new kony.ui.Label({
        "id": "lblAnswer",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "ADVANCED",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxQuestions.add(lblQuestion, lblAnswer);
    var CopyFlexGroup0e6cf88f433694a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexGroup0e6cf88f433694a",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "3dp",
        "width": "100%"
    }, {}, {});
    CopyFlexGroup0e6cf88f433694a.setDefaultUnit(kony.flex.DP);
    var CopyLabel03cecb6ca32cb49 = new kony.ui.Label({
        "id": "CopyLabel03cecb6ca32cb49",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Jewelry",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel04e3085704e3d43 = new kony.ui.Label({
        "id": "CopyLabel04e3085704e3d43",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "INTERMEDIAT",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexGroup0e6cf88f433694a.add(CopyLabel03cecb6ca32cb49, CopyLabel04e3085704e3d43);
    var CopyFlexGroup0616df8ebae0546 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyFlexGroup0616df8ebae0546",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "3dp",
        "width": "100%"
    }, {}, {});
    CopyFlexGroup0616df8ebae0546.setDefaultUnit(kony.flex.DP);
    var CopyLabel00a7173c675eb46 = new kony.ui.Label({
        "id": "CopyLabel00a7173c675eb46",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Baking Crafts",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel025483c8760f248 = new kony.ui.Label({
        "id": "CopyLabel025483c8760f248",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "BEGINNER",
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexGroup0616df8ebae0546.add(CopyLabel00a7173c675eb46, CopyLabel025483c8760f248);
    var segSkills = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "lblReviewProfile": "Label",
            "lblReviewProfileNo": "Label"
        }, {
            "lblReviewProfile": "Label",
            "lblReviewProfileNo": "Label"
        }, {
            "lblReviewProfile": "Label",
            "lblReviewProfileNo": "Label"
        }],
        "groupCells": false,
        "id": "segSkills",
        "isVisible": true,
        "left": "2dp",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "seg2Normal",
        "rowTemplate": flxReviweProfile,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "5dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxReviweProfile": "flxReviweProfile",
            "lblReviewProfile": "lblReviewProfile",
            "lblReviewProfileNo": "lblReviewProfileNo"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    CopyFlexContainer087d3b7dd178c44.add(flxQuestions, CopyFlexGroup0e6cf88f433694a, CopyFlexGroup0616df8ebae0546, segSkills);
    var CopyFlexContainer00066b8ed64c042 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFlexContainer00066b8ed64c042",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer00066b8ed64c042.setDefaultUnit(kony.flex.DP);
    var CopyLabel0fe77691d1a3b4a = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0fe77691d1a3b4a",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamBold24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.preferredstore"),
        "textStyle": {},
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnStore = new kony.ui.Button({
        "focusSkin": "sknBtnBgTransReg24pxDarkFoc",
        "height": "100%",
        "id": "btnStore",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknBtnBgTransReg24pxDark",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.edit"),
        "top": "0dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 3, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    CopyFlexContainer00066b8ed64c042.add(CopyLabel0fe77691d1a3b4a, btnStore);
    var CopyFlexContainer0cec9ffcf8af54e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "clipBounds": true,
        "id": "CopyFlexContainer0cec9ffcf8af54e",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "7dp",
        "width": "94%",
        "zIndex": 7
    }, {}, {});
    CopyFlexContainer0cec9ffcf8af54e.setDefaultUnit(kony.flex.DP);
    var CopyLabel055a9a5cadcf54f = new kony.ui.Label({
        "id": "CopyLabel055a9a5cadcf54f",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "5301 Belt Line Rd. Ste 101",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopyLabel015c44ef24d2d48 = new kony.ui.Label({
        "id": "CopyLabel015c44ef24d2d48",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblGothamRegular24px515151",
        "text": "Dallas, TX, 75247",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexContainer0cec9ffcf8af54e.add(CopyLabel055a9a5cadcf54f, CopyLabel015c44ef24d2d48);
    var flexLastLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flexLastLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flexLastLine.setDefaultUnit(kony.flex.DP);
    flexLastLine.add();
    var flexTermsAndConditions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "35dp",
        "id": "flexTermsAndConditions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {}, {});
    flexTermsAndConditions.setDefaultUnit(kony.flex.DP);
    var rtxtAgreeConditions = new kony.ui.RichText({
        "height": "100%",
        "id": "rtxtAgreeConditions",
        "isVisible": true,
        "left": "35dp",
        "linkSkin": "sknRTGothamRegular20pxLink",
        "right": "0dp",
        "skin": "sknRTGothamRegular20pxDark",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.termsAndConditions"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgCheckAgree = new kony.ui.Image2({
        "centerY": "50%",
        "height": "14dp",
        "id": "imgCheckAgree",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "checkbox_off.png",
        "width": "14dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnAgree = new kony.ui.Button({
        "focusSkin": "sknBtnTransNoText",
        "height": "100%",
        "id": "btnAgree",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransNoText",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flexTermsAndConditions.add(rtxtAgreeConditions, imgCheckAgree, btnAgree);
    var btnCompleteProfile = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnRedFoc",
        "height": "40dp",
        "id": "btnCompleteProfile",
        "isVisible": true,
        "skin": "sknBtnRed",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.completeProfile"),
        "top": "10dp",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var flxBottomSpacerDND = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxBottomSpacerDND",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxBottomSpacerDND.setDefaultUnit(kony.flex.DP);
    flxBottomSpacerDND.add();
    var CopyflxBottomSpacerDND01be48f30dea648 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "64dp",
        "id": "CopyflxBottomSpacerDND01be48f30dea648",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxBottomSpacerDND01be48f30dea648.setDefaultUnit(kony.flex.DP);
    CopyflxBottomSpacerDND01be48f30dea648.add();
    flxMainContainer.add(flxTopSpacerDND, FlexContainer0b4738c7579e24b, FlexContainer0c7681a8a5e9640, CopyFlexContainer05dc65fa01d7e42, CopyFlexContainer0ab67a429e8c741, flxChild, CopyFlexContainer0093217c7c1ee45, flxInterests, flxInterestsvalues, flxInterestsSkills, CopyFlexContainer087d3b7dd178c44, CopyFlexContainer00066b8ed64c042, CopyFlexContainer0cec9ffcf8af54e, flexLastLine, flexTermsAndConditions, btnCompleteProfile, flxBottomSpacerDND, CopyflxBottomSpacerDND01be48f30dea648);
    var flxHeaderTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxHeaderTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgRed",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxHeaderTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblFormTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblFormTitle",
        "isVisible": true,
        "skin": "sknLblGothamBold32pxWhite",
        "text": kony.i18n.getLocalizedString("i18.phone.rewards.reviewprofile"),
        "textStyle": {},
        "top": "0dp",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblChevron = new kony.ui.Label({
        "height": "100%",
        "id": "lblChevron",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblIcon32pxWhite",
        "text": "L",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnHeaderLeft = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransMed16pxWhiteFoc",
        "height": "100%",
        "id": "btnHeaderLeft",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnTransMed16pxWhite",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": false,
        "padding": [9, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnTransReg16pxWhite",
        "height": "100%",
        "id": "btnClose",
        "isVisible": false,
        "right": "10dp",
        "skin": "sknBtnTransReg16pxWhite",
        "text": kony.i18n.getLocalizedString("i18.review.close"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    flxHeaderTitleContainer.add(lblFormTitle, lblChevron, btnHeaderLeft, btnClose);
    var flxOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgLtGrayOp50",
        "top": "0dp",
        "width": "100%",
        "zIndex": 7
    }, {}, {});
    flxOverlay.setDefaultUnit(kony.flex.DP);
    var flxDialogbox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxDialogbox",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknFlexBgWhiteRadius30",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxDialogbox.setDefaultUnit(kony.flex.DP);
    var lbl1 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl1",
        "isVisible": true,
        "skin": "sknLblSFNSBold",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.confirmationAlert"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lbl2 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lbl2",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblSFNSRegular",
        "text": kony.i18n.getLocalizedString("i18n.rewardsProfile.confirmMessage"),
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnTransMed32pxBlue",
        "height": "40dp",
        "id": "btnOk",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknBtnTransMed32pxBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.signIn.ok"),
        "top": "10dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var flxLine = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "flxLine",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknFlexBgBlackOpaque20",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLine.setDefaultUnit(kony.flex.DP);
    flxLine.add();
    var btnYes = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknBtnSFNSRegularBlue",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBtnSFNSRegularBlue",
        "text": kony.i18n.getLocalizedString("i18n.phone.common.okayButton"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxDialogbox.add(lbl1, lbl2, btnOk, flxLine, btnYes);
    flxOverlay.add(flxDialogbox);
    var kmff7cf7fd3c144b38f0023b4e4b9dc69 = new kony.ui.FlexContainer({
        "bottom": "0dp",
        "clipBounds": true,
        "isMaster": true,
        "height": "9%",
        "id": "flxFooterWrap",
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "width": "100%",
        "zIndex": 5,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "CopysknFlexBgD"
    }, {}, {});
    kmff7cf7fd3c144b38f0023b4e4b9dc69.setDefaultUnit(kony.flex.DP);
    var kmcddd8a82b184b4db210b7b9cccc9c7b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProducts",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmcddd8a82b184b4db210b7b9cccc9c7b.setDefaultUnit(kony.flex.DP);
    var kmb7d80e86981496884e43f7e55910ef9 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProducts",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProducts"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmb546039af11473fa394a6b77a0f9cab = new kony.ui.Button({
        "height": "100%",
        "id": "btnProducts",
        "left": "0dp",
        "text": "O",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmcddd8a82b184b4db210b7b9cccc9c7b.add(kmb7d80e86981496884e43f7e55910ef9, kmb546039af11473fa394a6b77a0f9cab);
    var kmd205cf7da2844ecbaf74e7ba274d18b = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyLists",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": false,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmd205cf7da2844ecbaf74e7ba274d18b.setDefaultUnit(kony.flex.DP);
    var kma0a9e9feb8d4759b31fde256469c41f = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMyLists",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMyLists"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var kmf620419bdb64c3293840aca7d05dcfe = new kony.ui.Button({
        "height": "100%",
        "id": "btnMyLists",
        "left": "0dp",
        "text": "I",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "sknBtnCouponBox"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    var km716e23c5a0942b394741274a988967b = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgCouponIcon",
        "right": "25.67%",
        "src": "coupon_icon.png",
        "width": "36dp",
        "zIndex": 1,
        "isVisible": true,
        "skin": "slImage"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    kmd205cf7da2844ecbaf74e7ba274d18b.add(kma0a9e9feb8d4759b31fde256469c41f, kmf620419bdb64c3293840aca7d05dcfe, km716e23c5a0942b394741274a988967b);
    var kmb3898129f85480db9c6024a3230c5a5 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMyCoupons",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmb3898129f85480db9c6024a3230c5a5.setDefaultUnit(kony.flex.DP);
    var kme61d2173d3340de934414dc63869b29 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblCoupons",
        "text": "Coupons",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km423add31c2347e19d1c7ec8227f1925 = new kony.ui.Button({
        "height": "100%",
        "id": "btnCoupon",
        "left": "0dp",
        "text": "C",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmb3898129f85480db9c6024a3230c5a5.add(kme61d2173d3340de934414dc63869b29, km423add31c2347e19d1c7ec8227f1925);
    var km7c8ca5e260647e987277e6054ff0ed7 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxWeeklyAd",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km7c8ca5e260647e987277e6054ff0ed7.setDefaultUnit(kony.flex.DP);
    var kmff996c66b2e4ef0892758c2512f67b0 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblWeeklyAd",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblWeeklyAd"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km5311b4cffbd480c8c2c98a0d7d9eb80 = new kony.ui.Button({
        "height": "100%",
        "id": "btnWeeklyAd",
        "left": "0dp",
        "text": "W",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km7c8ca5e260647e987277e6054ff0ed7.add(kmff996c66b2e4ef0892758c2512f67b0, km5311b4cffbd480c8c2c98a0d7d9eb80);
    var km2a836685d1d4c3eb68e98be137fa724 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxProjects",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2a836685d1d4c3eb68e98be137fa724.setDefaultUnit(kony.flex.DP);
    var kmf84ca8d0be14bc9acf96b4d935a1f66 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblProjects",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblProjects"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km48900bfe04c4fb4b82f4862d0fa2081 = new kony.ui.Button({
        "height": "100%",
        "id": "btnProjects",
        "left": "0dp",
        "text": "J",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km2a836685d1d4c3eb68e98be137fa724.add(kmf84ca8d0be14bc9acf96b4d935a1f66, km48900bfe04c4fb4b82f4862d0fa2081);
    var km8cc4c9d1e9f46ad8fe93c39a95cbcd8 = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxMore",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km8cc4c9d1e9f46ad8fe93c39a95cbcd8.setDefaultUnit(kony.flex.DP);
    var km91a8d6fc6ed4300bcca298e2d8b5dda = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblMore",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblMore"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km0710e81d9e94d88a218117e2d11ded0 = new kony.ui.Button({
        "height": "100%",
        "id": "btnMore",
        "left": "0dp",
        "text": "G",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km8cc4c9d1e9f46ad8fe93c39a95cbcd8.add(km91a8d6fc6ed4300bcca298e2d8b5dda, km0710e81d9e94d88a218117e2d11ded0);
    var km2b44b2efc8f4f1ebe2d0e5a6ef3ebcf = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxEvents",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    km2b44b2efc8f4f1ebe2d0e5a6ef3ebcf.setDefaultUnit(kony.flex.DP);
    var km442bfac22cf4bff8fc5cb0277f75618 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblEvents",
        "text": kony.i18n.getLocalizedString("i18n.phone.masterFooter.lblEvents"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km16878b2ea174931a875fc1441742af3 = new kony.ui.Button({
        "height": "100%",
        "id": "btnEvents",
        "left": "0dp",
        "text": "E",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    km2b44b2efc8f4f1ebe2d0e5a6ef3ebcf.add(km442bfac22cf4bff8fc5cb0277f75618, km16878b2ea174931a875fc1441742af3);
    var kmaf1800ae63b4fa2b8e390e48961a50d = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "100%",
        "id": "flxStoreMap",
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1,
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "isVisible": true,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    kmaf1800ae63b4fa2b8e390e48961a50d.setDefaultUnit(kony.flex.DP);
    var km653588a162449fd80233bb2ffedc789 = new kony.ui.Label({
        "bottom": "5dp",
        "centerX": "50%",
        "id": "lblStoreMap",
        "text": kony.i18n.getLocalizedString("i18n.phone.Common.btnStoreMap"),
        "textStyle": {},
        "width": "100%",
        "zIndex": 1,
        "isVisible": true,
        "skin": "CopysknLblGothamMedium"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var km43c22710c534324ab9cc83919feee1c = new kony.ui.Button({
        "height": "100%",
        "id": "btnStoreMap",
        "left": "0dp",
        "text": "N",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1,
        "focusSkin": "CopysknBtnIconFooterFocus",
        "isVisible": true,
        "skin": "CopysknBtnIconFooter"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 20],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    kmaf1800ae63b4fa2b8e390e48961a50d.add(km653588a162449fd80233bb2ffedc789, km43c22710c534324ab9cc83919feee1c);
    kmff7cf7fd3c144b38f0023b4e4b9dc69.add(kmcddd8a82b184b4db210b7b9cccc9c7b, kmd205cf7da2844ecbaf74e7ba274d18b, kmb3898129f85480db9c6024a3230c5a5, km7c8ca5e260647e987277e6054ff0ed7, km2a836685d1d4c3eb68e98be137fa724, km8cc4c9d1e9f46ad8fe93c39a95cbcd8, km2b44b2efc8f4f1ebe2d0e5a6ef3ebcf, kmaf1800ae63b4fa2b8e390e48961a50d);
    frmReviewProfile.add(flxMainContainer, flxHeaderTitleContainer, flxOverlay, kmff7cf7fd3c144b38f0023b4e4b9dc69);
};

function frmReviewProfileGlobals() {
    frmReviewProfile = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmReviewProfile,
        "enabledForIdleTimeout": true,
        "id": "frmReviewProfile",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknFormRed"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "transitionFade"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionDuration": 0.3,
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};