function initializetempMenuList() {
    flexMenuListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flexMenuListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlexBgLtGray"
    }, {}, {});
    flexMenuListContainer.setDefaultUnit(kony.flex.DP);
    var lblIconMenuList = new kony.ui.Label({
        "centerY": "50%",
        "height": "45dp",
        "id": "lblIconMenuList",
        "isVisible": true,
        "left": "4%",
        "skin": "sknLblIconMoreMenu",
        "text": "A",
        "textStyle": {},
        "width": "35dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMenuListItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMenuListItem",
        "isVisible": true,
        "left": "60dp",
        "skin": "sknLblMontserratSemiBold33333316px",
        "text": "Label",
        "textStyle": {},
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIconMenuArrow = new kony.ui.Label({
        "centerY": "50%",
        "height": "25dp",
        "id": "lblIconMenuArrow",
        "isVisible": true,
        "right": "4%",
        "skin": "sknLblIconMoreMenu66666616px",
        "text": "K",
        "textStyle": {},
        "width": "20dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flexMenuListContainer.add(lblIconMenuList, lblMenuListItem, lblIconMenuArrow);
}