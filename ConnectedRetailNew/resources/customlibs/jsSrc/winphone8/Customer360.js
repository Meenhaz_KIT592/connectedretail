var Customer360={};
Customer360.OneManager=function(){}
Customer360.OneManager.prototype.sendInteraction= function(interactionPath,interactionProperties,interactionCallback){};
Customer360.OneManager.prototype.sendBaseTouchPoints= function(properties){};
Customer360.OneManager.prototype.sendPushToken= function(pushToken){};
Customer360.OneManager.prototype.getTID= function(){};
Customer360.OneManager.prototype.sfmcUpdateCustomAttributes= function(sfmcCustomObject){};
Customer360.OneManager.prototype.sfmcTogglePushNotifications= function(pushToggle){};

Customer360.OneManagerAndroid=function(){}
Customer360.OneManagerAndroid.prototype.sendInteraction= function(interactionPath,interactionProperties,interactionCB){};
Customer360.OneManagerAndroid.prototype.sendBaseTouchPoints= function(properties,successCallback){};
Customer360.OneManagerAndroid.prototype.enableAndSendPushToken= function(senderId,pushToken){};
Customer360.OneManagerAndroid.prototype.getTID= function(){};
Customer360.OneManagerAndroid.prototype.enableMarketingCloudSdk= function(){};
Customer360.OneManagerAndroid.prototype.sendAttributes= function(text){};
Customer360.OneManagerAndroid.prototype.disableNotifications= function(){};
Customer360.OneManagerAndroid.prototype.enableNotifications= function(){};
Customer360.OneManagerAndroid.prototype.injectCallback= function(callback){};
Customer360.OneManagerAndroid.prototype.setSFMCPushToken= function(pushToken){};

