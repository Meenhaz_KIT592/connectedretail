package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.analytics.customerone.OneSDKManager;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_Customer360 extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new OneManagerAndroid();
 return libs;
 }



	public N_Customer360(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "Customer360";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class OneManagerAndroid extends JSLibrary {

 
 
	public static final String sendInteraction = "sendInteraction";
 
 
	public static final String sendBaseTouchPoints = "sendBaseTouchPoints";
 
 
	public static final String enableAndSendPushToken = "enableAndSendPushToken";
 
 
	public static final String getTID = "getTID";
 
 
	public static final String enableMarketingCloudSdk = "enableMarketingCloudSdk";
 
 
	public static final String sendAttributes = "sendAttributes";
 
 
	public static final String disableNotifications = "disableNotifications";
 
 
	public static final String enableNotifications = "enableNotifications";
 
 
	public static final String injectCallback = "injectCallback";
 
 
	public static final String setSFMCPushToken = "setSFMCPushToken";
 
	String[] methods = { sendInteraction, sendBaseTouchPoints, enableAndSendPushToken, getTID, enableMarketingCloudSdk, sendAttributes, disableNotifications, enableNotifications, injectCallback, setSFMCPushToken };

	public Object createInstance(final Object[] params) {
 return new com.analytics.customerone.OneSDKManager(
 (java.lang.String)params[0] , (java.lang.String)params[1] , (java.lang.String)params[2] , (java.lang.String)params[3] , (java.lang.String)params[4] , (java.lang.String)params[5] , (java.lang.String)params[6] , (java.lang.String)params[7] , (java.lang.String)params[8] );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 3 || paramLen > 4){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String interactionPath0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 interactionPath0 = (java.lang.String)params[0+inc];
 }
 java.lang.String interactionProperties0 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 interactionProperties0 = (java.lang.String)params[1+inc];
 }
 com.konylabs.vm.Function interactionCB0 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 interactionCB0 = (com.konylabs.vm.Function)params[2+inc];
 }
 ret = this.sendInteraction(params[0]
 ,interactionPath0
 ,interactionProperties0
 ,interactionCB0
 );
 
 			break;
 		case 1:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String properties1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 properties1 = (java.lang.String)params[0+inc];
 }
 com.konylabs.vm.Function successCallback1 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 successCallback1 = (com.konylabs.vm.Function)params[1+inc];
 }
 ret = this.sendBaseTouchPoints(params[0]
 ,properties1
 ,successCallback1
 );
 
 			break;
 		case 2:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String senderId2 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 senderId2 = (java.lang.String)params[0+inc];
 }
 java.lang.String pushToken2 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 pushToken2 = (java.lang.String)params[1+inc];
 }
 ret = this.enableAndSendPushToken(params[0]
 ,senderId2
 ,pushToken2
 );
 
 			break;
 		case 3:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.getTID(params[0]
 );
 
 			break;
 		case 4:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.enableMarketingCloudSdk(params[0]
 );
 
 			break;
 		case 5:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text5 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text5 = (java.lang.String)params[0+inc];
 }
 ret = this.sendAttributes(params[0]
 ,text5
 );
 
 			break;
 		case 6:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.disableNotifications(params[0]
 );
 
 			break;
 		case 7:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.enableNotifications(params[0]
 );
 
 			break;
 		case 8:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callback8 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callback8 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.injectCallback(params[0]
 ,callback8
 );
 
 			break;
 		case 9:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String pushToken9 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 pushToken9 = (java.lang.String)params[0+inc];
 }
 ret = this.setSFMCPushToken(params[0]
 ,pushToken9
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "OneManagerAndroid";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] sendInteraction( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ,com.konylabs.vm.Function inputKey2
 ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).sendInteraction( inputKey0
 , inputKey1
 , (com.konylabs.vm.Function)inputKey2
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] sendBaseTouchPoints( Object self ,java.lang.String inputKey0
 ,com.konylabs.vm.Function inputKey1
 ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).sendBaseTouchPoints( inputKey0
 , (com.konylabs.vm.Function)inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] enableAndSendPushToken( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).enablePush( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] getTID( Object self ){
 
		Object[] ret = null;
 java.lang.String val = ((com.analytics.customerone.OneSDKManager)self).getTID( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] enableMarketingCloudSdk( Object self ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).enableMarketingCloudSdk( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] sendAttributes( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).sendAttributes( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] disableNotifications( Object self ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).disableNotifications( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] enableNotifications( Object self ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).enableNotifications( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] injectCallback( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).injectCallback( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setSFMCPushToken( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.analytics.customerone.OneSDKManager)self).setSFMCPushToken( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
