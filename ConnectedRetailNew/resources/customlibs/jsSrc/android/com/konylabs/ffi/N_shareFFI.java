package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.share.Share;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_shareFFI extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new ShareTest();
 return libs;
 }



	public N_shareFFI(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "shareFFI";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class ShareTest extends JSLibrary {

 
 
	public static final String shareSomething = "shareSomething";
 
	String[] methods = { shareSomething };

	public Object createInstance(final Object[] params) {
 return new com.michaels.share.Share(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.LuaTable message0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 message0 = (com.konylabs.vm.LuaTable)params[0+inc];
 }
 ret = this.shareSomething(params[0]
 ,message0
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "ShareTest";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] shareSomething( Object self ,com.konylabs.vm.LuaTable inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.share.Share)self).shareSomething( (java.util.Vector)TableLib.convertToList(inputKey0)
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
