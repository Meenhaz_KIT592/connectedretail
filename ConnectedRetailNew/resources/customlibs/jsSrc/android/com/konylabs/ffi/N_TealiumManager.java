package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.tealium.TealiumManager;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_TealiumManager extends JSLibrary {

 
 
	public static final String initialize = "initialize";
 
 
	public static final String trackView = "trackView";
 
 
	public static final String trackEvent = "trackEvent";
 
	String[] methods = { initialize, trackView, trackEvent };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_TealiumManager(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.initialize( );
 
 			break;
 		case 1:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String viewName1 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 viewName1 = (java.lang.String)params[0];
 }
 java.lang.Object data1 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 data1 = (java.lang.Object)params[1];
 }
 ret = this.trackView( viewName1, data1 );
 
 			break;
 		case 2:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String eventName2 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 eventName2 = (java.lang.String)params[0];
 }
 java.lang.Object data2 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 data2 = (java.lang.Object)params[1];
 }
 ret = this.trackEvent( eventName2, data2 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "TealiumManager";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] initialize( ){
 
		Object[] ret = null;
 com.michaels.tealium.TealiumManager.initialize( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] trackView( java.lang.String inputKey0, java.lang.Object inputKey1 ){
 
		Object[] ret = null;
 com.michaels.tealium.TealiumManager.trackView( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] trackEvent( java.lang.String inputKey0, java.lang.Object inputKey1 ){
 
		Object[] ret = null;
 com.michaels.tealium.TealiumManager.trackEvent( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
