package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.imagerotate.RotateImage;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_imagerotateforandroid extends JSLibrary {

 
 
	public static final String getRotatedImage = "getRotatedImage";
 
 
	public static final String deleteTmpImage = "deleteTmpImage";
 
	String[] methods = { getRotatedImage, deleteTmpImage };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_imagerotateforandroid(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 3){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String imageString0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 imageString0 = (java.lang.String)params[0];
 }
 Double maxSideOut0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 maxSideOut0 = (Double)params[1];
 }
 Double compFactor0 = null;
 if(params[2] != null && params[2] != LuaNil.nil) {
 compFactor0 = (Double)params[2];
 }
 ret = this.getRotatedImage( imageString0, maxSideOut0, compFactor0 );
 
 			break;
 		case 1:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.deleteTmpImage( );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "imagerotateforandroid";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] getRotatedImage( java.lang.String inputKey0, Double inputKey1, Double inputKey2 ){
 
		Object[] ret = null;
 java.lang.String val = com.michaels.imagerotate.RotateImage.getRotatedImage( inputKey0
 , inputKey1.intValue() , inputKey2.intValue() );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] deleteTmpImage( ){
 
		Object[] ret = null;
 com.michaels.imagerotate.RotateImage.deleteImage( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
