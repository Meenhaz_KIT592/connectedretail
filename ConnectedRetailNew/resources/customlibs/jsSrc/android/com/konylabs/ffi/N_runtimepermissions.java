package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.settinglib.PermissionSettings;
import com.michaels.settinglib.PermissionWrapper;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_runtimepermissions extends JSLibrary {

 
 
	public static final String invokePermission = "invokePermission";
 
	String[] methods = { invokePermission };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new permissions();
 return libs;
 }



	public N_runtimepermissions(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 com.konylabs.vm.Function callback0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 callback0 = (com.konylabs.vm.Function)params[0];
 }
 ret = this.invokePermission( callback0 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "runtimepermissions";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] invokePermission( com.konylabs.vm.Function inputKey0 ){
 
		Object[] ret = null;
 com.michaels.settinglib.PermissionWrapper.invokePermission( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 


class permissions extends JSLibrary {

 
 
	public static final String setOktext = "setOktext";
 
 
	public static final String setMessage = "setMessage";
 
 
	public static final String setCancelText = "setCancelText";
 
 
	public static final String setServiceText = "setServiceText";
 
 
	public static final String requestPermission = "requestPermission";
 
 
	public static final String setDenyText = "setDenyText";
 
 
	public static final String setDenyButtonText = "setDenyButtonText";
 
 
	public static final String verifyDenySetting = "verifyDenySetting";
 
	String[] methods = { setOktext, setMessage, setCancelText, setServiceText, requestPermission, setDenyText, setDenyButtonText, verifyDenySetting };

	public Object createInstance(final Object[] params) {
 return new com.michaels.settinglib.PermissionSettings(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text0 = (java.lang.String)params[0+inc];
 }
 ret = this.setOktext(params[0]
 ,text0
 );
 
 			break;
 		case 1:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text1 = (java.lang.String)params[0+inc];
 }
 ret = this.setMessage(params[0]
 ,text1
 );
 
 			break;
 		case 2:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text2 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text2 = (java.lang.String)params[0+inc];
 }
 ret = this.setCancelText(params[0]
 ,text2
 );
 
 			break;
 		case 3:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text3 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text3 = (java.lang.String)params[0+inc];
 }
 ret = this.setServiceText(params[0]
 ,text3
 );
 
 			break;
 		case 4:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text4 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text4 = (java.lang.String)params[0+inc];
 }
 ret = this.requestPermission(params[0]
 ,text4
 );
 
 			break;
 		case 5:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text5 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text5 = (java.lang.String)params[0+inc];
 }
 ret = this.setDenyText(params[0]
 ,text5
 );
 
 			break;
 		case 6:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text6 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text6 = (java.lang.String)params[0+inc];
 }
 ret = this.setDenyButtonText(params[0]
 ,text6
 );
 
 			break;
 		case 7:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String text7 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 text7 = (java.lang.String)params[0+inc];
 }
 ret = this.verifyDenySetting(params[0]
 ,text7
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "permissions";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] setOktext( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).setOktext( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setMessage( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).setMessage( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setCancelText( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).setCancelText( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setServiceText( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).setServiceText( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] requestPermission( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).requestPermission( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setDenyText( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).setDenyText( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setDenyButtonText( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.settinglib.PermissionSettings)self).setDenyButtonText( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] verifyDenySetting( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.michaels.settinglib.PermissionSettings)self).verifyDenySetting( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
}

};
