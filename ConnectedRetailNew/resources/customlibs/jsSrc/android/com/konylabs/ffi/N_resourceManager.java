package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.deviceutils.MichaelsResourceManager;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_resourceManager extends JSLibrary {

 
 
	public static final String isBluetoothAvailable = "isBluetoothAvailable";
 
 
	public static final String isGPSAvailable = "isGPSAvailable";
 
	String[] methods = { isBluetoothAvailable, isGPSAvailable };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_resourceManager(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.isBluetoothAvailable( );
 
 			break;
 		case 1:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.isGPSAvailable( );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "resourceManager";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] isBluetoothAvailable( ){
 
		Object[] ret = null;
 Boolean val = new Boolean(com.michaels.deviceutils.MichaelsResourceManager.isBluetoothAvailable( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isGPSAvailable( ){
 
		Object[] ret = null;
 Boolean val = new Boolean(com.michaels.deviceutils.MichaelsResourceManager.isGPSAvailable( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
