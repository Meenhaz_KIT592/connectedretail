package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.brightness.IncreaseScreenBrightness;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_bright extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new createInstance();
 return libs;
 }



	public N_bright(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "bright";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class createInstance extends JSLibrary {

 
 
	public static final String setBrightness = "setBrightness";
 
 
	public static final String getBrightness = "getBrightness";
 
 
	public static final String requestPermission = "requestPermission";
 
	String[] methods = { setBrightness, getBrightness, requestPermission };

	public Object createInstance(final Object[] params) {
 return new com.michaels.brightness.IncreaseScreenBrightness(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String brightness0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 brightness0 = (java.lang.String)params[0+inc];
 }
 ret = this.setBrightness(params[0]
 ,brightness0
 );
 
 			break;
 		case 1:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.getBrightness(params[0]
 );
 
 			break;
 		case 2:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.requestPermission(params[0]
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "createInstance";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] setBrightness( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.michaels.brightness.IncreaseScreenBrightness)self).setBrightness( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] getBrightness( Object self ){
 
		Object[] ret = null;
 Double val = new Double(((com.michaels.brightness.IncreaseScreenBrightness)self).getBrightness( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] requestPermission( Object self ){
 
		Object[] ret = null;
 ((com.michaels.brightness.IncreaseScreenBrightness)self).requestPermission( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
