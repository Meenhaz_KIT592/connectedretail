package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.michaels.beacon.MicBeaconManager;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_Beacon extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new BeaconManager();
 return libs;
 }



	public N_Beacon(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "Beacon";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class BeaconManager extends JSLibrary {

 
 
	public static final String initializeEstimote = "initializeEstimote";
 
 
	public static final String startMonitorListener = "startMonitorListener";
 
 
	public static final String isBlueToothEnabled = "isBlueToothEnabled";
 
 
	public static final String startDiscovery = "startDiscovery";
 
	String[] methods = { initializeEstimote, startMonitorListener, isBlueToothEnabled, startDiscovery };

	public Object createInstance(final Object[] params) {
 return new com.michaels.beacon.MicBeaconManager(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 4 || paramLen > 5){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String appid0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 appid0 = (java.lang.String)params[0+inc];
 }
 java.lang.String token0 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 token0 = (java.lang.String)params[1+inc];
 }
 Double timeOutPeriod0 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 timeOutPeriod0 = (Double)params[2+inc];
 }
 java.lang.String uuid0 = null;
 if(params[3+inc] != null && params[3+inc] != LuaNil.nil) {
 uuid0 = (java.lang.String)params[3+inc];
 }
 ret = this.initializeEstimote(params[0]
 ,appid0
 ,token0
 ,timeOutPeriod0
 ,uuid0
 );
 
 			break;
 		case 1:
 if (paramLen < 3 || paramLen > 4){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callerEnterCallBack1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callerEnterCallBack1 = (com.konylabs.vm.Function)params[0+inc];
 }
 com.konylabs.vm.Function callerExitCallBack1 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 callerExitCallBack1 = (com.konylabs.vm.Function)params[1+inc];
 }
 com.konylabs.vm.Function beaconMgmtCallback1 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 beaconMgmtCallback1 = (com.konylabs.vm.Function)params[2+inc];
 }
 ret = this.startMonitorListener(params[0]
 ,callerEnterCallBack1
 ,callerExitCallBack1
 ,beaconMgmtCallback1
 );
 
 			break;
 		case 2:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.isBlueToothEnabled(params[0]
 );
 
 			break;
 		case 3:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.startDiscovery(params[0]
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "BeaconManager";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] initializeEstimote( Object self ,java.lang.String inputKey0
 ,java.lang.String inputKey1
 ,Double inputKey2
 ,java.lang.String inputKey3
 ){
 
		Object[] ret = null;
 ((com.michaels.beacon.MicBeaconManager)self).initializeEstimote( inputKey0
 , inputKey1
 , inputKey2.intValue() , inputKey3
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] startMonitorListener( Object self ,com.konylabs.vm.Function inputKey0
 ,com.konylabs.vm.Function inputKey1
 ,com.konylabs.vm.Function inputKey2
 ){
 
		Object[] ret = null;
 ((com.michaels.beacon.MicBeaconManager)self).startMonitorListener( (com.konylabs.vm.Function)inputKey0
 , (com.konylabs.vm.Function)inputKey1
 , (com.konylabs.vm.Function)inputKey2
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isBlueToothEnabled( Object self ){
 
		Object[] ret = null;
 Boolean val = new Boolean(((com.michaels.beacon.MicBeaconManager)self).isBlueToothEnabled( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] startDiscovery( Object self ){
 
		Object[] ret = null;
 ((com.michaels.beacon.MicBeaconManager)self).startDiscovery( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
