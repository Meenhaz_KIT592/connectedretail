package com.konylabs.customwidget;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;

import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;
import com.konylabs.api.ui.KonyCustomWidget;

public class N_customWidget extends JSLibrary {

 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[4];
 libs[0] = new barcodescanner();
 libs[1] = new Zoom();
 libs[2] = new ZoomWidget();
 libs[3] = new CustomAnimWidget();
 return libs;
 }
		
		public N_customWidget(){
	}
	public String getNameSpace() {
		return "customWidget";
	}

static class barcodescanner extends JSLibrary {
 
 
	public static final String startCamera = "startCamera";
 
 
	public static final String endCamera = "endCamera";
 
	String[] methods = { startCamera, endCamera };
 String[] props = { "scanResultCallback" };
 private static Hashtable<String, Integer> propertyTypeMappings = null;
 public Object createInstance(final Object[] properties, long jsobject) {
 if(propertyTypeMappings == null) {
 propertyTypeMappings = new Hashtable<String, Integer>();
 propertyTypeMappings.put("scanResultCallback",KonyCustomWidget.NATIVE_DATA_TYPE_FUNCTION);
 
 }

 KonyCustomWidget widget = new com.michaels.barcode.BarcodeWidgetView();
 widget.initProperties(properties,jsobject,propertyTypeMappings);
 return widget;
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.startCamera(params[0]
 );
 
 			break;
 		case 1:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.endCamera(params[0]
 );
 
 			break;
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "barcodescanner";
	}
	public String[] getProperties() {
		// TODO Auto-generated method stub
		return props;
	}
	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] startCamera( Object self ){
 
		Object[] ret = null;
 ((com.michaels.barcode.BarcodeWidgetView)self).startCamera( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] endCamera( Object self ){
 
		Object[] ret = null;
 ((com.michaels.barcode.BarcodeWidgetView)self).stopCamera( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}


static class Zoom extends JSLibrary {
 
 
	public static final String updateImageUrl = "updateImageUrl";
 
	String[] methods = { updateImageUrl };
 String[] props = { "imageWidth", "imageHeight", "handleTouchStart", "handleTouchEnd", "widgetWidth", "widgetHeight", "context" };
 private static Hashtable<String, Integer> propertyTypeMappings = null;
 public Object createInstance(final Object[] properties, long jsobject) {
 if(propertyTypeMappings == null) {
 propertyTypeMappings = new Hashtable<String, Integer>();
 propertyTypeMappings.put("imageWidth",KonyCustomWidget.NATIVE_DATA_TYPE_PRIMITIVE_INT);
 propertyTypeMappings.put("imageHeight",KonyCustomWidget.NATIVE_DATA_TYPE_PRIMITIVE_INT);
 propertyTypeMappings.put("handleTouchStart",KonyCustomWidget.NATIVE_DATA_TYPE_FUNCTION);
 propertyTypeMappings.put("handleTouchEnd",KonyCustomWidget.NATIVE_DATA_TYPE_FUNCTION);
 propertyTypeMappings.put("widgetWidth",KonyCustomWidget.NATIVE_DATA_TYPE_PRIMITIVE_INT);
 propertyTypeMappings.put("widgetHeight",KonyCustomWidget.NATIVE_DATA_TYPE_PRIMITIVE_INT);
 propertyTypeMappings.put("context",KonyCustomWidget.NATIVE_DATA_TYPE_OBJECT);
 
 }

 KonyCustomWidget widget = new com.kony.customwidgets.ZoomImage();
 widget.initProperties(properties,jsobject,propertyTypeMappings);
 return widget;
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String imageURL0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 imageURL0 = (java.lang.String)params[0+inc];
 }
 ret = this.updateImageUrl(params[0]
 ,imageURL0
 );
 
 			break;
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "Zoom";
	}
	public String[] getProperties() {
		// TODO Auto-generated method stub
		return props;
	}
	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] updateImageUrl( Object self ,java.lang.String inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.customwidgets.ZoomImage)self).updateImageUrl( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}


static class ZoomWidget extends JSLibrary {
 
	String[] methods = { };
 String[] props = { "imageURL" };
 private static Hashtable<String, Integer> propertyTypeMappings = null;
 public Object createInstance(final Object[] properties, long jsobject) {
 if(propertyTypeMappings == null) {
 propertyTypeMappings = new Hashtable<String, Integer>();
 propertyTypeMappings.put("imageURL",KonyCustomWidget.NATIVE_DATA_TYPE_STRING);
 
 }

 KonyCustomWidget widget = new com.kony.zoom.ZoomView();
 widget.initProperties(properties,jsobject,propertyTypeMappings);
 return widget;
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "ZoomWidget";
	}
	public String[] getProperties() {
		// TODO Auto-generated method stub
		return props;
	}
	/*
	 * return should be status(0 and !0),address
	 */
 
}


static class CustomAnimWidget extends JSLibrary {
 
 
	public static final String setProgress = "setProgress";
 
	String[] methods = { setProgress };
 String[] props = { };
 private static Hashtable<String, Integer> propertyTypeMappings = null;
 public Object createInstance(final Object[] properties, long jsobject) {
 if(propertyTypeMappings == null) {
 propertyTypeMappings = new Hashtable<String, Integer>();
 
 }

 KonyCustomWidget widget = new com.michaels.customanimindicator.CustomAnimWidget();
 widget.initProperties(properties,jsobject,propertyTypeMappings);
 return widget;
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 try {
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 4 || paramLen > 5){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 Integer duration0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 duration0 = ((Double)params[0+inc]).intValue();
 }
 Integer startAt0 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 startAt0 = ((Double)params[1+inc]).intValue();
 }
 Integer endAt0 = null;
 if(params[2+inc] != null && params[2+inc] != LuaNil.nil) {
 endAt0 = ((Double)params[2+inc]).intValue();
 }
 com.konylabs.vm.Function callBackEvent0 = null;
 if(params[3+inc] != null && params[3+inc] != LuaNil.nil) {
 callBackEvent0 = (com.konylabs.vm.Function)params[3+inc];
 }
 ret = this.setProgress(params[0]
 ,duration0
 ,startAt0
 ,endAt0
 ,callBackEvent0
 );
 
 			break;
 		default:
			break;
		}
 }catch (Exception e){
			ret = new Object[]{e.getMessage(), new Double(101), e.getMessage()};
		}
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "CustomAnimWidget";
	}
	public String[] getProperties() {
		// TODO Auto-generated method stub
		return props;
	}
	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] setProgress( Object self ,Integer inputKey0
 ,Integer inputKey1
 ,Integer inputKey2
 ,com.konylabs.vm.Function inputKey3
 ){
 
		Object[] ret = null;
 ((com.michaels.customanimindicator.CustomAnimWidget)self).setProgress( inputKey0.intValue() , inputKey1.intValue() , inputKey2.intValue() , (com.konylabs.vm.Function)inputKey3
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};
