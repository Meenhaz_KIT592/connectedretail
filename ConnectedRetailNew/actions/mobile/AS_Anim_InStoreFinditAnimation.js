function AS_Anim_InStoreFinditAnimation(eventobject) {
    return AS_Form_d6612ad3508b4310ae07c3351ecc09b0(eventobject);
}

function AS_Form_d6612ad3508b4310ae07c3351ecc09b0(eventobject) {
    function TRANSFORM_ACTION_NEW____3683825ae4fd435f895cf5f5d16be15a_Callback() {
        frmHome.flxFindit["isVisible"] = false;
    }

    function STYLE_ACTION____f8d1e5ea9a3d46ae85b259050e0b3862_Callback() {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.scale(0.1, 0.1);
        frmHome.flxFindit.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": TRANSFORM_ACTION_NEW____3683825ae4fd435f895cf5f5d16be15a_Callback
        });
    }

    function TRANSFORM_ACTION____dbcdb213db6b4514baf858994d76db68_Callback() {}

    function ____f0419c72de62481491f9c33728e573e8_Callback() {
        frmHome.btnMyList["isVisible"] = true;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.scale(1, 1);
        frmHome.btnMyList.animate(kony.ui.createAnimation({
            "100": {
                "centerY": "95dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 1.5,
            "iterationCount": "1",
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": TRANSFORM_ACTION____dbcdb213db6b4514baf858994d76db68_Callback
        });
    }

    function ____4e7c55f647814d129ddda3f2fe459d9c_Callback() {}

    function TRANSFORM_ACTION_NEW____8d453622f395443bbfe8ff6e644b6168_Callback() {
        frmHome.btnBrowseProducts["isVisible"] = true;
        var trans100 = kony.ui.makeAffineTransform();
        trans100.scale(1, 1);
        frmHome.btnBrowseProducts.animate(kony.ui.createAnimation({
            "100": {
                "centerY": "40dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 1.5,
            "iterationCount": "1",
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": ____4e7c55f647814d129ddda3f2fe459d9c_Callback
        });
    }

    function MOVE_ACTION____683b9fff1d5d485ab29cc307d04eedab_Callback() {
        var trans100 = kony.ui.makeAffineTransform();
        trans100.scale(0.5, 0.5);
        frmHome.btnBrowseProducts.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.001
        }, {
            "animationEnd": TRANSFORM_ACTION_NEW____8d453622f395443bbfe8ff6e644b6168_Callback
        });
        var trans100 = kony.ui.makeAffineTransform();
        trans100.scale(0.5, 0.5);
        frmHome.btnMyList.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.001
        }, {
            "animationEnd": ____f0419c72de62481491f9c33728e573e8_Callback
        });
        frmHome.lblFindit.animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "opacity": 0.01
            }
        }), {
            "delay": 1.5,
            "iterationCount": "1",
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.05
        }, {
            "animationEnd": STYLE_ACTION____f8d1e5ea9a3d46ae85b259050e0b3862_Callback
        });
    }

    function MOVE_ACTION____fea66017f2d442d8ac95ae96109ba2ad_Callback() {
        frmHome.flxFindit.animate(kony.ui.createAnimation({
            "100": {
                "centerY": "50%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": MOVE_ACTION____683b9fff1d5d485ab29cc307d04eedab_Callback
        });
    }
    frmHome.flxFindit.animate(kony.ui.createAnimation({
        "100": {
            "centerY": "60%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 1,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": MOVE_ACTION____fea66017f2d442d8ac95ae96109ba2ad_Callback
    });
}