function AS_Anim_asTransRotate(eventobject) {
    return AS_Button_ef1133be64f949ee982efa8b01a02cf5(eventobject);
}

function AS_Button_ef1133be64f949ee982efa8b01a02cf5(eventobject) {
    function ROTATE_ACTION____b9f10d36f32e4c7195c4eb7a926842f7_Callback() {}
    var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate(180);
    frmHome.lblChevron.animate(kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": ROTATE_ACTION____b9f10d36f32e4c7195c4eb7a926842f7_Callback
    });
}